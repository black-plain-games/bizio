# Getting Started

1. Build the Bizio.Server solution
1. Publish the `Bizio.Database` project by double-clicking the `local.publish.xml` file
1. Add `127.0.0.1	bizio.local` to your hosts files
1. Add a new site to IIS called `bizio`
  1. Edit the only existing binding, giving it a host name of `bizio.local`
1. In SSMS, right click `Security` > `Logins` and add a new Login
  1. Name it `IIS APPPOOL\bizio`
  1. Under `User Mapping`, check off `Bizio`
  1 Also check off `db_datareader` and `db_datawriter`
1. Build the `Bizio.Client.Desktop` solution
1. Run `bizio.reg` in the `source\Release` folder