﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ProjectBuilder
{
    public class Range
    {
        public int Minimum { get; set; }

        public int Maximum { get; set; }
    }

    public class Skill
    {
        public int Id { get; set; }

        public Range Value { get; set; }

        public bool IsRequired { get; set; }
    }

    public class ProjectDefinition
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public Range Value { get; set; }

        public Range Length { get; set; }

        public List<Skill> Skills { get; set; }
    }

    internal class Program
    {
        private static Range GetRange(IEnumerable<string> cells, int index)
        {
            if (int.TryParse(cells.ElementAt(index).Replace(",", ""), out var minimum) &&
                int.TryParse(cells.ElementAt(index + 1).Replace(",", ""), out var maximum))
            {
                return new Range
                {
                    Minimum = minimum,
                    Maximum = maximum
                };
            }

            return null;
        }

        private static readonly string ProjectDefinitionFormatString = @"PRINT 'ProjectDefinition'

SET IDENTITY_INSERT [dbo].[ProjectDefinition] ON

MERGE INTO [dbo].[ProjectDefinition] AS TARGET
    USING ( VALUES
        {0}
    ) AS SOURCE ([Id],[Name],[Description],[MinimumValue],[MaximumValue],[MinimumProjectLength],[MaximumProjectLength])
ON TARGET.[Id] = SOURCE.[Id]
WHEN MATCHED THEN
    UPDATE SET
        [Name] = SOURCE.[Name],
        [Description] = SOURCE.[Description],
        [MinimumValue] = SOURCE.[MinimumValue],
        [MaximumValue] = SOURCE.[MaximumValue],
        [MinimumProjectLength] = SOURCE.[MinimumProjectLength],
        [MaximumProjectLength] = SOURCE.[MaximumProjectLength]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([Id],[Name],[Description],[MinimumValue],[MaximumValue],[MinimumProjectLength],[MaximumProjectLength])
    VALUES ([Id],[Name],[Description],[MinimumValue],[MaximumValue],[MinimumProjectLength],[MaximumProjectLength]);

SET IDENTITY_INSERT [dbo].[ProjectDefinition] OFF";

        private static readonly string SkillsFormatString = @"PRINT 'ProjectDefinitionSkill'

DELETE
FROM [dbo].[ProjectDefinitionSkill]

MERGE INTO [dbo].[ProjectDefinitionSkill] AS TARGET
    USING ( VALUES
        {0}
    ) AS SOURCE ([ProjectDefinitionId],[SkillDefinitionId],[Minimum],[Maximum],[IsRequired])
ON TARGET.[ProjectDefinitionId] = SOURCE.[ProjectDefinitionId] AND TARGET.[SkillDefinitionId] = SOURCE.[SkillDefinitionId]
WHEN MATCHED THEN
    UPDATE SET
        [Minimum] = SOURCE.[Minimum],
        [Maximum] = SOURCE.[Maximum],
        [IsRequired] = SOURCE.[IsRequired]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([ProjectDefinitionId],[SkillDefinitionId],[Minimum],[Maximum],[IsRequired])
    VALUES ([ProjectDefinitionId],[SkillDefinitionId],[Minimum],[Maximum],[IsRequired]);";

        private static readonly string IndustryFormatString = @"PRINT 'ProjectDefinitionIndustry'

DELETE
FROM [dbo].[ProjectDefinitionIndustry]

MERGE INTO [dbo].[ProjectDefinitionIndustry] AS TARGET
    USING ( VALUES
        {0}
    ) AS SOURCE ([IndustryId],[ProjectDefinitionId])
ON TARGET.[ProjectDefinitionId] = SOURCE.[ProjectDefinitionId] AND TARGET.[IndustryId] = SOURCE.[IndustryId]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([IndustryId],[ProjectDefinitionId])
    VALUES ([IndustryId],[ProjectDefinitionId]);";

        private static void Main(string[] args)
        {
            var projects = new List<ProjectDefinition>();

            var rowLabels = new Dictionary<int, string>
            {
                { 0, "Name" },
                { 1, "Description" },
                { 2, "Value" },
                { 3, "Length" },

                { 4, "1" },
                { 5, "2" },
                { 6, "3" },
                { 7, "4" },
                { 8, "5" },

                { 9, "6" },
                { 10, "7" },
                { 11, "8" },

                { 12, "9" },
                { 13, "10" },
                { 14, "11" },

                { 15, "12" },
                { 16, "13" },
                { 17, "14" },

                { 18, "15" },
                { 19, "16" },
                { 20, "17" },

                { 21, "18" },
                { 22, "19" },
                { 23, "20" }
            };

            using (var reader = new StreamReader("projects.tsv"))
            {
                for (var row = 0; !reader.EndOfStream; row++)
                {
                    var line = reader.ReadLine();

                    if (!rowLabels.ContainsKey(row))
                    {
                        continue;
                    }

                    var cells = line.Split('\t').Skip(2);

                    var label = rowLabels[row];

                    for (var column = 0; column < cells.Count(); column += 3)
                    {
                        ProjectDefinition project = null;

                        if (label == "Name")
                        {
                            project = new ProjectDefinition();
                        }
                        else
                        {
                            project = projects[column / 3];
                        }

                        switch (label)
                        {
                            case "Name":
                                projects.Add(new ProjectDefinition
                                {
                                    Name = cells.ElementAt(column).Replace("'", "''"),
                                    Skills = new List<Skill>()
                                });
                                break;
                            case "Description":
                                project.Description = cells.ElementAt(column).Replace("'", "''");
                                break;
                            case "Value":
                                project.Value = GetRange(cells, column);
                                break;
                            case "Length":
                                project.Length = GetRange(cells, column);
                                break;

                            default:
                                var skill = new Skill
                                {
                                    Id = int.Parse(label)
                                };

                                skill.Value = GetRange(cells, column);

                                if (skill.Value == null)
                                {
                                    continue;
                                }

                                if (int.TryParse(cells.ElementAt(column + 2), out var isRequired))
                                {
                                    skill.IsRequired = isRequired != 0;
                                }

                                project.Skills.Add(skill);

                                break;
                        }
                    }
                }
            }

            var projectDefinitions = new List<string>();

            var skills = new List<string>();

            var industries = new List<string>();

            var index = 1;

            foreach (var project in projects)
            {
                industries.Add($"(1, {index})");

                projectDefinitions.Add($"({index}, '{project.Name}', '{project.Description}', {project.Value.Minimum}, {project.Value.Maximum}, {project.Length.Minimum}, {project.Length.Maximum})");

                foreach (var skill in project.Skills)
                {
                    skills.Add($"({index}, {skill.Id}, {skill.Value.Minimum}, {skill.Value.Maximum}, {(skill.IsRequired ? 1 : 0)})");
                }

                index++;
            }

            using (var writer = new StreamWriter(@"..\..\..\..\source\Bizio.Database\scripts\seed\ProjectDefinition.sql"))
            {
                writer.Write(string.Format(ProjectDefinitionFormatString, string.Join(",\r\n        ", projectDefinitions)));
            }

            using (var writer = new StreamWriter(@"..\..\..\..\source\Bizio.Database\scripts\seed\ProjectDefinitionSkill.sql"))
            {
                writer.Write(string.Format(SkillsFormatString, string.Join(",\r\n        ", skills)));
            }

            using (var writer = new StreamWriter(@"..\..\..\..\source\Bizio.Database\scripts\seed\ProjectDefinitionIndustry.sql"))
            {
                writer.Write(string.Format(IndustryFormatString, string.Join(",\r\n        ", industries)));
            }
        }
    }
}
