﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace XamlTextFinder
{
    public class FileToFix
    {
        public string Path { get; set; }

        public string Contents { get; set; }

        public List<Capture> Captures { get; set; }
    }

    internal class Program
    {
        private static readonly string languagesPath = @"D:\source\black-plain-games\bizio\source\Bizio.Client.Desktop\languages";

        private static IDictionary<string, IDictionary<string, string>> LoadLanguages()
        {
            using (var reader = new StreamReader(languagesPath))
            {
                return JsonConvert.DeserializeObject<IDictionary<string, IDictionary<string, string>>>(reader.ReadToEnd());
            }
        }

        private static void SaveLanguages(IDictionary<string, IDictionary<string, string>> languages)
        {
            using (var writer = new StreamWriter(languagesPath))
            {
                writer.Write(JsonConvert.SerializeObject(languages, Formatting.Indented));
            }
        }

        private static void FindAndReplaceTranslations()
        {
            var languages = LoadLanguages();

            var directory = @"D:\source\black-plain-games\bizio\source\Bizio.Client.Desktop\";

            var files = Directory.EnumerateFiles(directory, "*.xaml", SearchOption.AllDirectories);

            var regex = new Regex("(Text|Label|Header|Content)=\"([a-zA-Z\\s0-9.\\/?!$%^&*]*)\"");

            var reports = new List<FileToFix>();

            foreach (var file in files)
            {
                string fileContents = null;

                Console.WriteLine(file);

                using (var reader = new StreamReader(file))
                {
                    fileContents = reader.ReadToEnd();
                }

                var matches = regex.Matches(fileContents);

                var report = new List<Capture>();

                foreach (Match match in matches)
                {
                    foreach (Group group in match.Groups)
                    {
                        foreach (Capture capture in group.Captures)
                        {
                            report.Add(capture);
                        }
                    }
                }

                if (report.Any())
                {
                    reports.Add(new FileToFix
                    {
                        Path = file,
                        Contents = fileContents,
                        Captures = report
                    });
                }
            }

            var reportFile = @"report.txt";

            using (var writer = new StreamWriter(reportFile))
            {
                foreach (var report in reports)
                {
                    writer.WriteLine(report.Path);

                    foreach (var capture in report.Captures)
                    {
                        writer.WriteLine($"\t{capture.Index}\t{capture.Length}\t{capture.Value}");
                    }
                }
            }

            Console.WriteLine();

            Console.WriteLine();

            Console.WriteLine("Replacements");

            Console.WriteLine();

            Console.WriteLine();

            var replacements = new Dictionary<FileToFix, Dictionary<string, string>>();

            var specialCharacters = "~!@#$%^&*()_+-=/\\|<>,.'\"`?\t\r\n ".Select(c => $"{c}");

            var uiText = new StringBuilder();

            foreach (var file in reports)
            {
                var fileReplacements = new Dictionary<string, string>();

                replacements.Add(file, fileReplacements);

                for (var index = 2; index < file.Captures.Count; index += 3)
                {
                    var value = file.Captures[index].Value;

                    if (fileReplacements.Values.Contains(value) ||
                        string.Compare("bizio", value, true) == 0)
                    {
                        continue;
                    }

                    var key = value;

                    foreach (var specialCharacter in specialCharacters)
                    {
                        key = key.Replace(specialCharacter, "");
                    }

                    if (string.IsNullOrWhiteSpace(key))
                    {
                        continue;
                    }

                    if (!languages["en"].ContainsKey(key))
                    {
                        Console.WriteLine($"[{key}]:\t\"{value}\"");

                        languages["en"].Add(key, value);

                        uiText.AppendLine();

                        uiText.AppendLine($"        public static string {key} => Get();");
                    }

                    var attribute = file.Captures[index - 1].Value;

                    if (!fileReplacements.ContainsKey(file.Captures[index - 2].Value))
                    {
                        fileReplacements.Add(file.Captures[index - 2].Value, $"{attribute}=\"{{x:Static z:UiText.{key}}}\"");
                    }
                }
            }

            SaveLanguages(languages);

            //foreach (var file in replacements)
            //{
            //    using (var writer = new StreamWriter(file.Key.Path.Replace(".xaml", "_old.xaml")))
            //    {
            //        writer.WriteLine(file.Key.Contents);
            //    }

            //    var fileContents = file.Key.Contents;

            //    foreach (var replacement in file.Value)
            //    {
            //        fileContents = fileContents.Replace(replacement.Key, replacement.Value);
            //    }

            //    using (var writer = new StreamWriter(file.Key.Path))
            //    {
            //        writer.WriteLine(fileContents);
            //    }
            //}

            var uiTextUpdates = "UiText.txt";

            using (var writer = new StreamWriter(uiTextUpdates))
            {
                writer.Write(uiText);
            }
        }

        private static void ConvertToSql()
        {
            var languages = LoadLanguages();

            var languageScript = @"PRINT 'Language'

MERGE INTO [dbo].[Language] AS TARGET
    USING ( VALUES
{0}
    ) AS SOURCE ([Id],[Name],[Iso2LetterCode])
ON TARGET.[Id] = SOURCE.[Id]
WHEN MATCHED THEN
    UPDATE SET
        [Name] = SOURCE.[Name],
        [Iso2LetterCode] = SOURCE.[Iso2LetterCode]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([Id],[Name],[Iso2LetterCode])
    VALUES ([Id],[Name],[Iso2LetterCode]);";

            var textResourceKeyScript = @"PRINT 'TextResourceKey'

MERGE INTO [dbo].[TextResourceKey] AS TARGET
    USING ( VALUES
{0}
    ) AS SOURCE ([Id],[Name])
ON TARGET.[Id] = SOURCE.[Id]
WHEN MATCHED THEN
    UPDATE SET
        [Name] = SOURCE.[Name]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([Id],[Name])
    VALUES ([Id],[Name]);";

            var textResourceScript = @"PRINT 'TextResource'

MERGE INTO [dbo].[TextResource] AS TARGET
    USING ( VALUES
{0}
    ) AS SOURCE ([LanguageId],[TextResourceKeyId],[Text])
ON TARGET.[LanguageId] = SOURCE.[LanguageId] AND TARGET.[TextResourceKeyId] = SOURCE.[TextResourceKeyId]
WHEN MATCHED THEN
    UPDATE SET
        [Text] = SOURCE.[Text],
        [DateModified] = SYSDATETIMEOFFSET()
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([LanguageId],[TextResourceKeyId],[Text])
    VALUES ([LanguageId],[TextResourceKeyId],[Text]);";

            var languageValues = new List<string>();

            var languageIndex = 1;

            var textResourceKeys = new Dictionary<string, int>();

            var textResourceKeyValues = new List<string>();

            var textResourceKeyId = 1;

            var textResourceValues = new List<string>();

            foreach (var language in languages)
            {
                var culture = CultureInfo.GetCultureInfo(language.Key);

                languageValues.Add($"\t\t({languageIndex}, '{culture.NativeName}', '{language.Key}')");

                foreach (var textResource in language.Value)
                {
                    if (!textResourceKeys.ContainsKey(textResource.Key))
                    {
                        textResourceKeyValues.Add($"\t\t({textResourceKeyId}, '{textResource.Key}')");

                        textResourceKeys.Add(textResource.Key, textResourceKeyId++);
                    }

                    var currentTextResourceKeyId = textResourceKeys[textResource.Key];

                    textResourceValues.Add($"\t\t({languageIndex}, {currentTextResourceKeyId}, '{textResource.Value.Replace("'", "''").Replace("&#39;", "''")}')");
                }

                languageIndex++;
            }

            using (var writer = new StreamWriter(@"D:\source\black-plain-games\bizio\source\Bizio.Database\scripts\seed\Language.sql"))
            {
                writer.Write(languageScript, string.Join(",\r\n", languageValues));
            }

            using (var writer = new StreamWriter(@"D:\source\black-plain-games\bizio\source\Bizio.Database\scripts\seed\TextResourceKey.sql"))
            {
                writer.Write(textResourceKeyScript, string.Join(",\r\n", textResourceKeyValues));
            }

            using (var writer = new StreamWriter(@"D:\source\black-plain-games\bizio\source\Bizio.Database\scripts\seed\TextResource.sql"))
            {
                writer.Write(textResourceScript, string.Join(",\r\n", textResourceValues));
            }
        }

        private static readonly string _urlFormat = "language/translate/v2?key=AIzaSyB0umucTBDABC6JvuQFQl8XEsiG0jgSY_M&source=en&target={0}&q={1}";

        private static void AddTranslation(string targetLanguage, IDictionary<string, IDictionary<string, string>> languages)
        {
            var client = new RestClient("https://translation.googleapis.com/");

            var language = new Dictionary<string, string>();

            var english = languages["en"];

            foreach (var key in english)
            {
                var request = new RestRequest(string.Format(_urlFormat, targetLanguage, HttpUtility.UrlEncode(key.Value)), Method.GET);

                var response = client.Execute(request);

                var translation = GetTranslation(response.Content);

                Console.WriteLine($"\t{key.Key}");

                Console.WriteLine($"\t{translation}");

                Console.WriteLine();

                language.Add(key.Key, translation);
            }

            if (languages.ContainsKey(targetLanguage))
            {
                languages[targetLanguage] = language;
            }
            else
            {
                languages.Add(targetLanguage, language);
            }
        }

        private static string GetTranslation(string responseBody)
        {
            var result = JsonConvert.DeserializeObject<TranslateResponse>(responseBody);

            return result.Data.Translations.FirstOrDefault()?.TranslatedText;
        }

        private static void Main(string[] args)
        {
            decimal x = 3.2m;

            byte y = 3;

            decimal z = x * y;

            ConvertToSql();

            //var languages = LoadLanguages();

            //var targets = new[] { "es", "hi", "fr", "ru" };

            //foreach (var target in targets)
            //{
            //    Console.WriteLine($"Convertering to {target}");

            //    AddTranslation(target, languages);
            //}

            //SaveLanguages(languages);

            Console.ReadKey();
        }

        private class Translation
        {
            public string TranslatedText { get; set; }
        }

        private class Data
        {
            public Translation[] Translations { get; set; }
        }

        private class TranslateResponse
        {
            public Data Data { get; set; }
        }
    }
}
