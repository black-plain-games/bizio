SELECT SD.Name, COUNT(1) AS Count
FROM ActionRequirement AR
JOIN SkillDefinition SD
	ON SD.Id = AR.SkillDefinitionId
GROUP BY SD.Name
ORDER BY COUNT(1) DESC

SELECT A.Name, COUNT(1) AS RequirementCount, SUM(AR.Value) AS TotalValue, AVG(AR.Value) AS AverageValue
FROM ActionRequirement AR
JOIN Action A
	ON A.Id = AR.ActionId
GROUP BY A.Name
ORDER BY AVG(AR.Value) DESC, SUM(AR.Value) DESC


SELECT *
FROM CompanyActionAccumulation
WHERE ActionID = 1