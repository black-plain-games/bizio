; WITH HighWeightSkills AS (
SELECT PSD.SkillDefinitionId, COUNT(1) AS HighWeightCount
FROM ProfessionSkillDefinition PSD
WHERE PSD.Weight > 50
GROUP BY PSD.SkillDefinitionId)
, LowWeightSkills AS (
SELECT PSD.SkillDefinitionId, COUNT(1) AS LowWeightCount
FROM ProfessionSkillDefinition PSD
WHERE PSD.Weight < 50
GROUP BY PSD.SkillDefinitionId)
SELECT SD.Id, SD.Name, HWS.HighWeightCount, LWS.LowWeightCount
FROM SkillDefinition SD
LEFT JOIN HighWeightSkills HWS
	ON HWS.SkillDefinitionId = SD.Id
LEFT JOIN LowWeightSkills LWS
	ON LWS.SkillDefinitionId = SD.Id
ORDER BY HighWeightCount DESC