﻿using Bizio.Administration.Data;
using System;
using System.Globalization;
using System.Linq;

namespace Bizio.Administration.Converters
{
    public class IdToAttributeConverter : BaseValueConverter
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!byte.TryParse($"{value}", out var attributeId))
            {
                return null;
            }

            return StaticData.Attributes?.FirstOrDefault(a => a.Id == attributeId);
        }
    }
}