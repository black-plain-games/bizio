﻿using Bizio.Administration.Data;
using System;
using System.Globalization;
using System.Linq;

namespace Bizio.Administration.Converters
{
    class IdToIndustryConverter : BaseValueConverter
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!byte.TryParse($"{value}", out var industryId))
            {
                return null;
            }

            return StaticData.Industries?.FirstOrDefault(i => i.Id == industryId);
        }
    }
}