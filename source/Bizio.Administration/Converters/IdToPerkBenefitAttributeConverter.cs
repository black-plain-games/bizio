﻿using Bizio.Administration.Data;
using System;
using System.Globalization;
using System.Linq;

namespace Bizio.Administration.Converters
{
    public class IdToPerkBenefitAttributeConverter : BaseValueConverter
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var id = byte.Parse($"{value}");

            return StaticData.PerkBenefitAttributes.FirstOrDefault(ptt => ptt.Id == id);
        }
    }
}