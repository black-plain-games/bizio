﻿using Bizio.Administration.Data;
using System;
using System.Globalization;
using System.Linq;

namespace Bizio.Administration.Converters
{
    public class IdToPerkTargetTypeConverter : BaseValueConverter
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var id = byte.Parse($"{value}");

            return StaticData.PerkTargetTypes.FirstOrDefault(ptt => ptt.Id == id);
        }
    }
}