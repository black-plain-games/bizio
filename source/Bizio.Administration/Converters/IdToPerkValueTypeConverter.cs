﻿using Bizio.Administration.Data;
using System;
using System.Globalization;
using System.Linq;

namespace Bizio.Administration.Converters
{
    public class IdToPerkValueTypeConverter : BaseValueConverter
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var id = byte.Parse($"{value}");

            return StaticData.PerkValueTypes.FirstOrDefault(ptt => ptt.Id == id);
        }
    }
}