﻿using Bizio.Administration.Data;
using System;
using System.Globalization;
using System.Linq;

namespace Bizio.Administration.Converters
{
    public class IdToProfessionConverter : BaseValueConverter
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!byte.TryParse($"{value}", out var professionId))
            {
                return null;
            }

            return StaticData.Professions?.FirstOrDefault(p => p.Id == professionId);
        }
    }
}