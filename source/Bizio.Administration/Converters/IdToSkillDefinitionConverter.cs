﻿using Bizio.Administration.Data;
using System;
using System.Globalization;
using System.Linq;

namespace Bizio.Administration.Converters
{
    public class IdToSkillDefinitionConverter : BaseValueConverter
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!byte.TryParse($"{value}", out var skillDefinitionId))
            {
                return null;
            }

            return StaticData.SkillDefinitions?.FirstOrDefault(a => a.Id == skillDefinitionId);
        }
    }
}