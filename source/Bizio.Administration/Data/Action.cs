﻿using System.Collections.ObjectModel;

namespace Bizio.Administration.Data
{
    public class Action : EntityNotifyOnPropertyChanged<Entities.Action>
    {
        public byte Id
        {
            get
            {
                return GetField<byte>();
            }
            set
            {
                SetField(value);
            }
        }

        public string Name
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public int MaxCount
        {
            get
            {
                return GetField<int>();
            }
            set
            {
                SetField(value);
            }
        }

        public int DefaultCount
        {
            get
            {
                return GetField<int>();
            }
            set
            {
                SetField(value);
            }
        }

        public ObservableCollection<ActionRequirement> Requirements
        {
            get
            {
                return GetField<ObservableCollection<ActionRequirement>>();
            }
            set
            {
                SetField(value);
            }
        }
    }
}