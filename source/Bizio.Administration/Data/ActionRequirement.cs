﻿namespace Bizio.Administration.Data
{
    public class ActionRequirement : EntityNotifyOnPropertyChanged<Entities.ActionRequirement>
    {
        public byte SkillDefinitionId
        {
            get
            {
                return GetField<byte>();
            }
            set
            {
                SetField(value);
            }
        }

        public decimal Value
        {
            get
            {
                return GetField<decimal>();
            }
            set
            {
                SetField(value);
            }
        }
    }
}