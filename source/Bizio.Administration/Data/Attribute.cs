﻿namespace Bizio.Administration.Data
{
    public class Attribute : EntityNotifyOnPropertyChanged<Entities.Attribute>
    {
        public byte Id
        {
            get
            {
                return GetField<byte>();
            }
            set
            {
                SetField(value);
            }
        }

        public string Name
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }
    }
}