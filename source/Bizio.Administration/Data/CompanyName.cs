﻿namespace Bizio.Administration.Data
{
    public class CompanyName : EntityNotifyOnPropertyChanged<Entities.CompanyName>
    {
        public byte IndustryId
        {
            get
            {
                return GetField<byte>();
            }
            set
            {
                SetField(value);
            }
        }

        public string Name
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }
    }
}