﻿namespace Bizio.Administration.Data
{
    public class ConfigurationKey : EntityNotifyOnPropertyChanged<Entities.ConfigurationKey>
    {
        public byte Id
        {
            get
            {
                return GetField<byte>();
            }
            set
            {
                SetField(value);
            }
        }

        public string Name
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }
    }
}