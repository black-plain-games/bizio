﻿using BlackPlain.Wpf.Core;

namespace Bizio.Administration.Data
{
    public abstract class EntityNotifyOnPropertyChanged<TEntity> : NotifyOnPropertyChanged, IEntity<TEntity>
        where TEntity : class
    {
        public TEntity Entity { get; set; }
    }
}