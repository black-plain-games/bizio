﻿namespace Bizio.Administration.Data
{
    public enum EntityType
    {
        Professions,
        SkillDefinitions,
        Attributes,
        Personalities,
        GlobalConfigurations,
        GameConfigurations,
        ConfigurationKeys,
        Industries,
        CompanyNames,
        Games,
        ProjectDefinitions,
        Notifications,
        Actions,
        Perks,
        PerkTargetTypes,
        PerkValueTypes,
        PerkBenefitAttributes,
        PerkConditionAttributes,
        PerkConditionComparisons
    }
}