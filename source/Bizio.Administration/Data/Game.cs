﻿using System;

namespace Bizio.Administration.Data
{
    public class Game : EntityNotifyOnPropertyChanged<Entities.Game>
    {
        public int Id
        {
            get
            {
                return GetField<int>();
            }
            set
            {
                SetField(value);
            }
        }


        public DateTimeOffset CreatedDate
        {
            get
            {
                return GetField<DateTimeOffset>();
            }
            set
            {
                SetField(value);
            }
        }
    }
}