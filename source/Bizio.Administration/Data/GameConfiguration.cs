﻿namespace Bizio.Administration.Data
{
    public class GameConfiguration : EntityNotifyOnPropertyChanged<Entities.GameConfiguration>
    {
        public byte ConfigurationKeyId
        {
            get
            {
                return GetField<byte>();
            }
            set
            {
                SetField(value);
            }
        }

        public string Value
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public int GameId
        {
            get
            {
                return GetField<int>();
            }
            set
            {
                SetField(value);
            }
        }
    }
}