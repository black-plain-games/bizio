﻿namespace Bizio.Administration.Data
{
    public class GlobalConfiguration : EntityNotifyOnPropertyChanged<Entities.GlobalConfiguration>
    {
        public byte ConfigurationKeyId
        {
            get
            {
                return GetField<byte>();
            }
            set
            {
                SetField(value);
            }
        }

        public string Value
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }
    }
}