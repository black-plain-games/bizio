﻿namespace Bizio.Administration.Data
{
    public interface IEntity<T>
        where T : class
    {
        T Entity { get; set; }
    }
}