﻿namespace Bizio.Administration.Data
{
    public class IdNamePair<TId, TName, TEntity> : EntityNotifyOnPropertyChanged<TEntity>
        where TEntity : class
    {
        public TId Id
        {
            get
            {
                return GetField<TId>();
            }
            set
            {
                SetField(value);
            }
        }

        public TName Name
        {
            get
            {
                return GetField<TName>();
            }
            set
            {
                SetField(value);
            }
        }
    }
}