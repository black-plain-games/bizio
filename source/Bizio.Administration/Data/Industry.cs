﻿using System.Collections.ObjectModel;

namespace Bizio.Administration.Data
{
    public class Industry : EntityNotifyOnPropertyChanged<Entities.Industry>
    {
        public byte Id
        {
            get
            {
                return GetField<byte>();
            }
            set
            {
                SetField(value);
            }
        }

        public string Name
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public string Description
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public ObservableCollection<byte> Professions
        {
            get
            {
                return GetField<ObservableCollection<byte>>();
            }
            set
            {
                SetField(value);
            }
        }

        public ObservableCollection<CompanyName> CompanyNames
        {
            get
            {
                return GetField<ObservableCollection<CompanyName>>();
            }
            set
            {
                SetField(value);
            }
        }

        public ObservableCollection<byte> SkillDefinitions
        {
            get
            {
                return GetField<ObservableCollection<byte>>();
            }
            set
            {
                SetField(value);
            }
        }
    }
}