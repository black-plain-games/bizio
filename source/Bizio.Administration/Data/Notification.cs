﻿using System;

namespace Bizio.Administration.Data
{
    public class Notification : EntityNotifyOnPropertyChanged<Entities.Notification>
    {
        public int Id
        {
            get
            {
                return GetField<int>();
            }
            set
            {
                SetField(value);
            }
        }

        public DateTimeOffset DateVisible
        {
            get
            {
                return GetField<DateTimeOffset>();
            }
            set
            {
                SetField(value);
            }
        }

        public string Subject
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public string Message
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }
    }
}