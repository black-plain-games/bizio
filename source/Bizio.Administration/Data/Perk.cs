﻿using System.Collections.ObjectModel;

namespace Bizio.Administration.Data
{
    public class Perk : EntityNotifyOnPropertyChanged<Entities.Perk>
    {
        public short Id
        {
            get
            {
                return GetField<short>();
            }
            set
            {
                SetField(value);
            }
        }

        public string Name
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public string Description
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public short? MaxCount
        {
            get
            {
                return GetField<short?>();
            }
            set
            {
                SetField(value);
            }
        }

        public decimal? InitialCost
        {
            get
            {
                return GetField<decimal?>();
            }
            set
            {
                SetField(value);
            }
        }

        public decimal? RecurringCost
        {
            get
            {
                return GetField<decimal?>();
            }
            set
            {
                SetField(value);
            }
        }

        public byte? RecurringCostInterval
        {
            get
            {
                return GetField<byte?>();
            }
            set
            {
                SetField(value);
            }
        }

        public ObservableCollection<PerkBenefit> Benefits
        {
            get
            {
                return GetField<ObservableCollection<PerkBenefit>>();
            }
            set
            {
                SetField(value);
            }
        }

        public ObservableCollection<PerkCondition> Conditions
        {
            get
            {
                return GetField<ObservableCollection<PerkCondition>>();
            }
            set
            {
                SetField(value);
            }
        }
    }
}