﻿namespace Bizio.Administration.Data
{
    public class PerkBenefit : EntityNotifyOnPropertyChanged<Entities.PerkBenefit>
    {
        public bool IsNew
        {
            get
            {
                return GetField<bool>();
            }
            set
            {
                SetField(value);
            }
        }

        public byte TargetTypeId
        {
            get
            {
                return GetField<byte>();
            }
            set
            {
                SetField(value);
            }
        }

        public byte AttributeId
        {
            get
            {
                return GetField<byte>();
            }
            set
            {
                SetField(value);
            }
        }

        public byte ValueTypeId
        {
            get
            {
                return GetField<byte>();
            }
            set
            {
                SetField(value);
            }
        }

        public decimal Value
        {
            get
            {
                return GetField<decimal>();
            }
            set
            {
                SetField(value);
            }
        }
    }
}