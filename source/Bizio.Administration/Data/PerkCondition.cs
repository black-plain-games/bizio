﻿namespace Bizio.Administration.Data
{
    public class PerkCondition : EntityNotifyOnPropertyChanged<Entities.PerkCondition>
    {
        public bool IsNew
        {
            get
            {
                return GetField<bool>();
            }
            set
            {
                SetField(value);
            }
        }

        public byte TargetTypeId
        {
            get
            {
                return GetField<byte>();
            }
            set
            {
                SetField(value);
            }
        }

        public byte AttributeId
        {
            get
            {
                return GetField<byte>();
            }
            set
            {
                SetField(value);
            }
        }

        public byte ComparisonId
        {
            get
            {
                return GetField<byte>();
            }
            set
            {
                SetField(value);
            }
        }

        public decimal Value
        {
            get
            {
                return GetField<decimal>();
            }
            set
            {
                SetField(value);
            }
        }
    }
}