﻿using System.Collections.ObjectModel;

namespace Bizio.Administration.Data
{
    public class Personality : EntityNotifyOnPropertyChanged<Entities.Personality>
    {
        public byte Id
        {
            get
            {
                return GetField<byte>();
            }
            set
            {
                SetField(value);
            }
        }

        public string Name
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public string Description
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public ObservableCollection<PersonalityAttribute> Attributes
        {
            get
            {
                return GetField<ObservableCollection<PersonalityAttribute>>();
            }
            set
            {
                SetField(value);
            }
        }

        public Personality()
        {
            Attributes = new ObservableCollection<PersonalityAttribute>();
        }
    }
}