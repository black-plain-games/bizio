﻿namespace Bizio.Administration.Data
{
    public class PersonalityAttribute : EntityNotifyOnPropertyChanged<Entities.PersonalityAttribute>
    {
        public byte AttributeId
        {
            get
            {
                return GetField<byte>();
            }
            set
            {
                SetField(value);
            }
        }

        public decimal Value
        {
            get
            {
                return GetField<decimal>();
            }
            set
            {
                SetField(value);
            }
        }
    }
}