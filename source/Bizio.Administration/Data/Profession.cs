﻿using System.Collections.ObjectModel;

namespace Bizio.Administration.Data
{
    public class Profession : EntityNotifyOnPropertyChanged<Entities.Profession>
    {
        public byte Id
        {
            get
            {
                return GetField<byte>();
            }
            set
            {
                SetField(value);
            }
        }

        public string Name
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public ObservableCollection<ProfessionSkillDefinition> SkillDefinitions
        {
            get
            {
                return GetField<ObservableCollection<ProfessionSkillDefinition>>();
            }
            set
            {
                SetField(value);
            }
        }
    }
}