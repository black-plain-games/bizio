﻿using System;

namespace Bizio.Administration.Data
{
    public class ProfessionSkillDefinition : EntityNotifyOnPropertyChanged<Entities.ProfessionSkillDefinition>
    {
        public byte ProfessionId
        {
            get
            {
                return GetField<byte>();
            }
            set
            {
                SetField(value);
            }
        }

        public byte SkillDefinitionId
        {
            get
            {
                return GetField<byte>();
            }
            set
            {
                SetField(value);
            }
        }

        public byte Weight
        {
            get
            {
                return GetField<byte>();
            }
            set
            {
                SetField(value);

                WeightChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        public event EventHandler WeightChanged;
    }
}