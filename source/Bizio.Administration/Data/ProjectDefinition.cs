﻿using System.Collections.ObjectModel;

namespace Bizio.Administration.Data
{
    public class ProjectDefinition : EntityNotifyOnPropertyChanged<Entities.ProjectDefinition>
    {
        public short Id
        {
            get
            {
                return GetField<short>();
            }
            set
            {
                SetField(value);
            }
        }

        public string Name
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public string Description
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public Range<decimal> Value
        {
            get
            {
                return GetField<Range<decimal>>();
            }
            set
            {
                SetField(value);
            }
        }

        public Range<int> ProjectLength
        {
            get
            {
                return GetField<Range<int>>();
            }
            set
            {
                SetField(value);
            }
        }

        public ObservableCollection<byte> Industries
        {
            get
            {
                return GetField<ObservableCollection<byte>>();
            }
            set
            {
                SetField(value);
            }
        }

        public ObservableCollection<ProjectDefinitionSkill> Skills
        {
            get
            {
                return GetField<ObservableCollection<ProjectDefinitionSkill>>();
            }
            set
            {
                SetField(value);
            }
        }
    }
}