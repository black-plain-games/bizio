﻿namespace Bizio.Administration.Data
{
    public class ProjectDefinitionSkill : EntityNotifyOnPropertyChanged<Entities.ProjectDefinitionSkill>
    {
        public byte SkillDefinitionId
        {
            get
            {
                return GetField<byte>();
            }
            set
            {
                SetField(value);
            }
        }

        public Range<decimal> Value
        {
            get
            {
                return GetField<Range<decimal>>();
            }
            set
            {
                SetField(value);
            }
        }
    }
}