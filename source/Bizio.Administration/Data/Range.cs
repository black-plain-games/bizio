﻿using BlackPlain.Wpf.Core;

namespace Bizio.Administration.Data
{
    public class Range<T> : NotifyOnPropertyChanged
    {
        public T Minimum
        {
            get
            {
                return GetField<T>();
            }
            set
            {
                SetField(value);
            }
        }

        public T Maximum
        {
            get
            {
                return GetField<T>();
            }
            set
            {
                SetField(value);
            }
        }
    }
}