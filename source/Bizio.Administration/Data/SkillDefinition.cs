﻿namespace Bizio.Administration.Data
{
    public class SkillDefinition : EntityNotifyOnPropertyChanged<Entities.SkillDefinition>
    {
        public byte Id
        {
            get
            {
                return GetField<byte>();
            }
            set
            {
                SetField(value);
            }
        }

        public string Name
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public Range<decimal> Value
        {
            get
            {
                return GetField<Range<decimal>>();
            }
            set
            {
                SetField(value);
            }
        }
    }
}