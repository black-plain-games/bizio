﻿using BlackPlain.Wpf.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Bizio.Administration.Data
{
    public static class StaticData
    {
        public static Entities.BizioEntities Entity { get; }

        public static ObservableCollection<Profession> Professions { get; private set; }

        public static ObservableCollection<SkillDefinition> SkillDefinitions { get; private set; }

        public static ObservableCollection<Attribute> Attributes { get; private set; }

        public static ObservableCollection<Personality> Personalities { get; private set; }

        public static ObservableCollection<GlobalConfiguration> GlobalConfigurations { get; private set; }

        public static ObservableCollection<GameConfiguration> GameConfigurations { get; private set; }

        public static ObservableCollection<ConfigurationKey> ConfigurationKeys { get; private set; }

        public static ObservableCollection<Industry> Industries { get; private set; }

        public static ObservableCollection<CompanyName> CompanyNames { get; private set; }

        public static ObservableCollection<Game> Games { get; private set; }

        public static ObservableCollection<ProjectDefinition> ProjectDefinitions { get; private set; }

        public static ObservableCollection<Notification> Notifications { get; private set; }

        public static ObservableCollection<Action> Actions { get; private set; }

        public static ObservableCollection<Perk> Perks { get; private set; }

        public static ObservableCollection<IdNamePair<byte, string, Entities.PerkTargetType>> PerkTargetTypes { get; private set; }

        public static ObservableCollection<IdNamePair<byte, string, Entities.PerkValueType>> PerkValueTypes { get; private set; }

        public static ObservableCollection<IdNamePair<byte, string, Entities.PerkBenefitAttribute>> PerkBenefitAttributes { get; private set; }

        public static ObservableCollection<IdNamePair<byte, string, Entities.PerkConditionAttribute>> PerkConditionAttributes { get; private set; }

        public static ObservableCollection<IdNamePair<byte, string, Entities.PerkConditionComparison>> PerkConditionComparisons { get; private set; }

        static StaticData()
        {
            Professions = new ObservableCollection<Profession>();

            SkillDefinitions = new ObservableCollection<SkillDefinition>();

            Attributes = new ObservableCollection<Attribute>();

            Personalities = new ObservableCollection<Personality>();

            GlobalConfigurations = new ObservableCollection<GlobalConfiguration>();

            GameConfigurations = new ObservableCollection<GameConfiguration>();

            ConfigurationKeys = new ObservableCollection<ConfigurationKey>();

            Industries = new ObservableCollection<Industry>();

            CompanyNames = new ObservableCollection<CompanyName>();

            Games = new ObservableCollection<Game>();

            ProjectDefinitions = new ObservableCollection<ProjectDefinition>();

            Notifications = new ObservableCollection<Notification>();

            Actions = new ObservableCollection<Action>();

            Perks = new ObservableCollection<Perk>();

            PerkTargetTypes = new ObservableCollection<IdNamePair<byte, string, Entities.PerkTargetType>>();

            PerkValueTypes = new ObservableCollection<IdNamePair<byte, string, Entities.PerkValueType>>();

            PerkBenefitAttributes = new ObservableCollection<IdNamePair<byte, string, Entities.PerkBenefitAttribute>>();

            PerkConditionAttributes = new ObservableCollection<IdNamePair<byte, string, Entities.PerkConditionAttribute>>();

            PerkConditionComparisons = new ObservableCollection<IdNamePair<byte, string, Entities.PerkConditionComparison>>();

            Entity = new Entities.BizioEntities();
        }

        public static void Load(params EntityType[] entityTypes)
        {
            UIManager.TryInvokeOnUIThread(() => LoadInternal(entityTypes));
        }

        private static void LoadInternal(params EntityType[] entityTypes)
        {
            Entity.LoadEntities(entityTypes, EntityType.Professions,
                () => Professions,
                x => x.Professions,
                x => new Profession
                {
                    Entity = x,
                    Id = x.Id,
                    Name = x.Name,
                    SkillDefinitions = x.ProfessionSkillDefinitions.ToCollection(psd => new ProfessionSkillDefinition
                    {
                        Entity = psd,
                        SkillDefinitionId = psd.SkillDefinitionId,
                        Weight = psd.Weight
                    })
                });

            Entity.LoadEntities(entityTypes, EntityType.SkillDefinitions,
                () => SkillDefinitions,
                x => x.SkillDefinitions,
                x => new SkillDefinition
                {
                    Entity = x,
                    Id = x.Id,
                    Name = x.Name,
                    Value = new Range<decimal>
                    {
                        Minimum = x.Minimum,
                        Maximum = x.Maximum
                    }
                });

            Entity.LoadEntities(entityTypes, EntityType.Attributes,
                () => Attributes,
                x => x.Attributes,
                x => new Attribute
                {
                    Entity = x,
                    Id = x.Id,
                    Name = x.Name
                });

            Entity.LoadEntities(entityTypes, EntityType.Personalities,
                () => Personalities,
                x => x.Personalities,
                x => new Personality
                {
                    Entity = x,
                    Id = x.Id,
                    Description = x.Description,
                    Name = x.Name,
                    Attributes = x.PersonalityAttributes.ToCollection(pa => new PersonalityAttribute
                    {
                        Entity = pa,
                        AttributeId = pa.AttributeId,
                        Value = pa.Value
                    })
                });

            Entity.LoadEntities(entityTypes, EntityType.GlobalConfigurations,
                () => GlobalConfigurations,
                x => x.GlobalConfigurations,
                x => new GlobalConfiguration
                {
                    Entity = x,
                    ConfigurationKeyId = x.ConfigurationKeyId,
                    Value = x.Value
                });

            Entity.LoadEntities(entityTypes, EntityType.GameConfigurations,
                () => GameConfigurations,
                x => x.GameConfigurations,
                x => new GameConfiguration
                {
                    Entity = x,
                    GameId = x.GameId,
                    ConfigurationKeyId = x.ConfigurationKeyId,
                    Value = x.Value
                });

            Entity.LoadEntities(entityTypes, EntityType.ConfigurationKeys,
                () => ConfigurationKeys,
                x => x.ConfigurationKeys,
                x => new ConfigurationKey
                {
                    Entity = x,
                    Id = x.Id,
                    Name = x.Name
                });

            Entity.LoadEntities(entityTypes, EntityType.Industries,
                () => Industries,
                x => x.Industries,
                x => new Industry
                {
                    Entity = x,
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description,
                    Professions = x.Professions.ToCollection(p => p.Id),
                    CompanyNames = x.CompanyNames.ToCollection(cn => new CompanyName
                    {
                        Entity = cn,
                        IndustryId = cn.IndustryId,
                        Name = cn.Name
                    }),
                    SkillDefinitions = x.SkillDefinitions.ToCollection(sd => sd.Id)
                });

            Entity.LoadEntities(entityTypes, EntityType.Games,
                () => Games,
                x => x.Games,
                x => new Game
                {
                    Entity = x,
                    Id = x.Id,
                    CreatedDate = x.CreatedDate
                });

            Entity.LoadEntities(entityTypes, EntityType.ProjectDefinitions,
                () => ProjectDefinitions,
                x => x.ProjectDefinitions,
                x => new ProjectDefinition
                {
                    Id = x.Id,
                    Description = x.Description,
                    Name = x.Name,
                    ProjectLength = new Range<int>
                    {
                        Minimum = x.MinimumProjectLength,
                        Maximum = x.MaximumProjectLength
                    },
                    Value = new Range<decimal>
                    {
                        Minimum = x.MinimumValue,
                        Maximum = x.MaximumValue
                    },
                    Industries = x.Industries.ToCollection(i => i.Id),
                    Skills = x.ProjectDefinitionSkills.ToCollection(pds => new ProjectDefinitionSkill
                    {
                        Entity = pds,
                        SkillDefinitionId = pds.SkillDefinitionId,
                        Value = new Range<decimal>
                        {
                            Minimum = pds.Minimum,
                            Maximum = pds.Maximum
                        }
                    })
                });

            Entity.LoadEntities(entityTypes, EntityType.Notifications,
                () => Notifications,
                x => x.Notifications,
                x => new Notification
                {
                    Entity = x,
                    Id = x.Id,
                    DateVisible = x.DateVisible,
                    Subject = x.Subject,
                    Message = x.Message
                });

            Entity.LoadEntities(entityTypes, EntityType.Actions,
                () => Actions,
                x => x.Actions,
                x => new Action
                {
                    Entity = x,
                    Id = x.Id,
                    Name = x.Name,
                    MaxCount = x.MaxCount,
                    DefaultCount = x.DefaultCount,
                    Requirements = x.ActionRequirements.ToCollection(ar => new ActionRequirement
                    {
                        Entity = ar,
                        SkillDefinitionId = ar.SkillDefinitionId,
                        Value = ar.Value
                    })
                });

            Entity.LoadEntities(entityTypes, EntityType.Perks,
                () => Perks,
                x => x.Perks,
                x => new Perk
                {
                    Entity = x,
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description,
                    InitialCost = x.InitialCost,
                    RecurringCost = x.RecurringCost,
                    RecurringCostInterval = x.RecurringCostInterval,
                    MaxCount = x.MaxCount,
                    Benefits = x.PerkBenefits.ToCollection(pb => new PerkBenefit
                    {
                        Entity = pb,
                        AttributeId = pb.AttributeId,
                        TargetTypeId = pb.TargetTypeId,
                        Value = pb.Value,
                        ValueTypeId = pb.ValueTypeId,
                        IsNew = false
                    }),
                    Conditions = x.PerkConditions.ToCollection(pc => new PerkCondition
                    {
                        Entity = pc,
                        AttributeId = pc.AttributeId,
                        ComparisonId = pc.ComparisonId,
                        TargetTypeId = pc.TargetTypeId,
                        Value = pc.Value,
                        IsNew = false
                    })
                });

            Entity.LoadEntities(entityTypes, EntityType.PerkTargetTypes,
                () => PerkTargetTypes,
                x => x.PerkTargetTypes,
                x => new IdNamePair<byte, string, Entities.PerkTargetType>
                {
                    Entity = x,
                    Id = x.Id,
                    Name = x.Name
                });

            Entity.LoadEntities(entityTypes, EntityType.PerkValueTypes,
                () => PerkValueTypes,
                x => x.PerkValueTypes,
                x => new IdNamePair<byte, string, Entities.PerkValueType>
                {
                    Entity = x,
                    Id = x.Id,
                    Name = x.Name
                });

            Entity.LoadEntities(entityTypes, EntityType.PerkBenefitAttributes,
                () => PerkBenefitAttributes,
                x => x.PerkBenefitAttributes,
                x => new IdNamePair<byte, string, Entities.PerkBenefitAttribute>
                {
                    Entity = x,
                    Id = x.Id,
                    Name = x.Name
                });

            Entity.LoadEntities(entityTypes, EntityType.PerkConditionAttributes,
                () => PerkConditionAttributes,
                x => x.PerkConditionAttributes,
                x => new IdNamePair<byte, string, Entities.PerkConditionAttribute>
                {
                    Entity = x,
                    Id = x.Id,
                    Name = x.Name
                });

            Entity.LoadEntities(entityTypes, EntityType.PerkConditionComparisons,
                () => PerkConditionComparisons,
                x => x.PerkConditionComparisons,
                x => new IdNamePair<byte, string, Entities.PerkConditionComparison>
                {
                    Entity = x,
                    Id = x.Id,
                    Name = x.Name
                });
        }

        private static ObservableCollection<U> ToCollection<T, U>(this IEnumerable<T> collection, Func<T, U> transformer)
        {
            return new ObservableCollection<U>(collection.Select(transformer));
        }

        private static void LoadEntities<TModel, TEntity>(this Entities.BizioEntities entity, EntityType[] entityTypes, EntityType entityType, Func<ObservableCollection<TModel>> getter, Func<Entities.BizioEntities, IEnumerable<TEntity>> dbGetter, Func<TEntity, TModel> converter, params EntityType[] dependencies)
            where TModel : IEntity<TEntity>
            where TEntity : class
        {
            if (dependencies.Except(entityTypes).Any())
            {
                LoadInternal(dependencies);
            }

            if (entityTypes.Contains(entityType))
            {
                var collection = getter();

                collection.Clear();

                foreach (var dbValue in dbGetter(entity))
                {
                    var model = converter(dbValue);

                    model.Entity = dbValue;

                    collection.Add(model);
                }
            }
        }
    }
}