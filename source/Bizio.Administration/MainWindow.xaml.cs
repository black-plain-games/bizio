﻿using Bizio.Administration.Data;
using BlackPlain.Wpf.Core;

namespace Bizio.Administration
{
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();

            UIManager.SetUI();
        }

        ~MainWindow()
        {
            StaticData.Entity.Dispose();
        }
    }
}