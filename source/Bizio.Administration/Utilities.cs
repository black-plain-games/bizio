﻿using Bizio.Administration.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Bizio.Administration
{
    public struct CrudCollections<TModel, TEntity>
    {
        public IEnumerable<TModel> Additions;

        public IDictionary<TModel, TEntity> Updates;

        public IEnumerable<TEntity> Deletions;
    }

    public static class Utilities
    {
        public static CrudCollections<TModel, TEntity> GetChanges<TModel, TEntity>(this IEnumerable<TEntity> entities, IEnumerable<TModel> models, Func<TModel, TEntity, bool> comparer)
        {
            var additions = models.Where(m => !entities.Any(e => comparer(m, e))).ToList();

            var deletions = entities.Where(e => !models.Any(m => comparer(m, e))).ToList();

            var existingModels = models.Except(additions).ToList();

            var remainingEntities = entities.Except(deletions).ToList();

            var updates = remainingEntities.ToDictionary(e => existingModels.First(m => comparer(m, e)));

            return new CrudCollections<TModel, TEntity>
            {
                Additions = additions,
                Deletions = deletions,
                Updates = updates
            };
        }

        public static IdNamePair<byte, string, T> Add<T>(this DbSet<T> entities)
            where T : class, new()
        {
            dynamic output = new T();

            output.Name = "noname";

            entities.Add(output);

            StaticData.Entity.SaveChanges();

            return new IdNamePair<byte, string, T>
            {
                Entity = output,
                Id = output.Id,
                Name = output.Name
            };
        }

        private static string FormatValue(object value)
        {
            if (value == null)
            {
                return "NULL";
            }

            if (value is string)
            {
                var escapedValue = $"{value}".Replace("'", "''");

                return $"'{escapedValue}'";
            }

            if (value is DateTime || value is DateTime? ||
                value is DateTimeOffset || value is DateTimeOffset?)
            {
                return $"{value:O}";
            }

            return $"{value}";
        }

        private static string GetIdentityInsertClause(bool useIdentityInsertClause, string tableName, bool doTurnOn)
        {
            if (!useIdentityInsertClause)
            {
                return string.Empty;
            }

            return $"\r\n\r\nSET IDENTITY_INSERT [dbo].[{tableName}] " + (doTurnOn ? "ON" : "OFF");
        }

        private static void SaveSeedFile(string tableName, bool useIdentityInsertClause, IEnumerable<IEnumerable<object>> records, IEnumerable<string> allColumns, IEnumerable<string> primaryKeys, IEnumerable<string> updateColumns)
        {
            var formattedAllColumns = string.Join(",", allColumns.Select(c => $"[{c}]"));

            var formattedMatchColumns = string.Join(" AND ", primaryKeys.Select(c => $"TARGET.[{c}] = SOURCE.[{c}]"));

            var formattedValues = string.Join(",\r\n        ", records.Select(r => $"({string.Join(", ", r.Select(FormatValue))})"));

            var updateClause = string.Empty;

            if (updateColumns.Any())
            {
                var formattedUpdateColumns = string.Join(",\r\n        ", updateColumns.Select(c => $"[{c}] = SOURCE.[{c}]"));

                updateClause = $@"
WHEN MATCHED THEN
    UPDATE SET
        {formattedUpdateColumns}";
            }



            var seed = $@"PRINT '{tableName}'{GetIdentityInsertClause(useIdentityInsertClause, tableName, true)}

MERGE INTO [dbo].[{tableName}] AS TARGET
    USING ( VALUES
        {formattedValues}
    ) AS SOURCE ({formattedAllColumns})
ON {formattedMatchColumns}{updateClause}
WHEN NOT MATCHED BY TARGET THEN
    INSERT ({formattedAllColumns})
    VALUES ({formattedAllColumns});{GetIdentityInsertClause(useIdentityInsertClause, tableName, false)}";

            using (var writer = new StreamWriter($"../../../Bizio.Database/scripts/seed/{tableName}.sql"))
            {
                writer.Write(seed);
            }
        }

        public static void SaveSeedFile<TEntity>(this IEnumerable<TEntity> entities, string tableName, bool useIdentityInsertClause, params string[] primaryKeys)
        {
            var type = typeof(TEntity);

            var properties = type.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.GetProperty);

            var primitiveProperties = properties
                .Where(p => p.PropertyType.IsPrimitive || p.PropertyType == typeof(string) || p.PropertyType == typeof(decimal) || p.PropertyType == typeof(decimal?))
                .OrderByDescending(pi => primaryKeys.Contains(pi.Name));

            var allColumns = primitiveProperties.Select(p => p.Name).ToList();

            var records = new List<IEnumerable<object>>();

            foreach (var entity in entities)
            {
                var record = primitiveProperties.Select(p => p.GetValue(entity)).ToList();

                records.Add(record);
            }

            SaveSeedFile(tableName, useIdentityInsertClause, records, allColumns, primaryKeys, allColumns.Except(primaryKeys));
        }

        public static void SaveSeedFile<TEntity>(this IEnumerable<TEntity> entities, string tableName, params string[] primaryKeys) => SaveSeedFile(entities, tableName, false, primaryKeys);

        public static void SaveSeedFile<TEntity>(this IEnumerable<TEntity> entities, string tableName) => SaveSeedFile(entities, tableName, true, "Id");
    }
}