﻿using Bizio.Administration.Data;
using BlackPlain.Wpf.Core;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace Bizio.Administration.ViewModels
{
    public class ActionsViewModel : ViewModelBase
    {
        public ObservableCollection<Action> Actions
        {
            get
            {
                return GetField<ObservableCollection<Action>>();
            }
            set
            {
                SetField(value);
            }
        }

        public Action SelectedAction
        {
            get
            {
                return GetField<Action>();
            }
            set
            {
                SetField(value);
            }
        }

        public ActionRequirement SelectedActionRequirement
        {
            get
            {
                return GetField<ActionRequirement>();
            }
            set
            {
                SetField(value);
            }
        }

        public ObservableCollection<SkillDefinition> SkillDefinitions
        {
            get
            {
                return GetField<ObservableCollection<SkillDefinition>>();
            }
            set
            {
                SetField(value);
            }
        }

        public SkillDefinition SelectedSkillDefinition
        {
            get
            {
                return GetField<SkillDefinition>();
            }
            set
            {
                SetField(value);
            }
        }

        public ICommand AddActionRequirementCommand => GetCommand(AddActionRequirement, CanAddActionRequirement);

        public ICommand ResetActionCommand => GetCommand(ResetAction, CanResetAction);

        public ICommand DeleteActionRequirementCommand => GetCommand(DeleteActionRequirement, CanDeleteActionRequirement);

        public ICommand SaveChangesCommand => GetCommand(SaveChanges);

        public ICommand ClearChangesCommand => GetCommand(ClearChanges);

        protected override void LoadViewModel(object state)
        {
            StaticData.Load(EntityType.Actions, EntityType.SkillDefinitions);

            Actions = StaticData.Actions;

            SkillDefinitions = StaticData.SkillDefinitions;
        }

        private bool CanAddActionRequirement() => SelectedAction != null && SelectedSkillDefinition != null;

        private bool CanResetAction() => SelectedAction != null;

        private bool CanDeleteActionRequirement() => SelectedActionRequirement != null;

        private void AddActionRequirement()
        {
            var requirement = new ActionRequirement
            {
                SkillDefinitionId = SelectedSkillDefinition.Id,
                Value = SelectedSkillDefinition.Value.Minimum
            };

            requirement.Entity = new Entities.ActionRequirement
            {
                ActionId = SelectedAction.Id,
                SkillDefinitionId = requirement.SkillDefinitionId,
                Value = requirement.Value
            };

            StaticData.Entity.ActionRequirements.Add(requirement.Entity);

            StaticData.Entity.SaveChanges();

            SelectedAction.Requirements.Add(requirement);

            SelectedActionRequirement = requirement;
        }

        private void ResetAction() => ResetAction(SelectedAction);

        private void ResetAction(Action action)
        {
            action.Name = action.Entity.Name;

            action.MaxCount = action.Entity.MaxCount;

            action.DefaultCount = action.Entity.DefaultCount;

            foreach (var requirement in action.Requirements)
            {
                requirement.Value = requirement.Entity.Value;
            }
        }

        private void DeleteActionRequirement()
        {
            StaticData.Entity.ActionRequirements.Remove(SelectedActionRequirement.Entity);

            StaticData.Entity.SaveChanges();

            SelectedAction.Requirements.Remove(SelectedActionRequirement);
        }

        private void SaveChanges()
        {
            foreach (var action in Actions)
            {
                action.Entity.Name = action.Name;

                action.Entity.MaxCount = action.MaxCount;

                action.Entity.DefaultCount = action.DefaultCount;

                foreach (var requirement in action.Requirements)
                {
                    requirement.Entity.Value = requirement.Value;
                }
            }

            StaticData.Entity.SaveChanges();
        }

        private void ClearChanges()
        {
            foreach (var action in Actions)
            {
                ResetAction(action);
            }
        }
    }
}