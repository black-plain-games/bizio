﻿using Bizio.Administration.Data;
using BlackPlain.Wpf.Core;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace Bizio.Administration.ViewModels
{
    public class ConfigurationViewModel : ViewModelBase
    {
        public ObservableCollection<ConfigurationKey> ConfigurationKeys
        {
            get
            {
                return GetField<ObservableCollection<ConfigurationKey>>();
            }
            set
            {
                SetField(value);
            }
        }

        public ObservableCollection<GlobalConfiguration> GlobalConfigurations
        {
            get
            {
                return GetField<ObservableCollection<GlobalConfiguration>>();
            }
            set
            {
                SetField(value);
            }
        }

        public ConfigurationKey SelectedConfigurationKey
        {
            get
            {
                return GetField<ConfigurationKey>();
            }
            set
            {
                SetField(value);

                OnPropertyChanged(nameof(SelectedGlobalConfiguration));

                OnPropertyChanged(nameof(SelectedGameConfiguration));
            }
        }

        public GlobalConfiguration SelectedGlobalConfiguration => GlobalConfigurations?.FirstOrDefault(gc => gc.ConfigurationKeyId == SelectedConfigurationKey?.Id);

        public ObservableCollection<Game> Games
        {
            get
            {
                return GetField<ObservableCollection<Game>>();
            }
            set
            {
                SetField(value);
            }
        }

        public Game SelectedGame
        {
            get
            {
                return GetField<Game>();
            }
            set
            {
                SetField(value);

                OnPropertyChanged(nameof(SelectedGameConfigurations));
            }
        }

        public ObservableCollection<GameConfiguration> GameConfigurations
        {
            get
            {
                return GetField<ObservableCollection<GameConfiguration>>();
            }
            set
            {
                SetField(value);
            }
        }

        public IEnumerable<GameConfiguration> SelectedGameConfigurations => GameConfigurations?.Where(gc => gc.GameId == SelectedGame?.Id);

        public GameConfiguration SelectedGameConfiguration => SelectedGameConfigurations?.FirstOrDefault(gc => gc.ConfigurationKeyId == SelectedConfigurationKey?.Id);

        public ICommand AddNewConfigurationKeyCommand => GetCommand(AddNewConfigurationKey);

        public ICommand DeleteSelectedConfigurationKeyCommand => GetCommand(DeleteSelectedConfigurationKey, CanDeleteConfigurationKey);

        public ICommand AddNewGameConfigurationCommand => GetCommand(AddNewGameConfiguration, CanAddNewGameConfiguration);

        public ICommand DeleteSelectedGameConfigurationCommand => GetCommand(DeleteSelectedGameConfiguration, CanDeleteSelectedGameConfiguration);

        public ICommand SaveChangesCommand => GetCommand(SaveChanges);

        protected override void LoadViewModel(object state)
        {
            StaticData.Load(EntityType.ConfigurationKeys, EntityType.GameConfigurations, EntityType.GlobalConfigurations, EntityType.Games);

            ConfigurationKeys = StaticData.ConfigurationKeys;

            GlobalConfigurations = StaticData.GlobalConfigurations;

            Games = StaticData.Games;

            GameConfigurations = StaticData.GameConfigurations;

            SelectedConfigurationKey = ConfigurationKeys.FirstOrDefault();

            SelectedGame = Games.FirstOrDefault();
        }

        private void SaveChanges()
        {
            foreach (var configurationKey in ConfigurationKeys)
            {
                configurationKey.Entity.Name = configurationKey.Name;
            }

            foreach (var globalConfiguration in GlobalConfigurations)
            {
                globalConfiguration.Entity.Value = globalConfiguration.Value;
            }

            foreach (var gameConfiguration in GameConfigurations)
            {
                gameConfiguration.Entity.Value = gameConfiguration.Value;
            }

            StaticData.Entity.SaveChanges();
        }

        private bool CanAddNewGameConfiguration()
        {
            return SelectedConfigurationKey != null && SelectedGame != null && SelectedGameConfiguration == null;
        }

        private bool CanDeleteSelectedGameConfiguration()
        {
            return SelectedGameConfiguration != null;
        }

        private bool CanDeleteConfigurationKey()
        {
            return SelectedConfigurationKey != null;
        }

        private void AddNewConfigurationKey()
        {
            var key = new ConfigurationKey
            {
                Name = "New Configuration Key"
            };

            ConfigurationKeys.Add(key);

            var config = new GlobalConfiguration
            {
                ConfigurationKeyId = key.Id,
                Value = string.Empty
            };

            GlobalConfigurations.Add(config);

            key.Entity = new Entities.ConfigurationKey
            {
                Name = key.Name
            };

            StaticData.Entity.ConfigurationKeys.Add(key.Entity);

            StaticData.Entity.SaveChanges();

            config.ConfigurationKeyId = key.Id = key.Entity.Id;

            config.Entity = new Entities.GlobalConfiguration
            {
                ConfigurationKeyId = key.Entity.Id,
                Value = config.Value
            };

            StaticData.Entity.GlobalConfigurations.Add(config.Entity);

            StaticData.Entity.SaveChanges();

            SelectedConfigurationKey = key;
        }

        private void DeleteSelectedConfigurationKey()
        {
            var globalConfig = GlobalConfigurations.First(gc => gc.ConfigurationKeyId == SelectedConfigurationKey.Id);

            var gameConfigs = GameConfigurations.Where(gc => gc.ConfigurationKeyId == SelectedConfigurationKey.Id);

            GlobalConfigurations.Remove(globalConfig);

            foreach (var gameConfig in gameConfigs)
            {
                GameConfigurations.Remove(gameConfig);
            }

            var dbKey = StaticData.Entity.ConfigurationKeys.First(ck => ck.Id == SelectedConfigurationKey.Id);

            var dbGlobal = StaticData.Entity.GlobalConfigurations.First(gc => gc.ConfigurationKeyId == dbKey.Id);

            StaticData.Entity.GlobalConfigurations.Remove(dbGlobal);

            StaticData.Entity.GameConfigurations.RemoveRange(dbKey.GameConfigurations);

            StaticData.Entity.ConfigurationKeys.Remove(dbKey);

            StaticData.Entity.SaveChanges();

            ConfigurationKeys.Remove(SelectedConfigurationKey);

            SelectedConfigurationKey = ConfigurationKeys.FirstOrDefault();
        }

        private void AddNewGameConfiguration()
        {
            var gameConfiguration = new GameConfiguration
            {
                ConfigurationKeyId = SelectedConfigurationKey.Id,
                GameId = SelectedGame.Id,
                Value = SelectedGlobalConfiguration.Value
            };

            StaticData.Entity.GameConfigurations.Add(gameConfiguration.Entity = new Entities.GameConfiguration
            {
                ConfigurationKeyId = gameConfiguration.ConfigurationKeyId,
                GameId = gameConfiguration.GameId,
                Value = gameConfiguration.Value
            });

            StaticData.Entity.SaveChanges();

            GameConfigurations.Add(gameConfiguration);

            OnPropertyChanged(nameof(SelectedGameConfiguration));
        }

        private void DeleteSelectedGameConfiguration()
        {
            StaticData.Entity.GameConfigurations.Remove(SelectedGameConfiguration.Entity);

            StaticData.Entity.SaveChanges();

            GameConfigurations.Remove(SelectedGameConfiguration);

            OnPropertyChanged(nameof(SelectedGameConfiguration));
        }
    }
}