﻿using Bizio.Administration.Data;
using BlackPlain.Wpf.Core;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace Bizio.Administration.ViewModels
{
    public class IndustriesViewModel : ViewModelBase
    {
        public ObservableCollection<Industry> Industries
        {
            get
            {
                return GetField<ObservableCollection<Industry>>();
            }
            set
            {
                SetField(value);
            }
        }

        public ObservableCollection<Profession> Professions
        {
            get
            {
                return GetField<ObservableCollection<Profession>>();
            }
            set
            {
                SetField(value);
            }
        }

        public Industry SelectedIndustry
        {
            get
            {
                return GetField<Industry>();
            }
            set
            {
                SetField(value);
            }
        }

        public Profession SelectedProfession
        {
            get
            {
                return GetField<Profession>();
            }
            set
            {
                SetField(value);
            }
        }

        public byte? SelectedIndustryProfession
        {
            get
            {
                return GetField<byte?>();
            }
            set
            {
                SetField(value);
            }
        }

        public CompanyName SelectedCompanyName
        {
            get
            {
                return GetField<CompanyName>();
            }
            set
            {
                SetField(value);
            }
        }

        public string NewCompanyName
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public ObservableCollection<SkillDefinition> SkillDefinitions
        {
            get
            {
                return GetField<ObservableCollection<SkillDefinition>>();
            }
            set
            {
                SetField(value);
            }
        }

        public SkillDefinition SelectedSkillDefinition
        {
            get
            {
                return GetField<SkillDefinition>();
            }
            set
            {
                SetField(value);
            }
        }

        public byte? SelectedMandatorySkillDefinition
        {
            get
            {
                return GetField<byte?>();
            }
            set
            {
                SetField(value);
            }
        }

        public ICommand AddNewIndustryCommand => GetCommand(AddNewIndustry);

        public ICommand DeleteSelectedIndustryCommand => GetCommand(DeleteSelectedIndustry, IsIndustrySelected);

        public ICommand SaveChangesCommand => GetCommand(SaveChanges);

        public ICommand AddNewCompanyNameCommand => GetCommand(AddNewCompanyName, CanAddCompanyName);

        public ICommand DeleteCompanyNameCommand => GetCommand(DeleteSelectedCompanyName, IsCompanyNameSelected);

        public ICommand AddProfessionToIndustryCommand => GetCommand(AddProfessionToIndustry, CanAddProfessionToIndustry);

        public ICommand RemoveProfessionFromIndustryCommand => GetCommand(RemoveProfessionFromIndustry, CanRemoveProfessionFromIndustry);

        public ICommand AddSkillDefinitionCommand => GetCommand(AddSkillDefinition, CanAddSkillDefinition);

        public ICommand RemoveSkillDefinitionCommand => GetCommand(RemoveSkillDefinition, CanRemoveSkillDefinition);

        protected override void LoadViewModel(object state)
        {
            StaticData.Load(EntityType.Industries, EntityType.Professions, EntityType.SkillDefinitions);

            Industries = StaticData.Industries;

            Professions = StaticData.Professions;

            SkillDefinitions = StaticData.SkillDefinitions;
        }

        private bool IsIndustrySelected() => SelectedIndustry != null;

        private bool IsCompanyNameSelected() => SelectedCompanyName != null;

        private bool CanAddProfessionToIndustry() => !SelectedIndustry?.Professions.Any(p => p == SelectedProfession?.Id) ?? false;

        private bool CanAddCompanyName() => SelectedIndustry != null && !string.IsNullOrWhiteSpace(NewCompanyName);

        private bool CanRemoveProfessionFromIndustry() => SelectedIndustry != null && SelectedIndustryProfession.HasValue;

        private bool CanAddSkillDefinition(object obj) => SelectedIndustry != null && SelectedSkillDefinition != null;

        private bool CanRemoveSkillDefinition() => SelectedIndustry != null && SelectedMandatorySkillDefinition.HasValue;

        private void AddNewIndustry()
        {
            var industry = new Industry
            {
                Name = "New Industry",
                Description = "New industry description goes here",
                Professions = new ObservableCollection<byte>(),
                CompanyNames = new ObservableCollection<CompanyName>()
            };

            industry.Entity = new Entities.Industry
            {
                Name = industry.Name,
                Description = industry.Description
            };

            StaticData.Entity.Industries.Add(industry.Entity);

            StaticData.Entity.SaveChanges();

            industry.Id = industry.Entity.Id;

            Industries.Add(industry);

            SelectedIndustry = industry;
        }

        private void DeleteSelectedIndustry()
        {
            StaticData.Entity.Industries.Remove(SelectedIndustry.Entity);

            Industries.Remove(SelectedIndustry);

            SelectedIndustry = Industries.FirstOrDefault();

            StaticData.Entity.SaveChanges();
        }

        private void AddNewCompanyName()
        {
            var companyName = new CompanyName
            {
                IndustryId = SelectedIndustry.Id,
                Name = NewCompanyName.Trim()
            };

            SelectedIndustry.CompanyNames.Add(companyName);

            companyName.Entity = new Entities.CompanyName
            {
                IndustryId = companyName.IndustryId,
                Name = companyName.Name
            };

            StaticData.Entity.CompanyNames.Add(companyName.Entity);

            StaticData.Entity.SaveChanges();

            SelectedCompanyName = companyName;

            NewCompanyName = null;
        }

        private void DeleteSelectedCompanyName()
        {
            StaticData.Entity.CompanyNames.Remove(SelectedCompanyName.Entity);

            StaticData.Entity.SaveChanges();

            SelectedIndustry.CompanyNames.Remove(SelectedCompanyName);

            SelectedCompanyName = SelectedIndustry.CompanyNames.FirstOrDefault();
        }

        private void AddProfessionToIndustry()
        {
            SelectedIndustry.Entity.Professions.Add(SelectedProfession.Entity);

            StaticData.Entity.SaveChanges();

            SelectedIndustry.Professions.Add(SelectedProfession.Id);
        }

        private void RemoveProfessionFromIndustry()
        {
            var dbIndustryProfession = SelectedIndustry.Entity.Professions.First(p => p.Id == SelectedIndustryProfession.Value);

            SelectedIndustry.Entity.Professions.Remove(dbIndustryProfession);

            StaticData.Entity.SaveChanges();

            SelectedIndustry.Professions.Remove(SelectedIndustryProfession.Value);
        }

        private void AddSkillDefinition()
        {
            SelectedIndustry.SkillDefinitions.Add(SelectedSkillDefinition.Id);

            SelectedIndustry.Entity.SkillDefinitions.Add(SelectedSkillDefinition.Entity);

            StaticData.Entity.SaveChanges();
        }

        private void RemoveSkillDefinition()
        {
            var skillDefinition = SkillDefinitions.FirstOrDefault(sd => sd.Id == SelectedMandatorySkillDefinition.Value);

            SelectedIndustry.Entity.SkillDefinitions.Remove(skillDefinition.Entity);

            StaticData.Entity.SaveChanges();

            SelectedIndustry.SkillDefinitions.Remove(skillDefinition.Id);
        }

        private void SaveChanges()
        {
            foreach (var industry in Industries)
            {
                industry.Entity.Name = industry.Name;

                industry.Entity.Description = industry.Description;
            }
        }
    }
}