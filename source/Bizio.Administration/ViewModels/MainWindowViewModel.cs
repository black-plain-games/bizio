﻿using Bizio.Administration.Data;
using BlackPlain.Wpf.Core;
using System.Linq;
using System.Windows.Input;

namespace Bizio.Administration.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        public ICommand ExportSeedScriptsCommand => GetAsyncCommand(ExportSeedScripts);

        private void ExportSeedScripts()
        {
            IsBusy = true;

            StaticData.Entity.Actions.SaveSeedFile("Action");

            StaticData.Entity.ActionRequirements.SaveSeedFile("ActionRequirement", "ActionId", "SkillDefinitionId");

            StaticData.Entity.Attributes.SaveSeedFile("Attribute");

            StaticData.Entity.CompanyMessageStatus.SaveSeedFile("CompanyMessageStatus");

            StaticData.Entity.CompanyNames.SaveSeedFile("CompanyName", "IndustryId", "Name");

            StaticData.Entity.CompanyTransactionTypes.SaveSeedFile("CompanyTransactionType");

            StaticData.Entity.ConfigurationKeys.SaveSeedFile("ConfigurationKey");

            StaticData.Entity.DifficultyLevels.SaveSeedFile("DifficultyLevel");

            StaticData.Entity.FirstNames.SaveSeedFile("FirstName", "GenderId", "Name");

            StaticData.Entity.Genders.SaveSeedFile("Gender");

            StaticData.Entity.GlobalConfigurations.SaveSeedFile("GlobalConfiguration", "ConfigurationKeyId");

            StaticData.Entity.Industries.SaveSeedFile("Industry");

            StaticData.Entity.Professions.SelectMany(p => p.Industries.Select(i => new { IndustryId = i.Id, ProfessionId = p.Id })).SaveSeedFile("IndustryProfession", "IndustryId", "ProfessionId");

            StaticData.Entity.SkillDefinitions.SelectMany(sd => sd.Industries.Select(i => new { IndustryId = i.Id, SkillDefinitionId = sd.Id })).SaveSeedFile("IndustrySkillDefinition", "IndustryId", "SkillDefinitionId");

            StaticData.Entity.LastNames.SaveSeedFile("LastName", "Name");

            StaticData.Entity.MessageLevels.SaveSeedFile("MessageLevel");

            StaticData.Entity.Perks.SaveSeedFile("Perk");

            StaticData.Entity.PerkBenefits.SaveSeedFile("PerkBenefit", "PerkId", "TargetTypeId", "AttributeId");

            StaticData.Entity.PerkBenefitAttributes.SaveSeedFile("PerkBenefitAttribute");

            StaticData.Entity.PerkConditions.SaveSeedFile("PerkCondition", "PerkId", "TargetTypeId", "AttributeId");

            StaticData.Entity.PerkConditionAttributes.SaveSeedFile("PerkConditionAttribute");

            StaticData.Entity.PerkConditionComparisons.SaveSeedFile("PerkConditionComparison");

            StaticData.Entity.PerkTargetTypes.SaveSeedFile("PerkTargetType");

            StaticData.Entity.PerkValueTypes.SaveSeedFile("PerkValueType");

            StaticData.Entity.Personalities.SaveSeedFile("Personality");

            StaticData.Entity.PersonalityAttributes.SaveSeedFile("PersonalityAttribute", "PersonalityId", "AttributeId");

            StaticData.Entity.Professions.SaveSeedFile("Profession");

            StaticData.Entity.ProfessionSkillDefinitions.SaveSeedFile("ProfessionSkillDefinition", "ProfessionId", "SkillDefinitionId");

            StaticData.Entity.ProjectDefinitions.SaveSeedFile("ProjectDefinition");

            StaticData.Entity.ProjectDefinitions.SelectMany(pd => pd.Industries.Select(i => new { IndustryId = i.Id, ProjectDefinitionId = pd.Id })).SaveSeedFile("ProjectDefinitionIndustry", "ProjectDefinitionId", "IndustryId");

            StaticData.Entity.ProjectDefinitionSkills.SaveSeedFile("ProjectDefinitionSkill", "ProjectDefinitionId", "SkillDefinitionId");

            StaticData.Entity.ProjectResults.SaveSeedFile("ProjectResult");

            StaticData.Entity.ProjectStatus.SaveSeedFile("ProjectStatus");

            StaticData.Entity.Roles.SaveSeedFile("Role");

            StaticData.Entity.SkillDefinitions.SaveSeedFile("SkillDefinition");

            IsBusy = false;
        }
    }
}