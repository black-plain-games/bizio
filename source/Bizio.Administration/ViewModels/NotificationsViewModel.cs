﻿using Bizio.Administration.Data;
using BlackPlain.Wpf.Core;
using System;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace Bizio.Administration.ViewModels
{
    public class NotificationsViewModel : ViewModelBase
    {
        public ObservableCollection<Notification> Notifications
        {
            get
            {
                return GetField<ObservableCollection<Notification>>();
            }
            set
            {
                SetField(value);
            }
        }

        public Notification SelectedNotification
        {
            get
            {
                return GetField<Notification>();
            }
            set
            {
                SetField(value);
            }
        }

        public ICommand AddNotificationCommand => GetCommand(AddNotification);

        public ICommand DuplicateNotificationCommand => GetCommand(DuplicateNotification, IsNotificationSelected);

        public ICommand DeleteNotificationCommand => GetCommand(DeleteNotification, CanDeleteNotification);

        public ICommand SaveChangesCommand => GetCommand(SaveChanges);

        public ICommand ClearChangesCommand => GetCommand(ClearChanges);

        protected override void LoadViewModel(object state)
        {
            StaticData.Load(EntityType.Notifications);

            Notifications = StaticData.Notifications;
        }

        private bool IsNotificationSelected() => SelectedNotification != null;

        private bool CanDeleteNotification() => SelectedNotification?.DateVisible > DateTimeOffset.Now;

        private void AddNotification()
        {
            var notification = new Notification
            {
                DateVisible = DateTime.Today.AddDays(1),
                Subject = "New notification",
                Message = "New message"
            };

            AddNotification(notification);
        }

        private void AddNotification(Notification notification)
        {
            var dbNotification = new Entities.Notification
            {
                DateVisible = notification.DateVisible,
                Subject = notification.Subject,
                Message = notification.Message
            };

            StaticData.Entity.Notifications.Add(dbNotification);

            StaticData.Entity.SaveChanges();

            notification.Entity = dbNotification;

            Notifications.Add(notification);

            SelectedNotification = notification;
        }

        private void DuplicateNotification()
        {
            var notification = new Notification
            {
                DateVisible = DateTime.Today.AddDays(1),
                Subject = SelectedNotification.Subject,
                Message = SelectedNotification.Message
            };

            AddNotification(notification);
        }

        private void DeleteNotification()
        {
            StaticData.Entity.Notifications.Remove(SelectedNotification.Entity);

            StaticData.Entity.SaveChanges();

            Notifications.Remove(SelectedNotification);

            SelectedNotification = null;
        }

        private void SaveChanges()
        {
            foreach (var notification in Notifications)
            {
                notification.Entity.DateVisible = notification.DateVisible;

                notification.Entity.Subject = notification.Subject;

                notification.Entity.Message = notification.Message;
            }

            StaticData.Entity.SaveChanges();
        }

        private void ClearChanges()
        {
            foreach (var notification in Notifications)
            {
                notification.DateVisible = notification.Entity.DateVisible;

                notification.Subject = notification.Entity.Subject;

                notification.Message = notification.Entity.Message;
            }
        }
    }
}