﻿using Bizio.Administration.Data;
using BlackPlain.Wpf.Core;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace Bizio.Administration.ViewModels
{
    public class PerksViewModel : ViewModelBase
    {
        public ObservableCollection<Perk> Perks
        {
            get
            {
                return GetField<ObservableCollection<Perk>>();
            }
            set
            {
                SetField(value);
            }
        }

        public ObservableCollection<IdNamePair<byte, string, Entities.PerkTargetType>> PerkTargetTypes
        {
            get
            {
                return GetField<ObservableCollection<IdNamePair<byte, string, Entities.PerkTargetType>>>();
            }
            set
            {
                SetField(value);
            }
        }

        public ObservableCollection<IdNamePair<byte, string, Entities.PerkValueType>> PerkValueTypes
        {
            get
            {
                return GetField<ObservableCollection<IdNamePair<byte, string, Entities.PerkValueType>>>();
            }
            set
            {
                SetField(value);
            }
        }

        public ObservableCollection<IdNamePair<byte, string, Entities.PerkBenefitAttribute>> PerkBenefitAttributes
        {
            get
            {
                return GetField<ObservableCollection<IdNamePair<byte, string, Entities.PerkBenefitAttribute>>>();
            }
            set
            {
                SetField(value);
            }
        }

        public ObservableCollection<IdNamePair<byte, string, Entities.PerkConditionAttribute>> PerkConditionAttributes
        {
            get
            {
                return GetField<ObservableCollection<IdNamePair<byte, string, Entities.PerkConditionAttribute>>>();
            }
            set
            {
                SetField(value);
            }
        }

        public ObservableCollection<IdNamePair<byte, string, Entities.PerkConditionComparison>> PerkConditionComparisons
        {
            get
            {
                return GetField<ObservableCollection<IdNamePair<byte, string, Entities.PerkConditionComparison>>>();
            }
            set
            {
                SetField(value);
            }
        }


        public IdNamePair<byte, string, Entities.PerkTargetType> SelectedPerkTargetType
        {
            get
            {
                return GetField<IdNamePair<byte, string, Entities.PerkTargetType>>();
            }
            set
            {
                SetField(value);
            }
        }

        public IdNamePair<byte, string, Entities.PerkValueType> SelectedPerkValueType
        {
            get
            {
                return GetField<IdNamePair<byte, string, Entities.PerkValueType>>();
            }
            set
            {
                SetField(value);
            }
        }

        public IdNamePair<byte, string, Entities.PerkBenefitAttribute> SelectedPerkBenefitAttribute
        {
            get
            {
                return GetField<IdNamePair<byte, string, Entities.PerkBenefitAttribute>>();
            }
            set
            {
                SetField(value);
            }
        }

        public IdNamePair<byte, string, Entities.PerkConditionAttribute> SelectedPerkConditionAttribute
        {
            get
            {
                return GetField<IdNamePair<byte, string, Entities.PerkConditionAttribute>>();
            }
            set
            {
                SetField(value);
            }
        }

        public IdNamePair<byte, string, Entities.PerkConditionComparison> SelectedPerkConditionComparison
        {
            get
            {
                return GetField<IdNamePair<byte, string, Entities.PerkConditionComparison>>();
            }
            set
            {
                SetField(value);
            }
        }


        public Perk SelectedPerk
        {
            get
            {
                return GetField<Perk>();
            }
            set
            {
                SetField(value);
            }
        }

        public PerkBenefit SelectedPerkBenefit
        {
            get
            {
                return GetField<PerkBenefit>();
            }
            set
            {
                SetField(value);
            }
        }

        public PerkCondition SelectedPerkCondition
        {
            get
            {
                return GetField<PerkCondition>();
            }
            set
            {
                SetField(value);
            }
        }

        public ICommand AddPerkCommand => GetCommand(AddPerk);

        public ICommand ResetPerkCommand => GetCommand(ResetPerk, CanResetPerk);

        public ICommand DeletePerkCommand => GetCommand(DeletePerk, CanDeletePerk);

        public ICommand SaveChangesCommand => GetCommand(SaveChanges, CanSaveChanges);

        public ICommand ClearChangesCommand => GetCommand(ClearChanges);

        public ICommand NewPerkBenefitCommand => GetCommand(NewPerkBenefit, CanNewPerkBenefit);

        public ICommand AddPerkBenefitCommand => GetCommand(AddPerkBenefit, CanAddPerkBenefit);

        public ICommand CancelAddPerkBenefitCommand => GetCommand(CancelAddPerkBenefit, CanCancelAddPerkBenefit);

        public ICommand DeletePerkBenefitCommand => GetCommand(DeletePerkBenefit, CanDeletePerkBenefit);

        public ICommand NewPerkConditionCommand => GetCommand(NewPerkCondition, CanNewPerkCondition);

        public ICommand AddPerkConditionCommand => GetCommand(AddPerkCondition, CanAddPerkCondition);

        public ICommand CancelAddPerkConditionCommand => GetCommand(CancelAddPerkCondition, CanCancelAddPerkCondition);

        public ICommand DeletePerkConditionCommand => GetCommand(DeletePerkCondition, CanDeletePerkCondition);

        public ICommand ClearAdditionalChangesCommand => GetCommand(ClearAdditionalChanges);

        public ICommand SaveAdditionalChangesCommand => GetCommand(SaveAdditionalChanges);

        public ICommand AddTargetTypeCommand => GetCommand(AddTargetType);

        public ICommand DeleteTargetTypeCommand => GetCommand(DeleteTargetType, CanDeleteTargetType);

        public ICommand AddValueTypeCommand => GetCommand(AddValueType);

        public ICommand DeleteValueTypeCommand => GetCommand(DeleteValueType, CanDeleteValueType);

        public ICommand AddBenefitAttributeCommand => GetCommand(AddBenefitAttribute);

        public ICommand DeleteBenefitAttributeCommand => GetCommand(DeleteBenefitAttribute, CanDeleteBenefitAttribute);

        public ICommand AddConditionAttributeCommand => GetCommand(AddConditionAttribute);

        public ICommand DeleteConditionAttributeCommand => GetCommand(DeleteConditionAttribute, CanDeleteConditionAttribute);

        public ICommand AddConditionComparisonCommand => GetCommand(AddConditionComparison);

        public ICommand DeleteConditionComparisonCommand => GetCommand(DeleteConditionComparison, CanDeleteConditionComparison);

        protected override void LoadViewModel(object state)
        {
            StaticData.Load(EntityType.Perks,
                EntityType.PerkTargetTypes,
                EntityType.PerkValueTypes,
                EntityType.PerkBenefitAttributes,
                EntityType.PerkConditionAttributes,
                EntityType.PerkConditionComparisons);

            Perks = StaticData.Perks;

            PerkTargetTypes = StaticData.PerkTargetTypes;

            PerkValueTypes = StaticData.PerkValueTypes;

            PerkBenefitAttributes = StaticData.PerkBenefitAttributes;

            PerkConditionAttributes = StaticData.PerkConditionAttributes;

            PerkConditionComparisons = StaticData.PerkConditionComparisons;
        }

        private bool CanResetPerk() => SelectedPerk != null;

        private bool CanDeletePerk() => SelectedPerk != null;

        private bool CanNewPerkBenefit() => SelectedPerk != null;

        private bool CanAddPerkBenefit() => SelectedPerkBenefit?.IsNew == true;

        private bool CanDeletePerkBenefit() => SelectedPerkBenefit != null;

        private bool CanNewPerkCondition() => SelectedPerk != null;

        private bool CanAddPerkCondition() => SelectedPerkCondition?.IsNew == true;

        private bool CanDeletePerkCondition() => SelectedPerkCondition != null;

        private bool CanSaveChanges() => Perks?.All(p => p.InitialCost > 0 || p.RecurringCost > 0) == true;

        private bool CanCancelAddPerkBenefit() => SelectedPerkBenefit?.IsNew == true;

        private bool CanCancelAddPerkCondition() => SelectedPerkBenefit?.IsNew == true;

        private bool CanDeleteTargetType() => SelectedPerkTargetType != null;

        private bool CanDeleteValueType() => SelectedPerkValueType != null;

        private bool CanDeleteBenefitAttribute() => SelectedPerkBenefitAttribute != null;

        private bool CanDeleteConditionAttribute() => SelectedPerkConditionAttribute != null;

        private bool CanDeleteConditionComparison() => SelectedPerkConditionComparison != null;

        private void AddPerk()
        {
            var perk = new Perk
            {
                Name = "New perk",
                Description = "New perk description",
                InitialCost = 1,
                Benefits = new ObservableCollection<PerkBenefit>(),
                Conditions = new ObservableCollection<PerkCondition>()
            };

            perk.Entity = new Entities.Perk
            {
                Name = perk.Name,
                Description = perk.Description,
                InitialCost = perk.InitialCost
            };

            StaticData.Entity.Perks.Add(perk.Entity);

            StaticData.Entity.SaveChanges();

            Perks.Add(perk);

            SelectedPerk = perk;
        }

        private void ResetPerk()
        {
            ResetPerk(SelectedPerk);
        }

        private void DeletePerk()
        {
            StaticData.Entity.Perks.Remove(SelectedPerk.Entity);

            StaticData.Entity.SaveChanges();

            Perks.Remove(SelectedPerk);

            SelectedPerk = null;
        }

        private void NewPerkBenefit()
        {
            SelectedPerkBenefit = new PerkBenefit
            {
                AttributeId = 1,
                ValueTypeId = 1,
                TargetTypeId = 1,
                Value = 0,
                IsNew = true
            };
        }

        private void AddPerkBenefit()
        {
            SelectedPerkBenefit.Entity = new Entities.PerkBenefit
            {
                AttributeId = SelectedPerkBenefit.AttributeId,
                ValueTypeId = SelectedPerkBenefit.ValueTypeId,
                TargetTypeId = SelectedPerkBenefit.TargetTypeId,
                Value = SelectedPerkBenefit.Value,
                Perk = SelectedPerk.Entity
            };

            StaticData.Entity.PerkBenefits.Add(SelectedPerkBenefit.Entity);

            StaticData.Entity.SaveChanges();

            SelectedPerk.Benefits.Add(SelectedPerkBenefit);

            SelectedPerkBenefit.IsNew = false;
        }

        private void CancelAddPerkBenefit()
        {
            SelectedPerkBenefit = SelectedPerk.Benefits.FirstOrDefault();
        }

        private void DeletePerkBenefit()
        {
            StaticData.Entity.PerkBenefits.Remove(SelectedPerkBenefit.Entity);

            StaticData.Entity.SaveChanges();

            SelectedPerk.Benefits.Remove(SelectedPerkBenefit);

            SelectedPerkBenefit = null;
        }

        private void NewPerkCondition()
        {
            SelectedPerkCondition = new PerkCondition
            {
                IsNew = true,
                TargetTypeId = 1,
                AttributeId = 1,
                ComparisonId = 1,
                Value = 0
            };
        }

        private void AddPerkCondition()
        {
            SelectedPerkCondition.Entity = new Entities.PerkCondition
            {
                TargetTypeId = SelectedPerkCondition.TargetTypeId,
                AttributeId = SelectedPerkCondition.AttributeId,
                ComparisonId = SelectedPerkCondition.ComparisonId,
                Value = SelectedPerkCondition.Value,
                Perk = SelectedPerk.Entity
            };

            StaticData.Entity.PerkConditions.Add(SelectedPerkCondition.Entity);

            StaticData.Entity.SaveChanges();

            SelectedPerk.Conditions.Add(SelectedPerkCondition);

            SelectedPerkCondition.IsNew = false;
        }

        private void CancelAddPerkCondition()
        {
            SelectedPerkCondition = SelectedPerk.Conditions.FirstOrDefault();
        }

        private void DeletePerkCondition()
        {
            StaticData.Entity.PerkConditions.Remove(SelectedPerkCondition.Entity);

            StaticData.Entity.SaveChanges();

            SelectedPerk.Conditions.Remove(SelectedPerkCondition);

            SelectedPerkCondition = null;
        }

        private void SaveChanges()
        {
            foreach (var perk in Perks)
            {
                perk.Entity.Name = perk.Name;

                perk.Entity.Description = perk.Description;

                perk.Entity.InitialCost = perk.InitialCost;

                perk.Entity.MaxCount = perk.MaxCount;

                perk.Entity.RecurringCost = perk.RecurringCost;

                perk.Entity.RecurringCostInterval = perk.RecurringCostInterval;

                foreach (var benefit in perk.Benefits)
                {
                    benefit.Entity.AttributeId = benefit.AttributeId;

                    benefit.Entity.TargetTypeId = benefit.TargetTypeId;

                    benefit.Entity.ValueTypeId = benefit.ValueTypeId;

                    benefit.Entity.Value = benefit.Value;
                }

                foreach (var condition in perk.Conditions)
                {
                    condition.Entity.AttributeId = condition.AttributeId;

                    condition.Entity.ComparisonId = condition.ComparisonId;

                    condition.Entity.TargetTypeId = condition.TargetTypeId;

                    condition.Entity.Value = condition.Value;
                }
            }

            StaticData.Entity.SaveChanges();
        }

        private void ResetPerk(Perk perk)
        {
            perk.Name = perk.Entity.Name;

            perk.Description = perk.Entity.Description;

            perk.InitialCost = perk.Entity.InitialCost;

            perk.MaxCount = perk.Entity.MaxCount;

            perk.RecurringCost = perk.Entity.RecurringCost;

            perk.RecurringCostInterval = perk.Entity.RecurringCostInterval;

            foreach (var benefit in perk.Benefits)
            {
                benefit.AttributeId = benefit.Entity.AttributeId;

                benefit.TargetTypeId = benefit.Entity.TargetTypeId;

                benefit.ValueTypeId = benefit.Entity.ValueTypeId;

                benefit.Value = benefit.Entity.Value;
            }

            foreach (var condition in perk.Conditions)
            {
                condition.AttributeId = condition.Entity.AttributeId;

                condition.ComparisonId = condition.Entity.ComparisonId;

                condition.TargetTypeId = condition.Entity.TargetTypeId;

                condition.Value = condition.Entity.Value;
            }
        }

        private void ClearChanges()
        {
            foreach (var perk in Perks)
            {
                ResetPerk(perk);
            }
        }

        private void ClearAdditionalChanges()
        {
            foreach (var targetType in PerkTargetTypes)
            {
                targetType.Name = targetType.Entity.Name;
            }
        }

        private void SaveAdditionalChanges()
        {
            foreach (var targetType in PerkTargetTypes)
            {
                targetType.Entity.Name = targetType.Name;
            }

            StaticData.Entity.SaveChanges();
        }

        private void AddTargetType()
        {
            PerkTargetTypes.Add(SelectedPerkTargetType = StaticData.Entity.PerkTargetTypes.Add());
        }

        private void DeleteTargetType()
        {
            StaticData.Entity.PerkTargetTypes.Remove(SelectedPerkTargetType.Entity);

            StaticData.Entity.SaveChanges();

            PerkTargetTypes.Remove(SelectedPerkTargetType);

            SelectedPerkTargetType = null;
        }

        private void AddValueType()
        {
            PerkValueTypes.Add(SelectedPerkValueType = StaticData.Entity.PerkValueTypes.Add());
        }

        private void DeleteValueType()
        {
            StaticData.Entity.PerkValueTypes.Remove(SelectedPerkValueType.Entity);

            StaticData.Entity.SaveChanges();

            PerkValueTypes.Remove(SelectedPerkValueType);

            SelectedPerkValueType = null;
        }

        private void AddBenefitAttribute()
        {
            PerkBenefitAttributes.Add(SelectedPerkBenefitAttribute = StaticData.Entity.PerkBenefitAttributes.Add());
        }

        private void DeleteBenefitAttribute()
        {
            StaticData.Entity.PerkBenefitAttributes.Remove(SelectedPerkBenefitAttribute.Entity);

            StaticData.Entity.SaveChanges();

            PerkBenefitAttributes.Remove(SelectedPerkBenefitAttribute);

            SelectedPerkBenefitAttribute = null;
        }

        private void AddConditionAttribute()
        {
            PerkConditionAttributes.Add(SelectedPerkConditionAttribute = StaticData.Entity.PerkConditionAttributes.Add());
        }

        private void DeleteConditionAttribute()
        {
            StaticData.Entity.PerkConditionAttributes.Remove(SelectedPerkConditionAttribute.Entity);

            StaticData.Entity.SaveChanges();

            PerkConditionAttributes.Remove(SelectedPerkConditionAttribute);

            SelectedPerkConditionAttribute = null;
        }

        private void AddConditionComparison()
        {
            PerkConditionComparisons.Add(SelectedPerkConditionComparison = StaticData.Entity.PerkConditionComparisons.Add());
        }

        private void DeleteConditionComparison()
        {
            StaticData.Entity.PerkConditionComparisons.Remove(SelectedPerkConditionComparison.Entity);

            StaticData.Entity.SaveChanges();

            PerkConditionComparisons.Remove(SelectedPerkConditionComparison);

            SelectedPerkConditionComparison = null;
        }
    }
}