﻿using Bizio.Administration.Data;
using BlackPlain.Wpf.Core;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace Bizio.Administration.ViewModels
{
    public class PersonalitiesViewModel : ViewModelBase
    {
        public ObservableCollection<Personality> Personalities
        {
            get
            {
                return GetField<ObservableCollection<Personality>>();
            }
            set
            {
                SetField(value);
            }
        }

        public Personality SelectedPersonality
        {
            get
            {
                return GetField<Personality>();
            }
            set
            {
                SetField(value);
            }
        }

        public PersonalityAttribute SelectedPersonalityAttribute
        {
            get
            {
                return GetField<PersonalityAttribute>();
            }
            set
            {
                SetField(value);
            }
        }

        public ICommand AddNewPersonalityCommand => GetCommand(AddNewPersonality);

        public ICommand DuplicateSelectedPersonalityCommand => GetCommand(DuplicateSelectedPersonality, IsPersonalitySelected);

        public ICommand ResetSelectedPersonalityCommand => GetCommand(ResetSelectedPersonality, IsPersonalitySelected);

        public ICommand DeleteSelectedPersonalityCommand => GetCommand(DeleteSelectedPersonality, IsPersonalitySelected);

        public ICommand ClearChangesCommand => GetCommand(ClearChanges);

        public ICommand SaveChangesCommand => GetCommand(SaveChanges);

        protected override void LoadViewModel(object state)
        {
            StaticData.Load(EntityType.Personalities, EntityType.Attributes);

            Personalities = StaticData.Personalities;

            SelectedPersonality = Personalities.FirstOrDefault();

            SelectedPersonalityAttribute = SelectedPersonality?.Attributes.FirstOrDefault();
        }

        private bool IsPersonalitySelected() => SelectedPersonality != null;

        private void AddNewPersonality()
        {
            var personality = new Personality
            {
                Name = "New Personality",
                Description = "New personality description",
                Attributes = new ObservableCollection<PersonalityAttribute>(StaticData.Attributes.Select(a => new PersonalityAttribute
                {
                    AttributeId = a.Id,
                    Value = 0
                }))
            };

            AddNewPersonality(personality);
        }

        private void AddNewPersonality(Personality personality)
        {
            personality.Entity = new Entities.Personality
            {
                Name = personality.Name,
                Description = personality.Description
            };

            foreach (var originalAttribute in personality.Attributes)
            {
                var attribute = new PersonalityAttribute
                {
                    AttributeId = originalAttribute.AttributeId,
                    Value = originalAttribute.Value
                };

                attribute.Entity = new Entities.PersonalityAttribute
                {
                    AttributeId = attribute.AttributeId,
                    Value = attribute.Value
                };

                personality.Attributes.Add(attribute);

                personality.Entity.PersonalityAttributes.Add(attribute.Entity);
            }

            StaticData.Entity.Personalities.Add(personality.Entity);

            StaticData.Entity.SaveChanges();

            personality.Id = personality.Entity.Id;

            Personalities.Add(personality);
        }

        private void DuplicateSelectedPersonality()
        {
            var personality = new Personality
            {
                Name = $"DUPLICATE {SelectedPersonality.Name}",
                Description = SelectedPersonality.Description,
                Attributes = new ObservableCollection<PersonalityAttribute>(SelectedPersonality.Attributes.Select(a => new PersonalityAttribute
                {
                    AttributeId = a.AttributeId,
                    Value = a.Value
                }))
            };

            AddNewPersonality(personality);
        }

        private void ResetSelectedPersonality() => ResetPersonality(SelectedPersonality);

        private void ResetPersonality(Personality personality)
        {
            personality.Name = personality.Entity.Name;

            personality.Description = personality.Entity.Description;

            foreach (var attribute in personality.Attributes)
            {
                attribute.AttributeId = attribute.Entity.AttributeId;

                attribute.Value = attribute.Entity.Value;
            }
        }

        private void ClearChanges()
        {
            foreach (var personality in Personalities)
            {
                ResetPersonality(personality);
            }
        }

        private void SaveChanges()
        {
            foreach (var personality in Personalities)
            {
                personality.Entity.Name = personality.Name;

                personality.Entity.Description = personality.Description;

                foreach (var attribute in personality.Attributes)
                {
                    attribute.Entity.Value = attribute.Value;
                }
            }

            StaticData.Entity.SaveChanges();
        }

        private void DeleteSelectedPersonality()
        {
            var result = MessageBox.Show("Are you sure you want to delete the selected personality? This cannot be undone.", "Confirm Delete", MessageBoxButton.YesNo);

            if (result != MessageBoxResult.Yes)
            {
                return;
            }

            StaticData.Entity.Personalities.Remove(SelectedPersonality.Entity);

            StaticData.Entity.SaveChanges();

            Personalities.Remove(SelectedPersonality);

            SelectedPersonality = Personalities.FirstOrDefault();
        }
    }
}