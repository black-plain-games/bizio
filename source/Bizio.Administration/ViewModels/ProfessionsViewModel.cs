﻿using Bizio.Administration.Data;
using BlackPlain.Wpf.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace Bizio.Administration.ViewModels
{
    public class ProfessionsViewModel : ViewModelBase
    {
        public ObservableCollection<Profession> Professions
        {
            get
            {
                return GetField<ObservableCollection<Profession>>();
            }
            set
            {
                SetField(value);
            }
        }

        public ObservableCollection<SkillDefinition> SkillDefinitions
        {
            get
            {
                return GetField<ObservableCollection<SkillDefinition>>();
            }
            set
            {
                SetField(value);
            }
        }

        public SkillDefinition SelectedSkillDefinition
        {
            get
            {
                return GetField<SkillDefinition>();
            }
            set
            {
                SetField(value);
            }
        }

        public ProfessionSkillDefinition SelectedProfessionSkillDefinition
        {
            get
            {
                return GetField<ProfessionSkillDefinition>();
            }
            set
            {
                if (SelectedProfessionSkillDefinition != null)
                {
                    SelectedProfessionSkillDefinition.WeightChanged -= OnWeightChanged;
                }

                SetField(value);

                if (value != null)
                {
                    value.WeightChanged += OnWeightChanged;
                }
            }
        }

        public Profession SelectedProfession
        {
            get
            {
                return GetField<Profession>();
            }
            set
            {
                SetField(value);

                OnPropertyChanged(nameof(ProfessionWeight));
            }
        }

        public int ProfessionWeight => _professionWeightPoints - SelectedProfession?.SkillDefinitions.Sum(sd => sd.Weight - _defaultSkillWeight) ?? 0;

        public ICommand AddNewProfessionCommand => GetCommand(AddNewProfession);

        public ICommand DuplicateProfessionCommand => GetCommand(DuplicateProfession, IsProfessionSelected);

        public ICommand ResetProfessionCommand => GetCommand(ResetProfession, IsProfessionSelected);

        public ICommand DeleteProfessionCommand => GetCommand(DeleteProfession, IsProfessionSelected);

        public ICommand AddNewProfessionSkillDefinitionCommand => GetCommand(AddNewProfessionSkillDefinition, CanAddProfessionSkillDefinition);

        public ICommand DeleteProfessionSkillDefinitionCommand => GetCommand(DeleteProfessionSkillDefinition, CanDeleteProfessionSkillDefinition);

        public ICommand SaveChangesCommand => GetCommand(SaveChanges);

        public ICommand ClearChangesCommand => GetCommand(ClearChanges);

        protected override void LoadViewModel(object state)
        {
            StaticData.Load(EntityType.Professions, EntityType.SkillDefinitions);

            Professions = StaticData.Professions;

            SkillDefinitions = StaticData.SkillDefinitions;

            SelectedProfession = Professions.FirstOrDefault();
        }

        private bool IsProfessionSelected() => SelectedProfession != null;

        private bool CanAddProfessionSkillDefinition() => SelectedProfession != null && SelectedSkillDefinition != null && !SelectedProfession.SkillDefinitions.Any(sd => sd.SkillDefinitionId == SelectedSkillDefinition.Id);

        private bool CanDeleteProfessionSkillDefinition() => SelectedProfession != null && SelectedProfessionSkillDefinition != null;

        private void AddNewProfessionSkillDefinition()
        {
            var professionSkillDefinition = new ProfessionSkillDefinition
            {
                ProfessionId = SelectedProfession.Id,
                SkillDefinitionId = SelectedSkillDefinition.Id,
                Weight = 125
            };

            professionSkillDefinition.Entity = new Entities.ProfessionSkillDefinition
            {
                ProfessionId = SelectedProfession.Id,
                SkillDefinitionId = professionSkillDefinition.SkillDefinitionId,
                Weight = professionSkillDefinition.Weight
            };

            StaticData.Entity.ProfessionSkillDefinitions.Add(professionSkillDefinition.Entity);

            StaticData.Entity.SaveChanges();

            SelectedProfession.SkillDefinitions.Add(professionSkillDefinition);

            SelectedProfessionSkillDefinition = professionSkillDefinition;
        }

        private void OnWeightChanged(object sender, EventArgs e)
        {
            OnPropertyChanged(nameof(ProfessionWeight));
        }

        private void DeleteProfessionSkillDefinition()
        {
            StaticData.Entity.ProfessionSkillDefinitions.Remove(SelectedProfessionSkillDefinition.Entity);

            StaticData.Entity.SaveChanges();

            SelectedProfession.SkillDefinitions.Remove(SelectedProfessionSkillDefinition);
        }

        private void AddNewProfession()
        {
            var profession = new Profession
            {
                Name = "New Profession",
                SkillDefinitions = new ObservableCollection<ProfessionSkillDefinition>()
            };

            AddNewProfession(profession);
        }

        private void AddNewProfession(Profession profession)
        {
            profession.Entity = new Entities.Profession
            {
                Name = profession.Name,
                ProfessionSkillDefinitions = new List<Entities.ProfessionSkillDefinition>()
            };

            foreach (var skillDefinition in profession.SkillDefinitions)
            {
                profession.Entity.ProfessionSkillDefinitions.Add(skillDefinition.Entity = new Entities.ProfessionSkillDefinition
                {
                    ProfessionId = skillDefinition.ProfessionId,
                    SkillDefinitionId = skillDefinition.SkillDefinitionId,
                    Weight = skillDefinition.Weight
                });
            }

            StaticData.Entity.Professions.Add(profession.Entity);

            StaticData.Entity.SaveChanges();

            profession.Id = profession.Entity.Id;

            Professions.Add(profession);
        }

        private void DuplicateProfession()
        {
            var profession = new Profession
            {
                Name = $"DUPLICATE {SelectedProfession.Name}",
                SkillDefinitions = new ObservableCollection<ProfessionSkillDefinition>(SelectedProfession.SkillDefinitions.Select(sd => new ProfessionSkillDefinition
                {
                    ProfessionId = sd.ProfessionId,
                    SkillDefinitionId = sd.SkillDefinitionId,
                    Weight = sd.Weight
                }))
            };

            AddNewProfession(profession);
        }

        private void ResetProfession() => ResetProfession(SelectedProfession);

        private void ResetProfession(Profession profession)
        {
            profession.Name = profession.Entity.Name;

            foreach (var skillDefinition in profession.SkillDefinitions)
            {
                skillDefinition.Weight = skillDefinition.Entity.Weight;
            }
        }

        private void DeleteProfession()
        {
            StaticData.Entity.Professions.Remove(SelectedProfession.Entity);

            StaticData.Entity.SaveChanges();

            Professions.Remove(SelectedProfession);
        }

        private void SaveChanges()
        {
            foreach (var profession in Professions)
            {
                profession.Entity.Name = profession.Name;

                foreach (var skillDefinition in profession.SkillDefinitions)
                {
                    skillDefinition.Entity.SkillDefinitionId = skillDefinition.SkillDefinitionId;

                    skillDefinition.Entity.Weight = skillDefinition.Weight;
                }
            }

            StaticData.Entity.SaveChanges();
        }

        private void ClearChanges()
        {
            foreach (var profession in Professions)
            {
                ResetProfession(profession);
            }
        }

        private static readonly int _professionWeightPoints = 500;

        private static readonly int _defaultSkillWeight = 50;
    }
}