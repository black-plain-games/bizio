﻿using Bizio.Administration.Data;
using BlackPlain.Wpf.Core;
using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Windows.Input;

namespace Bizio.Administration.ViewModels
{
    public class AverageRange<T> : Range<T>
    {
        public T Average
        {
            get
            {
                return GetField<T>();
            }
            set
            {
                SetField(value);
            }
        }
    }

    public class ProjectDefinitionsViewModel : ViewModelBase
    {
        public ObservableCollection<ProjectDefinition> ProjectDefinitions
        {
            get
            {
                return GetField<ObservableCollection<ProjectDefinition>>();
            }
            set
            {
                SetField(value);
            }
        }

        public ProjectDefinition SelectedProjectDefinition
        {
            get
            {
                return GetField<ProjectDefinition>();
            }
            set
            {
                var oldValue = GetField<ProjectDefinition>();

                if (oldValue != null)
                {
                    oldValue.Skills.CollectionChanged -= OnSkillAdded;

                    foreach (var skill in oldValue.Skills)
                    {
                        skill.Value.PropertyChanged += UpdateTotalSkillPoints;
                    }
                }

                SetField(value);

                if (value != null)
                {
                    value.Skills.CollectionChanged += OnSkillAdded;

                    foreach (var skill in value.Skills)
                    {
                        skill.Value.PropertyChanged += UpdateTotalSkillPoints;
                    }
                }

                UpdateTotalSkillPoints(null, null);
            }
        }

        private void OnSkillAdded(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e == null)
            {
                return;
            }

            foreach (ProjectDefinitionSkill pds in e.NewItems)
            {
                pds.Value.PropertyChanged += UpdateTotalSkillPoints;
            }

            if (e.OldItems == null)
            {
                return;
            }

            foreach (ProjectDefinitionSkill pds in e.OldItems)
            {
                pds.Value.PropertyChanged -= UpdateTotalSkillPoints;
            }
        }

        private void UpdateTotalSkillPoints(object sender, EventArgs args)
        {
            var skillValues = SelectedProjectDefinition.Skills.Select(s => s.Value);

            var skillPoints = new AverageRange<decimal>
            {
                Minimum = skillValues.Sum(r => r.Minimum),
                Maximum = skillValues.Sum(r => r.Maximum)
            };

            skillPoints.Average = (skillPoints.Minimum + skillPoints.Maximum) / 2m;

            SkillPoints = skillPoints;

            var minimumTurns = (decimal)SelectedProjectDefinition.Skills.Count * .25m;

            var suggestedTurns = new AverageRange<decimal>
            {
                Minimum = Math.Ceiling(minimumTurns + SelectedProjectDefinition.Skills.Max(s => s.Value.Minimum / 250m)),
                Maximum = Math.Ceiling(minimumTurns + SelectedProjectDefinition.Skills.Max(s => s.Value.Maximum / 50m)),
            };

            suggestedTurns.Average = (suggestedTurns.Minimum + suggestedTurns.Maximum) / 2m;

            SuggestedTurns = suggestedTurns;

            var suggestedValue = new AverageRange<decimal>
            {
                Minimum = SelectedProjectDefinition.Skills.Sum(s => s.Value.Minimum) * suggestedTurns.Minimum,
                Maximum = SelectedProjectDefinition.Skills.Sum(s => s.Value.Maximum) * suggestedTurns.Maximum,
            };

            suggestedValue.Average = (suggestedValue.Minimum + suggestedValue.Maximum) / 2m;

            SuggestedValue = suggestedValue;
        }

        public ObservableCollection<SkillDefinition> SkillDefinitions
        {
            get
            {
                return GetField<ObservableCollection<SkillDefinition>>();
            }
            set
            {
                SetField(value);
            }
        }

        public SkillDefinition SelectedSkillDefinition
        {
            get
            {
                return GetField<SkillDefinition>();
            }
            set
            {
                SetField(value);
            }
        }

        public ProjectDefinitionSkill SelectedProjectDefinitionSkill
        {
            get
            {
                return GetField<ProjectDefinitionSkill>();
            }
            set
            {
                SetField(value);
            }
        }

        public ObservableCollection<Industry> Industries
        {
            get
            {
                return GetField<ObservableCollection<Industry>>();
            }
            set
            {
                SetField(value);
            }
        }

        public Industry SelectedIndustry
        {
            get
            {
                return GetField<Industry>();
            }
            set
            {
                SetField(value);
            }
        }

        public byte? SelectedProjectDefinitionIndustry
        {
            get
            {
                return GetField<byte?>();
            }
            set
            {
                SetField(value);
            }
        }

        public decimal? TotalMinimumSkillPoints => SelectedProjectDefinition?.Skills.Sum(s => s.Value.Minimum);

        public decimal? TotalMaximumSkillPoints => SelectedProjectDefinition?.Skills.Sum(s => s.Value.Maximum);

        public AverageRange<decimal> SkillPoints
        {
            get
            {
                return GetField<AverageRange<decimal>>();
            }
            set
            {
                SetField(value);
            }
        }

        public AverageRange<decimal> SuggestedValue
        {
            get
            {
                return GetField<AverageRange<decimal>>();
            }
            set
            {
                SetField(value);
            }
        }

        public AverageRange<decimal> SuggestedTurns
        {
            get
            {
                return GetField<AverageRange<decimal>>();
            }
            set
            {
                SetField(value);
            }
        }

        public ICommand AddNewProjectDefinitionCommand => GetCommand(AddNewProjectDefinition);

        public ICommand DuplicateProjectDefinitionCommand => GetCommand(DuplicateProjectDefinition, IsProjectDefinitionSelected);

        public ICommand ResetProjectDefinitionCommand => GetCommand(ResetProjectDefinition, IsProjectDefinitionSelected);

        public ICommand DeleteProjectDefinitionCommand => GetCommand(DeleteProjectDefinition, IsProjectDefinitionSelected);

        public ICommand AddSkillDefinitionCommand => GetCommand(AddSkillDefinition, CanAddSkillDefinition);

        public ICommand RemoveProjectDefinitionSkillCommand => GetCommand(RemoveProjectDefinitionSkill, CanRemoveProjectDefinitionSkill);

        public ICommand AddIndustryCommand => GetCommand(AddIndustry, CanAddIndustry);

        public ICommand RemoveProjectDefinitionIndustryCommand => GetCommand(RemoveProjectDefinitionIndustry, CanRemoveProjectDefinitionIndustry);

        public ICommand ClearChangesCommand => GetCommand(ClearChanges);

        public ICommand SaveChangesCommand => GetCommand(SaveChanges);

        protected override void LoadViewModel(object state)
        {
            StaticData.Load(EntityType.ProjectDefinitions, EntityType.Industries, EntityType.SkillDefinitions);

            ProjectDefinitions = StaticData.ProjectDefinitions;

            Industries = StaticData.Industries;

            SkillDefinitions = StaticData.SkillDefinitions;
        }

        private bool IsProjectDefinitionSelected() => SelectedProjectDefinition != null;

        private bool CanAddSkillDefinition() => SelectedProjectDefinition != null && SelectedSkillDefinition != null;

        private bool CanRemoveProjectDefinitionSkill() => SelectedProjectDefinition != null && SelectedProjectDefinitionSkill != null;

        private bool CanAddIndustry() => SelectedProjectDefinition != null && SelectedIndustry != null;

        private bool CanRemoveProjectDefinitionIndustry() => SelectedProjectDefinition != null && SelectedProjectDefinitionIndustry.HasValue;

        private void AddNewProjectDefinition()
        {
            var projectDefinition = new ProjectDefinition
            {
                Name = "New project definition",
                Description = "New project definition",
                ProjectLength = new Range<int>
                {
                    Minimum = 1,
                    Maximum = 52
                },
                Value = new Range<decimal>
                {
                    Minimum = 100,
                    Maximum = 100000
                },
                Industries = new ObservableCollection<byte>(),
                Skills = new ObservableCollection<ProjectDefinitionSkill>()
            };

            AddNewProjectDefinition(projectDefinition);
        }

        private void AddNewProjectDefinition(ProjectDefinition projectDefinition)
        {
            projectDefinition.Entity = new Entities.ProjectDefinition
            {
                Name = projectDefinition.Name,
                Description = projectDefinition.Description,
                MinimumProjectLength = projectDefinition.ProjectLength.Minimum,
                MaximumProjectLength = projectDefinition.ProjectLength.Maximum,
                MinimumValue = projectDefinition.Value.Minimum,
                MaximumValue = projectDefinition.Value.Maximum
            };

            StaticData.Entity.ProjectDefinitions.Add(projectDefinition.Entity);

            foreach (var skill in projectDefinition.Skills)
            {
                StaticData.Entity.ProjectDefinitionSkills.Add(skill.Entity = new Entities.ProjectDefinitionSkill
                {
                    ProjectDefinitionId = projectDefinition.Id,
                    SkillDefinitionId = skill.SkillDefinitionId,
                    Minimum = skill.Value.Minimum,
                    Maximum = skill.Value.Maximum
                });
            }

            foreach (var dbIndustry in StaticData.Entity.Industries.Where(i => projectDefinition.Industries.Contains(i.Id)))
            {
                dbIndustry.ProjectDefinitions.Add(projectDefinition.Entity);
            }

            StaticData.Entity.SaveChanges();

            projectDefinition.Id = projectDefinition.Entity.Id;

            ProjectDefinitions.Add(projectDefinition);

            SelectedProjectDefinition = projectDefinition;
        }

        private void DuplicateProjectDefinition()
        {
            var projectDefinition = new ProjectDefinition
            {
                Name = $"DUPLICATE {SelectedProjectDefinition.Name}",
                Description = SelectedProjectDefinition.Description,
                ProjectLength = new Range<int>
                {
                    Minimum = SelectedProjectDefinition.ProjectLength.Minimum,
                    Maximum = SelectedProjectDefinition.ProjectLength.Maximum
                },
                Value = new Range<decimal>
                {
                    Minimum = SelectedProjectDefinition.Value.Minimum,
                    Maximum = SelectedProjectDefinition.Value.Maximum
                },
                Industries = new ObservableCollection<byte>(SelectedProjectDefinition.Industries),
                Skills = new ObservableCollection<ProjectDefinitionSkill>(SelectedProjectDefinition.Skills.Select(s => new ProjectDefinitionSkill
                {
                    SkillDefinitionId = s.SkillDefinitionId,
                    Value = new Range<decimal>
                    {
                        Minimum = s.Value.Minimum,
                        Maximum = s.Value.Maximum
                    }
                }))
            };

            AddNewProjectDefinition(projectDefinition);
        }

        private void ResetProjectDefinition() => ResetProjectDefinition(SelectedProjectDefinition);

        private void ResetProjectDefinition(ProjectDefinition projectDefinition)
        {
            projectDefinition.Name = projectDefinition.Entity.Name;

            projectDefinition.Description = projectDefinition.Entity.Description;

            projectDefinition.ProjectLength.Minimum = projectDefinition.Entity.MinimumProjectLength;

            projectDefinition.ProjectLength.Maximum = projectDefinition.Entity.MaximumProjectLength;

            projectDefinition.Value.Minimum = projectDefinition.Entity.MinimumValue;

            projectDefinition.Value.Maximum = projectDefinition.Entity.MaximumValue;

            projectDefinition.Industries.Clear();

            foreach (var dbIndustry in projectDefinition.Entity.Industries)
            {
                projectDefinition.Industries.Add(dbIndustry.Id);
            }

            foreach (var skill in projectDefinition.Skills)
            {
                skill.SkillDefinitionId = skill.Entity.SkillDefinitionId;

                skill.Value.Minimum = skill.Entity.Minimum;

                skill.Value.Maximum = skill.Entity.Maximum;
            }
        }

        private void DeleteProjectDefinition()
        {
            StaticData.Entity.ProjectDefinitions.Remove(SelectedProjectDefinition.Entity);

            StaticData.Entity.SaveChanges();

            ProjectDefinitions.Remove(SelectedProjectDefinition);

            SelectedProjectDefinition = null;
        }

        private void AddSkillDefinition()
        {
            var projectDefinitionSkill = new ProjectDefinitionSkill
            {
                SkillDefinitionId = SelectedSkillDefinition.Id,
                Value = new Range<decimal>
                {
                    Minimum = 10,
                    Maximum = 1000
                }
            };

            projectDefinitionSkill.Entity = new Entities.ProjectDefinitionSkill
            {
                ProjectDefinitionId = SelectedProjectDefinition.Id,
                SkillDefinitionId = projectDefinitionSkill.SkillDefinitionId,
                Minimum = projectDefinitionSkill.Value.Minimum,
                Maximum = projectDefinitionSkill.Value.Maximum
            };

            StaticData.Entity.ProjectDefinitionSkills.Add(projectDefinitionSkill.Entity);

            StaticData.Entity.SaveChanges();

            SelectedProjectDefinition.Skills.Add(projectDefinitionSkill);
        }

        private void RemoveProjectDefinitionSkill()
        {
            StaticData.Entity.ProjectDefinitionSkills.Remove(SelectedProjectDefinitionSkill.Entity);

            StaticData.Entity.SaveChanges();

            SelectedProjectDefinition.Skills.Remove(SelectedProjectDefinitionSkill);

            SelectedProjectDefinitionSkill = null;
        }

        private void AddIndustry()
        {
            SelectedProjectDefinition.Industries.Add(SelectedIndustry.Id);

            SelectedProjectDefinition.Entity.Industries.Add(SelectedIndustry.Entity);

            StaticData.Entity.SaveChanges();
        }

        private void RemoveProjectDefinitionIndustry()
        {
            var dbIndustry = SelectedProjectDefinition.Entity.Industries.First(i => i.Id == SelectedProjectDefinitionIndustry.Value);

            SelectedProjectDefinition.Industries.Remove(SelectedProjectDefinitionIndustry.Value);

            SelectedProjectDefinition.Entity.Industries.Remove(dbIndustry);

            StaticData.Entity.SaveChanges();

            SelectedProjectDefinitionIndustry = null;
        }

        private void ClearChanges()
        {
            foreach (var projectDefinition in ProjectDefinitions)
            {
                ResetProjectDefinition(projectDefinition);
            }
        }

        private void SaveChanges()
        {
            foreach (var projectDefinition in ProjectDefinitions)
            {
                projectDefinition.Entity.Name = projectDefinition.Name;

                projectDefinition.Entity.Description = projectDefinition.Description;

                projectDefinition.Entity.MinimumProjectLength = projectDefinition.ProjectLength.Minimum;

                projectDefinition.Entity.MaximumProjectLength = projectDefinition.ProjectLength.Maximum;

                projectDefinition.Entity.MinimumValue = projectDefinition.Value.Minimum;

                projectDefinition.Entity.MaximumValue = projectDefinition.Value.Maximum;

                foreach (var skill in projectDefinition.Skills)
                {
                    skill.Entity.SkillDefinitionId = skill.SkillDefinitionId;

                    skill.Entity.Minimum = skill.Value.Minimum;

                    skill.Entity.Maximum = skill.Value.Maximum;
                }
            }

            StaticData.Entity.SaveChanges();
        }
    }
}