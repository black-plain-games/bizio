﻿using Bizio.Administration.Data;
using BlackPlain.Wpf.Core;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace Bizio.Administration.ViewModels
{
    public class SkillDefinitionsViewModel : ViewModelBase
    {
        public ObservableCollection<SkillDefinition> SkillDefinitions
        {
            get
            {
                return GetField<ObservableCollection<SkillDefinition>>();
            }
            set
            {
                SetField(value);
            }
        }

        public SkillDefinition SelectedSkillDefinition
        {
            get
            {
                return GetField<SkillDefinition>();
            }
            set
            {
                SetField(value);
            }
        }

        public ICommand AddNewSkillDefinitionCommand => GetCommand(AddNewSkillDefinition);

        public ICommand DuplicateSelectedSkillDefinitionCommand => GetCommand(DuplicateSelectedSkillDefinition, IsSkillDefinitionSelected);

        public ICommand ResetSelectedSkillDefinitionCommand => GetCommand(ResetSelectedSkillDefinition, IsSkillDefinitionSelected);

        public ICommand DeleteSelectedSkillDefinitionCommand => GetCommand(DeleteSelectedSkillDefinition, IsSkillDefinitionSelected);

        public ICommand ClearChangesCommand => GetCommand(ClearChanges);

        public ICommand SaveChangesCommand => GetCommand(SaveChanges);

        protected override void LoadViewModel(object state)
        {
            StaticData.Load(EntityType.SkillDefinitions);

            SkillDefinitions = StaticData.SkillDefinitions;
        }

        private void AddNewSkillDefinition()
        {
            var skillDefinition = new SkillDefinition
            {
                Name = "New Skill Definition",
                Value = new Range<decimal>
                {
                    Minimum = 0,
                    Maximum = 200
                }
            };

            AddSkillDefinition(skillDefinition);
        }

        private void AddSkillDefinition(SkillDefinition skillDefinition)
        {
            skillDefinition.Entity = new Entities.SkillDefinition
            {
                Name = skillDefinition.Name,
                Minimum = skillDefinition.Value.Minimum,
                Maximum = skillDefinition.Value.Maximum
            };

            StaticData.Entity.SkillDefinitions.Add(skillDefinition.Entity);

            StaticData.Entity.SaveChanges();

            skillDefinition.Id = skillDefinition.Entity.Id;

            SkillDefinitions.Add(skillDefinition);

            SelectedSkillDefinition = skillDefinition;
        }

        private bool IsSkillDefinitionSelected() => SelectedSkillDefinition != null;

        private void DuplicateSelectedSkillDefinition()
        {
            var skillDefinition = new SkillDefinition
            {
                Name = $"DUPLICATE {SelectedSkillDefinition.Name}",
                Value = new Range<decimal>
                {
                    Minimum = SelectedSkillDefinition.Value.Minimum,
                    Maximum = SelectedSkillDefinition.Value.Maximum
                }
            };

            AddSkillDefinition(skillDefinition);
        }

        private void ResetSelectedSkillDefinition() => ResetSelectedSkillDefinition(SelectedSkillDefinition);

        private void ResetSelectedSkillDefinition(SkillDefinition skillDefinition)
        {
            skillDefinition.Name = skillDefinition.Entity.Name;

            skillDefinition.Value.Minimum = skillDefinition.Entity.Minimum;

            skillDefinition.Value.Maximum = skillDefinition.Entity.Maximum;
        }

        private void DeleteSelectedSkillDefinition()
        {
            StaticData.Entity.SkillDefinitions.Remove(SelectedSkillDefinition.Entity);

            StaticData.Entity.SaveChanges();

            SkillDefinitions.Remove(SelectedSkillDefinition);
        }

        private void ClearChanges()
        {
            foreach (var skillDefinition in SkillDefinitions)
            {
                ResetSelectedSkillDefinition(skillDefinition);
            }
        }

        private void SaveChanges()
        {
            foreach (var skillDefinition in SkillDefinitions)
            {
                skillDefinition.Entity.Name = skillDefinition.Name;

                skillDefinition.Entity.Minimum = skillDefinition.Value.Minimum;

                skillDefinition.Entity.Maximum = skillDefinition.Value.Maximum;
            }

            StaticData.Entity.SaveChanges();
        }
    }
}