﻿using Bizio.Administration.ViewModels;

namespace Bizio.Administration.Views
{
    public partial class ProjectDefinitionsView
    {
        private ProjectDefinitionsViewModel ViewModel => DataContext as ProjectDefinitionsViewModel;

        public ProjectDefinitionsView()
        {
            InitializeComponent();
        }
    }
}