﻿using Bizio.Client.Desktop.Core;
using Bizio.Client.Desktop.ViewModels;
using Bizio.Client.Desktop.Views;
using Bizio.Repositories;
using BlackPlain.Bizio.Services;
using Microsoft.Extensions.DependencyInjection;
using System.Windows;

namespace Bizio.Client.Desktop
{
    public partial class App : Application
    {
        private readonly IServiceProvider _provider;

        public App()
        {
            var services = new ServiceCollection()
                .AddSingleton<ISharedResourceService, SharedResourceService>()
                .AddBizioRepositories()
                .AddBizioServices()
                .RegisterViewModels()
                .RegisterViews();

            _provider = services.BuildServiceProvider();
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            DataFilePaths.StaticDataFilePath = Paths.StaticDataPath;
            DataFilePaths.ConfigurationFilePath = Paths.ConfigurationPath;
            DataFilePaths.GamesFilePath = Paths.SavedGamesPath;

            UiText.LoadLanguages();

            _provider.GetRequiredService<MainWindow>().Show();
        }
    }
}
