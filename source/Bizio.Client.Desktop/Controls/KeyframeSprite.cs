﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Timers;
using System.Windows;

namespace Bizio.Client.Desktop.Controls
{
    public class KeyframeSprite : SpriteBase
    {
        #region Dependency Properties

        private static readonly DependencyPropertyKey FramesPropertyKey = DependencyProperty.RegisterReadOnly("Frames",
            typeof(ObservableCollection<SpriteFrame>), typeof(KeyframeSprite),
            new FrameworkPropertyMetadata(new ObservableCollection<SpriteFrame>(),
                FrameworkPropertyMetadataOptions.AffectsRender));

        public static readonly DependencyProperty FramesProperty = FramesPropertyKey.DependencyProperty;

        public static readonly DependencyProperty SourceWidthProperty = DependencyProperty.Register("SourceWidth", typeof(double?), typeof(KeyframeSprite),
            new FrameworkPropertyMetadata(null,
                FrameworkPropertyMetadataOptions.AffectsMeasure |
                FrameworkPropertyMetadataOptions.AffectsRender));

        public static readonly DependencyProperty SourceHeightProperty = DependencyProperty.Register("SourceHeight", typeof(double?), typeof(KeyframeSprite),
            new FrameworkPropertyMetadata(null,
                FrameworkPropertyMetadataOptions.AffectsMeasure |
                FrameworkPropertyMetadataOptions.AffectsRender));

        public static readonly DependencyProperty DefaultFrameTimeProperty = DependencyProperty.Register("DefaultFrameTime", typeof(double?), typeof(KeyframeSprite),
            new FrameworkPropertyMetadata(null));

        #endregion

        public ObservableCollection<SpriteFrame> Frames
        {
            get
            {
                return (ObservableCollection<SpriteFrame>)GetValue(FramesProperty);
            }
            set
            {
                SetValue(FramesProperty, value);
            }
        }

        public double? SourceWidth
        {
            get
            {
                return (double?)GetValue(SourceWidthProperty);
            }
            set
            {
                SetValue(SourceWidthProperty, value);
            }
        }

        public double? SourceHeight
        {
            get
            {
                return (double?)GetValue(SourceHeightProperty);
            }
            set
            {
                SetValue(SourceHeightProperty, value);
            }
        }

        public double? DefaultFrameTime
        {
            get
            {
                return (double?)GetValue(DefaultFrameTimeProperty);
            }
            set
            {
                SetValue(DefaultFrameTimeProperty, value);
            }
        }

        protected override int GetFrameCount()
        {
            return Frames.Count;
        }

        public KeyframeSprite()
            : base()
        {
            SetValue(FramesPropertyKey, new ObservableCollection<SpriteFrame>());
        }

        protected override void OnInitialized(EventArgs e)
        {
            if (Frames.Any())
            {
                FrameTime = GetCurrentFrameTime();
            }

            base.OnInitialized(e);
        }

        protected override void OnFrameChanged(object sender, ElapsedEventArgs e)
        {
            FrameTime = GetCurrentFrameTime();
        }

        private double GetCurrentFrameTime()
        {
            return Frames[CurrentFrame].FrameTime ?? DefaultFrameTime.Value;
        }

        protected override Rect GetViewbox()
        {
            return Frames[CurrentFrame].GetViewbox(SourceWidth, SourceHeight);
        }
    }
}