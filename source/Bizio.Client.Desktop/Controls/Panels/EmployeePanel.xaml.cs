﻿using Bizio.Core.Model.Companies;
using System.Windows;
using System.Windows.Input;

namespace Bizio.Client.Desktop.Controls.Panels
{
    public class EmployeePanel : BaseControl
    {
        public static readonly DependencyProperty EmployeeProperty = Register<Employee, EmployeePanel>(nameof(Employee));

        public static readonly DependencyProperty OfferValueProperty = Register<int, EmployeePanel>(nameof(OfferValue));

        public static readonly DependencyProperty AllocationsProperty = Register<IEnumerable<Allocation>, EmployeePanel>(nameof(Allocations));

        public static readonly DependencyProperty AddAllocationCommandProperty = Register<ICommand, EmployeePanel>(nameof(AddAllocationCommand));

        public static readonly DependencyProperty EditAllocationCommandProperty = Register<ICommand, EmployeePanel>(nameof(EditAllocationCommand));

        public static readonly DependencyProperty DeleteAllocationCommandProperty = Register<ICommand, EmployeePanel>(nameof(DeleteAllocationCommand));

        public static readonly DependencyProperty RemainingTurnActionCountProperty = Register<IDictionary<byte, int>, EmployeePanel>(nameof(RemainingTurnActionCount));

        public Employee Employee
        {
            get
            {
                return GetValue<Employee>(EmployeeProperty);
            }
            set
            {
                SetValue(EmployeeProperty, value);
            }
        }

        public int OfferValue
        {
            get
            {
                return GetValue<int>(OfferValueProperty);
            }
            set
            {
                SetValue(OfferValueProperty, value);
            }
        }

        public IEnumerable<Allocation> Allocations
        {
            get
            {
                return GetValue<IEnumerable<Allocation>>(AllocationsProperty);
            }
            set
            {
                SetValue(AllocationsProperty, value);
            }
        }

        public ICommand AddAllocationCommand
        {
            get
            {
                return GetValue<ICommand>(AddAllocationCommandProperty);
            }
            set
            {
                SetValue(AddAllocationCommandProperty, value);
            }
        }

        public ICommand EditAllocationCommand
        {
            get
            {
                return GetValue<ICommand>(EditAllocationCommandProperty);
            }
            set
            {
                SetValue(EditAllocationCommandProperty, value);
            }
        }

        public ICommand DeleteAllocationCommand
        {
            get
            {
                return GetValue<ICommand>(DeleteAllocationCommandProperty);
            }
            set
            {
                SetValue(DeleteAllocationCommandProperty, value);
            }
        }

        public IDictionary<byte, int> RemainingTurnActionCount
        {
            get
            {
                return GetValue<IDictionary<byte, int>>(RemainingTurnActionCountProperty);
            }
            set
            {
                SetValue(RemainingTurnActionCountProperty, value);
            }
        }

        static EmployeePanel()
        {
            ResetDefaultStyleKey<EmployeePanel>();
        }
    }
}