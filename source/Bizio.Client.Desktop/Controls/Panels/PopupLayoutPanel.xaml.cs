﻿using System.Collections;
using System.Windows;
using System.Windows.Input;

namespace Bizio.Client.Desktop.Controls.Panels
{
    public class PopupLayoutPanel : BaseControl
    {
        public static readonly DependencyProperty HeaderProperty = Register<object, PopupLayoutPanel>(nameof(Header));

        public static readonly DependencyProperty ContentProperty = Register<object, PopupLayoutPanel>(nameof(Content));

        public static readonly DependencyProperty ErrorContentProperty = Register<object, PopupLayoutPanel>(nameof(ErrorContent));

        public static readonly DependencyProperty ErrorsProperty = Register<IEnumerable<string>, PopupLayoutPanel>(nameof(Errors));

        public static readonly DependencyProperty DismissCommandProperty = Register<ICommand, PopupLayoutPanel>(nameof(DismissCommand));

        public static readonly DependencyProperty ButtonsProperty = Register<IList, PopupLayoutPanel>(nameof(Buttons));

        public static readonly DependencyProperty IsCancelVisibleProperty = Register<bool, PopupLayoutPanel>(nameof(IsCancelVisible), true);

        public static readonly DependencyProperty IsBusyProperty = Register<bool, PopupLayoutPanel>(nameof(IsBusy), true);

        public object Header
        {
            get
            {
                return GetValue<object>(HeaderProperty);
            }
            set
            {
                SetValue(HeaderProperty, value);
            }
        }

        public object Content
        {
            get
            {
                return GetValue<object>(ContentProperty);
            }
            set
            {
                SetValue(ContentProperty, value);
            }
        }

        public object ErrorContent
        {
            get
            {
                return GetValue<object>(ErrorContentProperty);
            }
            set
            {
                SetValue(ErrorContentProperty, value);
            }
        }

        public IEnumerable<string> Errors
        {
            get
            {
                return GetValue<IEnumerable<string>>(ErrorsProperty);
            }
            set
            {
                SetValue(ErrorsProperty, value);
            }
        }

        public ICommand DismissCommand
        {
            get
            {
                return GetValue<ICommand>(DismissCommandProperty);
            }
            set
            {
                SetValue(DismissCommandProperty, value);
            }
        }

        public IList Buttons
        {
            get
            {
                return GetValue<IList>(ButtonsProperty);
            }
            set
            {
                SetValue(ButtonsProperty, value);
            }
        }

        public bool IsCancelVisible
        {
            get
            {
                return (bool)GetValue(IsCancelVisibleProperty);
            }
            set
            {
                SetValue(IsCancelVisibleProperty, value);
            }
        }

        public bool IsBusy
        {
            get
            {
                return (bool)GetValue(IsBusyProperty);
            }
            set
            {
                SetValue(IsBusyProperty, value);
            }
        }

        public PopupLayoutPanel()
        {
            Buttons = new List<FrameworkElement>();
        }

        static PopupLayoutPanel()
        {
            ResetDefaultStyleKey<PopupLayoutPanel>();
        }
    }
}