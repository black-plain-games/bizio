﻿using Bizio.Core.Model.Companies;
using Bizio.Core.Model.People;
using Bizio.Core.Utilities;
using System.Windows;

namespace Bizio.Client.Desktop.Controls.Panels
{
    public class ProspectPanel : BaseControl
    {
        public static readonly DependencyProperty PersonProperty = Register<Person, ProspectPanel>(nameof(Person));

        public static readonly DependencyProperty SalaryProperty = Register<Range<int>, ProspectPanel>(nameof(Salary));

        public static readonly DependencyProperty ProspectSkillsProperty = Register<IEnumerable<ProspectSkill>, ProspectPanel>(nameof(ProspectSkills));

        public static readonly DependencyProperty SkillsProperty = Register<IEnumerable<Skill>, ProspectPanel>(nameof(Skills));

        public static readonly DependencyProperty OfferValueProperty = Register<int?, ProspectPanel>(nameof(OfferValue));

        public static readonly DependencyProperty DisplayAdditionalInformationProperty = Register<bool, ProspectPanel>(nameof(DisplayAdditionalInformation), false);

        public Person Person
        {
            get
            {
                return GetValue<Person>(PersonProperty);
            }
            set
            {
                SetValue(PersonProperty, value);
            }
        }

        public Range<int> Salary
        {
            get
            {
                return GetValue<Range<int>>(SalaryProperty);
            }
            set
            {
                SetValue(SalaryProperty, value);
            }
        }

        public IEnumerable<ProspectSkill> ProspectSkills
        {
            get
            {
                return GetValue<IEnumerable<ProspectSkill>>(ProspectSkillsProperty);
            }
            set
            {
                SetValue(ProspectSkillsProperty, value);
            }
        }

        public IEnumerable<Skill> Skills
        {
            get
            {
                return GetValue<IEnumerable<Skill>>(SkillsProperty);
            }
            set
            {
                SetValue(SkillsProperty, value);
            }
        }

        public int? OfferValue
        {
            get
            {
                return GetValue<int?>(OfferValueProperty);
            }
            set
            {
                SetValue(OfferValueProperty, value);
            }
        }

        public bool DisplayAdditionalInformation
        {
            get
            {
                return GetValue<bool>(DisplayAdditionalInformationProperty);
            }
            set
            {
                SetValue(DisplayAdditionalInformationProperty, value);
            }
        }

        static ProspectPanel()
        {
            ResetDefaultStyleKey<ProspectPanel>();
        }
    }
}