﻿using System.Timers;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Bizio.Client.Desktop.Controls
{
    public abstract class SpriteBase : FrameworkElement, IDisposable
    {
        public static readonly DependencyProperty SourceProperty = DependencyProperty.Register(nameof(Source), typeof(ImageSource), typeof(SpriteBase),
            new FrameworkPropertyMetadata(null,
                FrameworkPropertyMetadataOptions.AffectsArrange |
                FrameworkPropertyMetadataOptions.AffectsMeasure |
                FrameworkPropertyMetadataOptions.AffectsRender));

        public static readonly DependencyProperty RedMaskProperty = DependencyProperty.Register(nameof(RedMask), typeof(byte), typeof(SpriteBase),
            new FrameworkPropertyMetadata(byte.MaxValue,
                FrameworkPropertyMetadataOptions.AffectsRender));

        public static readonly DependencyProperty GreenMaskProperty = DependencyProperty.Register(nameof(GreenMask), typeof(byte), typeof(SpriteBase),
            new FrameworkPropertyMetadata(byte.MaxValue,
                FrameworkPropertyMetadataOptions.AffectsRender));

        public static readonly DependencyProperty BlueMaskProperty = DependencyProperty.Register(nameof(BlueMask), typeof(byte), typeof(SpriteBase),
            new FrameworkPropertyMetadata(byte.MaxValue,
                FrameworkPropertyMetadataOptions.AffectsRender));

        public static readonly DependencyProperty AlphaMaskProperty = DependencyProperty.Register(nameof(AlphaMask), typeof(byte), typeof(SpriteBase),
            new FrameworkPropertyMetadata(byte.MaxValue,
                FrameworkPropertyMetadataOptions.AffectsRender));

        public static readonly DependencyProperty CurrentFrameProperty = DependencyProperty.Register(nameof(CurrentFrame), typeof(int), typeof(SpriteBase),
            new FrameworkPropertyMetadata(0,
                FrameworkPropertyMetadataOptions.AffectsMeasure |
                FrameworkPropertyMetadataOptions.AffectsRender));

        public static readonly DependencyProperty FrameTimeProperty = DependencyProperty.Register(nameof(FrameTime), typeof(double), typeof(SpriteBase),
            new FrameworkPropertyMetadata(0d,
                FrameworkPropertyMetadataOptions.AffectsRender));

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            switch (e.Property.Name)
            {
                case nameof(Source):
                case nameof(RedMask):
                case nameof(GreenMask):
                case nameof(BlueMask):
                case nameof(AlphaMask):
                    InitializeImageBrush();
                    break;
            }
        }

        public ImageSource Source
        {
            get
            {
                return (ImageSource)GetValue(SourceProperty);
            }
            set
            {
                SetValue(SourceProperty, value);
            }
        }

        public byte RedMask
        {
            get
            {
                return (byte)GetValue(RedMaskProperty);
            }
            set
            {
                SetValue(RedMaskProperty, value);
            }
        }

        public byte GreenMask
        {
            get
            {
                return (byte)GetValue(GreenMaskProperty);
            }
            set
            {
                SetValue(GreenMaskProperty, value);
            }
        }

        public byte BlueMask
        {
            get
            {
                return (byte)GetValue(BlueMaskProperty);
            }
            set
            {
                SetValue(BlueMaskProperty, value);
            }
        }

        public byte AlphaMask
        {
            get
            {
                return (byte)GetValue(AlphaMaskProperty);
            }
            set
            {
                SetValue(AlphaMaskProperty, value);
            }
        }

        public int CurrentFrame
        {
            get
            {
                return (int)GetValue(CurrentFrameProperty);
            }
            set
            {
                var targetFrame = value;

                var frameCount = GetFrameCount();

                while (frameCount > 0 &&
                    targetFrame >= frameCount)
                {
                    targetFrame -= frameCount;
                }

                SetValue(CurrentFrameProperty, targetFrame);

                UpdateViewbox();
            }
        }

        public double FrameTime
        {
            get
            {
                return (double)GetValue(FrameTimeProperty);
            }
            set
            {
                SetValue(FrameTimeProperty, value);

                _frameTimer.Stop();

                _frameTimer.Interval = value > 0 ? value : 0;

                if (value > 0)
                {
                    _frameTimer.Start();
                }
            }
        }

        private ImageBrush Image
        {
            get
            {
                if (_image == null)
                {
                    InitializeImageBrush();
                }

                return _image;
            }
        }

        protected SpriteBase()
        {
            _frameTimer = new System.Timers.Timer(1);

            _frameTimer.Elapsed += FrameChanged;
        }

        ~SpriteBase()
        {
            Dispose(false);
        }

        protected void UpdateViewbox()
        {
            _image.Viewbox = GetViewbox();
        }

        protected override void OnInitialized(EventArgs e)
        {
            if (FrameTime > 0)
            {
                _frameTimer.Interval = FrameTime;

                _frameTimer.Start();
            }

            base.OnInitialized(e);
        }

        protected override Size MeasureOverride(Size availableSize)
        {
            var width = availableSize.Width;

            var height = availableSize.Height;

            if (double.IsInfinity(width))
            {
                width = Source.Width;
            }

            if (double.IsInfinity(height))
            {
                height = Source.Height;
            }

            var size = new Size(width, height);

            _geometry = new RectangleGeometry(new Rect(size));

            return size;
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            drawingContext.DrawGeometry(Image, null, _geometry);
        }

        protected virtual Rect GetViewbox()
        {
            return new Rect(0, 0, Source.Width, Source.Height);
        }

        protected virtual void OnFrameChanged(object sender, ElapsedEventArgs e)
        {
        }

        protected abstract int GetFrameCount();

        private void FrameChanged(object sender, ElapsedEventArgs e)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                CurrentFrame++;

                OnFrameChanged(sender, e);
            }));
        }

        private void InitializeImageBrush()
        {
            InitializeImageBrush(Source);
        }

        private void InitializeImageBrush(ImageSource source)
        {
            var output = new WriteableBitmap((BitmapSource)source);

            var stride = output.PixelWidth * (output.Format.BitsPerPixel / 8);

            var pixelCount = output.PixelHeight * stride;

            var pixels = new byte[pixelCount];

            output.CopyPixels(pixels, stride, 0);

            var blueIndex = 0;
            var redIndex = 0;
            var greenIndex = 0;
            var alphaIndex = 0;

            if (output.Format == PixelFormats.Bgra32)
            {
                blueIndex = 0;
                greenIndex = 1;
                redIndex = 2;
                alphaIndex = 3;
            }

            var mask = new double[4];

            mask[redIndex] = (double)RedMask / 255;
            mask[greenIndex] = (double)GreenMask / 255;
            mask[blueIndex] = (double)BlueMask / 255;
            mask[alphaIndex] = (double)AlphaMask / 255;

            for (var index = 0; index < pixelCount; index++)
            {
                var value = (double)pixels[index];

                var maskIndex = index % 4;

                var maskedValue = (byte)(mask[maskIndex] * value);

                pixels[index] = maskedValue;
            }

            output.WritePixels(new Int32Rect(0, 0, (int)output.Width, (int)output.Height), pixels, stride, 0);

            _image = new ImageBrush(output)
            {
                Viewbox = GetViewbox(),
                ViewboxUnits = BrushMappingMode.Absolute
            };
        }

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool isDisposing)
        {
            if (isDisposing)
            {
                if (_frameTimer != null)
                {
                    _frameTimer.Dispose();
                }
            }
        }

        private readonly System.Timers.Timer _frameTimer;

        private ImageBrush _image;

        private Geometry _geometry;
    }
}