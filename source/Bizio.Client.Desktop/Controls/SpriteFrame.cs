﻿using System.Windows;

namespace Bizio.Client.Desktop.Controls
{
    public class SpriteFrame
    {
        public double SourceX { get; set; }

        public double SourceY { get; set; }

        public double? SourceWidth { get; set; }

        public double? SourceHeight { get; set; }

        public int? FrameTime { get; set; }

        public Rect GetViewbox(double? defaultWidth, double? defaultHeight)
        {
            return new Rect(SourceX, SourceY, SourceWidth ?? defaultWidth.Value, SourceHeight ?? defaultHeight.Value);
        }
    }
}