﻿using System.Globalization;

namespace Bizio.Client.Desktop.Converters
{
    public class Binding : System.Windows.Data.Binding
    {
        public static CultureInfo GlobalCulture { get; set; }

        public Binding()
            : base()
        {
            ConverterCulture = GlobalCulture;
        }

        public Binding(string path)
            : base(path)
        {
            ConverterCulture = GlobalCulture;
        }
    }
}