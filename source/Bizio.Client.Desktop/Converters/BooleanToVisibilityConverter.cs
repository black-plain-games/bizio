﻿using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Bizio.Client.Desktop.Converters
{
    public class BooleanToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var parsedValue = (bool?)value;

            parsedValue = ValueConverterHelpers.TryInvert(parsedValue, parameter);

            return parsedValue == true ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var parsedValue = (Visibility)value;

            var result = parsedValue == Visibility.Visible;

            return ValueConverterHelpers.TryInvert(result, parameter);
        }
    }
}
