﻿using Bizio.Core.Model.Companies;
using System.Globalization;
using D = Bizio.Core.Data.Companies;

namespace Bizio.Client.Desktop.Converters
{
    public class CompanyMessageListConverter : BaseConverter
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is not IEnumerable<CompanyMessage> messages)
            {
                return null;
            }

            var status = Utilities.AsEnum<D.CompanyMessageStatus>(parameter);

            return messages.Where(m => m.Status == status);
        }
    }
}
