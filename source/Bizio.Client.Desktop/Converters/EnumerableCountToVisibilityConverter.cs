﻿using System.Collections;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Bizio.Client.Desktop.Converters
{
    public class EnumerableCountToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var collection = (IEnumerable)value;

            var count = 0;

            var enumerator = collection.GetEnumerator();

            while (enumerator.MoveNext())
            {
                count++;
            }

            var comparison = ParseParameter(parameter);

            var isVisible = true;

            switch (comparison.Comparison)
            {
                case ComparisonType.EqualTo:
                    isVisible = count == comparison.Count;
                    break;
                case ComparisonType.GreaterThan:
                    isVisible = count > comparison.Count;
                    break;
                case ComparisonType.GreaterThanOrEqualTo:
                    isVisible = count >= comparison.Count;
                    break;
                case ComparisonType.LessThan:
                    isVisible = count < comparison.Count;
                    break;
                case ComparisonType.LessThanOrEqualTo:
                    isVisible = count <= comparison.Count;
                    break;
                case ComparisonType.NotEqualTo:
                    isVisible = count != comparison.Count;
                    break;
            }

            return isVisible ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        private static Descriptor ParseParameter(object parameter)
        {
            var comparison = $"{parameter}".ToLower();

            var count = 0;

            var comparisonType = ComparisonType.GreaterThan;

            if (comparison.Length < 2 || char.IsDigit(comparison[0]))
            {
                count = int.Parse(comparison);
            }
            else if (char.IsDigit(comparison[1]))
            {
                count = int.Parse(comparison.Substring(1, comparison.Length - 1));

                switch (comparison[0])
                {
                    case 'l':
                        comparisonType = ComparisonType.LessThan;
                        break;
                    case 'g':
                        comparisonType = ComparisonType.GreaterThan;
                        break;
                    case 'e':
                        comparisonType = ComparisonType.EqualTo;
                        break;
                }
            }
            else if (char.IsDigit(comparison[2]))
            {
                count = int.Parse(comparison.Substring(2, comparison.Length - 2));

                var sub = comparison.Substring(0, 2);

                switch (sub)
                {
                    case "le":
                        comparisonType = ComparisonType.LessThanOrEqualTo;
                        break;
                    case "ge":
                        comparisonType = ComparisonType.GreaterThanOrEqualTo;
                        break;
                    case "ne":
                        comparisonType = ComparisonType.NotEqualTo;
                        break;
                }
            }

            return new Descriptor(comparisonType, count);
        }

        private struct Descriptor
        {
            public ComparisonType Comparison;

            public int Count;

            public Descriptor(ComparisonType comparison, int count)
            {
                Comparison = comparison;

                Count = count;
            }
        }

        private enum ComparisonType
        {
            LessThan,
            GreaterThan,
            LessThanOrEqualTo,
            GreaterThanOrEqualTo,
            EqualTo,
            NotEqualTo
        }
    }
}