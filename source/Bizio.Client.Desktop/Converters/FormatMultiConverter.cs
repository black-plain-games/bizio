﻿using System;
using System.Globalization;

namespace Bizio.Client.Desktop.Converters
{
    public class FormatMultiConverter : BaseMultiConverter
    {
        public override object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            return string.Format($"{parameter}", values);
        }
    }
}
