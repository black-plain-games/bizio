﻿namespace Bizio.Client.Desktop.Converters
{
    internal class GameDateDiffConverter : BaseConverter
    {
        public static DateTime? GameStartDate { get; set; }

        public override object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (GameStartDate == null)
            {
                return value;
            }

            if (value is not DateTime date)
            {
                return value;
            }

            int years = Math.Abs(date.Year - GameStartDate.Value.Year);
            int months = Math.Abs(date.Month - GameStartDate.Value.Month);
            int days = Math.Abs(date.Day - GameStartDate.Value.Day);
            int hours = Math.Abs(date.Hour - GameStartDate.Value.Hour);
            int minutes = Math.Abs(date.Minute - GameStartDate.Value.Minute);
            int seconds = Math.Abs(date.Second - GameStartDate.Value.Second);

            // Adjust for negative values
            if (seconds < 0)
            {
                seconds += 60;
                minutes--;
            }
            if (minutes < 0)
            {
                minutes += 60;
                hours--;
            }
            if (hours < 0)
            {
                hours += 24;
                days--;
            }
            if (days < 0)
            {
                var prevMonth = date.AddMonths(-1);
                days += DateTime.DaysInMonth(prevMonth.Year, prevMonth.Month);
                months--;
            }
            if (months < 0)
            {
                months += 12;
                years--;
            }

            var parts = new List<string>();

            var format = parameter as string ?? "YMD,";

            foreach (var symbol in format)
            {
                switch (symbol)
                {
                    case 'Y':
                        parts.Add($"{years} years");
                        break;
                    case 'M':
                        parts.Add($"{months} months");
                        break;
                    case 'D':
                        parts.Add($"{days} days");
                        break;
                    case 'y':
                        parts.Add($"{years}y");
                        break;
                    case 'm':
                        parts.Add($"{months}m");
                        break;
                    case 'd':
                        parts.Add($"{days}d");
                        break;
                }
            }

            var separator = " ";

            if (!KnownSymbols.Contains(format.Last()))
            {
                separator = $"{format.Last()} ";
            }

            return string.Join(separator, parts);
        }

        private static readonly char[] KnownSymbols = ['Y', 'M', 'D', 'y', 'm', 'd'];

        private static readonly DateTime Zero = new(1, 1, 1);
    }
}
