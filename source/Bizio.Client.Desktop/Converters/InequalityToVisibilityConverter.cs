﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Bizio.Client.Desktop.Converters
{
    public class InequalityToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool result;

            if (parameter is int)
            {
                var iValue = int.Parse($"{value}");

                var iParameter = int.Parse($"{parameter}");

                result = iValue != iParameter;
            }
            else
            {
                result = $"{value}" != $"{parameter}";
            }

            return result ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}