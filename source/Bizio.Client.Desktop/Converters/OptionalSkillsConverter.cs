﻿using Bizio.Client.Desktop.Core;
using Bizio.Core.Model.People;
using System.Globalization;
using System.Windows.Data;

namespace Bizio.Client.Desktop.Converters
{
    public class OptionalSkillsConverter : IMultiValueConverter
    {
        public object? Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null || values.Length < 3) return null;

            if (values[0] is not IEnumerable<SkillDefinition> allItems) return null;
            if (values[1] is not IEnumerable<IdValuePair<SkillDefinition, int>> selectedItems) return allItems.OrderBy(i => i.Name);

            var remainingItems = allItems.Where(i => selectedItems.All(si => si.Id != i));

            if (values[2] is not SkillDefinition currentSelectedItem) return remainingItems.OrderBy(i => i.Name);

            return remainingItems.Concat([currentSelectedItem]).OrderBy(i => i.Name);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException("ConvertBack is not implemented.");
        }
    }
}
