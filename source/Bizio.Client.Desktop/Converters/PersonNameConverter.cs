﻿using Bizio.Core.Model.People;
using System.Globalization;

namespace Bizio.Client.Desktop.Converters
{
    public class PersonNameConverter : BaseConverter
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Person person)
            {
                return $"{person.FirstName} {person.LastName}";
            }

            return value;
        }
    }
}