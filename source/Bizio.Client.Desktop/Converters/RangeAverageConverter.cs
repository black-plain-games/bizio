﻿using Bizio.Core.Utilities;
using System.Globalization;
using System.Windows.Data;

namespace Bizio.Client.Desktop.Converters
{
    public class RangeAverageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Range<int>)
            {
                var range = value as Range<int>;

                return (range.Maximum + range.Minimum) / 2;
            }

            if (value is Range<byte>)
            {
                var range = value as Range<byte>;

                return (range.Maximum + range.Minimum) / 2;
            }

            if (value is Range<decimal>)
            {
                var range = value as Range<decimal>;

                return (range.Maximum + range.Minimum) / 2m;
            }

            return 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}