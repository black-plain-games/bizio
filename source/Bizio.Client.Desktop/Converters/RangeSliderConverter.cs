﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace Bizio.Client.Desktop.Converters
{
    public class RangeSliderConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var slider = value as Slider;

            var length = 0.0;

            if (!slider.IsSelectionRangeEnabled)
            {
                return new GridLength(length, GridUnitType.Star);
            }

            switch ($"{parameter}".ToLower())
            {
                case "start":
                    length = slider.SelectionStart - slider.Minimum;
                    break;

                case "end":
                    length = slider.Maximum - slider.SelectionEnd;
                    break;

                case "range":
                    length = slider.SelectionEnd - slider.SelectionStart;
                    break;
            }

            return new GridLength(length, GridUnitType.Star);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}