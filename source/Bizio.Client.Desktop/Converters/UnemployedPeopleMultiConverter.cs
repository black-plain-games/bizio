﻿using Bizio.Core.Model.Companies;
using Bizio.Core.Model.People;
using System.Globalization;
using System.Windows;

namespace Bizio.Client.Desktop.Converters
{
    public class UnemployedPeopleMultiConverter : BaseMultiConverter
    {
        public override object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Length != 2)
            {
                throw new ArgumentException("UnemployedPeopleMultiConverter expects 2 values");
            }

            if (values.Any(v => v == DependencyProperty.UnsetValue))
            {
                return null;
            }

            if (values[0] is not IEnumerable<Person> people || values[1] is not IEnumerable<Prospect> prospects)
            {
                throw new ArgumentException("Must provide a list of people and a list of prospects");
            }

            return people.Except(prospects.Select(p => p.Person));
        }
    }
}