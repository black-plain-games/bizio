﻿namespace Bizio.Client.Desktop.Converters
{
    internal static class ValueConverterHelpers
    {
        public static bool TryInvert(bool? value, object parameter)
        {
            if (IsInverted(parameter))
            {
                return value == false;
            }

            return value == true;
        }

        public static bool IsInverted(object isInvertedParameter)
        {
            return bool.TryParse($"{isInvertedParameter}", out var doInvert) && doInvert;
        }
    }
}