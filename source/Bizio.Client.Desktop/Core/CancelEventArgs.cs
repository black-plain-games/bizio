﻿using System.ComponentModel;

namespace Bizio.Client.Desktop.Core
{
    public class CancelEventArgs<TData> : CancelEventArgs
    {
        public TData Data { get; }

        public CancelEventArgs(TData data)
            : base()
        {
            Data = data;
        }

        public CancelEventArgs(TData data, bool cancel)
            : base(cancel)
        {
            Data = data;
        }
    }
}
