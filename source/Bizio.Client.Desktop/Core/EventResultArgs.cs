﻿namespace Bizio.Client.Desktop.Core
{
    public class EventResultArgs<TArg, TResult>(TArg argument) : EventArgs
    {
        public TArg Argument { get; } = argument;

        public TResult Result { get; set; }

        public bool DoDismiss { get; set; } = true;
    }
}