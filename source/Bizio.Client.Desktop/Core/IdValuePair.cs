﻿using Bizio.Core;
using Bizio.Core.Model;

namespace Bizio.Client.Desktop.Core
{
    public delegate void OnValueChanging<TValue>(object sender, ValueChangingEventArgs<TValue> e);

    public delegate void OnValueChanged<TValue>(object sender, ValueChangedEventArgs<TValue> e);

    public class IdValuePair<TId, TValue> : NotifyOnPropertyChanged, IIdentifiable<TId>
    {
        public TId Id
        {
            get
            {
                return G<TId>();
            }
            set
            {
                var currentValue = G<TId>();

                var args = new ValueChangingEventArgs<TId>(currentValue, value);

                IdChanging?.Invoke(this, args);

                if (!args.IsCancelled)
                {
                    S(value);

                    IdChanged?.Invoke(this, new ValueChangedEventArgs<TId>(currentValue, value));
                }
            }
        }

        public TValue Value
        {
            get
            {
                return G<TValue>();
            }
            set
            {
                var currentValue = G<TValue>();

                var args = new ValueChangingEventArgs<TValue>(currentValue, value);

                ValueChanging?.Invoke(this, args);

                if (!args.IsCancelled)
                {
                    S(value);

                    ValueChanged?.Invoke(this, new ValueChangedEventArgs<TValue>(currentValue, value));
                }
            }
        }

        public event OnValueChanging<TId> IdChanging;

        public event OnValueChanged<TId> IdChanged;

        public event OnValueChanging<TValue> ValueChanging;

        public event OnValueChanged<TValue> ValueChanged;

        public IdValuePair() { }

        public IdValuePair(TId id, TValue value)
        {
            Id = id;

            Value = value;
        }
    }
}