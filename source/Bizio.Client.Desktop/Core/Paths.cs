﻿namespace Bizio.Client.Desktop.Core
{
    public static class Paths
    {
        public const string StaticDataPath = "Data/bizio";
        public const string ConfigurationPath = "Data/config";
        public const string LanguagesPath = "Data/languages";
        public const string SettingsPath = "Data/settings";
        public const string SavedGamesPath = "Data/games";
        public const string NotificationsPath = "Data/notifications";
        public const string Exceptions = "Data/exceptions";
    }
}
