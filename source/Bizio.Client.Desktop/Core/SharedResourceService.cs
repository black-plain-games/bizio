﻿namespace Bizio.Client.Desktop.Core
{
    public interface ISharedResourceService
    {
        T Get<T>(string name);
        void Set<T>(string name, T value);
        void Remove(string name);
        bool Contains(string name);
    }

    internal class SharedResourceService : ISharedResourceService
    {
        public bool Contains(string name) => _resources.ContainsKey(name);

        public T Get<T>(string name) => (T)_resources[name];

        public void Remove(string name) => _resources.Remove(name);

        public void Set<T>(string name, T value) => _resources[name] = value;

        private readonly Dictionary<string, object?> _resources = [];
    }
}
