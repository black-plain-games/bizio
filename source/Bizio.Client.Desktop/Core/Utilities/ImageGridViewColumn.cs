using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;

namespace Bizio.Client.Desktop.Core.Utilities
{
    public abstract class ImageGridViewColumn : GridViewColumn, IValueConverter
    {
        protected ImageGridViewColumn()
            : this(Stretch.None)
        {
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) => GetImageSource(value);

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => throw new NotImplementedException();

        protected ImageGridViewColumn(Stretch imageStretch)
        {
            var imageElement = new FrameworkElementFactory(typeof(Image));

            imageElement.SetBinding(Image.SourceProperty, new Binding
            {
                Converter = this,
                Mode = BindingMode.OneWay
            });

            imageElement.SetBinding(Image.StretchProperty, new Binding
            {
                Source = imageStretch
            });

            CellTemplate = new DataTemplate
            {
                VisualTree = imageElement
            };
        }

        protected abstract ImageSource GetImageSource(object value);
    }
}