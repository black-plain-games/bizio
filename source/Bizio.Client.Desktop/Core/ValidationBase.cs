﻿using Bizio.Core.Model;
using System.ComponentModel;

namespace Bizio.Client.Desktop.Core
{
    public abstract class ValidationBase : NotifyOnPropertyChanged
    {
        public bool HasErrors
        {
            get => G<bool>();
            private set => S(value);
        }

        public IEnumerable<string> Errors => _validationErrors.SelectMany(e => e.Value);

        protected static readonly IEnumerable<string> NoErrors = [];

        protected void CancelOnHasErrors<TArgs>(object sender, CancelEventArgs<TArgs> args)
        {
            if (HasErrors)
            {
                args.Cancel = true;
            }
        }

        protected ValidationBase()
        {
            PropertyChanged += OnValidate;

            MarkAsUnvalidated(nameof(HasErrors));

            MarkAsUnvalidated(nameof(Errors));
        }

        protected virtual IEnumerable<string> Validate(string propertyName) => [];

        protected void MarkAsUnvalidated(string propertyName)
        {
            if (!_unvalidatedProperties.Contains(propertyName))
            {
                _unvalidatedProperties.Add(propertyName);
            }
        }

        private void OnValidate(object? sender, PropertyChangedEventArgs e)
        {
            var propertyName = e.PropertyName ?? string.Empty;

            if (_unvalidatedProperties.Contains(propertyName))
            {
                return;
            }

            var errors = Validate(propertyName);

            if (!_validationErrors.TryAdd(propertyName, errors))
            {
                _validationErrors[propertyName] = errors;
            }

            HasErrors = _validationErrors.Values.Any(v => v.Any());

            OnPropertyChanged(nameof(Errors));
        }

        protected readonly Dictionary<string, IEnumerable<string>> _validationErrors = [];

        private readonly ICollection<string> _unvalidatedProperties = [];
    }
}
