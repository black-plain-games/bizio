﻿namespace Bizio.Client.Desktop.Core
{
    public class ValueChangedEventArgs<TValue> : EventArgs
    {
        public TValue PreviousValue { get; }

        public TValue CurrentValue { get; }

        public ValueChangedEventArgs(TValue previousValue, TValue currentValue)
        {
            PreviousValue = previousValue;

            CurrentValue = currentValue;
        }
    }
}