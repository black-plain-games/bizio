﻿using Bizio.Client.Desktop.ViewModels;
using Bizio.Core;
using System.Windows;
using System.Windows.Input;

namespace Bizio.Client.Desktop
{
    public partial class MainWindow : Window
    {
        public MainWindow(IMainViewModel mainViewModel)
        {
            InitializeComponent();

            UIManager.SetTaskScheduler();

            var tb = FindResource("DefaultCursor") as FrameworkElement;

            Mouse.OverrideCursor = tb.Cursor;

            DataContext = mainViewModel;
        }
    }
}