﻿using Bizio.Client.Desktop.Core;
using Bizio.Core.Model;
using Bizio.Core.Model.People;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;
using P = Bizio.Core.Data.People;

namespace Bizio.Client.Desktop.Models
{
    public class CreatePersonData : NotifyOnPropertyChanged
    {
        public event EventHandler? DataChanged;

        public string FirstName { get => G<string>(); set => S2(value); }
        public string LastName { get => G<string>(); set => S2(value); }
        public DateTime Birthday { get => G<DateTime>(); set => S2(value); }
        public P.Gender Gender { get => G<P.Gender>(); set => S2(value); }
        public Personality Personality { get => G<Personality>(); set => S2(value); }

        public ObservableCollection<IdValuePair<SkillDefinition, int>> MandatorySkills { get; } = [];
        public ObservableCollection<IdValuePair<SkillDefinition, int>> OptionalSkills { get; } = [];

        public CreatePersonData()
        {
            MandatorySkills.CollectionChanged += (s, e) => DataChanged?.Invoke(this, EventArgs.Empty);
            OptionalSkills.CollectionChanged += (s, e) => DataChanged?.Invoke(this, EventArgs.Empty);
        }

        private void S2(object value, [CallerMemberName] string property = "")
        {
            S(value, true, property);
            DataChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}
