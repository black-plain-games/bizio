﻿namespace Bizio.Client.Desktop.Models
{
    public record DifficultyLevel(string Name, int MaxOptionalSkillCount, int MaxTotalSkillPoints, int InitialFunds);
}
