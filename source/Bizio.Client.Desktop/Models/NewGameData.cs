﻿using Bizio.Core.Model;
using Bizio.Core.Model.Companies;
using System.Runtime.CompilerServices;

namespace Bizio.Client.Desktop.Models
{
    public class NewGameData : NotifyOnPropertyChanged
    {
        public event EventHandler? DataChanged;

        public DateTime StartDate { get => G<DateTime>(); set => S2(value); }
        public int DaysPerTurn { get => G<int>(); set => S2(value); }
        public string CompanyName { get => G<string>(); set => S2(value); }
        public int NumberOfCompanies { get => G<int>(); set => S2(value); }
        public Industry Industry { get => G<Industry>(); set => S2(value); }
        public int InitialFunds { get => G<int>(); set => S2(value); }

        public CreatePersonData Founder
        {
            get => G<CreatePersonData>();
            set
            {
                var previous = G<CreatePersonData>();

                if (previous != null)
                {
                    previous.DataChanged -= DataChanged;
                }

                S2(value);
                value.DataChanged += DataChanged;
            }
        }

        private void S2(object value, [CallerMemberName] string property = "")
        {
            S(value, true, property);
            DataChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}
