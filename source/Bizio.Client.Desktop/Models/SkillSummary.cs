﻿using Bizio.Core.Model;
using Bizio.Core.Model.People;

namespace Bizio.Client.Desktop.Models
{
    public class SkillSummary : NotifyOnPropertyChanged
    {
        public SkillDefinition Skill { get => G<SkillDefinition>(); set => S(value); }

        public int Total { get => G<int>(); set => S(value); }
        public int Count { get => G<int>(); set => S(value); }
        public Person BestPerson { get => G<Person>(); set => S(value); }
        public int BestPersonAmount { get => G<int>(); set => S(value); }
    }
}
