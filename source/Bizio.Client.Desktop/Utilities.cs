﻿using Bizio.Client.Desktop.ViewModels;
using Bizio.Client.Desktop.ViewModels.Popups;
using Bizio.Client.Desktop.Views.Popups;

namespace Bizio.Client.Desktop
{
    public static class Utilities
    {
        public static void CreateMessagePopup(IRouter router, string title, string message)
        {
            var vm = new MessagePopupViewModel(router)
            {
                Title = title,
                Message = message
            };

            vm.AddOption("OK");

            var view = new MessagePopupView
            {
                DataContext = vm
            };

            router.ShowPopup(view);
        }

        public static void CreateMessagePopup(IRouter router, string title, string message, params (string Label, Action? Action)[] options)
        {
            var vm = new MessagePopupViewModel(router)
            {
                Title = title,
                Message = message
            };

            foreach (var option in options)
            {
                if (option.Action == null)
                {
                    vm.AddOption(option.Label);
                }
                else
                {
                    vm.AddOption(option.Label, (s, e) => option.Action());
                }
            }

            var view = new MessagePopupView
            {
                DataContext = vm
            };

            router.ShowPopup(view);
        }

        public static TEnum AsEnum<TEnum>(this object value) => (TEnum)Enum.Parse(typeof(TEnum), $"{value}");
    }
}
