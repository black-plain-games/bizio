﻿using Bizio.Client.Desktop.Converters;
using Bizio.Client.Desktop.Core;
using Bizio.Client.Desktop.Views;
using Bizio.Core;
using Bizio.Core.Model.Companies;
using Bizio.Core.Model.Game;
using BlackPlain.Bizio.Services;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels
{
    public enum Panel
    {
        None = 0,
        People = 1,
        UserCompany = 2,
        OtherCompanies = 3,
        Projects = 4,
        Store = 5,
        Mailbox = 6
    }

    public interface IGameViewModel
    {
        Game Game { get; }
        int UserRank { get; }
        Company UserCompany { get; }
        IDictionary<Panel, int> PanelUpdates { get; }
        Panel VisiblePanel { get; }

        ICommand TryQuitCommand { get; }
        ICommand SaveGameCommand { get; }
        ICommand NextTurnCommand { get; }
        ICommand TogglePanelCommand { get; }

        IPeoplePanelViewModel PeoplePanelViewModel { get; }
        IMailboxPanelViewModel MailboxPanelViewModel { get; }
        IUserCompanyPanelViewModel UserCompanyPanelViewModel { get; }
    }

    internal class GameViewModel(
        ISharedResourceService sharedResourceService,
        IConfigurationService configurationService,
        IGameService gameService,
        IRouter router,
        ITurnService turnService,
        IPeoplePanelViewModel peoplePanelViewModel,
        IMailboxPanelViewModel mailboxPanelViewModel,
        IUserCompanyPanelViewModel userCompanyPanelViewModel
        ) : ViewModelBase, IGameViewModel
    {
        public Game Game { get => G<Game>(); set => S(value); }
        public int UserRank => Game?.Companies.OrderByDescending(c => c.Money).ToList().IndexOf(UserCompany) + 1 ?? 0;
        public Company UserCompany { get => G<Company>(); set => S(value); }
        public IDictionary<Panel, int> PanelUpdates { get => G<IDictionary<Panel, int>>(); set => S(value); }
        public Panel VisiblePanel { get => G<Panel>(); set => S(value); }

        public ICommand TryQuitCommand => GetCommand(TryQuit);
        public ICommand NextTurnCommand => GetCommand(NextTurn);
        public ICommand SaveGameCommand => GetCommand(SaveGame);
        public ICommand TogglePanelCommand => GetCommand(TogglePanel);

        public IPeoplePanelViewModel PeoplePanelViewModel { get => G<IPeoplePanelViewModel>(); set => S(value); }
        public IMailboxPanelViewModel MailboxPanelViewModel { get => G<IMailboxPanelViewModel>(); set => S(value); }
        public IUserCompanyPanelViewModel UserCompanyPanelViewModel { get => G<IUserCompanyPanelViewModel>(); set => S(value); }

        protected override void LoadViewModel(object? state)
        {
            configurationService.LoadAsync(default);

            Game = sharedResourceService.Get<Game>("game");
            GameDateDiffConverter.GameStartDate = Game.StartDate;
            UserCompany = Game.Companies.First(c => c.UserId.HasValue);

            // REMOVE THIS EVENTUALLY
            foreach (var action in UserCompany.Actions)
            {
                action.Count += 3;
            }

            OnPropertyChanged(nameof(UserRank));

            VisiblePanel = Panel.None;

            PanelUpdates = new Dictionary<Panel, int>
            {
                { Panel.People, 0 },
                { Panel.UserCompany, 0 },
                { Panel.OtherCompanies, 0 },
                { Panel.Projects, 0 },
                { Panel.Store, 0 },
                { Panel.Mailbox, 0 },
            };

            PeoplePanelViewModel = peoplePanelViewModel;
            MailboxPanelViewModel = mailboxPanelViewModel;
            UserCompanyPanelViewModel = userCompanyPanelViewModel;

            foreach (var panel in PanelViewModels)
            {
                panel.Game = Game;
                panel.UserCompany = UserCompany;
                panel.PanelClosed += OnPanelClosed;
            }

            UpdatePanels();
        }

        private void OnPanelClosed(object? sender, EventArgs args)
        {
            VisiblePanel = Panel.None;
        }

        private void TryQuit()
        {
            Utilities.CreateMessagePopup(router,
                "Confirm Quit",
                "Are you sure you want quit?",
                [
                    ("No", null),
                    ("Yes", () =>
                    {
                        sharedResourceService.Remove("game");
                        router.NavigateTo<MainMenuView>();
                    })
                ]);
        }

        private void NextTurn()
        {
            ExecuteSafely(async () =>
            {
                await UIManager.ExecuteAsync(() =>
                {
                    turnService.ProcessTurn(Game);
                });

                UpdatePanels();
            });
        }

        private void UpdatePanels()
        {
            foreach (var panel in PanelViewModels)
            {
                panel.OnTurnProcessed();
            }
        }

        private void SaveGame()
        {
            ExecuteSafely(async () =>
            {
                await gameService.SaveAsync(Game, default);
            });
        }

        private async void ExecuteSafely(Func<Task> action)
        {
            IsBusy = true;

            await Task.Factory.StartNew(async () =>
            {
                try
                {
                    await action();
                    await Task.Delay(200);
                }
                catch (Exception err)
                {
                    await UIManager.ExecuteAsync(() =>
                    {
                        Utilities.CreateMessagePopup(router, "Exception Raised!", err.Message, [("Okay", null)]);
                    });
                }

                IsBusy = false;
            });
        }

        private void TogglePanel(object? state)
        {
            var panel = state.AsEnum<Panel>();

            if (VisiblePanel == panel)
            {
                VisiblePanel = (int)Panel.None;
            }
            else
            {
                VisiblePanel = panel;
            }
        }

        private readonly IPanelViewModel[] PanelViewModels = [peoplePanelViewModel, mailboxPanelViewModel, userCompanyPanelViewModel];
    }
}
