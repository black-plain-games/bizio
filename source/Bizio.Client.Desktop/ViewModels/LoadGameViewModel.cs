﻿using Bizio.Client.Desktop.Core;
using Bizio.Client.Desktop.Views;
using Bizio.Core.Model.Game;
using BlackPlain.Bizio.Services;
using System.Windows.Input;
using D = Bizio.Core.Data.Game;

namespace Bizio.Client.Desktop.ViewModels
{
    public interface ILoadGameViewModel : IViewModelBase
    {
        IEnumerable<GameMetadata> Games { get; }

        GameMetadata? SelectedGame { get; }
        GameMetadata? SelectedGameInProgress { get; }
        GameMetadata? SelectedGameCompleted { get; }

        ICommand LoadGameCommand { get; }
        ICommand BackCommand { get; }
    }

    public class LoadGameViewModel(
        IRouter router,
        IStaticDataService staticDataService,
        IGameService gameService,
        ISharedResourceService sharedResourceService
        ) : ViewModelBase, ILoadGameViewModel
    {
        public IEnumerable<GameMetadata> Games { get => G<IEnumerable<GameMetadata>>(); set => S(value); }

        public GameMetadata? SelectedGame { get => G<GameMetadata>(); set => S(value); }

        public GameMetadata? SelectedGameInProgress
        {
            get => G<GameMetadata>();
            set
            {
                S(value);
                SelectedGame = value ?? SelectedGameCompleted;
                OnPropertyChanged(nameof(LoadGameCommand));
            }
        }

        public GameMetadata? SelectedGameCompleted
        {
            get => G<GameMetadata>();
            set
            {
                S(value);
                SelectedGame = value ?? SelectedGameInProgress;
            }
        }

        public ICommand LoadGameCommand => GetCommand(LoadGame, CanLoadGame);
        public ICommand BackCommand => GetCommand(Back);

        protected override async void LoadViewModel(object? state)
        {
            await staticDataService.LoadAsync(default);

            Games = await gameService.LoadAsync(default);

            SelectedGame = Games.FirstOrDefault();

            SelectedGameCompleted = null;
        }

        private void Back() => router.NavigateTo<MainMenuView>();

        private async void LoadGame()
        {
            var game = await gameService.LoadAsync(SelectedGameInProgress!.Id, default);

            sharedResourceService.Set("game", game);

            router.NavigateTo<GameView>();
        }

        private bool CanLoadGame()
        {
            return SelectedGameInProgress?.Status == D.GameStatus.Playing;
        }
    }
}