﻿using Bizio.Core.Model.Companies;
using System.Windows.Input;
using D = Bizio.Core.Data.Companies;

namespace Bizio.Client.Desktop.ViewModels
{
    public interface IMailboxPanelViewModel : IPanelViewModel
    {
        Company UserCompany { get; set; }

        CompanyMessage? SelectedInboxMessage { get; }
        CompanyMessage? SelectedReadMessage { get; }

        ICommand ReadMessageCommand { get; }
        ICommand DeleteMessageCommand { get; }
        ICommand UnreadMessageCommand { get; }
        ICommand ReadAllMessagesCommand { get; }
        ICommand UnreadAllMessagesCommand { get; }
        ICommand DeleteAllMessagesCommand { get; }
    }

    internal class MailboxPanelViewModel : PanelViewModel, IMailboxPanelViewModel
    {
        public Company UserCompany { get => G<Company>(); set => S(value); }

        public CompanyMessage? SelectedInboxMessage { get => G<CompanyMessage>(); set => S(value); }
        public CompanyMessage? SelectedReadMessage { get => G<CompanyMessage>(); set => S(value); }

        public ICommand ReadMessageCommand => GetCommand(o => SetMessageStatus(o as CompanyMessage, D.CompanyMessageStatus.Read));
        public ICommand DeleteMessageCommand => GetCommand(o => SetMessageStatus(o as CompanyMessage, D.CompanyMessageStatus.Deleted));
        public ICommand UnreadMessageCommand => GetCommand(o => SetMessageStatus(o as CompanyMessage, D.CompanyMessageStatus.UnRead));

        public ICommand ReadAllMessagesCommand => GetCommand(() => ChangeMessageStatuses(D.CompanyMessageStatus.UnRead, D.CompanyMessageStatus.Read));
        public ICommand UnreadAllMessagesCommand => GetCommand(() => ChangeMessageStatuses(D.CompanyMessageStatus.Read, D.CompanyMessageStatus.UnRead));
        public ICommand DeleteAllMessagesCommand => GetCommand(() => ChangeMessageStatuses(D.CompanyMessageStatus.Read, D.CompanyMessageStatus.Deleted));

        private void ChangeMessageStatuses(D.CompanyMessageStatus previous, D.CompanyMessageStatus next)
        {
            foreach (var message in UserCompany.Messages)
            {
                if (message.Status == previous)
                {
                    message.Status = next;
                }
            }

            UserCompany.OnPropertyChanged(nameof(UserCompany.Messages));
        }

        private void SetMessageStatus(CompanyMessage? message, D.CompanyMessageStatus status)
        {
            if (message == null)
            {
                return;
            }

            message.Status = status;
            UserCompany.OnPropertyChanged(nameof(UserCompany.Messages));
        }

        protected override void ClosePanel()
        {
            if (SelectedInboxMessage != null || SelectedReadMessage != null)
            {
                SelectedInboxMessage = null;
                SelectedReadMessage = null;
            }
            else
            {
                base.ClosePanel();
            }
        }


    }
}
