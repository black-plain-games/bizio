﻿using Bizio.Client.Desktop.Views;
using System.Windows;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels
{
    public interface IMainMenuViewModel : IViewModelBase
    {
        ICommand NewGameCommand { get; }
        ICommand LoadGameCommand { get; }
        ICommand ViewSettingsCommand { get; }
        ICommand ViewNotificationsCommand { get; }
        ICommand QuitCommand { get; }
        int UnreadNotificationsCount { get; }
    }

    public class MainMenuViewModel(IRouter router) : ViewModelBase, IMainMenuViewModel
    {
        public ICommand NewGameCommand => GetCommand(CreateNewGame);

        public ICommand LoadGameCommand => GetCommand(LoadGame);

        public ICommand ViewSettingsCommand => GetCommand(ViewSettings);

        public ICommand ViewNotificationsCommand => GetCommand(ViewNotifications);

        public ICommand QuitCommand => GetCommand(Quit);

        public int UnreadNotificationsCount { get => G<int>(); set => S(value); }

        private void CreateNewGame() => router.NavigateTo<NewGameView>();

        private void LoadGame() => router.NavigateTo<LoadGameView>();

        private void ViewSettings()
        {
        }

        private void ViewNotifications()
        {
        }

        private void Quit()
        {
            Application.Current.Shutdown();

            //if (_viewStateService.CurrentPopup != null)
            //{
            //    return;
            //}

            //var viewModel = Utilities.CreateMessagePopup(_viewStateService, UiText.ConfirmQuit, UiText.ConfirmQuitDetails, UiText.No);

            //viewModel.AddOption(UiText.Yes, (s, e) => UIManager.TryInvokeOnUIThread(() => _application.Shutdown()));
        }
    }
}
