﻿using Bizio.Client.Desktop.ViewModels.Popups;
using Bizio.Client.Desktop.Views;
using Bizio.Client.Desktop.Views.Popups;
using Bizio.Core;
using Microsoft.Extensions.DependencyInjection;
using System.Windows;

namespace Bizio.Client.Desktop.ViewModels
{
    public interface IRouter
    {
        void NavigateTo<TView>() where TView : FrameworkElement;
        void ShowPopup(FrameworkElement view);
        void ShowPopup(FrameworkElement view, HorizontalAlignment horizontalAlignment, VerticalAlignment verticalAlignment);
        void ShowPopup<TView>() where TView : FrameworkElement;
        void ShowPopup<TView>(HorizontalAlignment horizontalAlignment, VerticalAlignment verticalAlignment) where TView : FrameworkElement;
        void DismissPopup();
    }

    public interface IMainViewModel : IViewModelBase, IRouter
    {
        FrameworkElement? CurrentView { get; }
        FrameworkElement? CurrentPopup { get; }
    }

    public class MainViewModel(IServiceProvider serviceProvider) : ViewModelBase, IMainViewModel
    {
        public FrameworkElement? CurrentView { get => G<FrameworkElement>(); set => S(value); }

        public FrameworkElement? CurrentPopup => _popups.Count > 0 ? _popups.Peek() : null;

        public void NavigateTo<TView>() where TView : FrameworkElement
        {
            UIManager.ExecuteAsync(() =>
            {
                CurrentView = serviceProvider.GetRequiredService<TView>();
            });
        }

        public void ShowPopup(FrameworkElement view)
        {
            ShowPopup(view, HorizontalAlignment.Center, VerticalAlignment.Center);
        }

        public void ShowPopup(FrameworkElement view, HorizontalAlignment horizontalAlignment, VerticalAlignment verticalAlignment)
        {
            _popups.Push(new PopupPresenterView
            {
                DataContext = new PopupPresenterViewModel(this, view)
                {
                    HorizontalAlignment = horizontalAlignment,
                    VerticalAlignment = verticalAlignment
                }
            });

            OnPropertyChanged(nameof(CurrentPopup));
        }

        public void ShowPopup<TView>() where TView : FrameworkElement
        {
            ShowPopup<TView>(HorizontalAlignment.Center, VerticalAlignment.Center);
        }

        public void ShowPopup<TView>(HorizontalAlignment horizontalAlignment, VerticalAlignment verticalAlignment) where TView : FrameworkElement
        {
            var view = serviceProvider.GetRequiredService<TView>();

            ShowPopup(view, horizontalAlignment, verticalAlignment);
        }

        public void DismissPopup()
        {
            _popups.Pop();
            OnPropertyChanged(nameof(CurrentPopup));
        }

        protected override void LoadViewModel(object? state)
        {
            UIManager.ExecuteAsync(NavigateTo<MainMenuView>);

        }

        private readonly Stack<FrameworkElement> _popups = [];
    }
}
