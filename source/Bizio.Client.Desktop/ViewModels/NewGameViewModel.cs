﻿using Bizio.Client.Desktop.Core;
using Bizio.Client.Desktop.Models;
using Bizio.Client.Desktop.Views;
using Bizio.Core.Data.Configuration;
using Bizio.Core.Model;
using Bizio.Core.Model.People;
using Bizio.Core.Services;
using Bizio.Core.Utilities;
using BlackPlain.Bizio.Services;
using System.Collections.ObjectModel;
using System.Windows.Input;
using P = Bizio.Core.Data.People;

namespace Bizio.Client.Desktop.ViewModels
{
    public interface INewGameViewModel : IViewModelBase
    {
        IEnumerable<DifficultyLevel> DifficultyLevels { get; }
        IEnumerable<P.Gender> Genders { get; }
        ObservableCollection<string> Errors { get; }

        DifficultyLevel? SelectedDifficultyLevel { get; }
        NewGameData? Data { get; }
        StaticData? StaticData { get; }
        int CurrentStepNumber { get; }
        string? HelpText { get; }
        int RemainingSkillPoints { get; }

        bool IsNavigateToFounderSettingsEnabled { get; }

        ICommand ChangeHelpTextCommand { get; }
        ICommand BackCommand { get; }
        ICommand ContinueCommand { get; }
        ICommand StartNewGameCommand { get; }
        ICommand IncreaseSkillValueCommand { get; }
        ICommand DecreaseSkillValueCommand { get; }
    }

    public class NewGameViewModel(
        IRouter router,
        IStaticDataService staticDataService,
        IGameService gameService,
        IConfigurationService configurationService,
        ISharedResourceService sharedResourceService
        ) : ViewModelBase, INewGameViewModel
    {
        public IEnumerable<DifficultyLevel> DifficultyLevels { get; } =
        [
            new DifficultyLevel("Easy", 5, 250, 100000),
            new DifficultyLevel("Normal", 4, 200, 75000),
            new DifficultyLevel("Hard", 3, 150, 50000),
            new DifficultyLevel("Very Hard", 2, 100, 25000)
        ];

        public IEnumerable<P.Gender> Genders { get; } = [P.Gender.Male, P.Gender.Female];

        public ObservableCollection<string> Errors { get; } = [];

        public DifficultyLevel? SelectedDifficultyLevel
        {
            get => G<DifficultyLevel>();
            set
            {
                S(value);
                ResetFounderSkills();
            }
        }
        public NewGameData? Data { get => G<NewGameData>(); set => S(value); }
        public StaticData? StaticData { get => G<StaticData>(); set => S(value); }
        public int CurrentStepNumber { get => G<int>(); set => S(value); }
        public string? HelpText { get => G<string>(); set => S(value); }
        public int RemainingSkillPoints { get => G<int>(); set => S(value); }

        public bool IsNavigateToFounderSettingsEnabled => CanNavigateToFounderSettings();

        public ICommand ChangeHelpTextCommand => GetCommand(ChangeHelpText);
        public ICommand BackCommand => GetCommand(Back);
        public ICommand ContinueCommand => GetCommand(Continue, CanContinue);
        public ICommand StartNewGameCommand => GetCommand(StartNewGameAsync);
        public ICommand IncreaseSkillValueCommand => GetCommand(IncreaseSkillValue);
        public ICommand DecreaseSkillValueCommand => GetCommand(DecreaseSkillValue);

        protected override async void LoadViewModel(object? state)
        {
            StaticData = await staticDataService.LoadAsync(true, default);

            Data = new NewGameData
            {
                StartDate = DateTime.Today,
                DaysPerTurn = 1,
                CompanyName = "New Company",
                NumberOfCompanies = 3,
                InitialFunds = 0,
                Industry = null,
                Founder = new CreatePersonData
                {
                    FirstName = "John",
                    LastName = "Smith",
                    Birthday = DateTime.Today.AddYears(-22),
                    Gender = P.Gender.Male,
                    Personality = StaticData.Personalities.First()
                }
            };

            Data.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName == nameof(Data.Industry))
                {
                    ResetFounderSkills();
                };
            };

            Data.DataChanged += (s, e) =>
            {
                OnPropertyChanged(nameof(IsNavigateToFounderSettingsEnabled));
                OnPropertyChanged(nameof(ContinueCommand));
            };

            CurrentStepNumber = 0;
        }

        #region Command Handlers

        private void ChangeHelpText(object? obj)
        {
            _helpText.TryGetValue($"{obj}", out var text);

            HelpText = text;
        }

        private void Back()
        {
            switch (CurrentStepNumber)
            {
                case 0:
                    router.NavigateTo<MainMenuView>();
                    break;
                case 1:
                    CurrentStepNumber--;
                    break;
            }

            OnPropertyChanged(nameof(ContinueCommand));
        }

        private void Continue()
        {
            switch (CurrentStepNumber)
            {
                case 0:
                    CurrentStepNumber++;
                    break;
                case 1:
                    ConfirmStartNewGame();
                    break;
            }

            OnPropertyChanged(nameof(ContinueCommand));
        }

        private bool CanContinue(object? data)
        {
            if (CurrentStepNumber == 0)
            {
                return CanNavigateToFounderSettings();
            }

            return CanStartNewGame();
        }

        private async void StartNewGameAsync()
        {
            var personality = StaticData.Personalities.Random();

            var skills = Data.Founder.OptionalSkills.Concat(Data.Founder.MandatorySkills).ToDictionary(s => s.Id.Id, s => s.Value);

            var founderParameters = new CreatePersonParameters(Data.Founder.FirstName, Data.Founder.LastName, Data.Founder.Gender, Data.Founder.Birthday, personality.Id, skills);

            var configuration = new Dictionary<ConfigurationKey, string>();

            var createGameParameters = new CreateGameParameters(Guid.NewGuid(), Data.StartDate, Data.Industry.Id, Data.DaysPerTurn, founderParameters, Data.CompanyName, Data.InitialFunds, Data.NumberOfCompanies, configuration);

            await configurationService.LoadAsync(default);

            var gameId = await gameService.CreateAsync(createGameParameters, default);

            var game = await gameService.LoadAsync(gameId, default);

            sharedResourceService.Set("game", game);

            router.NavigateTo<GameView>();
        }

        private void IncreaseSkillValue(object? obj)
        {
            if (obj is IdValuePair<SkillDefinition, int> skillValuePair)
            {
                skillValuePair.Value++;
            }
        }

        private void DecreaseSkillValue(object? obj)
        {
            if (obj is IdValuePair<SkillDefinition, int> skillValuePair)
            {
                skillValuePair.Value--;
            }
        }

        #endregion

        #region Helpers

        private bool CanNavigateToFounderSettings()
        {
            if (Data == null) return false;
            if (Data.InitialFunds <= 0) return false;
            if (Data.NumberOfCompanies <= 0) return false;
            if (Data.Industry == null) return false;
            if (Data.StartDate == default) return false;
            if (string.IsNullOrWhiteSpace(Data.CompanyName)) return false;
            if (Data.DaysPerTurn <= 0) return false;
            if (SelectedDifficultyLevel == null) return false;

            return true;
        }

        private void ResetFounderSkills()
        {
            Data.Founder.MandatorySkills.Clear();
            Data.Founder.OptionalSkills.Clear();

            RemainingSkillPoints = SelectedDifficultyLevel?.MaxTotalSkillPoints ?? 0;
            Data.InitialFunds = SelectedDifficultyLevel?.InitialFunds ?? 0;

            if (Data.Industry != null)
            {
                foreach (var skillDefinition in Data.Industry.MandatorySkills)
                {
                    Data.Founder.MandatorySkills.Add(CreateSkillValuePair(skillDefinition));
                }

                var optionalSkillCount = SelectedDifficultyLevel?.MaxOptionalSkillCount ?? 0;

                while (Data.Founder.OptionalSkills.Count < optionalSkillCount)
                {
                    Data.Founder.OptionalSkills.Add(CreateSkillValuePair(null));
                }

                OnPropertyChanged(nameof(IsNavigateToFounderSettingsEnabled));
                OnPropertyChanged(nameof(ContinueCommand));
            }
        }

        private IdValuePair<SkillDefinition, int> CreateSkillValuePair(SkillDefinition skillDefinition)
        {
            var skill = new IdValuePair<SkillDefinition, int>(skillDefinition, 0);

            skill.IdChanged += (s, e) =>
            {
                OnPropertyChanged(nameof(ContinueCommand));
                Data.Industry.OnPropertyChanged(nameof(Data.Industry.OptionalSkills));
            };

            skill.ValueChanging += (s, e) =>
            {
                if (e.NewValue < 0)
                {
                    e.IsCancelled = true;
                    return;
                }

                var delta = e.NewValue - e.OriginalValue;
                if (RemainingSkillPoints - delta < 0)
                {
                    e.IsCancelled = true;
                }
            };

            skill.ValueChanged += (s, e) =>
            {
                var delta = e.CurrentValue - e.PreviousValue;
                RemainingSkillPoints -= delta;
            };

            return skill;
        }

        private void ConfirmStartNewGame()
        {
            Utilities.CreateMessagePopup(router, "Confirm New Game", "Are you sure you want to start a new game?", [("No", null), ("Yes", StartNewGameAsync)]);
        }

        private bool CanStartNewGame()
        {
            if (!CanNavigateToFounderSettings()) return false;

            if (Data.Founder.OptionalSkills.Any(s => s.Id == null))
            {
                return false;
            }

            return true;
        }

        #endregion

        private readonly Dictionary<string, string> _helpText = new()
        {
            { "GameName", "Something helpful about game name" },
            { "StartDate", "Something helpful about start date" },
            { "TurnSpeed", "Something helpful about turn speed" },
            { "CompanyName", "Something helpful about company name" },
            { "NumberOfCompanies", "Something helpful about number of companies" },
            { "Industry", "Something helpful about industry" },
            { "Difficulty", "Something helpful about difficulty" },
            { "InitialFunds", "Something helpful about initial funds" }
        };
    }
}
