﻿using Microsoft.Extensions.DependencyInjection;

namespace Bizio.Client.Desktop.ViewModels
{
    public static class Packages
    {
        public static IServiceCollection RegisterViewModels(this IServiceCollection services)
        {
            return services
                .AddSingleton<IMainViewModel, MainViewModel>()
                .AddSingleton<IRouter>(p => p.GetRequiredService<IMainViewModel>())
                .AddTransient<IMainMenuViewModel, MainMenuViewModel>()
                .AddTransient<INewGameViewModel, NewGameViewModel>()
                .AddTransient<ILoadGameViewModel, LoadGameViewModel>()
                .AddTransient<IGameViewModel, GameViewModel>()
                .AddTransient<IPeoplePanelViewModel, PeoplePanelViewModel>()
                .AddTransient<IMailboxPanelViewModel, MailboxPanelViewModel>()
                .AddTransient<IUserCompanyPanelViewModel, UserCompanyPanelViewModel>();
        }
    }
}
