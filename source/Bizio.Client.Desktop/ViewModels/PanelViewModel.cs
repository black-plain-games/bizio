﻿using Bizio.Core.Model;
using Bizio.Core.Model.Companies;
using Bizio.Core.Model.Game;
using System.Windows.Input;
using DA = Bizio.Core.Data.Actions;

namespace Bizio.Client.Desktop.ViewModels
{
    public interface IPanelViewModel
    {
        event EventHandler? PanelClosed;
        Game Game { get; set; }
        Company UserCompany { get; set; }
        ObservableDictionary<DA.ActionType, int> RemainingTurnActionCount { get; }
        ICommand ClosePanelCommand { get; }
        void OnTurnProcessed();
    }

    public abstract class PanelViewModel : ViewModelBase, IPanelViewModel
    {
        public event EventHandler? PanelClosed;

        public Game Game { get => G<Game>(); set => S(value); }

        public Company UserCompany { get => G<Company>(); set => S(value); }

        public ObservableDictionary<DA.ActionType, int> RemainingTurnActionCount { get; } = [];

        public ICommand ClosePanelCommand => GetCommand(ClosePanel);

        protected virtual void ClosePanel()
        {
            PanelClosed?.Invoke(this, EventArgs.Empty);
        }

        public virtual void OnTurnProcessed()
        {
            foreach (var action in UserCompany.Actions)
            {
                RemainingTurnActionCount[action.Action.Type] = action.Count;
            }
        }
    }
}
