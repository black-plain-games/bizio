﻿using Bizio.Client.Desktop.ViewModels.Popups;
using Bizio.Client.Desktop.Views.Popups;
using Bizio.Core;
using Bizio.Core.Data.Actions;
using Bizio.Core.Model.Companies;
using Bizio.Core.Model.People;
using BlackPlain.Bizio.Services.Actions.Data;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels
{
    public interface IPeoplePanelViewModel : IPanelViewModel
    {
        Person? SelectedPerson { get; }
        Prospect? SelectedProspect { get; }

        bool HasMadeOfferToSelectedPerson { get; }
        bool HasInterviewedSelectedPerson { get; }

        bool HasMadeOfferToSelectedProspect { get; }
        bool HasInterviewedSelectedProspect { get; }

        int? SelectedPersonOfferValue { get; }
        int? SelectedProspectOfferValue { get; }

        ICommand DraftOfferCommand { get; }
        ICommand CancelOfferCommand { get; }

        ICommand InterviewCommand { get; }
        ICommand CancelInterviewCommand { get; }
    }

    internal class PeoplePanelViewModel(IRouter router) : PanelViewModel, IPeoplePanelViewModel
    {
        public Person? SelectedPerson
        {
            get => G<Person>();
            set
            {
                S(value);
                HasInterviewedSelectedPerson = value != null && Interviews.TryGetValue(value, out var interview) && interview;
                if (value != null && Offers.TryGetValue(value, out var offer))
                {
                    HasMadeOfferToSelectedPerson = value != null;
                    SelectedPersonOfferValue = offer.Salary;
                }
                else
                {
                    SelectedPersonOfferValue = null;
                }
            }
        }
        public Prospect? SelectedProspect
        {
            get => G<Prospect?>();
            set
            {
                S(value);
                HasInterviewedSelectedProspect = value != null && Interviews.TryGetValue(value.Person, out var interview) && interview;
                if (value != null && Offers.TryGetValue(value.Person, out var offer))
                {
                    HasMadeOfferToSelectedProspect = value != null;
                    SelectedProspectOfferValue = offer.Salary;
                }
                else
                {
                    SelectedProspectOfferValue = null;
                }
            }
        }

        public bool HasMadeOfferToSelectedPerson { get => G<bool>(); set => S(value); }
        public bool HasInterviewedSelectedPerson { get => G<bool>(); set => S(value); }

        public bool HasMadeOfferToSelectedProspect { get => G<bool>(); set => S(value); }
        public bool HasInterviewedSelectedProspect { get => G<bool>(); set => S(value); }

        public int? SelectedPersonOfferValue { get => G<int?>(); set => S(value); }
        public int? SelectedProspectOfferValue { get => G<int?>(); set => S(value); }

        public ICommand DraftOfferCommand => GetCommand(DraftOffer, CanDraftOffer);
        public ICommand CancelOfferCommand => GetCommand(CancelOffer);

        public ICommand InterviewCommand => GetCommand(Interview, CanInterview);
        public ICommand CancelInterviewCommand => GetCommand(CancelInterview);

        public override void OnTurnProcessed()
        {
            base.OnTurnProcessed();

            Offers.Clear();
            Interviews.Clear();

            HasMadeOfferToSelectedPerson = false;
            HasInterviewedSelectedPerson = false;
            HasMadeOfferToSelectedProspect = false;
            HasInterviewedSelectedProspect = false;
            SelectedPersonOfferValue = null;
            SelectedProspectOfferValue = null;

            OnPropertyChanged(nameof(DraftOfferCommand));
            OnPropertyChanged(nameof(InterviewCommand));
        }

        protected override void ClosePanel()
        {
            if (SelectedPerson == null)
            {
                base.ClosePanel();
            }
            else
            {
                SelectedPerson = null;
            }
        }

        private static Person GetTarget(object? target)
        {
            if (target is Person p)
            {
                return p;
            }
            else if (target is Prospect prospect)
            {
                return prospect.Person;
            }

            throw new ArgumentException("Invalid target");
        }

        private void DraftOffer(object? target)
        {
            var person = GetTarget(target);

            var vm = new DraftOfferPopupViewModel(router, person, UserCompany.Money);

            vm.OfferMade += CreateMakeOfferActionData;

            var view = new DraftOfferPopupView
            {
                DataContext = vm
            };

            router.ShowPopup(view);
        }

        private void CreateMakeOfferActionData(object? sender, MakeOfferEventArgs? e)
        {
            var action = new MakeOfferActionData(e.Person.Id, e.OfferValue);

            UserCompany.TurnActions.Add(action);

            Offers[e.Person] = action;

            if (HasMadeOfferToSelectedPerson = e.Person == SelectedPerson)
            {
                SelectedPersonOfferValue = e.OfferValue;
            }

            if (HasMadeOfferToSelectedProspect = e.Person == SelectedProspect?.Person)
            {
                SelectedProspectOfferValue = e.OfferValue;
            }

            RemainingTurnActionCount[ActionType.MakeOffer]--;
            OnPropertyChanged(nameof(DraftOfferCommand));
        }

        private bool CanDraftOffer(object? target)
        {
            return RemainingTurnActionCount[ActionType.MakeOffer] > 0;
        }

        private void CancelOffer(object? target)
        {
            var person = GetTarget(target);

            UserCompany.TurnActions.RemoveAction<MakeOfferActionData>(a => a.PersonId == person.Id);

            Offers.Remove(person);

            if (person == SelectedPerson)
            {
                HasMadeOfferToSelectedPerson = false;
                SelectedPersonOfferValue = null;
            }

            if (person == SelectedProspect?.Person)
            {
                HasMadeOfferToSelectedProspect = false;
                SelectedProspectOfferValue = null;
            }

            RemainingTurnActionCount[ActionType.MakeOffer]++;
            OnPropertyChanged(nameof(DraftOfferCommand));
        }

        private void Interview(object? target)
        {
            var person = GetTarget(target);
            UserCompany.TurnActions.Add(new InterviewProspectActionData(person.Id));

            Interviews[person] = true;
            if (target is Prospect)
            {
                HasInterviewedSelectedProspect = true;
            }
            else
            {
                HasInterviewedSelectedPerson = true;
            }
            RemainingTurnActionCount[ActionType.InterviewProspect]--;
            OnPropertyChanged(nameof(InterviewCommand));
        }

        private bool CanInterview(object? target)
        {
            return RemainingTurnActionCount[ActionType.InterviewProspect] > 0;
        }

        private void CancelInterview(object? target)
        {
            var person = GetTarget(target);

            UserCompany.TurnActions.RemoveAction<InterviewProspectActionData>(a => a.PersonId == person.Id);

            Interviews[person] = false;
            if (target is Prospect)
            {
                HasInterviewedSelectedProspect = false;
            }
            else
            {
                HasInterviewedSelectedPerson = false;
            }
            RemainingTurnActionCount[ActionType.InterviewProspect]++;
            OnPropertyChanged(nameof(InterviewCommand));
        }

        private readonly Dictionary<Person, MakeOfferActionData> Offers = [];
        private readonly Dictionary<Person, bool> Interviews = [];
    }
}
