﻿using Bizio.Client.Desktop.Core;
using Bizio.Client.Desktop.ViewModels.Popups;
using System.ComponentModel;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels
{
    public abstract class PopupViewModelBase(IRouter router) : ValidationBase, IPopupViewModel
    {
        public bool IsBusy
        {
            get => G<bool>();
            set => S(value);
        }

        public event EventHandler? Dismissed;

        public event EventHandler<CancelEventArgs>? Dismissing;

        public ICommand DismissCommand => new RelayCommand(x => Dismiss(), CanDismiss);

        protected bool CanDismiss(object? parameter)
        {
            var args = new CancelEventArgs();

            Dismissing?.Invoke(this, args);

            return !args.Cancel;
        }

        protected void Dismiss() => Dismiss(this, EventArgs.Empty);

        protected void Dismiss<TArgs>(object? sender, TArgs? args)
            where TArgs : EventArgs
        {
            router.DismissPopup();

            Dismissed?.Invoke(sender, args ?? EventArgs.Empty);
        }

        protected ICommand GetCommand<TArgs>(EventHandler<TArgs?>? action, Predicate<TArgs?> canExecute, Func<TArgs> argsGetter)
        {
            return new RelayCommand(x => action?.Invoke(this, argsGetter.Invoke() ?? default), x => canExecute(argsGetter.Invoke() ?? default));
        }
    }
}