﻿using Bizio.Core.Model.People;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels.Popups
{
    public class MakeOfferEventArgs(Person person, int offerValue) : EventArgs
    {
        public Person Person { get; } = person;

        public int OfferValue { get; } = offerValue;
    }

    internal class DraftOfferPopupViewModel : PopupViewModelBase
    {
        public event EventHandler<MakeOfferEventArgs?>? OfferMade;

        public Person Person { get; }

        public int OfferValue
        {
            get => G<int>();
            set
            {
                S(value);
                OnPropertyChanged(nameof(MakeOfferCommand));
            }
        }

        public ICommand MakeOfferCommand => GetCommand(OfferMade, CanMakeOffer, () => new MakeOfferEventArgs(Person, OfferValue));

        public DraftOfferPopupViewModel(IRouter router, Person person, long maximumOffer)
            : base(router)
        {
            Person = person;
            _maximumOffer = maximumOffer;
            OfferMade += Dismiss;
        }

        private bool CanMakeOffer(MakeOfferEventArgs? obj) => !HasErrors;

        protected override IEnumerable<string> Validate(string propertyName)
        {
            if (propertyName != nameof(OfferValue))
            {
                yield break;
            }

            if (OfferValue < 1)
            {
                yield return $"Offer must be greater than {0:C0}";
            }

            if (OfferValue > _maximumOffer)
            {
                yield return $"Offer must be less than {_maximumOffer:C0}";
            }
        }

        private readonly long _maximumOffer;
    }
}
