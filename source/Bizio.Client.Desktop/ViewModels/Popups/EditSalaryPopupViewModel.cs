﻿using Bizio.Core.Model.Companies;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels.Popups
{
    public class EditSalaryEventArgs(Employee employee, int salary) : EventArgs
    {
        public Employee Employee { get; } = employee;

        public int Salary { get; } = salary;
    }

    public class EditSalaryPopupViewModel : OptionsPopupViewModelBase
    {
        public event EventHandler<EditSalaryEventArgs?>? EditSalary;

        public Employee Employee { get; }

        public int Salary
        {
            get => G<int>();
            set
            {
                S(value);
                OnPropertyChanged(nameof(EditSalaryCommand));
            }
        }

        public ICommand EditSalaryCommand => GetCommand(EditSalary, CanEditSalary, () => new EditSalaryEventArgs(Employee, Salary));

        public EditSalaryPopupViewModel(IRouter router, Employee employee, long maximum)
            : base(router)
        {
            Employee = employee;
            Salary = Employee.Salary;
            _maximumSalary = maximum;
            EditSalary += Dismiss;
        }

        private bool CanEditSalary(object? parameter) => HasErrors;

        protected override IEnumerable<string> Validate(string propertyName)
        {
            if (propertyName != nameof(Salary))
            {
                yield break;
            }

            if (Salary == Employee.Salary)
            {
                yield return $"Salary must be different than current salary";
            }

            if (Salary > _maximumSalary)
            {
                yield return $"Salary must less than or equal to {_maximumSalary:C0}";
            }

            if (Salary < 1)
            {
                yield return $"Salary must be greater than {0:C0}";
            }
        }

        private readonly long _maximumSalary;
    }
}
