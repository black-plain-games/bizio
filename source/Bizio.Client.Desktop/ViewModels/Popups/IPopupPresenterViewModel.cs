﻿using System.Windows;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels.Popups
{
    public interface IPopupPresenterViewModel
    {
        FrameworkElement View { get; }
        HorizontalAlignment HorizontalAlignment { get; }
        VerticalAlignment VerticalAlignment { get; }
        ICommand DismissCommand { get; }
    }
}