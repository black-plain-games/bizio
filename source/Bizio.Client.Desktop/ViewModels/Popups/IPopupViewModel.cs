﻿using System.ComponentModel;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels.Popups
{
    public interface IPopupViewModel
    {
        event EventHandler Dismissed;

        event EventHandler<CancelEventArgs> Dismissing;

        ICommand DismissCommand { get; }
    }
}