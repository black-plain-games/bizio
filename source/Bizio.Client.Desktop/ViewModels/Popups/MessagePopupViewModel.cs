﻿namespace Bizio.Client.Desktop.ViewModels.Popups
{
    public class MessagePopupViewModel(IRouter router) : OptionsPopupViewModelBase(router)
    {
        public string Title { get => G<string>(); set => S(value); }

        public string Message { get => G<string>(); set => S(value); }
    }
}