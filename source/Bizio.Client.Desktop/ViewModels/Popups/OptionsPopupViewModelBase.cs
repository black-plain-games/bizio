﻿using System.ComponentModel;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels.Popups
{
    public abstract class OptionsPopupViewModelBase(IRouter router) : PopupViewModelBase(router), IOptionsPopupViewModel
    {
        public IEnumerable<IPopupOption> Options => _options;

        public void AddOption(string label) => AddOption(label, null, null, true);

        public void AddOption(string label, EventHandler selected) => AddOption(label, selected, null, true);

        public void AddOption(string label, EventHandler selected, bool doDismiss) => AddOption(label, selected, null, doDismiss);

        public void AddOption(string label, EventHandler selected, EventHandler<CancelEventArgs> selecting) => AddOption(label, selected, selecting, true);

        public void AddOption(string label, EventHandler selected, EventHandler<CancelEventArgs> selecting, bool doDismiss)
        {
            var option = new CustomPopupOption(label, x => IsBusy = x);

            if (selecting != null)
            {
                option.Selecting += selecting;
            }

            if (selected != null)
            {
                option.Selected += selected;
            }

            if (doDismiss)
            {
                option.Selecting += (s, e) => CanDismiss(null);

                option.Selected += (s, e) =>
                {
                    router.DismissPopup();
                };
            }

            _options.Add(option);
        }

        private readonly ICollection<IPopupOption> _options = [];

        private class CustomPopupOption(string label, Action<bool> setIsBusy) : IPopupOption
        {
            public string Label { get; } = label;

            public ICommand SelectCommand => new RelayCommand(x => OnSelected(), x => OnSelecting());

            public event EventHandler Selected;

            public event EventHandler<CancelEventArgs> Selecting;

            private bool OnSelecting()
            {
                var args = new CancelEventArgs();

                Selecting?.Invoke(this, args);

                return !args.Cancel;
            }

            private void OnSelected()
            {
                Task.Factory.StartNew(() =>
                {
                    setIsBusy(true);

                    Selected?.Invoke(this, EventArgs.Empty);

                    setIsBusy(false);
                });
            }
        }
    }
}