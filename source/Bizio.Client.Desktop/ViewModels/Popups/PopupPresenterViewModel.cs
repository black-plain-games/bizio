﻿using System.Windows;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels.Popups
{
    public class PopupPresenterViewModel : ViewModelBase, IPopupPresenterViewModel
    {
        public FrameworkElement View { get => G<FrameworkElement>() ?? throw new Exception("Cannot present a popup without a view"); set => S(value); }

        public HorizontalAlignment HorizontalAlignment { get => G<HorizontalAlignment>(); set => S(value); }
        public VerticalAlignment VerticalAlignment { get => G<VerticalAlignment>(); set => S(value); }

        public ICommand DismissCommand => GetCommand(_router.DismissPopup);

        public PopupPresenterViewModel(IRouter router, FrameworkElement view)
        {
            _router = router;
            View = view;
        }

        private readonly IRouter _router;
    }
}