﻿using Bizio.Client.Desktop.Models;
using Bizio.Client.Desktop.ViewModels.Popups;
using Bizio.Client.Desktop.Views.Popups;
using Bizio.Core.Model.Companies;
using BlackPlain.Bizio.Services.Actions.Data;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels
{
    public interface IUserCompanyPanelViewModel : IPanelViewModel
    {
        Employee? SelectedEmployee { get; set; }

        IEnumerable<SkillSummary>? MandatorySkillSummaries { get; }
        IEnumerable<SkillSummary>? OptionalSkillSummaries { get; }

        int TotalIncome { get; }
        int TotalExpenses { get; }

        bool IsFiringSelectedEmployee { get; set; }

        ICommand FireSelectedEmployeeCommand { get; }
        ICommand CancelFiringSelectedEmployeeCommand { get; }

        bool IsAdjustingSelectedEmployeeSalary { get; set; }
        ICommand EditSalaryCommand { get; }
        ICommand CancelEditSalaryCommand { get; }
    }

    public class UserCompanyPanelViewModel(IRouter router) : PanelViewModel, IUserCompanyPanelViewModel
    {
        public Employee? SelectedEmployee
        {
            get => G<Employee>();
            set
            {
                S(value);

                IsFiringSelectedEmployee = value != null && EmployeesBeingFired.ContainsKey(value);
                IsAdjustingSelectedEmployeeSalary = value != null && EmployeesChangingSalary.ContainsKey(value);

                OnPropertyChanged(nameof(FireSelectedEmployeeCommand));
                OnPropertyChanged(nameof(CancelFiringSelectedEmployeeCommand));

                OnPropertyChanged(nameof(EditSalaryCommand));
                OnPropertyChanged(nameof(CancelEditSalaryCommand));
            }
        }

        public IEnumerable<SkillSummary>? MandatorySkillSummaries { get => G<IEnumerable<SkillSummary>>(); set => S(value); }
        public IEnumerable<SkillSummary>? OptionalSkillSummaries { get => G<IEnumerable<SkillSummary>>(); set => S(value); }

        public int TotalIncome { get => G<int>(); set => S(value); }
        public int TotalExpenses { get => G<int>(); set => S(value); }

        public bool IsFiringSelectedEmployee { get => G<bool>(); set => S(value); }

        public ICommand FireSelectedEmployeeCommand => GetCommand(FireSelectedEmployee, CanFireSelectedEmployee);
        public ICommand CancelFiringSelectedEmployeeCommand => GetCommand(CancelFiringSelectedEmployee);

        public bool IsAdjustingSelectedEmployeeSalary { get => G<bool>(); set => S(value); }

        public ICommand EditSalaryCommand => GetCommand(EditSalary, CanEditSalary);
        public ICommand CancelEditSalaryCommand => GetCommand(CancelEditSalary);

        private void CancelEditSalary()
        {
            var actionData = EmployeesChangingSalary[SelectedEmployee];

            UserCompany.TurnActions.Remove(actionData);

            EmployeesChangingSalary.Remove(SelectedEmployee);

            IsAdjustingSelectedEmployeeSalary = false;

            OnPropertyChanged(nameof(EditSalaryCommand));
            OnPropertyChanged(nameof(FireSelectedEmployeeCommand));
        }

        private void EditSalary()
        {
            var vm = new EditSalaryPopupViewModel(router, SelectedEmployee, UserCompany.Money);

            vm.EditSalary += CreateAdjustSalaryActionData;

            var view = new EditSalaryPopupView
            {
                DataContext = vm
            };

            router.ShowPopup(view);
        }

        private void CreateAdjustSalaryActionData(object? sender, EditSalaryEventArgs e)
        {
            var actionData = new AdjustSalaryActionData(e.Employee.Id, e.Salary);

            UserCompany.TurnActions.Add(actionData);

            EmployeesChangingSalary.Add(e.Employee, actionData);

            IsAdjustingSelectedEmployeeSalary = SelectedEmployee == e.Employee;

            OnPropertyChanged(nameof(EditSalaryCommand));
            OnPropertyChanged(nameof(FireSelectedEmployeeCommand));
        }

        private bool CanEditSalary()
        {
            if (SelectedEmployee == null)
            {
                return false;
            }

            if (SelectedEmployee.IsFounder)
            {
                return false;
            }

            if (EmployeesBeingFired.ContainsKey(SelectedEmployee))
            {
                return false;
            }

            if (EmployeesChangingSalary.ContainsKey(SelectedEmployee))
            {
                return false;
            }

            if (RemainingTurnActionCount[Bizio.Core.Data.Actions.ActionType.AdjustSalary] == 0)
            {
                return false;
            }

            return true;
        }

        protected override void ClosePanel()
        {
            if (SelectedEmployee != null)
            {
                SelectedEmployee = null;
            }
            else
            {
                base.ClosePanel();
            }
        }

        public override void OnTurnProcessed()
        {
            base.OnTurnProcessed();

            EmployeesBeingFired.Clear();
            IsFiringSelectedEmployee = false;

            EmployeesChangingSalary.Clear();
            IsAdjustingSelectedEmployeeSalary = false;

            TotalIncome = UserCompany.Transactions.Where(t => t.Amount > 0).Sum(t => t.Amount);
            TotalExpenses = UserCompany.Transactions.Where(t => t.Amount < 0).Sum(t => t.Amount);

            var mandatorySkillSummaries = new List<SkillSummary>();
            var optionalSkillSummaries = new List<SkillSummary>();

            var skillGroups = UserCompany.Employees.SelectMany(e => e.Person.Skills).GroupBy(s => s.SkillDefinition);

            foreach (var group in skillGroups)
            {
                var total = group.Sum(s => s.Value);
                var count = group.Count();
                var bestPersonAmount = group.Max(s => s.Value);
                var bestEmployee = UserCompany.Employees.First(e => e.Person.Skills.Any(s => s.SkillDefinition == group.Key && s.Value == bestPersonAmount));

                var summary = new SkillSummary
                {
                    Skill = group.Key,
                    Total = total,
                    Count = count,
                    BestPerson = bestEmployee.Person,
                    BestPersonAmount = bestPersonAmount
                };

                if (Game.Industry.MandatorySkills.Contains(group.Key))
                {
                    mandatorySkillSummaries.Add(summary);
                }
                else
                {
                    optionalSkillSummaries.Add(summary);
                }
            }

            MandatorySkillSummaries = mandatorySkillSummaries;
            OptionalSkillSummaries = optionalSkillSummaries;
        }

        private void CancelFiringSelectedEmployee()
        {
            var actionData = EmployeesBeingFired[SelectedEmployee];

            UserCompany.TurnActions.Remove(actionData);

            EmployeesBeingFired.Remove(SelectedEmployee);

            IsFiringSelectedEmployee = false;

            OnPropertyChanged(nameof(EditSalaryCommand));
        }

        private bool CanFireSelectedEmployee()
        {
            if (SelectedEmployee == null)
            {
                return false;
            }

            if (SelectedEmployee.IsFounder)
            {
                return false;
            }

            if (EmployeesBeingFired.ContainsKey(SelectedEmployee))
            {
                return false;
            }

            if (EmployeesChangingSalary.ContainsKey(SelectedEmployee))
            {
                return false;
            }

            return true;
        }

        private void FireSelectedEmployee()
        {
            var actionData = new FireEmployeeActionData(SelectedEmployee.Id);

            UserCompany.TurnActions.Add(actionData);

            EmployeesBeingFired.Add(SelectedEmployee, actionData);

            IsFiringSelectedEmployee = true;

            OnPropertyChanged(nameof(EditSalaryCommand));
        }

        private readonly Dictionary<Employee, FireEmployeeActionData> EmployeesBeingFired = [];

        private readonly Dictionary<Employee, AdjustSalaryActionData> EmployeesChangingSalary = [];
    }
}
