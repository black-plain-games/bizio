﻿using Bizio.Client.Desktop.ViewModels;
using System.Windows.Controls;

namespace Bizio.Client.Desktop.Views
{
    public partial class GameView : UserControl
    {
        public GameView(IGameViewModel gameViewModel)
        {
            InitializeComponent();

            DataContext = gameViewModel;
        }
    }
}
