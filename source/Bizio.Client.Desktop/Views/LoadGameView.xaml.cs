﻿using Bizio.Client.Desktop.ViewModels;
using Bizio.Core.Model.Game;
using System.Windows.Controls;
using System.Windows.Data;
using D = Bizio.Core.Data.Game;

namespace Bizio.Client.Desktop.Views
{
    public partial class LoadGameView : UserControl
    {
        public LoadGameView(ILoadGameViewModel loadGameViewModel)
        {
            InitializeComponent();

            DataContext = loadGameViewModel;
        }

        private void OnFilterPlayableGames(object sender, FilterEventArgs e)
        {
            e.Accepted = e.Item is GameMetadata { Status: D.GameStatus.Playing };
        }

        private void OnFilterUnplayableGames(object sender, FilterEventArgs e)
        {
            e.Accepted = e.Item is GameMetadata { Status: D.GameStatus.UserLost };
        }
    }
}