﻿using Bizio.Client.Desktop.ViewModels;
using System.Windows.Controls;

namespace Bizio.Client.Desktop.Views
{
    public partial class MainMenuView : UserControl
    {
        public MainMenuView(IMainMenuViewModel mainMenuViewModel)
        {
            InitializeComponent();

            DataContext = mainMenuViewModel;
        }
    }
}
