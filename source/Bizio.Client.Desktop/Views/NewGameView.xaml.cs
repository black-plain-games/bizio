﻿using Bizio.Client.Desktop.ViewModels;
using System.Windows.Controls;

namespace Bizio.Client.Desktop.Views
{
    public partial class NewGameView : UserControl
    {
        public NewGameView(INewGameViewModel newGameViewModel)
        {
            InitializeComponent();

            DataContext = newGameViewModel;
        }
    }
}
