﻿using Bizio.Client.Desktop.Views.Popups;
using Microsoft.Extensions.DependencyInjection;

namespace Bizio.Client.Desktop.Views
{
    public static class Packages
    {
        public static IServiceCollection RegisterViews(this IServiceCollection services)
        {
            return services
                .AddSingleton<MainWindow>()
                .AddTransient<MainMenuView>()
                .AddTransient<NewGameView>()
                .AddTransient<SamplePopup>()
                .AddTransient<LoadGameView>()
                .AddTransient<GameView>();
        }
    }
}
