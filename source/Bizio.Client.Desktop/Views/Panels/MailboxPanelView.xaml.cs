﻿using Bizio.Core.Model.Companies;
using System.Windows.Controls;
using System.Windows.Data;
using D = Bizio.Core.Data.Companies;

namespace Bizio.Client.Desktop.Views.Panels
{
    public partial class MailboxPanelView : UserControl
    {
        public MailboxPanelView()
        {
            InitializeComponent();
        }

        private void UnreadMessagesFilter(object sender, FilterEventArgs e)
        {
            if (e.Item is CompanyMessage message)
            {
                e.Accepted = message.Status == D.CompanyMessageStatus.UnRead;

                return;
            }

            e.Accepted = false;
        }

        private void ReadMessagesFilter(object sender, FilterEventArgs e)
        {
            if (e.Item is CompanyMessage message)
            {
                e.Accepted = message.Status == D.CompanyMessageStatus.Read;

                return;
            }

            e.Accepted = false;
        }
    }
}
