﻿using System.Windows.Controls;

namespace Bizio.Client.Desktop.Views.Popups
{
    /// <summary>
    /// Interaction logic for DraftOfferPopupView.xaml
    /// </summary>
    public partial class DraftOfferPopupView : UserControl
    {
        public DraftOfferPopupView()
        {
            InitializeComponent();
        }
    }
}
