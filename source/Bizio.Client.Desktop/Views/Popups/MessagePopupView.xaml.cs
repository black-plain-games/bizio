﻿using System.Windows.Controls;

namespace Bizio.Client.Desktop.Views.Popups
{
    /// <summary>
    /// Interaction logic for MessagePopupView.xaml
    /// </summary>
    public partial class MessagePopupView : UserControl
    {
        public MessagePopupView()
        {
            InitializeComponent();
        }
    }
}
