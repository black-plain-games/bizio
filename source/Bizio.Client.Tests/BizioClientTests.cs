﻿using BlackPlain.Core.Services;
using BlackPlain.Core.Web;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Xunit;

namespace Bizio.Client.Tests
{
    [TestClass]
    public class BizioClientTests
    {
        private readonly IBizioClient _client;

        public BizioClientTests()
        {
            _client = new BizioClient(new AppConfigSettingService());
        }

        [Theory]
        [InlineData("poop")]
        public void Ping_succeeds(string message)
        {
            var response = _client.Ping(message);

            response.Should().NotBeNull();

            response.Code.Should().Be(ResponseCode.OK);

            response.Message.Should().BeNullOrWhiteSpace();

            response.Content.Should().NotBeNull();

            response.Content.Exception.Should().BeNull();

            response.Content.ValidationErrors.Should().BeNullOrEmpty();

            response.Content.Data.Should().NotBeNullOrWhiteSpace();

            response.Content.Data.Should().Contain(message);

            _client.Data.RetrieveLanguageData(DateTimeOffset.MinValue);
        }

        [Fact]
        [TestMethod]
        public void Data_RetrieveLanguageData_succeeds()
        {
            var response = _client.Data.RetrieveLanguageData(DateTimeOffset.MinValue);

            response.Should().NotBeNull();

            response.Code.Should().Be(ResponseCode.OK);

            response.Message.Should().BeNullOrWhiteSpace();

            response.Content.Should().NotBeNull();

            response.Content.Exception.Should().BeNull();

            response.Content.ValidationErrors.Should().BeNullOrEmpty();

            response.Content.Data.Should().NotBeNull();

            response.Content.Data.Languages.Should().NotBeNullOrEmpty();

            response.Content.Data.TextResourceKeys.Should().NotBeNull();

            response.Content.Data.TextResourceKeys.Count.Should().BeGreaterThan(0);
        }
    }
}