﻿using Bizio.Server.Model.Core;
using BlackPlain.Core.Web;
using Microsoft.Extensions.Options;
using RestSharp;

namespace Bizio.Client
{
    public class BizioClient : ClientBase, IBizioClient
    {
        public INotificationClient Notifications { get; }

        public IDataClient Data { get; }

        public IGameClient Games { get; }

        public BizioClient(IOptions<BizioClientOptions> options) : base(new RestClient(options.Value.BizioBaseUrl), "api")
        {
            Notifications = new NotificationClient(Client);

            Data = new DataClient(Client);

            Games = new GameClient(Client);
        }

        public Response<ApiResponseMessage<string>> Ping(string message)
        {
            var request = new RestRequest(GetUrl($"ping/{message}"));

            var response = Client.Get(request);

            return Parse<ApiResponseMessage<string>>(response);
        }
    }
}