﻿using Bizio.Server.Model.Core;
using Bizio.Server.Model.Text;
using BlackPlain.Core.Web;
using RestSharp;
using System;

namespace Bizio.Client
{
    public class DataClient : ClientBase, IDataClient
    {
        public DataClient(IRestClient client)
            : base(client, "api/data")
        {
        }

        public Response<ApiResponseMessage<StaticDataDto>> RetrieveStaticData(string token)
        {
            var request = CreateAuthorizedRequest(token, GetUrl("static"));

            var response = Client.Get(request);

            return Parse<ApiResponseMessage<StaticDataDto>>(response);
        }

        public Response<ApiResponseMessage<LanguageDataDto>> RetrieveLanguageData(DateTimeOffset minimumDateModified)
        {
            var request = new RestRequest(GetUrl("languages/{0}", minimumDateModified.ToString("MM-dd-yyyy")));

            var response = Client.Get(request);

            return Parse<ApiResponseMessage<LanguageDataDto>>(response);
        }
    }
}