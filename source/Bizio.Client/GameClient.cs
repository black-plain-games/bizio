﻿using Bizio.Server.Model.Core;
using Bizio.Server.Model.Game;
using BlackPlain.Core.Web;
using RestSharp;
using System.Collections.Generic;

namespace Bizio.Client
{
    public class GameClient : ClientBase, IGameClient
    {
        public GameClient(IRestClient client)
            : base(client, "api/game")
        {
        }

        public Response<ApiResponseMessage<int>> Create(string token, NewGameDto newGameDto)
        {
            var request = CreateAuthorizedRequest(token, GetUrl());

            request.AddJsonBody(newGameDto);

            var response = Client.Post(request);

            return Parse<ApiResponseMessage<int>>(response);
        }

        public Response<ApiResponseMessage<GameDataDto>> Retrieve(string token, int gameId)
        {
            var request = CreateAuthorizedRequest(token, GetUrl($"{gameId}"));

            var response = Client.Get(request);

            return Parse<ApiResponseMessage<GameDataDto>>(response);
        }

        public Response<ApiResponseMessage<IEnumerable<GameDto>>> Retrieve(string token)
        {
            var request = CreateAuthorizedRequest(token, GetUrl());

            var response = Client.Get(request);

            return Parse<ApiResponseMessage<IEnumerable<GameDto>>>(response);
        }

        public Response<ApiResponseMessage> Update(string token, int gameId, ProcessTurnDto processTurnDto)
        {
            var request = CreateAuthorizedRequest(token, GetUrl($"{gameId}"));

            request.AddJsonBody(processTurnDto);

            var response = Client.Post(request);

            return Parse<ApiResponseMessage>(response);
        }
    }
}