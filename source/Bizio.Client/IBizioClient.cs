﻿using Bizio.Server.Model.Core;
using BlackPlain.Core.Web;

namespace Bizio.Client
{
    public interface IBizioClient
    {
        INotificationClient Notifications { get; }

        IDataClient Data { get; }

        IGameClient Games { get; }

        Response<ApiResponseMessage<string>> Ping(string message);
    }
}