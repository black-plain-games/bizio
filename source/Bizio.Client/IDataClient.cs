﻿using Bizio.Server.Model.Core;
using Bizio.Server.Model.Text;
using BlackPlain.Core.Web;
using System;

namespace Bizio.Client
{
    public interface IDataClient
    {
        Response<ApiResponseMessage<StaticDataDto>> RetrieveStaticData(string token);

        Response<ApiResponseMessage<LanguageDataDto>> RetrieveLanguageData(DateTimeOffset minimumDateModified);
    }
}