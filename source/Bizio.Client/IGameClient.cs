﻿using Bizio.Server.Model.Core;
using Bizio.Server.Model.Game;
using BlackPlain.Core.Web;
using System.Collections.Generic;

namespace Bizio.Client
{
    public interface IGameClient
    {
        Response<ApiResponseMessage<int>> Create(string token, NewGameDto newGameDto);

        Response<ApiResponseMessage<GameDataDto>> Retrieve(string token, int gameId);

        Response<ApiResponseMessage<IEnumerable<GameDto>>> Retrieve(string token);

        Response<ApiResponseMessage> Update(string token, int gameId, ProcessTurnDto processTurnDto);
    }
}