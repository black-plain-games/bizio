﻿using Bizio.Server.Model;
using Bizio.Server.Model.Core;
using BlackPlain.Core.Web;
using System.Collections.Generic;

namespace Bizio.Client
{
    public interface INotificationClient
    {
        Response<ApiResponseMessage<IEnumerable<NotificationDto>>> Retrieve(string token, int? minimumId);
    }
}