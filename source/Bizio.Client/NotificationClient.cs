﻿using Bizio.Server.Model;
using Bizio.Server.Model.Core;
using BlackPlain.Core.Web;
using RestSharp;
using System.Collections.Generic;

namespace Bizio.Client
{
    public class NotificationClient : ClientBase, INotificationClient
    {
        public NotificationClient(IRestClient client)
            : base(client, "api/notification")
        {
        }

        public Response<ApiResponseMessage<IEnumerable<NotificationDto>>> Retrieve(string token, int? minimumId)
        {
            var request = CreateAuthorizedRequest(token, GetUrl($"{minimumId ?? -1}"));

            var response = Client.Get(request);

            return Parse<ApiResponseMessage<IEnumerable<NotificationDto>>>(response);
        }
    }
}