﻿using System.Collections.Generic;

namespace Bizio.Core.Data.Actions
{
    public struct ActionExecutionInformation
    {
        public bool CanPerformAction;

        public bool ShouldPerformAction;

        public string Message;

        public IEnumerable<object> Data;

        public static readonly ActionExecutionInformation Yes = new() { CanPerformAction = true };
    }
}