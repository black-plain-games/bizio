﻿using System;
using System.Collections.Generic;

namespace Bizio.Core.Data.Actions
{
    public interface IAction : IIdentifiable<Guid>
    {
        int DefaultCount { get; set; }
        int MaxCount { get; set; }
        string Name { get; set; }
        IEnumerable<ActionRequirement> Requirements { get; set; }
        ActionType Type { get; set; }
    }
}