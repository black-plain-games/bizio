﻿using System;

namespace Bizio.Core.Data.Companies
{
    public class Employee : IIdentifiable<Guid>
    {
        public Guid Id { get; set; }
        public Guid PersonId { get; set; }
        public bool IsFounder { get; set; }
        public int Happiness { get; set; }
        public int Salary { get; set; }
    }
}