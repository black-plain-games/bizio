﻿using Bizio.Core.Utilities;
using System;
using System.Collections.Generic;

namespace Bizio.Core.Data.Companies
{
    public class Prospect : IIdentifiable<Guid>
    {
        public Guid Id { get; set; }
        public Guid PersonId { get; set; }
        public int Accuracy { get; set; }
        public IEnumerable<ProspectSkill> Skills { get; set; }
        public Range<int> Salary { get; }
    }
}