﻿using Bizio.Core.Utilities;
using System;

namespace Bizio.Core.Data.Companies
{
    /// <summary>
    /// Represents the known range for a person's skill.
    /// </summary>
    public class ProspectSkill
    {
        /// <summary>
        /// Gets or sets the skill definition the value range represents.
        /// </summary>
        public Guid SkillDefinitionId { get; set; }

        /// <summary>
        /// Gets the range of the value for this skill.
        /// </summary>
        public Range<int> Value { get; }
    }
}