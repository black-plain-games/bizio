﻿using System;

namespace Bizio.Core.Data.Companies
{
    public class Transaction : IIdentifiable<Guid>
    {
        public Guid Id { get; set; }
        public TransactionType Type { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public int Amount { get; set; }
        public long EndingBalance { get; set; }
    }
}