﻿namespace Bizio.Core.Data.Companies
{
    /// <summary>
    /// Defines the possible types of transactions.
    /// </summary>
    public enum TransactionType : byte
    {
        None,
        Payroll = 1,
        HireEmployee,
        FireEmployee,
        PurchasePerk,
        SellPerk,
        Perk,
        Project,
        Maintenance
    }
}