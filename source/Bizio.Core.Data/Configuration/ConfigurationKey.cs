﻿namespace Bizio.Core.Data.Configuration
{
    public enum ConfigurationKey
    {
        INVALID,

        /// <summary>
        /// Indicates the chance of a newly generated person being female.
        /// </summary>
        PercentFemale,

        /// <summary>
        /// Indicates the minimum number of optional skills a person can have.
        /// </summary>
        MinimumOptionalSkillCount,

        /// <summary>
        /// Indicates the maximum number of optional skills a person can have.
        /// </summary>
        MaximumOptionalSkillCount,

        /// <summary>
        /// Indicates the number of people to randomly generate for a new game.
        /// </summary>
        InitialPersonPoolSize,

        /// <summary>
        /// Indicates the number of people to generate when there are no umployeed people left.
        /// </summary>
        PersonGenerationBatchSize,

        /// <summary>
        /// Indicates the number of projects generated when there are none left.
        /// </summary>
        ProjectGenerationBatchSize,

        /// <summary>
        /// Indicates the maximum number of turns a project extension can be.
        /// </summary>
        MaximumExtensionLength,

        /// <summary>
        /// Indicates how much a perk can be sold for, based on it's original value.
        /// </summary>
        PerkResellFactor,

        /// <summary>
        /// Indicates how many employees are in a large company.
        /// </summary>
        LargeCompanySize,

        /// <summary>
        /// When evaluating an offer, indicates what the greatest variance between desired and actual
        /// company size can be for a person to accept an offer.
        /// </summary>
        CompanySizeMaxDifferential,

        /// <summary>
        /// Indicates how many prospects must exist at one time for a company to be considered
        /// "fast-growing".
        /// </summary>
        FastCompanyGrowthValue,

        /// <summary>
        /// When evaluating an offer, indicates what the greatest variance between desired and actual
        /// company growth can be for a person to accept an offer.
        /// </summary>
        CompanyGrowthMaxDifferential,

        /// <summary>
        /// Indicates the minimum number of people that are available to hire at the beginning of a turn.
        /// </summary>
        MinimumPersonCount,

        /// <summary>
        /// Indicates the minimum number of projects that are available at the beginning of a turn.
        /// </summary>
        MinimumProjectCount,

        /// <summary>
        /// Indicates the percent of the value and reputation that will be earned when a project is
        /// cancelled.
        /// </summary>
        ProjectCancelledPercent,

        /// <summary>
        /// Indicates the percent of the value and reputation that will be earned when a project is
        /// cancelled.
        /// </summary>
        ProjectFailedPercent,

        /// <summary>
        /// Indicates the additional percent of the value and reputation that will be earned when a project
        /// is completed, cancelled, or failed with an extension.
        /// </summary>
        ProjectExtensionValueAdjustmentPercent,

        /// <summary>
        /// A value between 0 and 200 indicating the likelihood that a person will be generated with a skill
        /// that they are not affine or averse to.
        /// </summary>
        DefaultSkillWeight,

        /// <summary>
        /// The highest amount of salary that can be earned by having maximum mandatory skill points
        /// </summary>
        MandatorySkillSalaryWeight,

        /// <summary>
        /// The highest amount of salary that can be earned by having maximum optional skill points
        /// </summary>
        OptionalSkillSalaryWeight,

        /// <summary>
        /// The salary paid to an employee with 0 skill points in all skills
        /// </summary>
        BaseSalary,

        /// <summary>
        /// A coefficient used when calculating a person's salary
        /// </summary>
        SalaryCalculationMagnitude,

        /// <summary>
        /// The cost, per turn, to maintain an employee including the founder
        /// </summary>
        EmployeeMaintenanceCost
    }
}