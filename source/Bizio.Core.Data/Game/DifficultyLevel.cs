﻿using System;

namespace Bizio.Core.Data.Game
{
    public class DifficultyLevel : IIdentifiable<Guid>
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public int MaximumOptionalSkillCount { get; set; }

        public int MaximumTotalSkillPoints { get; set; }

        public int InitialFunds { get; set; }
    }
}