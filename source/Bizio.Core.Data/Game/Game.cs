﻿using Bizio.Core.Data.Companies;
using Bizio.Core.Data.Configuration;
using Bizio.Core.Data.People;
using Bizio.Core.Data.Projects;
using System;
using System.Collections.Generic;

namespace Bizio.Core.Data.Game
{
    public class Game : IIdentifiable<Guid>
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
        public GameStatus Status { get; set; }

        public DateTime StartDate { get; set; }
        public int CurrentTurn { get; set; }

        public Guid IndustryId { get; set; }

        public IEnumerable<Company> Companies { get; set; }
        public IEnumerable<Project> Projects { get; set; }
        public IEnumerable<Person> People { get; set; }
        public IDictionary<ConfigurationKey, string> Configuration { get; set; }

        public DateTime CurrentDate => StartDate.AddDays(CurrentTurn);
    }
}