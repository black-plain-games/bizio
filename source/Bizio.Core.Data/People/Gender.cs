﻿namespace Bizio.Core.Data.People
{
    /// <summary>
    /// Represents the gender of a person. Surprise.
    /// </summary>
    public enum Gender : byte
    {
        None,
        Male = 1,
        Female
    }
}