﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Core.Data.People
{
    /// <summary>
    /// Represents the descriptive information for a personality.
    /// </summary>
    public class Personality : IIdentifiable<Guid>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public IEnumerable<PersonalityAttribute> Attributes { get; set; }

        public int GetValue(PersonalityAttributeId attributeId) => Attributes.First(a => a.Id == attributeId).Value;
    }
}