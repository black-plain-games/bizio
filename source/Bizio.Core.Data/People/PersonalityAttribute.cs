﻿namespace Bizio.Core.Data.People
{
    public class PersonalityAttribute
    {
        public PersonalityAttributeId Id { get; set; }
        public int Value { get; set; }

        public PersonalityAttribute() { }

        public PersonalityAttribute(PersonalityAttributeId id, int value)
        {
            Id = id;
            Value = value;
        }
    }
}