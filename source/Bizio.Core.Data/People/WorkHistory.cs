﻿using System;

namespace Bizio.Core.Data.People
{
    public class WorkHistory
    {
        public Guid CompanyId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int StartingSalary { get; set; }
        public int? EndingSalary { get; set; }
    }
}