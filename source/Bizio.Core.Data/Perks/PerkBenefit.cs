﻿namespace Bizio.Core.Data.Perks
{
    public class PerkBenefit
    {
        public PerkTargetType Target { get; set; }
        public PerkBenefitAttribute Attribute { get; set; }
        public int Value { get; set; }
        public PerkValueType ValueType { get; set; }
    }
}