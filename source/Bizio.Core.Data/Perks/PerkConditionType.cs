﻿namespace Bizio.Core.Data.Perks
{
    /// <summary>
    /// Defines the entity a condition is for.
    /// </summary>
    public enum PerkConditionType : byte
    {
        None,
        Employee = 1,
        Company
    }
}