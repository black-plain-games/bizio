﻿namespace Bizio.Core.Data.Perks
{
    /// <summary>
    /// Defines the type of entity a perk can target.
    /// </summary>
    public enum PerkTargetType : byte
    {
        Person = 1,
        Employee,
        Company
    }
}