﻿using System;
using System.Collections.Generic;

namespace Bizio.Core.Data.Projects
{
    public class Project : IIdentifiable<Guid>
    {
        public Guid Id { get; set; }
        public Guid ProjectDefinitionId { get; set; }
        public int Value { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime? ExtensionDeadline { get; set; }
        public IEnumerable<ProjectRequirement> Requirements { get; set; }
        public StarCategory ReputationRequired { get; set; }
        public ProjectStatus Status { get; set; }
        public ProjectResult? Result { get; set; }
    }
}