﻿using Bizio.Core.Utilities;
using System;

namespace Bizio.Core.Data.Projects
{
    public class ProjectDefinitionSkill
    {
        public Guid SkillDefinitionId { get; set; }
        public Range<int> Value { get; set; }
        public bool IsRequired { get; set; }

        public ProjectDefinitionSkill() { }

        public ProjectDefinitionSkill(Guid skillDefinitionId, int minValue, int maxValue, bool isRequired = false)
        {
            SkillDefinitionId = skillDefinitionId;
            Value = new Range<int>(minValue, maxValue);
            IsRequired = isRequired;
        }
    }
}