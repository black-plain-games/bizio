﻿namespace Bizio.Core.Data.Projects
{
    /// <summary>
    /// Defines the possible outcomes for a project
    /// </summary>
    public enum ProjectResult : byte
    {
        INVALID = 0,
        Success,
        Cancelled,
        Failure
    }
}