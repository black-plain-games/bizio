﻿using Bizio.Core.Data.Companies;
using Bizio.Core.Data.People;
using Bizio.Core.Data.Perks;
using System;
using System.Collections.Generic;

namespace Bizio.Core.Data
{
    public class StaticData
    {
        public IDictionary<Guid, Actions.Action> Actions { get; set; }
        public IDictionary<Guid, SkillDefinition> SkillDefinitions { get; set; }
        public IDictionary<Guid, Industry> Industries { get; set; }
        public IDictionary<Guid, Perk> Perks { get; set; }
        public IDictionary<Guid, Personality> Personalities { get; set; }
        public IDictionary<Guid, Person> DefaultPeople { get; set; }
        public IEnumerable<string> LastNames { get; set; }
        public IDictionary<Gender, IEnumerable<string>> FirstNames { get; set; }
    }
}
