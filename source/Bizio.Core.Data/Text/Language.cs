﻿using System;
using System.Collections.Generic;

namespace Bizio.Core.Data.Text
{
    public class Language : IIdentifiable<Guid>
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Iso2LetterCode { get; set; }

        public DateTimeOffset DateModified { get; set; }

        public IDictionary<Guid, TextResource> Text { get; set; }
    }
}
