﻿using System;

namespace Bizio.Core.Data.Text
{
    public class TextResource
    {
        public string Text { get; set; }

        public DateTimeOffset DateModified { get; set; }
    }
}
