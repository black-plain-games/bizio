﻿using System;
using System.Collections.ObjectModel;
using DA = Bizio.Core.Data.Actions;

namespace Bizio.Core.Model.Actions
{
    public class Action : NotifyOnPropertyChanged, IIdentifiable<Guid>
    {
        public Guid Id { get => G<Guid>(); set => S(value); }
        public DA.ActionType Type { get => G<DA.ActionType>(); set => S(value); }
        public string Name { get => G<string>(); set => S(value); }
        public ObservableCollection<ActionRequirement> Requirements { get => G<ObservableCollection<ActionRequirement>>(); set => S(value); }
        public int MaxCount { get => G<int>(); set => S(value); }
        public int DefaultCount { get => G<int>(); set => S(value); }
    }
}
