﻿using Bizio.Core.Model;
using System;
using System.Collections.ObjectModel;

namespace Bizio.Core.Model.Companies
{
    public class CompanyAction : NotifyOnPropertyChanged
    {
        public Action Action { get => G<Action>(); set => S(value); }
        public ObservableCollection<CompanyActionAccumulation> Accumulations { get => G<ObservableCollection<CompanyActionAccumulation>>(); set => S(value); }
        public int Count { get => G<int>(); set => S(value); }
        public bool IsActive { get => G<bool>(); set => S(value); }
    }
}
