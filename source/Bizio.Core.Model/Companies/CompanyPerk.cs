﻿using Bizio.Core.Model;
using Bizio.Core.Model.Perks;
using System;

namespace Bizio.Core.Model.Companies
{
    public class CompanyPerk : NotifyOnPropertyChanged
    {
        public Perk Perk { get => G<Perk>(); set => S(value); }
        public DateTime? NextPaymentDate { get => G<DateTime?>(); set => S(value); }
    }
}
