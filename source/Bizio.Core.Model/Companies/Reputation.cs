﻿using Bizio.Core.Model;

namespace Bizio.Core.Model.Companies
{
    public class Reputation : NotifyOnPropertyChanged
    {
        public int EarnedStars { get => G<int>(); set => S(value); }
        public int PossibleStars { get => G<int>(); set => S(value); }
    }
}
