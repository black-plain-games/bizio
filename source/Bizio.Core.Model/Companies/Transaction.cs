﻿using Bizio.Core.Data.Companies;
using Bizio.Core.Model;
using System;

namespace Bizio.Core.Model.Companies
{
    public class Transaction : NotifyOnPropertyChanged, IIdentifiable<Guid>
    {
        public Guid Id { get => G<Guid>(); set => S(value); }
        public TransactionType Type { get => G<TransactionType>(); set => S(value); }
        public DateTime Date { get => G<DateTime>(); set => S(value); }
        public string Description { get => G<string>(); set => S(value); }
        public int Amount { get => G<int>(); set => S(value); }
        public long EndingBalance { get => G<long>(); set => S(value); }
    }
}
