﻿using Bizio.Core.Data.Configuration;
using Bizio.Core.Data.Game;
using Bizio.Core.Model;
using Bizio.Core.Model.Companies;
using Bizio.Core.Model.People;
using Bizio.Core.Model.Projects;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Bizio.Core.Model.Game
{
    public class Game : NotifyOnPropertyChanged, IIdentifiable<Guid>
    {
        public Guid Id { get => G<Guid>(); set => S(value); }
        public Guid UserId { get => G<Guid>(); set => S(value); }
        public DateTimeOffset CreatedDate { get => G<DateTimeOffset>(); set => S(value); }
        public GameStatus Status { get => G<GameStatus>(); set => S(value); }

        public DateTime StartDate { get => G<DateTime>(); set => S(value); }
        public int CurrentTurn { get => G<int>(); set => S(value); }

        public Industry Industry { get => G<Industry>(); set => S(value); }

        public ObservableCollection<Company> Companies { get => G<ObservableCollection<Company>>(); set => S(value); }
        public ObservableCollection<Project> Projects { get => G<ObservableCollection<Project>>(); set => S(value); }
        public ObservableCollection<Person> People { get => G<ObservableCollection<Person>>(); set => S(value); }
        public IDictionary<ConfigurationKey, string> Configuration { get => G<IDictionary<ConfigurationKey, string>>(); set => S(value); }

        public DateTime CurrentDate => StartDate.AddDays(CurrentTurn);
    }
}
