﻿using Bizio.Core.Data.People;
using System;
using System.Collections.ObjectModel;

namespace Bizio.Core.Model.People
{
    public class Person : NotifyOnPropertyChanged, IIdentifiable<Guid>
    {
        public Guid Id { get => G<Guid>(); set => S(value); }
        public string FirstName { get => G<string>(); set => S(value); }
        public string LastName { get => G<string>(); set => S(value); }
        public DateTime Birthday { get => G<DateTime>(); set => S(value); }
        public Profession? Profession { get => G<Profession?>(); set => S(value); }
        public ObservableCollection<Skill> Skills { get => G<ObservableCollection<Skill>>(); set => S(value); }
        public Gender Gender { get => G<Gender>(); set => S(value); }
        public DateTime RetirementDate { get => G<DateTime>(); set => S(value); }
        public Personality Personality { get => G<Personality>(); set => S(value); }
        public ObservableCollection<WorkHistory> WorkHistory { get => G<ObservableCollection<WorkHistory>>(); set => S(value); }
    }
}
