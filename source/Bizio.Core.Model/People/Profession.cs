﻿using Bizio.Core.Model;
using System;
using System.Collections.ObjectModel;

namespace Bizio.Core.Model.People
{
    public class Profession : NotifyOnPropertyChanged, IIdentifiable<Guid>
    {
        public Guid Id { get => G<Guid>(); set => S(value); }

        public string Name { get => G<string>(); set => S(value); }

        public ObservableCollection<ProfessionSkillDefinition> SkillDefinitions { get => G<ObservableCollection<ProfessionSkillDefinition>>(); set => S(value); }
    }
}
