﻿using Bizio.Core.Model;

namespace Bizio.Core.Model.People
{
    public class ProfessionSkillDefinition : NotifyOnPropertyChanged
    {
        public SkillDefinition SkillDefinition { get => G<SkillDefinition>(); set => S(value); }

        public int Weight { get => G<int>(); set => S(value); }
    }
}
