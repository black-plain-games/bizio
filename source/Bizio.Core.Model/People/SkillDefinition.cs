﻿using Bizio.Core.Model;
using Bizio.Core.Utilities;
using System;

namespace Bizio.Core.Model.People
{
    public class SkillDefinition : NotifyOnPropertyChanged, IIdentifiable<Guid>
    {
        public Guid Id { get => G<Guid>(); set => S(value); }
        public string Name { get => G<string>(); set => S(value); }
        public Range<byte> Value { get => G<Range<byte>>(); set => S(value); }
    }
}