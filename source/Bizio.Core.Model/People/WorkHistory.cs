﻿using Bizio.Core.Model;
using Bizio.Core.Model.Companies;
using System;

namespace Bizio.Core.Model.People
{
    public class WorkHistory : NotifyOnPropertyChanged
    {
        public Company CompanyId { get => G<Company>(); set => S(value); }
        public DateTime StartDate { get => G<DateTime>(); set => S(value); }
        public DateTime? EndDate { get => G<DateTime?>(); set => S(value); }
        public int StartingSalary { get => G<int>(); set => S(value); }
        public int? EndingSalary { get => G<int?>(); set => S(value); }
    }
}
