﻿using Bizio.Core.Model;
using System;
using System.Collections.ObjectModel;

namespace Bizio.Core.Model.Perks
{
    public class Perk : NotifyOnPropertyChanged, IIdentifiable<Guid>
    {
        public Guid Id { get => G<Guid>(); set => S(value); }
        public string Name { get => G<string>(); set => S(value); }
        public string Description { get => G<string>(); set => S(value); }
        public int? MaxCount { get => G<int?>(); set => S(value); }
        public int? InitialCost { get => G<int?>(); set => S(value); }
        public int? RecurringCost { get => G<int?>(); set => S(value); }
        public int? RecurringCostInterval { get => G<int?>(); set => S(value); }
        public ObservableCollection<PerkCondition> Conditions { get => G<ObservableCollection<PerkCondition>>(); set => S(value); }
        public ObservableCollection<PerkBenefit> Benefits { get => G<ObservableCollection<PerkBenefit>>(); set => S(value); }
    }
}
