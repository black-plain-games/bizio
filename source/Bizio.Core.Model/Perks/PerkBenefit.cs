﻿using Bizio.Core.Data.Perks;
using Bizio.Core.Model;

namespace Bizio.Core.Model.Perks
{
    public class PerkBenefit : NotifyOnPropertyChanged
    {
        public PerkTargetType Target { get => G<PerkTargetType>(); set => S(value); }
        public PerkBenefitAttribute Attribute { get => G<PerkBenefitAttribute>(); set => S(value); }
        public int Value { get => G<int>(); set => S(value); }
        public PerkValueType ValueType { get => G<PerkValueType>(); set => S(value); }

    }
}
