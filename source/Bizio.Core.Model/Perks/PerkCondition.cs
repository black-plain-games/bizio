﻿using Bizio.Core.Data.Perks;
using Bizio.Core.Model;

namespace Bizio.Core.Model.Perks
{
    public class PerkCondition : NotifyOnPropertyChanged
    {
        public PerkTargetType Target { get => G<PerkTargetType>(); set => S(value); }
        public PerkConditionAttribute Attribute { get => G<PerkConditionAttribute>(); set => S(value); }
        public int Value { get => G<int>(); set => S(value); }
        public PerkConditionComparisonType Comparison { get => G<PerkConditionComparisonType>(); set => S(value); }
    }
}
