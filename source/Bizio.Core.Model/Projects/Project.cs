﻿using Bizio.Core.Data.Projects;
using Bizio.Core.Model;
using System;
using System.Collections.ObjectModel;

namespace Bizio.Core.Model.Projects
{
    public class Project : NotifyOnPropertyChanged, IIdentifiable<Guid>
    {
        public Guid Id { get => G<Guid>(); set => S(value); }
        public ProjectDefinition ProjectDefinition { get => G<ProjectDefinition>(); set => S(value); }
        public int Value { get => G<int>(); set => S(value); }
        public DateTime Deadline { get => G<DateTime>(); set => S(value); }
        public DateTime? ExtensionDeadline { get => G<DateTime?>(); set => S(value); }
        public ObservableCollection<ProjectRequirement> Requirements { get => G<ObservableCollection<ProjectRequirement>>(); set => S(value); }
        public StarCategory ReputationRequired { get => G<StarCategory>(); set => S(value); }
        public ProjectStatus Status { get => G<ProjectStatus>(); set => S(value); }
        public ProjectResult? Result { get => G<ProjectResult?>(); set => S(value); }
    }
}
