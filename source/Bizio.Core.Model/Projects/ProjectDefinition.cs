﻿using Bizio.Core.Model;
using Bizio.Core.Utilities;
using System;
using System.Collections.ObjectModel;

namespace Bizio.Core.Model.Projects
{
    public class ProjectDefinition : NotifyOnPropertyChanged, IIdentifiable<Guid>
    {
        public Guid Id { get => G<Guid>(); set => S(value); }
        public string Name { get => G<string>(); set => S(value); }
        public string Description { get => G<string>(); set => S(value); }
        public Range<int> Value { get => G<Range<int>>(); set => S(value); }
        public Range<int> ProjectLength { get => G<Range<int>>(); set => S(value); }
        public ObservableCollection<ProjectDefinitionSkill> Skills { get => G<ObservableCollection<ProjectDefinitionSkill>>(); set => S(value); }
    }
}
