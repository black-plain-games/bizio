﻿using Bizio.Core.Model.People;
using Bizio.Core.Utilities;

namespace Bizio.Core.Model.Projects
{
    public class ProjectDefinitionSkill : NotifyOnPropertyChanged
    {
        public SkillDefinition SkillDefinition { get => G<SkillDefinition>(); set => S(value); }
        public Range<int> Value { get => G<Range<int>>(); set => S(value); }
        public bool IsRequired { get => G<bool>(); set => S(value); }
    }
}
