﻿using Bizio.Core.Model;
using Bizio.Core.Model.People;

namespace Bizio.Core.Model.Projects
{
    public class ProjectRequirement : NotifyOnPropertyChanged
    {
        public SkillDefinition SkillDefinition { get => G<SkillDefinition>(); set => S(value); }
        public int TargetValue { get => G<int>(); set => S(value); }
        public int CurrentValue { get => G<int>(); set => S(value); }

    }
}
