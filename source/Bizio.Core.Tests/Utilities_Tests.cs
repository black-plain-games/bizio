﻿using Bizio.Core.Utilities;
using FluentAssertions;
using System;
using System.Collections.Generic;
using Xunit;

namespace Bizio.Core.Tests
{
    public class Utilities_Tests
    {
        public enum TestEnum
        {
            Value1 = 1,
            Value2 = 2
        }

        private static readonly decimal MillisecondsPerYear = (decimal)TimeSpan.FromDays(365).TotalMilliseconds;

        [Theory]
        [InlineData(3.77, .25, 3.75)]
        [InlineData(-1.1, .2, -1)]
        public void ClampTo_Succeeds(decimal value, decimal increment, decimal expected)
        {
            var result = Utilities.Utilities.ClampTo(value, increment);

            result
                .Should()
                .Be(expected);
        }

        [Theory]
        [InlineData(TestEnum.Value1, TestEnum.Value1)]
        [InlineData(TestEnum.Value2, TestEnum.Value2)]
        [InlineData(1, TestEnum.Value1)]
        [InlineData(2, TestEnum.Value2)]
        [InlineData("1", TestEnum.Value1)]
        [InlineData("2", TestEnum.Value2)]
        [InlineData("Value1", TestEnum.Value1)]
        [InlineData("Value2", TestEnum.Value2)]
        public void ParseEnum_Succeeds(object input, TestEnum expected)
        {
            var result = Utilities.Utilities.ParseEnum<TestEnum>(input);

            result
                .Should()
                .Be(expected);
        }

        [Theory]
        [InlineData(3)]
        [InlineData("3")]
        [InlineData("pie")]
        public void ParseEnum_Fails(object input)
        {
            Action action = () => Utilities.Utilities.ParseEnum<TestEnum>(input);

            action
                .Should()
                .Throw<ArgumentException>();
        }

        [Fact]
        public void GetRandomDate_Succeeds()
        {
            var minDate = DateTime.Now;

            var maxDate = minDate.AddMilliseconds((double)Utilities.Utilities.GetRandomDecimal(0, MillisecondsPerYear));

            var result = Utilities.Utilities.GetRandomDate(minDate, maxDate);

            result
                .Should()
                .BeOnOrAfter(minDate)
                .And
                .BeOnOrBefore(maxDate)
                .And
                .Subject.Value.TimeOfDay
                .Should()
                .Be(TimeSpan.Zero);
        }

        [Fact]
        public void GetRandomDate_Fails()
        {
            var minDate = DateTime.Now;

            var maxDate = minDate.AddMilliseconds(-(double)Utilities.Utilities.GetRandomDecimal(0, MillisecondsPerYear));

            Action action = () => Utilities.Utilities.GetRandomDate(minDate, maxDate);

            action
                .Should()
                .Throw<ArgumentException>();
        }

        [Fact]
        public void GetRandomDate_WithTimePart_Succeeds()
        {
            var minDate = DateTime.Now;

            var maxDate = minDate.AddMilliseconds((double)Utilities.Utilities.GetRandomDecimal(0, MillisecondsPerYear));

            var result = Utilities.Utilities.GetRandomDate(minDate, maxDate, false);

            result
                .Should()
                .BeOnOrAfter(minDate)
                .And
                .BeOnOrBefore(maxDate)
                .And
                .Subject.Value.TimeOfDay
                .Should()
                .NotBe(TimeSpan.Zero);
        }

        [Theory]
        [InlineData(new TestEnum[] { })]
        [InlineData(new[] { TestEnum.Value2 })]
        public void GetRandomValue_Exclusions_Succeeds(TestEnum[] exclusions)
        {
            var result = Utilities.Utilities.GetRandomValue(exclusions);

            exclusions
                .Should()
                .NotContain(result);
        }

        [Fact]
        public void GetRandomValue_AllExclusions_Succeeds()
        {
            var exclusions = new[] { TestEnum.Value1, TestEnum.Value2 };

            var result = Utilities.Utilities.GetRandomValue(exclusions);

            result.
                Should().
                Be(default(TestEnum));
        }

        [Theory]
        [InlineData(1, 5)]
        [InlineData(-10, -4)]
        [InlineData(-2, 5)]
        [InlineData(80, int.MaxValue)]
        [InlineData(int.MinValue, 4)]
        [InlineData(int.MinValue, int.MaxValue)]
        public void GetRanged_Int(int min, int max)
        {
            var result = Utilities.Utilities.GetRanged(new Range<int>(min, max));

            result.
                Should()
                .BeInRange(min, max);
        }

        public static IEnumerable<object[]> GetRanged_Decimal_TestCases
        {
            get
            {
                yield return new object[] { 1m, 5m };
                yield return new object[] { -10m, -4m };
                yield return new object[] { -2m, 5m };
                yield return new object[] { 80m, decimal.MaxValue };
                yield return new object[] { decimal.MinValue, 4m };
                yield return new object[] { decimal.MinValue, decimal.MaxValue };
            }
        }

        [Theory]
        [MemberData(nameof(GetRanged_Decimal_TestCases))]
        public void GetRanged_Decimal(decimal min, decimal max)
        {
            var result = Utilities.Utilities.GetRanged(new Range<decimal>(min, max));

            result.
                Should()
                .BeInRange(min, max);
        }

        public static IEnumerable<object[]> GetRanged_Decimal_Precision_TestCases
        {
            get
            {
                yield return new object[] { -100000m, 100000m, 1 };
                yield return new object[] { -100000m, 100000m, 2 };
                yield return new object[] { -100000m, 100000m, 3 };
                yield return new object[] { -100000m, 100000m, 4 };
                yield return new object[] { -100000m, 100000m, 5 };
                yield return new object[] { -100000m, 100000m, 6 };
            }
        }

        [Theory]
        [MemberData(nameof(GetRanged_Decimal_Precision_TestCases))]
        public void GetRanged_Decimal_Precision(decimal min, decimal max, int precision)
        {
            var result = Utilities.Utilities.GetRanged(new Range<decimal>(min, max), precision);

            var wholePart = decimal.Truncate(result);

            var value = result - wholePart;

            for (var decimalPlace = precision; decimalPlace > 0; decimalPlace--)
            {
                value *= 10m;

                wholePart = decimal.Truncate(value);

                value -= wholePart;
            }

            value
                .Should()
                .Be(decimal.Zero);
        }

        [Theory]
        [InlineData("test", "TEST", true)]
        [InlineData("test", "TesT", true)]
        [InlineData("two Words", "two words", true)]
        [InlineData("not equal", "EQUAl", false)]
        public void CompareStrings_IgnoreCase(string value1, string value2, bool areEqual)
        {
            var result = Utilities.Utilities.CompareStrings(value1, value2, true);

            result
                .Should()
                .Be(areEqual);
        }
    }
}