﻿using Bizio.Core;
using Bizio.Core.Model.Companies;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Bizio.Core
{
    public static class CollectionExtensions
    {
        public static IDictionary<Guid, TValue> ToDictionary<TValue>(this IEnumerable<TValue> collection)
            where TValue : IIdentifiable<Guid>
        {
            return collection.ToDictionary(i => i.Id);
        }

        public static ObservableCollection<T> Observe<T>(this IEnumerable<T> collection)
        {
            return new ObservableCollection<T>(collection);
        }

        public static T Find<T, TId>(this IEnumerable<T> collection, TId id)
            where T : IIdentifiable<TId>
        {
            return collection.FirstOrDefault(i => i.Id.Equals(id));
        }

        public static void RemoveWhere<T>(this ICollection<T> collection, Predicate<T> predicate)
        {
            var matches = collection.Where(x => predicate(x)).ToList();

            foreach (var match in matches)
            {
                collection.Remove(match);
            }
        }

        public static void RemoveAction<T>(this ICollection<IActionData> collection, Predicate<T> predicate)
            where T : IActionData
        {
            collection.RemoveWhere(x => x is T t && predicate(t));
        }
    }
}