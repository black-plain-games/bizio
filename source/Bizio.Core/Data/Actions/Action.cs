﻿using System;
using System.Collections.Generic;

namespace Bizio.Core.Data.Actions
{
    public class Action : IAction
    {
        public Guid Id { get; set; }
        public ActionType Type { get; set; }
        public string Name { get; set; }
        public IEnumerable<ActionRequirement> Requirements { get; set; }
        public int MaxCount { get; set; }
        public int DefaultCount { get; set; }
    }
}