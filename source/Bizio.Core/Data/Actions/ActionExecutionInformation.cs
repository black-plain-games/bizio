﻿namespace Bizio.Core.Data.Actions
{
    public struct ActionExecutionInformation
    {
        public bool CanPerformAction;
        public string Message;
    }
}