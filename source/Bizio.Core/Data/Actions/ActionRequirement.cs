﻿using System;

namespace Bizio.Core.Data.Actions
{
    public class ActionRequirement
    {
        public Guid SkillDefinitionId { get; set; }
        public int Value { get; set; }
    }
}