﻿namespace Bizio.Core.Data.Actions
{
    public enum ActionType
    {
        None = 0,
        InterviewProspect,
        MakeOffer,
        FireEmployee,
        AdjustSalary,
        AdjustAllocation,
        AcceptProject,
        RequestExtension,
        SubmitProject,
        PurchasePerk,
        SellPerk,
        ChangeCompanyMessageStatus,
        ToggleCompanyAction
    }
}