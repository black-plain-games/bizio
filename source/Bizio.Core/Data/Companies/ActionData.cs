﻿using Bizio.Core.Data.Actions;

namespace Bizio.Core.Data.Companies
{
    public class ActionData
    {
        public ActionType ActionType { get; set; }
        public string DataType { get; set; }
        public string Data { get; set; }
    }
}
