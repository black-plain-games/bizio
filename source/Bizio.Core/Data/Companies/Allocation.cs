﻿using System;

namespace Bizio.Core.Data.Companies
{
    public class Allocation
    {
        public Guid EmployeeId { get; set; }
        public Guid ProjectId { get; set; }
        public ProjectRole Role { get; set; }
        public int Percent { get; set; }
    }
}