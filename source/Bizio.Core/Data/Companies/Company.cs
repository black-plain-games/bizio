﻿using Bizio.Core.Data.Projects;
using System;
using System.Collections.Generic;

namespace Bizio.Core.Data.Companies
{
    public class Company : IIdentifiable<Guid>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public long Money { get; set; }
        public Guid? UserId { get; set; }
        public int InitialAccuracy { get; set; }
        public Reputation Reputation { get; set; }
        public CompanyStatus Status { get; set; }
        public ICollection<Prospect> Prospects { get; set; }
        public ICollection<Employee> Employees { get; set; }
        public ICollection<Project> Projects { get; set; }
        public ICollection<Allocation> Allocations { get; set; }
        public ICollection<CompanyPerk> Perks { get; set; }
        public ICollection<Transaction> Transactions { get; set; }
        public ICollection<CompanyAction> Actions { get; set; }
        public ICollection<CompanyMessage> Messages { get; set; }
        public ICollection<ActionData> TurnActions { get; set; }
    }
}
