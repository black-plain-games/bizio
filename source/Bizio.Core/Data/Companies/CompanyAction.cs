﻿using System;
using System.Collections.Generic;

namespace Bizio.Core.Data.Companies
{
    public class CompanyAction
    {
        public Guid ActionId { get; set; }
        public IEnumerable<CompanyActionAccumulation> Accumulations { get; set; }
        public int Count { get; set; }
        public bool IsActive { get; set; }
    }
}