﻿using System;

namespace Bizio.Core.Data.Companies
{
    public class CompanyActionAccumulation
    {
        public Guid SkillDefinitionId { get; set; }
        public int Value { get; set; }
    }
}