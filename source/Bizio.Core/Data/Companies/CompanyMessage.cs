﻿using System;

namespace Bizio.Core.Data.Companies
{
    public class CompanyMessage : IIdentifiable<Guid>
    {
        public Guid Id { get; set; }
        public CompanyMessageStatus Status { get; set; }
        public DateTime DateCreated { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public string Source { get; set; }
    }
}