﻿namespace Bizio.Core.Data.Companies
{
    public enum CompanyMessageStatus : byte
    {
        None,
        UnRead = 1,
        Read,
        Deleted
    }
}