﻿using System;

namespace Bizio.Core.Data.Companies
{
    public class CompanyPerk
    {
        public Guid PerkId { get; set; }
        public DateTime? NextPaymentDate { get; set; }
    }
}