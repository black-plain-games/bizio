﻿namespace Bizio.Core.Data.Companies
{
    public enum CompanyStatus
    {
        InBusiness = 1,
        Bankrupt = 2
    }
}