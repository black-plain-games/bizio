﻿using Bizio.Core.Data.People;
using System;

namespace Bizio.Core.Data.Companies
{
    public class Employee : IIdentifiable<Guid>
    {
        public Guid Id { get; set; }
        public Person Person { get; set; }
        public bool IsFounder { get; set; }
        public int Happiness { get; set; }
        public int Salary { get; set; }
        public DateTime HireDate { get; set; }
    }
}