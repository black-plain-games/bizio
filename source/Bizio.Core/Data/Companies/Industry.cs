﻿using Bizio.Core.Data.People;
using Bizio.Core.Data.Perks;
using Bizio.Core.Data.Projects;
using System;
using System.Collections.Generic;

namespace Bizio.Core.Data.Companies
{
    public class Industry : IIdentifiable<Guid>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<Guid> MandatorySkills { get; set; }
        public IEnumerable<Guid> OptionalSkills { get; set; }
        public IEnumerable<string> CompanyNames { get; set; }
        public IEnumerable<Profession> Professions { get; set; }
        public IEnumerable<ProjectDefinition> ProjectDefinitions { get; set; }
        public IEnumerable<Perk> Perks { get; set; }
    }
}