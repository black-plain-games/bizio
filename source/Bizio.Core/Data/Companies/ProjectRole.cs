﻿namespace Bizio.Core.Data.Companies
{
    public enum ProjectRole : byte
    {
        None,
        Training = 1,
        Staff,
        Lead,
        Manager
    }
}