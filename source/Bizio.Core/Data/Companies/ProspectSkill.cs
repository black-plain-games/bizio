﻿using Bizio.Core.Utilities;
using System;

namespace Bizio.Core.Data.Companies
{
    public class ProspectSkill
    {
        public Guid SkillDefinitionId { get; set; }
        public Range<int> Value { get; set; }
    }
}