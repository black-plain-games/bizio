﻿namespace Bizio.Core.Data.Companies
{
    public class Reputation
    {
        public int EarnedStars { get; set; }
        public int PossibleStars { get; set; }
    }
}