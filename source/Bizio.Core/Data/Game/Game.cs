﻿using Bizio.Core.Data.Companies;
using Bizio.Core.Data.Configuration;
using Bizio.Core.Data.People;
using Bizio.Core.Data.Projects;
using System.Collections.Generic;

namespace Bizio.Core.Data.Game
{
    public class Game : GameMetadata
    {
        public IEnumerable<Company> Companies { get; set; }
        public IEnumerable<Project> Projects { get; set; }
        public IEnumerable<Person> People { get; set; }
        public IDictionary<ConfigurationKey, string> Configuration { get; set; }
    }
}