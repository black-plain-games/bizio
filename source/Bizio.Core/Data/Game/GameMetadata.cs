﻿using System;

namespace Bizio.Core.Data.Game
{
    public class GameMetadata : IIdentifiable<Guid>
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public string UserCompanyName { get; set; }
        public string FounderFirstName { get; set; }
        public string FounderLastName { get; set; }
        public int DaysPerTurn { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
        public GameStatus Status { get; set; }

        public DateTime StartDate { get; set; }
        public int CurrentTurn { get; set; }

        public Guid IndustryId { get; set; }
    }
}