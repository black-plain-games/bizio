﻿namespace Bizio.Core.Data.Game
{
    public enum GameStatus
    {
        Playing = 1,
        UserLost = 2
    }
}