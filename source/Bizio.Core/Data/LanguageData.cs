﻿using Bizio.Core.Data.Text;
using System.Collections.Generic;

namespace Bizio.Core.Data
{
    public class LanguageData
    {
        public IEnumerable<Language> Languages { get; set; }
        public IEnumerable<TextResourceKey> TextResourceKeys { get; set; }
    }
}
