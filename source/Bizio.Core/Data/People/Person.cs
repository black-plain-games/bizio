﻿using System;
using System.Collections.Generic;

namespace Bizio.Core.Data.People
{
    public class Person : IIdentifiable<Guid>
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Birthday { get; set; }
        public Guid? ProfessionId { get; set; }
        public ICollection<Skill> Skills { get; set; }
        public Gender Gender { get; set; }
        public DateTime RetirementDate { get; set; }
        public Guid PersonalityId { get; set; }
        public ICollection<WorkHistory> WorkHistory { get; set; }
    }
}
