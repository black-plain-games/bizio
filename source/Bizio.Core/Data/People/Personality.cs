﻿using System;
using System.Collections.Generic;

namespace Bizio.Core.Data.People
{
    /// <summary>
    /// Represents the descriptive information for a personality.
    /// </summary>
    public class Personality : IIdentifiable<Guid>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public IEnumerable<PersonalityAttribute> Attributes { get; set; }
    }
}