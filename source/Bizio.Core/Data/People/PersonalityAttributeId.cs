﻿namespace Bizio.Core.Data.People
{
    /// <summary>
    /// Defines the various personality attribute data points.
    /// </summary>
    public enum PersonalityAttributeId : byte
    {
        None,
        MinimumRetirementAge = 1,
        MoneyFactor = 2,
        LiteralPerkFactor = 3,
        PercentagePerkFactor = 4,
        MandatorySkillFactor = 5,
        OptionalSkillFactor = 6,
        DesiredCompanySize = 7,
        CompanySizeImportance = 8,
        DesiredCompanyGrowth = 9,
        CompanyGrowthImportance = 10,
        MinimumDesiredEmployeeWorkload = 11,
        MaximumDesiredEmployeeWorkload = 12,
        MinimumPerkCount = 13,
        MinimumDesiredRelevance = 14,
        MaximumDesiredRelevance = 15,
        MaximumOfferReasons = 16,

        // Probably need to delete everything before here
        DesiredProspectPoolSize = 17,
        HireProspectUrgency = 18,
        DesiredEmployeesPerProject = 19,
        DesiredProspectAccuracy = 20,
        HirePersonUrgency = 21,
        DesiredProjectsPerEmployee = 22,
        MinimumProjectCompletionPercent = 23,
        WaitPeriodBeforeFirstSalaryAdjustment = 24,
        MandatorySkillMaxSalary = 25,
        OptionalSkillMaxSalary = 26,
        RequiredIncreaseForAdjustment = 27,
        RequiredDecreaseForAdjustment = 28,
        MinimumRetentionTurns = 29,
        MinimumMoneyPercentForOffer = 30,
        FireEmployeeLookbackWeeks = 31,
        FireEmployeeMinimumPositivePayrollPecent = 32,
        FireEmployeeLookbackProjectCount = 33,
        FireEmployeeMinimumSkillValue = 34,
        SellPerkDangerCapitolThreshold = 35,
        SellPerkTurnsAboveThreshold = 36,
        OfferGreed = 37,
    }
}