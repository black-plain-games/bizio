﻿using System;
using System.Collections.Generic;

namespace Bizio.Core.Data.People
{
    public class Profession : IIdentifiable<Guid>
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public IEnumerable<ProfessionSkillDefinition> SkillDefinitions { get; set; }
    }
}