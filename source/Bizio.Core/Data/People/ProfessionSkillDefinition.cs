﻿using System;

namespace Bizio.Core.Data.People
{
    public class ProfessionSkillDefinition
    {
        public Guid SkillDefinitionId { get; set; }

        public int Weight { get; set; }
    }
}