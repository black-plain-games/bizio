﻿using System;

namespace Bizio.Core.Data.People
{
    public class Skill
    {
        public Guid SkillDefinitionId { get; set; }
        public int Value { get; set; }
        public int LearnRate { get; set; }
        public int ForgetRate { get; set; }
    }
}