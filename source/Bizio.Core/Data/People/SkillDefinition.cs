﻿using Bizio.Core.Utilities;
using System;

namespace Bizio.Core.Data.People
{
    /// <summary>
    /// Represents a type of skill.
    /// </summary>
    public class SkillDefinition : IIdentifiable<Guid>
    {
        /// <summary>
        /// Gets or sets the unique id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the name (description)
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the range of allowable values for any instance of this skill.
        /// </summary>
        public Range<byte> Value { get; set; }
    }
}