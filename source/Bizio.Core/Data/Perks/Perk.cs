﻿using System;
using System.Collections.Generic;

namespace Bizio.Core.Data.Perks
{
    public class Perk : IIdentifiable<Guid>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? MaxCount { get; set; }
        public int? InitialCost { get; set; }
        public int? RecurringCost { get; set; }
        public int? RecurringCostInterval { get; set; }
        public IEnumerable<PerkCondition> Conditions { get; set; }
        public IEnumerable<PerkBenefit> Benefits { get; set; }
    }
}