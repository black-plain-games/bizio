﻿namespace Bizio.Core.Data.Perks
{
    /// <summary>
    /// Defines the possible attributes that can be benefited.
    /// </summary>
    public enum PerkBenefitAttribute
    {
        // Company conditions

        Money = 1,

        // Person conditions

        // Employee conditions

        Happiness = 2
    }
}