﻿namespace Bizio.Core.Data.Perks
{
    /// <summary>
    /// Represents a comparison that must be true for a perk to apply
    /// to a given target.
    /// </summary>
    public class PerkCondition
    {
        public PerkTargetType Target { get; set; }
        public PerkConditionAttribute Attribute { get; set; }
        public int Value { get; set; }
        public PerkConditionComparisonType Comparison { get; set; }
    }
}