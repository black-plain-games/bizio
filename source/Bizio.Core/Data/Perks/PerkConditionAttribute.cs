﻿namespace Bizio.Core.Data.Perks
{
    /// <summary>
    /// Defines the possible attributes that can be evaluated.
    /// </summary>
    public enum PerkConditionAttribute : byte
    {
        None,

        // Company conditions

        Money = 1,
        PerkCount = 2,
        ProjectCount = 3,
        ProspectCount = 4,
        EmployeeCount = 5,

        // Person conditions

        SkillCount = 6,

        // Employee conditions

        Happiness = 7
    }
}