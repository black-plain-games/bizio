﻿namespace Bizio.Core.Data.Perks
{
    /// <summary>
    /// Defines the possible comparison operators.
    /// </summary>
    public enum PerkConditionComparisonType : byte
    {
        Equal = 1,
        NotEqual,
        LessThan,
        LessThanEqualTo,
        GreaterThan,
        GreaterThanEqualTo
    }
}