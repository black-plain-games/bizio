﻿namespace Bizio.Core.Data.Perks
{
    /// <summary>
    /// Defines how a value should be interpreted on a condition.
    /// </summary>
    public enum PerkValueType : byte
    {
        Literal = 1,
        Percentage
    }
}