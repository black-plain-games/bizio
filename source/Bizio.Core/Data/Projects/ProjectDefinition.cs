﻿using Bizio.Core.Utilities;
using System;
using System.Collections.Generic;

namespace Bizio.Core.Data.Projects
{
    public class ProjectDefinition : IIdentifiable<Guid>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Range<int> Value { get; set; }
        public Range<int> ProjectLength { get; set; }
        public IEnumerable<ProjectDefinitionSkill> Skills { get; set; }
    }
}