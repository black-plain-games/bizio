﻿using System;

namespace Bizio.Core.Data.Projects
{
    public class ProjectRequirement
    {
        public Guid SkillDefinitionId { get; set; }
        public int TargetValue { get; set; }
        public int CurrentValue { get; set; }
    }
}