﻿namespace Bizio.Core.Data.Projects
{
    /// <summary>
    /// Defines the stages of a project
    /// </summary>
    public enum ProjectStatus : byte
    {
        INVALID = 0,
        Unassigned,
        InProgress,
        Completed
    }
}