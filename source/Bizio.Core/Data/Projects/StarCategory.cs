﻿namespace Bizio.Core.Data.Projects
{
    /// <summary>
    /// Defines all possible categories of reputation.
    /// </summary>
    public enum StarCategory : byte
    {
        ZeroStar,
        OneStar,
        TwoStar,
        ThreeStar,
        FourStar,
        FiveStar
    }
}