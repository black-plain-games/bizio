﻿using System;

namespace Bizio.Core.Data.Text
{
    public class TextResourceKey : IIdentifiable<Guid>
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
