﻿namespace Bizio.Core
{
    public interface IIdentifiable<T>
    {
        T Id { get; set; }
    }

    public static class Extensions
    {
        public const bool IsDebug = true;

        public static string Label<T>(this IIdentifiable<T> self, params object[] extra)
        {
            if (!IsDebug)
            {
                return string.Empty;
            }

            var a = string.Join("", "[", $"{self.Id}"[30..], "]");
            var b = string.Join(" ", extra);

            return string.Join(" ", a, b);
        }

        public static string Label(this object self, params object[] extra)
        {
            if (!IsDebug)
            {
                return string.Empty;
            }

            return string.Join(" ", extra);
        }
    }
}