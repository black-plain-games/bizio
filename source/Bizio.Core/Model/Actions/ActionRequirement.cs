﻿using Bizio.Core.Model.People;

namespace Bizio.Core.Model.Actions
{
    public class ActionRequirement : NotifyOnPropertyChanged
    {
        public SkillDefinition SkillDefinition { get => G<SkillDefinition>(); set => S(value); }
        public int Value { get => G<int>(); set => S(value); }

        public override string ToString() => $"{SkillDefinition.Name} {Value}";
    }
}
