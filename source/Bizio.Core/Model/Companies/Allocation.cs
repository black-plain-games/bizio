﻿using Bizio.Core.Data.Companies;
using Bizio.Core.Model.Projects;

namespace Bizio.Core.Model.Companies
{
    public class Allocation : NotifyOnPropertyChanged
    {
        public Employee Employee { get => G<Employee>(); set => S(value); }
        public Project Project { get => G<Project>(); set => S(value); }
        public ProjectRole Role { get => G<ProjectRole>(); set => S(value); }
        public int Percent { get => G<int>(); set => S(value); }
    }
}
