﻿using Bizio.Core.Model.Projects;
using System;
using System.Collections.ObjectModel;

namespace Bizio.Core.Model.Companies
{
    public class Company : NotifyOnPropertyChanged, IIdentifiable<Guid>
    {
        public Guid Id { get => G<Guid>(); set => S(value); }
        public string Name { get => G<string>(); set => S(value); }
        public long Money { get => G<long>(); set => S(value); }
        public Guid? UserId { get => G<Guid?>(); set => S(value); }
        public int InitialAccuracy { get => G<int>(); set => S(value); }
        public Reputation Reputation { get => G<Reputation>(); set => S(value); }
        public Data.Companies.CompanyStatus Status { get => G<Data.Companies.CompanyStatus>(); set => S(value); }
        public ObservableCollection<Prospect> Prospects { get => G<ObservableCollection<Prospect>>(); set => S(value); }
        public ObservableCollection<Employee> Employees { get => G<ObservableCollection<Employee>>(); set => S(value); }
        public ObservableCollection<Project> Projects { get => G<ObservableCollection<Project>>(); set => S(value); }
        public ObservableCollection<Allocation> Allocations { get => G<ObservableCollection<Allocation>>(); set => S(value); }
        public ObservableCollection<CompanyPerk> Perks { get => G<ObservableCollection<CompanyPerk>>(); set => S(value); }
        public ObservableCollection<Transaction> Transactions { get => G<ObservableCollection<Transaction>>(); set => S(value); }
        public ObservableCollection<CompanyAction> Actions { get => G<ObservableCollection<CompanyAction>>(); set => S(value); }
        public ObservableCollection<CompanyMessage> Messages { get => G<ObservableCollection<CompanyMessage>>(); set => S(value); }
        public ObservableCollection<IActionData> TurnActions { get => G<ObservableCollection<IActionData>>(); set => S(value); }

        public override string ToString() => this.Label(Name);
    }
}
