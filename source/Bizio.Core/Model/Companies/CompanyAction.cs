﻿using System.Collections.ObjectModel;
using A = Bizio.Core.Model.Actions;

namespace Bizio.Core.Model.Companies
{
    public class CompanyAction : NotifyOnPropertyChanged
    {
        public A.Action Action { get => G<A.Action>(); set => S(value); }
        public ObservableCollection<CompanyActionAccumulation> Accumulations { get => G<ObservableCollection<CompanyActionAccumulation>>(); set => S(value); }
        public int Count { get => G<int>(); set => S(value); }
        public bool IsActive { get => G<bool>(); set => S(value); }

        public override string ToString() => $"{Action.Name} ({Count})";
    }
}
