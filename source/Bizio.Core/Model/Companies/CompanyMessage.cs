﻿using Bizio.Core.Data.Companies;
using System;

namespace Bizio.Core.Model.Companies
{
    public class CompanyMessage : NotifyOnPropertyChanged, IIdentifiable<Guid>
    {
        public Guid Id { get => G<Guid>(); set => S(value); }
        public CompanyMessageStatus Status { get => G<CompanyMessageStatus>(); set => S(value); }
        public DateTime DateCreated { get => G<DateTime>(); set => S(value); }
        public string Subject { get => G<string>(); set => S(value); }
        public string Message { get => G<string>(); set => S(value); }
        public string Source { get => G<string>(); set => S(value); }

        public override string ToString() => $"{Subject}";
    }
}
