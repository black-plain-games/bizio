﻿using Bizio.Core.Model.People;
using System;

namespace Bizio.Core.Model.Companies
{
    public class Employee : NotifyOnPropertyChanged, IIdentifiable<Guid>
    {
        public Guid Id { get => G<Guid>(); set => S(value); }
        public Person Person { get => G<Person>(); set => S(value); }
        public bool IsFounder { get => G<bool>(); set => S(value); }
        public int Happiness { get => G<int>(); set => S(value); }
        public int Salary { get => G<int>(); set => S(value); }
        public DateTime HireDate { get => G<DateTime>(); set => S(value); }

        public override string ToString() => this.Label(Person.FirstName, Person.LastName);
    }
}
