﻿using Bizio.Core.Data.Actions;

namespace Bizio.Core.Model.Companies
{
    public interface IActionData
    {
        ActionType ActionType { get; }

        string Serialize();
    }
}