﻿using Bizio.Core.Model.People;
using Bizio.Core.Model.Perks;
using Bizio.Core.Model.Projects;
using System;
using System.Collections.ObjectModel;

namespace Bizio.Core.Model.Companies
{
    public class Industry : NotifyOnPropertyChanged, IIdentifiable<Guid>
    {
        public Guid Id { get => G<Guid>(); set => S(value); }
        public string Name { get => G<string>(); set => S(value); }
        public ObservableCollection<SkillDefinition> MandatorySkills { get => G<ObservableCollection<SkillDefinition>>(); set => S(value); }
        public ObservableCollection<SkillDefinition> OptionalSkills { get => G<ObservableCollection<SkillDefinition>>(); set => S(value); }
        public ObservableCollection<string> CompanyNames { get => G<ObservableCollection<string>>(); set => S(value); }
        public ObservableCollection<Profession> Professions { get => G<ObservableCollection<Profession>>(); set => S(value); }
        public ObservableCollection<ProjectDefinition> ProjectDefinitions { get => G<ObservableCollection<ProjectDefinition>>(); set => S(value); }
        public ObservableCollection<Perk> Perks { get => G<ObservableCollection<Perk>>(); set => S(value); }
    }
}
