﻿using Bizio.Core.Model.People;
using Bizio.Core.Utilities;
using System;
using System.Collections.ObjectModel;

namespace Bizio.Core.Model.Companies
{
    public class Prospect : NotifyOnPropertyChanged, IIdentifiable<Guid>
    {
        public Guid Id { get => G<Guid>(); set => S(value); }
        public Person Person { get => G<Person>(); set => S(value); }
        public int Accuracy { get => G<int>(); set => S(value); }
        public ObservableCollection<ProspectSkill> Skills { get => G<ObservableCollection<ProspectSkill>>(); set => S(value); }
        public Range<int> Salary { get => G<Range<int>>(); set => S(value); }

        public override string ToString() => this.Label(Person.FirstName, Person.LastName);
    }
}
