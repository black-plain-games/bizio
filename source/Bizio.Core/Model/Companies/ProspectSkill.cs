﻿using Bizio.Core.Model.People;
using Bizio.Core.Utilities;

namespace Bizio.Core.Model.Companies
{
    public class ProspectSkill : NotifyOnPropertyChanged
    {
        public SkillDefinition SkillDefinition { get => G<SkillDefinition>(); set => S(value); }
        public Range<int> Value { get => G<Range<int>>(); set => S(value); }

        public override string ToString() => $"{SkillDefinition.Name} ({Value.Minimum}-{Value.Maximum})";
    }
}
