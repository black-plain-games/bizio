﻿namespace Bizio.Core.Model.Companies
{
    public class Reputation : NotifyOnPropertyChanged
    {
        public int EarnedStars
        {
            get => G<int>();
            set
            {
                S(value);
                Value = PossibleStars == 0 ? 0 : (float)EarnedStars / PossibleStars;
            }
        }

        public int PossibleStars
        {
            get => G<int>();
            set
            {
                S(value);
                Value = PossibleStars == 0 ? 0 : (float)EarnedStars / PossibleStars;
            }
        }

        public float Value { get => G<float>(); set => S(value); }
    }
}
