﻿using Bizio.Core.Model.Companies;
using Bizio.Core.Model.People;
using Bizio.Core.Model.Projects;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Bizio.Core.Model.Game
{
    public class Game : GameMetadata
    {
        public ObservableCollection<Company> Companies { get => G<ObservableCollection<Company>>(); set => S(value); }
        public ObservableCollection<Project> Projects { get => G<ObservableCollection<Project>>(); set => S(value); }
        public ObservableCollection<Person> People { get => G<ObservableCollection<Person>>(); set => S(value); }
        public IDictionary<Data.Configuration.ConfigurationKey, string> Configuration { get => G<IDictionary<Data.Configuration.ConfigurationKey, string>>(); set => S(value); }
    }
}
