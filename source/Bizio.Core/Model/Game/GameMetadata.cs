﻿using Bizio.Core.Model.Companies;
using System;

namespace Bizio.Core.Model.Game
{
    public class GameMetadata : NotifyOnPropertyChanged, IIdentifiable<Guid>
    {
        public Guid Id { get => G<Guid>(); set => S(value); }
        public Guid UserId { get => G<Guid>(); set => S(value); }
        public string UserCompanyName { get => G<string>(); set => S(value); }
        public string FounderFirstName { get => G<string>(); set => S(value); }
        public string FounderLastName { get => G<string>(); set => S(value); }
        public int DaysPerTurn { get => G<int>(); set => S(value); }
        public DateTimeOffset CreatedDate { get => G<DateTimeOffset>(); set => S(value); }
        public Data.Game.GameStatus Status { get => G<Data.Game.GameStatus>(); set => S(value); }

        public DateTime StartDate { get => G<DateTime>(); set => S(value); }
        public int CurrentTurn
        {
            get => G<int>(); set
            {
                S(value);
                OnPropertyChanged(nameof(CurrentDate));
            }
        }

        public Industry Industry { get => G<Industry>(); set => S(value); }

        public DateTime CurrentDate => StartDate.AddDays(CurrentTurn);

        public override string ToString() => this.Label(UserCompanyName, "Turn:", CurrentTurn);
    }
}
