﻿using System.Collections.Concurrent;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace Bizio.Core.Model
{
    public class NotifyOnPropertyChanged : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public async void OnPropertyChanged([CallerMemberName] string property = "")
        {
            var args = _args.GetOrAdd(property, x => new PropertyChangedEventArgs(x));

            await UIManager.ExecuteAsync(() =>
            {
                PropertyChanged?.Invoke(this, args);
            });
        }

        protected NotifyOnPropertyChanged()
        {
            _args = new ConcurrentDictionary<string, PropertyChangedEventArgs>();

            _fields = new ConcurrentDictionary<string, object>();
        }

        protected T? G<T>([CallerMemberName] string name = "")
        {
            return G<T>(default, name);
        }

        protected T? G<T>(T? defaultValue, [CallerMemberName] string name = "")
        {
            var output = defaultValue;

            if (_fields.TryGetValue(name, out var value))
            {
                try
                {
                    output = (T)value;
                }
                catch
                {
                    Debug.WriteLine($"Failed to convert field '{name}' of type '{value.GetType()}' to type '{typeof(T)}'.");
                }
            }

            return output;
        }

        protected bool S(object value, bool doNotify = true, [CallerMemberName] string property = "")
        {
            var didChange = true;

            _fields.AddOrUpdate(property, value, (key, oldValue) =>
            {
                didChange = oldValue != value;
                return value;
            });

            if (didChange && doNotify)
            {
                OnPropertyChanged(property);
            }

            return didChange;
        }

        protected bool S<T>(ObservableCollection<T> value, bool doNotify = true, [CallerMemberName] string property = "")
        {
            // This shouldn't be necessary, but it is

            var didChange = true;

            void OnCollectionChanged(object s, NotifyCollectionChangedEventArgs e)
            {
                OnPropertyChanged(property);
            }

            _fields.AddOrUpdate(property, value, (key, oldValue) =>
            {
                didChange = oldValue != value;

                if (didChange &&
                    oldValue is ObservableCollection<T> oldCollection &&
                    oldValue != null)
                {
                    oldCollection.CollectionChanged -= OnCollectionChanged;
                }

                return value;
            });

            if (didChange && value != null)
            {
                value.CollectionChanged += OnCollectionChanged;
            }

            if (didChange && doNotify)
            {
                OnPropertyChanged(property);
            }

            return didChange;
        }

        private readonly ConcurrentDictionary<string, PropertyChangedEventArgs> _args;

        private readonly ConcurrentDictionary<string, object> _fields;
    }
}