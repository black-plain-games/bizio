﻿using Bizio.Core.Data.People;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Core.Model.People
{
    public class Personality : NotifyOnPropertyChanged, IIdentifiable<Guid>
    {
        public Guid Id { get => G<Guid>(); set => S(value); }
        public string Name { get => G<string>(); set => S(value); }
        public string Description { get => G<string>(); set => S(value); }
        public IEnumerable<PersonalityAttribute> Attributes { get => G<IEnumerable<PersonalityAttribute>>(); set => S(value); }

        public int GetValue(PersonalityAttributeId attributeId) => Attributes.First(a => a.Id == attributeId).Value;
    }
}
