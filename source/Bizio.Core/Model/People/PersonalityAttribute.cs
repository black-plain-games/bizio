﻿using Bizio.Core.Data.People;
using Bizio.Core.Model;

namespace Bizio.Core.Model.People
{
    public class PersonalityAttribute : NotifyOnPropertyChanged
    {
        public PersonalityAttributeId Id { get => G<PersonalityAttributeId>(); set => S(value); }
        public int Value { get => G<int>(); set => S(value); }
    }
}
