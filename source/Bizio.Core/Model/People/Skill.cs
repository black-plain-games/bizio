﻿namespace Bizio.Core.Model.People
{
    public class Skill : NotifyOnPropertyChanged
    {
        public SkillDefinition SkillDefinition { get => G<SkillDefinition>(); set => S(value); }
        public int Value { get => G<int>(); set => S(value); }
        public int LearnRate { get => G<int>(); set => S(value); }
        public int ForgetRate { get => G<int>(); set => S(value); }

        public override string ToString() => $"{SkillDefinition.Name} ({Value})";
    }
}
