﻿using Bizio.Core.Model.Actions;
using Bizio.Core.Model.Companies;
using Bizio.Core.Model.People;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Bizio.Core.Model
{
    public class StaticData : NotifyOnPropertyChanged
    {
        public ObservableCollection<Action> Actions { get => G<ObservableCollection<Action>>(); set => S(value); }
        public ObservableCollection<SkillDefinition> SkillDefinitions { get => G<ObservableCollection<SkillDefinition>>(); set => S(value); }
        public ObservableCollection<Industry> Industries { get => G<ObservableCollection<Industry>>(); set => S(value); }
        public ObservableCollection<Personality> Personalities { get => G<ObservableCollection<Personality>>(); set => S(value); }
        public ObservableCollection<Person> DefaultPeople { get => G<ObservableCollection<Person>>(); set => S(value); }
        public IEnumerable<string> LastNames { get; set; }
        public IDictionary<Data.People.Gender, IEnumerable<string>> FirstNames { get; set; }
    }
}
