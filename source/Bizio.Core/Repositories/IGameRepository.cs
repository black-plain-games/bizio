﻿using Bizio.Core.Data.Configuration;
using Bizio.Core.Data.Game;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Bizio.Core.Repositories
{
    public interface IGameRepository
    {
        Task<IEnumerable<GameMetadata>> LoadAsync(CancellationToken cancellationToken = default);
        Task<Game> LoadAsync(Guid id, CancellationToken cancellationToken = default);
        Task SaveAsync(Game game, CancellationToken cancellationToken = default);
        Task DeleteAsync(Guid id, CancellationToken cancellationToken = default);
    }

    public interface IConfigurationRepository
    {
        Task<IDictionary<ConfigurationKey, string>> LoadAsync(CancellationToken cancellationToken = default);
        Task<IDictionary<ConfigurationKey, string>> LoadAsync(bool forceReload, CancellationToken cancellationToken = default);
    }
}