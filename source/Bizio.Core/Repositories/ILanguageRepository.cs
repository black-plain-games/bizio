﻿using Bizio.Core.Data;
using System.Threading;
using System.Threading.Tasks;

namespace Bizio.Core.Repositories
{
    public interface ILanguageRepository
    {
        Task<LanguageData> LoadAsync(bool forceReload, CancellationToken cancellationToken = default);
    }
}