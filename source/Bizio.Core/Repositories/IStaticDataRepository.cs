﻿using Bizio.Core.Data;
using System.Threading;
using System.Threading.Tasks;

namespace Bizio.Core.Repositories
{
    public interface IStaticDataRepository
    {
        Task<StaticData> LoadAsync(bool forceReload, CancellationToken cancellationToken = default);
    }
}