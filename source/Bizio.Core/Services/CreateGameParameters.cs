﻿using Bizio.Core.Data.Configuration;
using System;
using System.Collections.Generic;

namespace Bizio.Core.Services
{
    public record CreateGameParameters(
        Guid UserId,
        DateTime StartDate,
        Guid IndustryId,
        int DaysPerTurn,
        CreatePersonParameters Founder,
        string CompanyName,
        long InitialFunds,
        int NumberOfCompanies,
        IDictionary<ConfigurationKey, string> Configuration
    );
}