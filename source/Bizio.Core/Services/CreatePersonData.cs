﻿using Bizio.Core.Data.People;
using System;
using System.Collections.Generic;

namespace Bizio.Core.Services
{
    public record CreatePersonParameters(
        string FirstName,
        string LastName,
        Gender Gender,
        DateTime Birthday,
        Guid PersonalityId,
        IDictionary<Guid, int> Skills
    );
}