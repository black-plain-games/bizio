﻿using Bizio.Core.Data.Actions;
using Bizio.Core.Model.Companies;
using Bizio.Core.Model.Game;

namespace Bizio.Core.Services
{
    public interface IActionHandler
    {
        ActionType Type { get; }

        ActionExecutionInformation CanPerformAction(Company company, IActionData data);
        CompanyMessage ProcessAction(IActionData data, Game game, Company company);
    }
}