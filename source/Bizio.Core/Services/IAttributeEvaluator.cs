﻿using Bizio.Core.Data.Perks;

namespace Bizio.Core.Services
{
    /// <summary>
    /// Defines the structure of a strategy for evaluating attributes for perk
    /// application.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IAttributeEvaluator<T>
    {
        /// <summary>
        /// Gets the attribute being evaluated.
        /// </summary>
        PerkConditionAttribute Attribute { get; }

        /// <summary>
        /// Determines the value of the attribute.
        /// </summary>
        /// <param name="company">The object the attribute is being evaluated for.</param>
        /// <returns>The weighted value of the attribute.</returns>
        decimal Evaluate(T target);
    }
}