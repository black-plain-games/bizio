﻿using Bizio.Core.Model.Companies;
using Bizio.Core.Model.People;

namespace Bizio.Core.Services
{
    public interface IOfferEvaluator
    {
        OfferResponseReason? Evaluate(byte industryId, Company company, Person person, decimal amount);
    }
}