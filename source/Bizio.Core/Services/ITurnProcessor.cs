﻿using Bizio.Core.Model.Game;

namespace Bizio.Core.Services
{
    public interface ITurnProcessor
    {
        void ProcessTurn(Game game);
    }
}
