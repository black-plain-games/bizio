﻿using Bizio.Core.Model.People;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Core.Services
{
    /// <summary>
    /// Represents a person's reaction to an offer of employment.
    /// </summary>
    public class OfferResponse
    {
        /// <summary>
        /// Gets a flag indicating whether the offer was accepted.
        /// </summary>
        public bool DidAccept { get; }

        /// <summary>
        /// Gets an enumeration of reasons the offer was declined, if it was.
        /// </summary>
        public IEnumerable<OfferResponseReason> Reasons { get; }

        /// <summary>
        /// Initializes a new instance of the OfferResponse class.
        /// </summary>
        /// <param name="reasons">The reasons the offer was declined, if any.</param>
        public OfferResponse(IEnumerable<OfferResponseReason> reasons, int maxReasons)
        {
            DidAccept = reasons.Count() <= maxReasons;

            Reasons = reasons;
        }
    }
}