﻿namespace Bizio.Core.Model.People
{
    /// <summary>
    /// Defines the possible reasons an offer would be declined.
    /// </summary>
    public enum OfferResponseReason
    {
        None,
        OfferValue = 1,
        CompanySize,
        CompanyGrowth,
        EmployeeWorkload,
        Perks,
        Relevance
    }
}