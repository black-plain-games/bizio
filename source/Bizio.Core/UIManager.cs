﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Bizio.Core
{
    public static class UIManager
    {
        public static void SetTaskScheduler()
        {
            try
            {
                _taskScheduler = TaskScheduler.FromCurrentSynchronizationContext();
            }
            catch
            {
                _taskScheduler = TaskScheduler.Current;
            }
        }

        public static async Task ExecuteAsync(Action action, CancellationToken cancellationToken = default)
        {
            if (_taskScheduler == null)
            {
                action();

                return;
                //throw new InvalidOperationException("Cannot call ExecuteAsync before calling SetTaskScheduler");
            }

            await Task.Factory.StartNew(action, cancellationToken, TaskCreationOptions.None, _taskScheduler);
        }

        private static TaskScheduler? _taskScheduler;
    }
}