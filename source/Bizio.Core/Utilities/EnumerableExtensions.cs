﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Core.Utilities
{
    /// <summary>
    /// Provides extension methods to IEnumerable objects.
    /// </summary>
    public static class EnumerableExtensions
    {
        /// <summary>
        /// Selects a random element from the sequence.
        /// </summary>
        /// <typeparam name="T">The inner type of the collection.</typeparam>
        /// <param name="caller"></param>
        /// <returns>A randomly selected element from the collection.</returns>
        public static T Random<T>(this IEnumerable<T> caller)
        {
            return caller.ElementAtOrDefault(Utilities.GetRandomInt(0, caller.Count()));
        }

        /// <summary>
        /// Selects a random element from the sequence.
        /// </summary>
        /// <typeparam name="T">The inner type of the collection.</typeparam>
        /// <param name="caller"></param>
        /// <returns>A randomly selected element from the collection.</returns>
        public static T Random<T>(this IEnumerable<T> caller, Func<T, bool> predicate)
        {
            var filtered = caller.Where(predicate);

            return filtered.ElementAtOrDefault(Utilities.GetRandomInt(0, filtered.Count()));
        }
    }
}