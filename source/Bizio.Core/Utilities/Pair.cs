﻿namespace Bizio.Core.Utilities
{
    public class Pair<TId, TValue>(TId id, TValue value)
    {
        public TId Id { get; set; } = id;

        public TValue Value { get; set; } = value;
    }
}
