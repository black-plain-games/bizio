﻿using Bizio.Core.Model;

namespace Bizio.Core.Utilities
{
    public class Range<T> : NotifyOnPropertyChanged
    {
        public T Minimum { get => G<T>(); set => S(value); }
        public T Maximum { get => G<T>(); set => S(value); }

        public Range() { }

        public Range(T minimum, T maximum)
        {
            Minimum = minimum;
            Maximum = maximum;
        }
    }
}