﻿using Bizio.Core.Data.Companies;
using Bizio.Core.Data.Projects;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Core.Utilities
{
    public static class Utilities
    {
        public static readonly int InvalidId = -7;

        public static readonly int DefaultDecimalPrecision = 6;

        public static readonly decimal ReasonableDecimalMax = 1000000000000.0m;

        public static T ChangeType<T>(this object data)
        {
            return (T)Convert.ChangeType(data, typeof(T));
        }

        public static T ParseEnum<T>(this object data)
        {
            var output = (T)Enum.Parse(typeof(T), $"{data}");

            if (Enum.IsDefined(typeof(T), output))
            {
                return output;
            }

            throw new ArgumentException($"Value '{data}' not found in enum type '{typeof(T)}'");
        }

        public static bool GetRandomBool()
        {
            return GetRandomBool(50);
        }

        public static bool GetRandomBool(int trueChance)
        {
            return GetRandomInt(0, 100) < trueChance;
        }

        public static bool GetRandomBool(decimal trueChance)
        {
            var trueNumber = (int)(100 * trueChance);

            return GetRandomInt(0, 100) < trueNumber;
        }

        public static int GetRandomInt()
        {
            return R.Next();
        }

        public static int GetRandomInt(int max)
        {
            return R.Next(max);
        }

        public static int GetRandomInt(int min, int max)
        {
            return R.Next(min, max);
        }

        public static int GetRandomInt(Range<byte> range) => GetRandomInt(range.Minimum, range.Maximum);

        public static int GetRandomInt(Range<int> range) => GetRandomInt(range.Minimum, range.Maximum);

        public static decimal GetRandomDecimal()
        {
            return GetRandomDecimal(DEFAULT_MINIMUM_DECIMAL_VALUE, DEFAULT_MAXIMUM_DECIMAL_VALUE);
        }

        public static decimal GetRandomDecimal(decimal max)
        {
            return GetRandomDecimal(DEFAULT_MINIMUM_DECIMAL_VALUE, max);
        }

        public static byte GetRandomByte()
        {
            return (byte)R.Next(byte.MinValue, byte.MaxValue);
        }

        public static decimal GetRandomDecimal(decimal min, decimal max)
        {
            var range = max - min;

            var coef = (decimal)R.NextDouble();

            var result = min + (coef * range);

            return Math.Round(result, DefaultDecimalPrecision);
        }

        public static DateTime GetRandomDate()
        {
            return GetRandomDate(true);
        }

        public static DateTime GetRandomDate(bool onlyDatePart)
        {
            return GetRandomDate(DateTime.Today.AddYears(-100), DateTime.Today, onlyDatePart);
        }

        public static DateTime GetRandomDate(DateTime min, DateTime max)
        {
            return GetRandomDate(min, max, true);
        }

        public static DateTime GetRandomDate(DateTime min, DateTime max, bool onlyDatePart)
        {
            if (min > max)
            {
                throw new ArgumentException("Minimum date must be less than or equal to maximum date.");
            }

            var output = min.AddDays((double)GetRandomDecimal(0, (decimal)(max - min).TotalDays));

            output = output.AddMilliseconds(-output.Millisecond);

            if (onlyDatePart)
            {
                output = output.Date;
            }

            return output;
        }

        public static T GetRandomValue<T>(params T[] exclusions)
        {
            var values = GetValues<T>();

            if (exclusions != null && exclusions.Any())
            {
                values = values.Except(exclusions);
            }
            else
            {
                var noneValue = values.First();

                values = values.Except([noneValue]);
            }

            return values.Random();
        }

        public static bool ContainsValue<T, X>(X value, params T[] exclusions)
            where T : struct
        {
#pragma warning disable IDE0007
            if (Enum.TryParse($"{value}", true, out T enumValue))
#pragma warning restore IDE0007
            {
                return GetValues<T>().Except(exclusions).Contains(enumValue);
            }

            return false;
        }

        /// <summary>
        /// Generates a random whole number within the specified range.
        /// </summary>
        /// <param name="range">The range defining the minimum and maximum values that can be returned.</param>
        /// <returns>A whole number.</returns>
        public static int GetRanged(Range<int> range)
        {
            if (range == null)
            {
                throw new ArgumentNullException(nameof(range));
            }

            var random = R.NextDouble();

            long difference = range.Maximum - range.Minimum;

            return (int)(range.Minimum + (difference * random));
        }

        public static byte GetRanged(Range<byte> range)
        {
            if (range == null)
            {
                throw new ArgumentNullException(nameof(range));
            }

            var random = R.NextDouble();

            var difference = range.Maximum - range.Minimum;

            return (byte)(range.Minimum + (difference * random));
        }

        public static decimal GetRanged(Range<decimal> range)
        {
            return GetRanged(range, -1);
        }

        public static decimal GetRanged(Range<decimal> range, int precision)
        {
            if (range == null)
            {
                throw new ArgumentNullException(nameof(range));
            }

            var random = R.NextDouble();

            var min = Convert.ToDouble(range.Minimum);

            var max = Convert.ToDouble(range.Maximum);

            var difference = max - min;

            if (precision < 0)
            {
                precision = DefaultDecimalPrecision;
            }

            var result = Convert.ToDecimal(min + (difference * random));

            return decimal.Round(result, precision);
        }

        /// <summary>
        /// Clamps the given value to the nearest increment by rounding towards zero.
        /// </summary>
        /// <param name="value">The value to clamp.</param>
        /// <param name="increment">The increment to round to.</param>
        /// <returns>The rounded value.</returns>
        public static decimal ClampTo(decimal value, decimal increment)
        {
            var output = 0.0m;

            if (value > 0.0m)
            {
                while (output + increment < value)
                {
                    output += increment;
                }
            }
            else
            {
                while (output - increment > value)
                {
                    output -= increment;
                }
            }

            return output;
        }

        public static decimal Round(this decimal value) => Math.Round(value, DefaultDecimalPrecision);

        public static decimal Round(this decimal value, int precision) => Math.Round(value, precision);

        /// <summary>
        /// Creates an enumerable collection of enum values.
        /// </summary>
        /// <typeparam name="T">The type of the enum.</typeparam>
        /// <returns>A collection containing the publicly visible members of the specified enum.</returns>
        public static IEnumerable<T> GetValues<T>(params T[] exclusions)
        {
            return Enum.GetValues(typeof(T))
                       .Cast<T>()
                       .Except(exclusions);
        }

        public static bool CompareStrings(string value1, string value2)
        {
            return CompareStrings(value1, value2, false);
        }

        public static bool CompareStrings(string value1, string value2, bool ignoreCase)
        {
            var comparison = StringComparison.CurrentCulture;

            if (ignoreCase)
            {
                comparison = StringComparison.CurrentCultureIgnoreCase;
            }

            return string.Compare(value1, value2, comparison) == 0;
        }

        public static DateTime GetDate(DateTime startDate, int turnCount)
        {
            return GetDate(startDate, 1, turnCount);
        }

        public static DateTime GetDate(DateTime startDate, byte weeksPerTurn, int turnCount)
        {
            checked
            {
                return startDate.AddDays(7 * weeksPerTurn * turnCount);
            }
        }

        public static decimal Average(this Range<decimal> range)
        {
            checked
            {
                return (range.Maximum + range.Minimum) / 2;
            }
        }

        public static decimal CalculateProgress(IEnumerable<ProjectRequirement> requirements)
        {
            var currentValue = requirements.Sum(r => Math.Min(r.TargetValue, r.CurrentValue));

            var targetValue = requirements.Sum(r => r.TargetValue);

            return currentValue / targetValue;
        }

        public static decimal CalculateFutureProgress(Project project, Company company, int turns)
        {
            throw new NotImplementedException();
            //var clonedRequirements = project.Requirements.Select(r => r.Clone());

            //foreach (var allocation in company.Allocations.Where(a => a.Project == project))
            //{
            //    var matchedRequirements = allocation.Employee.Person.Skills.Join(clonedRequirements, s => s.SkillDefinition.Id, r => r.SkillDefinition.Id, (s, r) => r);

            //    foreach (var requirement in matchedRequirements)
            //    {
            //        if (requirement.TargetValue == requirement.CurrentValue)
            //        {
            //            continue;
            //        }

            //        var skillValue = allocation.Employee.Person.GetSkillValue(requirement.SkillDefinition.Id) * turns;

            //        var maxValue = requirement.TargetValue;

            //        requirement.CurrentValue += Math.Min(skillValue, maxValue);
            //    }
            //}

            //return CalculateProgress(clonedRequirements);
        }

        public static TValue SafeGet<TKey, TValue>(this IReadOnlyDictionary<TKey, TValue> dictionary, TKey key)
        {
            if (dictionary.ContainsKey(key))
            {
                return dictionary[key];
            }

            return default;
        }

        public static Range<int> ToRange(this int value, int accuracy)
        {
            var x = 100 - accuracy;
            var shift = R.Next(x);

            if (value - shift < 0)
            {
                shift = value;
            }

            var min = value - shift;
            var max = value + x - shift;
            return new Range<int>(min, max);
        }

        private static Random R => _random ??= new Random();

        private static Random _random;

        private const decimal DEFAULT_MINIMUM_DECIMAL_VALUE = -500000000.0m;

        private const decimal DEFAULT_MAXIMUM_DECIMAL_VALUE = 500000000.0m;
    }
}