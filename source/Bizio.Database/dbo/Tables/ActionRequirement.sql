﻿CREATE TABLE [dbo].[ActionRequirement]
(
	[ActionId] TINYINT NOT NULL,
	[SkillDefinitionId] TINYINT NOT NULL,
	[Value] DECIMAL(9, 2) NOT NULL,
	CONSTRAINT [PK_ActionRequirement]
		PRIMARY KEY CLUSTERED ([ActionId],[SkillDefinitionId]),
	CONSTRAINT [FK_ActionRequirement_Action]
		FOREIGN KEY ([ActionId])
		REFERENCES [dbo].[Action]([Id]),
	CONSTRAINT [FK_ActionRequirement_SkillDefinition]
		FOREIGN KEY ([SkillDefinitionId])
		REFERENCES [dbo].[SkillDefinition]([Id])
)