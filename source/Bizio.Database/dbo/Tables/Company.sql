﻿CREATE TABLE [dbo].[Company]
(
	[Id] INT NOT NULL IDENTITY,
	[GameId] INT NOT NULL,
	[Name] NVARCHAR(64) NOT NULL,
	[Money] DECIMAL(19, 2) NOT NULL,
	[InitialAccuracy] TINYINT NOT NULL,
	[UserId] INT NULL,
	[RivalId] INT NULL,
	[StatusId] TINYINT NOT NULL
		CONSTRAINT [DF_Company_StatusId]
		DEFAULT 1,
	CONSTRAINT [PK_Company]
		PRIMARY KEY CLUSTERED ([Id]),
	CONSTRAINT [FK_Company_GameId]
		FOREIGN KEY ([GameId])
		REFERENCES [dbo].[Game]([Id]),
	CONSTRAINT [FK_Company_RivalId]
		FOREIGN KEY ([RivalId])
		REFERENCES [dbo].[Company]([Id]),
	CONSTRAINT [FK_Company_StatusId]
		FOREIGN KEY ([StatusId])
		REFERENCES [dbo].[CompanyStatus]([Id])
)