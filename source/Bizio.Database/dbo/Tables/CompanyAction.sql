﻿CREATE TABLE [dbo].[CompanyAction]
(
	[CompanyId] INT NOT NULL,
	[ActionId] TINYINT NOT NULL,
	[Count] SMALLINT NOT NULL,
	[IsActive] BIT NOT NULL,
	CONSTRAINT [PK_CompanyAction]
		PRIMARY KEY CLUSTERED ([CompanyId], [ActionId]),
	CONSTRAINT [FK_CompanyAction_CompanyId]
		FOREIGN KEY ([CompanyId])
		REFERENCES [dbo].[Company]([Id]),
	CONSTRAINT [FK_CompanyAction_ActionId]
		FOREIGN KEY ([ActionId])
		REFERENCES [dbo].[Action]([Id])
)