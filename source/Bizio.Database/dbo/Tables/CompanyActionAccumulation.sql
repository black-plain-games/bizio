﻿CREATE TABLE [dbo].[CompanyActionAccumulation]
(
	[CompanyId] INT NOT NULL,
	[ActionId] TINYINT NOT NULL,
	[SkillDefinitionId] TINYINT NOT NULL,
	[Value] DECIMAL(9, 2) NOT NULL,
	CONSTRAINT [PK_Company_Action_Accumulation]
		PRIMARY KEY CLUSTERED ([CompanyId],[ActionId],[SkillDefinitionId]),
	CONSTRAINT [FK_CompanyActionAccumulation_CompanyAction]
		FOREIGN KEY ([CompanyId],[ActionId])
		REFERENCES [dbo].[CompanyAction]([CompanyId],[ActionId]),
	CONSTRAINT [FK_CompanyActionAccumulation_SkillDefinition]
		FOREIGN KEY ([SkillDefinitionId])
		REFERENCES [dbo].[SkillDefinition]([Id])
)