﻿CREATE TABLE [dbo].[CompanyEmployee]
(
	[CompanyId] INT NOT NULL,
	[PersonId] INT NOT NULL,
	[Salary] DECIMAL(9, 2) NOT NULL,
	[IsFounder] BIT NOT NULL,
	CONSTRAINT [PK_CompanyEmployee]
		PRIMARY KEY CLUSTERED ([CompanyId],[PersonId]),
	CONSTRAINT [FK_CompanyEmployee_Company]
		FOREIGN KEY ([CompanyId])
		REFERENCES [dbo].[Company]([Id]),
	CONSTRAINT [FK_CompanyEmployee_Person]
		FOREIGN KEY ([PersonId])
		REFERENCES [dbo].[Person]([Id])
)