﻿CREATE TABLE [dbo].[CompanyMessage]
(
	[Id] INT NOT NULL IDENTITY,
	[CompanyId] INT NOT NULL,
	[DateCreated] DATE NOT NULL,
	[StatusId] TINYINT NOT NULL,
	[Source] NVARCHAR(64) NOT NULL,
	[Subject] NVARCHAR(128) NOT NULL,
	[Message] NVARCHAR(1024) NOT NULL,
	CONSTRAINT [PK_CompanyMessage]
		PRIMARY KEY CLUSTERED ([Id]),
	CONSTRAINT [FK_CompanyMessage_Company]
		FOREIGN KEY ([CompanyId])
		REFERENCES [dbo].[Company]([Id]),
	CONSTRAINT [FK_CompanyMessage_Status]
		FOREIGN KEY ([StatusId])
		REFERENCES [dbo].[CompanyMessageStatus]([Id])
)
