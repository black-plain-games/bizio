﻿CREATE TABLE [dbo].[CompanyMessageStatus]
(
	[Id] TINYINT NOT NULL IDENTITY,
	[Description] NVARCHAR(32) NOT NULL,
	CONSTRAINT [PK_CompanyMessageStatus]
		PRIMARY KEY CLUSTERED ([Id]),
)
