﻿CREATE TABLE [dbo].[CompanyName]
(
	[IndustryId] TINYINT NOT NULL,
	[Name] NVARCHAR(64) NOT NULL,
	CONSTRAINT [PK_CompanyName]
		PRIMARY KEY CLUSTERED ([IndustryId], [Name]),
	CONSTRAINT [FK_CompanyName_Industry]
		FOREIGN KEY ([IndustryId])
		REFERENCES [dbo].[Industry]([Id]),
)