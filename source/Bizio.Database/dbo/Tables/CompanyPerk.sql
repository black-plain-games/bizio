﻿CREATE TABLE [dbo].[CompanyPerk]
(
    [Id] INT NOT NULL IDENTITY,
    [CompanyId] INT NOT NULL,
    [PerkId] SMALLINT NOT NULL,
    [NextPaymentDate] DATE NULL,
    CONSTRAINT [PK_CompanyPerk]
        PRIMARY KEY CLUSTERED ([Id]),
    CONSTRAINT [FK_CompanyPerk_Company]
        FOREIGN KEY ([CompanyId])
        REFERENCES [dbo].[Company]([Id]),
    CONSTRAINT [FK_CompanyPerk_Perk]
        FOREIGN KEY ([PerkId])
        REFERENCES [dbo].[Perk]([Id])
)