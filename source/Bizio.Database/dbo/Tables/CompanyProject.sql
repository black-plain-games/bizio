﻿CREATE TABLE [dbo].[CompanyProject]
(
	[CompanyId] INT NOT NULL,
	[ProjectId] INT NOT NULL,
	CONSTRAINT [PK_CompanyProject]
		PRIMARY KEY CLUSTERED ([CompanyId],[ProjectId]),
	CONSTRAINT [FK_CompanyProject_Company]
		FOREIGN KEY ([CompanyId])
		REFERENCES [dbo].[Company]([Id]),
	CONSTRAINT [FK_CompanyProject_Project]
		FOREIGN KEY ([ProjectId])
		REFERENCES [dbo].[Project]([Id])
)