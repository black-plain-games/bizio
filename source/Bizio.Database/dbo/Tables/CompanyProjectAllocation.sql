﻿CREATE TABLE [dbo].[CompanyProjectAllocation]
(
	[CompanyId] INT NOT NULL,
	[PersonId] INT NOT NULL,
	[ProjectId] INT NOT NULL,
	[RoleId] TINYINT NOT NULL,
	[Percent] TINYINT NOT NULL,
	CONSTRAINT [PK_CompanyProjectAllocation]
		PRIMARY KEY CLUSTERED ([CompanyId],[PersonId],[ProjectId]),
	CONSTRAINT [FK_CompanyProjectAllocation_Employee]
		FOREIGN KEY ([CompanyId],[PersonId])
		REFERENCES [dbo].[CompanyEmployee]([CompanyId],[PersonId]),
	CONSTRAINT [FK_CompanyProjectAllocation_Project]
		FOREIGN KEY ([ProjectId])
		REFERENCES [dbo].[Project]([Id]),
	CONSTRAINT [FK_CompanyProjectAllocation_Role]
		FOREIGN KEY ([RoleId])
		REFERENCES [dbo].[Role]([Id])
)