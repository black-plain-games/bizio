﻿CREATE TABLE [dbo].[CompanyProspect]
(
	[CompanyId] INT NOT NULL,
	[PersonId] INT NOT NULL,
	[Accuracy] TINYINT NOT NULL,
	[SalaryMinimum] DECIMAL(9, 2) NOT NULL,
	[SalaryMaximum] DECIMAL(9, 2) NOT NULL,
	CONSTRAINT [PK_CompanyProspect]
		PRIMARY KEY CLUSTERED ([CompanyId],[PersonId]),
	CONSTRAINT [FK_CompanyProspect_Company]
		FOREIGN KEY ([CompanyId])
		REFERENCES [dbo].[Company]([Id]),
	CONSTRAINT [FK_CompanyProspect_Person]
		FOREIGN KEY ([PersonId])
		REFERENCES [dbo].[Person]([Id]),
)