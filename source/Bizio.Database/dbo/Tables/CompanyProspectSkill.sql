﻿CREATE TABLE [dbo].[CompanyProspectSkill]
(
	[CompanyId] INT NOT NULL,
	[PersonId] INT NOT NULL,
	[SkillDefinitionId] TINYINT NOT NULL,
	[MinimumValue] DECIMAL(9, 2) NOT NULL,
	[MaximumValue] DECIMAL(9, 2) NOT NULL
	CONSTRAINT [PK_CompanyProspectSkill]
		PRIMARY KEY CLUSTERED ([CompanyId],[PersonId],[SkillDefinitionId]),
	CONSTRAINT [FK_CompanyProspectSkill_CompanyProspect]
		FOREIGN KEY ([CompanyId],[PersonId])
		REFERENCES [dbo].[CompanyProspect]([CompanyId],[PersonId]),
	CONSTRAINT [FK_CompanyProspectSkill_SkillDefinition]
		FOREIGN KEY ([SkillDefinitionId])
		REFERENCES [dbo].[SkillDefinition]([Id])
)