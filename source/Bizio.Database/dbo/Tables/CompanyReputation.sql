﻿CREATE TABLE [dbo].[CompanyReputation]
(
	[CompanyId] INT NOT NULL,
	[EarnedStars] DECIMAL(19,2) NOT NULL,
	[PossibleStars] INT NOT NULL,
	CONSTRAINT [PK_CompanyReputation]
		PRIMARY KEY CLUSTERED ([CompanyId]),
	CONSTRAINT [FK_CompanyReputation_Company]
		FOREIGN KEY ([CompanyId])
		REFERENCES [dbo].[Company]([Id])
)