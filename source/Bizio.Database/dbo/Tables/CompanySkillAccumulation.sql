﻿CREATE TABLE [dbo].[CompanySkillAccumulation]
(
	[CompanyId] INT NOT NULL,
	[SkillDefinitionId] TINYINT NOT NULL,
	[Value] DECIMAL(19, 2) NOT NULL,
	CONSTRAINT [PK_CompanySkillAccumulation]
		PRIMARY KEY CLUSTERED ([CompanyId],[SkillDefinitionId]),
	CONSTRAINT [FK_CompanySkillAccumulation_Company]
		FOREIGN KEY ([CompanyId])
		REFERENCES [dbo].[Company]([Id]),
	CONSTRAINT [FK_CompanySkillAccumulation_SkillDefinition]
		FOREIGN KEY ([SkillDefinitionId])
		REFERENCES [dbo].[SkillDefinition]([Id]),
)