﻿CREATE TABLE [dbo].[CompanyTransaction]
(
	[Id] INT NOT NULL IDENTITY,
	[CompanyId] INT NOT NULL,
	[TypeId] TINYINT NOT NULL,
	[Date] DATE NOT NULL,
	[Description] NVARCHAR(1024),
	[Amount] DECIMAL(19, 2) NOT NULL,
	[EndingBalance] DECIMAL(19, 2) NOT NULL,
	CONSTRAINT [PK_CompanyTransaction]
		PRIMARY KEY CLUSTERED ([Id]),
	CONSTRAINT [FK_CompanyTransaction_Company]
		FOREIGN KEY ([CompanyId])
		REFERENCES [dbo].[Company]([Id]),
	CONSTRAINT [FK_CompanyTransaction_CompanyTransactionType]
		FOREIGN KEY ([TypeId])
		REFERENCES [dbo].[CompanyTransactionType]([Id]),
)