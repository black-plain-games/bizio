﻿CREATE TABLE [dbo].[CompanyTransactionType]
(
	[Id] TINYINT NOT NULL IDENTITY,
	[Description] NVARCHAR(64) NOT NULL,
	CONSTRAINT [PK_CompanyTransactionType]
		PRIMARY KEY CLUSTERED ([Id])
)