﻿CREATE TABLE [dbo].[FirstName]
(
	[GenderId] TINYINT NOT NULL,
	[Name] NVARCHAR(64) NOT NULL,
	CONSTRAINT [PK_FirstName]
		PRIMARY KEY CLUSTERED ([GenderId], [Name]),
	CONSTRAINT [FK_FirstName_Gender]
		FOREIGN KEY ([GenderId])
		REFERENCES [dbo].[Gender]([Id])
)