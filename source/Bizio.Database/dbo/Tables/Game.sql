﻿CREATE TABLE [dbo].[Game]
(
	[Id] INT NOT NULL IDENTITY,
	[CreatedBy] INT NOT NULL,
	[CreatedDate] DATETIMEOFFSET NOT NULL,
	[IndustryId] TINYINT NOT NULL,
    [StatusId] TINYINT NOT NULL
        CONSTRAINT [DF_Game_Status]
        DEFAULT 1,
	CONSTRAINT [PK_Game]
		PRIMARY KEY CLUSTERED ([Id]),
	CONSTRAINT [FK_Game_Industry]
		FOREIGN KEY ([IndustryId])
		REFERENCES [Industry]([Id]),
	CONSTRAINT [FK_Game_Id]
		FOREIGN KEY ([StatusId])
		REFERENCES [GameStatus]([Id])
)