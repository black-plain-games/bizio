﻿CREATE TABLE [dbo].[GameClock]
(
	[GameId] INT NOT NULL,
	[CurrentTurn] SMALLINT NOT NULL,
	[StartDate] DATE NOT NULL,
	[WeeksPerTurn] TINYINT NOT NULL,
	CONSTRAINT [PK_GameClock]
		PRIMARY KEY CLUSTERED ([GameId]),
	CONSTRAINT [FK_GameClock_Game]
		FOREIGN KEY ([GameId])
		REFERENCES [dbo].[Game]([Id]),
	CONSTRAINT [CHK_GameClock_WeeksPerTurn]
		CHECK ([WeeksPerTurn] > 0)
)