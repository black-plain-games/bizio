﻿CREATE TABLE [dbo].[GameConfiguration]
(
	[GameId] INT NOT NULL,
	[ConfigurationKeyId] TINYINT NOT NULL,
	[Value] NVARCHAR(MAX) NOT NULL,
	CONSTRAINT [PK_GameConfiguration]
		PRIMARY KEY CLUSTERED ([GameId], [ConfigurationKeyId]),
	CONSTRAINT [FK_GameConfiguration_Game]
		FOREIGN KEY ([GameId])
		REFERENCES [dbo].[Game]([Id]),
	CONSTRAINT [FK_GameConfiguration_ConfigurationKey]
		FOREIGN KEY ([ConfigurationKeyId])
		REFERENCES [dbo].[ConfigurationKey]([Id])
)