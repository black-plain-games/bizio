﻿CREATE TABLE [dbo].[GlobalConfiguration]
(
	[ConfigurationKeyId] TINYINT NOT NULL,
	[Value] NVARCHAR(MAX) NOT NULL,
	CONSTRAINT [PK_GlobalConfiguration]
		PRIMARY KEY CLUSTERED ([ConfigurationKeyId])
)