﻿CREATE TABLE [dbo].[Message]
(
	[Id] INT NOT NULL IDENTITY,
	[GameId] INT NOT NULL,
	[MessageLevelId] TINYINT NOT NULL,
	[Text] NVARCHAR(MAX),
	[TimeStamp] DATETIMEOFFSET NOT NULL,
	[CurrentTurn] INT NOT NULL,
	[StackTrace] NVARCHAR(MAX),
	CONSTRAINT [PK_Message]
		PRIMARY KEY CLUSTERED ([Id]),
	CONSTRAINT [FK_Message_Game]
		FOREIGN KEY ([GameId])
		REFERENCES [dbo].[Game]([Id]),
	CONSTRAINT [FK_Message_MessageLevel]
		FOREIGN KEY ([MessageLevelId])
		REFERENCES [dbo].[MessageLevel]([Id])

)
