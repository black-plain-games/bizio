﻿CREATE TABLE [dbo].[Notification]
(
    [Id] INT NOT NULL IDENTITY,
    [DateVisible] DATETIMEOFFSET NOT NULL,
    [Subject] NVARCHAR(1024) NOT NULL,
    [Message] NVARCHAR(MAX) NOT NULL,
    CONSTRAINT [PK_Notification]
        PRIMARY KEY CLUSTERED ([Id])
)