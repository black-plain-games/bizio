﻿CREATE TABLE [dbo].[Perk]
(
	[Id] SMALLINT NOT NULL IDENTITY,
	[Name] NVARCHAR(64) NOT NULL,
	[Description] NVARCHAR(1024) NOT NULL,
	[MaxCount] TINYINT NULL,
	[InitialCost] DECIMAL(9, 2) NULL,
	[RecurringCost] DECIMAL(9, 2) NULL,
	[RecurringCostInterval] TINYINT NULL,
	CONSTRAINT [PK_Perk]
		PRIMARY KEY CLUSTERED ([Id]),
	CONSTRAINT [CHK_Perk_InitialCost]
		CHECK ([InitialCost] IS NULL OR [InitialCost] > 0),
	CONSTRAINT [CHK_Perk_RecurringCost]
		CHECK ([RecurringCost] IS NULL OR [RecurringCost] > 0),
	CONSTRAINT [CHK_Perk_RecurringCostInterval]
		CHECK ([RecurringCostInterval] IS NULL OR [RecurringCostInterval] > 0),
	CONSTRAINT [CHK_Perk_Cost]
		CHECK ([InitialCost] IS NOT NULL OR [RecurringCost] IS NOT NULL)
)