﻿CREATE TABLE [dbo].[PerkBenefit]
(
	[PerkId] SMALLINT NOT NULL,
	[TargetTypeId] TINYINT NOT NULL,
	[AttributeId] TINYINT NOT NULL,
	[Value] DECIMAL(9, 2) NOT NULL,
	[ValueTypeId] TINYINT NOT NULL,
	CONSTRAINT [PK_PerkBenefit]
		PRIMARY KEY CLUSTERED ([PerkId],[TargetTypeId],[AttributeId]),
	CONSTRAINT [FK_PerkBenefit_Perk]
		FOREIGN KEY ([PerkId])
		REFERENCES [dbo].[Perk]([Id]),
	CONSTRAINT [FK_PerkBenefit_PerkTargetType]
		FOREIGN KEY ([TargetTypeId])
		REFERENCES [dbo].[PerkTargetType]([Id]),
	CONSTRAINT [FK_PerkBenefit_PerkBenefitAttribute]
		FOREIGN KEY ([AttributeId])
		REFERENCES [dbo].[PerkBenefitAttribute]([Id]),
	CONSTRAINT [FK_Perk_PerkValueType]
		FOREIGN KEY ([ValueTypeId])
		REFERENCES [dbo].[PerkValueType]([Id])
)