﻿CREATE TABLE [dbo].[PerkBenefitAttribute]
(
	[Id] TINYINT NOT NULL IDENTITY,
	[Name] NVARCHAR(64),
	CONSTRAINT [PK_PerkBenefitAttribute]
		PRIMARY KEY CLUSTERED ([Id])
)