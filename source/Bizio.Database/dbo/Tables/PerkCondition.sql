﻿CREATE TABLE [dbo].[PerkCondition]
(
	[PerkId] SMALLINT NOT NULL,
	[TargetTypeId] TINYINT NOT NULL,
	[AttributeId] TINYINT NOT NULL,
	[Value] DECIMAL(9, 2) NOT NULL,
	[ComparisonId] TINYINT NOT NULL,
	CONSTRAINT [PK_PerkCondition]
		PRIMARY KEY CLUSTERED ([PerkId],[TargetTypeId],[AttributeId]),
	CONSTRAINT [FK_PerkCondition_Perk]
		FOREIGN KEY ([PerkId])
		REFERENCES [dbo].[Perk]([Id]),
	CONSTRAINT [FK_PerkCondition_PerkTargetType]
		FOREIGN KEY ([TargetTypeId])
		REFERENCES [dbo].[PerkTargetType]([Id]),
	CONSTRAINT [FK_PerkCondition_PerkConditionAttribute]
		FOREIGN KEY ([AttributeId])
		REFERENCES [dbo].[PerkConditionAttribute]([Id]),
	CONSTRAINT [FK_PerkCondition_PerkConditionComparison]
		FOREIGN KEY ([ComparisonId])
		REFERENCES [dbo].[PerkConditionComparison]([Id])
)