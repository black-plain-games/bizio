﻿CREATE TABLE [dbo].[PerkConditionAttribute]
(
	[Id] TINYINT NOT NULL IDENTITY,
	[Name] NVARCHAR(64),
	CONSTRAINT [PK_PerkConditionAttribute]
		PRIMARY KEY CLUSTERED ([Id])
)