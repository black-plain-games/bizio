﻿CREATE TABLE [dbo].[PerkConditionComparison]
(
	[Id] TINYINT NOT NULL IDENTITY,
	[Name] NVARCHAR(32) NOT NULL,
	CONSTRAINT [PK_PerkConditionComparison]
		PRIMARY KEY CLUSTERED ([Id])
)