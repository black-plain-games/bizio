﻿CREATE TABLE [dbo].[Person]
(
    [Id] INT NOT NULL IDENTITY,
    [GameId] INT NOT NULL,
    [GenderId] TINYINT NOT NULL,
    [PersonalityId] TINYINT NOT NULL,
    [FirstName] NVARCHAR(64) NOT NULL,
    [LastName] NVARCHAR(64) NOT NULL,
    [Birthday] DATE NOT NULL,
    [RetirementDate] DATE NOT NULL,
    [ProfessionId] TINYINT NULL,
    CONSTRAINT [PK_Person]
        PRIMARY KEY CLUSTERED ([Id]),
    CONSTRAINT [FK_Person_Game]
        FOREIGN KEY ([GameId])
        REFERENCES [dbo].[Game]([Id]),
    CONSTRAINT [FK_Person_Gender]
        FOREIGN KEY ([GenderId])
        REFERENCES [dbo].[Gender]([Id]),
    CONSTRAINT [FK_Person_Personality]
        FOREIGN KEY ([PersonalityId])
        REFERENCES [dbo].[Personality]([Id]),
    CONSTRAINT [FK_Person_Profession]
        FOREIGN KEY ([ProfessionId])
        REFERENCES [dbo].[Profession] ([Id]),
    CONSTRAINT [CHK_Person_Birthday_RetirementDate]
        CHECK ([RetirementDate] > DATEADD(YEAR, 20, [Birthday]))
)