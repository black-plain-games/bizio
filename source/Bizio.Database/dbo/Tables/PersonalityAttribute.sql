﻿CREATE TABLE [dbo].[PersonalityAttribute]
(
	[PersonalityId] TINYINT NOT NULL,
	[AttributeId] TINYINT NOT NULL,
	[Value] DECIMAL(9, 2) NOT NULL,
	CONSTRAINT [PK_PersonalityAttribute]
		PRIMARY KEY CLUSTERED ([PersonalityId],[AttributeId]),
	CONSTRAINT [FK_PersonalityAttribute_Personality]
		FOREIGN KEY ([PersonalityId])
		REFERENCES [dbo].[Personality]([Id]),
	CONSTRAINT [FK_PersonalityAttribute_Attribute]
		FOREIGN KEY ([AttributeId])
		REFERENCES [dbo].[Attribute]([Id])
)