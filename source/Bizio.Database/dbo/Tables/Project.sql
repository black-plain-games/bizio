﻿CREATE TABLE [dbo].[Project]
(
	[Id] INT NOT NULL IDENTITY,
	[GameId] INT NOT NULL,
	[ProjectDefinitionId] SMALLINT NOT NULL,
	[Value] INT NOT NULL,
	[Deadline] DATE NOT NULL,
	[ExtensionDeadline] DATE NULL,
	[ReputationRequired] TINYINT NOT NULL,
	[StatusId] TINYINT NOT NULL,
	[ResultId] TINYINT NULL,
	CONSTRAINT [PK_Project]
		PRIMARY KEY CLUSTERED ([Id]),
	CONSTRAINT [FK_Project_Game]
		FOREIGN KEY ([GameId])
		REFERENCES [dbo].[Game]([Id]),
	CONSTRAINT [FK_Project_ProjectDefinition]
		FOREIGN KEY ([ProjectDefinitionId])
		REFERENCES [dbo].[ProjectDefinition]([Id]),
	CONSTRAINT [FK_Project_ProjectStatus]
		FOREIGN KEY ([StatusId])
		REFERENCES [dbo].[ProjectStatus]([Id]),
	CONSTRAINT [FK_Project_ProjectResult]
		FOREIGN KEY ([ResultId])
		REFERENCES [dbo].[ProjectResult]([Id])
)