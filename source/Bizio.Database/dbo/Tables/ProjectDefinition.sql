﻿CREATE TABLE [dbo].[ProjectDefinition]
(
	[Id] SMALLINT NOT NULL IDENTITY,
	[Name] NVARCHAR(64) NOT NULL,
	[Description] NVARCHAR(1024) NOT NULL,
	[MinimumValue] INT NOT NULL,
	[MaximumValue] INT NOT NULL,
	[MinimumProjectLength] TINYINT NOT NULL,
	[MaximumProjectLength] TINYINT NOT NULL,
	CONSTRAINT [PK_ProjectDefinition]
		PRIMARY KEY CLUSTERED ([Id])
)