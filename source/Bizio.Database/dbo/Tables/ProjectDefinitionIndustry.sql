﻿CREATE TABLE [dbo].[ProjectDefinitionIndustry]
(
	[ProjectDefinitionId] SMALLINT NOT NULL,
	[IndustryId] TINYINT NOT NULL,
	CONSTRAINT [PK_ProjectDefinitionIndustry]
		PRIMARY KEY CLUSTERED ([ProjectDefinitionId],[IndustryId]),
	CONSTRAINT [FK_ProjectDefinitionIndustry_ProjectDefinition]
		FOREIGN KEY ([ProjectDefinitionId])
		REFERENCES [dbo].[ProjectDefinition]([Id]),
	CONSTRAINT [FK_ProjectDefinitionIndustry_Industry]
		FOREIGN KEY ([IndustryId])
		REFERENCES [dbo].[Industry]([Id])
)