﻿CREATE TABLE [dbo].[ProjectDefinitionSkill]
(
    [ProjectDefinitionId] SMALLINT NOT NULL,
    [SkillDefinitionId] TINYINT NOT NULL,
    [Minimum] SMALLINT NOT NULL,
    [Maximum] SMALLINT NOT NULL,
    [IsRequired] BIT NOT NULL,
    CONSTRAINT [PK_ProjectDefinitionSkill]
        PRIMARY KEY CLUSTERED ([ProjectDefinitionId],[SkillDefinitionId]),
    CONSTRAINT [FK_ProjectDefinitionSkill_ProjectDefinition]
        FOREIGN KEY ([ProjectDefinitionId])
        REFERENCES [dbo].[ProjectDefinition]([Id]),
    CONSTRAINT [FK_ProjectDefinitionSkill_SkillDefinition]
        FOREIGN KEY ([SkillDefinitionId])
        REFERENCES [dbo].[SkillDefinition]([Id])
)