﻿CREATE TABLE [dbo].[ProjectRequirement]
(
	[ProjectId] INT NOT NULL,
	[SkillDefinitionId] TINYINT NOT NULL,
	[TargetValue] INT NOT NULL,
	[CurrentValue] DECIMAL(9, 2) NOT NULL,
	CONSTRAINT [PK_ProjectRequirement]
		PRIMARY KEY CLUSTERED ([ProjectId],[SkillDefinitionId]),
	CONSTRAINT [FK_ProjectRequirement_Project]
		FOREIGN KEY ([ProjectId])
		REFERENCES [dbo].[Project]([Id]),
	CONSTRAINT [FK_ProjectRequirement_SkillDefinition]
		FOREIGN KEY ([SkillDefinitionId])
		REFERENCES [dbo].[SkillDefinition]([Id])
)