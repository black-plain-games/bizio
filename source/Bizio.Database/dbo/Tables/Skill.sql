﻿CREATE TABLE [dbo].[Skill]
(
	[PersonId] INT NOT NULL,
	[SkillDefinitionId] TINYINT NOT NULL,
	[Value] DECIMAL(9, 2) NOT NULL,
	[LearnRate] TINYINT NOT NULL,
	[ForgetRate] TINYINT NOT NULL,
	CONSTRAINT [PK_Skill]
		PRIMARY KEY CLUSTERED ([PersonId],[SkillDefinitionId]),
	CONSTRAINT [FK_Skill_Person]
		FOREIGN KEY ([PersonId])
		REFERENCES [dbo].[Person]([Id]),
	CONSTRAINT [FK_Skill_SkillDefinition]
		FOREIGN KEY ([SkillDefinitionId])
		REFERENCES [dbo].[SkillDefinition]([Id]),
)