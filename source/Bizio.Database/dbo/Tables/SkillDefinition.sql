﻿CREATE TABLE [dbo].[SkillDefinition]
(
    [Id] TINYINT NOT NULL IDENTITY,
    [Name] NVARCHAR(64) NOT NULL,
    [Minimum] TINYINT NOT NULL,
    [Maximum] TINYINT NOT NULL,
    CONSTRAINT [PK_SkillDefinition]
        PRIMARY KEY CLUSTERED ([Id])
)