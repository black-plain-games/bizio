﻿CREATE TABLE [dbo].[WorkHistory]
(
	[Id] INT NOT NULL IDENTITY,
	[PersonId] INT NOT NULL,
	[CompanyId] INT NOT NULL,
	[StartDate] DATE NOT NULL,
	[EndDate] DATE NULL,
	[StartingSalary] DECIMAL(9, 2) NOT NULL,
	[EndingSalary] DECIMAL(9, 2) NULL,
	CONSTRAINT [PK_WorkHistory]
		PRIMARY KEY CLUSTERED ([Id]),
	CONSTRAINT [FK_WorkHistory_Person]
		FOREIGN KEY ([PersonId])
		REFERENCES [dbo].[Person]([Id]),
	CONSTRAINT [FK_WorkHistory_Company]
		FOREIGN KEY ([CompanyId])
		REFERENCES [dbo].[Company]([Id]),
)