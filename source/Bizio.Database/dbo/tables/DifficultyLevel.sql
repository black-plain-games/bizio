﻿CREATE TABLE [dbo].[DifficultyLevel]
(
    [Id] TINYINT NOT NULL IDENTITY,
    [Name] NVARCHAR(64) NOT NULL,
    [MaximumOptionalSkillCount] TINYINT NOT NULL,
    [MaximumTotalSkillPoints] SMALLINT NOT NULL,
    [InitialFunds] DECIMAL(9, 2) NOT NULL,
    CONSTRAINT [PK_DifficultyLevel]
        PRIMARY KEY CLUSTERED ([Id]),
    CONSTRAINT [CHK_DifficultyLevel_MaximumOptionalSkillCount]
        CHECK ([MaximumOptionalSkillCount] >= 0),
    CONSTRAINT [CHK_DifficultyLevel_MaximumTotalSkillPoints]
        CHECK ([MaximumTotalSkillPoints] >= 0),
    CONSTRAINT [CHK_DifficultyLevel_InitialFunds]
        CHECK ([InitialFunds] >= 0)
)