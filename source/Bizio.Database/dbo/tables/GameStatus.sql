﻿CREATE TABLE [dbo].[GameStatus]
(
    [Id] TINYINT NOT NULL,
    [Name] NVARCHAR(64) NOT NULL,
    CONSTRAINT [PK_GameStatus_Id]
        PRIMARY KEY CLUSTERED ([Id])
)
