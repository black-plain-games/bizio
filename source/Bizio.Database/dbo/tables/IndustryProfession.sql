﻿CREATE TABLE [dbo].[IndustryProfession]
(
    [IndustryId] TINYINT NOT NULL,
    [ProfessionId] TINYINT NOT NULL,
    CONSTRAINT [PK_IndustryProfession_Id]
        PRIMARY KEY CLUSTERED ([IndustryId], [ProfessionId]),
    CONSTRAINT [FK_IndustryProfession_IndustryId]
        FOREIGN KEY ([IndustryId])
        REFERENCES [dbo].[Industry] ([Id]),
    CONSTRAINT [FK_IndustryProfession_ProfessionId]
        FOREIGN KEY ([ProfessionId])
        REFERENCES [dbo].[Profession] ([Id])
)