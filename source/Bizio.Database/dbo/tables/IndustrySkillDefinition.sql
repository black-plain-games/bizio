﻿CREATE TABLE [dbo].[IndustrySkillDefinition]
(
    [IndustryId] TINYINT NOT NULL,
    [SkillDefinitionId] TINYINT NOT NULL,
    CONSTRAINT [PK_IndustrySkillDefinition_Id]
        PRIMARY KEY CLUSTERED ([IndustryId], [SkillDefinitionId]),
    CONSTRAINT [FK_IndustrySkillDefinition_IndustryId]
        FOREIGN KEY ([IndustryId])
        REFERENCES [dbo].[Industry] ([Id]),
    CONSTRAINT [FK_IndustrySkillDefinition_SkillDefinitionId]
        FOREIGN KEY ([SkillDefinitionId])
        REFERENCES [dbo].[SkillDefinition] ([Id])
)