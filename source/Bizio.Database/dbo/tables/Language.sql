﻿CREATE TABLE [dbo].[Language]
(
    [Id] TINYINT NOT NULL,
    [Name] NVARCHAR(32) NOT NULL,
    [Iso2LetterCode] NVARCHAR(2) NOT NULL,
    [DateModified] DATETIMEOFFSET NOT NULL
    CONSTRAINT [DF_Language_DateModified]
        DEFAULT SYSDATETIMEOFFSET(),
    CONSTRAINT [PK_Language]
        PRIMARY KEY CLUSTERED ([Id])
)
