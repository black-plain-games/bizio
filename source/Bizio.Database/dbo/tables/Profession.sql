﻿CREATE TABLE [dbo].[Profession]
(
    [Id] TINYINT NOT NULL IDENTITY,
    [Name] NVARCHAR(64) NOT NULL,
    CONSTRAINT [PK_Department_Id]
        PRIMARY KEY CLUSTERED ([Id])
)