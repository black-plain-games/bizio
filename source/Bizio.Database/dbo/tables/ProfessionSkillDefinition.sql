﻿CREATE TABLE [dbo].[ProfessionSkillDefinition]
(
    [ProfessionId] TINYINT NOT NULL,
    [SkillDefinitionId] TINYINT NOT NULL,
    [Weight] TINYINT NOT NULL,
    CONSTRAINT [PK_ProfessionSkillDefinition_Id]
        PRIMARY KEY CLUSTERED ([ProfessionId], [SkillDefinitionId]),
    CONSTRAINT [FK_ProfessionSkillDefinition_ProfessionId]
        FOREIGN KEY ([ProfessionId])
        REFERENCES [dbo].[Profession] ([Id]),
    CONSTRAINT [FK_ProfessionSkillDefinition_SkillDefinitionId]
        FOREIGN KEY ([SkillDefinitionId])
        REFERENCES [dbo].[SkillDefinition] ([Id])
)