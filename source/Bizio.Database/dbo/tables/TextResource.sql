﻿CREATE TABLE [dbo].[TextResource]
(
    [LanguageId] TINYINT NOT NULL,
    [TextResourceKeyId] SMALLINT NOT NULL,
    [Text] NVARCHAR(MAX) NOT NULL,
    [DateModified] DATETIMEOFFSET NOT NULL
    CONSTRAINT [DF_TextResource_DateModified]
        DEFAULT SYSDATETIMEOFFSET(),
    CONSTRAINT [PK_TextResource]
        PRIMARY KEY CLUSTERED ([LanguageId], [TextResourceKeyId]),
    CONSTRAINT [FK_TextResource_LanguageId]
        FOREIGN KEY ([LanguageId])
        REFERENCES [dbo].[Language] ([Id]),
    CONSTRAINT [FK_TextResource_TextResourceKeyId]
        FOREIGN KEY ([TextResourceKeyId])
        REFERENCES [dbo].[TextResourceKey] ([Id])

)
