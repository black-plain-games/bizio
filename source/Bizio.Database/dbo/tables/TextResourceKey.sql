﻿CREATE TABLE [dbo].[TextResourceKey]
(
    [Id] SMALLINT NOT NULL,
    [Name] NVARCHAR(128) NOT NULL,
    [DateModified] DATETIMEOFFSET NOT NULL
    CONSTRAINT [DF_TextResourceKey_DateModified]
        DEFAULT SYSDATETIMEOFFSET(),
    CONSTRAINT [PK_TextResourceKey]
        PRIMARY KEY CLUSTERED ([Id])
)
