PRINT 'Action'

SET IDENTITY_INSERT [dbo].[Action] ON

MERGE INTO [dbo].[Action] AS TARGET
USING
(VALUES
    (1, 'Recruit Person', 10, 1),
    (2, 'Interview Prospect', 10, 1),
    (3, 'Make Offer', 10, 1),
    (4, 'Fire Employee', 5, 1),
    (5, 'Adjust Salary', 10, 1),
    (6, 'Adjust Allocation', 1000, 10),
    (7, 'Accept Project', 10, 1),
    (8, 'Request Extension', 10, 1),
    (9, 'Submit Project', 10, 1),
    (10, 'Purchase Perk', 10, 1),
    (11, 'Sell Perk', 10, 1),
    (12, 'Change Message Status', 1000, 1000),
    (13, 'Toggle Company Action', 1000, 1000)
) AS SOURCE ([Id],[Name],[MaxCount],[DefaultCount])
ON
	TARGET.[Id] = SOURCE.[Id]
WHEN MATCHED THEN
    UPDATE SET
        [Name] = SOURCE.[Name],
        [MaxCount] = SOURCE.[MaxCount],
        [DefaultCount] = SOURCE.[DefaultCount]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([Id],[Name],[MaxCount],[DefaultCount])
    VALUES ([Id],[Name],[MaxCount],[DefaultCount]);

SET IDENTITY_INSERT [dbo].[Action] OFF