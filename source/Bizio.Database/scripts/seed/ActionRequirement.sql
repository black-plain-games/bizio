PRINT 'ActionRequirement'

MERGE INTO [dbo].[ActionRequirement] AS TARGET
    USING ( VALUES
        (1, 1, 75.00000000),
        (1, 2, 100.00000000),
        (1, 4, 75.00000000),
        (2, 1, 50.00000000),
        (2, 2, 50.00000000),
        (2, 4, 75.00000000),
        (3, 1, 50.00000000),
        (3, 2, 75.00000000),
        (3, 5, 50.00000000),
        (4, 1, 100.00000000),
        (4, 2, 100.00000000),
        (4, 3, 100.00000000),
        (4, 5, 100.00000000),
        (5, 1, 50.00000000),
        (5, 2, 100.00000000),
        (5, 5, 50.00000000),
        (6, 1, 50.00000000),
        (6, 2, 100.00000000),
        (7, 1, 50.00000000),
        (7, 2, 100.00000000),
        (7, 3, 75.00000000),
        (7, 4, 100.00000000),
        (8, 1, 100.00000000),
        (8, 2, 100.00000000),
        (8, 3, 100.00000000),
        (8, 5, 100.00000000),
        (9, 1, 50.00000000),
        (9, 2, 100.00000000),
        (9, 3, 50.00000000),
        (10, 1, 50.00000000),
        (10, 2, 50.00000000),
        (11, 1, 50.00000000),
        (11, 2, 50.00000000)
    ) AS SOURCE ([ActionId],[SkillDefinitionId],[Value])
ON TARGET.[ActionId] = SOURCE.[ActionId] AND TARGET.[SkillDefinitionId] = SOURCE.[SkillDefinitionId]
WHEN MATCHED THEN
    UPDATE SET
        [Value] = SOURCE.[Value]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([ActionId],[SkillDefinitionId],[Value])
    VALUES ([ActionId],[SkillDefinitionId],[Value]);