PRINT 'Attribute'

SET IDENTITY_INSERT [dbo].[Attribute] ON

MERGE INTO [dbo].[Attribute] AS TARGET
    USING ( VALUES
        (1, 'MinimumRetirementAge'),
        (2, 'Money'),
        (3, 'LiteralPerkFactor'),
        (4, 'PercentagePerkFactor'),
        (5, 'MandatorySkillFactor'),
        (6, 'OptionalSkillFactor'),
        (7, 'DesiredCompanySize'),
        (8, 'CompanySizeImportance'),
        (9, 'DesiredCompanyGrowth'),
        (10, 'CompanyGrowthImportance'),
        (11, 'MinimumDesiredEmployeeWorkload'),
        (12, 'MaximumDesiredEmployeeWorkload'),
        (13, 'MinimumPerkCount'),
        (14, 'MinimumDesiredRelevance'),
        (15, 'MaximumDesiredRelevance'),
        (16, 'MaximumOfferReasons'),
        (17, 'DesiredProspectPoolSize'),
        (18, 'HireProspectUrgency'),
        (19, 'DesiredEmployeesPerProject'),
        (20, 'DesiredProspectAccuracy'),
        (21, 'HirePersonUrgency'),
        (22, 'DesiredProjectsPerEmployee'),
        (23, 'MinimumProjectCompletionPercent'),
        (24, 'WaitPeriodBeforeFirstSalaryAdjustment'),
        (25, 'MandatorySkillMaxSalary'),
        (26, 'OptionalSkillMaxSalary'),
        (27, 'RequiredIncreaseForAdjustment'),
        (28, 'RequiredDecreaseForAdjustment'),
        (29, 'MinimumRetentionTurns'),
        (30, 'MinimumMoneyPercentForOffer'),
        (31, 'FireEmployeeLookbackWeeks'),
        (32, 'FireEmployeeMinimumPositivePayrollPecent'),
        (33, 'FireEmployeeLookbackProjectCount'),
        (34, 'FireEmployeeMinimumSkillValue'),
        (35, 'Sell Perk: Danger Capitol Threshold'),
        (36, 'Sell Perk: Turns Above Threshold'),
        (37, 'Offer Greed')
    ) AS SOURCE ([Id],[Name])
ON TARGET.[Id] = SOURCE.[Id]
WHEN MATCHED THEN
    UPDATE SET
        [Name] = SOURCE.[Name]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([Id],[Name])
    VALUES ([Id],[Name]);

SET IDENTITY_INSERT [dbo].[Attribute] OFF