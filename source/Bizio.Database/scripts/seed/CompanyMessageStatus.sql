PRINT 'CompanyMessageStatus'

SET IDENTITY_INSERT [dbo].[CompanyMessageStatus] ON

MERGE INTO [dbo].[CompanyMessageStatus] AS TARGET
    USING ( VALUES
        (1, 'UnRead'),
        (2, 'Read'),
        (3, 'Deleted')
    ) AS SOURCE ([Id],[Description])
ON TARGET.[Id] = SOURCE.[Id]
WHEN MATCHED THEN
    UPDATE SET
        [Description] = SOURCE.[Description]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([Id],[Description])
    VALUES ([Id],[Description]);

SET IDENTITY_INSERT [dbo].[CompanyMessageStatus] OFF