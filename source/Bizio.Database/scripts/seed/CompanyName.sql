PRINT 'CompanyName'

MERGE INTO [dbo].[CompanyName] AS TARGET
    USING ( VALUES
        (1, '48 Interactive'),
        (1, 'A Studios'),
        (1, 'Ando'),
        (1, 'AO Games'),
        (1, 'ArrNA'),
        (1, 'Coynami'),
        (1, 'Datnee'),
        (1, 'Digital Envoy'),
        (1, 'Heat Storm'),
        (1, 'Imperfect Universe'),
        (1, 'LaidBack Entertainment'),
        (1, 'Maxi'),
        (1, 'MD-soft'),
        (1, 'M-soft'),
        (1, 'Nambai Banco'),
        (1, 'Ninecent'),
        (1, 'Noodle'),
        (1, 'Pony'),
        (1, 'Poopskrieg'),
        (1, 'Round E'),
        (1, 'TryThree Interactive'),
        (1, 'U-soft'),
        (1, 'Wagner Boys'),
        (1, 'Webrocks'),
        (1, 'WebSmooth'),
        (1, 'Wexlon')
    ) AS SOURCE ([IndustryId],[Name])
ON TARGET.[IndustryId] = SOURCE.[IndustryId] AND TARGET.[Name] = SOURCE.[Name]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([IndustryId],[Name])
    VALUES ([IndustryId],[Name]);