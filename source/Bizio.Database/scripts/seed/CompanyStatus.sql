﻿PRINT 'CompanyStatus'

MERGE INTO [dbo].[CompanyStatus] AS TARGET
    USING ( VALUES
        (1, 'In Business'),
        (2, 'Bankrupt')
    ) AS SOURCE ([Id],[Name])
ON TARGET.[Id] = SOURCE.[Id]
WHEN MATCHED THEN
    UPDATE SET
        [Name] = SOURCE.[Name]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([Id],[Name])
    VALUES ([Id],[Name]);