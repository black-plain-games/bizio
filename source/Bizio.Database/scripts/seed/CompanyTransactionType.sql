PRINT 'CompanyTransactionType'

SET IDENTITY_INSERT [dbo].[CompanyTransactionType] ON

MERGE INTO [dbo].[CompanyTransactionType] AS TARGET
    USING ( VALUES
        (1, 'Payroll'),
        (2, 'Hire Employee'),
        (3, 'Fire Employee'),
        (4, 'Purchase Perk'),
        (5, 'Sell Perk'),
        (6, 'Perk'),
        (7, 'Project'),
        (8, 'Maintenance')
    ) AS SOURCE ([Id],[Description])
ON TARGET.[Id] = SOURCE.[Id]
WHEN MATCHED THEN
    UPDATE SET
        [Description] = SOURCE.[Description]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([Id],[Description])
    VALUES ([Id],[Description]);

SET IDENTITY_INSERT [dbo].[CompanyTransactionType] OFF