PRINT 'ConfigurationKey'

SET IDENTITY_INSERT [dbo].[ConfigurationKey] ON

MERGE INTO [dbo].[ConfigurationKey] AS TARGET
    USING ( VALUES
        (1, 'Percent Female'),
        (2, 'Minimum Number of Optional Skills'),
        (3, 'Maximum Number of Optional Skills'),
        (4, 'Initial Person Pool Size'),
        (5, 'Person Generation Batch Size'),
        (6, 'Project Generation Batch Size'),
        (7, 'Maximum Extension Length'),
        (8, 'Perk Resell Factor'),
        (9, 'Large Company Size'),
        (10, 'Company Size Max Differential'),
        (11, 'Fast Company Growth Value'),
        (12, 'Company Growth Max Differential'),
        (13, 'Minimum Person Count'),
        (14, 'Minimum Project Count'),
        (15, 'Project cancelled percent'),
        (16, 'Project failed percent'),
        (17, 'Project extension value adjustment percent'),
        (18, 'Default Skill Weight'),
        (19, 'Mandatory skill salary weight'),
        (20, 'Optional skill salary weight'),
        (21, 'Base Salary'),
        (22, 'Salary calculation magnitude'),
        (23, 'EmployeeMaintenanceCost')
    ) AS SOURCE ([Id],[Name])
ON TARGET.[Id] = SOURCE.[Id]
WHEN MATCHED THEN
    UPDATE SET
        [Name] = SOURCE.[Name]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([Id],[Name])
    VALUES ([Id],[Name]);

SET IDENTITY_INSERT [dbo].[ConfigurationKey] OFF