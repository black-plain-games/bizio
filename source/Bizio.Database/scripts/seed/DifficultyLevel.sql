PRINT 'DifficultyLevel'

SET IDENTITY_INSERT [dbo].[DifficultyLevel] ON

MERGE INTO [dbo].[DifficultyLevel] AS TARGET
    USING ( VALUES
        (1, 'Dreamer', 4, 500, 50000.00000000),
        (2, 'Visionary', 3, 400, 40000.00000000),
        (3, 'Mogul', 2, 300, 30000.00000000)
    ) AS SOURCE ([Id],[Name],[MaximumOptionalSkillCount],[MaximumTotalSkillPoints],[InitialFunds])
ON TARGET.[Id] = SOURCE.[Id]
WHEN MATCHED THEN
    UPDATE SET
        [Name] = SOURCE.[Name],
        [MaximumOptionalSkillCount] = SOURCE.[MaximumOptionalSkillCount],
        [MaximumTotalSkillPoints] = SOURCE.[MaximumTotalSkillPoints],
        [InitialFunds] = SOURCE.[InitialFunds]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([Id],[Name],[MaximumOptionalSkillCount],[MaximumTotalSkillPoints],[InitialFunds])
    VALUES ([Id],[Name],[MaximumOptionalSkillCount],[MaximumTotalSkillPoints],[InitialFunds]);

SET IDENTITY_INSERT [dbo].[DifficultyLevel] OFF