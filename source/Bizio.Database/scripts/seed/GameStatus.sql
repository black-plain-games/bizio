﻿PRINT 'GameStatus'

MERGE INTO [dbo].[GameStatus] AS TARGET
    USING ( VALUES
        (1, 'Playing'),
        (2, 'User Lost')
    ) AS SOURCE ([Id],[Name])
ON TARGET.[Id] = SOURCE.[Id]
WHEN MATCHED THEN
    UPDATE SET
        [Name] = SOURCE.[Name]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([Id],[Name])
    VALUES ([Id],[Name]);