PRINT 'Gender'

SET IDENTITY_INSERT [dbo].[Gender] ON

MERGE INTO [dbo].[Gender] AS TARGET
    USING ( VALUES
        (1, 'Male'),
        (2, 'Female')
    ) AS SOURCE ([Id],[Description])
ON TARGET.[Id] = SOURCE.[Id]
WHEN MATCHED THEN
    UPDATE SET
        [Description] = SOURCE.[Description]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([Id],[Description])
    VALUES ([Id],[Description]);

SET IDENTITY_INSERT [dbo].[Gender] OFF