PRINT 'GlobalConfiguration'

MERGE INTO [dbo].[GlobalConfiguration] AS TARGET
    USING ( VALUES
        (1, '50'),
        (2, '1'),
        (3, '6'),
        (4, '25'),
        (5, '5'),
        (6, '15'),
        (7, '12'),
        (8, '.6'),
        (9, '10'),
        (10, '.1'),
        (11, '10'),
        (12, '.1'),
        (13, '10'),
        (14, '20'),
        (15, '.7'),
        (16, '.8'),
        (17, '.2'),
        (18, '50'),
        (19, '3'),
        (20, '7'),
        (21, '25000'),
        (22, '5000'),
        (23, '100')
    ) AS SOURCE ([ConfigurationKeyId],[Value])
ON TARGET.[ConfigurationKeyId] = SOURCE.[ConfigurationKeyId]
WHEN MATCHED THEN
    UPDATE SET
        [Value] = SOURCE.[Value]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([ConfigurationKeyId],[Value])
    VALUES ([ConfigurationKeyId],[Value]);