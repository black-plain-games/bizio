PRINT 'Industry'

SET IDENTITY_INSERT [dbo].[Industry] ON

MERGE INTO [dbo].[Industry] AS TARGET
    USING ( VALUES
        (1, 'Software', 'It seems like everything is running some sort of software these days, whether it''s your watch, your TV, or your car. It takes a lot more than one person who understands code to make the software world turn and starting a business has as much potential to be a terrific failure as it does to land you on the cover of Forbes magazine. Do you have what it takes to make a name for yourself in the wild world of technology?')
    ) AS SOURCE ([Id],[Name],[Description])
ON TARGET.[Id] = SOURCE.[Id]
WHEN MATCHED THEN
    UPDATE SET
        [Name] = SOURCE.[Name],
        [Description] = SOURCE.[Description]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([Id],[Name],[Description])
    VALUES ([Id],[Name],[Description]);

SET IDENTITY_INSERT [dbo].[Industry] OFF