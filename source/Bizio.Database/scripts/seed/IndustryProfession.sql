PRINT 'IndustryProfession'

MERGE INTO [dbo].[IndustryProfession] AS TARGET
    USING ( VALUES
        (1, 1),
        (1, 2),
        (1, 3),
        (1, 4),
        (1, 5)
    ) AS SOURCE ([IndustryId],[ProfessionId])
ON TARGET.[IndustryId] = SOURCE.[IndustryId] AND TARGET.[ProfessionId] = SOURCE.[ProfessionId]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([IndustryId],[ProfessionId])
    VALUES ([IndustryId],[ProfessionId]);