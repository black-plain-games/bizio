PRINT 'IndustrySkillDefinition'

MERGE INTO [dbo].[IndustrySkillDefinition] AS TARGET
    USING ( VALUES
        (1, 1),
        (1, 2),
        (1, 3),
        (1, 4),
        (1, 5)
    ) AS SOURCE ([IndustryId],[SkillDefinitionId])
ON TARGET.[IndustryId] = SOURCE.[IndustryId] AND TARGET.[SkillDefinitionId] = SOURCE.[SkillDefinitionId]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([IndustryId],[SkillDefinitionId])
    VALUES ([IndustryId],[SkillDefinitionId]);