PRINT 'Language'

MERGE INTO [dbo].[Language] AS TARGET
	USING ( VALUES
		(1, N'English', 'en'),
		(2, N'español', 'es'),
		(3, N'हिन्दी', 'hi'),
		(4, N'français', 'fr'),
		(5, N'русский', 'ru')
	) AS SOURCE ([Id],[Name],[Iso2LetterCode])
ON TARGET.[Id] = SOURCE.[Id]
WHEN MATCHED THEN
	UPDATE SET
		[Name] = SOURCE.[Name],
		[Iso2LetterCode] = SOURCE.[Iso2LetterCode]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([Id],[Name],[Iso2LetterCode])
	VALUES ([Id],[Name],[Iso2LetterCode]);