PRINT 'MessageLevel'

SET IDENTITY_INSERT [dbo].[MessageLevel] ON

MERGE INTO [dbo].[MessageLevel] AS TARGET
    USING ( VALUES
        (1, 'Debug'),
        (2, 'Warning'),
        (3, 'Exception')
    ) AS SOURCE ([Id],[Description])
ON TARGET.[Id] = SOURCE.[Id]
WHEN MATCHED THEN
    UPDATE SET
        [Description] = SOURCE.[Description]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([Id],[Description])
    VALUES ([Id],[Description]);

SET IDENTITY_INSERT [dbo].[MessageLevel] OFF