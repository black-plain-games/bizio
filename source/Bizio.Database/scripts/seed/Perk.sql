PRINT 'Perk'

SET IDENTITY_INSERT [dbo].[Perk] ON

MERGE INTO [dbo].[Perk] AS TARGET
    USING ( VALUES
        (1, 'Bookshelf', 'Offers a variety of books that boost employees skills slightly.', 1000.00000000, 10.00000000)
    ) AS SOURCE ([Id],[Name],[Description],[InitialCost],[RecurringCost])
ON TARGET.[Id] = SOURCE.[Id]
WHEN MATCHED THEN
    UPDATE SET
        [Name] = SOURCE.[Name],
        [Description] = SOURCE.[Description],
        [InitialCost] = SOURCE.[InitialCost],
        [RecurringCost] = SOURCE.[RecurringCost]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([Id],[Name],[Description],[InitialCost],[RecurringCost])
    VALUES ([Id],[Name],[Description],[InitialCost],[RecurringCost]);

SET IDENTITY_INSERT [dbo].[Perk] OFF