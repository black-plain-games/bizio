PRINT 'PerkBenefit'

MERGE INTO [dbo].[PerkBenefit] AS TARGET
    USING ( VALUES
        (1, 2, 2, 5.00000000, 1),
        (1, 3, 1, 0.01000000, 2)
    ) AS SOURCE ([PerkId],[TargetTypeId],[AttributeId],[Value],[ValueTypeId])
ON TARGET.[PerkId] = SOURCE.[PerkId] AND TARGET.[TargetTypeId] = SOURCE.[TargetTypeId] AND TARGET.[AttributeId] = SOURCE.[AttributeId]
WHEN MATCHED THEN
    UPDATE SET
        [Value] = SOURCE.[Value],
        [ValueTypeId] = SOURCE.[ValueTypeId]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([PerkId],[TargetTypeId],[AttributeId],[Value],[ValueTypeId])
    VALUES ([PerkId],[TargetTypeId],[AttributeId],[Value],[ValueTypeId]);