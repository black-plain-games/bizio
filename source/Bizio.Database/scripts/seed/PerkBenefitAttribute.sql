PRINT 'PerkBenefitAttribute'

SET IDENTITY_INSERT [dbo].[PerkBenefitAttribute] ON

MERGE INTO [dbo].[PerkBenefitAttribute] AS TARGET
    USING ( VALUES
        (1, 'Money'),
        (2, 'Happiness')
    ) AS SOURCE ([Id],[Name])
ON TARGET.[Id] = SOURCE.[Id]
WHEN MATCHED THEN
    UPDATE SET
        [Name] = SOURCE.[Name]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([Id],[Name])
    VALUES ([Id],[Name]);

SET IDENTITY_INSERT [dbo].[PerkBenefitAttribute] OFF