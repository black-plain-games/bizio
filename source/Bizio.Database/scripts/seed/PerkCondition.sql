PRINT 'PerkCondition'

MERGE INTO [dbo].[PerkCondition] AS TARGET
    USING ( VALUES
        (1, 2, 6, 3.00000000, 6),
        (1, 3, 1, 2500.00000000, 6)
    ) AS SOURCE ([PerkId],[TargetTypeId],[AttributeId],[Value],[ComparisonId])
ON TARGET.[PerkId] = SOURCE.[PerkId] AND TARGET.[TargetTypeId] = SOURCE.[TargetTypeId] AND TARGET.[AttributeId] = SOURCE.[AttributeId]
WHEN MATCHED THEN
    UPDATE SET
        [Value] = SOURCE.[Value],
        [ComparisonId] = SOURCE.[ComparisonId]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([PerkId],[TargetTypeId],[AttributeId],[Value],[ComparisonId])
    VALUES ([PerkId],[TargetTypeId],[AttributeId],[Value],[ComparisonId]);