PRINT 'PerkConditionAttribute'

SET IDENTITY_INSERT [dbo].[PerkConditionAttribute] ON

MERGE INTO [dbo].[PerkConditionAttribute] AS TARGET
    USING ( VALUES
        (1, 'Money'),
        (2, 'Number of Perks'),
        (3, 'Number of Projects'),
        (4, 'Number of Prospects'),
        (5, 'Number of Employees'),
        (6, 'Number of Skills'),
        (7, 'Happiness')
    ) AS SOURCE ([Id],[Name])
ON TARGET.[Id] = SOURCE.[Id]
WHEN MATCHED THEN
    UPDATE SET
        [Name] = SOURCE.[Name]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([Id],[Name])
    VALUES ([Id],[Name]);

SET IDENTITY_INSERT [dbo].[PerkConditionAttribute] OFF