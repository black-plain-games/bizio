PRINT 'PerkConditionComparison'

SET IDENTITY_INSERT [dbo].[PerkConditionComparison] ON

MERGE INTO [dbo].[PerkConditionComparison] AS TARGET
    USING ( VALUES
        (1, 'Equal'),
        (2, 'Not equal'),
        (3, 'Less than'),
        (4, 'Less than or equal to'),
        (5, 'Greater than'),
        (6, 'Greater than or equal to')
    ) AS SOURCE ([Id],[Name])
ON TARGET.[Id] = SOURCE.[Id]
WHEN MATCHED THEN
    UPDATE SET
        [Name] = SOURCE.[Name]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([Id],[Name])
    VALUES ([Id],[Name]);

SET IDENTITY_INSERT [dbo].[PerkConditionComparison] OFF