PRINT 'PerkTargetType'

SET IDENTITY_INSERT [dbo].[PerkTargetType] ON

MERGE INTO [dbo].[PerkTargetType] AS TARGET
    USING ( VALUES
        (1, 'Person'),
        (2, 'Employee'),
        (3, 'Company')
    ) AS SOURCE ([Id],[Name])
ON TARGET.[Id] = SOURCE.[Id]
WHEN MATCHED THEN
    UPDATE SET
        [Name] = SOURCE.[Name]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([Id],[Name])
    VALUES ([Id],[Name]);

SET IDENTITY_INSERT [dbo].[PerkTargetType] OFF