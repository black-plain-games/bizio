PRINT 'PerkValueType'

SET IDENTITY_INSERT [dbo].[PerkValueType] ON

MERGE INTO [dbo].[PerkValueType] AS TARGET
    USING ( VALUES
        (1, 'Literal'),
        (2, 'Percentage')
    ) AS SOURCE ([Id],[Name])
ON TARGET.[Id] = SOURCE.[Id]
WHEN MATCHED THEN
    UPDATE SET
        [Name] = SOURCE.[Name]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([Id],[Name])
    VALUES ([Id],[Name]);

SET IDENTITY_INSERT [dbo].[PerkValueType] OFF