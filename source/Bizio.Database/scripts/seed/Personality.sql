PRINT 'Personality'

SET IDENTITY_INSERT [dbo].[Personality] ON

MERGE INTO [dbo].[Personality] AS TARGET
    USING ( VALUES
        (1, 'Neutral', 'Defined by their lack of strong opinion, a neutral person is the most agreeable and easy to please person in a company. It will take a wild decision to bother them and they may still find a way to rationalize it so everything is okay.')
    ) AS SOURCE ([Id],[Name],[Description])
ON TARGET.[Id] = SOURCE.[Id]
WHEN MATCHED THEN
    UPDATE SET
        [Name] = SOURCE.[Name],
        [Description] = SOURCE.[Description]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([Id],[Name],[Description])
    VALUES ([Id],[Name],[Description]);

SET IDENTITY_INSERT [dbo].[Personality] OFF