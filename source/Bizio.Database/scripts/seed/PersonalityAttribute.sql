PRINT 'PersonalityAttribute'

MERGE INTO [dbo].[PersonalityAttribute] AS TARGET
    USING ( VALUES
        (1, 1, 45.00000000),
        (1, 2, 0.50000000),
        (1, 3, 0.50000000),
        (1, 4, 0.50000000),
        (1, 5, 0.50000000),
        (1, 6, 0.50000000),
        (1, 7, 0.50000000),
        (1, 8, 0.50000000),
        (1, 9, 0.50000000),
        (1, 10, 0.75000000),
        (1, 11, 0.50000000),
        (1, 12, 0.75000000),
        (1, 13, 1.00000000),
        (1, 14, 0.25000000),
        (1, 15, 1.00000000),
        (1, 16, 0.00000000),
        (1, 17, 2.00000000),
        (1, 18, 0.75000000),
        (1, 19, 2.00000000),
        (1, 20, 0.90000000),
        (1, 21, 0.85000000),
        (1, 22, 2.00000000),
        (1, 23, 0.80000000),
        (1, 24, 8.00000000),
        (1, 25, 100000.00000000),
        (1, 26, 400000.00000000),
        (1, 27, 0.05000000),
        (1, 28, 0.10000000),
        (1, 29, 10.00000000),
        (1, 30, 0.40000000),
        (1, 31, 8.00000000),
        (1, 32, 0.20000000),
        (1, 33, 3.00000000),
        (1, 34, 10.00000000),
        (1, 35, 0.50000000),
        (1, 36, 5.00000000),
        (1, 37, 1.00000000)
    ) AS SOURCE ([PersonalityId],[AttributeId],[Value])
ON TARGET.[PersonalityId] = SOURCE.[PersonalityId] AND TARGET.[AttributeId] = SOURCE.[AttributeId]
WHEN MATCHED THEN
    UPDATE SET
        [Value] = SOURCE.[Value]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([PersonalityId],[AttributeId],[Value])
    VALUES ([PersonalityId],[AttributeId],[Value]);