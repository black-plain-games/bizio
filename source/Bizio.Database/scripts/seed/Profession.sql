PRINT 'Profession'

SET IDENTITY_INSERT [dbo].[Profession] ON

MERGE INTO [dbo].[Profession] AS TARGET
    USING ( VALUES
        (1, 'Executive'),
        (2, 'Designer'),
        (3, 'Engineer'),
        (4, 'Tester'),
        (5, 'Salesperson'),
        (6, 'Generalist')
    ) AS SOURCE ([Id],[Name])
ON TARGET.[Id] = SOURCE.[Id]
WHEN MATCHED THEN
    UPDATE SET
        [Name] = SOURCE.[Name]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([Id],[Name])
    VALUES ([Id],[Name]);

SET IDENTITY_INSERT [dbo].[Profession] OFF