PRINT 'ProfessionSkillDefinition'

MERGE INTO [dbo].[ProfessionSkillDefinition] AS TARGET
    USING ( VALUES
        -- Executive	
        (1, 6, 255),	-- Planning
        (1, 7, 255),	-- Analysis
        (1, 8, 255),	-- Leadership
        (1, 9, 255),	-- Interface Design
        (1, 10, 255),	-- User Experience
        (1, 11, 255),	-- Creativity
        (1, 12, 255),	-- Server Programming
        (1, 13, 255),	-- Interface Programming
        (1, 14, 255),	-- Database Programming
        (1, 15, 255),	-- Manual Testing
        (1, 16, 255),	-- Automated Testing
        (1, 17, 255),	-- Test Planning
        (1, 18, 255),	-- Marketing
        (1, 19, 255),	-- Pricing
        (1, 20, 255),	-- Documentation
	
        -- Designer	
        (2, 6, 255),	-- Planning
        (2, 7, 255),	-- Analysis
        (2, 8, 255),	-- Leadership
        (2, 9, 255),	-- Interface Design
        (2, 10, 255),	-- User Experience
        (2, 11, 255),	-- Creativity
        (2, 12, 255),	-- Server Programming
        (2, 13, 255),	-- Interface Programming
        (2, 14, 255),	-- Database Programming
        (2, 15, 255),	-- Manual Testing
        (2, 16, 255),	-- Automated Testing
        (2, 17, 255),	-- Test Planning
        (2, 18, 255),	-- Marketing
        (2, 19, 255),	-- Pricing
        (2, 20, 255),	-- Documentation
	
        -- Engineer	
        (3, 6, 255),	-- Planning
        (3, 7, 255),	-- Analysis
        (3, 8, 255),	-- Leadership
        (3, 9, 255),	-- Interface Design
        (3, 10, 255),	-- User Experience
        (3, 11, 255),	-- Creativity
        (3, 12, 255),	-- Server Programming
        (3, 13, 255),	-- Interface Programming
        (3, 14, 255),	-- Database Programming
        (3, 15, 255),	-- Manual Testing
        (3, 16, 255),	-- Automated Testing
        (3, 17, 255),	-- Test Planning
        (3, 18, 255),	-- Marketing
        (3, 19, 255),	-- Pricing
        (3, 20, 255),	-- Documentation
	
        -- Tester	
        (4, 6, 255),	-- Planning
        (4, 7, 255),	-- Analysis
        (4, 8, 255),	-- Leadership
        (4, 9, 255),	-- Interface Design
        (4, 10, 255),	-- User Experience
        (4, 11, 255),	-- Creativity
        (4, 12, 255),	-- Server Programming
        (4, 13, 255),	-- Interface Programming
        (4, 14, 255),	-- Database Programming
        (4, 15, 255),	-- Manual Testing
        (4, 16, 255),	-- Automated Testing
        (4, 17, 255),	-- Test Planning
        (4, 18, 255),	-- Marketing
        (4, 19, 255),	-- Pricing
        (4, 20, 255),	-- Documentation
	
        -- Salesperson	
        (5, 6, 255),	-- Planning
        (5, 7, 255),	-- Analysis
        (5, 8, 255),	-- Leadership
        (5, 9, 255),	-- Interface Design
        (5, 10, 255),	-- User Experience
        (5, 11, 255),	-- Creativity
        (5, 12, 255),	-- Server Programming
        (5, 13, 255),	-- Interface Programming
        (5, 14, 255),	-- Database Programming
        (5, 15, 255),	-- Manual Testing
        (5, 16, 255),	-- Automated Testing
        (5, 17, 255),	-- Test Planning
        (5, 18, 255),	-- Marketing
        (5, 19, 255),	-- Pricing
        (5, 20, 255),	-- Documentation
	
        -- Generalist	
        (6, 6, 255),	-- Planning
        (6, 7, 255),	-- Analysis
        (6, 8, 255),	-- Leadership
        (6, 9, 255),	-- Interface Design
        (6, 10, 255),	-- User Experience
        (6, 11, 255),	-- Creativity
        (6, 12, 255),	-- Server Programming
        (6, 13, 255),	-- Interface Programming
        (6, 14, 255),	-- Database Programming
        (6, 15, 255),	-- Manual Testing
        (6, 16, 255),	-- Automated Testing
        (6, 17, 255),	-- Test Planning
        (6, 18, 255),	-- Marketing
        (6, 19, 255),	-- Pricing
        (6, 20, 255)	-- Documentation
    ) AS SOURCE ([ProfessionId],[SkillDefinitionId],[Weight])
ON TARGET.[ProfessionId] = SOURCE.[ProfessionId] AND TARGET.[SkillDefinitionId] = SOURCE.[SkillDefinitionId]
WHEN MATCHED THEN
    UPDATE SET
        [Weight] = SOURCE.[Weight]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([ProfessionId],[SkillDefinitionId],[Weight])
    VALUES ([ProfessionId],[SkillDefinitionId],[Weight]);