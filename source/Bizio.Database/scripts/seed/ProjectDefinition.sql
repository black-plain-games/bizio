PRINT 'ProjectDefinition'

SET IDENTITY_INSERT [dbo].[ProjectDefinition] ON

MERGE INTO [dbo].[ProjectDefinition] AS TARGET
    USING ( VALUES
        (1, 'Manage an integration project', 'Our company is working on integrating with a brand new platform and our current management team is out of their league. Can you step in an manage our team to success?', 2750, 9000, 5, 10),
        (2, 'Redesign online shopping portal', 'We''re trying to keep up with our competitors in the online retail space but our site design is really out of date and unintuitive. We need someone to overhaul our shopping experience and bring it to the modern era.', 2750, 9000, 5, 10),
        (3, 'Build internal tools', 'Our development team is swamped with work but our support and sales staff desperately need better tools to work with. We need someone to make some rough-and-dirty tools our team can use to support their business workflows.', 2750, 9000, 5, 10),
        (4, 'Verify app for release', 'We''ve been working on a proprietary IP that will hit the app store soon but our QA team isn''t catching things and providers have repeatedly declined to publish our app until it is more stable. We need someone to assist us in identifying and documenting these issues so we can get this published.', 2750, 9000, 5, 10),
        (5, 'Create sales plan', 'We''ve been so focused on engineering and testing our algorithm that we never properly planned for the day when it was ready to sell. We''re a business-to-business company so we need someone to help up come up with a formal strategy to market and sell this algorithm to prospective clients.', 2750, 9000, 5, 10),
        (6, 'Donation proof of concept', 'We need to secure funding for our large donation platform idea, but we need to show prospective investors what it will look like first.', 11000, 36000, 6, 12),
        (7, 'Employee management portal', 'We''re really unhappy with the current tools we use to track employees working hours. We need a couple of people to help us put together something that we can use to digitize our punch cards.', 11000, 36000, 6, 12),
        (8, 'Oversee QA department', 'We currently have a handful of QA testing one of our major projects but it isn''t going well. We need some experience both in testing and in management to help our staff get their act together and properly QA our product before it''s released.', 11000, 36000, 6, 12),
        (9, 'Publish indie game', 'My team and I have put together a great game! We''ve worked day and night for months now to get it just right, bug free, and looking perfect. Our problem is: we didn''t think about how to sell this thing! We need your help getting our title in front of the right people and helping us turn a profit here.', 11000, 36000, 6, 12),
        (10, 'Great idea for an app', 'A few of us have come up with a business plan for a great app and are looking for a design/engineering team to help us take it to production. We can share more details once you''ve been signed onto the project.', 11000, 36000, 6, 12),
        (11, 'Basically complete online photo editor', 'I''ve been working tirelessly on these back-end photo editing algorithms for months and I decided to put a UI on it. Now I''ve got this great web photo editor almost totally complete, but it needs some front-end polish and minor testing. Think you can help?', 11000, 36000, 6, 12),
        (12, 'Data entry plugin', 'Our team has completed development and verification of a universal plugin for data entry needs. This is very marketable to other businesses, large and small, that do data entry operations. Our problem is that it needs to look more user friendly and we need to strategize how we will sell this to clients.', 11000, 36000, 6, 12),
        (13, 'Develop business APIs', 'Our product has long been a useful webpage for customers that pay to use it. Lately, we''ve been getting a lot of requests to expose our services to other engineers using APIs instead of just our web page. We don''t have the development staff on hand to meet these needs but we recognize the monetization potential here.', 11000, 36000, 6, 12),
        (14, 'On demand blog building', 'I''ve been courting a lot of up-and-comers who are looking for customized blogs, beyond what your basic blog-builders can provide. Generally speaking the blogs aren''t too complicated, but depending on what the client asks for we also need to figure out how to properly charge them for the work.', 11000, 36000, 6, 12),
        (15, 'Co-contract project', 'Our contracting service has the leadership, design, and programming experience to meet the needs of our clients. We''re looking to sub-contract the testing and sales work if the numbers make sense.', 11000, 36000, 6, 12),
        (16, 'Mall kiosks', 'A local mall has asked us to design and develop several digital information kiosks for their guests.', 24750, 81000, 7, 14),
        (17, 'Revamp guest check in service', 'Many restaurants use our digital check in service but it''s in dire need of modernization.', 24750, 81000, 7, 14),
        (18, 'Dog and pony show', 'We''re trying to raise funding (and awareness) for the digital marketing services we provide to companies in need. We need to put together a razzle-dazzle display that users (potential clients) can play with to understand exactly what we do.', 24750, 81000, 7, 14),
        (19, 'Project way behind schedule', 'Our tech team is in the process of being exited and entirely replaced for botching several of our business critical systems. We need someone to come in, short term, and fix what they''ve broken.', 24750, 81000, 7, 14),
        (20, 'Beyond MVP', 'We''re finally bringing our answer to our rivals product to market shortly but due to several business reasons we''ve only been able to maintain parity with them. We need a tiger-team to help us out during this last leg of the cycle by adding in some key ''wants'' our customers have identified. This will surely help us steal some of our competition''s customers.', 24750, 81000, 7, 14),
        (21, 'Bring it to production', 'Our programmers have finished their end of the work way ahead of schedule, which is great. The problem is that we didn''t have time to appropriately staff up for testing and sales. We need some help getting our development-complete project fully complete.', 24750, 81000, 7, 14),
        (22, 'Core redo', 'Our app is old, let''s face it. We''ve long since gotten rid of the original talent that built it and now we need to just redo the whole thing.', 24750, 81000, 7, 14),
        (23, 'Overhaul', 'We''ve spent months getting our product to this point but something just doesn''t feel right. We''ve identified the shift we need to make to get closer to our original vision but now we need help getting there.', 24750, 81000, 7, 14),
        (24, 'Retirement idea', 'Since I''ve retired I''ve been tinkering with an idea I''ve had for a while. I''m a self taught programmer and am proud that I''ve finished what I started. My family keeps telling me to sell what I''ve made but I know it''s not ready for the public yet. I need your help polishing and selling this product.', 24750, 81000, 7, 14),
        (25, 'Precision instruments', 'Our company has long made analog instruments that are widely touted to be the best, most accurate instruments available. We recognize the push for digital and are looking to expand our operations in that direction. We need a team to help us bring our most popular device to the digital age.', 24750, 81000, 7, 14),
        (26, 'Digital content manager', 'We''re looking to build a system that allows customers to manage all of their digital content online.', 44000, 144000, 8, 16),
        (27, 'Document collaborator', 'Our app allows users to work together when editing their spreadsheets, documents, slideshows, pictures, you name it!', 44000, 144000, 8, 16),
        (28, 'Starting a business', 'I recently graduated from a very prestigous institution with my Masters degree, majoring in computer science with a dual minor in cyber security as well as drama. My father, a respected alumni of the same institution, has gifted me the captial to undertake my first true business venture. I''ll share more when I''ve fully vetted you, but know that this venture may make us both very wealthy.', 44000, 144000, 8, 16),
        (29, 'Insurance sales platform', 'Since we''re in the business of insurance, we''re not looking to become a ''tech shop''. We''d like to hire a group of contracts to build us an online sales platform so we can allow our customers to buy insurance online.', 44000, 144000, 8, 16),
        (30, 'Corporate restructuring', 'We''re going through a lot of changes, top down of course. In the interim we''re going to need a task force to keep our projects on track. This includes all of our engineering, creative, and sales initiatives.', 44000, 144000, 8, 16),
        (31, 'Finish a hit game', 'We''re so far behind schedule and there are not enough hours in the day to get this game finished in time. We''ve got the capital to bring in whoever makes sense, but we''re desperate to get this done on time. Please help.', 68750, 225000, 10, 20)
    ) AS SOURCE ([Id],[Name],[Description],[MinimumValue],[MaximumValue],[MinimumProjectLength],[MaximumProjectLength])
ON TARGET.[Id] = SOURCE.[Id]
WHEN MATCHED THEN
    UPDATE SET
        [Name] = SOURCE.[Name],
        [Description] = SOURCE.[Description],
        [MinimumValue] = SOURCE.[MinimumValue],
        [MaximumValue] = SOURCE.[MaximumValue],
        [MinimumProjectLength] = SOURCE.[MinimumProjectLength],
        [MaximumProjectLength] = SOURCE.[MaximumProjectLength]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([Id],[Name],[Description],[MinimumValue],[MaximumValue],[MinimumProjectLength],[MaximumProjectLength])
    VALUES ([Id],[Name],[Description],[MinimumValue],[MaximumValue],[MinimumProjectLength],[MaximumProjectLength]);

SET IDENTITY_INSERT [dbo].[ProjectDefinition] OFF