PRINT 'ProjectResult'

SET IDENTITY_INSERT [dbo].[ProjectResult] ON

MERGE INTO [dbo].[ProjectResult] AS TARGET
    USING ( VALUES
        (1, 'Success'),
        (2, 'Cancelled'),
        (3, 'Failure')
    ) AS SOURCE ([Id],[Name])
ON TARGET.[Id] = SOURCE.[Id]
WHEN MATCHED THEN
    UPDATE SET
        [Name] = SOURCE.[Name]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([Id],[Name])
    VALUES ([Id],[Name]);

SET IDENTITY_INSERT [dbo].[ProjectResult] OFF