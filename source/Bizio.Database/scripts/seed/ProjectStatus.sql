PRINT 'ProjectStatus'

SET IDENTITY_INSERT [dbo].[ProjectStatus] ON

MERGE INTO [dbo].[ProjectStatus] AS TARGET
    USING ( VALUES
        (1, 'Unassigned'),
        (2, 'In Progress'),
        (3, 'Completed')
    ) AS SOURCE ([Id],[Name])
ON TARGET.[Id] = SOURCE.[Id]
WHEN MATCHED THEN
    UPDATE SET
        [Name] = SOURCE.[Name]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([Id],[Name])
    VALUES ([Id],[Name]);

SET IDENTITY_INSERT [dbo].[ProjectStatus] OFF