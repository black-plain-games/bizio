PRINT 'Role'

SET IDENTITY_INSERT [dbo].[Role] ON

MERGE INTO [dbo].[Role] AS TARGET
    USING ( VALUES
        (1, 'Training'),
        (2, 'Staff'),
        (3, 'Lead'),
        (4, 'Manager')
    ) AS SOURCE ([Id],[Name])
ON TARGET.[Id] = SOURCE.[Id]
WHEN MATCHED THEN
    UPDATE SET
        [Name] = SOURCE.[Name]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([Id],[Name])
    VALUES ([Id],[Name]);

SET IDENTITY_INSERT [dbo].[Role] OFF