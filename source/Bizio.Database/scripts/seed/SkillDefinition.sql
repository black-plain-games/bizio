PRINT 'SkillDefinition'

SET IDENTITY_INSERT [dbo].[SkillDefinition] ON

MERGE INTO [dbo].[SkillDefinition] AS TARGET
    USING ( VALUES
        (1, 'Administration', 0, 255),
        (2, 'Accounting', 0, 255),
        (3, 'Communication', 0, 255),
        (4, 'Research', 0, 255),
        (5, 'Writing', 0, 255),

        (6, 'Planning', 0, 255),
        (7, 'Analysis', 0, 255),
        (8, 'Leadership', 0, 255),

        (9, 'Interface Design', 0, 255),
        (10, 'User Experience', 0, 255),
        (11, 'Creativity', 0, 255),

        (12, 'Server Programming', 0, 255),
        (13, 'Interface Programming', 0, 255),
        (14, 'Database Programming', 0, 255),

        (15, 'Manual Testing', 0, 255),
        (16, 'Automated Testing', 0, 255),
        (17, 'Test Planning', 0, 255),

        (18, 'Marketing', 0, 255),
        (19, 'Pricing', 0, 255),
        (20, 'Documentation', 0, 255)

    ) AS SOURCE ([Id],[Name],[Minimum],[Maximum])
ON TARGET.[Id] = SOURCE.[Id]
WHEN MATCHED THEN
    UPDATE SET
        [Name] = SOURCE.[Name],
        [Minimum] = SOURCE.[Minimum],
        [Maximum] = SOURCE.[Maximum]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([Id],[Name],[Minimum],[Maximum])
    VALUES ([Id],[Name],[Minimum],[Maximum]);

SET IDENTITY_INSERT [dbo].[SkillDefinition] OFF