﻿using Bizio.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Repositories
{
    internal static class CollectionExtensions
    {
        public static IDictionary<Guid, TValue> ToDictionary<TValue>(this IEnumerable<TValue> collection)
            where TValue : IIdentifiable<Guid>
        {
            return collection.ToDictionary(i => i.Id);
        }
    }
}
