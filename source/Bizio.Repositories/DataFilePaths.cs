﻿namespace Bizio.Repositories
{
    public static class DataFilePaths
    {
        public static string StaticDataFilePath = "bizio.json";
        public static string ConfigurationFilePath = "config.json";
        public static string LanguagesFilePath = "languages.json";
        public static string GamesFilePath = "games.json";
    }
}