﻿using Bizio.Core.Data;
using Bizio.Core.Data.Actions;
using Bizio.Core.Data.Companies;
using Bizio.Core.Data.Configuration;
using Bizio.Core.Data.People;
using Bizio.Core.Data.Perks;
using Bizio.Core.Data.Projects;
using Bizio.Core.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bizio.Repositories
{
    public interface IDefaultDataGenerator
    {
        Task GenerateDefaultBizioData(CancellationToken cancellationToken = default);
        Task GenerateDefaultConfigurationData(CancellationToken cancellationToken = default);
        Guid Id(GuidUse g);
    }

    internal class DefaultDataGenerator : IDefaultDataGenerator
    {
        public async Task GenerateDefaultBizioData(CancellationToken cancellationToken = default)
        {
            var data = GetBizioData();

            var json = new StringBuilder(JsonConvert.SerializeObject(data, Formatting.Indented));

            using (var writer = new StreamWriter(DataFilePaths.StaticDataFilePath))
            {
                await writer.WriteAsync(json, cancellationToken);
            }
        }

        public async Task GenerateDefaultConfigurationData(CancellationToken cancellationToken = default)
        {
            var data = GetConfiguration();

            var json = new StringBuilder(JsonConvert.SerializeObject(data, Formatting.Indented));

            using (var writer = new StreamWriter(DataFilePaths.ConfigurationFilePath))
            {
                await writer.WriteAsync(json, cancellationToken);
            }
        }

        private StaticData GetBizioData()
        {
            return new StaticData
            {
                Actions = GetActions().ToDictionary(),
                DefaultPeople = GetPeople().ToDictionary(),
                FirstNames = GetFirstNames(),
                Industries = GetIndustries().ToDictionary(),
                LastNames = GetLastNames(),
                Personalities = GetPersonalities().ToDictionary(),
                SkillDefinitions = GetSkillDefinitions().ToDictionary(),
            };
        }

        private IEnumerable<Core.Data.Actions.Action> GetActions()
        {
            return [
                new()
                {
                    Id = Id(GuidUse.ActionInterviewProspect),
                    Type = ActionType.InterviewProspect,
                    Name = "Interview Prospect",
                    DefaultCount = 1,
                    MaxCount = 10,
                    Requirements = [
                        new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionAdministration), Value = 50 },
                        new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionAccounting), Value = 50 },
                        new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionResearch), Value = 75 }
                    ]
                },
                new()
                {
                    Id = Id(GuidUse.ActionMakeOffer),
                    Type = ActionType.MakeOffer,
                    Name = "Make Offer",
                    DefaultCount = 1,
                    MaxCount = 10,
                    Requirements = [
                        new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionAdministration), Value = 50 },
                        new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionAccounting), Value = 75 },
                        new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionWriting), Value = 50 }
                    ]
                },
                new()
                {
                    Id = Id(GuidUse.ActionFireEmployee),
                    Type = ActionType.FireEmployee,
                    Name = "Fire Employee",
                    DefaultCount = 1,
                    MaxCount = 5,
                    Requirements = [
                        new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionAdministration), Value = 100 },
                        new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionAccounting), Value = 100 },
                        new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionCommunication), Value = 100 },
                        new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionWriting), Value = 100 }
                    ]
                },
                new()
                {
                    Id = Id(GuidUse.ActionAdjustSalary),
                    Type = ActionType.AdjustSalary,
                    Name = "Adjust Salary",
                    DefaultCount = 1,
                    MaxCount = 10,
                    Requirements = [
                        new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionAdministration), Value = 50 },
                        new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionAccounting), Value = 100 },
                        new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionWriting), Value = 50 }
                    ]
                },
                new()
                {
                    Id = Id(GuidUse.ActionAdjustAllocation),
                    Type = ActionType.AdjustAllocation,
                    Name = "Adjust Allocation",
                    DefaultCount = 10,
                    MaxCount = 1000,
                    Requirements = [
                        new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionAdministration), Value = 50 },
                        new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionAccounting), Value = 100 }
                    ]
                },
                new()
                {
                    Id = Id(GuidUse.ActionAcceptProject),
                    Type = ActionType.AcceptProject,
                    Name = "Accept Project",
                    DefaultCount = 1,
                    MaxCount = 10,
                    Requirements = [
                        new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionAdministration), Value = 50 },
                        new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionAccounting), Value = 100 },
                        new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionCommunication), Value = 75 },
                        new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionResearch), Value = 100 }
                    ]
                },
                new()
                {
                    Id = Id(GuidUse.ActionRequestExtension),
                    Type = ActionType.RequestExtension,
                    Name = "Request Extension",
                    DefaultCount = 1,
                    MaxCount = 10,
                    Requirements = [
                        new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionAdministration), Value = 100 },
                        new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionAccounting), Value = 100 },
                        new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionCommunication), Value = 100 },
                        new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionWriting), Value = 100 }
                    ]
                },
                new()
                {
                    Id = Id(GuidUse.ActionSubmitProject),
                    Type = ActionType.SubmitProject,
                    Name = "Submit Project",
                    DefaultCount = 1,
                    MaxCount = 10,
                    Requirements = [
                        new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionAdministration), Value = 50 },
                        new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionAccounting), Value = 100 },
                        new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionCommunication), Value = 50 }
                    ]
                },
                new()
                {
                    Id = Id(GuidUse.ActionPurchasePerk),
                    Type = ActionType.PurchasePerk,
                    Name = "Purchase Perk",
                    DefaultCount = 1,
                    MaxCount = 10,
                    Requirements = [
                        new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionAdministration), Value = 50 },
                        new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionAccounting), Value = 50 }
                    ]
                },
                new()
                {
                    Id = Id(GuidUse.ActionSellPerk),
                    Type = ActionType.SellPerk,
                    Name = "Sell Perk",
                    DefaultCount = 1,
                    MaxCount = 10,
                    Requirements = [
                        new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionAdministration), Value = 50 },
                        new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionAccounting), Value = 50 }
                    ]
                },
                new()
                {
                    Id = Id(GuidUse.ActionChangeMessageStatus),
                    Type = ActionType.ChangeCompanyMessageStatus,
                    Name = "Change Message Status",
                    DefaultCount = 1000,
                    MaxCount = 1000,
                    Requirements = []
                },
                new()
                {
                    Id = Id(GuidUse.ActionToggleCompanyAction),
                    Type = ActionType.ToggleCompanyAction,
                    Name = "Toggle Company Action",
                    DefaultCount = 1000,
                    MaxCount = 1000,
                    Requirements = []
                }
            ];
        }

        private Dictionary<ConfigurationKey, string> GetConfiguration()
        {
            return new()
            {
                { ConfigurationKey.PercentFemale, "50" },
                { ConfigurationKey.MinimumOptionalSkillCount, "1" },
                { ConfigurationKey.MaximumOptionalSkillCount, "6" },
                { ConfigurationKey.InitialPersonPoolSize, "25" },
                { ConfigurationKey.PersonGenerationBatchSize, "5" },
                { ConfigurationKey.ProjectGenerationBatchSize, "15" },
                { ConfigurationKey.MaximumExtensionLength, "12" },
                { ConfigurationKey.PerkResellFactor, ".6" },
                { ConfigurationKey.LargeCompanySize, "10" },
                { ConfigurationKey.CompanySizeMaxDifferential, ".1" },
                { ConfigurationKey.FastCompanyGrowthValue, "10" },
                { ConfigurationKey.CompanyGrowthMaxDifferential, ".1" },
                { ConfigurationKey.MinimumPersonCount, "10" },
                { ConfigurationKey.MinimumProjectCount, "20" },
                { ConfigurationKey.ProjectCancelledPercent, ".7" },
                { ConfigurationKey.ProjectFailedPercent, ".8" },
                { ConfigurationKey.ProjectExtensionValueAdjustmentPercent, ".2" },
                { ConfigurationKey.DefaultSkillWeight, "50" },
                { ConfigurationKey.MandatorySkillSalaryWeight, "3" },
                { ConfigurationKey.OptionalSkillSalaryWeight, "7" },
                { ConfigurationKey.BaseSalary, "25000" },
                { ConfigurationKey.SalaryCalculationMagnitude, "5000" },
                { ConfigurationKey.EmployeeMaintenanceCost, "100" },
                { ConfigurationKey.DefaultProspectAccuracy, "25" }
            };
        }

        private static IEnumerable<Person> GetPeople()
        {
            return [];
        }

        private Dictionary<Gender, IEnumerable<string>> GetFirstNames()
        {
            return new()
            {
                { Gender.Male, ["Aaron", "Adam", "Adrian", "Alan", "Albert", "Alex", "Alexander", "Alfred", "Allan", "Allen", "Alvin", "Andre", "Andrew", "Angel", "Anthony", "Antonio", "Arnold", "Arthur", "Barry", "Ben", "Benjamin", "Bernard", "Bill", "Billy", "Bobby", "Brad", "Bradley", "Brandon", "Brent", "Brett", "Brian", "Bruce", "Bryan", "Calvin", "Carl", "Carlos", "Cecil", "Chad", "Charles", "Charlie", "Chester", "Chris", "Christian", "Christopher", "Clarence", "Claude", "Clifford", "Clinton", "Clyde", "Cody", "Corey", "Cory", "Craig", "Curtis", "Dale", "Dan", "Daniel", "Danny", "Darrell", "Darren", "Darryl", "David", "Dean", "Dennis", "Derek", "Derrick", "Don", "Donald", "Douglas", "Duane", "Dustin", "Earl", "Eddie", "Edgar", "Edward", "Edwin", "Elmer", "Eric", "Erik", "Ernest", "Eugene", "Fernando", "Floyd", "Francis", "Francisco", "Frank", "Franklin", "Fred", "Frederick", "Gabriel", "Gary", "Gene", "George", "Gerald", "Gilbert", "Glen", "Glenn", "Gordon", "Greg", "Gregory", "Harold", "Harry", "Harvey", "Hector", "Henry", "Herbert", "Herman", "Howard", "Jack", "Jacob", "James", "Jamie", "Jared", "Jason", "Javier", "Jay", "Jeff", "Jeffery", "Jeffrey", "Jeremy", "Jerome", "Jerry", "Jesse", "Jessie", "Jesus", "Jim", "Jimmy", "Joe", "Joel", "John", "Johnny", "Jon", "Jonathan", "Jorge", "Jose", "Joseph", "Joshua", "Juan", "Julio", "Justin", "Karl", "Keith", "Kelly", "Kenneth", "Kevin", "Kurt", "Kyle", "Lance", "Larry", "Lawrence", "Lee", "Leo", "Leon", "Leonard", "Leroy", "Leslie", "Lester", "Lewis", "Lloyd", "Lonnie", "Louis", "Luis", "Manuel", "Marc", "Marcus", "Mario", "Mark", "Martin", "Marvin", "Mathew", "Matthew", "Maurice", "Melvin", "Michael", "Miguel", "Mike", "Milton", "Mitchell", "Nathan", "Nathaniel", "Neil", "Nicholas", "Norman", "Oscar", "Patrick", "Paul", "Pedro", "Peter", "Philip", "Phillip", "Rafael", "Ralph", "Ramon", "Randall", "Randy", "Raul", "Ray", "Raymond", "Reginald", "Ricardo", "Richard", "Rick", "Ricky", "Robert", "Roberto", "Rodney", "Roger", "Roland", "Ron", "Ronald", "Ronnie", "Roy", "Ruben", "Russell", "Ryan", "Sam", "Samuel", "Scott", "Shane", "Shaun", "Shawn", "Stanley", "Stephen", "Steve", "Steven", "Ted", "Terry", "Theodore", "Thomas", "Tim", "Timothy", "Todd", "Tom", "Tommy", "Tony", "Travis", "Troy", "Tyler", "Tyrone", "Vernon", "Victor", "Vincent", "Walter", "Warren", "Wayne", "Wesley", "William", "Willie", "Zachary"] },
                { Gender.Female, ["Agnes", "Alice", "Alicia", "Allison", "Alma", "Amanda", "Amber", "Amy", "Ana", "Andrea", "Angela", "Anita", "Ann", "Anna", "Anne", "Annette", "April", "Arlene", "Ashley", "Audrey", "Barbara", "Beatrice", "Bernice", "Bertha", "Bessie", "Beth", "Betty", "Beverly", "Bonnie", "Brenda", "Brittany", "Carla", "Carmen", "Carol", "Caroline", "Carolyn", "Carrie", "Catherine", "Cathy", "Charlene", "Charlotte", "Cheryl", "Christina", "Christine", "Cindy", "Clara", "Claudia", "Colleen", "Connie", "Constance", "Courtney", "Crystal", "Cynthia", "Dana", "Danielle", "Darlene", "Dawn", "Debbie", "Deborah", "Debra", "Delores", "Denise", "Diana", "Diane", "Dolores", "Donna", "Dora", "Doris", "Dorothy", "Edith", "Edna", "Eileen", "Elaine", "Eleanor", "Elizabeth", "Ella", "Ellen", "Elsie", "Emily", "Emma", "Erica", "Erin", "Esther", "Ethel", "Eva", "Evelyn", "Florence", "Francis", "Gail", "Georgia", "Geraldine", "Gertrude", "Gina", "Gladys", "Glenda", "Gloria", "Grace", "Hazel", "Heather", "Heidi", "Helen", "Holly", "Ida", "Irene", "Jackie", "Jacqueline", "Jamie", "Jane", "Janet", "Janice", "Jean", "Jeanette", "Jeanne", "Jennifer", "Jessica", "Jessie", "Jill", "Jo", "Joan", "Joann", "Joanne", "Josephine", "Joy", "Joyce", "Juanita", "Judith", "Judy", "Julia", "Julie", "June", "Karen", "Katherine", "Kathleen", "Kathrine", "Kathy", "Katie", "Kelly", "Kim", "Kimberly", "Kristen", "Laura", "Lauren", "Laurie", "Leslie", "Lillian", "Lillie", "Linda", "Lisa", "Lois", "Loretta", "Lori", "Lorraine", "Louise", "Lucille", "Lucy", "Lydia", "Lynn", "Marcia", "Margaret", "Maria", "Marian", "Marie", "Marilyn", "Marion", "Marjorie", "Marlene", "Martha", "Mary", "Maureen", "Megan", "Melanie", "Melinda", "Melissa", "Michele", "Michelle", "Mildred", "Minnie", "Monica", "Nancy", "Natalie", "Nellie", "Nicole", "Norma", "Pamela", "Patricia", "Paula", "Pauline", "Pearl", "Peggy", "Phyllis", "Rachel", "Rebecca", "Regina", "Renee", "Rhonda", "Rita", "Roberta", "Robin", "Rosa", "Rose", "Rosemary", "Ruby", "Ruth", "Sally", "Samantha", "Sandra", "Sara", "Sarah", "Shannon", "Sharon", "Sheila", "Sherry", "Shirley", "Stacey", "Stacy", "Stella", "Stephanie", "Sue", "Susan", "Suzanne", "Sylvia", "Tamara", "Tammy", "Tanya", "Tara", "Teresa", "Terri", "Thelma", "Theresa", "Tiffany", "Tina", "Tonya", "Tracy", "Valerie", "Vanessa", "Vera", "Veronica", "Vicki", "Victoria", "Viola", "Virginia", "Vivian", "Wanda", "Wendy", "Willie", "Wilma", "Yolanda", "Yvonne"] }
            };
        }

        private IEnumerable<Industry> GetIndustries()
        {
            return [
                new Industry
                {
                    Id = Id(GuidUse.IndustrySoftwareEngineering),
                    Name = "Software Engineering",
                    CompanyNames = ["Vertex Labs", "CodeAxis", "Apex Digital", "DataSync", "OmniTech Solutions", "CircuitFlow", "EchoAI", "Nimbus Systems", "Vortex Data", "NextEra Logic",  "InfinityWorks", "ZenoTech", "ClearPath Technologies", "Nova Solutions", "BrightLayer", "Nimble Networks", "CoreLogic Systems", "Prime Circuit", "Summit Cloud", "HyperDyne",  "StratusNet", "IntegroSoft", "Visionary Analytics", "Helix Dynamics", "Axon Solutions", "SkyEdge", "PolarNet", "Ardent Logic", "Greenline Systems", "DeepCore AI",  "PulseWorks", "Velocity Dynamics", "QuasarTech", "DeltaNet", "Beacon Systems", "Global Dataworks", "Synapse AI", "Tetra Networks", "ForwardLogic", "Pinnacle Systems",  "Momentum Dynamics", "Astrobyte", "Catalyst Labs", "ClearSky Technologies", "Next Horizon Systems", "BrightMind", "CirrusLogic", "OmniGrid", "NeoSphere", "PrimeWave",  "Sigma AI", "Quantum Stack", "TerraWorks", "NovaNet", "ZenithCore", "Argon Dynamics", "Blue Wave Analytics", "Matrix AI", "Digital Arc", "TrueNorth Systems",  "Vertex Analytics", "Radial Logic", "Cloudline Systems", "OptimaTech", "Perceptive AI", "StrataGrid", "RedSky Solutions", "DataForge", "FlexWare", "Echo Dynamics",  "InfiniSoft", "Keystone Systems", "Apollo Circuitry", "Unity Networks", "Atlas Cloud", "Omega Analytics", "CipherSoft", "Element Edge", "BaseTech", "SkyWorks",  "FutureSync", "Precision Logic", "CoreMetrics", "Delta Dynamics", "BrightNet", "Apex Systems", "Modulus AI", "TechNova", "NexisWare", "Summit Dynamics",  "Alpha Systems", "Luminal Tech", "GridCore", "NewPath Solutions", "Lumen Technologies", "CodeWell", "Horizon Analytics", "UltraNet", "Pinnacle AI", "NXT"],
                    OptionalSkills = [
                        Id(GuidUse.SkillDefinitionPlanning),
                        Id(GuidUse.SkillDefinitionAnalysis),
                        Id(GuidUse.SkillDefinitionLeadership),
                        Id(GuidUse.SkillDefinitionInterfaceDesign),
                        Id(GuidUse.SkillDefinitionUserExperience),
                        Id(GuidUse.SkillDefinitionCreativity),
                        Id(GuidUse.SkillDefinitionServerProgramming),
                        Id(GuidUse.SkillDefinitionInterfaceProgramming),
                        Id(GuidUse.SkillDefinitionDatabaseProgramming),
                        Id(GuidUse.SkillDefinitionManualTesting),
                        Id(GuidUse.SkillDefinitionAutomatedTesting),
                        Id(GuidUse.SkillDefinitionTestPlanning),
                        Id(GuidUse.SkillDefinitionMarketing),
                        Id(GuidUse.SkillDefinitionPricing),
                        Id(GuidUse.SkillDefinitionDocumentation)
                    ],
                    MandatorySkills = [
                        Id(GuidUse.SkillDefinitionAdministration),
                        Id(GuidUse.SkillDefinitionAccounting),
                        Id(GuidUse.SkillDefinitionCommunication),
                        Id(GuidUse.SkillDefinitionResearch),
                        Id(GuidUse.SkillDefinitionWriting)
                    ],
                    ProjectDefinitions = GetProjectDefinitions(),
                    Professions = [
                        new()
                        {
                            Id = Id(GuidUse.ProfessionExecutive),
                            Name = "Executive",
                            SkillDefinitions = GetDefaultProfessionSkillDefinitions()
                        },
                        new()
                        {
                            Id = Id(GuidUse.ProfessionDesigner),
                            Name = "Designer",
                            SkillDefinitions = GetDefaultProfessionSkillDefinitions()
                        },
                        new()
                        {
                            Id = Id(GuidUse.ProfessionEngineer),
                            Name = "Engineer",
                            SkillDefinitions = GetDefaultProfessionSkillDefinitions()
                        },
                        new()
                        {
                            Id = Id(GuidUse.ProfessionTester),
                            Name = "Tester",
                            SkillDefinitions = GetDefaultProfessionSkillDefinitions()
                        },
                        new()
                        {
                            Id = Id(GuidUse.ProfessionSalesperson),
                            Name = "Salesperson",
                            SkillDefinitions = GetDefaultProfessionSkillDefinitions()
                        },
                        new()
                        {
                            Id = Id(GuidUse.ProfessionGeneralist),
                            Name = "Generalist",
                            SkillDefinitions = GetDefaultProfessionSkillDefinitions()
                        },
                    ],
                    Perks = [
                        new()
                        {
                            Id = Id(GuidUse.PerkBookshelf),
                            Name = "Bookshelf",
                            Description = "Offers a variety of books that boost employees skills slightly.",
                            InitialCost = 1000,
                            MaxCount = 10,
                            RecurringCost = 10,
                            RecurringCostInterval = 1,
                            Benefits = [
                                new()
                                {
                                    Target = PerkTargetType.Employee,
                                    Attribute = PerkBenefitAttribute.Happiness,
                                    Value = 5,
                                    ValueType = PerkValueType.Literal
                                },
                                new()
                                {
                                    Target = PerkTargetType.Company,
                                    Attribute = PerkBenefitAttribute.Money,
                                    Value = 1,
                                    ValueType = PerkValueType.Percentage
                                }
                            ],
                            Conditions = [
                                new()
                                {
                                    Target = PerkTargetType.Employee,
                                    Attribute = PerkConditionAttribute.SkillCount,
                                    Comparison = PerkConditionComparisonType.GreaterThanEqualTo,
                                    Value = 6
                                },
                                new()
                                {
                                    Target = PerkTargetType.Company,
                                    Attribute = PerkConditionAttribute.Money,
                                    Comparison = PerkConditionComparisonType.GreaterThanEqualTo,
                                    Value = 2500
                                }
                            ]
                        }
                    ]
                }
            ];
        }

        private static IEnumerable<string> GetLastNames()
        {
            return ["Adams", "Alexander", "Allen", "Alvarez", "Anderson", "Andrews", "Armstrong", "Arnold", "Austin", "Bailey", "Baker", "Banks", "Barnes", "Bell", "Bennett", "Berry", "Bishop", "Black", "Bowman", "Boyd", "Bradley", "Brooks", "Brown", "Bryant", "Burke", "Burns", "Burton", "Butler", "Campbell", "Carpenter", "Carr", "Carroll", "Carter", "Castillo", "Chapman", "Chavez", "Clark", "Cole", "Coleman", "Collins", "Cook", "Cooper", "Cox", "Crawford", "Cruz", "Cunningham", "Daniels", "Davis", "Day", "Dean", "Diaz", "Dixon", "Duncan", "Dunn", "Edwards", "Elliott", "Ellis", "Evans", "Ferguson", "Fernandez", "Fields", "Fisher", "Flores", "Ford", "Foster", "Fowler", "Fox", "Franklin", "Frazier", "Freeman", "Fuller", "Garcia", "Gardner", "Garrett", "Garza", "George", "Gibson", "Gilbert", "Gomez", "Gonzales", "Gonzalez", "Gordon", "Graham", "Grant", "Gray", "Green", "Greene", "Griffin", "Gutierrez", "Hall", "Hamilton", "Hansen", "Hanson", "Harper", "Harris", "Harrison", "Hart", "Harvey", "Hawkins", "Hayes", "Henderson", "Henry", "Hernandez", "Hicks", "Hill", "Holmes", "Howard", "Howell", "Hudson", "Hughes", "Hunt", "Hunter", "Jackson", "Jacobs", "James", "Jenkins", "Johnson", "Johnston", "Jones", "Jordan", "Kelley", "Kelly", "Kennedy", "Kim", "King", "Knight", "Lane", "Larson", "Lawrence", "Lawson", "Lee", "Lewis", "Little", "Long", "Lopez", "Lynch", "Marshall", "Martin", "Martinez", "Mason", "Matthews", "McCoy", "McDonald", "Medina", "Mendoza", "Meyer", "Miller", "Mills", "Mitchell", "Montgomery", "Moore", "Morales", "Moreno", "Morgan", "Morris", "Morrison", "Murphy", "Murray", "Myers", "Nelson", "Nguyen", "Nichols", "Oliver", "Olson", "Ortiz", "Owens", "Palmer", "Parker", "Patterson", "Payne", "Perez", "Perkins", "Perry", "Peters", "Peterson", "Phillips", "Pierce", "Porter", "Powell", "Price", "Ramirez", "Ramos", "Ray", "Reed", "Reid", "Reyes", "Reynolds", "Rice", "Richards", "Richardson", "Riley", "Rivera", "Roberts", "Robertson", "Robinson", "Rodriguez", "Rogers", "Romero", "Rose", "Ross", "Ruiz", "Russell", "Ryan", "Sanchez", "Sanders", "Schmidt", "Scott", "Shaw", "Simmons", "Simpson", "Sims", "Smith", "Snyder", "Spencer", "Stanley", "Stephens", "Stevens", "Stewart", "Stone", "Sullivan", "Taylor", "Thomas", "Thompson", "Torres", "Tucker", "Turner", "Vasquez", "Wagner", "Walker", "Wallace", "Ward", "Warren", "Washington", "Watkins", "Watson", "Weaver", "Webb", "Welch", "Wells", "West", "Wheeler", "White", "Williams", "Williamson", "Willis", "Wilson", "Wood", "Woods", "Wright", "Young"];
        }

        private IEnumerable<Personality> GetPersonalities()
        {
            return [
                new()
                {
                    Id = Id(GuidUse.PersonalityNeutral),
                    Name = "Neutral",
                    Description = "Defined by their lack of strong opinion, a neutral person is the most agreeable and easy to please person in a company. It will take a wild decision to bother them and they may still find a way to rationalize it so everything is okay.",
                    Attributes = [
                        new(PersonalityAttributeId.MinimumRetirementAge, 45),
                        new(PersonalityAttributeId.MoneyFactor, 50),
                        new(PersonalityAttributeId.LiteralPerkFactor, 50),
                        new(PersonalityAttributeId.PercentagePerkFactor, 50),
                        new(PersonalityAttributeId.MandatorySkillFactor, 50),
                        new(PersonalityAttributeId.OptionalSkillFactor, 50),
                        new(PersonalityAttributeId.DesiredCompanySize, 50),
                        new(PersonalityAttributeId.CompanySizeImportance, 50),
                        new(PersonalityAttributeId.DesiredCompanyGrowth, 50),
                        new(PersonalityAttributeId.CompanyGrowthImportance, 75),
                        new(PersonalityAttributeId.MinimumDesiredEmployeeWorkload, 50),
                        new(PersonalityAttributeId.MaximumDesiredEmployeeWorkload, 75),
                        new(PersonalityAttributeId.MinimumPerkCount, 1),
                        new(PersonalityAttributeId.MinimumDesiredRelevance, 25),
                        new(PersonalityAttributeId.MaximumDesiredRelevance, 1),
                        new(PersonalityAttributeId.MaximumOfferReasons, 0),
                        new(PersonalityAttributeId.DesiredProspectPoolSize, 2),
                        new(PersonalityAttributeId.HireProspectUrgency, 75),
                        new(PersonalityAttributeId.DesiredEmployeesPerProject, 2),
                        new(PersonalityAttributeId.DesiredProspectAccuracy, 90),
                        new(PersonalityAttributeId.HirePersonUrgency, 85),
                        new(PersonalityAttributeId.DesiredProjectsPerEmployee, 2),
                        new(PersonalityAttributeId.MinimumProjectCompletionPercent, 80),
                        new(PersonalityAttributeId.WaitPeriodBeforeFirstSalaryAdjustment, 8),
                        new(PersonalityAttributeId.MandatorySkillMaxSalary, 100000),
                        new(PersonalityAttributeId.OptionalSkillMaxSalary, 400000),
                        new(PersonalityAttributeId.RequiredIncreaseForAdjustment, 5),
                        new(PersonalityAttributeId.RequiredDecreaseForAdjustment, 10),
                        new(PersonalityAttributeId.MinimumRetentionTurns, 10),
                        new(PersonalityAttributeId.MinimumMoneyPercentForOffer, 40),
                        new(PersonalityAttributeId.FireEmployeeLookbackWeeks, 8),
                        new(PersonalityAttributeId.FireEmployeeMinimumPositivePayrollPecent, 20),
                        new(PersonalityAttributeId.FireEmployeeLookbackProjectCount, 3),
                        new(PersonalityAttributeId.FireEmployeeMinimumSkillValue, 10),
                        new(PersonalityAttributeId.SellPerkDangerCapitolThreshold, 50),
                        new(PersonalityAttributeId.SellPerkTurnsAboveThreshold, 5),
                        new(PersonalityAttributeId.OfferGreed, 1),
                    ]
                }
            ];
        }

        private IEnumerable<ProfessionSkillDefinition> GetDefaultProfessionSkillDefinitions()
        {
            return [
                new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionPlanning), Weight = 255 },
                new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionAnalysis), Weight = 255 },
                new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionLeadership), Weight = 255 },
                new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionInterfaceDesign), Weight = 255 },
                new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionUserExperience), Weight = 255 },
                new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionCreativity), Weight = 255 },
                new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionServerProgramming), Weight = 255 },
                new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionInterfaceProgramming), Weight = 255 },
                new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionDatabaseProgramming), Weight = 255 },
                new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionManualTesting), Weight = 255 },
                new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionAutomatedTesting), Weight = 255 },
                new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionTestPlanning), Weight = 255 },
                new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionMarketing), Weight = 255 },
                new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionPricing), Weight = 255 },
                new() { SkillDefinitionId = Id(GuidUse.SkillDefinitionDocumentation), Weight = 255 },
            ];
        }

        private IEnumerable<ProjectDefinition> GetProjectDefinitions()
        {
            return [
                new ProjectDefinition
                {
                    Id = Id(GuidUse.ProjectDefinitionAIEnhancedRecommendationSystem),
                    Name = "AI-Enhanced Recommendation System",
                    Description = "Develop an intelligent recommendation engine using machine learning algorithms to personalize user experiences.",
                    Value = new Range<int>(25000, 35000),
                    ProjectLength = new Range<int>(90, 120),
                    Skills = [
                        new(Id(GuidUse.SkillDefinitionServerProgramming), 3000, 5000, true),
                        new(Id(GuidUse.SkillDefinitionDatabaseProgramming), 2000, 4000, true),
                        new(Id(GuidUse.SkillDefinitionUserExperience), 1500, 2500),
                        new(Id(GuidUse.SkillDefinitionInterfaceDesign), 2000, 3000),
                        new(Id(GuidUse.SkillDefinitionCreativity), 500, 1000),
                        new(Id(GuidUse.SkillDefinitionAutomatedTesting), 1500, 2000),
                        new(Id(GuidUse.SkillDefinitionTestPlanning), 1000, 1500),
                        new(Id(GuidUse.SkillDefinitionAnalysis), 1500, 2500)
                    ]
                },
                new ProjectDefinition
                {
                    Id = Id(GuidUse.ProjectDefinitionCloudDataLake),
                    Name = "Cloud Data Lake",
                    Description = "Set up a cloud data lake to store and analyze large volumes of unstructured data from multiple sources.",
                    Value = new Range<int>(20000, 30000),
                    ProjectLength = new Range<int>(90, 120),
                    Skills = [
                        new(Id(GuidUse.SkillDefinitionServerProgramming), 5000, 8000, true),
                        new(Id(GuidUse.SkillDefinitionDatabaseProgramming), 3000, 5000, true),
                        new(Id(GuidUse.SkillDefinitionInterfaceProgramming), 1000, 2000),
                        new(Id(GuidUse.SkillDefinitionAutomatedTesting), 1000, 2000),
                        new(Id(GuidUse.SkillDefinitionAnalysis), 1500, 2500),
                        new(Id(GuidUse.SkillDefinitionTestPlanning), 1500, 2500),
                        new(Id(GuidUse.SkillDefinitionLeadership), 2000, 3000),
                        new(Id(GuidUse.SkillDefinitionMarketing), 500, 1000)
                    ]
                },
                new ProjectDefinition
                {
                    Id = Id(GuidUse.ProjectDefinitionEnterpriseResourcePlanning),
                    Name = "Enterprise Resource Planning",
                    Description = "Implement a full-scale ERP system for the company, covering finance, inventory, human resources, and supply chain.",
                    Value = new Range<int>(30000, 40000),
                    ProjectLength = new Range<int>(120, 150),
                    Skills = [
                        new(Id(GuidUse.SkillDefinitionServerProgramming), 5000, 8000, true),
                        new(Id(GuidUse.SkillDefinitionDatabaseProgramming), 4000, 6000, true),
                        new(Id(GuidUse.SkillDefinitionTestPlanning), 2000, 3000),
                        new(Id(GuidUse.SkillDefinitionAutomatedTesting), 1000, 1500),
                        new(Id(GuidUse.SkillDefinitionLeadership), 2000, 3000),
                        new(Id(GuidUse.SkillDefinitionAnalysis), 1500, 2000),
                        new(Id(GuidUse.SkillDefinitionInterfaceProgramming), 1500, 2500),
                        new(Id(GuidUse.SkillDefinitionCreativity), 500, 1000),
                        new(Id(GuidUse.SkillDefinitionPricing), 500, 1000)
                    ]
                },
                new ProjectDefinition
                {
                    Id = Id(GuidUse.ProjectDefinitionBlockchainIntegration),
                    Name = "Blockchain Integration",
                    Description = "Integrate blockchain technology into the existing product to allow for secure, transparent transactions.",
                    Value = new Range<int>(15000, 22000),
                    ProjectLength = new Range<int>(60, 90),
                    Skills = [
                        new(Id(GuidUse.SkillDefinitionServerProgramming), 3000, 5000, true),
                        new(Id(GuidUse.SkillDefinitionDatabaseProgramming), 2000, 3000, true),
                        new(Id(GuidUse.SkillDefinitionInterfaceProgramming), 1000, 1500),
                        new(Id(GuidUse.SkillDefinitionAutomatedTesting), 1000, 1500),
                        new(Id(GuidUse.SkillDefinitionCreativity), 500, 1000),
                        new(Id(GuidUse.SkillDefinitionLeadership), 1000, 2000),
                        new(Id(GuidUse.SkillDefinitionMarketing), 1000, 2000)
                    ]
                },
                new ProjectDefinition
                {
                    Id = Id(GuidUse.ProjectDefinitionSmartHomeAutomation),
                    Name = "Smart Home Automation",
                    Description = "Design and build a system for managing and automating smart home devices.",
                    Value = new Range<int>(18000, 24000),
                    ProjectLength = new Range<int>(75, 90),
                    Skills = [
                        new(Id(GuidUse.SkillDefinitionServerProgramming), 2000, 3000, true),
                        new(Id(GuidUse.SkillDefinitionInterfaceProgramming), 1000, 2000, true),
                        new(Id(GuidUse.SkillDefinitionUserExperience), 1000, 2000),
                        new(Id(GuidUse.SkillDefinitionTestPlanning), 500, 1000),
                        new(Id(GuidUse.SkillDefinitionCreativity), 500, 1000),
                        new(Id(GuidUse.SkillDefinitionDatabaseProgramming), 1000, 1500),
                        new(Id(GuidUse.SkillDefinitionAutomatedTesting), 500, 1000),
                        new(Id(GuidUse.SkillDefinitionAnalysis), 1000, 1500)
                    ]
                },
                new ProjectDefinition
                {
                    Id = Id(GuidUse.ProjectDefinitionVirtualRealityTrainingPlatform),
                    Name = "Virtual Reality Training Platform",
                    Description = "Create a VR platform for training employees in complex tasks, integrating real-time performance feedback and analytics.",
                    Value = new Range<int>(30000, 40000),
                    ProjectLength = new Range<int>(120, 150),
                    Skills = [
                        new(Id(GuidUse.SkillDefinitionServerProgramming), 5000, 8000, true),
                        new(Id(GuidUse.SkillDefinitionDatabaseProgramming), 3000, 4000),
                        new(Id(GuidUse.SkillDefinitionInterfaceDesign), 2000, 3000),
                        new(Id(GuidUse.SkillDefinitionUserExperience), 2000, 3000),
                        new(Id(GuidUse.SkillDefinitionCreativity), 1000, 1500),
                        new(Id(GuidUse.SkillDefinitionTestPlanning), 1000, 2000),
                        new(Id(GuidUse.SkillDefinitionAutomatedTesting), 1000, 1500),
                        new(Id(GuidUse.SkillDefinitionLeadership), 2000, 3000),
                        new(Id(GuidUse.SkillDefinitionAnalysis), 1500, 2500)
                    ]
                },
                new ProjectDefinition
                {
                    Id = Id(GuidUse.ProjectDefinitionAIChatbotForCustomerSupport),
                    Name = "AI Chatbot for Customer Support",
                    Description = "Build a chatbot powered by AI to handle customer inquiries, improving response times and accuracy.",
                    Value = new Range<int>(15000, 20000),
                    ProjectLength = new Range<int>(60, 90),
                    Skills = [
                        new(Id(GuidUse.SkillDefinitionServerProgramming), 3000, 5000, true),
                        new(Id(GuidUse.SkillDefinitionInterfaceProgramming), 1000, 1500),
                        new(Id(GuidUse.SkillDefinitionUserExperience), 1000, 2000),
                        new(Id(GuidUse.SkillDefinitionAutomatedTesting), 1000, 1500),
                        new(Id(GuidUse.SkillDefinitionCreativity), 500, 1000),
                        new(Id(GuidUse.SkillDefinitionTestPlanning), 500, 1000),
                        new(Id(GuidUse.SkillDefinitionAnalysis), 1000, 2000)
                    ]
                },
                new ProjectDefinition
                {
                    Id = Id(GuidUse.ProjectDefinitionDataWarehouseUpgrade),
                    Name = "Data Warehouse Upgrade",
                    Description = "Upgrade the company's data warehouse to improve data retrieval speed and add new features for analytics.",
                    Value = new Range<int>(22000, 30000),
                    ProjectLength = new Range<int>(90, 120),
                    Skills = [
                        new(Id(GuidUse.SkillDefinitionServerProgramming), 3000, 5000, true),
                        new(Id(GuidUse.SkillDefinitionDatabaseProgramming), 3000, 5000, true),
                        new(Id(GuidUse.SkillDefinitionInterfaceProgramming), 1000, 1500),
                        new(Id(GuidUse.SkillDefinitionTestPlanning), 1000, 1500),
                        new(Id(GuidUse.SkillDefinitionLeadership), 1000, 2000),
                        new(Id(GuidUse.SkillDefinitionMarketing), 500, 1000),
                        new(Id(GuidUse.SkillDefinitionAnalysis), 1500, 2500),
                        new(Id(GuidUse.SkillDefinitionCreativity), 500, 1000),
                        new(Id(GuidUse.SkillDefinitionPricing), 1000, 1500)
                    ]
                },
                new ProjectDefinition
                {
                    Id = Id(GuidUse.ProjectDefinitionNextGenECommercePlatform),
                    Name = "Next-Gen E-Commerce Platform",
                    Description = "Build a new e-commerce platform that includes advanced AI-based product recommendations and personalized shopping experiences.",
                    Value = new Range<int>(35000, 50000),
                    ProjectLength = new Range<int>(150, 180),
                    Skills = [
                        new(Id(GuidUse.SkillDefinitionServerProgramming), 5000, 8000, true),
                        new(Id(GuidUse.SkillDefinitionDatabaseProgramming), 4000, 6000, true),
                        new(Id(GuidUse.SkillDefinitionInterfaceProgramming), 2000, 3000),
                        new(Id(GuidUse.SkillDefinitionAutomatedTesting), 1000, 1500),
                        new(Id(GuidUse.SkillDefinitionTestPlanning), 1000, 1500),
                        new(Id(GuidUse.SkillDefinitionAnalysis), 1500, 2500),
                        new(Id(GuidUse.SkillDefinitionCreativity), 500, 1000),
                        new(Id(GuidUse.SkillDefinitionMarketing), 1000, 1500),
                        new(Id(GuidUse.SkillDefinitionPricing), 1000, 1500),
                        new(Id(GuidUse.SkillDefinitionLeadership), 2000, 3000)
                    ]
                }
            ];
        }

        private IEnumerable<SkillDefinition> GetSkillDefinitions()
        {
            return [
                new() { Id = Id(GuidUse.SkillDefinitionAdministration), Name = "Administration", Value = new(0, 255) },
                new() { Id = Id(GuidUse.SkillDefinitionAccounting), Name = "Accounting", Value = new(0, 255) },
                new() { Id = Id(GuidUse.SkillDefinitionCommunication), Name = "Communication", Value = new(0, 255) },
                new() { Id = Id(GuidUse.SkillDefinitionResearch), Name = "Research", Value = new(0, 255) },
                new() { Id = Id(GuidUse.SkillDefinitionWriting), Name = "Writing", Value = new(0, 255) },
                new() { Id = Id(GuidUse.SkillDefinitionPlanning), Name = "Planning", Value = new(0, 255) },
                new() { Id = Id(GuidUse.SkillDefinitionAnalysis), Name = "Analysis", Value = new(0, 255) },
                new() { Id = Id(GuidUse.SkillDefinitionLeadership), Name = "Leadership", Value = new(0, 255) },
                new() { Id = Id(GuidUse.SkillDefinitionInterfaceDesign), Name = "Interface Design", Value = new(0, 255) },
                new() { Id = Id(GuidUse.SkillDefinitionUserExperience), Name = "User Experience", Value = new(0, 255) },
                new() { Id = Id(GuidUse.SkillDefinitionCreativity), Name = "Creativity", Value = new(0, 255) },
                new() { Id = Id(GuidUse.SkillDefinitionServerProgramming), Name = "Server Programming", Value = new(0, 255) },
                new() { Id = Id(GuidUse.SkillDefinitionInterfaceProgramming), Name = "Interface Programming", Value = new(0, 255) },
                new() { Id = Id(GuidUse.SkillDefinitionDatabaseProgramming), Name = "Database Programming", Value = new(0, 255) },
                new() { Id = Id(GuidUse.SkillDefinitionManualTesting), Name = "Manual Testing", Value = new(0, 255) },
                new() { Id = Id(GuidUse.SkillDefinitionAutomatedTesting), Name = "Automated Testing", Value = new(0, 255) },
                new() { Id = Id(GuidUse.SkillDefinitionTestPlanning), Name = "Test Planning", Value = new(0, 255) },
                new() { Id = Id(GuidUse.SkillDefinitionMarketing), Name = "Marketing", Value = new(0, 255) },
                new() { Id = Id(GuidUse.SkillDefinitionPricing), Name = "Pricing", Value = new(0, 255) },
                new() { Id = Id(GuidUse.SkillDefinitionDocumentation), Name = "Documentation", Value = new(0, 255) }
            ];
        }

        public Guid Id(GuidUse g) => new(GUID_STRINGS[(int)g]);

        private static readonly string[] GUID_STRINGS = ["f9f11618-a8bc-4585-b37d-7c7463997003", "397bc13d-9b0b-4521-b026-4ef5e1f3fe12", "35f49334-4771-4ad4-b294-87091472872c", "9d1b22ea-7f1e-4413-a0b1-011a75aed05c", "30650872-4744-4fc6-b4c0-63b68efee19f", "ff84bc5d-8e4f-4561-b252-275c34ad1101", "ec02c2a0-0322-456d-a967-afc3ada4c02e", "fab7a165-9c6f-466e-87fd-dfd0ee1e7587", "e6309a1a-2428-4b1f-ba1a-095bffb06f3f", "a31efdc0-1e73-4ea1-bc6e-1bf32395fb25", "8070825f-82db-4d19-b0ee-7ed880ba8842", "99571ceb-60cc-4d8e-a73f-8db673a09a99", "22619791-ab53-43ec-8724-8dd4e9b2d10b", "7b00a7e5-f150-4fbf-892a-c448106cc00c", "12a8b692-6377-4071-8a69-213f7e03190c", "5b44fad3-0034-44af-b9f9-7d9e0f0a4914", "4c24b409-117c-49ae-a2d6-f21ca243dcbc", "79429745-a127-4a40-9993-ce1af8676f0e", "15a97f69-8d3d-48de-a149-eedc952f125b", "bed11383-4d22-47c1-b38c-5b19c8c9bac9", "1d27cdae-dd8a-41d7-9db8-9a4f1f052776", "f338a287-499a-4e77-8e48-173df1933f51", "d75c9053-788b-45df-bac0-c6eb55b82b4a", "a539207e-9a7a-44e5-8a0e-eb4ab57ff0d8", "cb88b051-0c8f-4d61-9961-8bb72f76001d", "4e53d66e-f07d-4679-b3d5-542d10d41178", "17eaf03a-79fc-4d7c-881a-0a069ee6b459", "7d04e2ce-874e-468b-9937-96087841d7a6", "3b4a4655-c538-4cdf-881c-84a75f16d5b9", "c8be6b1a-8921-46a6-9483-6d4d67e271e1", "d74203e6-7375-49f8-98eb-c4ddb35f0d5c", "347d4518-1a7a-4a91-9ca8-c597ff62763a", "3260fcea-7ab4-4ff2-af5e-9c47042b0f23", "22762228-75e3-472e-8a22-2a48e35edbfe", "4886a0bb-8289-4feb-8c3c-7bbd74f4afb8", "cf799725-b659-4751-bb9f-ad50152cb9fb", "dd5c33f5-aeb7-4431-a2a9-72de8284513f", "c5a0a551-fab8-4d90-880c-bd1336e6724e", "9971e2c7-db1d-4b80-9954-e7182ba535db", "1f77e49d-a8dd-453a-9741-0eeaba0f971a", "a52ab1a2-2f0d-43ab-8ef4-8425d813954d", "ba972b6a-c0ec-4661-97a3-f649c498b7be", "4cd85239-1776-410c-9530-848884f2f10a", "a31caa68-93fa-4ae3-aa22-ce7808f4167c", "f76923d4-22b2-4b15-9ddd-9c570e3c548f", "04590451-1c80-4f34-aab9-ccc11d0aabc1", "ccf4cd41-a155-46ea-ad2d-888e38faafa8", "47c20dc7-6383-4ffa-81b8-cce2bb1ebe5e", "36abdd12-371a-4ec9-8157-fe83d037056c", "5d28c791-fe85-49d1-9187-45dd98ae9151", "7686afad-2154-4522-998f-6c30dc29f826", "3e58d679-d156-4385-84d3-5b070ed3ed3a", "e15aad8c-7b66-4cad-a9f1-beadeaf88772", "daf4751e-f792-48e2-bcfa-5c8e88031c4c", "848d9594-5bf8-42e5-818d-87cf14b0512b", "3cd99d6f-851e-4827-b860-6aea8dba7c2a", "edfd4ac5-22f2-443d-b4e0-da2d3eed4e03", "8d58ccff-c32d-49b4-bd98-56778fd6ac55", "1037c9eb-5241-43a8-a68e-7520e0f51aaa", "681b8902-8cda-45f0-82c1-504d6addeeba", "373e32bd-f2dd-48a0-bf4c-cd3d7a91cb18", "840851fc-c159-4f46-8ef1-7f7e8577ac34", "12af8ad6-f57e-49fd-b58a-5492b5be969e", "3ddeb5a2-b9bb-46f4-a043-5c979652fdda", "33d37e53-0eff-4eb7-b3da-b3fb46ac3076", "9128b5a4-c3d3-491c-993e-ac1e2c4b40b0", "f5985e20-61ae-429e-8b64-782706d90ccf", "98e3bca5-7a6d-403a-aa97-de621861cf74", "843b2299-8737-423e-944d-5e656c44e5d0", "0ea48b01-7c70-42e8-90cb-b7a10e075d48", "2f6da3a2-6df0-49ea-b77a-0a1f0b4d6ea1", "351dfc96-2f87-4bb0-abdb-1687d78edbcc", "b075da0f-327f-4023-a0b4-fb59ff01d303", "1e79304e-3166-4457-b56d-866af1239475", "23a45b94-304c-4bc4-8798-51d46971db83", "7fb9571a-befe-475b-bfcd-dcc7c2f766c9", "08cac8d8-edec-46e6-b976-4343aee1cb3c", "3f5ba714-4a6a-4165-bd83-935e810e4fbe", "b8e29750-9ebd-41f6-89e5-eb4203256155", "d1fbfdd7-606f-4c30-89b8-984999a49aaf", "ec68e48b-3a7b-4fed-801b-a1e32a5754ea", "4140ec22-9b5e-4409-ae21-11b7cd74440a", "f3cd4866-f205-453f-91f4-308513904217", "300b1743-5f14-4f7d-a044-8b02bfc739c9", "8fdc1e42-30da-4947-b843-dbb320a45d42", "ca79822f-d14b-42e6-a08d-13e95c3d552c", "b091e241-a5e4-4596-ba31-1398f5638390", "37fea816-076e-457c-b916-a8adba1d3b78", "164bfb2e-c4bd-437c-8659-14690430d890", "9e4e0eb2-829c-48a0-9294-0a1ba0b6a61a", "d130a32e-16ba-463e-9eac-a36f07b57f95", "31e8862d-7bfd-467e-83d9-5efdf478231a", "d18d49bc-6fb5-4cd6-82c6-43672cb3d69b", "2f5bf19d-31cb-4de3-8531-9fd718e333f0", "ceb6c9d1-a7a8-4ce7-ab63-e24ed9d83fdc", "793db472-7e88-45c9-8b30-04824d5dc498", "c1c03e97-7c82-4c8d-9494-7a2c3bfc0350", "324ce3e7-cb20-4e27-88b4-73caf7922475", "1b022e72-3a72-4c23-979b-595012e9df51", "c3ee73a2-0c12-4865-83d9-4cb6c8f1a4bf"];
    }
}
