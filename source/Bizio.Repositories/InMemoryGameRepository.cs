﻿using Bizio.Core.Data.Game;
using Bizio.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Bizio.Repositories
{
    internal class InMemoryGameRepository : IGameRepository
    {
        public Task<IEnumerable<GameMetadata>> LoadAsync(CancellationToken cancellationToken = default)
        {
            return Task.FromResult(_games.Cast<GameMetadata>());
        }

        public Task<Game> LoadAsync(Guid id, CancellationToken cancellationToken = default)
        {
            return Task.FromResult(_games.FirstOrDefault(g => g.Id == id));
        }

        public Task SaveAsync(Game game, CancellationToken cancellationToken = default)
        {
            var match = _games.FirstOrDefault(g => g.Id == game.Id);

            if (match != null)
            {
                _games.Remove(match);
            }

            _games.Add(game);

            return Task.CompletedTask;
        }

        public Task DeleteAsync(Guid id, CancellationToken cancellationToken = default)
        {
            var match = _games.FirstOrDefault(g => g.Id == id);

            if (match == null)
            {
                return Task.CompletedTask;
            }

            _games.Remove(match);

            return Task.CompletedTask;
        }

        private readonly List<Game> _games = [];
    }
}