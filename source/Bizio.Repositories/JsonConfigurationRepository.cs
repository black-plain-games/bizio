﻿using Bizio.Core.Data.Configuration;
using Bizio.Core.Repositories;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Bizio.Repositories
{
    internal class JsonConfigurationRepository : JsonRepositoryBase, IConfigurationRepository
    {
        public async Task<IDictionary<ConfigurationKey, string>> LoadAsync(CancellationToken cancellationToken = default)
        {
            return await LoadAsync(false, cancellationToken);
        }

        public async Task<IDictionary<ConfigurationKey, string>> LoadAsync(bool forceReload, CancellationToken cancellationToken = default)
        {
            if (_configuration != null && !forceReload)
            {
                return _configuration;
            }

            return _configuration = await ReadFileAsync<IDictionary<ConfigurationKey, string>>(DataFilePaths.ConfigurationFilePath, cancellationToken);
        }

        private IDictionary<ConfigurationKey, string> _configuration;
    }
}
