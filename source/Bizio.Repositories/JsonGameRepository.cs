﻿using Bizio.Core;
using Bizio.Core.Data.Game;
using Bizio.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Bizio.Repositories
{
    internal class JsonGameRepository : JsonRepositoryBase, IGameRepository
    {
        public async Task<IEnumerable<GameMetadata>> LoadAsync(CancellationToken cancellationToken = default)
        {
            return await ReadFileAsync<IEnumerable<GameMetadata>>(DataFilePaths.GamesFilePath, cancellationToken);
        }

        public async Task<Game> LoadAsync(Guid id, CancellationToken cancellationToken = default)
        {
            var games = await ReadFileAsync<IEnumerable<Game>>(DataFilePaths.GamesFilePath, cancellationToken);

            return games.Find(id);
        }

        public async Task SaveAsync(Game game, CancellationToken cancellationToken = default)
        {
            var games = await LoadFullAsync(cancellationToken);

            var match = games.FirstOrDefault(g => g.Id == game.Id);

            var updated = games.ToList();

            if (match != null)
            {
                updated.Remove(match);
            }


            updated.Add(game);

            await SaveFileAsync(DataFilePaths.GamesFilePath, updated, cancellationToken);
        }

        public async Task DeleteAsync(Guid id, CancellationToken cancellationToken = default)
        {
            var games = await LoadFullAsync(cancellationToken);

            var match = games.FirstOrDefault(g => g.Id == id);

            if (match == null)
            {
                return;
            }

            var updated = games.ToList();

            updated.Remove(match);

            await SaveFileAsync(DataFilePaths.GamesFilePath, updated, cancellationToken);
        }

        protected static async Task<IEnumerable<Game>> LoadFullAsync(CancellationToken cancellationToken = default)
        {
            return await ReadFileAsync<IEnumerable<Game>>(DataFilePaths.GamesFilePath, cancellationToken);
        }
    }
}