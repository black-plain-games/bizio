﻿using Bizio.Core.Data;
using Bizio.Core.Repositories;
using System.Threading;
using System.Threading.Tasks;

namespace Bizio.Repositories
{
    internal class JsonLanguageRepository : JsonRepositoryBase, ILanguageRepository
    {
        public async Task<LanguageData> LoadAsync(bool forceReload, CancellationToken cancellationToken = default)
        {
            return await ReadFileAsync<LanguageData>(DataFilePaths.LanguagesFilePath, cancellationToken);
        }
    }
}