﻿using Newtonsoft.Json;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bizio.Repositories
{
    internal abstract class JsonRepositoryBase
    {
        protected static async Task<T> ReadFileAsync<T>(string path, CancellationToken cancellationToken = default)
        {
            if (!File.Exists(path))
            {
                return default;
            }

            string json;

            using (var reader = new StreamReader(path))
            {
                json = await reader.ReadToEndAsync(cancellationToken);
            }

            return JsonConvert.DeserializeObject<T>(json);
        }

        protected static T ReadFile<T>(string path)
        {
            if (!File.Exists(path))
            {
                return default;
            }

            string json;

            using (var reader = new StreamReader(path))
            {
                json = reader.ReadToEnd();
            }

            return JsonConvert.DeserializeObject<T>(json);
        }

        protected static async Task<bool> SaveFileAsync<T>(string path, T data, CancellationToken cancellationToken = default)
        {
            if (!File.Exists(path))
            {
                return false;
            }

            var json = JsonConvert.SerializeObject(data, Formatting.Indented);
            var jsonBuilder = new StringBuilder(json);

            using (var writer = new StreamWriter(path))
            {
                await writer.WriteAsync(jsonBuilder, cancellationToken);
            }

            return true;
        }
    }
}