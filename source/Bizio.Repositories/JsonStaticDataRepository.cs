﻿using Bizio.Core.Data;
using Bizio.Core.Repositories;
using System.Threading;
using System.Threading.Tasks;

namespace Bizio.Repositories
{
    internal class JsonStaticDataRepository : JsonRepositoryBase, IStaticDataRepository
    {
        public async Task<StaticData> LoadAsync(bool forceReload, CancellationToken cancellationToken = default)
        {
            if (_bizioData != null && !forceReload)
            {
                return _bizioData;
            }

            return _bizioData = await ReadFileAsync<StaticData>(DataFilePaths.StaticDataFilePath, cancellationToken);
        }

        private StaticData _bizioData;
    }
}