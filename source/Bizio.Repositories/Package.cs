﻿using Bizio.Core.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace Bizio.Repositories
{
    public static class Package
    {
        public static IServiceCollection AddBizioRepositories(this IServiceCollection services)
        {
            var output = services
                .AddTransient<IStaticDataRepository, JsonStaticDataRepository>()
                .AddTransient<ILanguageRepository, JsonLanguageRepository>()
                .AddTransient<IConfigurationRepository, JsonConfigurationRepository>();

            output.AddTransient<IGameRepository, JsonGameRepository>();

#if DEBUG
            //output.AddTransient<IDefaultDataGenerator, DefaultDataGenerator>();
            //output.AddTransient<IGameRepository, InMemoryGameRepository>();
#else
            output.AddTransient<IGameRepository, JsonGameRepository>();
#endif

            return output;
        }
    }
}