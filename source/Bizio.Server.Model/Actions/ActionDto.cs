﻿using Bizio.Server.Model.Core;
using System.Collections.Generic;

namespace Bizio.Server.Model.Actions
{
    public class ActionDto
    {
        public byte Id { get; set; }

        public string Name { get; set; }

        public short MaxCount { get; set; }

        public IEnumerable<IdValuePairDto<byte, decimal>> Requirements { get; set; }
    }
}