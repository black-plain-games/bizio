﻿using System.Collections.Generic;

namespace Bizio.Server.Model.Actions
{
    public class StaticActionsDataDto
    {
        public IEnumerable<ActionDto> Actions { get; set; }
    }
}