﻿using System.Collections.Generic;

namespace Bizio.Server.Model.Companies
{
    public class CompaniesDataDto
    {
        public IEnumerable<CompanyDto> Companies { get; set; }
    }
}