﻿using Bizio.Server.Model.Core;
using System.Collections.Generic;

namespace Bizio.Server.Model.Companies
{
    public class CompanyActionDto
    {
        public byte ActionId { get; set; }

        public short Count { get; set; }

        public bool IsActive { get; set; }

        public IEnumerable<IdValuePairDto<byte, decimal>> Accumulations { get; set; }
    }
}