﻿using Bizio.Server.Model.Projects;
using System.Collections.Generic;

namespace Bizio.Server.Model.Companies
{
    public class CompanyDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public decimal Money { get; set; }

        public byte InitialAccuracy { get; set; }

        public int? UserId { get; set; }

        public int? RivalId { get; set; }

        public byte StatusId { get; set; }

        public IEnumerable<CompanyActionDto> Actions { get; set; }

        public IEnumerable<AllocationDto> Allocations { get; set; }

        public IEnumerable<EmployeeDto> Employees { get; set; }

        public IEnumerable<CompanyMessageDto> Messages { get; set; }

        public IEnumerable<CompanyPerkDto> Perks { get; set; }

        public IEnumerable<ProjectDto> Projects { get; set; }

        public IEnumerable<ProspectDto> Prospects { get; set; }

        public ReputationDto Reputation { get; set; }

        public IEnumerable<TransactionDto> Transactions { get; set; }
    }
}