﻿using System;

namespace Bizio.Server.Model.Companies
{
    public class CompanyMessageDto
    {
        public int Id { get; set; }

        public DateTime DateCreated { get; set; }

        public byte Status { get; set; }

        public string Source { get; set; }

        public string Subject { get; set; }

        public string Message { get; set; }
    }
}