﻿using System;

namespace Bizio.Server.Model.Companies
{
    public class CompanyPerkDto
    {
        public int Id { get; set; }

        public int PerkId { get; set; }

        public DateTime? NextPaymentDate { get; set; }
    }
}