﻿namespace Bizio.Server.Model.Companies
{
    public enum CompanyStatusDto
    {
        InBusiness = 1,
        Bankrupt = 2
    }
}