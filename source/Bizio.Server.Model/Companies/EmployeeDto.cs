﻿namespace Bizio.Server.Model.Companies
{
    public class EmployeeDto
    {
        public decimal Happiness { get; set; }

        public bool IsFounder { get; set; }

        public int PersonId { get; set; }

        public decimal Salary { get; set; }
    }
}