﻿namespace Bizio.Server.Model.Companies
{
    public class IndustryDto
    {
        public byte Id { get; set; }

        public string Name { get; set; }
    }
}