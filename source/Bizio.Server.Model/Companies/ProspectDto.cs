﻿using Bizio.Server.Model.Core;
using System.Collections.Generic;

namespace Bizio.Server.Model.Companies
{
    public class ProspectDto
    {
        public byte Accuracy { get; set; }

        public int PersonId { get; set; }

        public RangeDto<decimal> Salary { get; set; }

        public IEnumerable<IdValuePairDto<byte, RangeDto<decimal>>> Skills { get; set; }
    }
}