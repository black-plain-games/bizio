﻿namespace Bizio.Server.Model.Companies
{
    public class ReputationDto
    {
        public decimal EarnedStars { get; set; }

        public int PossibleStars { get; set; }

        public decimal Value { get; set; }
    }
}