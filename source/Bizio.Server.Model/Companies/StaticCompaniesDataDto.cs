﻿using Bizio.Server.Model.Core;
using System.Collections.Generic;

namespace Bizio.Server.Model.Companies
{
    public class StaticCompaniesDataDto
    {
        public IEnumerable<IndustryDto> Industries { get; set; }

        public IEnumerable<IdValuePairDto<byte, string>> CompanyMessageStatuses { get; set; }

        public IEnumerable<IdValuePairDto<byte, string>> TransactionTypes { get; set; }
    }
}