﻿using System;

namespace Bizio.Server.Model.Companies
{
    public class TransactionDto
    {
        public int Id { get; set; }

        public decimal Amount { get; set; }

        public DateTime Date { get; set; }

        public string Description { get; set; }

        public decimal EndingBalance { get; set; }

        public TransactionTypeDto TransactionType { get; set; }
    }
}