﻿namespace Bizio.Server.Model.Companies
{
    public enum TransactionTypeDto : byte
    {
        None,
        Payroll = 1,
        HireEmployee,
        FireEmployee,
        PurchasePerk,
        SellPerk,
        Perk,
        Project
    }
}