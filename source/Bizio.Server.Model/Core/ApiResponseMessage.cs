﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Server.Model.Core
{
    public class ApiResponseMessage
    {
        public IEnumerable<string> ValidationErrors { get; set; }

        public Exception Exception { get; set; }

        public ApiResponseMessage() { }

        public static readonly ApiResponseMessage Empty = new ApiResponseMessage();

        public ApiResponseMessage(IEnumerable<string> validationErrors, Exception exception)
        {
            ValidationErrors = validationErrors ?? Enumerable.Empty<string>();

            Exception = exception;
        }
    }

    public class ApiResponseMessage<T> : ApiResponseMessage
    {
        public T Data { get; set; }

        public ApiResponseMessage()
            : base()
        {
        }

        public ApiResponseMessage(T data, IEnumerable<string> validationErrors, Exception exception)
            : base(validationErrors, exception)
        {
            Data = data;
        }
    }
}