﻿namespace Bizio.Server.Model.Core
{
    public class IdValuePairDto<TId, TValue>
    {
        public TId Id { get; set; }

        public TValue Value { get; set; }

        public IdValuePairDto() { }

        public IdValuePairDto(TId id, TValue value)
        {
            Id = id;

            Value = value;
        }
    }
}