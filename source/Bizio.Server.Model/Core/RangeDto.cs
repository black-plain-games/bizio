﻿namespace Bizio.Server.Model.Core
{
    public class RangeDto<T>
    {
        public T Minimum { get; set; }

        public T Maximum { get; set; }
    }
}