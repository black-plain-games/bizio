﻿using Bizio.Server.Model.Actions;
using Bizio.Server.Model.Companies;
using Bizio.Server.Model.Game;
using Bizio.Server.Model.People;
using Bizio.Server.Model.Perks;
using Bizio.Server.Model.Projects;

namespace Bizio.Server.Model.Core
{
    public class StaticDataDto
    {
        public StaticActionsDataDto ActionsData { get; set; }

        public StaticCompaniesDataDto CompaniesData { get; set; }

        public StaticPeopleDataDto PeopleData { get; set; }

        public StaticPerksDataDto PerksData { get; set; }

        public StaticProjectsDataDto ProjectsData { get; set; }

        public StaticGameDataDto GameData { get; set; }
    }
}