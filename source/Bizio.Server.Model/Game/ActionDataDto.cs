﻿using System.Collections.Generic;

namespace Bizio.Server.Model.Game
{
    public class ActionDataDto
    {
        public byte ActionTypeId { get; set; }

        public IDictionary<string, object> Data { get; set; }
    }
}