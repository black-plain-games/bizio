﻿namespace Bizio.Server.Model.Game
{
    public class DifficultyLevelDto
    {
        public byte Id { get; set; }

        public string Name { get; set; }

        public byte MaximumOptionalSkillCount { get; set; }

        public short MaximumTotalSkillPoints { get; set; }

        public decimal InitialFunds { get; set; }
    }
}