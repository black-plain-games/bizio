﻿using Bizio.Server.Model.Companies;
using Bizio.Server.Model.GameClock;
using Bizio.Server.Model.People;
using Bizio.Server.Model.Projects;

namespace Bizio.Server.Model.Game
{
    public class GameDataDto
    {
        public int GameId { get; set; }

        public byte IndustryId { get; set; }

        public byte StatusId { get; set; }

        public GameClockDataDto GameClockData { get; set; }

        public ProjectsDataDto ProjectsData { get; set; }

        public PeopleDataDto PeopleData { get; set; }

        public CompaniesDataDto CompaniesData { get; set; }
    }
}