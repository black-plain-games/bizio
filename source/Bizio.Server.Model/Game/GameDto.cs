﻿using System;

namespace Bizio.Server.Model.Game
{
    public class GameDto
    {
        public int Id { get; set; }

        public string FounderFirstName { get; set; }

        public string FounderLastName { get; set; }

        public string CompanyName { get; set; }

        public DateTimeOffset CreatedDate { get; set; }

        public short IndustryId { get; set; }

        public byte WeeksPerTurn { get; set; }

        public short CurrentTurn { get; set; }

        public byte StatusId { get; set; }
    }
}