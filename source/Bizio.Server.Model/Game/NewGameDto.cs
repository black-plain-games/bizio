﻿using Bizio.Server.Model.People;
using System;

namespace Bizio.Server.Model.Game
{
    public sealed class NewGameDto
    {
        public DateTime StartDate { get; set; }

        public byte WeeksPerTurn { get; set; }

        public CreatePersonDto Founder { get; set; }

        public string CompanyName { get; set; }

        public byte NumberOfCompanies { get; set; }

        public byte IndustryId { get; set; }

        public decimal InitialFunds { get; set; }
    }
}