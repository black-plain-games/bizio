﻿using System.Collections.Generic;

namespace Bizio.Server.Model.Game
{
    public class ProcessTurnDto
    {
        public int UserId { get; set; }

        public IEnumerable<ActionDataDto> Actions { get; set; }
    }
}