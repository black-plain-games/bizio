﻿using System.Collections.Generic;

namespace Bizio.Server.Model.Game
{
    public class StaticGameDataDto
    {
        public IEnumerable<DifficultyLevelDto> DifficultyLevels { get; set; }
    }
}