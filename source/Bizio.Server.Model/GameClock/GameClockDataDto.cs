﻿using System;

namespace Bizio.Server.Model.GameClock
{
    public class GameClockDataDto
    {
        public DateTime StartDate { get; set; }

        public DateTime CurrentDate { get; set; }

        public short CurrentTurn { get; set; }

        public byte WeeksPerTurn { get; set; }
    }
}