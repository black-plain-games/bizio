﻿using System;

namespace Bizio.Server.Model
{
    public class NotificationDto
    {
        public int Id { get; set; }

        public DateTimeOffset DateCreated { get; set; }

        public string Subject { get; set; }

        public string Message { get; set; }
    }
}