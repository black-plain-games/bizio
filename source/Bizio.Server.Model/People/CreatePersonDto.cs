﻿using Bizio.Server.Model.Core;
using System;
using System.Collections.Generic;

namespace Bizio.Server.Model.People
{
    public sealed class CreatePersonDto
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public byte Gender { get; set; }

        public DateTime Birthday { get; set; }

        public ushort PersonalityId { get; set; }

        public IEnumerable<IdValuePairDto<byte, decimal>> Skills { get; set; }
    }
}