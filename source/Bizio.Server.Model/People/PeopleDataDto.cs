﻿using System.Collections.Generic;

namespace Bizio.Server.Model.People
{
    public class PeopleDataDto
    {
        public IEnumerable<PersonDto> People { get; set; }
    }
}