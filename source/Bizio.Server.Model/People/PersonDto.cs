﻿using System;
using System.Collections.Generic;

namespace Bizio.Server.Model.People
{
    public class PersonDto
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public byte GenderId { get; set; }

        public DateTime Birthday { get; set; }

        public byte PersonalityId { get; set; }

        public DateTime RetirementDate { get; set; }

        public byte? ProfessionId { get; set; }

        public IEnumerable<SkillDto> Skills { get; set; }

        public IEnumerable<WorkHistoryDto> WorkHistory { get; set; }
    }
}