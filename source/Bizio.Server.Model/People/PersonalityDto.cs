﻿using Bizio.Server.Model.Core;
using System.Collections.Generic;

namespace Bizio.Server.Model.People
{
    public class PersonalityDto
    {
        public byte Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public IEnumerable<IdValuePairDto<byte, decimal>> Attributes { get; set; }
    }
}