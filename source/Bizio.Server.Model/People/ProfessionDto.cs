﻿using System.Collections.Generic;

namespace Bizio.Server.Model.People
{
    public class ProfessionDto
    {
        public byte Id { get; set; }

        public string Name { get; set; }

        public IEnumerable<ProfessionSkillDefinitionDto> SkillDefinitions { get; set; }
    }
}