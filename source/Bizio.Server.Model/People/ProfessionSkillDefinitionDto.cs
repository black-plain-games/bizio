﻿namespace Bizio.Server.Model.People
{
    public class ProfessionSkillDefinitionDto
    {
        public byte SkillDefinitionId { get; set; }

        public byte Weight { get; set; }
    }
}
