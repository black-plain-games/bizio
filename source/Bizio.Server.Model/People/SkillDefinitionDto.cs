﻿using Bizio.Server.Model.Core;

namespace Bizio.Server.Model.People
{
    public class SkillDefinitionDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public RangeDto<byte> Value { get; set; }
    }
}