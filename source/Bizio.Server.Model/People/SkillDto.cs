﻿namespace Bizio.Server.Model.People
{
    public class SkillDto
    {
        public byte ForgetRate { get; set; }

        public byte LearnRate { get; set; }

        public decimal Value { get; set; }

        public byte SkillDefinitionId { get; set; }
    }
}