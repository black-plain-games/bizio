﻿using Bizio.Server.Model.Core;
using System.Collections.Generic;

namespace Bizio.Server.Model.People
{
    public class StaticPeopleDataDto
    {
        public IEnumerable<SkillDefinitionDto> SkillDefinitions { get; set; }

        public IEnumerable<IdValuePairDto<byte, IEnumerable<SkillDefinitionDto>>> MandatorySkillDefinitions { get; set; }

        public IEnumerable<IdValuePairDto<byte, IEnumerable<ProfessionDto>>> Professions { get; set; }

        public IEnumerable<PersonalityDto> Personalities { get; set; }

        public IEnumerable<IdValuePairDto<byte, string>> Genders { get; set; }

        public IEnumerable<IdValuePairDto<byte, string>> PersonalityAttributeIds { get; set; }
    }
}