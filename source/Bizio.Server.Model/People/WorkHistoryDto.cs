﻿using System;

namespace Bizio.Server.Model.People
{
    public class WorkHistoryDto
    {
        public int CompanyId { get; set; }

        public DateTime? EndDate { get; set; }

        public decimal? EndingSalary { get; set; }

        public DateTime StartDate { get; set; }

        public decimal StartingSalary { get; set; }
    }
}