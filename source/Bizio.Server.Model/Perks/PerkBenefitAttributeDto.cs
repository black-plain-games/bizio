﻿namespace Bizio.Server.Model.Perks
{
    public enum PerkBenefitAttributeDto : byte
    {
        Money = 1,
        Happiness = 2
    }
}