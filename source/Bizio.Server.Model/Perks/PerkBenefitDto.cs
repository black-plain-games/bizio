﻿namespace Bizio.Server.Model.Perks
{
    public class PerkBenefitDto
    {
        public PerkTargetTypeDto TargetType { get; set; }

        public PerkBenefitAttributeDto Attribute { get; set; }

        public decimal Value { get; set; }

        public PerkValueTypeDto ValueType { get; set; }
    }
}