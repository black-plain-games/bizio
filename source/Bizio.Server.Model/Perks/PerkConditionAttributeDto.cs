﻿namespace Bizio.Server.Model.Perks
{
    public enum PerkConditionAttributeDto : byte
    {
        Money = 1,
        PerkCount = 2,
        ProjectCount = 3,
        ProspectCount = 4,
        EmployeeCount = 5,
        SkillCount = 6,
        Happiness = 7
    }
}