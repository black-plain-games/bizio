﻿namespace Bizio.Server.Model.Perks
{
    public enum PerkConditionComparisonTypeDto : byte
    {
        Equal = 1,
        NotEqual = 2,
        LessThan = 3,
        LessThanEqualTo = 4,
        GreaterThan = 5,
        GreaterThanEqualTo = 6
    }
}