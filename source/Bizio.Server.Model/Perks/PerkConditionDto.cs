﻿namespace Bizio.Server.Model.Perks
{
    public class PerkConditionDto
    {
        public PerkTargetTypeDto TargetType { get; set; }

        public PerkConditionAttributeDto Attribute { get; set; }

        public decimal Value { get; set; }

        public PerkConditionComparisonTypeDto ComparisonType { get; set; }
    }
}