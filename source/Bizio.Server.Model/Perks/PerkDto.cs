﻿using System.Collections.Generic;

namespace Bizio.Server.Model.Perks
{
    public class PerkDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public byte? MaxCount { get; set; }

        public decimal? InitialCost { get; set; }

        public decimal? RecurringCost { get; set; }

        public byte? RecurringCostInterval { get; set; }

        public IEnumerable<PerkBenefitDto> Benefits { get; set; }

        public IEnumerable<PerkConditionDto> Conditions { get; set; }
    }
}