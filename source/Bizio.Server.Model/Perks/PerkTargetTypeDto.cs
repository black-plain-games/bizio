﻿namespace Bizio.Server.Model.Perks
{
    public enum PerkTargetTypeDto : byte
    {
        Person = 1,
        Employee = 2,
        Company = 3
    }
}