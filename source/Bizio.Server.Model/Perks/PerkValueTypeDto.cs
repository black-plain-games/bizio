﻿namespace Bizio.Server.Model.Perks
{
    public enum PerkValueTypeDto : byte
    {
        Literal = 1,
        Percentage = 2
    }
}