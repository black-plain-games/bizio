﻿using System.Collections.Generic;

namespace Bizio.Server.Model.Perks
{
    public class StaticPerksDataDto
    {
        public IEnumerable<PerkDto> Perks { get; set; }
    }
}