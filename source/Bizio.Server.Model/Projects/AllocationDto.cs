﻿namespace Bizio.Server.Model.Projects
{
    public class AllocationDto
    {
        public byte Percent { get; set; }

        public int PersonId { get; set; }

        public int ProjectId { get; set; }

        public byte RoleId { get; set; }
    }
}