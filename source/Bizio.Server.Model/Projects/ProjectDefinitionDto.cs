﻿using Bizio.Server.Model.Core;
using System.Collections.Generic;

namespace Bizio.Server.Model.Projects
{
    public class ProjectDefinitionDto
    {
        public short Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public RangeDto<byte> ProjectLength { get; set; }

        public RangeDto<decimal> Value { get; set; }

        public IEnumerable<byte> Industries { get; set; }

        public IEnumerable<IdValuePairDto<byte, RangeDto<decimal>>> Skills { get; set; }
    }
}