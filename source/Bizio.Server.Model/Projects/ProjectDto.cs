﻿using System;
using System.Collections.Generic;

namespace Bizio.Server.Model.Projects
{
    public class ProjectDto
    {
        public int Id { get; set; }

        public DateTime Deadline { get; set; }

        public DateTime? ExtensionDeadline { get; set; }

        public short ProjectDefinitionId { get; set; }

        public byte ReputationRequired { get; set; }

        public decimal Value { get; set; }

        public ProjectStatusDto Status { get; set; }

        public IEnumerable<ProjectRequirementDto> Requirements { get; set; }
    }
}