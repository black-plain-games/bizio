﻿namespace Bizio.Server.Model.Projects
{
    public class ProjectRequirementDto
    {
        public byte SkillDefinitionId { get; set; }

        public decimal CurrentValue { get; set; }

        public decimal TargetValue { get; set; }

        public ProjectRequirementDto() { }

        public ProjectRequirementDto(byte skillDefinitionId, decimal currentValue, decimal targetValue)
        {
            SkillDefinitionId = skillDefinitionId;

            CurrentValue = currentValue;

            TargetValue = targetValue;
        }
    }
}