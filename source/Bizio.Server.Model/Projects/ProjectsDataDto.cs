﻿using System.Collections.Generic;

namespace Bizio.Server.Model.Projects
{
    public class ProjectsDataDto
    {
        public IEnumerable<ProjectDto> Projects { get; set; }
    }
}