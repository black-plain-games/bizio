﻿using Bizio.Server.Model.Core;
using System.Collections.Generic;

namespace Bizio.Server.Model.Projects
{
    public class StaticProjectsDataDto
    {
        public IEnumerable<ProjectDefinitionDto> ProjectDefinitions { get; set; }

        public IEnumerable<IdValuePairDto<byte, string>> ProjectRoles { get; set; }

        public IEnumerable<IdValuePairDto<byte, string>> ProjectStatuses { get; set; }

        public IEnumerable<IdValuePairDto<byte, string>> StarCategories { get; set; }
    }
}