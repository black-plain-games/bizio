﻿using System.Collections.Generic;

namespace Bizio.Server.Model.Text
{
    public class LanguageDataDto
    {
        public IEnumerable<LanguageDto> Languages { get; set; }

        public IDictionary<short, string> TextResourceKeys { get; set; }
    }
}
