﻿using System.Collections.Generic;

namespace Bizio.Server.Model.Text
{
    public class LanguageDto
    {
        public byte Id { get; set; }

        public string Name { get; set; }

        public string Iso2LetterCode { get; set; }

        public IDictionary<short, string> Text { get; set; }
    }
}
