﻿using Bizio.Core.Services;
using Bizio.Server.Model.Core;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace Bizio.Server.Controllers
{
    public class BaseApiController : Controller
    {
        protected int? UserId
        {
            get
            {
                return 7;

                if (ClaimsPrincipal.Current == null)
                {
                    return null;
                }

                var userIdClaim = ClaimsPrincipal.Current.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier);

                if (userIdClaim == null ||!int.TryParse(userIdClaim.Value, out var userId))
                {
                    return null;
                }

                return userId;
            }
        }

        protected BaseApiController(ILoggingService loggingService)
        {
            _loggingService = loggingService;
        }

        protected IActionResult Validate<T>(T argument, Func<T, IEnumerable<string>> validator)
        {
            var validationErrors = validator(argument);

            if (validationErrors != null && validationErrors.Any())
            {
                return ValidationErrors(validationErrors);
            }

            return null;
        }

        [Obsolete("Use ProcessActionAsync instead")]
        protected IActionResult ProcessAction(Action method)
        {
            try
            {
                method();

                return Succeeded();
            }
            catch (Exception err)
            {
                _loggingService.Exception(err);

                return Exception(err);
            }
            finally
            {
                _loggingService.Save(true);
            }
        }

        [Obsolete("Use ProcessActionAsync instead")]
        protected IActionResult ProcessAction<TArg1>(Action<TArg1> method, TArg1 arg1)
        {
            try
            {
                method(arg1);

                return Succeeded();
            }
            catch (Exception err)
            {
                _loggingService.Exception(err);

                return Exception(err);
            }
            finally
            {
                _loggingService.Save(true);
            }
        }

        [Obsolete("Use ProcessActionAsync instead")]
        protected IActionResult ProcessAction<TArg1, TArg2>(Action<TArg1, TArg2> method, TArg1 arg1, TArg2 arg2)
        {
            try
            {
                method(arg1, arg2);

                return Succeeded();
            }
            catch (Exception err)
            {
                _loggingService.Exception(err);

                return Exception(err);
            }
            finally
            {
                _loggingService.Save(true);
            }
        }

        [Obsolete("Use ProcessActionAsync instead")]
        protected IActionResult ProcessAction<TReturn>(Func<TReturn> method)
        {
            try
            {
                return Succeeded(method());
            }
            catch (Exception err)
            {
                _loggingService.Exception(err);

                return Exception(err);
            }
            finally
            {
                _loggingService.Save(true);
            }
        }

        [Obsolete("Use ProcessActionAsync instead")]
        protected IActionResult ProcessAction<TArg1, TReturn>(Func<TArg1, TReturn> method, TArg1 arg1)
        {
            try
            {
                return Succeeded(method(arg1));
            }
            catch (Exception err)
            {
                _loggingService.Exception(err);

                return Exception(err);
            }
            finally
            {
                _loggingService.Save(true);
            }
        }

        [Obsolete("Use ProcessActionAsync instead")]
        protected IActionResult ProcessAction<TArg1, TArg2, TReturn>(Func<TArg1, TArg2, TReturn> method, TArg1 arg1, TArg2 arg2)
        {
            try
            {
                return Succeeded(method(arg1, arg2));
            }
            catch (Exception err)
            {
                _loggingService.Exception(err);

                return Exception(err);
            }
            finally
            {
                _loggingService.Save(true);
            }
        }

        protected async Task<IActionResult> ProcessActionAsync(Func<CancellationToken, Task> method, CancellationToken cancellationToken = default)
        {
            try
            {
                await method(cancellationToken);

                return Succeeded();
            }
            catch (Exception err)
            {
                _loggingService.Exception(err);

                return Exception(err);
            }
            finally
            {
                _loggingService.Save(true);
            }
        }

        protected async Task<IActionResult> ProcessActionAsync<TReturn>(Func<CancellationToken, Task<TReturn>> method, CancellationToken cancellationToken = default)
        {
            try
            {
                return Succeeded(await method(cancellationToken));
            }
            catch (Exception err)
            {
                _loggingService.Exception(err);

                return Exception(err);
            }
            finally
            {
                _loggingService.Save(true);
            }
        }

        protected async Task<IActionResult> ProcessActionAsync<TArg1, TReturn>(Func<TArg1, CancellationToken, Task<TReturn>> method, TArg1 arg1, CancellationToken cancellationToken = default)
        {
            try
            {
                return Succeeded(await method(arg1, cancellationToken));
            }
            catch (Exception err)
            {
                _loggingService.Exception(err);

                return Exception(err);
            }
            finally
            {
                _loggingService.Save(true);
            }
        }

        protected async Task<IActionResult> ProcessActionAsync<TArg1>(Func<TArg1, CancellationToken, Task> method, TArg1 arg1, CancellationToken cancellationToken = default)
        {
            try
            {
                await method(arg1, cancellationToken);

                return Succeeded();
            }
            catch (Exception err)
            {
                _loggingService.Exception(err);

                return Exception(err);
            }
            finally
            {
                _loggingService.Save(true);
            }
        }

        protected async Task<IActionResult> ProcessActionAsync<TArg1, TArg2, TReturn>(Func<TArg1, TArg2, CancellationToken, Task<TReturn>> method, TArg1 arg1, TArg2 arg2, CancellationToken cancellationToken = default)
        {
            try
            {
                return Succeeded(await method(arg1, arg2, cancellationToken));
            }
            catch (Exception err)
            {
                _loggingService.Exception(err);

                return Exception(err);
            }
            finally
            {
                _loggingService.Save(true);
            }
        }

        protected async Task<IActionResult> ProcessActionAsync<TArg1, TArg2>(Func<TArg1, TArg2, CancellationToken, Task> method, TArg1 arg1, TArg2 arg2, CancellationToken cancellationToken = default)
        {
            try
            {
                await method(arg1, arg2, cancellationToken);

                return Succeeded();
            }
            catch (Exception err)
            {
                _loggingService.Exception(err);

                return Exception(err);
            }
            finally
            {
                _loggingService.Save(true);
            }
        }

        private IActionResult Exception(Exception exception)
        {
#if DEBUG
            Debugger.Break();
#endif
            return StatusCode(500, new ApiResponseMessage(null, exception));
        }

        private IActionResult ValidationErrors(IEnumerable<string> validationErrors) => BadRequest(new ApiResponseMessage(validationErrors, null));

        private IActionResult Succeeded() => Ok(ApiResponseMessage.Empty);

        private IActionResult Succeeded<T>(T data) => Ok(new ApiResponseMessage<T>(data, null, null));

        private readonly ILoggingService _loggingService;
    }
}