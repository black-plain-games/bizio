﻿using Bizio.Core.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading;
using System.Threading.Tasks;

namespace Bizio.Server.Controllers
{
    [Route("api")]
    public class BizioController : BaseApiController
    {
        public BizioController(ILoggingService loggingService)
            : base(loggingService)
        {
        }

        // YOU ARE IN THE PROCESS OF FIXING THE ERRORS
        // IN THE CONTROLLERS. YOU THEN NEED TO PORT OVER
        // ALL OF THE DI REGISTRATIONS, CONFIGURATION VALUES
        // AND THEN SEE IF IT ALL WORKS

        [HttpGet, Route("ping/{message}")]
        public async Task<IActionResult> PingAsync(string message, CancellationToken cancellationToken = default)
        {
            return await ProcessActionAsync(PingInternalAsync, message, cancellationToken);
        }

        private async Task<string> PingInternalAsync(string message, CancellationToken cancellationToken = default)
        {
            await Task.Yield();

            cancellationToken.ThrowIfCancellationRequested();

            return $"Server heard '{message}'.";
        }
    }
}