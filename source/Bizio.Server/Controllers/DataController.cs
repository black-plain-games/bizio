﻿using Bizio.Core.Services;
using Bizio.Server.Model.Core;
using Bizio.Server.Model.Text;
using Bizio.Server.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Bizio.Server.Controllers
{
    [Route("api/data")]
    public class DataController : BaseApiController
    {
        public DataController(
            ILoggingService loggingService,
            IDtoMappingService dtoMappingService,
            IGameService gameService,
            ITextService textService)
            : base(loggingService)
        {
            _gameService = gameService;

            _dtoMappingService = dtoMappingService;

            _textService = textService;
        }

       // [Authorize(Roles = "User,Administrator")]
        [HttpGet, Route("static")]
        public async Task<IActionResult> RetrieveStaticDataAsync(CancellationToken cancellationToken = default)
        {
            return await ProcessActionAsync(RetrieveStaticDataInternalAsync, cancellationToken);
        }

        private async Task<StaticDataDto> RetrieveStaticDataInternalAsync(CancellationToken cancellationToken = default)
        {
            await _gameService.InitializeAsync(cancellationToken);

            var exportData = await _gameService.ExportStaticDataAsync(cancellationToken);

            return _dtoMappingService.ToDto(exportData);
        }

       // [HttpGet, Route("languages/{minimumDateModified}")]
        public async Task<IActionResult> RetrieveLanguagesAsync(DateTimeOffset minimumDateModified, CancellationToken cancellationToken = default)
        {
            return await ProcessActionAsync(RetrieveLanguagesInternalAsync, minimumDateModified, cancellationToken);
        }

        private async Task<LanguageDataDto> RetrieveLanguagesInternalAsync(DateTimeOffset minimumDateModified, CancellationToken cancellationToken = default)
        {
            var languages = _textService.GetLanguages(minimumDateModified);

            await Task.Yield();

            cancellationToken.ThrowIfCancellationRequested();

            return _dtoMappingService.ToDto(languages);
        }

        private readonly IGameService _gameService;

        private readonly IDtoMappingService _dtoMappingService;

        private readonly ITextService _textService;
    }
}