﻿using Bizio.Core.Data.Contracts;
using Bizio.Core.Services;
using Bizio.Server.Model.Game;
using Bizio.Server.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Bizio.Server.Controllers
{
    [Route("api/game")]
    public class GameController : BaseApiController
    {
        public GameController(
            ILoggingService loggingService,
            IDtoMappingService dtoMappingService,
            IGameService gameService,
            IGameDataService gameDataService)
            : base(loggingService)
        {
            _dtoMappingService = dtoMappingService;
            _gameService = gameService;
            _gameDataService = gameDataService;
        }

        //[Authorize(Roles = "User")]
        [HttpPost, Route("")]
        public async Task<IActionResult> CreateAsync([FromBody]NewGameDto newGameDtoData, CancellationToken cancellationToken = default)
        {
            var output = Validate(newGameDtoData, DtoValidator.IsValid);

            if (output != null)
            {
                return output;
            }

            return await ProcessActionAsync(CreateInternalAsync, newGameDtoData, cancellationToken);
        }

        private async Task<int> CreateInternalAsync(NewGameDto newGameDtoData, CancellationToken cancellationToken = default)
        {
            await _gameService.InitializeAsync(cancellationToken);

            var newGameData = _dtoMappingService.ToData(newGameDtoData);

            newGameData.UserId = UserId.Value;

            _gameService.StartNewGame(newGameData);

            return _gameDataService.GameId.Value;
        }

        //[Authorize(Roles = "User")]
        [HttpGet, Route("{gameId}")]
        public async Task<IActionResult> RetrieveAsync(int gameId, CancellationToken cancellationToken = default)
        {
            return await ProcessActionAsync(RetrieveInternalAsync, gameId, cancellationToken);
        }

        private async Task<GameDataDto> RetrieveInternalAsync(int gameId, CancellationToken cancellationToken = default)
        {
            await _gameService.InitializeAsync(cancellationToken);

            _gameService.LoadGame(new LoadGameData
            {
                UserId = UserId.Value,
                GameId = gameId
            });

            var exportData = _gameService.ExportData();

            return _dtoMappingService.ToDto(exportData);
        }

       // [Authorize(Roles = "User")]
        [HttpGet, Route("")]
        public async Task<IActionResult> RetrieveAsync(CancellationToken cancellationToken = default)
        {
            return await ProcessActionAsync(RetrieveInternalAsync, cancellationToken);
        }

        private async Task<IEnumerable<GameDto>> RetrieveInternalAsync(CancellationToken cancellationToken = default)
        {
            await _gameService.InitializeAsync(cancellationToken);

            var games = _gameService.Retrieve(UserId.Value);

            return games.Select(g => _dtoMappingService.ToDto(g));
        }

       // [Authorize(Roles = "User")]
        [HttpPost, Route("{gameId}")]
        public async Task<IActionResult> UpdateAsync(int gameId, [FromBody]ProcessTurnDto processTurnDtoData, CancellationToken cancellationToken = default)
        {
            var output = Validate(processTurnDtoData, DtoValidator.IsValid);

            if (output != null)
            {
                return output;
            }

            await Task.Yield();

            cancellationToken.ThrowIfCancellationRequested();

            return await ProcessActionAsync(UpdateInternalAsync, gameId, processTurnDtoData, cancellationToken);
        }

        private async Task UpdateInternalAsync(int gameId, ProcessTurnDto processTurnDtoData, CancellationToken cancellationToken = default)
        {
            await _gameService.InitializeAsync(cancellationToken);

            _gameService.LoadGame(new LoadGameData
            {
                UserId = UserId.Value,// processTurnDtoData.UserId,
                GameId = gameId
            });

            var actionData = _dtoMappingService.ToData(processTurnDtoData);

            _gameService.ProcessTurn(actionData);

            _gameService.SaveGame();
        }

        private readonly IDtoMappingService _dtoMappingService;
        private readonly IGameService _gameService;
        private readonly IGameDataService _gameDataService;
    }
}