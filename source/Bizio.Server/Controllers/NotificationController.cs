﻿using Bizio.Core.Services;
using Bizio.Server.Model;
using Bizio.Server.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Bizio.Server.Controllers
{
    [Route("api/notification")]
    public class NotificationController : BaseApiController
    {
        private readonly IDtoMappingService _dtoMappingService;
        private readonly INotificationService _notificationService;

        public NotificationController(
            ILoggingService loggingService,
            IDtoMappingService dtoMappingService,
            INotificationService notificationService)
            : base(loggingService)
        {
            _dtoMappingService = dtoMappingService;

            _notificationService = notificationService;
        }

        //[Authorize(Roles = "User,Administrator")]
        [HttpGet, Route("{minimumId}")]
        public async Task<IActionResult> GetAsync(int minimumId, CancellationToken cancellationToken = default)
        {
            return await ProcessActionAsync(GetInternalAsync, minimumId, cancellationToken);
        }

        private async Task<IEnumerable<NotificationDto>> GetInternalAsync(int minimumId, CancellationToken cancellationToken = default)
        {
            await Task.Yield();

            cancellationToken.ThrowIfCancellationRequested();

            return _notificationService.Retrieve(minimumId).Select(_dtoMappingService.ToDto);
        }
    }
}