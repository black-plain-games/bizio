using DI = Bizio.Core.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using Bizio.Repositories;
using Bizio.Services;
using Bizio.Core.DependencyInjection;
using Bizio.Strategies;
using Bizio.Server.Utilities;
using Owin.Security.Providers.Steam;


namespace Bizio.Server
{
    public class MicrosoftDependencyInjector : DI.IDependencyInjector
    {
        private readonly IServiceCollection _services;

        public MicrosoftDependencyInjector(IServiceCollection services)
        {
            _services = services;
        }

        public IDependencyInjector RegisterBundle<TBundle>()
            where TBundle : IDependencyBundle, new()
        {
            new TBundle().RegisterServices(this);

            return this;
        }

        public IDependencyInjector RegisterService<TService, TImplementation>(DI.ServiceLifetime lifetime = DI.ServiceLifetime.Scoped)
            where TService : class
            where TImplementation : class, TService
        {
            switch (lifetime)
            {
                case DI.ServiceLifetime.Singleton:
                    _services.AddSingleton<TService, TImplementation>();
                    break;

                case DI.ServiceLifetime.Scoped:
                    _services.AddScoped<TService, TImplementation>();
                    break;

                case DI.ServiceLifetime.Transient:
                    _services.AddTransient<TService, TImplementation>();
                    break;
            }

            return this;
        }

        public IDependencyInjector RegisterService<TService>(DI.ServiceLifetime lifetime = DI.ServiceLifetime.Scoped)
            where TService : class
        {
            return RegisterService<TService, TService>(lifetime);
        }

        public IDependencyInjector RegisterService<TService, TImplementation>(Func<IServiceProvider, TImplementation> factory, DI.ServiceLifetime lifetime)
            where TService : class
            where TImplementation : class, TService
        {
            switch (lifetime)
            {
                case DI.ServiceLifetime.Singleton:
                    _services.AddSingleton<TService, TImplementation>(factory);
                    break;

                case DI.ServiceLifetime.Scoped:
                    _services.AddScoped<TService, TImplementation>(factory);
                    break;

                case DI.ServiceLifetime.Transient:
                    _services.AddTransient<TService, TImplementation>(factory);
                    break;
            }

            return this;
        }
    }

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            new MicrosoftDependencyInjector(services)
                .RegisterService<IDtoMappingService, DtoMappingService>()
                .RegisterBundle<BizioRepositoriesDependencyBundle>()
                .RegisterBundle<BizioServicesDependencyBundle>()
                .RegisterBundle<BizioStrategiesDependencyBundle>();
            
            services.Configure<BizioConnectionStringOptions>(options => Configuration.GetSection("BizioConnectionString").Bind(options));
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
