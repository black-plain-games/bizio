﻿using Bizio.Server.Model.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Bizio.Server.Utilities
{
    internal static class DtoHelpers
    {
        public static readonly DateTime DawnOfTime = new DateTime(1500, 1, 1);

        public static IEnumerable<IdValuePairDto<TId, string>> GetEnumValues<TId, TEnum>(params TEnum[] exclusions)
        {
            return Core.Utilities.Utilities.GetValues(exclusions)
                            .ToDto(x => (TId)Convert.ChangeType(x, typeof(TId)), x => SplitByCapitals($"{x}"))
                            .Where(x => x.Value != "None");
        }

        public static string SplitByCapitals(string source)
        {
            return Regex.Replace(source, @"((?<=\p{Ll})\p{Lu}|\p{Lu}(?=\p{Ll}))", " $1").Trim();
        }

        public static IEnumerable<IdValuePairDto<TId, TValue>> ToDto<TSource, TId, TValue>(this IEnumerable<TSource> _this, Func<TSource, TId> idSelector, Func<TSource, TValue> valueSelector)
        {
            return _this.Select(x => new IdValuePairDto<TId, TValue>(idSelector(x), valueSelector(x)));
        }
    }
}