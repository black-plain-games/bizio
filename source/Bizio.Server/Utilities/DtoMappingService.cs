﻿using Bizio.Core.Data;
using Bizio.Core.Data.Actions;
using Bizio.Core.Data.Companies;
using Bizio.Core.Data.Contracts;
using Bizio.Core.Data.Exports;
using Bizio.Core.Data.Exports.Statics;
using Bizio.Core.Data.Game;
using Bizio.Core.Data.People;
using Bizio.Core.Data.Perks;
using Bizio.Core.Data.Projects;
using Bizio.Core.Data.Text;
using Bizio.Core.Services.Bases;
using Bizio.Core.Utilities;
using Bizio.Server.Model;
using Bizio.Server.Model.Actions;
using Bizio.Server.Model.Companies;
using Bizio.Server.Model.Core;
using Bizio.Server.Model.Game;
using Bizio.Server.Model.GameClock;
using Bizio.Server.Model.People;
using Bizio.Server.Model.Perks;
using Bizio.Server.Model.Projects;
using Bizio.Server.Model.Text;
using Bizio.Strategies.Actions.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Bizio.Server.Utilities
{
    public class DtoMappingService : IDtoMappingService
    {
        public IEnumerable<IActionData> ToData(ProcessTurnDto dataDto)
        {
            return dataDto?.Actions?.Select(a => ToData(a));
        }

        public NewGameData ToData(NewGameDto dataDto)
        {
            return new NewGameData
            {
                CompanyName = dataDto.CompanyName,
                IndustryId = dataDto.IndustryId,
                InitialFunds = dataDto.InitialFunds,
                NumberOfCompanies = dataDto.NumberOfCompanies,
                StartDate = dataDto.StartDate,
                WeeksPerTurn = dataDto.WeeksPerTurn,
                Founder = ToData(dataDto.Founder)
            };
        }

        public GameDataDto ToDto(GameExportData data)
        {
            return new GameDataDto
            {
                GameId = data.GameId,
                IndustryId = data.IndustryId,
                StatusId = (byte)data.Status,
                GameClockData = ToDto(data.GameClockData),
                ProjectsData = ToDto(data.ProjectData),
                PeopleData = ToDto(data.PeopleData),
                CompaniesData = ToDto(data.CompanyData)
            };
        }

        public StaticDataDto ToDto(StaticExportData data)
        {
            return new StaticDataDto
            {
                ActionsData = ToDto(data.ActionsData),
                CompaniesData = ToDto(data.CompaniesData),
                PeopleData = ToDto(data.PeopleData),
                PerksData = ToDto(data.PerksData),
                ProjectsData = ToDto(data.ProjectData),
                GameData = ToDto(data.GameData)
            };
        }

        private StaticGameDataDto ToDto(StaticGameExportData difficultyLevelsData)
        {
            return new StaticGameDataDto
            {
                DifficultyLevels = difficultyLevelsData.DifficultyLevels.Select(ToDto).ToList()
            };
        }

        private DifficultyLevelDto ToDto(DifficultyLevel difficultyLevel)
        {
            return new DifficultyLevelDto
            {
                Id = difficultyLevel.Id,
                InitialFunds = difficultyLevel.InitialFunds,
                MaximumOptionalSkillCount = difficultyLevel.MaximumOptionalSkillCount,
                MaximumTotalSkillPoints = difficultyLevel.MaximumTotalSkillPoints,
                Name = difficultyLevel.Name
            };
        }

        public GameDto ToDto(Game data)
        {
            return new GameDto
            {
                Id = data.Id,
                CompanyName = data.CompanyName,
                CreatedDate = data.CreatedDate,
                CurrentTurn = data.CurrentTurn,
                FounderFirstName = data.FounderFirstName,
                FounderLastName = data.FounderLastName,
                IndustryId = data.IndustryId,
                WeeksPerTurn = data.WeeksPerTurn,
                StatusId = (byte)data.Status
            };
        }

        public NotificationDto ToDto(Notification data)
        {
            return new NotificationDto
            {
                Id = data.Id,
                DateCreated = data.DateCreated,
                Subject = data.Subject,
                Message = data.Message
            };
        }

        public LanguageDataDto ToDto(IEnumerable<Language> data)
        {
            return new LanguageDataDto
            {
                Languages = data.Select(l => new LanguageDto
                {
                    Id = l.Id,
                    Name = l.Name,
                    Iso2LetterCode = l.Iso2LetterCode,
                    Text = l.Text.ToDictionary(t => t.Key.Id, t => t.Value.Text)
                }),
                TextResourceKeys = data
                    .SelectMany(l => l.Text.Keys)
                    .Distinct(new TextResourceKeyComparer())
                    .ToDictionary(trk => trk.Id, trk => trk.Name)
            };
        }

        private class TextResourceKeyComparer : IEqualityComparer<TextResourceKey>
        {
            public bool Equals(TextResourceKey x, TextResourceKey y)
            {
                if (x == null || y == null)
                {
                    return false;
                }

                return x.Id == y.Id;
            }

            public int GetHashCode(TextResourceKey obj)
            {
                return obj.Id;
            }
        }

        private IActionData ToData(ActionDataDto dataDto)
        {
            if (dataDto == null)
            {
                return null;
            }

            T MapActionData<T>(IDictionary<string, object> data) where T : IActionData
            {
                object GetParameter(ParameterInfo parameter)
                {
                    var key = data.Keys.FirstOrDefault(k => string.Compare(k, parameter.Name, true) == 0);

                    if (key == null)
                    {
                        return null;
                    }

                    if (parameter.ParameterType.IsEnum)
                    {
                        return Enum.Parse(parameter.ParameterType, $"{data[key]}");
                    }

                    return Convert.ChangeType(data[key], parameter.ParameterType);
                }

                var constructor = typeof(T).GetConstructors().First();

                var parameters = constructor.GetParameters().Select(GetParameter).ToArray();

                return (T)constructor.Invoke(parameters);
            }

            return ((ActionType)dataDto.ActionTypeId) switch
            {
                ActionType.AcceptProject => MapActionData<AcceptProjectActionData>(dataDto.Data),
                ActionType.AdjustAllocation => MapActionData<AdjustAllocationActionData>(dataDto.Data),
                ActionType.AdjustSalary => MapActionData<AdjustSalaryActionData>(dataDto.Data),
                ActionType.ChangeCompanyMessageStatus => MapActionData<ChangeCompanyMessageStatusActionData>(dataDto.Data),
                ActionType.FireEmployee => MapActionData<FireEmployeeActionData>(dataDto.Data),
                ActionType.InterviewProspect => MapActionData<InterviewProspectActionData>(dataDto.Data),
                ActionType.MakeOffer => MapActionData<MakeOfferActionData>(dataDto.Data),
                ActionType.PurchasePerk => MapActionData<PurchasePerkActionData>(dataDto.Data),
                ActionType.RecruitPerson => MapActionData<RecruitPersonActionData>(dataDto.Data),
                ActionType.RequestExtension => MapActionData<RequestExtensionActionData>(dataDto.Data),
                ActionType.SellPerk => MapActionData<SellPerkActionData>(dataDto.Data),
                ActionType.SubmitProject => MapActionData<SubmitProjectActionData>(dataDto.Data),
                ActionType.ToggleCompanyAction => MapActionData<ToggleCompanyActionActionData>(dataDto.Data),
                _ => throw new Exception($"No known mapping for action type {dataDto.ActionTypeId}"),
            };
        }

        private CreatePersonData ToData(CreatePersonDto dataDto)
        {
            return new CreatePersonData(dataDto.Skills.ToDictionary(s => s.Id, s => s.Value))
            {
                Birthday = dataDto.Birthday,
                FirstName = dataDto.FirstName,
                Gender = (Gender)dataDto.Gender,
                LastName = dataDto.LastName,
                PersonalityId = dataDto.PersonalityId
            };
        }

        private CompaniesDataDto ToDto(CompaniesExportData data)
        {
            return new CompaniesDataDto
            {
                Companies = data.Companies.Select(ToDto),

            };
        }

        private CompanyActionDto ToDto(CompanyAction data)
        {
            return new CompanyActionDto
            {
                ActionId = data.Action.Id,
                IsActive = data.IsActive,
                Count = data.Count,
                Accumulations = data.Accumulations.ToDto(a => a.SkillDefinition.Id, a => a.Value)
            };
        }

        private CompanyDto ToDto(Company data)
        {
            return new CompanyDto
            {
                Id = data.Id,
                Name = data.Name,
                Money = data.Money,
                InitialAccuracy = data.InitialAccuracy,
                UserId = data.UserId,
                RivalId = data.Rival?.Id,
                StatusId = (byte)data.Status,
                Actions = data.Actions.Select(ToDto),
                Allocations = data.Allocations.Select(ToDto),
                Employees = data.Employees.Select(ToDto),
                Messages = data.Messages.Select(ToDto),
                Perks = data.Perks.Select(ToDto),
                Projects = data.Projects.Select(ToDto),
                Prospects = data.Prospects.Select(ToDto),
                Reputation = ToDto(data.Reputation),
                Transactions = data.Transactions.Select(ToDto)
            };
        }

        private GameClockDataDto ToDto(GameClockExportData data)
        {
            return new GameClockDataDto
            {
                StartDate = data.StartDate,
                CurrentDate = data.CurrentDate,
                CurrentTurn = data.CurrentTurn,
                WeeksPerTurn = data.WeeksPerTurn
            };
        }

        private StaticCompaniesDataDto ToDto(StaticIndustriesExportData data)
        {
            return new StaticCompaniesDataDto
            {
                Industries = data.Industries.Select(ToDto),
                CompanyMessageStatuses = DtoHelpers.GetEnumValues<byte, CompanyMessageStatus>(),
                TransactionTypes = DtoHelpers.GetEnumValues<byte, TransactionType>()
            };
        }

        private RangeDto<T> ToDto<T>(Range<T> data)
        {
            return new RangeDto<T>
            {
                Minimum = data.Minimum,
                Maximum = data.Maximum
            };
        }

        private IndustryDto ToDto(Industry data)
        {
            return new IndustryDto
            {
                Id = data.Id,
                Name = data.Name
            };
        }

        private StaticProjectsDataDto ToDto(StaticProjectsExportData projectsData)
        {
            return new StaticProjectsDataDto
            {
                ProjectDefinitions = projectsData.ProjectDefinitions.Select(ToDto),
                ProjectRoles = DtoHelpers.GetEnumValues<byte, ProjectRole>(),
                ProjectStatuses = DtoHelpers.GetEnumValues<byte, ProjectStatus>(),
                StarCategories = DtoHelpers.GetEnumValues<byte, StarCategory>()
            };
        }

        private ProjectDefinitionDto ToDto(ProjectDefinition data)
        {
            return new ProjectDefinitionDto
            {
                Id = data.Id,
                Name = data.Name,
                Description = data.Description,
                ProjectLength = ToDto(data.ProjectLength),
                Value = ToDto(data.Value),
                Industries = data.Industries.Select(i => i.Id),
                Skills = data.Skills.ToDto(s => s.SkillDefinition.Id, s => ToDto(s.Value))
            };
        }

        private StaticPerksDataDto ToDto(StaticPerksExportData data)
        {
            return new StaticPerksDataDto
            {
                Perks = data.Perks.Select(ToDto)
            };
        }

        private PerkDto ToDto(Perk data)
        {
            return new PerkDto
            {
                Id = data.Id,
                Name = data.Name,
                Description = data.Description,
                MaxCount = data.MaxCount,
                InitialCost = data.InitialCost,
                RecurringCost = data.RecurringCost,
                RecurringCostInterval = data.RecurringCostInterval,
                Benefits = data.Benefits.Select(ToDto),
                Conditions = data.Conditions.Select(ToDto)
            };
        }

        private PerkBenefitDto ToDto(PerkBenefit data)
        {
            return new PerkBenefitDto
            {
                Attribute = (PerkBenefitAttributeDto)data.Attribute,
                TargetType = (PerkTargetTypeDto)data.Target,
                Value = data.Value,
                ValueType = (PerkValueTypeDto)data.ValueType
            };
        }

        private PerkConditionDto ToDto(PerkCondition data)
        {
            return new PerkConditionDto
            {
                Attribute = (PerkConditionAttributeDto)data.Attribute,
                TargetType = (PerkTargetTypeDto)data.Target,
                Value = data.Value,
                ComparisonType = (PerkConditionComparisonTypeDto)data.Comparison
            };
        }

        private StaticActionsDataDto ToDto(StaticActionsExportData data)
        {
            return new StaticActionsDataDto
            {
                Actions = data.Actions.Select(ToDto)
            };
        }

        private ActionDto ToDto(Core.Data.Game.Action data)
        {
            return new ActionDto
            {
                Id = data.Id,
                MaxCount = data.MaxCount,
                Name = data.Name,
                Requirements = data.Requirements.ToDto(r => r.SkillDefinition.Id, r => r.Value)
            };
        }

        private StaticPeopleDataDto ToDto(StaticPeopleExportData data)
        {
            return new StaticPeopleDataDto
            {
                SkillDefinitions = data.SkillDefinitions.Select(ToDto),
                MandatorySkillDefinitions = data.MandatorySkillDefinitions.ToDto(sd => sd.Key, sd => sd.Value.Select(ToDto)),
                Professions = data.Professions.ToDto(p => p.Key, p => p.Value.Select(ToDto)),
                Personalities = data.Personalities.Select(ToDto),
                Genders = DtoHelpers.GetEnumValues<byte, Gender>(),
                PersonalityAttributeIds = DtoHelpers.GetEnumValues<byte, PersonalityAttributeId>()
            };
        }

        private SkillDefinitionDto ToDto(SkillDefinition data)
        {
            return new SkillDefinitionDto
            {
                Id = data.Id,
                Name = data.Name,
                Value = ToDto(data.Value)
            };
        }

        private ProfessionDto ToDto(Profession data)
        {
            return new ProfessionDto
            {
                Id = data.Id,
                Name = data.Name,
                SkillDefinitions = data.SkillDefinitions.Select(ToDto)
            };
        }

        private ProfessionSkillDefinitionDto ToDto(ProfessionSkillDefinition data)
        {
            return new ProfessionSkillDefinitionDto
            {
                SkillDefinitionId = data.SkillDefinition.Id,
                Weight = data.Weight
            };
        }

        private PersonalityDto ToDto(Personality data)
        {
            return new PersonalityDto
            {
                Id = data.Id,
                Name = data.Name,
                Description = data.Description,
                Attributes = data.Attributes.ToDto(a => (byte)a.Id, a => a.Value)
            };
        }

        private SkillDto ToDto(Skill data)
        {
            return new SkillDto
            {
                SkillDefinitionId = data.SkillDefinition.Id,
                Value = data.Value,
                LearnRate = data.LearnRate,
                ForgetRate = data.ForgetRate
            };
        }

        private WorkHistoryDto ToDto(WorkHistory data)
        {
            return new WorkHistoryDto
            {
                CompanyId = data.Company.Id,
                StartDate = data.StartDate,
                StartingSalary = data.StartingSalary,
                EndDate = data.EndDate,
                EndingSalary = data.EndingSalary
            };
        }

        private PersonDto ToDto(Person data)
        {
            return new PersonDto
            {
                Id = data.Id,
                Birthday = data.Birthday,
                FirstName = data.FirstName,
                GenderId = (byte)data.Gender,
                LastName = data.LastName,
                PersonalityId = data.Personality.Id,
                RetirementDate = data.RetirementDate,
                ProfessionId = data.ProfessionId,
                Skills = data.Skills.Select(ToDto),
                WorkHistory = data.WorkHistory.Select(ToDto)
            };
        }

        private PeopleDataDto ToDto(PeopleExportData data)
        {
            return new PeopleDataDto
            {
                People = data.People.Select(ToDto)
            };
        }

        private ProjectsDataDto ToDto(ProjectsExportData data)
        {
            return new ProjectsDataDto
            {
                Projects = data.Projects.Select(ToDto)
            };
        }

        private TransactionDto ToDto(Transaction data)
        {
            return new TransactionDto
            {
                Amount = data.Amount,
                Date = data.Date,
                Description = data.Description,
                EndingBalance = data.EndingBalance,
                Id = data.Id,
                TransactionType = (TransactionTypeDto)data.Type
            };
        }

        private ReputationDto ToDto(Reputation data)
        {
            return new ReputationDto
            {
                EarnedStars = data.EarnedStars,
                PossibleStars = data.PossibleStars,
                Value = data.Value
            };
        }

        private ProspectDto ToDto(Prospect data)
        {
            return new ProspectDto
            {
                Accuracy = data.Accuracy,
                PersonId = data.Person.Id,
                Salary = ToDto(data.Salary),
                Skills = data.Skills.ToDto(x => x.SkillDefinition.Id, x => ToDto(x.Value))
            };
        }

        private ProjectDto ToDto(Project data)
        {
            return new ProjectDto
            {
                Deadline = data.Deadline,
                ExtensionDeadline = data.ExtensionDeadline,
                Id = data.Id,
                ProjectDefinitionId = data.Definition.Id,
                ReputationRequired = (byte)data.ReputationRequired,
                Requirements = data.Requirements.Select(ToDto),
                Status = (ProjectStatusDto)data.Status,
                Value = data.Value
            };
        }

        private ProjectRequirementDto ToDto(ProjectRequirement data)
        {
            return new ProjectRequirementDto
            {
                CurrentValue = data.CurrentValue,
                SkillDefinitionId = data.SkillDefinition.Id,
                TargetValue = data.TargetValue
            };
        }

        private CompanyPerkDto ToDto(CompanyPerk data)
        {
            return new CompanyPerkDto
            {
                Id = data.Id,
                PerkId = data.Perk.Id,
                NextPaymentDate = data.NextPaymentDate
            };
        }

        private CompanyMessageDto ToDto(CompanyMessage data)
        {
            return new CompanyMessageDto
            {
                DateCreated = data.DateCreated,
                Id = data.Id,
                Message = data.Message,
                Source = data.Source,
                Status = (byte)data.Status,
                Subject = data.Subject
            };
        }

        private EmployeeDto ToDto(Employee data)
        {
            return new EmployeeDto
            {
                Happiness = data.Happiness,
                IsFounder = data.IsFounder,
                PersonId = data.Person.Id,
                Salary = data.Salary
            };
        }

        private AllocationDto ToDto(Allocation data)
        {
            return new AllocationDto
            {
                Percent = data.Percent,
                PersonId = data.Employee.Person.Id,
                ProjectId = data.Project.Id,
                RoleId = (byte)data.Role
            };
        }
    }
}