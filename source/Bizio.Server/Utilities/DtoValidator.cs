﻿using Bizio.Core.Data.Actions;
using Bizio.Core.Data.People;
using Bizio.Server.Model.Game;
using Bizio.Server.Model.People;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Server.Utilities
{
    public static class DtoValidator
    {
        private static readonly int CompanyNameMaxLength = 64;

        private static readonly int FirstNameMaxLength = 64;

        private static readonly int LastNameMaxLength = 64;

        public static IEnumerable<string> IsValid(NewGameDto dtoData)
        {
            var output = new List<string>();

            if (dtoData == null)
            {
                AddError(output, nameof(NewGameDto));

                return output;
            }

            if (string.IsNullOrWhiteSpace(dtoData.CompanyName) || dtoData.CompanyName.Length > CompanyNameMaxLength)
            {
                AddError(output, nameof(NewGameDto), nameof(dtoData.CompanyName), dtoData.CompanyName);
            }

            if (dtoData.StartDate < DtoHelpers.DawnOfTime)
            {
                AddError(output, nameof(NewGameDto), nameof(dtoData.StartDate), dtoData.StartDate);
            }

            if (dtoData.IndustryId <= 0)
            {
                AddError(output, nameof(NewGameDto), nameof(dtoData.IndustryId), dtoData.IndustryId);
            }

            if (dtoData.InitialFunds <= 0)
            {
                AddError(output, nameof(NewGameDto), nameof(dtoData.InitialFunds), dtoData.InitialFunds);
            }

            return IsValid(dtoData.Founder, output);
        }

        public static ICollection<string> IsValid(CreatePersonDto dtoData)
        {
            return IsValid(dtoData, new List<string>());
        }

        public static ICollection<string> IsValid(CreatePersonDto dtoData, ICollection<string> errors)
        {
            if (dtoData == null)
            {
                AddError(errors, nameof(CreatePersonDto));

                return errors;
            }

            if (string.IsNullOrWhiteSpace(dtoData.FirstName) || dtoData.FirstName.Length > FirstNameMaxLength)
            {
                AddError(errors, nameof(CreatePersonDto), nameof(dtoData.FirstName), dtoData.FirstName);
            }

            if (!Core.Utilities.Utilities.ContainsValue(dtoData.Gender, Gender.None))
            {
                AddError(errors, nameof(CreatePersonDto), nameof(dtoData.Gender), dtoData.Gender);
            }

            if (string.IsNullOrWhiteSpace(dtoData.LastName) || dtoData.LastName.Length > LastNameMaxLength)
            {
                AddError(errors, nameof(CreatePersonDto), nameof(dtoData.LastName), dtoData.LastName);
            }

            if (dtoData.Birthday <= DtoHelpers.DawnOfTime)
            {
                AddError(errors, nameof(CreatePersonDto), nameof(dtoData.Birthday), dtoData.Birthday);
            }

            if (dtoData.PersonalityId <= 0)
            {
                AddError(errors, nameof(CreatePersonDto), nameof(dtoData.PersonalityId), dtoData.PersonalityId);
            }

            if (dtoData.Skills == null || !dtoData.Skills.Any())
            {
                AddError(errors, nameof(CreatePersonDto), nameof(dtoData.Skills), dtoData.Skills?.Count());
            }

            return errors;
        }

        public static ICollection<string> IsValid(ProcessTurnDto dtoData)
        {
            var output = new List<string>();

            if (dtoData == null)
            {
                AddError(output, nameof(ProcessTurnDto));

                return output;
            }

            if (dtoData.Actions == null)
            {
                AddError(output, nameof(ProcessTurnDto), nameof(dtoData.Actions));
            }

            foreach (var action in dtoData.Actions)
            {
                if (!Core.Utilities.Utilities.ContainsValue(action.ActionTypeId, ActionType.None))
                {
                    AddError(output, nameof(ProcessTurnDto), nameof(action.ActionTypeId), action.ActionTypeId);
                }

                if (action.Data == null || !action.Data.Any())
                {
                    AddError(output, nameof(ProcessTurnDto), nameof(action.Data), action.Data?.Count);
                }

                foreach (var data in action.Data)
                {
                    if (data.Value == null)
                    {
                        AddError(output, nameof(ProcessTurnDto), nameof(action.Data), data.Value);
                    }
                }
            }

            return output;
        }

        private static void AddError(ICollection<string> errors, string dtoName)
        {
            AddError(errors, dtoName, null, null);
        }

        private static void AddError(ICollection<string> errors, string dtoName, string fieldName)
        {
            AddError(errors, dtoName, fieldName, null);
        }

        private static void AddError(ICollection<string> errors, string dtoName, string fieldName, object value)
        {
            var error = dtoName;

            if (!string.IsNullOrWhiteSpace(fieldName))
            {
                error += $".{fieldName}";
            }

            if (value != null)
            {
                error += $" ({value})";
            }

            errors.Add(error);
        }
    }
}