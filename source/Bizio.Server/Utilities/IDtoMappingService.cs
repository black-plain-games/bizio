﻿using Bizio.Core.Data;
using Bizio.Core.Data.Actions;
using Bizio.Core.Data.Contracts;
using Bizio.Core.Data.Exports;
using Bizio.Core.Data.Exports.Statics;
using Bizio.Core.Data.Game;
using Bizio.Core.Data.Text;
using Bizio.Server.Model;
using Bizio.Server.Model.Core;
using Bizio.Server.Model.Game;
using Bizio.Server.Model.Text;
using System.Collections.Generic;

namespace Bizio.Server.Utilities
{
    public interface IDtoMappingService
    {
        IEnumerable<IActionData> ToData(ProcessTurnDto dataDto);

        NewGameData ToData(NewGameDto dataDto);

        GameDataDto ToDto(GameExportData data);

        StaticDataDto ToDto(StaticExportData data);

        GameDto ToDto(Game data);

        NotificationDto ToDto(Notification data);

        LanguageDataDto ToDto(IEnumerable<Language> data);
    }
}