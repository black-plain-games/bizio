﻿using Bizio.Core.Data.Contracts;
using Bizio.Core.Repositories;
using Bizio.Core.Services;
using FluentAssertions;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace Bizio.Services.Tests
{
    public class GameClockService_Tests
    {
        public GameClockService_Tests()
        {
            _mockGameClockRepository = new Mock<IGameClockRepository>();

            _gameClockService = new GameClockService(_mockGameClockRepository.Object);
        }

        public static IEnumerable<object[]> CalculateDate_Turns_Succeeds_Data
        {
            get
            {
                yield return new object[]
                {
                    DateTime.Today, 1, 5, DateTime.Today.AddDays(35)
                };

                yield return new object[]
                {
                    DateTime.Today, 2, 4, DateTime.Today.AddDays(56)
                };
            }
        }

        [Theory]
        [MemberData(nameof(CalculateDate_Turns_Succeeds_Data))]
        public void CalculateDate_Turns_Succeeds(DateTime startDate, byte weeksPerTurn, int turns, DateTime expected)
        {
            _gameClockService.StartNewGame(new NewGameDataInternal(new NewGameData
            {
                StartDate = startDate,
                WeeksPerTurn = weeksPerTurn
            }));

            var date = _gameClockService.CalculateDate(turns);

            date.Should().Be(expected);
        }

        public static IEnumerable<object[]> CalculateDate_Succeeds_Data
        {
            get
            {
                yield return new object[]
                {
                    1, DateTime.Today, 3, DateTime.Today.AddDays(21)
                };

                yield return new object[]
                {
                    2, DateTime.Today.AddMonths(1).AddDays(3), 2, DateTime.Today.AddMonths(1).AddDays(31)
                };
            }
        }

        [Theory]
        [MemberData(nameof(CalculateDate_Succeeds_Data))]
        public void CalculateDate_Succeeds(byte weeksPerTurn, DateTime startDate, int turns, DateTime expected)
        {
            _gameClockService.StartNewGame(new NewGameDataInternal(new NewGameData
            {
                StartDate = DateTime.Today,
                WeeksPerTurn = weeksPerTurn
            }));

            var date = _gameClockService.CalculateDate(startDate, turns);

            date.Should().Be(expected);
        }

        public static IEnumerable<object[]> DateToTurnDifference_Succeeds_Data
        {
            get
            {
                yield return new object[]
                {
                    DateTime.Today, 1, DateTime.Today.AddDays(49), 7
                };

                yield return new object[]
                {
                    DateTime.Today, 3, DateTime.Today.AddDays(210), 10
                };
            }
        }

        [Theory]
        [MemberData(nameof(DateToTurnDifference_Succeeds_Data))]
        public void DateToTurnDifference_Succeeds(DateTime startDate, byte weeksPerTurn, DateTime date, int expected)
        {
            _gameClockService.StartNewGame(new NewGameDataInternal(new NewGameData
            {
                StartDate = startDate,
                WeeksPerTurn = weeksPerTurn
            }));

            var turn = _gameClockService.DateToTurnDifference(date);

            turn.Should().Be(expected);
        }

        private readonly Mock<IGameClockRepository> _mockGameClockRepository;

        private readonly IGameClockService _gameClockService;
    }
}