﻿using Bizio.Core.Data.Exports.Statics;
using Bizio.Core.Model.Actions;
using Bizio.Core.Repositories;
using Bizio.Core.Services;
using Bizio.Core.Services.Bases;
using Bizio.Core.Utilities;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Bizio.Services
{
    public class ActionService : ServiceBase<ActionService>, IActionService
    {
        public IEnumerable<Action> Actions { get; private set; }

        public ActionService(
            IDataRepository repository,
            ISkillDefinitionService skillDefinitionService)
            : base()
        {
            _repository = repository;
            _skillDefinitionService = skillDefinitionService;
        }

        public Action GetAction(byte id)
        {
            return Actions.FirstOrDefault(a => a.Id == id);
        }

        public async Task<StaticActionsExportData> ExportStaticDataAsync(CancellationToken cancellationToken = default)
        {
            await Task.Yield();

            cancellationToken.ThrowIfCancellationRequested();

            return new StaticActionsExportData
            {
                Actions = Actions
            };
        }

        protected override async Task InitializeInternalAsync(CancellationToken cancellationToken = default)
        {
            var initializePersonServiceTask = _skillDefinitionService.InitializeAsync(cancellationToken);

            Actions = await _repository.LoadActionsAsync(cancellationToken);

            await initializePersonServiceTask;

            foreach (var action in Actions)
            {
                foreach (var requirement in action.Requirements)
                {
                    requirement.SkillDefinition = _skillDefinitionService.GetSkillDefinition(requirement.SkillDefinition.Id);
                }
            }
        }

        private readonly IDataRepository _repository;
        private readonly ISkillDefinitionService _skillDefinitionService;
    }
}