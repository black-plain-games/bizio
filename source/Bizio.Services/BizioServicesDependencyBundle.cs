﻿using Bizio.Core.DependencyInjection;
using Bizio.Core.Services;

namespace Bizio.Services
{
    public class BizioServicesDependencyBundle : IDependencyBundle
    {
        public void RegisterServices(IDependencyInjector injector)
        {
            injector.RegisterService<IActionService, ActionService>();
            injector.RegisterService<ICompanyService, CompanyService>();
            injector.RegisterService<IConfigurationService, ConfigurationService>();
            injector.RegisterService<IGameClockService, GameClockService>();
            injector.RegisterService<IGameService, GameService>();
            injector.RegisterService<ILoggingService, LoggingService>();
            injector.RegisterService<IPerkService, PerkService>();
            injector.RegisterService<IPersonService, PersonService>();
            injector.RegisterService<IProjectService, ProjectService>();
            injector.RegisterService<INotificationService, NotificationService>();
            injector.RegisterService<ITextService, TextService>();
            injector.RegisterService<IIndustryService, IndustryService>();
            injector.RegisterService<ICompensationService, CompensationService>();
            injector.RegisterService<ISkillDefinitionService, SkillDefinitionService>();
            injector.RegisterService<IGameDataService, GameDataService>();
        }
    }
}