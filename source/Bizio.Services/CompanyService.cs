﻿using Bizio.Core.Data.Contracts;
using Bizio.Core.Data.Exports;
using Bizio.Core.Data.People;
using Bizio.Core.Model.Actions;
using Bizio.Core.Model.Companies;
using Bizio.Core.Model.People;
using Bizio.Core.Model.Perks;
using Bizio.Core.Model.Projects;
using Bizio.Core.Repositories;
using Bizio.Core.Services;
using Bizio.Core.Services.Bases;
using Bizio.Core.Services.Data;
using Bizio.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Bizio.Services
{
    public class CompanyService : ServiceBase<CompanyService>, ICompanyService
    {
        public IEnumerable<Company> Companies { get; private set; }

        public IEnumerable<Company> ActiveCompanies => Companies.Where(c => c.Status == CompanyStatus.InBusiness);

        public Company UserCompany => Companies?.FirstOrDefault(c => c.UserId.HasValue);

        public CompanyService(ICompanyRepository companyRepository,
            IActionService actionService,
            IPersonService personService,
            ISkillDefinitionService skillDefinitionService,
            IProjectService projectService,
            IPerkService perkService,
            ILoggingService loggingService,
            IGameClockService gameClockService,
            ICompensationService compensationService,
            IEnumerable<IActionDataGenerator> actionDataGenerators)
            : base()
        {
            _companyRepository = companyRepository;

            _actionService = actionService;

            _personService = personService;
            
            _skillDefinitionService = skillDefinitionService;
            
            _projectService = projectService;

            _perkService = perkService;

            _loggingService = loggingService;

            _gameClockService = gameClockService;

            _compensationService = compensationService;

            _actionDataGenerators = actionDataGenerators;
        }

        public void StartNewGame(NewGameDataInternal data)
        {
            data.UserCompany = CreateCompany(data.InputData.IndustryId, data.InputData.CompanyName, data.InputData.InitialFunds, data.UserPerson);

            data.UserCompany.UserId = data.InputData.UserId;

            var companies = new List<Company>();

            Companies = companies;

            while (companies.Count < data.InputData.NumberOfCompanies)
            {
                var founder = _personService.GetUnemployedPerson(data.InputData.IndustryId);

                var name = _companyNames[data.InputData.IndustryId].Where(cn => !Companies.Any(c => c.Name == cn))
                                                    .Random();

                var company = CreateCompany(data.InputData.IndustryId, name, data.InputData.InitialFunds, founder);

                companies.Add(company);
            }

            companies.Add(data.UserCompany);
        }

        public int SaveGame(int gameId)
        {
            var serviceData = GetData();

            using (var transaction = new RepositoryTransaction(_companyRepository))
            {
                _companyRepository.SaveGame(gameId, serviceData);
            }

            return gameId;
        }

        public void SaveCompanies(int gameId)
        {
            using (var transaction = new RepositoryTransaction(_companyRepository))
            {
                _companyRepository.SaveCompanies(gameId, Companies);
            }
        }

        public void LoadGame(LoadGameData data)
        {
            CompanyServiceData loadData = null;

            var x = new Stopwatch();

            x.Start();

            using (var transaction = new RepositoryTransaction(_companyRepository))
            {
                loadData = _companyRepository.LoadGame(data.UserId, data.GameId);
            }

            x.Stop();

            Companies = new List<Company>(loadData.Companies);
        }

        public Company GetCompany(int id)
        {
            return Companies.FirstOrDefault(c => c.Id == id);
        }

        public void SynchronizeData(byte currentIndustryId)
        {
            foreach (var company in Companies)
            {
                if (company.Rival != null)
                {
                    company.Rival = GetCompany(company.Rival.Id);
                }

                foreach (var action in company.Actions)
                {
                    action.Action = _actionService.GetAction(action.Action.Id);

                    foreach (var accumulation in action.Accumulations)
                    {
                        accumulation.SkillDefinition = _skillDefinitionService.GetSkillDefinition(accumulation.SkillDefinition.Id);
                    }
                }

                foreach (var employee in company.Employees)
                {
                    var person = _personService.GetPerson(employee.Person.Id);

                    if (person == null)
                    {
                        throw new Exception("No person!");
                    }

                    employee.Person = person;

                    employee.Happiness = CalculateHappiness(currentIndustryId, employee);
                }

                foreach (var perk in company.Perks)
                {
                    perk.Perk = _perkService.GetPerk(perk.Perk.Id);
                }

                var projectIds = company.Projects.Select(p => p.Id).ToList();

                company.Projects.Clear();

                foreach (var projectId in projectIds)
                {
                    company.Projects.Add(_projectService.GetProject(projectId));
                }

                foreach (var prospect in company.Prospects)
                {
                    prospect.Person = _personService.GetPerson(prospect.Person.Id);

                    foreach (var skill in prospect.Skills)
                    {
                        var skillDefinition = _skillDefinitionService.GetSkillDefinition(skill.SkillDefinition.Id);

                        if (skillDefinition == null)
                        {
                            _loggingService.Debug($"Company {company.Name} prospect {prospect.Person.Id} contains unknown skill {skill.SkillDefinition.Id}.");
                        }
                        else
                        {
                            skill.SkillDefinition = skillDefinition;
                        }
                    }
                }

                foreach (var allocation in company.Allocations)
                {
                    if (allocation.Employee.Person == null)
                    {
                        throw new Exception("Person on employee being allocated is null!");
                    }

                    allocation.Employee = company.Employees.FirstOrDefault(e => e.Person.Id == allocation.Employee.Person.Id);

                    allocation.Project = company.Projects.FirstOrDefault(p => p.Id == allocation.Project.Id);
                }
            }
        }

        public IEnumerable<IActionData> GetActionData(byte industryId, Company company)
        {
            var actionData = new List<IActionData>();

            foreach (var actionDataGenerator in _actionDataGenerators)
            {
                var data = actionDataGenerator.GenerateData(industryId, company);

                if (data.Any())
                {
                    actionData.AddRange(data);
                }
            }

            return actionData;
        }

        public void ProcessTransaction(Company company, TransactionType type, decimal amount, string description)
        {
            company.Money += amount;

            var transaction = new Transaction
            {
                Id = Utilities.InvalidId,
                Amount = amount,
                Date = _gameClockService.CurrentDate,
                Description = description,
                EndingBalance = company.Money,
                Type = type
            };

            company.Transactions.Add(transaction);
        }

        public CompaniesExportData ExportData()
        {
            return new CompaniesExportData(Companies);
        }

        public bool CanPerformAction(Company company, ActionType turnActionType)
        {
            var turnAction = GetCompanyAction(company, turnActionType);

            if (turnAction == null)
            {
                _loggingService.Warning($"Company {company.Id} cannot perform {turnActionType} action ({(byte)turnActionType}).");

                return false;
            }

            return turnAction.Count > 0;
        }

        public void RecruitPerson(byte industryId, Company company, Person person)
        {
            var prospect = CreateProspect(industryId, company, person);

            company.Prospects.Add(prospect);

            PerformAction(company, ActionType.RecruitPerson);
        }

        public void InterviewProspect(byte industryId, Company company, Prospect prospect)
        {
            prospect.Accuracy += (byte)((byte.MaxValue - prospect.Accuracy) / 2);

            RegenerateProspect(industryId, prospect.Person, prospect);

            PerformAction(company, ActionType.InterviewProspect);
        }

        public OfferResponse MakeOffer(byte industryId, Company company, Person person, decimal value)
        {
            if (company.Money < value)
            {
                throw new NotEnoughMoneyException(company.Id, value, company.Money);
            }

            var output = _personService.EvaluateOffer(industryId, company, person, value);

            if (output.DidAccept)
            {
                var employee = CreateEmployee(industryId, company, person, value);

                var prospect = company.Prospects.FirstOrDefault(p => p.Person.Id == person.Id);

                if (prospect != null)
                {
                    company.Prospects.Remove(prospect);
                }

                HirePerson(industryId, company, person, value);
            }

            PerformAction(company, ActionType.MakeOffer);

            return output;
        }

        public void FireEmployee(Company company, Employee employee)
        {
            ProcessTransaction(company, TransactionType.FireEmployee, 0, $"Fired {employee.Person.FirstName} {employee.Person.LastName}.");

            company.Employees.Remove(employee);

            var allocations = company.Allocations.Where(a => a.Employee == employee).ToList();

            foreach (var allocation in allocations)
            {
                company.Allocations.Remove(allocation);
            }

            var person = employee.Person;

            var workHistory = person.WorkHistory.FirstOrDefault(wh => !wh.EndDate.HasValue);

            workHistory.EndDate = _gameClockService.CurrentDate;

            workHistory.EndingSalary = employee.Salary;

            PerformAction(company, ActionType.FireEmployee);
        }

        public void AdjustSalary(Company company, Employee employee, decimal salary)
        {
            employee.Salary = salary;

            PerformAction(company, ActionType.AdjustSalary);
        }

        public void AcceptProject(Company company, Project project)
        {
            company.Projects.Add(project);

            project.Status = ProjectStatus.InProgress;

            PerformAction(company, ActionType.AcceptProject);
        }

        public void AdjustAllocation(Company company, Employee employee, Project project, byte percentage, ProjectRole role)
        {
            var allocation = company.Allocations.FirstOrDefault(a => a.Employee == employee && a.Project == project);

            if (percentage == 0)
            {
                company.Allocations.Remove(allocation);

                PerformAction(company, ActionType.AdjustAllocation);

                return;
            }

            if (allocation == null)
            {
                allocation = new Allocation
                {
                    Employee = employee,
                    Project = project
                };

                company.Allocations.Add(allocation);
            }

            allocation.Percent = percentage;

            allocation.Role = role;

            PerformAction(company, ActionType.AdjustAllocation);
        }

        public bool RequestExtension(Company company, Project project)
        {
            var result = _projectService.RequestExtension(project, company.Reputation);

            PerformAction(company, ActionType.RequestExtension);

            return result;
        }

        public void PurchasePerk(Company company, Perk perk)
        {
            if (perk.InitialCost.HasValue)
            {
                var cost = perk.InitialCost.Value;

                ProcessTransaction(company, TransactionType.PurchasePerk, -cost, $"Purchased {perk.Name} perk.");
            }

            company.Perks.Add(new CompanyPerk
            {
                Perk = perk,
                NextPaymentDate = perk.RecurringCostInterval.HasValue ? Utilities.GetDate(_gameClockService.CurrentDate, perk.RecurringCostInterval.Value) : (DateTime?)null
            });

            PerformAction(company, ActionType.PurchasePerk);
        }

        public void SellPerk(Company company, CompanyPerk companyPerk)
        {
            company.Perks.Remove(companyPerk);

            if (companyPerk.Perk.InitialCost.HasValue)
            {
                var value = _perkService.CalculatePerkResellValue(companyPerk.Perk);

                ProcessTransaction(company, TransactionType.SellPerk, value, $"Sold {companyPerk.Perk.Name} perks.");
            }

            PerformAction(company, ActionType.SellPerk);
        }

        public decimal CalculateHappiness(byte industryId, Employee employee)
        {
            if (employee.IsFounder)
            {
                return 100;
            }

            var expected = _compensationService.CalculateCompensationValue(industryId, employee.Person);

            return 100 * Math.Round(Math.Min(employee.Salary / expected, 1m), 2);
        }

        protected override async Task InitializeInternalAsync(CancellationToken cancellationToken = default)
        {
            using (var transaction = new RepositoryTransaction(_companyRepository))
            {
                _companyNames = await _companyRepository.LoadCompanyNamesAsync(cancellationToken);
            }
        }

        protected CompanyAction GetCompanyAction(Company company, ActionType actionType)
        {
            return company.Actions.FirstOrDefault(ca => ca.Action.Id == (byte)actionType);
        }

        protected void PerformAction(Company company, ActionType turnActionType)
        {
            var action = GetCompanyAction(company, turnActionType);

            action.Count--;
        }

        protected Company CreateCompany(byte industryId, decimal initialFunds)
        {
            var founder = _personService.GetUnemployedPerson(industryId);

            var name = _companyNames[industryId]
                .Where(cn => !Companies.Any(c => c.Name == cn))
                .Random();

            return CreateCompany(industryId, name, initialFunds, founder);
        }

        protected Company CreateCompany(byte industryId, string name, decimal initialFunds, Person founder)
        {
            var actions = new List<CompanyAction>();

            foreach (var action in _actionService.Actions)
            {
                var accumulations = new List<CompanyActionAccumulation>(action.Requirements.Count());

                foreach (var requirement in action.Requirements)
                {
                    accumulations.Add(new CompanyActionAccumulation
                    {
                        SkillDefinition = requirement.SkillDefinition
                    });
                }

                actions.Add(new CompanyAction(accumulations)
                {
                    Count = action.DefaultCount,
                    Action = action,
                    IsActive = true
                });
            }

            var output = new Company(new List<CompanyAction>(actions))
            {
                Id = Utilities.InvalidId,
                Name = name,
                Money = initialFunds,
                Status = CompanyStatus.InBusiness
            };

            FoundCompanyWith(industryId, output, founder);

            return output;
        }

        protected Prospect CreateProspect(byte industryId, Company company, Person person)
        {
            var skills = new List<ProspectSkill>();

            foreach (var skill in person.Skills)
            {
                skills.Add(new ProspectSkill
                {
                    SkillDefinition = skill.SkillDefinition
                });
            }

            var output = new Prospect(skills)
            {
                Person = person,
                Accuracy = company.InitialAccuracy
            };

            RegenerateProspect(industryId, person, output);

            return output;
        }

        protected void RegenerateProspect(byte industryId, Person person, Prospect prospect)
        {
            var lowerBoundCoefficient = Utilities.GetRandomDecimal(0, 1);

            var upperBoundCoefficient = 1.0m - lowerBoundCoefficient;

            var rangeCoefficient = 1m - ((decimal)prospect.Accuracy / 255m);

            foreach (var skill in person.Skills)
            {
                var range = (skill.SkillDefinition.Value.Maximum - skill.SkillDefinition.Value.Minimum) * rangeCoefficient;

                var min = skill.Value - (lowerBoundCoefficient * range);

                var max = skill.Value + (upperBoundCoefficient * range);

                if (min < 0)
                {
                    var offset = min * -1m;

                    min += offset;

                    max += offset;

                    if (max > skill.SkillDefinition.Value.Maximum)
                    {
                        max = skill.SkillDefinition.Value.Maximum;
                    }
                }

                var pSkill = prospect.Skills.FirstOrDefault(s => s.SkillDefinition == skill.SkillDefinition);

                pSkill.Value.Minimum = min;

                pSkill.Value.Maximum = max;
            }

            var salaryBase = _compensationService.CalculateCompensationValue(industryId, person);

            var salaryRange = salaryBase * rangeCoefficient;

            var salaryMin = salaryBase - (lowerBoundCoefficient * salaryRange);

            var salaryMax = salaryBase + (upperBoundCoefficient * salaryRange);

            prospect.Salary.Minimum = salaryMin;
                
            prospect.Salary.Maximum = salaryMax;
        }

        protected void FoundCompanyWith(byte industryId, Company company, Person person)
        {
            var founder = new Employee
            {
                Person = person,
                Salary = 0.0m,
                IsFounder = true
            };

            founder.Happiness = CalculateHappiness(industryId, founder);

            company.Employees.Add(founder);

            HirePerson(industryId, company, person, 0.0m);
        }

        protected void HirePerson(byte industryId, Company company, Person person, decimal salary)
        {
            var history = new WorkHistory
            {
                Id = Utilities.InvalidId,
                Company = company,
                StartDate = _gameClockService.CurrentDate,
                StartingSalary = salary
            };

            person.WorkHistory.Add(history);

            UpdateInitialAccuracy(industryId, company);
        }

        protected void UpdateInitialAccuracy(byte industryId, Company company)
        {
            var mandatorySkills = _skillDefinitionService.GetMandatorySkillDefinitions(industryId);

            var accuracy = company.Employees.Average(e => e.Person.Skills.Where(s => mandatorySkills.Contains(s.SkillDefinition)).Average(s => e.Person.GetSkillWeight(s.SkillDefinition.Id)));

            company.InitialAccuracy = (byte)(accuracy * byte.MaxValue);
        }

        protected Employee CreateEmployee(byte industryId, Company company, Person person, decimal salary)
        {
            var output = new Employee
            {
                Person = person,
                Salary = salary,
                IsFounder = false
            };

            output.Happiness = CalculateHappiness(industryId, output);

            company.Employees.Add(output);

            return output;
        }

        protected CompanyServiceData GetData()
        {
            return new CompanyServiceData
            {
                Companies = Companies
            };
        }

        private static IDictionary<byte, IEnumerable<string>> _companyNames;

        private readonly ICompanyRepository _companyRepository;

        private readonly IActionService _actionService;

        private readonly IPersonService _personService;
        
        private readonly ISkillDefinitionService _skillDefinitionService;
        
        private readonly IProjectService _projectService;

        private readonly IPerkService _perkService;

        private readonly ILoggingService _loggingService;

        private readonly IGameClockService _gameClockService;

        private readonly ICompensationService _compensationService;

        private readonly IEnumerable<IActionDataGenerator> _actionDataGenerators;
    }
}