﻿using Bizio.Core.Data.Configuration;
using Bizio.Core.Model.People;
using Bizio.Core.Services;
using Bizio.Core.Services.Bases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Bizio.Services
{
    public class CompensationService : ServiceBase<CompensationService>, ICompensationService
    {
        private readonly IConfigurationService _configurationService;
        private readonly ISkillDefinitionService _skillDefinitionService;

        public CompensationService(
            IConfigurationService configurationService,
            ISkillDefinitionService skillDefinitionService)
        {
            _configurationService = configurationService;

            _skillDefinitionService = skillDefinitionService;
        }

        public decimal CalculateCompensationValue(byte industryId, Person person) => CalculateCompensationValue(industryId, person.Skills, person);

        public decimal CalculateCompensationValue(byte industryId, IEnumerable<Skill> skills, Person person)
        {
            var baseSalary = _configurationService.GetDecimalValue(null, ConfigurationKey.BaseSalary);

            var mandatorySkillSalaryWeight = _configurationService.GetDecimalValue(null, ConfigurationKey.MandatorySkillSalaryWeight);

            var optionalSkillSalaryWeight = _configurationService.GetDecimalValue(null, ConfigurationKey.OptionalSkillSalaryWeight);

            var salaryMagnitude = _configurationService.GetDecimalValue(null, ConfigurationKey.SalaryCalculationMagnitude);

            decimal GetSkillWeight(Skill s)
            {
                var range = s.SkillDefinition.Value;

                return (s.Value - range.Minimum) / (range.Maximum - range.Minimum);
            }

            var mandatorySkillTotal = skills
                .Where(s => _skillDefinitionService.GetMandatorySkillDefinitions(industryId).Contains(s.SkillDefinition))
                .Sum(s => GetSkillWeight(s));

            var mandatorySkillSalary = salaryMagnitude * mandatorySkillTotal * mandatorySkillSalaryWeight;

            var optionalSkillTotal = skills
                .Where(s => !_skillDefinitionService.GetMandatorySkillDefinitions(industryId).Contains(s.SkillDefinition))
                .Sum(s => GetSkillWeight(s));

            var optionalSkillSalary = salaryMagnitude * optionalSkillTotal * optionalSkillSalaryWeight;

            var annualSalary = baseSalary + mandatorySkillSalary + optionalSkillSalary;

            var weeklySalary = Math.Round(annualSalary / 52);

            return weeklySalary;
        }

        protected async override Task InitializeInternalAsync(CancellationToken cancellationToken = default)
        {
            await _configurationService.InitializeAsync(cancellationToken);
        }
    }
}
