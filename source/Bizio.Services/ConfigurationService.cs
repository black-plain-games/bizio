﻿using Bizio.Core.Data.Configuration;
using Bizio.Core.Repositories;
using Bizio.Core.Services;
using Bizio.Core.Services.Bases;
using Bizio.Core.Utilities;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Bizio.Services
{
    public class ConfigurationService : ServiceBase<ConfigurationService>, IConfigurationService
    {
        public ConfigurationService(IConfigurationRepository configurationRepository)
            : base()
        {
            _configurationRepository = configurationRepository;
        }

        public string GetStringValue(int? gameId, ConfigurationKey key)
        {
            if (gameId.HasValue)
            {
                var gameConfiguration = GetGameConfigurationEntry(gameId.Value, key);

                if (!string.IsNullOrWhiteSpace(gameConfiguration))
                {
                    return gameConfiguration;
                }
            }

            var globalConfiguration = GetGlobalConfigurationEntry(key);

            if (!string.IsNullOrWhiteSpace(globalConfiguration))
            {
                return globalConfiguration;
            }

            throw new NullConfigurationException(key);
        }

        public string GetStringValue(int? gameId, ConfigurationKey key, string fallbackValue)
        {
            if (gameId.HasValue)
            {
                var gameConfiguration = GetGameConfigurationEntry(gameId.Value, key);

                if (!string.IsNullOrWhiteSpace(gameConfiguration))
                {
                    return gameConfiguration;
                }
            }

            var globalConfiguration = GetGlobalConfigurationEntry(key);

            if (!string.IsNullOrWhiteSpace(globalConfiguration))
            {
                return globalConfiguration;
            }

            return fallbackValue;
        }

        public int GetIntValue(int? gameId, ConfigurationKey key)
        {
            var value = GetStringValue(gameId, key);

            if (int.TryParse(value, out var output))
            {
                return output;
            }

            throw new ConfigurationTypeException(key, value, typeof(int));
        }

        public int GetIntValue(int? gameId, ConfigurationKey key, int fallbackValue)
        {
            var value = GetStringValue(gameId, key, null);

            if (value == null)
            {
                return fallbackValue;
            }

            if (int.TryParse(value, out var output))
            {
                return output;
            }

            return fallbackValue;
        }

        public byte GetByteValue(int? gameId, ConfigurationKey key)
        {
            var value = GetStringValue(gameId, key);

            if (byte.TryParse(value, out var output))
            {
                return output;
            }

            throw new ConfigurationTypeException(key, value, typeof(byte));
        }

        public byte GetByteValue(int? gameId, ConfigurationKey key, byte fallbackValue)
        {
            var value = GetStringValue(gameId, key, null);

            if (value == null)
            {
                return fallbackValue;
            }

            if (byte.TryParse(value, out var output))
            {
                return output;
            }

            return fallbackValue;
        }

        public short GetShortValue(int? gameId, ConfigurationKey key)
        {
            var value = GetStringValue(gameId, key);

            if (short.TryParse(value, out var output))
            {
                return output;
            }

            throw new ConfigurationTypeException(key, value, typeof(short));
        }

        public short GetShortValue(int? gameId, ConfigurationKey key, short fallbackValue)
        {
            var value = GetStringValue(gameId, key, null);

            if (value == null)
            {
                return fallbackValue;
            }

            if (short.TryParse(value, out var output))
            {
                return output;
            }

            return fallbackValue;
        }

        public decimal GetDecimalValue(int? gameId, ConfigurationKey key)
        {
            var value = GetStringValue(gameId, key);

            if (decimal.TryParse(value, out var output))
            {
                return output;
            }

            throw new ConfigurationTypeException(key, value, typeof(decimal));
        }

        public decimal GetDecimalValue(int? gameId, ConfigurationKey key, decimal fallbackValue)
        {
            var value = GetStringValue(gameId, key, null);

            if (value == null)
            {
                return fallbackValue;
            }

            if (decimal.TryParse(value, out var output))
            {
                return output;
            }

            return fallbackValue;
        }

        public bool GetBooleanValue(int? gameId, ConfigurationKey key)
        {
            var value = GetStringValue(gameId, key);

            if (bool.TryParse(value, out var output))
            {
                return output;
            }

            throw new ConfigurationTypeException(key, value, typeof(bool));
        }

        public bool GetBooleanValue(int? gameId, ConfigurationKey key, bool fallbackValue)
        {
            var value = GetStringValue(gameId, key, null);

            if (value == null)
            {
                return fallbackValue;
            }

            if (bool.TryParse(value, out var output))
            {
                return output;
            }

            return fallbackValue;
        }

        public void SetValue(int gameId, ConfigurationKey key, object value)
        {
            using (var transaction = new RepositoryTransaction(_configurationRepository))
            {
                _configurationRepository.SetValue(gameId, key, $"{value}");
            }
        }

        protected override async Task InitializeInternalAsync(CancellationToken cancellationToken = default)
        {
            await Task.Yield();

            cancellationToken.ThrowIfCancellationRequested();

            if (_globalWriteLock == null)
            {
                _globalWriteLock = new object();
            }

            if (_gameWriteLock == null)
            {
                _gameWriteLock = new object();
            }
        }

        protected string GetGlobalConfigurationEntry(ConfigurationKey key)
        {
            lock (_globalWriteLock)
            {
                if (_globalConfiguration == null)
                {
                    InitializeGlobalConfiguration();
                }
            }

            if (_globalConfiguration.ContainsKey(key))
            {
                return _globalConfiguration[key];
            }

            return null;
        }

        protected string GetGameConfigurationEntry(int gameId, ConfigurationKey key)
        {
            lock (_gameWriteLock)
            {
                if (_gameConfigurations == null)
                {
                    _gameConfigurations = new Dictionary<int, IDictionary<ConfigurationKey, string>>();
                }

                if (!_gameConfigurations.ContainsKey(gameId))
                {
                    InitializeGameConfiguration(gameId);
                }
            }

            if (_gameConfigurations.ContainsKey(gameId) && _gameConfigurations[gameId].ContainsKey(key))
            {
                return _gameConfigurations[gameId][key];
            }

            return null;
        }

        protected void InitializeGlobalConfiguration()
        {
            using (var transaction = new RepositoryTransaction(_configurationRepository))
            {
                _globalConfiguration = _configurationRepository.GetGlobalConfigurationEntries();
            }
        }

        protected void InitializeGameConfiguration(int gameId)
        {
            IDictionary<ConfigurationKey, string> configurations = null;

            using (var transaction = new RepositoryTransaction(_configurationRepository))
            {
                configurations = _configurationRepository.GetGameConfigurationEntries(gameId);
            }

            _gameConfigurations.Add(gameId, configurations);
        }

        private static object _globalWriteLock;

        private static object _gameWriteLock;

        private static IDictionary<ConfigurationKey, string> _globalConfiguration;

        private static IDictionary<int, IDictionary<ConfigurationKey, string>> _gameConfigurations;

        private readonly IConfigurationRepository _configurationRepository;
    }
}