﻿using Bizio.Core.Data.Contracts;
using Bizio.Core.Data.Exports;
using Bizio.Core.Repositories;
using Bizio.Core.Services;
using Bizio.Core.Services.Bases;
using Bizio.Core.Services.Data;
using Bizio.Core.Utilities;
using System;

namespace Bizio.Services
{
    public class GameClockService : ServiceBase<GameClockService>, IGameClockService
    {
        public DateTime CurrentDate { get; private set; }

        public DateTime StartDate { get; private set; }

        public short CurrentTurn { get; private set; }

        public byte WeeksPerTurn { get; private set; }

        public GameClockService(IGameClockRepository gameClockRepository)
            : base()
        {
            _gameClockRepository = gameClockRepository;
        }

        public void StartNewGame(NewGameDataInternal data)
        {
            StartDate = data.InputData.StartDate;

            WeeksPerTurn = data.InputData.WeeksPerTurn;

            CurrentTurn = 1;

            CurrentDate = StartDate;
        }

        public int SaveGame(int gameId)
        {
            var serviceData = GenerateData();

            using (var transaction = new RepositoryTransaction(_gameClockRepository))
            {
                _gameClockRepository.SaveGame(gameId, serviceData);
            }

            return gameId;
        }

        public void LoadGame(LoadGameData data)
        {
            GameClockServiceData serviceData = null;

            using (var transaction = new RepositoryTransaction(_gameClockRepository))
            {
                serviceData = _gameClockRepository.LoadGame(data.UserId, data.GameId);
            }

            CurrentTurn = serviceData.CurrentTurn;

            StartDate = serviceData.StartDate;

            WeeksPerTurn = serviceData.WeeksPerTurn;

            CurrentDate = Utilities.GetDate(StartDate, WeeksPerTurn, CurrentTurn - 1);
        }

        public void ProcessTurn()
        {
            CurrentTurn++;

            CurrentDate = Utilities.GetDate(StartDate, WeeksPerTurn, CurrentTurn - 1);
        }

        public GameClockExportData ExportData()
        {
            return new GameClockExportData
            {
                StartDate = StartDate,
                CurrentDate = CurrentDate,
                CurrentTurn = CurrentTurn,
                WeeksPerTurn = WeeksPerTurn
            };
        }

        public int DateToTurnDifference(DateTime date)
        {
            return (date - CurrentDate).Days / (WeeksPerTurn * 7);
        }

        public DateTime CalculateDate(int turns) => CalculateDate(CurrentDate, turns);

        public DateTime CalculateDate(DateTime startDate, int turns)
        {
            return startDate.AddDays(turns * WeeksPerTurn * 7);
        }

        protected GameClockServiceData GenerateData()
        {
            return new GameClockServiceData
            {
                CurrentTurn = CurrentTurn,
                StartDate = StartDate,
                WeeksPerTurn = WeeksPerTurn
            };
        }

        private readonly IGameClockRepository _gameClockRepository;
    }
}