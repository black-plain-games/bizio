﻿using Bizio.Core.Services;
using Bizio.Core.Services.Bases;

namespace Bizio.Services
{
    public class GameDataService : ServiceBase<GameDataService>, IGameDataService
    {
        public int? GameId { get; set; }
    }
}