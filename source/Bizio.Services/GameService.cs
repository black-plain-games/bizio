﻿using Bizio.Core.Data.Contracts;
using Bizio.Core.Data.Exports;
using Bizio.Core.Data.Exports.Statics;
using Bizio.Core.Model.Companies;
using Bizio.Core.Model.Game;
using Bizio.Core.Repositories;
using Bizio.Core.Services;
using Bizio.Core.Services.Bases;
using Bizio.Core.Utilities;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Bizio.Services
{
    public class GameService : ServiceBase<GameService>, IGameService
    {
        public string GameName { get; private set; }

        public Industry CurrentIndustry { get; private set; }

        public GameStatus Status { get; set; }

        public GameService(IGameRepository gameRepository,
            IDifficultyLevelRepository difficultyLevelRepository,
            IPersonService personService,
            ICompanyService companyService,
            IActionService actionService,
            IPerkService perkService,
            IProjectService projectService,
            IGameClockService gameClockService,
            IIndustryService industryService,
            IGameDataService gameDataService,
            IEnumerable<IActionHandler> turnActionHandlers,
            IEnumerable<ITurnProcessor> turnProcessors)
            : base()
        {
            _gameRepository = gameRepository;

            _difficultyLevelRepository = difficultyLevelRepository;

            _personService = personService;

            _companyService = companyService;

            _actionService = actionService;

            _perkService = perkService;

            _projectService = projectService;

            _gameClockService = gameClockService;

            _turnActionHandlers = turnActionHandlers;

            _turnProcessors = turnProcessors;

            _industryService = industryService;
            this._gameDataService = gameDataService;
        }

        public void StartNewGame(NewGameDataInternal data)
        {
            var gameId = Utilities.InvalidId;

            using (var transaction = new RepositoryTransaction(_gameRepository))
            {
                gameId = _gameRepository.Create(data.InputData);
            }

            _gameDataService.GameId = data.GameId = gameId;

            CurrentIndustry = _industryService.GetIndustry(data.InputData.IndustryId);

            _gameClockService.StartNewGame(data);

            _personService.StartNewGame(data);

            _companyService.StartNewGame(data);

            _projectService.StartNewGame(data);

            Status = GameStatus.Playing;

            SaveGame();
        }

        public int SaveGame()
        {
            var gameId = _gameDataService.GameId.Value;

            using (var transaction = new RepositoryTransaction(_gameRepository))
            {
                _gameRepository.SaveGame(gameId, new GameServiceData
                {
                    Status = Status
                });
            }

            _gameClockService.SaveGame(gameId);

            _companyService.SaveCompanies(gameId);

            _personService.SaveGame(gameId);

            _projectService.SaveGame(gameId);

            _companyService.SaveGame(gameId);

            return gameId;
        }

        public void LoadGame(LoadGameData data)
        {
            GameServiceData loadData = null;

            using (var transaction = new RepositoryTransaction(_gameRepository))
            {
                loadData = _gameRepository.LoadGame(data.UserId, data.GameId);
            }

            _gameDataService.GameId = data.GameId;

            GameName = loadData.GameName;

            Status = loadData.Status;

            CurrentIndustry = _industryService.GetIndustry(loadData.IndustryId);

            _gameClockService.LoadGame(data);

            _personService.LoadGame(data);

            _companyService.LoadGame(data);

            _projectService.LoadGame(data);

            _personService.SynchronizeData(_companyService.GetCompany);

            _companyService.SynchronizeData(CurrentIndustry.Id);
        }

        public void ProcessTurn(IEnumerable<IActionData> events)
        {
            var companyMessages = ProcessTurn(_companyService.UserCompany, events);

            _companyService.UserCompany.AddMessages(companyMessages);

            foreach (var company in _companyService.Companies.Where(c => c.Status == CompanyStatus.InBusiness))
            {
                if (company != _companyService.UserCompany)
                {
                    var companyEvents = _companyService.GetActionData(CurrentIndustry.Id, company);

                    var messages = ProcessTurn(company, companyEvents);

                    // Don't save messages for AI
                    if (company.UserId.HasValue)
                    {
                        company.AddMessages(messages);
                    }
                }
            }

            var externalMessages = FinalizeTurn();

            _companyService.UserCompany.AddMessages(externalMessages);
        }

        public IEnumerable<Game> Retrieve(int userId)
        {
            using (var transaction = new RepositoryTransaction(_gameRepository))
            {
                return _gameRepository.Retrieve(userId);
            }
        }

        public GameExportData ExportData()
        {
            return new GameExportData
            {
                GameId = _gameDataService.GameId.Value,
                IndustryId = CurrentIndustry.Id,
                GameClockData = _gameClockService.ExportData(),
                ProjectData = _projectService.ExportData(),
                PeopleData = _personService.ExportData(),
                CompanyData = _companyService.ExportData()
            };
        }

        public async Task<StaticExportData> ExportStaticDataAsync(CancellationToken cancellationToken = default)
        {
            var actionsDataTask = _actionService.ExportStaticDataAsync(cancellationToken);
            var industriesDataTask = _industryService.ExportStaticDataAsync(cancellationToken);
            var peopleDataTask = _personService.ExportStaticDataAsync(cancellationToken);
            var perksDataTask = _perkService.ExportStaticDataAsync(cancellationToken);
            var projectDataTask = _projectService.ExportStaticDataAsync(cancellationToken);
            var difficultyLevelsTask = RetrieveDifficultyLevelsAsync(cancellationToken);

            await Task.WhenAll(actionsDataTask, industriesDataTask, peopleDataTask, perksDataTask, projectDataTask, difficultyLevelsTask);

            return new StaticExportData
            {
                ActionsData = await actionsDataTask,
                CompaniesData = await industriesDataTask, // TODO: RENAME THIS PROPERTY
                PeopleData = await peopleDataTask,
                PerksData = await perksDataTask,
                ProjectData = await projectDataTask,
                GameData = new StaticGameExportData
                {
                    DifficultyLevels = await difficultyLevelsTask
                }
            };
        }

        protected override async Task InitializeInternalAsync(CancellationToken cancellationToken = default)
        {
            await Task.WhenAll(
                _industryService.InitializeAsync(cancellationToken),
                _personService.InitializeAsync(cancellationToken),
                _actionService.InitializeAsync(cancellationToken),
                _companyService.InitializeAsync(cancellationToken),
                _projectService.InitializeAsync(cancellationToken),
                _perkService.InitializeAsync(cancellationToken),
                _gameClockService.InitializeAsync(cancellationToken));
        }

        private async Task<IEnumerable<DifficultyLevel>> RetrieveDifficultyLevelsAsync(CancellationToken cancellationToken = default)
        {
            using (var transaction = new RepositoryTransaction(_difficultyLevelRepository))
            {
                return await _difficultyLevelRepository.RetrieveAsync(cancellationToken);
            }
        }

        private IEnumerable<CompanyMessage> ProcessTurn(Company company, IEnumerable<IActionData> actionData)
        {
            var output = new List<CompanyMessage>();

            foreach (var turnActionData in actionData)
            {
                var handler = _turnActionHandlers.FirstOrDefault(ta => ta.Type == turnActionData.ActionType);

                if (handler == null)
                {
                    throw new ActionException(turnActionData, $"Unknown turn action {turnActionData.ActionType}.");
                }

                var info = handler.CanPerformEvent(company, turnActionData);

                if (!info.ShouldPerformAction)
                {
                    continue;
                }

                if (!info.CanPerformAction)
                {
                    throw new ActionException(turnActionData, info.Message, info.Data);
                }

                var response = handler.ProcessEvent(CurrentIndustry.Id, company, turnActionData);

                if (response != null)
                {
                    output.Add(response);
                }
            }

            return output;
        }

        private IEnumerable<CompanyMessage> FinalizeTurn()
        {
            var output = new List<CompanyMessage>();

            foreach (var processor in _turnProcessors.OrderBy(tp => tp.Index))
            {
                output.AddRange(processor.ProcessTurn(CurrentIndustry.Id, _gameDataService.GameId.Value));
            }

            return output;
        }

        private readonly IGameRepository _gameRepository;
        private readonly IDifficultyLevelRepository _difficultyLevelRepository;

        private readonly IPersonService _personService;
        private readonly IActionService _actionService;
        private readonly ICompanyService _companyService;
        private readonly IProjectService _projectService;
        private readonly IPerkService _perkService;
        private readonly IGameClockService _gameClockService;
        private readonly IIndustryService _industryService;
        private readonly IGameDataService _gameDataService;
        private readonly IEnumerable<IActionHandler> _turnActionHandlers;

        private readonly IEnumerable<ITurnProcessor> _turnProcessors;
    }
}