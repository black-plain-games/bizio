﻿using Bizio.Core.Data.Exports.Statics;
using Bizio.Core.Model.Companies;
using Bizio.Core.Repositories;
using Bizio.Core.Services;
using Bizio.Core.Services.Bases;
using Bizio.Core.Utilities;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Bizio.Services
{
    public class IndustryService : ServiceBase<IndustryService>, IIndustryService
    {
        public IEnumerable<Industry> Industries { get; private set; }

        public IndustryService(IIndustryRepository industryRepository)
        {
            _industryRepository = industryRepository;
        }

        public Industry GetIndustry(byte industryId) => Industries.FirstOrDefault(i => i.Id == industryId);

        public async Task<StaticIndustriesExportData> ExportStaticDataAsync(CancellationToken cancellationToken = default)
        {
            await Task.Yield();

            cancellationToken.ThrowIfCancellationRequested();

            return new StaticIndustriesExportData
            {
                Industries = Industries
            };
        }

        protected override async Task InitializeInternalAsync(CancellationToken cancellationToken = default)
        {
            using (var transaction = new RepositoryTransaction(_industryRepository))
            {
                Industries = await _industryRepository.LoadIndustriesAsync(cancellationToken);
            }
        }

        private readonly IIndustryRepository _industryRepository;
    }
}