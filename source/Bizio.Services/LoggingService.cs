﻿using Bizio.Core.Data.Logging;
using Bizio.Core.Repositories;
using Bizio.Core.Services;
using Bizio.Core.Services.Bases;
using Bizio.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bizio.Services
{
    public sealed class LoggingService : ServiceBase<LoggingService>, ILoggingService
    {
        public IEnumerable<Message> Messages => _messages;

        public LoggingService(
            ILoggingRepository loggingRepository,
            IGameClockService gameClockService,
            IGameDataService gameDataService)
            : base()
        {
            _messages = new List<Message>();

            _loggingRepository = loggingRepository;

            _gameClockService = gameClockService;

            _gameDataService = gameDataService;
        }

        public void Subscribe(IMessageSubscriber subscriber, bool receivePreviousMessages)
        {
            _subscribers.Add(subscriber);

            if (receivePreviousMessages)
            {
                foreach (var notification in _messages)
                {
                    subscriber.ReceiveMessage(this, notification);
                }
            }
        }

        public void Unsubscribe(IMessageSubscriber subscriber)
        {
            _subscribers.Remove(subscriber);
        }

        private string GetStackTrace()
        {
#if DEBUG
            return Environment.StackTrace;
#else
            return "Stack trace omitted in Release mode";
#endif
        }

        public void Debug(string format, params object[] args) => Notify(MessageLevel.Debug, GetStackTrace(), format, args);

        public void Warning(string format, params object[] args) => Notify(MessageLevel.Warning, GetStackTrace(), format, args);

        public void Exception(string format, params object[] args) => Notify(MessageLevel.Exception, GetStackTrace(), format, args);

        public void Exception(Exception err)
        {
            var builder = new StringBuilder();

            builder.AppendFormat("Source: {0}", err.Source);

            builder.AppendLine();

            builder.AppendFormat("Target Site: {0}", err.TargetSite);

            builder.AppendLine();

            var currentException = err;

            while (currentException != null)
            {
                builder.AppendFormat("Exception: {0}", currentException.Message);

                builder.AppendLine();

                currentException = currentException.InnerException;
            }

            Notify(MessageLevel.Exception, err.StackTrace, builder.ToString());
        }

        public void Save(bool clear)
        {
            using (var transaction = new RepositoryTransaction(_loggingRepository))
            {
                _loggingRepository.SaveNotifications(_messages);
            }

            if (clear)
            {
                lock (_messagesWriteLock)
                {
                    _messages.Clear();
                }
            }
        }

        private void Notify(MessageLevel level, string stackTrace, string format, params object[] args)
        {
            var message = new Message
            {
                Level = level,
                TimeStamp = DateTime.Now,
                GameId = _gameDataService.GameId ?? 0,
                CurrentTurn = _gameClockService.CurrentTurn,
                StackTrace = stackTrace,
                Text = (args != null && args.Any()) ? string.Format(format, args) : format
            };

            lock (_messagesWriteLock)
            {
                _messages.Add(message);
            }

            Console.WriteLine(message.Text);

            foreach (var subscriber in _subscribers)
            {
                subscriber.ReceiveMessage(this, message);
            }
        }

        private readonly ILoggingRepository _loggingRepository;

        private readonly IGameClockService _gameClockService;
        private readonly IGameDataService _gameDataService;
        private readonly ICollection<Message> _messages;

        private readonly object _messagesWriteLock = new object();

        private static readonly ICollection<IMessageSubscriber> _subscribers = new List<IMessageSubscriber>();
    }
}