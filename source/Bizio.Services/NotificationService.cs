﻿using Bizio.Core.Data;
using Bizio.Core.Repositories;
using Bizio.Core.Services;
using Bizio.Core.Services.Bases;
using Bizio.Core.Utilities;
using System.Collections.Generic;

namespace Bizio.Services
{
    public class NotificationService : ServiceBase<NotificationService>, INotificationService
    {
        public NotificationService(INotificationRepository notificationRepository)
            : base()
        {
            _notificationRepository = notificationRepository;
        }

        public IEnumerable<Notification> Retrieve(int minimumId)
        {
            using (var transaction = new RepositoryTransaction(_notificationRepository))
            {
                return _notificationRepository.Retrieve(minimumId);
            }
        }

        private readonly INotificationRepository _notificationRepository;
    }
}