﻿using Bizio.Core.Data.Configuration;
using Bizio.Core.Data.Exports.Statics;
using Bizio.Core.Model.Companies;
using Bizio.Core.Model.People;
using Bizio.Core.Model.Perks;
using Bizio.Core.Repositories;
using Bizio.Core.Services;
using Bizio.Core.Services.Bases;
using Bizio.Core.Utilities;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Bizio.Services
{
    public class PerkService : ServiceBase<PerkService>, IPerkService
    {
        public IEnumerable<Perk> Perks { get; private set; }

        public PerkService(IPerkRepository perkRepository,
            IConfigurationService configurationService,
            IGameClockService gameClockService,
            ILoggingService loggingService,
            IEnumerable<IAttributeEvaluator<Company>> companyAttributeEvaluators,
            IEnumerable<IAttributeEvaluator<Employee>> employeeAttributeEvaluators,
            IEnumerable<IAttributeEvaluator<Person>> personAttributeEvaluators)
            : base()
        {
            _perkRepository = perkRepository;

            _configurationService = configurationService;

            _gameClockService = gameClockService;

            _loggingService = loggingService;

            _companyAttributeEvaluators = companyAttributeEvaluators;

            _employeeAttributeEvaluators = employeeAttributeEvaluators;

            _personAttributeEvaluators = personAttributeEvaluators;
        }

        public Perk GetPerk(int id)
        {
            return Perks.FirstOrDefault(p => p.Id == id);
        }

        public bool CanPurchasePerk(Company company, Perk perk)
        {
            if (company.Money < perk.InitialCost)
            {
                return false;
            }

            if (!DoesPerkApplyTo(perk, company))
            {
                _loggingService.Debug($"Perk {perk.Name} does not apply to company {company.Name}.");

                return false;
            }

            var companyPerks = company.Perks.Where(p => p.Perk == perk);

            if (companyPerks.Count() == perk.MaxCount)
            {
                _loggingService.Debug($"Company {company.Name} would own too many of perk {perk.Name}.");

                return false;
            }

            return true;
        }

        public bool DoesPerkApplyTo(Perk perk, Person person)
        {
            return EvaluatePerk(PerkTargetType.Person, perk, person, _personAttributeEvaluators);
        }

        public bool DoesPerkApplyTo(Perk perk, Employee employee)
        {
            return EvaluatePerk(PerkTargetType.Employee, perk, employee, _employeeAttributeEvaluators);
        }

        public bool DoesPerkApplyTo(Perk perk, Company company)
        {
            return EvaluatePerk(PerkTargetType.Company, perk, company, _companyAttributeEvaluators);
        }

        public async Task<StaticPerksExportData> ExportStaticDataAsync(CancellationToken cancellationToken = default)
        {
            await Task.Yield();

            cancellationToken.ThrowIfCancellationRequested();

            return new StaticPerksExportData
            {
                Perks = Perks
            };
        }

        public decimal CalculatePerkResellValue(Perk perk)
        {
            if (!perk.InitialCost.HasValue)
            {
                return decimal.Zero;
            }

            var perkResellRate = _configurationService.GetDecimalValue(null, ConfigurationKey.PerkResellFactor);

            return perk.InitialCost.Value * perkResellRate;
        }

        public decimal CalculatePerkRecurringCost(CompanyPerk perk, int turns)
        {
            if (!perk.NextPaymentDate.HasValue)
            {
                return decimal.Zero;
            }

            var nextPaymentDate = perk.NextPaymentDate.Value;

            var futureTurn = _gameClockService.CalculateDate(turns);

            var cost = 0m;

            while (nextPaymentDate <= futureTurn)
            {
                cost += perk.Perk.RecurringCost.Value;

                nextPaymentDate = _gameClockService.CalculateDate(nextPaymentDate, perk.Perk.RecurringCostInterval.Value);
            }

            return cost;
        }

        protected override async Task InitializeInternalAsync(CancellationToken cancellationToken = default)
        {
            using (var transaction = new RepositoryTransaction(_perkRepository))
            {
                Perks = await _perkRepository.LoadPerksAsync(cancellationToken);
            }
        }

        protected bool EvaluatePerk<T>(PerkTargetType taretType, Perk perk, T target, IEnumerable<IAttributeEvaluator<T>> evaluators)
        {
            var conditions = perk.Conditions.Where(c => c.Target == taretType);

            if (!conditions.Any())
            {
                return true;
            }

            var doesPerkApply = false;

            foreach (var condition in conditions)
            {
                var evaluator = evaluators.FirstOrDefault(e => e.Attribute == condition.Attribute);

                if (evaluator == null)
                {
                    continue;
                }

                var value = evaluator.Evaluate(target);

                doesPerkApply = CompareValues(value, condition.Value, condition.Comparison);

                if (!doesPerkApply)
                {
                    break;
                }
            }

            return doesPerkApply;
        }

        protected bool CompareValues(decimal lhs, decimal rhs, PerkConditionComparisonType comparisonType)
        {
            var result = false;

            switch (comparisonType)
            {
                case PerkConditionComparisonType.Equal:
                    result = lhs == rhs;
                    break;
                case PerkConditionComparisonType.GreaterThan:
                    result = lhs > rhs;
                    break;
                case PerkConditionComparisonType.GreaterThanEqualTo:
                    result = lhs >= rhs;
                    break;
                case PerkConditionComparisonType.LessThan:
                    result = lhs < rhs;
                    break;
                case PerkConditionComparisonType.LessThanEqualTo:
                    result = lhs <= rhs;
                    break;
                case PerkConditionComparisonType.NotEqual:
                    result = lhs != rhs;
                    break;
            }

            return result;
        }

        private readonly IEnumerable<IAttributeEvaluator<Company>> _companyAttributeEvaluators;

        private readonly IEnumerable<IAttributeEvaluator<Person>> _personAttributeEvaluators;

        private readonly IEnumerable<IAttributeEvaluator<Employee>> _employeeAttributeEvaluators;

        private readonly IPerkRepository _perkRepository;

        private readonly IConfigurationService _configurationService;

        private readonly IGameClockService _gameClockService;

        private readonly ILoggingService _loggingService;
    }
}