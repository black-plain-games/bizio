﻿using Bizio.Core.Data.Configuration;
using Bizio.Core.Data.Contracts;
using Bizio.Core.Model.Companies;
using Bizio.Core.Model.People;
using Bizio.Core.Repositories;
using Bizio.Core.Services;
using Bizio.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Bizio.Services
{
    public class PersonService : IPersonService
    {
        public PersonService(
            IBizioRepository bizioRepository,
            IConfigurationService configurationService,
            ILoggingService loggingService,
            IGameClockService gameClockService,
            ISkillDefinitionService skillDefinitionService,
            IEnumerable<IOfferEvaluator> offerEvaluators
            )
            : base()
        {
            _bizioRepository = bizioRepository;

            _configurationService = configurationService;

            _loggingService = loggingService;

            _gameClockService = gameClockService;
            
            _skillDefinitionService = skillDefinitionService;

            _offerEvaluators = offerEvaluators;
        }

        public void StartNewGame(NewGameDataInternal data)
        {
            // this shouldn't exist
            //var initialPersonPoolSize = _configurationService.GetIntValue(null, ConfigurationKey.InitialPersonPoolSize);

            //People = new List<Person>(initialPersonPoolSize);

            //GeneratePeople(data.InputData.IndustryId, initialPersonPoolSize);

            //data.UserPerson = CreateFounder(data.InputData.IndustryId, data.InputData.Founder);
        }

        public IEnumerable<Person> GeneratePeople(Guid industryId, int count)
        {
            var minimumOptionalSkillCount = _configurationService.GetIntValue(null, ConfigurationKey.MinimumOptionalSkillCount);

            var maximumOptionalSkillCount = _configurationService.GetIntValue(null, ConfigurationKey.MaximumOptionalSkillCount);

            var output = new List<Person>();

            for (var index = 0; index < count; index++)
            {
                var person = GeneratePerson(industryId, minimumOptionalSkillCount, maximumOptionalSkillCount);

                output.Add(person);
            }

            _loggingService.Debug($"{count} people generated.");

            return output;
        }

        public async Task<Person> CreateFounderAsync(Guid industryId, CreatePersonData data, CancellationToken cancellationToken = default)
        {
            if (data == null)
            {
                _loggingService.Exception("Cannot create person from null data.");

                return null;
            }

            var bizioData = await _bizioRepository.LoadAsync(false);

            var skills = new List<Skill>();

            foreach ((var skillDefinitionId, var value) in data.Skills)
            {
                if (!bizioData.SkillDefinitions.TryGetValue(skillDefinitionId, out var match))
                {
                    _loggingService.Exception($"Unable to find skill definition for {skillDefinitionId}.");

                    continue;
                }

                var v = value;

                if (v > match.Value.Maximum)
                {
                    _loggingService.Warning($"Provided value ({v}) for skill {skillDefinitionId} exceeds the maximum. Clamping will occur.");

                    v = match.Value.Maximum;
                }
                else if (v < match.Value.Minimum)
                {
                    _loggingService.Warning($"Provided value ({v}) for skill {skillDefinitionId} is below the minimum. Clamping will occur.");

                    v = match.Value.Minimum;
                }

                skills.Add(GenerateSkill(match, value));
            }

            foreach (var skillDefinitionId in bizioData.Industries[industryId].MandatorySkills)
            {
                if (!skills.Any(s => s.SkillDefinitionId == skillDefinitionId))
                {
                    _loggingService.Warning($"Founder '{data.FirstName} {data.LastName}' missing mandatory skill {skillDefinitionId} '{bizioData.SkillDefinitions[skillDefinitionId].Name}'.");

                    skills.Add(GenerateSkill(bizioData.SkillDefinitions[skillDefinitionId]));
                }
            }


            if (!bizioData.Personalities.TryGetValue(data.PersonalityId, out Personality personality))
            {
                _loggingService.Warning($"Unable to find personality {data.PersonalityId}. Using default.");

                personality = bizioData.Personalities.Values.FirstOrDefault();
            }

            return new Person
            {
                Id = Guid.NewGuid(),
                FirstName = data.FirstName,
                LastName = data.LastName,
                Gender = data.Gender,
                Birthday = data.Birthday,
                PersonalityId = personality.Id,
                Skills = skills,
                WorkHistory = [],
                RetirementDate = data.Birthday.AddYears(200) // This is a magic number that could be broken
            };
        }

        // you're here

        public Person GetUnemployedPerson(Guid currentIndustryId) => GetUnemployedPerson(currentIndustryId, Enumerable.Empty<int>());

        public Person GetUnemployedPerson(Guid industryId, IEnumerable<int> personIds)
        {
            var output = _people.Where(p => !personIds.Contains(p.Id) && IsUnemployed(p))
                                .Random();

            if (output == null)
            {
                var batchSize = _configurationService.GetIntValue(null, ConfigurationKey.PersonGenerationBatchSize);

                GeneratePeople(industryId, batchSize);

                output = _people.Where(p => !personIds.Contains(p.Id) && IsUnemployed(p))
                                .Random();
            }

            return output;
        }

        public Person GetPerson(Guid id)
        {
            return _people.FirstOrDefault(p => p.Id == id);
        }

        public OfferResponse EvaluateOffer(Guid industryId, Company company, Person person, decimal amount)
        {
            var reasons = new List<OfferResponseReason>();

            foreach (var evaluator in _offerEvaluators)
            {
                var reason = evaluator.Evaluate(industryId, company, person, amount);

                if (reason.HasValue)
                {
                    reasons.Add(reason.Value);
                }
            }

            var maxReasons = (int)person.GetPersonalityValue(PersonalityAttributeId.MaximumOfferReasons);

            return new OfferResponse(reasons, maxReasons);
        }

        public bool IsUnemployed(Person person)
        {
            return !person.WorkHistory.Any() || person.WorkHistory.All(wh => wh.EndDate.HasValue);
        }

        public bool IsRetired(Person person)
        {
            var currentDate = _gameClockService.CurrentDate;

            return person.RetirementDate <= currentDate;
        }

        public Personality GetPersonality(Guid id)
        {
            return _personalities.FirstOrDefault(p => p.Id == id);
        }

        public void AddSkill(Person person, SkillDefinition skillDefinition)
        {
            var skill = GenerateSkill(skillDefinition, 0);

            skill.Value = ((decimal)skill.LearnRate) / 255m;

            person.Skills.Add(skill);
        }

        private Skill GenerateSkill(SkillDefinition definition)
        {
            return GenerateSkill(definition, Utilities.GetRanged(definition.Value));
        }

        private Skill GenerateSkill(SkillDefinition definition, decimal value)
        {
            return new Skill
            {
                SkillDefinition = definition,
                Value = value,
                LearnRate = Utilities.GetRandomByte(),
                ForgetRate = Utilities.GetRandomByte()
            };
        }

        private SkillDefinition GetWeightedRandomSkillDefinition(IEnumerable<ProfessionSkillDefinition> professionSkillDefinitions)
        {
            var max = professionSkillDefinitions.Sum(sd => sd.Weight);

            var number = Utilities.GetRandomInt(max);

            var index = 0;

            foreach (var skillDefinition in professionSkillDefinitions)
            {
                index += skillDefinition.Weight;

                if (number <= index)
                {
                    return skillDefinition.SkillDefinition;
                }
            }

            return professionSkillDefinitions.Last().SkillDefinition;
        }

        private IEnumerable<Skill> GetOptionalSkills(IEnumerable<ProfessionSkillDefinition> professionSkillDefinitions, int skillCount)
        {
            var output = new List<Skill>();

            var definitions = professionSkillDefinitions.Where(sd => !output.Any(s => s.SkillDefinition == sd.SkillDefinition));

            for (var index = 0; index < skillCount; index++)
            {
                if (!definitions.Any())
                {
                    break;
                }

                var skillDefinition = GetWeightedRandomSkillDefinition(definitions);

                var skill = GenerateSkill(skillDefinition);

                output.Add(skill);
            }

            return output;
        }

        private Person GeneratePerson(Guid industryId, int minimumOptionalSkillCount, int maximumOptionalSkillCount)
        {
            Person output = null;

            var mandatorySkills = _skillDefinitionService
                .GetMandatorySkillDefinitions(industryId)
                .Select(sd => GenerateSkill(sd))
                .ToList();

            var optionalSkillCount = Utilities.GetRandomInt(minimumOptionalSkillCount, maximumOptionalSkillCount);

            var profession = _professions[industryId].Random();

            var skills = mandatorySkills.Concat(GetOptionalSkills(profession.SkillDefinitions, optionalSkillCount));

            output = new Person(skills)
            {
                Id = Utilities.InvalidId,
                Personality = _personalities.Random(),
                ProfessionId = profession.Id
            };

            if (Utilities.GetRandomBool(_configurationService.GetIntValue(null, ConfigurationKey.PercentFemale)))
            {
                output.Gender = Gender.Female;
            }
            else
            {
                output.Gender = Gender.Male;
            }

            output.FirstName = _firstNames[output.Gender].Random();

            output.LastName = _lastNames.Random();

            var minimumRetirementAge = (int)output.GetPersonalityValue(PersonalityAttributeId.MinimumRetirementAge);

            var retirementAge = Utilities.GetRandomInt(minimumRetirementAge, 85);

            var days = Utilities.GetRandomInt(0, 365);

            var years = Utilities.GetRandomInt(18, 60);

            output.Birthday = _gameClockService.CurrentDate.AddYears(-years).AddDays(days);

            output.RetirementDate = output.Birthday.AddYears(retirementAge)
                                                   .AddDays(Utilities.GetRandomInt(0, 365));

            return output;
        }

        private readonly IEnumerable<IOfferEvaluator> _offerEvaluators;
        private readonly IBizioRepository _bizioRepository;
        private readonly IConfigurationService _configurationService;
        private readonly ILoggingService _loggingService;
        private readonly IGameClockService _gameClockService;
        private readonly ISkillDefinitionService _skillDefinitionService;
    }
}