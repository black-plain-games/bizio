﻿using Bizio.Core.Data.Configuration;
using Bizio.Core.Data.Contracts;
using Bizio.Core.Data.Exports;
using Bizio.Core.Data.Exports.Statics;
using Bizio.Core.Model.Companies;
using Bizio.Core.Model.Projects;
using Bizio.Core.Repositories;
using Bizio.Core.Services;
using Bizio.Core.Services.Bases;
using Bizio.Core.Services.Data;
using Bizio.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Bizio.Services
{
    public class ProjectService : ServiceBase<ProjectService>, IProjectService, IInitialize
    {
        public IEnumerable<Project> Projects => _projects;

        public IEnumerable<ProjectDefinition> ProjectDefinitions { get; private set; }

        public bool ShouldGenerateZeroStarProject => !_projects.Any(p => p.Status == ProjectStatus.Unassigned && p.ReputationRequired == StarCategory.ZeroStar);

        public ProjectService(IProjectRepository projectRepository,
            IConfigurationService configurationService,
            IIndustryService industryService,
            ILoggingService loggingService,
            IGameClockService gameClockService)
            : base()
        {
            _projectRepository = projectRepository;

            _configurationService = configurationService;

            _industryService = industryService;

            _loggingService = loggingService;

            _gameClockService = gameClockService;
        }

        public void StartNewGame(NewGameDataInternal data)
        {
            var projectGenerationBatchSize = _configurationService.GetIntValue(null, ConfigurationKey.ProjectGenerationBatchSize);

            _projects = new List<Project>(projectGenerationBatchSize);

            var industry = _industryService.GetIndustry(data.InputData.IndustryId);

            GenerateProjects(industry, projectGenerationBatchSize);
        }

        public int SaveGame(int gameId)
        {
            var serviceData = GetData();

            using (var transaction = new RepositoryTransaction(_projectRepository))
            {
                _projectRepository.SaveGame(gameId, serviceData);
            }

            return gameId;
        }

        public void LoadGame(LoadGameData data)
        {
            ProjectServiceData loadData = null;

            using (var transaction = new RepositoryTransaction(_projectRepository))
            {
                loadData = _projectRepository.LoadGame(data.UserId, data.GameId);
            }

            _projects = new List<Project>(loadData.Projects);

            foreach (var project in _projects)
            {
                project.Definition = GetProjectDefinition(project.Definition.Id);
            }
        }

        public bool CanAcceptProject(Project project, Reputation reputation)
        {
            if (project.Status != ProjectStatus.Unassigned)
            {
                return false;
            }

            var required = (decimal)project.ReputationRequired - 1;

            var max = (decimal)StarCategory.FiveStar - 1;

            var requiredRating = required / max;

            return reputation.Value >= requiredRating;
        }

        public void GenerateProjects(Industry industry, int count)
        {
            for (var index = 0; index < count; index++)
            {
                var availableProjectDefinitions = GetAvailableProjectDefinitions(industry);

                if (availableProjectDefinitions.Any())
                {
                    GenerateProject(availableProjectDefinitions);
                }
                else
                {
                    _loggingService.Warning("No available project definitions for {0} industry.", industry);

                    break;
                }
            }
        }

        public bool IsProjectComplete(Project project)
        {
            return project.Requirements.All(r => r.CurrentValue >= r.TargetValue);
        }

        public ProjectDefinition GetProjectDefinition(short id)
        {
            return ProjectDefinitions.FirstOrDefault(pd => pd.Id == id);
        }

        public Project GetProject(int projectId)
        {
            return _projects.FirstOrDefault(p => p.Id == projectId);
        }

        public bool RequestExtension(Project project, Reputation reputation)
        {
            var companyReputation = (decimal)GetStarCategory(reputation.Value);

            var projectReputation = (decimal)project.ReputationRequired;

            var extensionChance = 1m / (2m * (.5m + Math.Max(projectReputation - companyReputation, 0)));

            var didGetExtension = Utilities.GetRandomBool(extensionChance);

            if (didGetExtension)
            {
                var maxExtensionLength = _configurationService.GetIntValue(null, ConfigurationKey.MaximumExtensionLength);

                var extensionLength = Utilities.GetRandomInt(1, maxExtensionLength);

                var extensionDeadline = Utilities.GetDate(project.Deadline, _gameClockService.WeeksPerTurn, extensionLength);

                project.ExtensionDeadline = extensionDeadline;
            }

            return didGetExtension;
        }

        public ProjectsExportData ExportData()
        {
            return new ProjectsExportData(Projects.Where(p => p.Status != ProjectStatus.Completed));
        }

        public async Task<StaticProjectsExportData> ExportStaticDataAsync(CancellationToken cancellationToken = default)
        {
            await Task.Yield();

            cancellationToken.ThrowIfCancellationRequested();

            return new StaticProjectsExportData
            {
                ProjectDefinitions = ProjectDefinitions
            };
        }

        protected StarCategory GetStarCategory(decimal value)
        {
            if (value > .8m)
            {
                return StarCategory.FiveStar;
            }
            else if (value > .6m)
            {
                return StarCategory.FourStar;
            }
            else if (value > .4m)
            {
                return StarCategory.ThreeStar;
            }
            else if (value > .2m)
            {
                return StarCategory.TwoStar;
            }
            else if (value > 0)
            {
                return StarCategory.OneStar;
            }

            return StarCategory.ZeroStar;
        }

        protected override async Task InitializeInternalAsync(CancellationToken cancellationToken = default)
        {
            var initializeIndustriesTask = _industryService.InitializeAsync(cancellationToken);

            using (var transaction = new RepositoryTransaction(_projectRepository))
            {
                ProjectDefinitions = await _projectRepository.LoadProjectDefinitionsAsync(cancellationToken);
            }

            await initializeIndustriesTask;

            RefreshIndustries();
        }

        protected void RefreshIndustries()
        {
            var projectDefinitions = new List<ProjectDefinition>();

            foreach (var projectDefinition in ProjectDefinitions)
            {
                var industries = new List<Industry>();

                foreach (var industry in projectDefinition.Industries)
                {
                    var realIndustry = _industryService.GetIndustry(industry.Id);

                    if (realIndustry != null)
                    {
                        industries.Add(realIndustry);
                    }
                    else
                    {
                        _loggingService.Warning("Cannot find industry {0} for project definition {1}.", industry.Id, projectDefinition.Id);
                    }
                }

                projectDefinitions.Add(new ProjectDefinition(industries, projectDefinition.Skills)
                {
                    Description = projectDefinition.Description,
                    Id = projectDefinition.Id,
                    Name = projectDefinition.Name,
                    ProjectLength = projectDefinition.ProjectLength,
                    Value = projectDefinition.Value
                });
            }

            ProjectDefinitions = projectDefinitions;
        }

        protected void GenerateProject(IEnumerable<ProjectDefinition> definitions)
        {
            var reputationRequired = StarCategory.ZeroStar;

            // We always have at least one Zero Star project, so everyone can work on something
            if (!ShouldGenerateZeroStarProject)
            {
                reputationRequired = Utilities.GetRandomValue<StarCategory>();
            }

            var definition = definitions.Random();

            if (definition != null)
            {
                var project = CreateProjectFromDefinition(definition, reputationRequired);

                _projects.Add(project);
            }
        }

        private static decimal CalculateValue(Range<decimal> value, StarCategory reputationRequired, int turnCount)
        {
            var range = value.Maximum - value.Minimum;

            // 6 is the number of possible star categories (0-5)
            var interval = range / 6m;

            decimal starCategory = (int)reputationRequired - 1;

            var minimum = value.Minimum + (interval * starCategory);

            var maximum = minimum + interval;

            return turnCount * Utilities.GetRandomDecimal(minimum, maximum);
        }

        protected Project CreateProjectFromDefinition(ProjectDefinition definition, StarCategory reputationRequired)
        {
            // Calculate deadline
            var startDate = _gameClockService.CurrentDate;

            var turnCount = Utilities.GetRanged(definition.ProjectLength);

            var weeksPerTurn = _gameClockService.WeeksPerTurn;

            var deadline = Utilities.GetDate(startDate, weeksPerTurn, turnCount);

            // Determine requirements
            var requiredSkills = definition.Skills.Where(s => s.IsRequired);

            var optionalSkills = Enumerable.Empty<ProjectDefinitionSkill>();

            if (reputationRequired != StarCategory.ZeroStar)
            {
                optionalSkills = definition.Skills.Where(s => !s.IsRequired && Utilities.GetRandomBool());
            }

            var requirements = requiredSkills
                .Concat(optionalSkills
                ).Select(s => new ProjectRequirement
                {
                    SkillDefinition = s.SkillDefinition,
                    TargetValue = CalculateValue(s.Value, reputationRequired, turnCount)
                });

            return new Project(requirements)
            {
                Id = Utilities.InvalidId,
                Definition = definition,
                ReputationRequired = reputationRequired,
                Status = ProjectStatus.Unassigned,
                Deadline = deadline,
                Value = CalculateValue(definition.Value, reputationRequired, turnCount)
            };
        }

        protected IEnumerable<ProjectDefinition> GetAvailableProjectDefinitions(Industry industry)
        {
            return ProjectDefinitions.Where(pd => pd.Industries.Contains(industry));
        }

        protected ProjectServiceData GetData()
        {
            return new ProjectServiceData
            {
                Projects = _projects
            };
        }

        private readonly IProjectRepository _projectRepository;

        private readonly IConfigurationService _configurationService;

        private readonly IIndustryService _industryService;

        private readonly ILoggingService _loggingService;

        private readonly IGameClockService _gameClockService;

        private ICollection<Project> _projects;
    }
}