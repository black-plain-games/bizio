﻿using Bizio.Core.Model.People;
using Bizio.Core.Repositories;
using Bizio.Core.Services;
using Bizio.Core.Services.Bases;
using Bizio.Core.Utilities;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Bizio.Services
{
    public class SkillDefinitionService : ServiceBase<SkillDefinitionService>, ISkillDefinitionService
    {
        public IEnumerable<SkillDefinition> SkillDefinitions { get; private set; }

        public SkillDefinitionService(IPersonRepository personRepository)
        {
            _personRepository = personRepository;
        }

        public SkillDefinition GetSkillDefinition(byte id) => SkillDefinitions.Single(sd => sd.Id == id);

        public IEnumerable<SkillDefinition> GetMandatorySkillDefinitions(byte industryId) => _mandatorySkillDefinitions[industryId];

        protected async override Task InitializeInternalAsync(CancellationToken cancellationToken = default)
        {
            using (var transaction = new RepositoryTransaction(_personRepository))
            {
                SkillDefinitions = await _personRepository.LoadSkillDefinitionsAsync(cancellationToken);

                // TODO: Make a repository for this service and move these there
                _mandatorySkillDefinitions = await _personRepository.LoadMandatorySkillDefinitionsAsync(SkillDefinitions, cancellationToken);
            }
        }

        public IReadOnlyDictionary<byte, IEnumerable<SkillDefinition>> GetMandatorySkillDefinitions() => _mandatorySkillDefinitions;

        private readonly IPersonRepository _personRepository;

        private static IReadOnlyDictionary<byte, IEnumerable<SkillDefinition>> _mandatorySkillDefinitions;
    }
}
