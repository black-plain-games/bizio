﻿using Bizio.Core.Model.Text;
using Bizio.Core.Repositories;
using Bizio.Core.Services;
using Bizio.Core.Services.Bases;
using Bizio.Core.Utilities;
using System;
using System.Collections.Generic;

namespace Bizio.Services
{
    public class TextService : ServiceBase<TextService>, ITextService
    {
        public TextService(ITextRepository textRepository)
            : base()
        {
            _textRepository = textRepository;
        }

        public IEnumerable<Language> GetLanguages(DateTimeOffset minimumDateModified)
        {
            using (var transaction = new RepositoryTransaction(_textRepository))
            {
                return _textRepository.GetLanguages(minimumDateModified);
            }
        }

        private readonly ITextRepository _textRepository;
    }
}
