﻿using Bizio.Core.Model.Actions;
using Bizio.Core.Services;
using System;

namespace Bizio.Strategies.Actions.Data
{
    public class AcceptProjectActionData(Guid projectId) : IActionData
    {
        public ActionType ActionType => ActionType.AcceptProject;
        public Guid ProjectId { get; } = projectId;
    }
}