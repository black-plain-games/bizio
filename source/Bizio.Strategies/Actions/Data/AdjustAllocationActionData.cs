﻿using Bizio.Core.Model.Actions;
using Bizio.Core.Model.Projects;
using Bizio.Core.Services;
using System;

namespace Bizio.Strategies.Actions.Data
{
    public class AdjustAllocationActionData(Guid personId, Guid projectId, int percentage, ProjectRole role) : IActionData
    {
        public ActionType ActionType => ActionType.AdjustAllocation;
        public Guid PersonId { get; } = personId;
        public Guid ProjectId { get; } = projectId;
        public int Percentage { get; } = percentage;
        public ProjectRole Role { get; } = role;
    }
}