﻿using Bizio.Core.Model.Actions;
using Bizio.Core.Services;
using System;

namespace Bizio.Strategies.Actions.Data
{
    public class AdjustSalaryActionData(Guid personId, int salary) : IActionData
    {
        public ActionType ActionType => ActionType.AdjustSalary;
        public Guid PersonId { get; } = personId;
        public int Salary { get; } = salary;
    }
}