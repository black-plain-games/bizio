﻿using Bizio.Core.Model.Actions;
using Bizio.Core.Model.Companies;
using Bizio.Core.Services;
using System;

namespace Bizio.Strategies.Actions.Data
{
    public class ChangeCompanyMessageStatusActionData(Guid messageId, CompanyMessageStatus status) : IActionData
    {
        public ActionType ActionType => ActionType.ChangeCompanyMessageStatus;
        public Guid MessageId { get; } = messageId;
        public CompanyMessageStatus Status { get; } = status;
    }
}