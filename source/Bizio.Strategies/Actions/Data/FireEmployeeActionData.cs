﻿using Bizio.Core.Model.Actions;
using Bizio.Core.Services;
using System;

namespace Bizio.Strategies.Actions.Data
{
    public class FireEmployeeActionData(Guid personId) : IActionData
    {
        public ActionType ActionType => ActionType.FireEmployee;
        public Guid PersonId { get; } = personId;
    }
}