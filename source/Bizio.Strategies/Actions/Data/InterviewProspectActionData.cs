﻿using Bizio.Core.Model.Actions;
using Bizio.Core.Services;
using System;

namespace Bizio.Strategies.Actions.Data
{
    public class InterviewProspectActionData(Guid personId) : IActionData
    {
        public ActionType ActionType => ActionType.InterviewProspect;
        public Guid PersonId { get; } = personId;
    }
}