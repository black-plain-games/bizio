﻿using Bizio.Core.Model.Actions;
using Bizio.Core.Services;
using System;

namespace Bizio.Strategies.Actions.Data
{
    public class MakeOfferActionData(Guid personId, int offerValue) : IActionData
    {
        public ActionType ActionType => ActionType.MakeOffer;
        public Guid PersonId { get; } = personId;
        public int OfferValue { get; } = offerValue;
    }
}