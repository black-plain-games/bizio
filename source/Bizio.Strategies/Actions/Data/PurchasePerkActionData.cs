﻿using Bizio.Core.Model.Actions;
using Bizio.Core.Services;
using System;

namespace Bizio.Strategies.Actions.Data
{
    public class PurchasePerkActionData(Guid perkId) : IActionData
    {
        public ActionType ActionType => ActionType.PurchasePerk;
        public Guid PerkId { get; } = perkId;
    }
}