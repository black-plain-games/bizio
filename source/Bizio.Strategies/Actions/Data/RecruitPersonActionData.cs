﻿using Bizio.Core.Model.Actions;
using Bizio.Core.Services;
using System;

namespace Bizio.Strategies.Actions.Data
{
    public class RecruitPersonActionData(Guid personId) : IActionData
    {
        public ActionType ActionType => ActionType.RecruitPerson;
        public Guid PersonId { get; } = personId;
    }
}