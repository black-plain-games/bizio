﻿using Bizio.Core.Model.Actions;
using Bizio.Core.Services;
using System;

namespace Bizio.Strategies.Actions.Data
{
    public class RequestExtensionActionData(Guid projectId) : IActionData
    {
        public ActionType ActionType => ActionType.RequestExtension;
        public Guid ProjectId { get; } = projectId;
    }
}