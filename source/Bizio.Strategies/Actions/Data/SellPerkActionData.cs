﻿using Bizio.Core.Model.Actions;
using Bizio.Core.Services;
using System;

namespace Bizio.Strategies.Actions.Data
{
    public class SellPerkActionData(Guid companyPerkId) : IActionData
    {
        public ActionType ActionType => ActionType.SellPerk;
        public Guid CompanyPerkId { get; } = companyPerkId;
    }
}