﻿using Bizio.Core.Model.Actions;
using Bizio.Core.Services;
using System;

namespace Bizio.Strategies.Actions.Data
{
    public class SubmitProjectActionData(Guid projectId, bool isCancellation) : IActionData
    {
        public ActionType ActionType => ActionType.SubmitProject;
        public Guid ProjectId { get; } = projectId;
        public bool IsCancellation { get; } = isCancellation;
    }
}