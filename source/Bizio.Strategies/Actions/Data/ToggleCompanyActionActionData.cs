﻿using Bizio.Core.Model.Actions;
using Bizio.Core.Services;
using System;

namespace Bizio.Strategies.Actions.Data
{
    public class ToggleCompanyActionActionData(Guid actionId) : IActionData
    {
        public ActionType ActionType => ActionType.ToggleCompanyAction;
        public Guid ActionId { get; } = actionId;
    }
}