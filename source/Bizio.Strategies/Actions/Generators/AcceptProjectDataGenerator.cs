﻿using Bizio.Core.Model.Actions;
using Bizio.Core.Model.Companies;
using Bizio.Core.Model.People;
using Bizio.Core.Model.Projects;
using Bizio.Core.Services;
using Bizio.Strategies.Actions.Data;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Strategies.Actions.Generators
{
    public class AcceptProjectDataGenerator : DataGenerator
    {
        public override ActionType Type => ActionType.AcceptProject;

        public AcceptProjectDataGenerator(IProjectService projectService, IGameClockService gameClockService)
        {
            _projectService = projectService;

            _gameClockService = gameClockService;
        }

        protected override IEnumerable<IActionData> GenerateData(byte industryId, Company company, int maxCount)
        {
            var currentProjectCount = company.Projects.Count(p => _gameClockService.DateToTurnDifference(p.ExtensionDeadline ?? p.Deadline) > 1);

            var founder = company.Employees.Single(e => e.IsFounder);

            var projectsPerEmployee = founder.Person.GetPersonalityValue(PersonalityAttributeId.DesiredProjectsPerEmployee);

            var employeesPerProject = founder.Person.GetPersonalityValue(PersonalityAttributeId.DesiredEmployeesPerProject);

            var capacity = projectsPerEmployee * company.Employees.Count;

            var throughput = employeesPerProject * currentProjectCount;

            var remainingCapacity = capacity - throughput;

            if (remainingCapacity < 0)
            {
                return Empty;
            }

            var output = new List<AcceptProjectActionData>();

            var availableProjects = _projectService.Projects
                .Where(p => !output.Any(ad => ad.ProjectId == p.Id) && _projectService.CanAcceptProject(p, company.Reputation))
                .OrderByDescending(p => GetProjectWeight(p, company));

            if (!availableProjects.Any())
            {
                return Empty;
            }

            for (var projectRequestIndex = 0; projectRequestIndex < maxCount && remainingCapacity > 0; projectRequestIndex++)
            {
                var project = availableProjects.FirstOrDefault();

                if (project == null)
                {
                    break;
                }

                output.Add(new AcceptProjectActionData(project.Id));

                var potentialThroughput = employeesPerProject * output.Count;

                remainingCapacity -= potentialThroughput;
            }

            return output;
        }

        private static decimal GetProjectWeight(Project project, Company company)
        {
            return 0;
        }

        private readonly IProjectService _projectService;

        private readonly IGameClockService _gameClockService;
    }
}