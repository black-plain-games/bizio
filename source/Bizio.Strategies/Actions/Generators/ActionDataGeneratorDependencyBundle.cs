﻿using Bizio.Core.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Bizio.Strategies.Actions.Generators
{
    public static class ActionDataGeneratorDependencyBundle
    {
        public static IServiceCollection RegisterActionDataGenerators(this IServiceCollection services)
        {
            return services
                .AddTransient<IActionDataGenerator, RecruitPersonDataGenerator>()
                .AddTransient<IActionDataGenerator, AcceptProjectDataGenerator>()
                .AddTransient<IActionDataGenerator, AdjustAllocationDataGenerator>()
                .AddTransient<IActionDataGenerator, InterviewProspectDataGenerator>()
                .AddTransient<IActionDataGenerator, MakeOfferDataGenerator>()
                .AddTransient<IActionDataGenerator, SubmitProjectDataGenerator>()
                .AddTransient<IActionDataGenerator, RequestExtensionDataGenerator>()
                .AddTransient<IActionDataGenerator, AdjustSalaryDataGenerator>()
                .AddTransient<IActionDataGenerator, FireEmployeeDataGenerator>()
                .AddTransient<IActionDataGenerator, PurchasePerkActionDataGenerator>()
                .AddTransient<IActionDataGenerator, SellPerkActionDataGenerator>();
        }
    }
}