﻿using Bizio.Core.Model.Actions;
using Bizio.Core.Model.Companies;
using Bizio.Core.Model.People;
using Bizio.Core.Model.Projects;
using Bizio.Core.Services;
using Bizio.Strategies.Actions.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Strategies.Actions.Generators
{
    public class AdjustAllocationDataGenerator : DataGenerator
    {
        public override ActionType Type => ActionType.AdjustAllocation;

        protected override IEnumerable<IActionData> GenerateData(byte industryId, Company company, int maxCount)
        {
            var projects = company.Projects.Where(p => p.Status == ProjectStatus.InProgress);

            if (!projects.Any())
            {
                return Empty;
            }

            var founder = company.Employees.Single(e => e.IsFounder);

            var projectsPerEmployee = (int)founder.Person.GetPersonalityValue(PersonalityAttributeId.DesiredProjectsPerEmployee);

            var employeesPerProject = (int)founder.Person.GetPersonalityValue(PersonalityAttributeId.DesiredEmployeesPerProject);

            var projectValues = new List<ProjectValue>();

            foreach (var project in projects)
            {
                foreach (var employee in company.Employees)
                {
                    var requiredSkills = project.Requirements.Join(employee.Person.Skills, r => r.SkillDefinition.Id, s => s.SkillDefinition.Id, (r, s) => new { Requirement = r, Skill = s });

                    if (!requiredSkills.Any())
                    {
                        continue;
                    }

                    var requirementTotal = 0m;

                    var skillTotal = 0m;

                    foreach (var requiredSkill in requiredSkills)
                    {
                        var remainingValue = requiredSkill.Requirement.TargetValue - requiredSkill.Requirement.CurrentValue;

                        var maxSkillValue = Math.Min(remainingValue, requiredSkill.Skill.Value);

                        requirementTotal += remainingValue;

                        skillTotal += maxSkillValue;
                    }

                    if (requirementTotal == 0)
                    {
                        continue;
                    }

                    projectValues.Add(new ProjectValue(project, employee, project.Value * (skillTotal / requirementTotal)));
                }
            }

            var allocations = new List<ProjectValue>();

            foreach (var project in projects.OrderByDescending(p => p.Value))
            {
                foreach (var projectValue in projectValues.Where(pv => pv.Project == project).OrderByDescending(pv => pv.Value))
                {
                    if (allocations.Count(a => a.Employee == projectValue.Employee) < projectsPerEmployee)
                    {
                        allocations.Add(projectValue);
                    }

                    if (allocations.Count(a => a.Project == projectValue.Project) == employeesPerProject)
                    {
                        break;
                    }
                }
            }

            var underAllocatedEmployees = company.Employees.Where(e => allocations.Count(a => a.Employee == e) < projectsPerEmployee);

            foreach (var employee in underAllocatedEmployees)
            {
                var remainingAllocations = projectsPerEmployee - allocations.Count(a => a.Employee == employee);

                allocations.AddRange(projectValues.Where(pv => pv.Employee == employee && !allocations.Contains(pv)).OrderByDescending(pv => pv.Value).Take(remainingAllocations));
            }

            var output = new List<AdjustAllocationActionData>();

            foreach (var project in allocations.GroupBy(a => a.Project))
            {
                foreach (var allocation in project)
                {
                    var percentage = byte.MaxValue / allocations.Count(a => a.Employee == allocation.Employee);

                    if (company.Allocations.Any(a => a.Employee == allocation.Employee &&
                    a.Percent == percentage &&
                    a.Project == project.Key &&
                    a.Role == ProjectRole.Staff))
                    {
                        continue;
                    }

                    output.Add(new AdjustAllocationActionData(allocation.Employee.Person.Id, project.Key.Id, (byte)percentage, ProjectRole.Staff));
                }
            }

            return output;
        }

        private class ProjectValue
        {
            public Project Project { get; }

            public Employee Employee { get; }

            public decimal Value { get; }

            public ProjectValue(Project project, Employee employee, decimal value)
            {
                Project = project;

                Employee = employee;

                Value = value;
            }
        }
    }
}