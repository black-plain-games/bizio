﻿using Bizio.Core.Model.Actions;
using Bizio.Core.Model.Companies;
using Bizio.Core.Model.People;
using Bizio.Core.Services;
using Bizio.Strategies.Actions.Data;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Strategies.Actions.Generators
{
    public class AdjustSalaryDataGenerator : DataGenerator
    {
        public override ActionType Type => ActionType.AdjustSalary;

        public AdjustSalaryDataGenerator(
            IGameClockService gameClockService,
            ICompensationService compensationService)
        {
            _gameClockService = gameClockService;
            _compensationService = compensationService;
        }

        protected override IEnumerable<IActionData> GenerateData(byte industryId, Company company, int maxCount)
        {
            var salariedEmployees = company.Employees.Where(e => !e.IsFounder);

            if (!salariedEmployees.Any())
            {
                return Empty;
            }

            var founder = company.Employees.Single(e => e.IsFounder);

            var waitPeriodBeforeFirstSalaryAdjustment = (int)founder.Person.GetPersonalityValue(PersonalityAttributeId.WaitPeriodBeforeFirstSalaryAdjustment);

            var increaseThresholdPercent = 1m + founder.Person.GetPersonalityValue(PersonalityAttributeId.RequiredIncreaseForAdjustment);

            var decreaseThresholdPercent = 1m - founder.Person.GetPersonalityValue(PersonalityAttributeId.RequiredDecreaseForAdjustment);

            var output = new List<AdjustSalaryActionData>();

            foreach (var employee in salariedEmployees)
            {
                if (output.Count == maxCount)
                {
                    break;
                }

                // Don't adjust salary for recent hires
                var currentWorkHistory = employee.Person.WorkHistory.Single(wh => !wh.EndDate.HasValue);

                if ((_gameClockService.CurrentDate - currentWorkHistory.StartDate).Days / 7 < waitPeriodBeforeFirstSalaryAdjustment)
                {
                    continue;
                }

                var accurateSalary = _compensationService.CalculateCompensationValue(industryId, employee.Person.Skills, founder.Person);

                var increaseThreshold = employee.Salary * increaseThresholdPercent;

                var decreaseThreshold = employee.Salary * decreaseThresholdPercent;

                if (accurateSalary >= increaseThreshold ||
                    accurateSalary <= decreaseThreshold)
                {
                    output.Add(new AdjustSalaryActionData(employee.Person.Id, accurateSalary));
                }
            }

            return output;
        }

        private readonly IGameClockService _gameClockService;
        private readonly ICompensationService _compensationService;
    }
}