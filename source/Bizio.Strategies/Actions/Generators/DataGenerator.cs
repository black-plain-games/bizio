﻿using Bizio.Core.Model.Actions;
using Bizio.Core.Model.Companies;
using Bizio.Core.Services;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Strategies.Actions.Generators
{
    public abstract class DataGenerator : IActionDataGenerator
    {
        public abstract ActionType Type { get; }

        public IEnumerable<IActionData> GenerateData(byte industryId, Company company)
        {
            var count = company.Actions.FirstOrDefault(a => a.Action.Id == (byte)Type).Count;

            if (count == 0)
            {
                return Empty;
            }

            return GenerateData(industryId, company, count);
        }

        protected abstract IEnumerable<IActionData> GenerateData(byte industryId, Company company, int maxCount);

        protected static readonly IEnumerable<IActionData> Empty = Enumerable.Empty<IActionData>();
    }
}