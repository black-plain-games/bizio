﻿using Bizio.Core.Model.Actions;
using Bizio.Core.Model.Companies;
using Bizio.Core.Model.People;
using Bizio.Core.Model.Projects;
using Bizio.Core.Services;
using Bizio.Core.Utilities;
using Bizio.Strategies.Actions.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Strategies.Actions.Generators
{
    public class FireEmployeeDataGenerator : DataGenerator
    {
        public override ActionType Type => ActionType.FireEmployee;

        public FireEmployeeDataGenerator(IGameClockService gameClockService)
        {
            _gameClockService = gameClockService;
        }

        protected override IEnumerable<IActionData> GenerateData(byte industryId, Company company, int maxCount)
        {
            var salariedEmployees = company.Employees.Where(e => !e.IsFounder);

            if (!salariedEmployees.Any())
            {
                return Empty;
            }

            var founder = company.Employees.Single(e => e.IsFounder);

            var lookbackProjectCount = (int)founder.Person.GetPersonalityValue(PersonalityAttributeId.FireEmployeeLookbackProjectCount);

            var recentCompletedProjects = company.Projects
                .Where(p => p.Status == ProjectStatus.Completed)
                .OrderByDescending(p => p.ExtensionDeadline ?? p.Deadline)
                .Take(lookbackProjectCount);

            if (recentCompletedProjects.Count() < lookbackProjectCount)
            {
                return Empty;
            }

            var lookbackWeeks = (int)founder.Person.GetPersonalityValue(PersonalityAttributeId.FireEmployeeLookbackWeeks);

            var lookbackDate = _gameClockService.CurrentDate.AddDays(-7 * lookbackWeeks);

            var recentTransactions = company.Transactions.Where(t => t.Date > lookbackDate);

            var recentIncome = recentTransactions.Where(t => t.Amount > 0).Sum(t => t.Amount);

            var recentPayroll = recentTransactions.Where(t => t.Amount < 0 && t.Type == TransactionType.Payroll).Sum(t => t.Amount);

            var maximumRecentPayroll = 1m - founder.Person.GetPersonalityValue(PersonalityAttributeId.FireEmployeeMinimumPositivePayrollPecent);

            var mustFire = maximumRecentPayroll * recentIncome < Math.Abs(recentPayroll);

            var skillValues = new Dictionary<byte, decimal>();

            foreach (var project in recentCompletedProjects)
            {
                var skillPointValue = project.Value / project.Requirements.Sum(r => r.TargetValue);

                foreach (var requirement in project.Requirements)
                {
                    if (!skillValues.ContainsKey(requirement.SkillDefinition.Id))
                    {
                        skillValues[requirement.SkillDefinition.Id] = 0;
                    }

                    skillValues[requirement.SkillDefinition.Id] += skillPointValue * requirement.SkillDefinition.Id;
                }
            }

            decimal GetSkillValue(Employee employee, Skill skill)
            {
                var skillValue = Math.Min(100m, skillValues.SafeGet(skill.SkillDefinition.Id));

                var skillWeight = employee.Person.GetSkillWeight(skill.SkillDefinition.Id);

                return skillWeight * skillValue;
            }

            var employeeValues = salariedEmployees.ToDictionary(e => e, e => e.Person.Skills.Sum(s => GetSkillValue(e, s))).OrderBy(e => e.Value);

            var output = new List<FireEmployeeActionData>();

            var minimumSkillValue = founder.Person.GetPersonalityValue(PersonalityAttributeId.FireEmployeeMinimumSkillValue);

            foreach (var employee in employeeValues)
            {
                if (output.Count == maxCount)
                {
                    break;
                }

                if (mustFire || employee.Value < minimumSkillValue)
                {
                    output.Add(new FireEmployeeActionData(employee.Key.Person.Id));
                }
            }

            return output;
        }

        private readonly IGameClockService _gameClockService;
    }
}