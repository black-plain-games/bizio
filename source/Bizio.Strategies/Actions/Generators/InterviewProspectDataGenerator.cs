﻿using Bizio.Core.Model.Actions;
using Bizio.Core.Model.Companies;
using Bizio.Core.Model.People;
using Bizio.Core.Model.Projects;
using Bizio.Core.Services;
using Bizio.Strategies.Actions.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Strategies.Actions.Generators
{
    public class InterviewProspectDataGenerator : DataGenerator
    {
        public override ActionType Type => ActionType.InterviewProspect;

        protected override IEnumerable<IActionData> GenerateData(byte industryId, Company company, int maxCount)
        {
            if (!company.Prospects.Any())
            {
                return Empty;
            }

            var founder = company.Employees.Single(e => e.IsFounder);

            var urgency = founder.Person.GetPersonalityValue(PersonalityAttributeId.HireProspectUrgency);

            var employeesPerProject = (int)founder.Person.GetPersonalityValue(PersonalityAttributeId.DesiredEmployeesPerProject);

            var numberOfProspectsToHire = (int)Math.Floor((company.Projects.Count * employeesPerProject * urgency) - company.Employees.Count);

            if (numberOfProspectsToHire < 1)
            {
                return Empty;
            }

            numberOfProspectsToHire = Math.Min(maxCount, numberOfProspectsToHire);

            var desiredAccuracy = founder.Person.GetPersonalityValue(PersonalityAttributeId.DesiredProspectAccuracy);

            return company.Prospects
                .Where(p => p.Accuracy < desiredAccuracy)
                .Select(p => new
                {
                    RelevantSkillWeight = GetRelevantSkillWeight(p, company.Projects),
                    PersonId = p.Person.Id
                })
                .Where(p => p.RelevantSkillWeight > 0)
                .OrderByDescending(p => p.RelevantSkillWeight)
                .Take(numberOfProspectsToHire)
                .Select(p => new InterviewProspectActionData(p.PersonId))
                .ToList();
        }

        /// <returns>The number of projects that require any skills this prospect has.</returns>
        private static int GetRelevantSkillWeight(Prospect prospect, IEnumerable<Project> projects)
        {
            return prospect.Skills.Sum(s => projects.Count(p => p.Requirements.Any(r => r.SkillDefinition.Id == s.SkillDefinition.Id)));
        }
    }
}