﻿using Bizio.Core.Model.Actions;
using Bizio.Core.Model.Companies;
using Bizio.Core.Model.People;
using Bizio.Core.Model.Projects;
using Bizio.Core.Services;
using Bizio.Core.Utilities;
using Bizio.Strategies.Actions.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Strategies.Actions.Generators
{
    public class MakeOfferDataGenerator : DataGenerator
    {
        public override ActionType Type => ActionType.MakeOffer;

        public MakeOfferDataGenerator(
            IPersonService personService,
            ICompensationService compensationService)
        {
            _personService = personService;
            _compensationService = compensationService;
        }

        protected override IEnumerable<IActionData> GenerateData(byte industryId, Company company, int maxCount)
        {
            var founder = company.Employees.Single(e => e.IsFounder);

            var output = new List<MakeOfferActionData>();

            var employeesPerProject = (int)founder.Person.GetPersonalityValue(PersonalityAttributeId.DesiredEmployeesPerProject);

            if (company.Prospects.Any())
            {
                var prospectUrgency = founder.Person.GetPersonalityValue(PersonalityAttributeId.HireProspectUrgency);

                var numberOfProspectsToHire = (int)Math.Floor((company.Projects.Count * employeesPerProject * prospectUrgency) - company.Employees.Count);

                if (numberOfProspectsToHire < 1)
                {
                    return Empty;
                }

                numberOfProspectsToHire = Math.Min(maxCount, numberOfProspectsToHire);

                var prospectsToHire = company.Prospects
                    .Select(p => new
                    {
                        RelevantSkillWeight = GetRelevantSkillWeight(p, company.Projects),
                        Prospect = p
                    })
                    .Where(p => p.RelevantSkillWeight > 0)
                    .OrderByDescending(p => p.RelevantSkillWeight)
                    .Take(numberOfProspectsToHire);

                foreach (var prospect in prospectsToHire)
                {
                    var offerAmount = prospect.Prospect.Salary.Average();

                    output.Add(new MakeOfferActionData(prospect.Prospect.Person.Id, offerAmount));
                }
            }

            var personUrgency = founder.Person.GetPersonalityValue(PersonalityAttributeId.HirePersonUrgency);

            var numberOfPeopleToHire = (int)Math.Floor((company.Projects.Count * employeesPerProject * personUrgency) - (company.Employees.Count + output.Count));

            numberOfPeopleToHire = Math.Min(maxCount - output.Count, numberOfPeopleToHire);

            if (numberOfPeopleToHire < 1)
            {
                return output;
            }

            var currentPayroll = company.Employees.Sum(e => e.Salary);

            var futurePayroll = 0m;

            var minimumTurnsOfRetention = founder.Person.GetPersonalityValue(PersonalityAttributeId.MinimumRetentionTurns);

            var minimumMoneyPercentForOffer = founder.Person.GetPersonalityValue(PersonalityAttributeId.MinimumMoneyPercentForOffer);

            var minimumCompanyMoney = company.Money * minimumMoneyPercentForOffer;

            var peopleToSkip = new List<int>();

            while (output.Count < numberOfPeopleToHire)
            {
                // don't hire prospects or people you're already trying to hire
                var person = _personService.GetUnemployedPerson(industryId, company.Prospects.Select(p => p.Person.Id).Concat(peopleToSkip));

                if (person == null)
                {
                    break;
                }

                var offer = _compensationService.CalculateCompensationValue(industryId, person.Skills, founder.Person);

                var potentialFuturePayroll = futurePayroll + offer + currentPayroll;

                var expectedFutureMoney = company.Money - (potentialFuturePayroll * minimumTurnsOfRetention);

                if (expectedFutureMoney > minimumCompanyMoney)
                {
                    output.Add(new MakeOfferActionData(person.Id, offer / 2m));
                }

                peopleToSkip.Add(person.Id);
            }

            return output;
        }

        /// <returns>The number of projects that require any skills this prospect has.</returns>
        private static int GetRelevantSkillWeight(Prospect prospect, IEnumerable<Project> projects)
        {
            return prospect.Skills.Sum(s => projects.Count(p => p.Requirements.Any(r => r.SkillDefinition.Id == s.SkillDefinition.Id)));
        }

        private readonly IPersonService _personService;
        private readonly ICompensationService _compensationService;
    }
}