﻿using Bizio.Core.Model.Actions;
using Bizio.Core.Model.Companies;
using Bizio.Core.Model.Perks;
using Bizio.Core.Services;
using Bizio.Strategies.Actions.Data;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Strategies.Actions.Generators
{
    public class PurchasePerkActionDataGenerator : DataGenerator
    {
        public override ActionType Type => ActionType.PurchasePerk;

        public PurchasePerkActionDataGenerator(IPerkService perkService)
        {
            _perkService = perkService;
        }

        protected override IEnumerable<IActionData> GenerateData(byte industryId, Company company, int maxCount)
        {
            var purchasablePerks = _perkService.Perks.Where(p => CanPurchasePerk(company, p));

            if (!purchasablePerks.Any())
            {
                return Empty;
            }

            // Order the perks by the number of benefits being applied (# benefits per perk * # employees that perk applies to)
            var orderedPerks = purchasablePerks
                .ToDictionary(p => p, p => p.Benefits.Count() * company.Employees.Count(e => _perkService.DoesPerkApplyTo(p, e)))
                .Where(p => p.Value > 0)
                .OrderByDescending(p => p.Value);

            var output = new List<PurchasePerkActionData>();

            var totalCost = 0m;

            foreach (var perk in orderedPerks)
            {
                var ownedCount = company.Perks.Count(p => p.Perk.Id == perk.Key.Id);

                var cost = perk.Key.InitialCost ?? 0m;

                var perksToPurchase = new List<PurchasePerkActionData>();

                // try to purchase one if
                // - you have enough turn actions to
                // - you don't own too many of this perk already
                // - you can afford it
                while (maxCount > (output.Count + perksToPurchase.Count) &&
                    (!perk.Key.MaxCount.HasValue || ownedCount + perksToPurchase.Count < perk.Key.MaxCount) &&
                    totalCost + cost <= company.Money)
                {
                    perksToPurchase.Add(new PurchasePerkActionData(perk.Key.Id));
                }

                output.AddRange(perksToPurchase);
            }

            return output;
        }

        private bool CanPurchasePerk(Company company, Perk perk)
        {
            if (perk.MaxCount <= company.Perks.Count(p => p.Perk.Id == perk.Id))
            {
                return false;
            }

            if (perk.InitialCost.HasValue && company.Money < perk.InitialCost.Value)
            {
                return false;
            }

            if (!_perkService.DoesPerkApplyTo(perk, company))
            {
                return false;
            }

            return true;
        }



        private readonly IPerkService _perkService;
    }
}