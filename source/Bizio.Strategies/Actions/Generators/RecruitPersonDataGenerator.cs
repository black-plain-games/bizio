﻿using Bizio.Core.Model.Actions;
using Bizio.Core.Model.Companies;
using Bizio.Core.Model.People;
using Bizio.Core.Services;
using Bizio.Strategies.Actions.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Strategies.Actions.Generators
{
    public class RecruitPersonDataGenerator : DataGenerator
    {
        public override ActionType Type => ActionType.RecruitPerson;

        public RecruitPersonDataGenerator(IPersonService personService)
        {
            _personService = personService;
        }

        protected override IEnumerable<IActionData> GenerateData(byte industryId, Company company, int maxCount)
        {
            var founder = company.Employees.FirstOrDefault(e => e.IsFounder);

            var desiredProspectPoolSize = founder.Person.GetPersonalityValue(PersonalityAttributeId.DesiredProspectPoolSize);

            var recruitCount = desiredProspectPoolSize - company.Prospects.Count;

            if (recruitCount < 1)
            {
                return Empty;
            }

            recruitCount = Math.Min(maxCount, recruitCount);

            var actionData = new List<IActionData>();

            var prospectPersonIds = company.Prospects.Select(p => p.Person.Id).ToList();

            for (var index = 0; index < recruitCount; index++)
            {
                var person = _personService.GetUnemployedPerson(industryId, prospectPersonIds);

                if (person == null)
                {
                    break;
                }

                actionData.Add(new RecruitPersonActionData(person.Id));

                prospectPersonIds.Add(person.Id);
            }

            return actionData;
        }

        private readonly IPersonService _personService;
    }
}