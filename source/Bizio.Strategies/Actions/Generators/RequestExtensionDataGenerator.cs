﻿using Bizio.Core.Model.Actions;
using Bizio.Core.Model.Companies;
using Bizio.Core.Model.People;
using Bizio.Core.Model.Projects;
using Bizio.Core.Services;
using Bizio.Core.Utilities;
using Bizio.Strategies.Actions.Data;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Strategies.Actions.Generators
{
    public class RequestExtensionDataGenerator : DataGenerator
    {
        public override ActionType Type => ActionType.RequestExtension;

        public RequestExtensionDataGenerator(IGameClockService gameClockService)
        {
            _gameClockService = gameClockService;
        }

        protected override IEnumerable<IActionData> GenerateData(byte industryId, Company company, int maxCount)
        {
            if (!company.Projects.Any())
            {
                return Empty;
            }

            var founder = company.Employees.Single(e => e.IsFounder);

            var minimumProjectCompletionPercent = founder.Person.GetPersonalityValue(PersonalityAttributeId.MinimumProjectCompletionPercent);

            var output = new List<RequestExtensionActionData>();

            foreach (var project in company.Projects.Where(p => p.Status == ProjectStatus.InProgress && !p.ExtensionDeadline.HasValue))
            {
                if (output.Count == maxCount)
                {
                    break;
                }

                var turns = _gameClockService.DateToTurnDifference(project.Deadline);

                var futureProgress = Utilities.CalculateFutureProgress(project, company, turns);

                // Don't consider requesting an extension unless the project is close to completion
                if (turns > 5 || futureProgress < .2m)
                {
                    continue;
                }

                if (futureProgress < minimumProjectCompletionPercent)
                {
                    output.Add(new RequestExtensionActionData(project.Id));
                }
            }

            return output;
        }

        private readonly IGameClockService _gameClockService;
    }
}