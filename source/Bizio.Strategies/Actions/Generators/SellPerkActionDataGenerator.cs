﻿using Bizio.Core.Model.Actions;
using Bizio.Core.Model.Companies;
using Bizio.Core.Model.People;
using Bizio.Core.Services;
using Bizio.Strategies.Actions.Data;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Strategies.Actions.Generators
{
    public class SellPerkActionDataGenerator : DataGenerator
    {
        public override ActionType Type => ActionType.SellPerk;

        public SellPerkActionDataGenerator(IPerkService perkService)
        {
            _perkService = perkService;
        }

        protected override IEnumerable<IActionData> GenerateData(byte industryId, Company company, int maxCount)
        {
            if (!company.Perks.Any())
            {
                return Empty;
            }

            var founder = company.Employees.First(e => e.IsFounder);

            // % of current capitol below which the founder considers to be dangerous
            var dangerCapitolThreshold = founder.Person.GetPersonalityValue(PersonalityAttributeId.SellPerkDangerCapitolThreshold);

            var minimumCapitol = company.Money * dangerCapitolThreshold;

            // How long does the founder want to stay above the threshold
            var turnsAboveThreshold = (int)founder.Person.GetPersonalityValue(PersonalityAttributeId.SellPerkTurnsAboveThreshold);

            var expenses = company.Employees.Sum(e => e.Salary) * turnsAboveThreshold;

            expenses += company.Perks.Sum(p => _perkService.CalculatePerkRecurringCost(p, turnsAboveThreshold));

            var futureCapitol = company.Money - expenses;

            if (futureCapitol > minimumCapitol)
            {
                return Empty;
            }

            var orderedPerks = company.Perks.OrderByDescending(p => CalculatePerkValue(p, turnsAboveThreshold)).ToList();

            var reclaimedValue = 0m;

            var output = new List<SellPerkActionData>();

            while (output.Count < maxCount &&
                orderedPerks.Any() &&
                futureCapitol + reclaimedValue < minimumCapitol)
            {
                var perk = orderedPerks.First();

                output.Add(new SellPerkActionData(perk.Id));

                reclaimedValue = CalculatePerkValue(perk, turnsAboveThreshold);
            }

            return output;
        }

        private decimal CalculatePerkValue(CompanyPerk perk, int turns)
        {
            var resellValue = _perkService.CalculatePerkResellValue(perk.Perk);

            var recurringCost = _perkService.CalculatePerkRecurringCost(perk, turns);

            return resellValue + recurringCost;
        }

        private readonly IPerkService _perkService;
    }
}