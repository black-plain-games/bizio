﻿using Bizio.Core.Model.Actions;
using Bizio.Core.Model.Companies;
using Bizio.Core.Model.People;
using Bizio.Core.Model.Projects;
using Bizio.Core.Services;
using Bizio.Core.Utilities;
using Bizio.Strategies.Actions.Data;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Strategies.Actions.Generators
{
    public class SubmitProjectDataGenerator : DataGenerator
    {
        public override ActionType Type => ActionType.SubmitProject;

        public SubmitProjectDataGenerator(IGameClockService gameClockService)
        {
            _gameClockService = gameClockService;
        }

        protected override IEnumerable<IActionData> GenerateData(byte industryId, Company company, int maxCount)
        {
            var projectsInProgress = company.Projects.Where(p => p.Status == ProjectStatus.InProgress);

            if (!projectsInProgress.Any())
            {
                return Empty;
            }

            var founder = company.Employees.Single(e => e.IsFounder);

            var minimumProjectCompletionPercent = founder.Person.GetPersonalityValue(PersonalityAttributeId.MinimumProjectCompletionPercent);

            var output = new List<SubmitProjectActionData>();

            foreach (var project in projectsInProgress.OrderByDescending(p => p.Value))
            {
                if (output.Count == maxCount)
                {
                    break;
                }

                var progress = Utilities.CalculateProgress(project.Requirements);

                if (progress > minimumProjectCompletionPercent)
                {
                    output.Add(new SubmitProjectActionData(project.Id, false));

                    continue;
                }

                // See if we should cancel the project because we won't complete it in time
                var deadline = project.ExtensionDeadline ?? project.Deadline;

                var turns = _gameClockService.DateToTurnDifference(deadline);

                // Don't consider cancelling projects with a lot of time left
                // or that haven't been started
                if (turns > 5 || (turns > 1 && progress < .2m))
                {
                    continue;
                }

                var futureProgress = Utilities.CalculateFutureProgress(project, company, turns);

                if (futureProgress < minimumProjectCompletionPercent)
                {
                    output.Add(new SubmitProjectActionData(project.Id, true));
                }
            }

            return output;
        }

        private readonly IGameClockService _gameClockService;
    }
}