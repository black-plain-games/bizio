﻿using Bizio.Core.Model.Actions;
using Bizio.Core.Model.Companies;
using Bizio.Core.Services;
using Bizio.Core.Utilities;
using Bizio.Strategies.Actions.Data;

namespace Bizio.Strategies.Actions.Handlers
{
    public class AcceptProjectAction : ActionHandlerBase
    {
        public AcceptProjectAction(ICompanyService companyService,
            IProjectService projectService,
            IGameClockService gameClockService)
            : base(ActionType.AcceptProject)
        {
            _companyService = companyService;

            _projectService = projectService;

            _gameClockService = gameClockService;
        }

        public override ActionExecutionInformation CanPerformEvent(Company company, IActionData eventData)
        {
            if (!_companyService.CanPerformAction(company, Type))
            {
                return new ActionExecutionInformation(false, $"Company {company.Id} cannot perform turn action {Type}.", company);
            }

            return Yes;
        }

        public override CompanyMessage ProcessEvent(byte industryId, Company company, IActionData eventData)
        {
            var data = eventData as AcceptProjectActionData;

            var message = new CompanyMessage
            {
                Id = Utilities.InvalidId,
                DateCreated = _gameClockService.CurrentDate,
                Source = "Project Management",
                Status = CompanyMessageStatus.UnRead
            };

            var project = _projectService.GetProject(data.ProjectId);

            if (project != null)
            {
                if (_projectService.CanAcceptProject(project, company.Reputation))
                {
                    _companyService.AcceptProject(company, project);

                    message.Subject = "We got the project!";

                    message.Message = $"We have been granted the {project.Definition.Name} project.";
                }
                else
                {
                    message.Subject = "We did not get the project...";

                    message.Message = $"We were deemed not reputable enough to be granted the {project.Definition.Name} project.";
                }
            }
            else
            {
                message.Subject = "We missed out on the project.";

                message.Message = $"A project we were seeking has been given to another company.";
            }

            return message;
        }

        private readonly ICompanyService _companyService;

        private readonly IProjectService _projectService;

        private readonly IGameClockService _gameClockService;
    }
}