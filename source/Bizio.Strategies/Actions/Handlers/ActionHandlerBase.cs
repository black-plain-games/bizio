﻿using Bizio.Core.Model.Actions;
using Bizio.Core.Model.Companies;
using Bizio.Core.Services;

namespace Bizio.Strategies.Actions.Handlers
{
    public abstract class ActionHandlerBase : IActionHandler
    {
        public ActionType Type { get; }

        public virtual ActionExecutionInformation CanPerformEvent(Company company, IActionData eventData)
        {
            return Yes;
        }

        public abstract CompanyMessage ProcessEvent(byte industryId, Company company, IActionData eventData);

        protected static readonly ActionExecutionInformation Yes = new ActionExecutionInformation(true);

        protected ActionHandlerBase(ActionType type)
        {
            Type = type;
        }
    }
}