﻿using Bizio.Core.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Bizio.Strategies.Actions.Handlers
{
    public static class ActionHandlerDependencyBundle
    {
        public static IServiceCollection RegisterActionHandlers(this IServiceCollection services)
        {
            return services
                .AddTransient<IActionHandler, AcceptProjectAction>()
                .AddTransient<IActionHandler, AdjustAllocationAction>()
                .AddTransient<IActionHandler, AdjustSalaryAction>()
                .AddTransient<IActionHandler, FireEmployeeAction>()
                .AddTransient<IActionHandler, InterviewProspectAction>()
                .AddTransient<IActionHandler, MakeOfferAction>()
                .AddTransient<IActionHandler, PurchasePerkAction>()
                .AddTransient<IActionHandler, RecruitPersonAction>()
                .AddTransient<IActionHandler, RequestExtensionAction>()
                .AddTransient<IActionHandler, SellPerkAction>()
                .AddTransient<IActionHandler, SubmitProjectAction>()
                .AddTransient<IActionHandler, ChangeCompanyMessageStatusAction>()
                .AddTransient<IActionHandler, ToggleCompanyActionActionHandler>();
        }
    }
}