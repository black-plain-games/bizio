﻿using Bizio.Core.Model.Actions;
using Bizio.Core.Model.Companies;
using Bizio.Core.Services;
using Bizio.Core.Utilities;
using Bizio.Strategies.Actions.Data;
using System.Linq;

namespace Bizio.Strategies.Actions.Handlers
{
    public class AdjustAllocationAction : ActionHandlerBase
    {
        public AdjustAllocationAction(
            ICompanyService companyService,
            IGameClockService gameClockService)
            : base(ActionType.AdjustAllocation)
        {
            _companyService = companyService;

            _gameClockService = gameClockService;
        }

        public override ActionExecutionInformation CanPerformEvent(Company company, IActionData eventData)
        {
            if (!_companyService.CanPerformAction(company, Type))
            {
                return new ActionExecutionInformation(false, $"Company {company.Id} cannot perform turn action {Type}.", company);
            }

            var data = eventData as AdjustAllocationActionData;

            var hasChanges = !company.Allocations.Any(a =>
                a.Employee.Person.Id == data.PersonId &&
                a.Percent == data.Percentage &&
                a.Project.Id == data.ProjectId &&
                a.Role == data.Role);

            return new ActionExecutionInformation(true, hasChanges);
        }

        public override CompanyMessage ProcessEvent(byte industryId, Company company, IActionData eventData)
        {
            var data = eventData as AdjustAllocationActionData;

            var employee = company.Employees.FirstOrDefault(e => e.Person.Id == data.PersonId);

            var project = company.Projects.FirstOrDefault(p => p.Id == data.ProjectId);

            _companyService.AdjustAllocation(company, employee, project, data.Percentage, data.Role);

            return new CompanyMessage
            {
                Id = Utilities.InvalidId,
                DateCreated = _gameClockService.CurrentDate,
                Status = CompanyMessageStatus.UnRead,
                Source = "Project Management",
                Subject = "Employee has been reallocated.",
                Message = $"{employee.Person.FirstName} {employee.Person.LastName} has been directed to spend {((double)data.Percentage / 255.0):P} of their time on {project.Definition.Name} in a {data.Role} role."
            };
        }

        private readonly ICompanyService _companyService;

        private readonly IGameClockService _gameClockService;
    }
}