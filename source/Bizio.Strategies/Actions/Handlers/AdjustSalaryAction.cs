﻿using Bizio.Core.Model.Actions;
using Bizio.Core.Model.Companies;
using Bizio.Core.Services;
using Bizio.Core.Utilities;
using Bizio.Strategies.Actions.Data;
using System.Linq;

namespace Bizio.Strategies.Actions.Handlers
{
    public class AdjustSalaryAction : ActionHandlerBase
    {
        public AdjustSalaryAction(
            ICompanyService companyService,
            IGameClockService gameClockService)
            : base(ActionType.AdjustSalary)
        {
            _companyService = companyService;

            _gameClockService = gameClockService;
        }

        public override ActionExecutionInformation CanPerformEvent(Company company, IActionData eventData)
        {
            if (!_companyService.CanPerformAction(company, Type))
            {
                return new ActionExecutionInformation(false, $"Company {company.Id} cannot perform turn action {Type}.", company);
            }

            var data = eventData as AdjustSalaryActionData;

            if (data.Salary < 0)
            {
                return new ActionExecutionInformation(false, $"Cannot have a negative employee value.", company, data.Salary);
            }

            return Yes;
        }

        public override CompanyMessage ProcessEvent(byte industryId, Company company, IActionData eventData)
        {
            var data = eventData as AdjustSalaryActionData;

            var employee = company.Employees.FirstOrDefault(e => e.Person.Id == data.PersonId);

            var previousSalary = employee.Salary;

            _companyService.AdjustSalary(company, employee, data.Salary);

            return new CompanyMessage
            {
                Id = Utilities.InvalidId,
                DateCreated = _gameClockService.CurrentDate,
                Status = CompanyMessageStatus.UnRead,
                Source = "Human Resources",
                Subject = "Employee Salary Adjustment",
                Message = $"{employee.Person.FirstName} {employee.Person.LastName}'s salary has been adjust from {previousSalary:C} to {employee.Salary:C}."
            };
        }

        private readonly ICompanyService _companyService;

        private readonly IGameClockService _gameClockService;
    }
}