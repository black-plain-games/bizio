﻿using Bizio.Core.Model.Actions;
using Bizio.Core.Model.Companies;
using Bizio.Core.Services;
using Bizio.Strategies.Actions.Data;
using System.Linq;

namespace Bizio.Strategies.Actions.Handlers
{
    public class ChangeCompanyMessageStatusAction : ActionHandlerBase
    {
        public ChangeCompanyMessageStatusAction()
            : base(ActionType.ChangeCompanyMessageStatus)
        {
        }

        public override CompanyMessage ProcessEvent(byte industryId, Company company, IActionData eventData)
        {
            var data = eventData as ChangeCompanyMessageStatusActionData;

            var message = company.Messages.Single(m => m.Id == data.MessageId);

            message.Status = data.Status;

            return null;
        }
    }
}