﻿using Bizio.Core.Model.Actions;
using Bizio.Core.Model.Companies;
using Bizio.Core.Services;
using Bizio.Core.Utilities;
using Bizio.Strategies.Actions.Data;
using System.Linq;

namespace Bizio.Strategies.Actions.Handlers
{
    public class FireEmployeeAction : ActionHandlerBase
    {
        public FireEmployeeAction(
            ICompanyService companyService,
            IGameClockService gameClockService)
            : base(ActionType.FireEmployee)
        {
            _companyService = companyService;

            _gameClockService = gameClockService;
        }

        public override ActionExecutionInformation CanPerformEvent(Company company, IActionData eventData)
        {
            if (!_companyService.CanPerformAction(company, Type))
            {
                return new ActionExecutionInformation(false, $"Company {company.Id} cannot perform turn action {Type}.", company);
            }

            return Yes;
        }

        public override CompanyMessage ProcessEvent(byte industryId, Company company, IActionData eventData)
        {
            var data = eventData as FireEmployeeActionData;

            var employee = company.Employees.FirstOrDefault(e => e.Person.Id == data.PersonId);

            _companyService.FireEmployee(company, employee);

            return new CompanyMessage
            {
                Id = Utilities.InvalidId,
                DateCreated = _gameClockService.CurrentDate,
                Status = CompanyMessageStatus.UnRead,
                Source = "Human Resources",
                Subject = "Employee has been fired.",
                Message = $"{employee.Person.FirstName} {employee.Person.LastName} has been fired."
            };
        }

        private readonly ICompanyService _companyService;

        private readonly IGameClockService _gameClockService;
    }
}