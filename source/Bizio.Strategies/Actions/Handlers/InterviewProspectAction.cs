﻿using Bizio.Core.Model.Actions;
using Bizio.Core.Model.Companies;
using Bizio.Core.Services;
using Bizio.Core.Utilities;
using Bizio.Strategies.Actions.Data;
using System.Linq;

namespace Bizio.Strategies.Actions.Handlers
{
    public class InterviewProspectAction : ActionHandlerBase
    {
        public InterviewProspectAction(
            ICompanyService companyService,
            IGameClockService gameClockService,
            IPersonService personService)
             : base(ActionType.InterviewProspect)
        {
            _companyService = companyService;

            _gameClockService = gameClockService;

            _personService = personService;
        }

        public override ActionExecutionInformation CanPerformEvent(Company company, IActionData eventData)
        {
            if (!_companyService.CanPerformAction(company, Type))
            {
                return new ActionExecutionInformation(false, $"Company {company.Id} cannot perform turn action {Type}.", company);
            }

            return Yes;
        }

        public override CompanyMessage ProcessEvent(byte industryId, Company company, IActionData eventData)
        {
            var data = eventData as InterviewProspectActionData;

            var prospect = company.Prospects.FirstOrDefault(p => p.Person.Id == data.PersonId);

            var message = new CompanyMessage
            {
                Id = Utilities.InvalidId,
                DateCreated = _gameClockService.CurrentDate,
                Status = CompanyMessageStatus.UnRead,
                Source = "Human Resources"
            };

            if (_personService.IsUnemployed(prospect.Person))
            {
                _companyService.InterviewProspect(industryId, company, prospect);

                message.Subject = "Interview Complete";

                message.Message = $"{prospect.Person.FirstName} {prospect.Person.LastName} has completed a round of interviews.";
            }
            else
            {
                message.Subject = "Prospect Off the Market";

                message.Message = $"{prospect.Person.FirstName} {prospect.Person.LastName} has recently accepted an offer elsewhere.";
            }

            return message;
        }

        private readonly ICompanyService _companyService;

        private readonly IGameClockService _gameClockService;

        private readonly IPersonService _personService;
    }
}