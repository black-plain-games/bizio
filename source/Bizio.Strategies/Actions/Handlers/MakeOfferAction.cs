﻿using Bizio.Core.Model.Actions;
using Bizio.Core.Model.Companies;
using Bizio.Core.Services;
using Bizio.Core.Utilities;
using Bizio.Strategies.Actions.Data;

namespace Bizio.Strategies.Actions.Handlers
{
    public class MakeOfferAction : ActionHandlerBase
    {
        public MakeOfferAction(
            ICompanyService companyService,
            IGameClockService gameClockService,
            IPersonService personService)
             : base(ActionType.MakeOffer)
        {
            _companyService = companyService;

            _gameClockService = gameClockService;

            _personService = personService;
        }

        public override ActionExecutionInformation CanPerformEvent(Company company, IActionData eventData)
        {
            if (!_companyService.CanPerformAction(company, Type))
            {
                return new ActionExecutionInformation(false, $"Company {company.Id} cannot perform turn action {Type}.", company);
            }

            var data = eventData as MakeOfferActionData;

            if (company.Money < data.OfferValue)
            {
                return new ActionExecutionInformation(false, $"Company {company.Id} does not have {data.OfferValue:C} for this offer.", company, data);
            }

            if (data.OfferValue < 0)
            {
                return new ActionExecutionInformation(false, $"Cannot offer negative amounts of money.", company, data);
            }

            return Yes;
        }

        public override CompanyMessage ProcessEvent(byte industryId, Company company, IActionData eventData)
        {
            var data = eventData as MakeOfferActionData;

            var person = _personService.GetPerson(data.PersonId);

            var message = new CompanyMessage
            {
                Id = Utilities.InvalidId,
                DateCreated = _gameClockService.CurrentDate,
                Status = CompanyMessageStatus.UnRead,
                Source = "Human Resources"
            };

            if (_personService.IsUnemployed(person))
            {
                var response = _companyService.MakeOffer(industryId, company, person, data.OfferValue);

                if (response.DidAccept)
                {
                    message.Subject = "Offer Accepted";

                    message.Message = $"{person.FirstName} {person.LastName} accepted an offer of {data.OfferValue:C}.";
                }
                else
                {
                    message.Subject = "Offer Declined";

                    message.Message = $"{person.FirstName} {person.LastName} declined an offer of {data.OfferValue:C}.";
                }
            }
            else
            {
                message.Subject = "Offer Too Late";

                message.Message = $"{person.FirstName} {person.LastName} recently accepted an offer elsewhere.";
            }

            return message;
        }

        private readonly ICompanyService _companyService;

        private readonly IGameClockService _gameClockService;

        private readonly IPersonService _personService;
    }
}