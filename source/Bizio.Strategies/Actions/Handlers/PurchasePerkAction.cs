﻿using Bizio.Core.Model.Actions;
using Bizio.Core.Model.Companies;
using Bizio.Core.Services;
using Bizio.Core.Utilities;
using Bizio.Strategies.Actions.Data;

namespace Bizio.Strategies.Actions.Handlers
{
    public class PurchasePerkAction : ActionHandlerBase
    {
        public PurchasePerkAction(
            ICompanyService companyService,
            IGameClockService gameClockService,
            IPerkService perkService)
            : base(ActionType.PurchasePerk)
        {
            _companyService = companyService;

            _gameClockService = gameClockService;

            _perkService = perkService;
        }

        public override ActionExecutionInformation CanPerformEvent(Company company, IActionData eventData)
        {
            if (!_companyService.CanPerformAction(company, Type))
            {
                return new ActionExecutionInformation(false, $"Company {company.Id} cannot perform turn action {Type}.", company);
            }

            var data = eventData as PurchasePerkActionData;

            var perk = _perkService.GetPerk(data.PerkId);

            if (!_perkService.CanPurchasePerk(company, perk))
            {
                return new ActionExecutionInformation(false, $"Company {company.Id} cannot purchase perk {perk.Id}.", company, data);
            }

            return Yes;
        }

        public override CompanyMessage ProcessEvent(byte industryId, Company company, IActionData eventData)
        {
            var data = eventData as PurchasePerkActionData;

            var perk = _perkService.GetPerk(data.PerkId);

            _companyService.PurchasePerk(company, perk);

            return new CompanyMessage
            {
                Id = Utilities.InvalidId,
                DateCreated = _gameClockService.CurrentDate,
                Status = CompanyMessageStatus.UnRead,
                Source = "Finance",
                Subject = "Purchase Made",
                Message = $"Purchased a {perk.Name} perk."
            };
        }

        private readonly ICompanyService _companyService;

        private readonly IGameClockService _gameClockService;

        private readonly IPerkService _perkService;
    }
}