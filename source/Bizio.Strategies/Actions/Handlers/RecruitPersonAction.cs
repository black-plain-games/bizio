﻿using Bizio.Core.Model.Actions;
using Bizio.Core.Model.Companies;
using Bizio.Core.Services;
using Bizio.Core.Utilities;
using Bizio.Strategies.Actions.Data;

namespace Bizio.Strategies.Actions.Handlers
{
    public class RecruitPersonAction : ActionHandlerBase
    {
        public RecruitPersonAction(
            ICompanyService companyService,
            IGameClockService gameClockService,
            IPersonService personService)
             : base(ActionType.RecruitPerson)
        {
            _companyService = companyService;

            _gameClockService = gameClockService;

            _personService = personService;
        }

        public override ActionExecutionInformation CanPerformEvent(Company company, IActionData eventData)
        {
            if (!_companyService.CanPerformAction(company, Type))
            {
                return new ActionExecutionInformation(false, $"Company {company.Id} cannot perform turn action {Type}.", company);
            }

            return Yes;
        }

        public override CompanyMessage ProcessEvent(byte industryId, Company company, IActionData eventData)
        {
            var data = eventData as RecruitPersonActionData;

            var person = _personService.GetPerson(data.PersonId);

            var message = new CompanyMessage
            {
                Id = Utilities.InvalidId,
                DateCreated = _gameClockService.CurrentDate,
                Status = CompanyMessageStatus.UnRead,
                Source = "Human Resources"
            };

            if (_personService.IsUnemployed(person))
            {
                _companyService.RecruitPerson(industryId, company, person);

                message.Subject = "Recruited Candidate";

                message.Message = $"We have successfully recruited {person.FirstName} {person.LastName}.";
            }
            else
            {
                message.Subject = "Missed a Potential Recruit";

                message.Message = $"{person.FirstName} {person.LastName} has been recently employed and cannot be recruited at this time.";
            }

            return message;
        }

        private readonly ICompanyService _companyService;

        private readonly IGameClockService _gameClockService;

        private readonly IPersonService _personService;
    }
}