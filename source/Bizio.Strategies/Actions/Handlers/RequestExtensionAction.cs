﻿using Bizio.Core.Model.Actions;
using Bizio.Core.Model.Companies;
using Bizio.Core.Services;
using Bizio.Core.Utilities;
using Bizio.Strategies.Actions.Data;
using System.Linq;

namespace Bizio.Strategies.Actions.Handlers
{
    public class RequestExtensionAction : ActionHandlerBase
    {
        public RequestExtensionAction(
            ICompanyService companyService,
            IGameClockService gameClockService)
            : base(ActionType.RequestExtension)
        {
            _companyService = companyService;

            _gameClockService = gameClockService;
        }

        public override ActionExecutionInformation CanPerformEvent(Company company, IActionData eventData)
        {
            if (!_companyService.CanPerformAction(company, Type))
            {
                return new ActionExecutionInformation(false, $"Company {company.Id} cannot perform turn action {Type}.", company);
            }

            return Yes;
        }

        public override CompanyMessage ProcessEvent(byte industryId, Company company, IActionData eventData)
        {
            var data = eventData as RequestExtensionActionData;

            var project = company.Projects.FirstOrDefault(p => p.Id == data.ProjectId);

            var didGetExtension = _companyService.RequestExtension(company, project);

            var message = new CompanyMessage
            {
                Id = Utilities.InvalidId,
                DateCreated = _gameClockService.CurrentDate,
                Status = CompanyMessageStatus.UnRead,
                Source = "Project Management"
            };

            if (didGetExtension)
            {
                message.Subject = "Extension Granted";

                message.Message = $"We received an extension on the {project.Definition.Name} project. It is now due {project.ExtensionDeadline.Value.ToShortDateString()}.";
            }
            else
            {
                message.Subject = "Extension Declined";

                message.Message = $"We did not receive an extension on the {project.Definition.Name} project.";
            }

            return message;
        }

        private readonly ICompanyService _companyService;

        private readonly IGameClockService _gameClockService;
    }
}