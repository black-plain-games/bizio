﻿using Bizio.Core.Model.Actions;
using Bizio.Core.Model.Companies;
using Bizio.Core.Services;
using Bizio.Core.Utilities;
using Bizio.Strategies.Actions.Data;
using System.Linq;

namespace Bizio.Strategies.Actions.Handlers
{
    public class SellPerkAction : ActionHandlerBase
    {
        public SellPerkAction(
            ICompanyService companyService,
            IGameClockService gameClockService)
            : base(ActionType.SellPerk)
        {
            _companyService = companyService;

            _gameClockService = gameClockService;
        }

        public override ActionExecutionInformation CanPerformEvent(Company company, IActionData eventData)
        {
            if (!_companyService.CanPerformAction(company, Type))
            {
                return new ActionExecutionInformation(false, $"Company {company.Id} cannot perform turn action {Type}.", company);
            }

            var data = eventData as SellPerkActionData;

            var companyPerk = company.Perks.FirstOrDefault(p => p.Id == data.CompanyPerkId);

            if (companyPerk == null)
            {
                return new ActionExecutionInformation(false, $"Company {company.Id} does not own company perk {data.CompanyPerkId}).", company);
            }

            return Yes;
        }

        public override CompanyMessage ProcessEvent(byte industryId, Company company, IActionData eventData)
        {
            var data = eventData as SellPerkActionData;

            var companyPerk = company.Perks.FirstOrDefault(p => p.Id == data.CompanyPerkId);

            _companyService.SellPerk(company, companyPerk);

            return new CompanyMessage
            {
                Id = Utilities.InvalidId,
                DateCreated = _gameClockService.CurrentDate,
                Status = CompanyMessageStatus.UnRead,
                Source = "Finance",
                Subject = "Assets Sold",
                Message = $"Sold 1 {companyPerk.Perk.Name}"
            };
        }

        private readonly ICompanyService _companyService;

        private readonly IGameClockService _gameClockService;
    }
}