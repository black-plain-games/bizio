﻿using Bizio.Core.Model.Actions;
using Bizio.Core.Model.Companies;
using Bizio.Core.Model.Projects;
using Bizio.Core.Services;
using Bizio.Strategies.Actions.Data;
using System.Linq;

namespace Bizio.Strategies.Actions.Handlers
{
    public class SubmitProjectAction : ActionHandlerBase
    {
        public SubmitProjectAction(
            ICompanyService companyService)
            : base(ActionType.SubmitProject)
        {
            _companyService = companyService;
        }

        public override ActionExecutionInformation CanPerformEvent(Company company, IActionData eventData)
        {
            if (!_companyService.CanPerformAction(company, Type))
            {
                return new ActionExecutionInformation(false, $"Company {company.Id} cannot perform turn action {Type}.", company);
            }

            return Yes;
        }

        public override CompanyMessage ProcessEvent(byte industryId, Company company, IActionData eventData)
        {
            var data = eventData as SubmitProjectActionData;

            var project = company.Projects.FirstOrDefault(p => p.Id == data.ProjectId);

            if (data.IsCancellation)
            {
                project.Result = ProjectResult.Cancelled;
            }
            else
            {
                project.Status = ProjectStatus.Completed;
            }

            return null;
        }

        private readonly ICompanyService _companyService;
    }
}