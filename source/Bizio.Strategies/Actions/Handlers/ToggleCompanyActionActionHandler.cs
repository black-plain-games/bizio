﻿using Bizio.Core.Model.Actions;
using Bizio.Core.Model.Companies;
using Bizio.Core.Services;
using Bizio.Core.Utilities;
using Bizio.Strategies.Actions.Data;
using System.Linq;

namespace Bizio.Strategies.Actions.Handlers
{
    public class ToggleCompanyActionActionHandler : ActionHandlerBase
    {
        public ToggleCompanyActionActionHandler(IGameClockService gameClockService)
            : base(ActionType.ToggleCompanyAction)
        {
            _gameClockService = gameClockService;
        }

        public override CompanyMessage ProcessEvent(byte industryId, Company company, IActionData eventData)
        {
            var data = eventData as ToggleCompanyActionActionData;

            var message = "Management has {0} investing resources in internal efforts labeled '{1}'.";

            var action = company.Actions.Single(a => a.Action.Id == data.ActionId);

            action.IsActive = !action.IsActive;

            return new CompanyMessage
            {
                Id = Utilities.InvalidId,
                DateCreated = _gameClockService.CurrentDate,
                Source = "Management",
                Status = CompanyMessageStatus.UnRead,
                Subject = "Action Status Changed",
                Message = string.Format(message, action.IsActive ? "started" : "stopped", action.Action.Name)
            };
        }

        private readonly IGameClockService _gameClockService;
    }
}