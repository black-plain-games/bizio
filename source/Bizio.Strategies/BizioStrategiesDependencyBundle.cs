﻿using Bizio.Strategies.Actions.Generators;
using Bizio.Strategies.Actions.Handlers;
using Bizio.Strategies.People.OfferEvaluators;
using Bizio.Strategies.Perks.AttributeEvaluators.Companies;
using Bizio.Strategies.Perks.AttributeEvaluators.Employees;
using Bizio.Strategies.Perks.AttributeEvaluators.People;
using Bizio.Strategies.Processors;
using Microsoft.Extensions.DependencyInjection;

namespace Bizio.Strategies
{
    public static class BizioStrategiesDependencyBundle
    {
        public static IServiceCollection RegisterStrategies(this IServiceCollection services)
        {
            return services
                .RegisterTurnProcessors()
                .RegisterActionDataGenerators()
                .RegisterActionHandlers()
                .RegisterOfferEvaluators()
                .RegisterCompanyAttributeEvaluators()
                .RegisterEmployeeAttributeEvaluators()
                .RegisterPersonAttributeEvaluators();
        }
    }
}