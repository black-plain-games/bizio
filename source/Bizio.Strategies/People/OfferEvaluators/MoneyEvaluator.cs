﻿using Bizio.Core.Model.Companies;
using Bizio.Core.Model.People;
using Bizio.Core.Services;

namespace Bizio.Strategies.People.OfferEvaluators
{
    public class MoneyEvaluator : IOfferEvaluator
    {
        public MoneyEvaluator(ICompensationService compensationService)
        {
            _compensationService = compensationService;
        }

        public OfferResponseReason? Evaluate(byte industryId, Company company, Person person, decimal amount)
        {
            var expectedCompensation = _compensationService.CalculateCompensationValue(industryId, person);

            var greed = person.GetPersonalityValue(PersonalityAttributeId.OfferGreed);

            expectedCompensation *= greed;

            if (amount >= expectedCompensation)
            {
                return null;
            }

            return OfferResponseReason.OfferValue;
        }

        private readonly ICompensationService _compensationService;
    }
}