﻿using Bizio.Core.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Bizio.Strategies.People.OfferEvaluators
{
    public static class OfferEvaluatorDependencyBundle
    {
        public static IServiceCollection RegisterOfferEvaluators(this IServiceCollection services)
        {
            return services.AddTransient<IOfferEvaluator, MoneyEvaluator>();
        }
    }
}