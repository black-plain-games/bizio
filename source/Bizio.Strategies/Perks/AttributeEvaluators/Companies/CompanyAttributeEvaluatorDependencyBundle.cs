﻿using Bizio.Core.Model.Companies;
using Bizio.Core.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Bizio.Strategies.Perks.AttributeEvaluators.Companies
{
    public static class CompanyAttributeEvaluatorDependencyBundle
    {
        public static IServiceCollection RegisterCompanyAttributeEvaluators(this IServiceCollection services)
        {
            return services
                .AddTransient<IAttributeEvaluator<Company>, CompanyMoneyEvaluator>()
                .AddTransient<IAttributeEvaluator<Company>, CompanyPerksEvaluator>()
                .AddTransient<IAttributeEvaluator<Company>, CompanyProjectsEvaluator>()
                .AddTransient<IAttributeEvaluator<Company>, CompanyProspectsEvaluator>()
                .AddTransient<IAttributeEvaluator<Company>, EmployeesEvaluator>();
        }
    }
}