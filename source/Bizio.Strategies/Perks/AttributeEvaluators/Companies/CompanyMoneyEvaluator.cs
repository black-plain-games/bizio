﻿using Bizio.Core.Model.Companies;
using Bizio.Core.Model.Perks;
using Bizio.Core.Services;

namespace Bizio.Strategies.Perks.AttributeEvaluators.Companies
{
    public class CompanyMoneyEvaluator : IAttributeEvaluator<Company>
    {
        public PerkConditionAttribute Attribute => PerkConditionAttribute.Money;

        public decimal Evaluate(Company company)
        {
            return company.Money;
        }
    }
}