﻿using Bizio.Core.Model.Companies;
using Bizio.Core.Model.Perks;
using Bizio.Core.Services;

namespace Bizio.Strategies.Perks.AttributeEvaluators.Companies
{
    public class CompanyPerksEvaluator : IAttributeEvaluator<Company>
    {
        public PerkConditionAttribute Attribute => PerkConditionAttribute.PerkCount;

        public decimal Evaluate(Company company)
        {
            return company.Perks.Count;
        }
    }
}