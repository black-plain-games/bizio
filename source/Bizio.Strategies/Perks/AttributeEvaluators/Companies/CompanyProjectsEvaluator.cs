﻿using Bizio.Core.Model.Companies;
using Bizio.Core.Model.Perks;
using Bizio.Core.Services;

namespace Bizio.Strategies.Perks.AttributeEvaluators.Companies
{
    public class CompanyProjectsEvaluator : IAttributeEvaluator<Company>
    {
        public PerkConditionAttribute Attribute => PerkConditionAttribute.ProjectCount;

        public decimal Evaluate(Company company)
        {
            return company.Projects.Count;
        }
    }
}