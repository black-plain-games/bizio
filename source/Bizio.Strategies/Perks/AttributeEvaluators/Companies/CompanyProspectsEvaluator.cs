﻿using Bizio.Core.Model.Companies;
using Bizio.Core.Model.Perks;
using Bizio.Core.Services;

namespace Bizio.Strategies.Perks.AttributeEvaluators.Companies
{
    public class CompanyProspectsEvaluator : IAttributeEvaluator<Company>
    {
        public PerkConditionAttribute Attribute => PerkConditionAttribute.ProspectCount;

        public decimal Evaluate(Company company)
        {
            return company.Prospects.Count;
        }
    }
}