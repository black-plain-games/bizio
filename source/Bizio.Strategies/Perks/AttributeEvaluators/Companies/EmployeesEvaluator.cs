﻿using Bizio.Core.Model.Companies;
using Bizio.Core.Model.Perks;
using Bizio.Core.Services;

namespace Bizio.Strategies.Perks.AttributeEvaluators.Companies
{
    public class EmployeesEvaluator : IAttributeEvaluator<Company>
    {
        public PerkConditionAttribute Attribute => PerkConditionAttribute.EmployeeCount;

        public decimal Evaluate(Company company)
        {
            return company.Employees.Count;
        }
    }
}