﻿using Bizio.Core.Model.People;
using Bizio.Core.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Bizio.Strategies.Perks.AttributeEvaluators.Employees
{
    public static class EmployeeAttributeEvaluatorDependencyBundle
    {
        public static IServiceCollection RegisterEmployeeAttributeEvaluators(this IServiceCollection services)
        {
            return services
                .AddTransient<IAttributeEvaluator<Employee>, EmployeeHappinessAttributeEvaluator>()
                .AddTransient<IAttributeEvaluator<Employee>, EmployeeSkillCountAttributeEvaluator>();
        }
    }
}