﻿using Bizio.Core.Model.People;
using Bizio.Core.Model.Perks;
using Bizio.Core.Services;

namespace Bizio.Strategies.Perks.AttributeEvaluators.Employees
{
    public class EmployeeHappinessAttributeEvaluator : IAttributeEvaluator<Employee>
    {
        public PerkConditionAttribute Attribute => PerkConditionAttribute.Happiness;

        public decimal Evaluate(Employee target)
        {
            return target.Happiness;
        }
    }
}