﻿using Bizio.Core.Model.People;
using Bizio.Core.Model.Perks;
using Bizio.Core.Services;
using System.Linq;

namespace Bizio.Strategies.Perks.AttributeEvaluators.Employees
{
    public class EmployeeSkillCountAttributeEvaluator : IAttributeEvaluator<Employee>
    {
        public PerkConditionAttribute Attribute => PerkConditionAttribute.SkillCount;

        public decimal Evaluate(Employee target)
        {
            return target.Person.Skills.Count();
        }
    }
}