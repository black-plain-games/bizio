﻿using Bizio.Core.Model.People;
using Bizio.Core.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Bizio.Strategies.Perks.AttributeEvaluators.People
{
    public static class PersonAttributeEvaluatorDependencyBundle
    {
        public static IServiceCollection RegisterPersonAttributeEvaluators(this IServiceCollection services)
        {
            return services
                .AddTransient<IAttributeEvaluator<Person>, PersonSkillCountAttributeEvaluator>();
        }
    }
}