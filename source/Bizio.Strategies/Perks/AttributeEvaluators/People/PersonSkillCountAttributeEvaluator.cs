﻿using Bizio.Core.Model.People;
using Bizio.Core.Model.Perks;
using Bizio.Core.Services;
using System.Linq;

namespace Bizio.Strategies.Perks.AttributeEvaluators.People
{
    public class PersonSkillCountAttributeEvaluator : IAttributeEvaluator<Person>
    {
        public PerkConditionAttribute Attribute => PerkConditionAttribute.SkillCount;

        public decimal Evaluate(Person target)
        {
            return target.Skills.Count();
        }
    }
}