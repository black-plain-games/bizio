﻿using Bizio.Core.Model.Companies;
using Bizio.Core.Services;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Strategies.Processors
{
    public class AdvanceGameClockProcessor : ITurnProcessor
    {
        public byte Index => byte.MaxValue;

        public AdvanceGameClockProcessor(IGameClockService gameClockService)
        {
            _gameClockService = gameClockService;
        }

        public IEnumerable<CompanyMessage> ProcessTurn(byte industryId, int gameId)
        {
            _gameClockService.ProcessTurn();

            return Enumerable.Empty<CompanyMessage>();
        }

        private readonly IGameClockService _gameClockService;
    }
}