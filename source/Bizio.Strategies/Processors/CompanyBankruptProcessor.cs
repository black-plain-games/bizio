﻿using Bizio.Core.Model.Companies;
using Bizio.Core.Model.Projects;
using Bizio.Core.Services;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Strategies.Processors
{
    public class CompanyBankruptProcessor : ITurnProcessor
    {
        public byte Index => byte.MaxValue;

        public CompanyBankruptProcessor(
            ICompanyService companyService,
            IGameClockService gameClockService)
        {
            _companyService = companyService;

            _gameClockService = gameClockService;
        }

        public IEnumerable<CompanyMessage> ProcessTurn(byte industryId, int gameId)
        {
            var didHurt = false;

            foreach (var company in _companyService.ActiveCompanies)
            {
                if (company.UserId == null && !didHurt)
                {
                    company.Money -= 10000;

                    didHurt = true;
                }

                if (company.Money <= 0)
                {
                    var remainingProjects = company.Projects.Where(p => !p.Result.HasValue && p.Status == ProjectStatus.InProgress);

                    while (remainingProjects.Any())
                    {
                        var project = remainingProjects.First();

                        project.Status = ProjectStatus.Unassigned;

                        company.Projects.Remove(project);
                    }

                    var employeeCount = company.Employees.Count;

                    while (company.Employees.Any())
                    {
                        var employee = company.Employees.First();

                        _companyService.FireEmployee(company, employee);
                    }

                    company.Status = CompanyStatus.Bankrupt;

                    if (company.UserId.HasValue)
                    {
                        // TODO: LOSE THE GAME
                        //_gameService.Status = GameStatus.UserLost;
                    }

                    yield return new CompanyMessage
                    {
                        DateCreated = _gameClockService.CurrentDate,
                        Source = "AOL News",
                        Status = CompanyMessageStatus.UnRead,
                        Subject = $"{company.Name} goes under!",
                        Message = $"{company.Name} has filed for bankruptcy, leaving {employeeCount} employees without a job."
                    };
                }
            }
        }

        private readonly ICompanyService _companyService;

        private readonly IGameClockService _gameClockService;
    }
}
