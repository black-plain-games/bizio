﻿using Bizio.Core.Model.Companies;
using Bizio.Core.Services;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Strategies.Processors
{
    public class EmployeeHappinessProcessor : ITurnProcessor
    {
        public byte Index => 0;

        public EmployeeHappinessProcessor(ICompanyService companyService)
        {
            _companyService = companyService;
        }

        public IEnumerable<CompanyMessage> ProcessTurn(byte industryId, int gameId)
        {
            foreach (var company in _companyService.ActiveCompanies)
            {
                foreach (var employee in company.Employees)
                {
                    if (employee.IsFounder)
                    {
                        continue;
                    }

                    employee.Happiness = _companyService.CalculateHappiness(industryId, employee);
                }
            }

            return Enumerable.Empty<CompanyMessage>();
        }

        private readonly ICompanyService _companyService;
    }
}