﻿using Bizio.Core.Model.Companies;
using Bizio.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Strategies.Processors
{
    public class EmployeeSkillChangeProcessor : ITurnProcessor
    {
        private readonly ICompanyService _companyService;

        private readonly IPersonService _personService;
        private readonly ISkillDefinitionService _skillDefinitionService;
        private readonly IGameClockService _gameClockService;

        public byte Index => 1;

        public EmployeeSkillChangeProcessor(
            ICompanyService companyService,
            IPersonService personService,
            ISkillDefinitionService skillDefinitionService,
            IGameClockService gameClockService)
        {
            _companyService = companyService;

            _personService = personService;
            _skillDefinitionService = skillDefinitionService;
            _gameClockService = gameClockService;
        }

        public IEnumerable<CompanyMessage> ProcessTurn(byte industryId, int gameId)
        {
            var output = new List<CompanyMessage>();

            var mandatorySkillDefinitions = _skillDefinitionService.GetMandatorySkillDefinitions(industryId);

            foreach (var company in _companyService.ActiveCompanies)
            {
                foreach (var employee in company.Employees)
                {
                    var allocations = company.Allocations.Where(a => a.Employee.Person.Id == employee.Person.Id);

                    foreach (var skill in employee.Person.Skills.Where(s => !mandatorySkillDefinitions.Contains(s.SkillDefinition)))
                    {
                        var learnRate = ((decimal)skill.LearnRate) / 255m;

                        var forgetRate = ((decimal)skill.ForgetRate) / 255m;

                        decimal allocationCount = allocations.Count(a => a.Project.Requirements.Any(r => r.SkillDefinition.Id == skill.SkillDefinition.Id));

                        var newSkillValue = 0m;

                        if (allocationCount == 0)
                        {
                            newSkillValue = Math.Max(0, skill.Value - forgetRate);
                        }
                        else
                        {
                            newSkillValue = Math.Min(skill.SkillDefinition.Value.Maximum, skill.Value + (allocationCount * learnRate));
                        }

                        if (newSkillValue == skill.SkillDefinition.Value.Maximum &&
                            skill.Value != skill.SkillDefinition.Value.Maximum)
                        {
                            output.Add(new CompanyMessage
                            {
                                DateCreated = _gameClockService.CurrentDate,
                                Source = "Employee Development",
                                Subject = $"{employee.Person.FirstName} {employee.Person.LastName} has mastered {skill.SkillDefinition.Name}",
                                Message = $"{employee.Person.FirstName} {employee.Person.LastName} has raised their {skill.SkillDefinition.Name} skill to the highest ever seen. Make sure they're compensated fairly since talent like this doesn't come around very often.",
                                Status = CompanyMessageStatus.UnRead
                            });
                        }

                        skill.Value = newSkillValue;
                    }

                    var unmetRequirements = allocations.SelectMany(a => a.Project.Requirements.Where(r => !employee.Person.Skills.Any(s => s.SkillDefinition.Id == r.SkillDefinition.Id))).Distinct();

                    foreach (var requirement in unmetRequirements)
                    {
                        _personService.AddSkill(employee.Person, requirement.SkillDefinition);
                    }
                }
            }

            return output;
        }
    }
}
