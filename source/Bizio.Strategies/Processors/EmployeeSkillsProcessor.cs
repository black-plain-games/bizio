﻿using Bizio.Core.Model.Companies;
using Bizio.Core.Services;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Strategies.Processors
{
    public class EmployeeSkillsProcessor : ITurnProcessor
    {
        public byte Index => 0;

        public EmployeeSkillsProcessor(ICompanyService companyService,
            IPerkService perkService)
        {
            _companyService = companyService;

            _perkService = perkService;
        }

        public IEnumerable<CompanyMessage> ProcessTurn(byte industryId, int gameId)
        {
            void ProcessActionAccumulations(Company company, IDictionary<byte, decimal> accumulations)
            {
                void UpdateCompanyAction(CompanyAction action)
                {
                    var requirements = action.Action;

                    if (requirements != null)
                    {
                        var meetsRequirements = true;

                        while (meetsRequirements && action.Count < requirements.MaxCount)
                        {
                            foreach (var requirement in requirements.Requirements)
                            {
                                var accumulation = action.Accumulations.FirstOrDefault(a => a.SkillDefinition == requirement.SkillDefinition);

                                if (accumulation == null ||
                                    accumulation.Value < requirement.Value)
                                {
                                    meetsRequirements = false;

                                    break;
                                }
                            }

                            if (meetsRequirements)
                            {
                                action.Count++;

                                foreach (var requirement in requirements.Requirements)
                                {
                                    var accumulation = action.Accumulations.FirstOrDefault(a => a.SkillDefinition == requirement.SkillDefinition);

                                    accumulation.Value -= requirement.Value;
                                }
                            }
                        }
                    }
                }

                decimal CalculateSkill(CompanyAction companyAction, byte skillDefinitionId)
                {
                    var accumulation = companyAction.Accumulations.FirstOrDefault(a => a.SkillDefinition.Id == skillDefinitionId);

                    if (accumulation == null)
                    {
                        return 0;
                    }

                    var requirement = companyAction.Action.Requirements.FirstOrDefault(r => r.SkillDefinition.Id == skillDefinitionId);

                    return requirement.Value - accumulation.Value;
                }

                var activeActions = company.Actions.Where(a => a.IsActive);

                foreach (var accumulation in accumulations)
                {
                    var actions = activeActions.Where(a => CalculateSkill(a, accumulation.Key) > 0);

                    if (!actions.Any())
                    {
                        continue;
                    }

                    foreach (var companyAction in actions)
                    {
                        var amount = accumulation.Value / actions.Count();

                        var actionAccumulation = companyAction.Accumulations.FirstOrDefault(a => a.SkillDefinition.Id == accumulation.Key);

                        actionAccumulation.Value += amount;
                    }
                }

                foreach (var action in company.Actions)
                {
                    UpdateCompanyAction(action);
                }
            }

            // sum up all employee skills
            // distribute them according to allocations
            // add whatever remains unused to "accumulations"
            // apply accumulated skills to company actions

            foreach (var company in _companyService.ActiveCompanies)
            {
                var accumulations = new Dictionary<byte, decimal>();

                foreach (var employee in company.Employees)
                {
                    var allocations = company.Allocations.Where(a => a.Employee == employee);

                    var skillValues = new Dictionary<byte, decimal>();

                    var remainingSkillValues = new Dictionary<byte, decimal>();

                    foreach (var skill in employee.Person.Skills)
                    {
                        var baseSkillValue = employee.Person.GetSkillValue(skill.SkillDefinition.Id);

                        var skillValue = baseSkillValue;

                        foreach (var perk in company.Perks)
                        {
                            if (!_perkService.DoesPerkApplyTo(perk.Perk, employee.Person) ||
                                !_perkService.DoesPerkApplyTo(perk.Perk, employee) ||
                                !_perkService.DoesPerkApplyTo(perk.Perk, company))
                            {
                                continue;
                            }
                        }

                        skillValues.Add(skill.SkillDefinition.Id, skillValue);

                        remainingSkillValues.Add(skill.SkillDefinition.Id, skillValue);
                    }

                    foreach (var allocation in allocations)
                    {
                        var allocationPercent = ((decimal)allocation.Percent) / 255m;
                        foreach (var requirement in allocation.Project.Requirements)
                        {
                            if (skillValues.ContainsKey(requirement.SkillDefinition.Id))
                            {
                                var value = allocationPercent * skillValues[requirement.SkillDefinition.Id];

                                requirement.CurrentValue += value;

                                remainingSkillValues[requirement.SkillDefinition.Id] -= value;
                            }
                        }
                    }

                    foreach (var skill in remainingSkillValues)
                    {
                        if (accumulations.ContainsKey(skill.Key))
                        {
                            accumulations[skill.Key] += skill.Value;
                        }
                        else
                        {
                            accumulations.Add(skill.Key, skill.Value);
                        }
                    }
                }

                ProcessActionAccumulations(company, accumulations);
            }

            return Enumerable.Empty<CompanyMessage>();
        }

        private readonly ICompanyService _companyService;

        private readonly IPerkService _perkService;
    }
}