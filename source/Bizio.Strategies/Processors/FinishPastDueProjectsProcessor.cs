﻿using Bizio.Core.Model.Companies;
using Bizio.Core.Model.Projects;
using Bizio.Core.Services;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Strategies.Processors
{
    public class FinishPastDueProjectsProcessor : ITurnProcessor
    {
        public byte Index => 0;

        public FinishPastDueProjectsProcessor(
            IProjectService projectService,
            IGameClockService gameClockService)
        {
            _projectService = projectService;

            _gameClockService = gameClockService;
        }

        public IEnumerable<CompanyMessage> ProcessTurn(byte industryId, int gameId)
        {
            foreach (var project in _projectService.Projects)
            {
                if (project.Status == ProjectStatus.Unassigned &&
                    _gameClockService.DateToTurnDifference(project.ExtensionDeadline ?? project.Deadline) <= 0)
                {
                    project.Status = ProjectStatus.Completed;
                }
            }

            return Enumerable.Empty<CompanyMessage>();
        }

        private readonly IProjectService _projectService;

        private readonly IGameClockService _gameClockService;
    }
}