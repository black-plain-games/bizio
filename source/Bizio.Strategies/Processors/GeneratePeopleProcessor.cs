﻿using Bizio.Core.Data.Configuration;
using Bizio.Core.Model.Companies;
using Bizio.Core.Services;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Strategies.Processors
{
    public class GeneratePeopleProcessor : ITurnProcessor
    {
        public byte Index => 1;

        public GeneratePeopleProcessor(
            IPersonService personService,
            IConfigurationService configurationService)
        {
            _personService = personService;

            _configurationService = configurationService;
        }

        public IEnumerable<CompanyMessage> ProcessTurn(byte industryId, int gameId)
        {
            var minimumPersonCount = _configurationService.GetIntValue(gameId, ConfigurationKey.MinimumPersonCount);

            var nonRetiredPersonCount = _personService.People.Count(p => !_personService.IsRetired(p) && _personService.IsUnemployed(p));

            var numberOfPeopleToGenerate = minimumPersonCount - nonRetiredPersonCount;

            if (numberOfPeopleToGenerate > 0)
            {
                _personService.GeneratePeople(_gameService.CurrentIndustry.Id, numberOfPeopleToGenerate);
            }

            return Enumerable.Empty<CompanyMessage>();
        }

        private readonly IPersonService _personService;

        private readonly IConfigurationService _configurationService;

        private readonly IGameService _gameService;
    }
}