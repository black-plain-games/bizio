﻿using Bizio.Core.Data.Configuration;
using Bizio.Core.Model.Companies;
using Bizio.Core.Model.Projects;
using Bizio.Core.Services;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Strategies.Processors
{
    public class GenerateProjectsProcessor : ITurnProcessor
    {
        public byte Index => 1;

        public GenerateProjectsProcessor(
            IProjectService projectService,
            IIndustryService industryService,
            IConfigurationService configurationService)
        {
            _projectService = projectService;
            _industryService = industryService;
            _configurationService = configurationService;
        }

        public IEnumerable<CompanyMessage> ProcessTurn(byte industryId, int gameId)
        {
            var industry = _industryService.GetIndustry(industryId);

            if (_projectService.ShouldGenerateZeroStarProject)
            {
                _projectService.GenerateProjects(industry, 1);
            }

            var minimumProjectCount = _configurationService.GetIntValue(gameId, ConfigurationKey.MinimumProjectCount);

            var numberOfProjectsToGenerate = minimumProjectCount - _projectService.Projects.Count(p => p.Status == ProjectStatus.Unassigned);

            if (numberOfProjectsToGenerate > 0)
            {
                _projectService.GenerateProjects(industry, numberOfProjectsToGenerate);
            }

            return Enumerable.Empty<CompanyMessage>();
        }

        private readonly IProjectService _projectService;
        private readonly IIndustryService _industryService;
        private readonly IConfigurationService _configurationService;
    }
}