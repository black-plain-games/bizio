﻿using Bizio.Core.Data.Configuration;
using Bizio.Core.Model.Companies;
using Bizio.Core.Services;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Strategies.Processors
{
    public class PayrollProcessor : ITurnProcessor
    {
        public byte Index => 0;

        public PayrollProcessor(
            ICompanyService companyService,
            IConfigurationService configurationService)
        {
            _companyService = companyService;

            _configurationService = configurationService;
        }

        public IEnumerable<CompanyMessage> ProcessTurn(byte industryId, int gameId)
        {
            var maintenanceCost = _configurationService.GetDecimalValue(gameId, ConfigurationKey.EmployeeMaintenanceCost);

            foreach (var company in _companyService.ActiveCompanies)
            {
                foreach (var employee in company.Employees)
                {
                    _companyService.ProcessTransaction(company, TransactionType.Maintenance, -maintenanceCost, $"Maintenance cost for {employee.Person.FirstName} {employee.Person.LastName}.");

                    if (!employee.IsFounder && employee.Salary != 0)
                    {
                        _companyService.ProcessTransaction(company, TransactionType.Payroll, -employee.Salary, $"Paid {employee.Person.FirstName} {employee.Person.LastName} {employee.Salary:C}.");
                    }
                }
            }

            return Enumerable.Empty<CompanyMessage>();
        }

        private readonly ICompanyService _companyService;

        private readonly IConfigurationService _configurationService;
    }
}