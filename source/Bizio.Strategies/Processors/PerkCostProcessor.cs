﻿using Bizio.Core.Model.Companies;
using Bizio.Core.Services;
using Bizio.Core.Utilities;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Strategies.Processors
{
    public class PerkCostProcessor : ITurnProcessor
    {
        public byte Index => 0;

        public PerkCostProcessor(
            ICompanyService companyService,
            IGameClockService gameClockService)
        {
            _companyService = companyService;

            _gameClockService = gameClockService;
        }

        public IEnumerable<CompanyMessage> ProcessTurn(byte industryId, int gameId)
        {
            foreach (var company in _companyService.ActiveCompanies)
            {
                foreach (var perk in company.Perks.Where(p => p.NextPaymentDate.HasValue && p.NextPaymentDate <= _gameClockService.CurrentDate))
                {
                    while (perk.NextPaymentDate <= _gameClockService.CurrentDate)
                    {
                        var message = $"Charged {perk.Perk.RecurringCost:C} for {perk.Perk.Name}.";

                        _companyService.ProcessTransaction(company, TransactionType.Perk, -perk.Perk.RecurringCost.Value, message);

                        perk.NextPaymentDate = Utilities.GetDate(perk.NextPaymentDate.Value, perk.Perk.RecurringCostInterval.Value);
                    }
                }
            }

            return Enumerable.Empty<CompanyMessage>();
        }

        private readonly ICompanyService _companyService;

        private readonly IGameClockService _gameClockService;
    }
}