﻿using Bizio.Core.Model.Companies;
using Bizio.Core.Services;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Strategies.Processors
{
    public class PersonRetirementProcessor : ITurnProcessor
    {
        public byte Index => 1;

        public PersonRetirementProcessor(
            IPersonService personService,
            IGameClockService gameClockService)
        {
            _personService = personService;

            _gameClockService = gameClockService;
        }

        public IEnumerable<CompanyMessage> ProcessTurn(byte industryId, int gameId)
        {
            foreach (var person in _personService.People.Where(p => !_personService.IsRetired(p)))
            {
                if (person.RetirementDate > _gameClockService.CurrentDate)
                {
                    continue;
                }

                var currentWorkHistory = person.WorkHistory.FirstOrDefault(wh => !wh.EndDate.HasValue);

                if (currentWorkHistory != null)
                {
                    var employee = currentWorkHistory.Company.Employees.FirstOrDefault(p => p.Person == person);

                    if (!employee.IsFounder)
                    {
                        currentWorkHistory.EndDate = _gameClockService.CurrentDate;

                        currentWorkHistory.EndingSalary = employee.Salary;
                    }
                }
            }

            return Enumerable.Empty<CompanyMessage>();
        }

        private readonly IPersonService _personService;

        private readonly IGameClockService _gameClockService;
    }
}