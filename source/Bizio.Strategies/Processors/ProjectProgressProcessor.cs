﻿using Bizio.Core.Data.Configuration;
using Bizio.Core.Model.Companies;
using Bizio.Core.Model.Projects;
using Bizio.Core.Services;
using System;
using System.Collections.Generic;

namespace Bizio.Strategies.Processors
{
    public class ProjectProgressProcessor : ITurnProcessor
    {
        public byte Index => 1;

        public ProjectProgressProcessor(
            ICompanyService companyService,
            IGameClockService gameClockService,
            IConfigurationService configurationService,
            IProjectService projectService)
        {
            _companyService = companyService;

            _gameClockService = gameClockService;

            _configurationService = configurationService;

            _projectService = projectService;
        }

        public IEnumerable<CompanyMessage> ProcessTurn(byte industryId, int gameId)
        {
            var messages = new List<CompanyMessage>();

            var projectsInProgress = new List<int>();

            foreach (var company in _companyService.ActiveCompanies)
            {
                foreach (var project in company.Projects)
                {
                    projectsInProgress.Add(project.Id);

                    var isProjectPastDue = _gameClockService.DateToTurnDifference(project.ExtensionDeadline ?? project.Deadline) <= 0;

                    // If you let the project expire, you failed
                    if (project.Status == ProjectStatus.InProgress && isProjectPastDue)
                    {
                        project.Result = ProjectResult.Failure;
                    }

                    // Don't calculate anything for projects that are ongoing or completed before now
                    if ((project.Status == ProjectStatus.InProgress && !project.Result.HasValue) ||
                        (project.Status == ProjectStatus.Completed && project.Result.HasValue))
                    {
                        continue;
                    }

                    project.Status = ProjectStatus.Completed;

                    if (project.Result == null)
                    {
                        project.Result = ProjectResult.Success;
                    }

                    var targetPoints = 0m;

                    var actualPoints = 0m;

                    foreach (var requirement in project.Requirements)
                    {
                        targetPoints += requirement.TargetValue;

                        // We cap single requirement accumulation to the target value to prevent
                        // someone from overloading one skill to make up for completely empty requirements
                        actualPoints += Math.Min(requirement.TargetValue, requirement.CurrentValue);
                    }

                    var completionPercent = actualPoints / targetPoints;

                    var baseValue = completionPercent * project.Value;

                    var baseStarsEarned = completionPercent * (decimal)project.ReputationRequired;

                    var resultCoefficient = 0m;

                    var status = string.Empty;

                    switch (project.Result)
                    {
                        case ProjectResult.Success:
                            status = "completing";
                            resultCoefficient = 1;
                            break;

                        case ProjectResult.Cancelled:
                            status = "cancelling";
                            resultCoefficient = _configurationService.GetDecimalValue(gameId, ConfigurationKey.ProjectCancelledPercent);
                            break;

                        case ProjectResult.Failure:
                            status = "failing";
                            resultCoefficient = _configurationService.GetDecimalValue(gameId, ConfigurationKey.ProjectFailedPercent);
                            break;
                    }

                    if (project.ExtensionDeadline.HasValue)
                    {
                        resultCoefficient -= _configurationService.GetDecimalValue(gameId, ConfigurationKey.ProjectExtensionValueAdjustmentPercent);

                        status += ", after an extension,";
                    }

                    var actualValue = baseValue * resultCoefficient;

                    var actualStarsEarned = baseStarsEarned * resultCoefficient;

                    _companyService.ProcessTransaction(company, TransactionType.Project, actualValue, $"{status} the {project.Definition.Name} project");

                    var previousStars = company.Reputation.Value * 5m;

                    company.Reputation.EarnedStars += actualStarsEarned;

                    company.Reputation.PossibleStars += (int)project.ReputationRequired;

                    var currentStars = company.Reputation.Value * 5m;

                    if (company.UserId.HasValue)
                    {
                        messages.Add(new CompanyMessage
                        {
                            DateCreated = _gameClockService.CurrentDate,
                            Source = "Project Management",
                            Status = CompanyMessageStatus.UnRead,
                            Subject = $"{project.Definition.Name} project result",
                            Message = $"We earned {actualValue:C} by {status} the {project.Definition.Name} project. Our reputation went from {previousStars:0.##} stars to {currentStars:0.##} stars."
                        });
                    }
                }
            }

            foreach (var project in _projectService.Projects)
            {
                if (projectsInProgress.Contains(project.Id))
                {
                    continue;
                }

                var deadline = project.ExtensionDeadline ?? project.Deadline;

                if (deadline <= _gameClockService.CurrentDate)
                {
                    project.Status = ProjectStatus.Completed;

                    project.Result = ProjectResult.Cancelled;
                }
            }

            return messages;
        }

        private readonly ICompanyService _companyService;

        private readonly IGameClockService _gameClockService;

        private readonly IProjectService _projectService;

        private readonly IConfigurationService _configurationService;
    }
}