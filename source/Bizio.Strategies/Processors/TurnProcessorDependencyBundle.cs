﻿using Bizio.Core.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Bizio.Strategies.Processors
{
    public static class TurnProcessorDependencyBundle
    {
        public static IServiceCollection RegisterTurnProcessors(this IServiceCollection services)
        {
            return services
                .AddTransient<ITurnProcessor, AdvanceGameClockProcessor>()
                .AddTransient<ITurnProcessor, EmployeeSkillsProcessor>()
                .AddTransient<ITurnProcessor, GeneratePeopleProcessor>()
                .AddTransient<ITurnProcessor, GenerateProjectsProcessor>()
                .AddTransient<ITurnProcessor, PayrollProcessor>()
                .AddTransient<ITurnProcessor, PerkCostProcessor>()
                .AddTransient<ITurnProcessor, PersonRetirementProcessor>()
                .AddTransient<ITurnProcessor, ProjectProgressProcessor>()
                .AddTransient<ITurnProcessor, FinishPastDueProjectsProcessor>()
                .AddTransient<ITurnProcessor, EmployeeHappinessProcessor>()
                .AddTransient<ITurnProcessor, EmployeeSkillChangeProcessor>()
                .AddTransient<ITurnProcessor, CompanyBankruptProcessor>();
        }
    }
}