﻿using Bizio.Core;
using Bizio.Repositories;
using BlackPlain.Bizio.Services;
using Microsoft.Extensions.DependencyInjection;

namespace BlackPlain.Bizio.ConsoleTestApp
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            UIManager.SetTaskScheduler();

            var services = new ServiceCollection()
                .AddBizioRepositories()
                .AddBizioServices()
                .AddTransient<ITestService, TestService>();

            using (var provider = services.BuildServiceProvider())
            {
                await provider
                    .GetRequiredService<ITestService>()
                    .RunAsync();
            }
        }
    }
}
