﻿using Bizio.Core.Model.People;
using Bizio.Core.Services;
using Bizio.Core.Utilities;
using Bizio.Repositories;
using BlackPlain.Bizio.Services;
using BlackPlain.Bizio.Services.Actions.Data;
using D = Bizio.Core.Data;

namespace BlackPlain.Bizio.ConsoleTestApp
{
    internal interface ITestService
    {
        Task RunAsync(CancellationToken cancellationToken = default);
    }

    internal class TestService(
        IStaticDataService _staticDataService,
        IConfigurationService _configurationService,
        IGameService _gameService,
        IDefaultDataGenerator _defaultDataGenerator,
        ITurnService _turnService
        ) : ITestService
    {
        private Guid Id(GuidUse key) => _defaultDataGenerator.Id(key);

        public async Task RunAsync(CancellationToken cancellationToken = default)
        {
            await _defaultDataGenerator.GenerateDefaultBizioData(cancellationToken);
            await _defaultDataGenerator.GenerateDefaultConfigurationData(cancellationToken);

            await _configurationService.LoadAsync(cancellationToken);
            await _staticDataService.LoadAsync(cancellationToken);

            var userId = Guid.NewGuid();

            var industry = _staticDataService.StaticData.Industries.FirstOrDefault() ?? throw new Exception("No industry available");
            var personality = _staticDataService.StaticData.Personalities.FirstOrDefault() ?? throw new Exception("No personalities available");

            var skills = new Dictionary<Guid, int>
            {
                { Id(GuidUse.SkillDefinitionAdministration), 10 },
                { Id(GuidUse.SkillDefinitionAccounting), 11 },
                { Id(GuidUse.SkillDefinitionCommunication), 12 },
                { Id(GuidUse.SkillDefinitionResearch), 13 },
                { Id(GuidUse.SkillDefinitionWriting), 14},

                { Id(GuidUse.SkillDefinitionServerProgramming), 15 },
                { Id(GuidUse.SkillDefinitionDatabaseProgramming), 16 },
                { Id(GuidUse.SkillDefinitionInterfaceProgramming), 17 },
            };
            var founderParams = new CreatePersonParameters("Will", "Custode", D.People.Gender.Male, new DateTime(1991, 9, 20), personality.Id, skills);

            var data = new CreateGameParameters(userId, DateTime.Today, industry.Id, 1, founderParams, "Test Co", 100000, 5, new Dictionary<D.Configuration.ConfigurationKey, string>());

            var gameId = await _gameService.CreateAsync(data, cancellationToken);

            var game = await _gameService.LoadAsync(gameId, cancellationToken);

            var project = game.Projects.OrderBy(p => p.ReputationRequired).First();
            var userCompany = game.Companies.FirstOrDefault(c => c.UserId == userId);
            var founder = userCompany.Employees.First();
            userCompany.Actions.FirstOrDefault(a => a.Action.Type == D.Actions.ActionType.InterviewProspect).Count = 100;
            userCompany.Actions.FirstOrDefault(a => a.Action.Type == D.Actions.ActionType.PurchasePerk).Count = 100;
            userCompany.Actions.FirstOrDefault(a => a.Action.Type == D.Actions.ActionType.RequestExtension).Count = 100;

            userCompany.Reputation.EarnedStars = 5;
            userCompany.Reputation.PossibleStars = 5;

            userCompany.TurnActions.Add(new AcceptProjectActionData(project.Id));

            var employee1 = userCompany.Employees.First();

            foreach (var requirement in project.Requirements)
            {
                var skill = employee1.Person.Skills.FirstOrDefault(s => s.SkillDefinition == requirement.SkillDefinition);

                if (skill != null)
                {
                    skill.Value = requirement.TargetValue / 4;
                }
                else
                {
                    employee1.Person.Skills.Add(new Skill
                    {
                        SkillDefinition = requirement.SkillDefinition,
                        Value = requirement.TargetValue / 4
                    });
                }
            }

            userCompany.TurnActions.Add(new AdjustAllocationActionData(employee1.Id, project.Id, 75, D.Companies.ProjectRole.Lead));

            _turnService.ProcessTurn(game);

            userCompany.TurnActions.Add(new RequestExtensionActionData(project.Id, 5));

            _turnService.ProcessTurn(game);
            _turnService.ProcessTurn(game);
            _turnService.ProcessTurn(game);
            _turnService.ProcessTurn(game);
            _turnService.ProcessTurn(game);
            _turnService.ProcessTurn(game);
            _turnService.ProcessTurn(game);
            _turnService.ProcessTurn(game);

            userCompany.TurnActions.Add(new SubmitProjectActionData(project.Id));

            _turnService.ProcessTurn(game);

            var person = game.People.Random();

            userCompany.TurnActions.Add(new MakeOfferActionData(person.Id, 1000));

            _turnService.ProcessTurn(game);

            var employee2 = userCompany.Employees.FirstOrDefault(e => e.Person == person);

            userCompany.TurnActions.Add(new AdjustSalaryActionData(employee2.Id, 2000));

            _turnService.ProcessTurn(game);

            userCompany.TurnActions.Add(new FireEmployeeActionData(employee2.Id));

            _turnService.ProcessTurn(game);

            var person2 = game.People.Random();

            userCompany.TurnActions.Add(new InterviewProspectActionData(person.Id));

            _turnService.ProcessTurn(game);

            userCompany.TurnActions.Add(new InterviewProspectActionData(person.Id));

            _turnService.ProcessTurn(game);

            userCompany.TurnActions.Add(new InterviewProspectActionData(person.Id));

            _turnService.ProcessTurn(game);

            var perk = game.Industry.Perks.Random();

            userCompany.TurnActions.Add(new PurchasePerkActionData(perk.Id, 8));

            _turnService.ProcessTurn(game);

            userCompany.TurnActions.Add(new SellPerkActionData(perk.Id, 3));

            _turnService.ProcessTurn(game);
        }
    }
}
