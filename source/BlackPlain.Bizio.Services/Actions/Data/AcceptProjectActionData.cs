﻿using Bizio.Core.Data.Actions;
using Bizio.Core.Model.Companies;
using Newtonsoft.Json;

namespace BlackPlain.Bizio.Services.Actions.Data
{
    public class AcceptProjectActionData(Guid projectId) : IActionData
    {
        public ActionType ActionType => ActionType.AcceptProject;
        public Guid ProjectId { get; set; } = projectId;

        public string Serialize() => JsonConvert.SerializeObject(this);
    }
}
