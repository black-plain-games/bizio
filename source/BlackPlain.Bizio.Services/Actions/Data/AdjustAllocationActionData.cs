﻿using Bizio.Core.Data.Actions;
using Bizio.Core.Model.Companies;
using Newtonsoft.Json;
using D = Bizio.Core.Data;

namespace BlackPlain.Bizio.Services.Actions.Data
{
    public class AdjustAllocationActionData(Guid employeeId, Guid projectId, int percentage, D.Companies.ProjectRole role) : IActionData
    {
        public ActionType ActionType => ActionType.AdjustAllocation;

        public Guid EmployeeId { get; set; } = employeeId;
        public Guid ProjectId { get; set; } = projectId;
        public int Percentage { get; set; } = percentage;
        public D.Companies.ProjectRole Role { get; set; } = role;

        public string Serialize() => JsonConvert.SerializeObject(this);
    }
}
