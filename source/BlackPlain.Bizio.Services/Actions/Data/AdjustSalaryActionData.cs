﻿using Bizio.Core.Data.Actions;
using Bizio.Core.Model.Companies;
using Newtonsoft.Json;

namespace BlackPlain.Bizio.Services.Actions.Data
{
    public class AdjustSalaryActionData(Guid employeeId, int salary) : IActionData
    {
        public ActionType ActionType => ActionType.AdjustSalary;

        public Guid EmployeeId { get; set; } = employeeId;
        public int Salary { get; set; } = salary;

        public string Serialize() => JsonConvert.SerializeObject(this);
    }
}
