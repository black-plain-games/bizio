﻿using Bizio.Core.Data.Actions;
using Bizio.Core.Model.Companies;
using Newtonsoft.Json;

namespace BlackPlain.Bizio.Services.Actions.Data
{
    public class FireEmployeeActionData(Guid employeeId) : IActionData
    {
        public ActionType ActionType => ActionType.FireEmployee;

        public Guid EmployeeId { get; set; } = employeeId;

        public string Serialize() => JsonConvert.SerializeObject(this);
    }
}
