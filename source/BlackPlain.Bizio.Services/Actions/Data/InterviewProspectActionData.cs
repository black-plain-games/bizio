﻿using Bizio.Core.Data.Actions;
using Bizio.Core.Model.Companies;
using Newtonsoft.Json;

namespace BlackPlain.Bizio.Services.Actions.Data
{
    public class InterviewProspectActionData(Guid personId) : IActionData
    {
        public ActionType ActionType => ActionType.InterviewProspect;

        public Guid PersonId { get; set; } = personId;

        public string Serialize() => JsonConvert.SerializeObject(this);
    }
}
