﻿using Bizio.Core.Data.Actions;
using Bizio.Core.Model.Companies;
using Newtonsoft.Json;

namespace BlackPlain.Bizio.Services.Actions.Data
{
    public class MakeOfferActionData(Guid personId, int salary) : IActionData
    {
        public ActionType ActionType => ActionType.MakeOffer;

        public Guid PersonId { get; set; } = personId;
        public int Salary { get; set; } = salary;

        public string Serialize() => JsonConvert.SerializeObject(this);
    }
}
