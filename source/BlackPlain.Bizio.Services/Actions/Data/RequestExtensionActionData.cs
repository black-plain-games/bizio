﻿using Bizio.Core.Data.Actions;
using Bizio.Core.Model.Companies;
using Newtonsoft.Json;

namespace BlackPlain.Bizio.Services.Actions.Data
{
    public class RequestExtensionActionData(Guid projectId, int turns) : IActionData
    {
        public ActionType ActionType => ActionType.RequestExtension;

        public Guid ProjectId { get; set; } = projectId;
        public int Turns { get; set; } = turns;

        public string Serialize() => JsonConvert.SerializeObject(this);
    }
}
