﻿using Bizio.Core.Data.Actions;
using Bizio.Core.Model.Companies;
using Newtonsoft.Json;

namespace BlackPlain.Bizio.Services.Actions.Data
{
    public class SellPerkActionData(Guid perkId, int quantity) : IActionData
    {
        public ActionType ActionType => ActionType.SellPerk;

        public Guid PerkId { get; set; } = perkId;
        public int Quantity { get; set; } = quantity;

        public string Serialize() => JsonConvert.SerializeObject(this);
    }
}
