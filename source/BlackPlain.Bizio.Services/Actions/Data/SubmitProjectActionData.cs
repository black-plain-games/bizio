﻿using Bizio.Core.Data.Actions;
using Bizio.Core.Model.Companies;
using Newtonsoft.Json;

namespace BlackPlain.Bizio.Services.Actions.Data
{
    public class SubmitProjectActionData(Guid projectId) : IActionData
    {
        public ActionType ActionType => ActionType.SubmitProject;

        public Guid ProjectId { get; set; } = projectId;

        public string Serialize() => JsonConvert.SerializeObject(this);
    }
}
