﻿using Bizio.Core;
using Bizio.Core.Data.Projects;
using Bizio.Core.Model.Companies;
using Bizio.Core.Model.Game;
using BlackPlain.Bizio.Services.Actions.Data;
using D = Bizio.Core.Data;

namespace BlackPlain.Bizio.Services.Actions.Handlers
{
    internal class AcceptProjectActionHandler() : ActionHandlerBase(D.Actions.ActionType.AcceptProject, "Project Management")
    {
        public override CompanyMessage ProcessAction(IActionData data, Game game, Company company)
        {
            var projectActionData = Interpret<AcceptProjectActionData>(data);

            var project = game.Projects.Find(projectActionData.ProjectId);

            if (project == null)
            {
                return Message(game, "Project not found", "Sample text");
            }

            var reputation = Math.Round(company.Reputation.Value, 1, MidpointRounding.ToPositiveInfinity);
            var required = (int)project.ReputationRequired - 1;

            if (reputation < required)
            {
                return Message(game, $"Not enough experience for this project (required {required:0.00}, have {reputation:0.00})", "Sample text");
            }

            game.Projects.Remove(project);

            project.Status = ProjectStatus.InProgress;
            company.Projects.Add(project);

            return Message(game, "We got the project", "Sample text");
        }
    }
}
