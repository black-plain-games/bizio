﻿using Bizio.Core.Data.Actions;
using Bizio.Core.Model.Companies;
using Bizio.Core.Model.Game;
using Bizio.Core.Services;
using D = Bizio.Core.Data;

namespace BlackPlain.Bizio.Services.Actions.Handlers
{
    internal abstract class ActionHandlerBase(ActionType type, string messageSource) : IActionHandler
    {
        public ActionType Type => type;

        public virtual ActionExecutionInformation CanPerformAction(Company company, IActionData data)
        {
            if (HasEnoughActionCount(company))
            {
                return Yes;
            }

            return No("Not enough action points to perform this action!");
        }

        public abstract CompanyMessage ProcessAction(IActionData data, Game game, Company company);

        protected bool HasEnoughActionCount(Company company)
        {
            var action = company.Actions.FirstOrDefault(a => a.Action.Type == Type) ?? throw new Exception("Action not found");

            return action.Count > 0;
        }

        protected CompanyMessage Message(Game game, string subject, string message) => new()
        {
            Id = Guid.NewGuid(),
            DateCreated = game.CurrentDate,
            Source = messageSource,
            Subject = subject,
            Status = D.Companies.CompanyMessageStatus.UnRead,
            Message = message
        };

        protected static T Interpret<T>(IActionData data) where T : class
        {
            return data as T ?? throw new Exception("Invalid data type provided");
        }

        protected static readonly ActionExecutionInformation Yes = new() { CanPerformAction = true };
        protected static ActionExecutionInformation No(string message) => new() { CanPerformAction = false, Message = message };
    }
}
