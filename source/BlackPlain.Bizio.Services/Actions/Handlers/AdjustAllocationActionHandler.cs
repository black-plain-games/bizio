﻿using Bizio.Core;
using Bizio.Core.Model.Companies;
using Bizio.Core.Model.Game;
using BlackPlain.Bizio.Services.Actions.Data;
using D = Bizio.Core.Data;

namespace BlackPlain.Bizio.Services.Actions.Handlers
{
    internal class AdjustAllocationActionHandler() : ActionHandlerBase(D.Actions.ActionType.AdjustAllocation, "Human Resources")
    {
        public override CompanyMessage ProcessAction(IActionData data, Game game, Company company)
        {
            var action = Interpret<AdjustAllocationActionData>(data);

            var project = company.Projects.Find(action.ProjectId);

            if (project == null)
            {
                return Message(game, "Could not adjust allocation", $"Project {action.ProjectId} not found");
            }

            var employee = company.Employees.Find(action.EmployeeId);

            if (employee == null)
            {
                return Message(game, "Could not adjust allocation", $"Employee {action.EmployeeId} not found");
            }

            var allocation = company.Allocations.FirstOrDefault(a => a.Employee == employee && a.Project == project);

            if (allocation == null)
            {
                allocation = new Allocation
                {
                    Employee = employee,
                    Project = project
                };

                company.Allocations.Add(allocation);
            }

            allocation.Percent = action.Percentage;
            allocation.Role = action.Role;

            return Message(game, "Allocation adjusted", $"Allocation for {employee.Person.FirstName} {employee.Person.LastName} on project {project.ProjectDefinition.Name} adjusted to {action.Percentage}%");
        }
    }
}
