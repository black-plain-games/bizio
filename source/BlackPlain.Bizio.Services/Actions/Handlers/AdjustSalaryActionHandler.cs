﻿using Bizio.Core;
using Bizio.Core.Model.Companies;
using Bizio.Core.Model.Game;
using BlackPlain.Bizio.Services.Actions.Data;
using D = Bizio.Core.Data;

namespace BlackPlain.Bizio.Services.Actions.Handlers
{
    internal class AdjustSalaryActionHandler() : ActionHandlerBase(D.Actions.ActionType.AdjustSalary, "Human Resources")
    {
        public override CompanyMessage ProcessAction(IActionData data, Game game, Company company)
        {
            var action = Interpret<AdjustSalaryActionData>(data);

            var employee = company.Employees.Find(action.EmployeeId);

            if (employee == null)
            {
                return Message(game, "Could not adjust salary", $"Employee {action.EmployeeId} not found");
            }

            employee.Salary = action.Salary;

            return Message(game, "Salary adjusted", $"Salary for {employee.Person.FirstName} {employee.Person.LastName} adjusted to {action.Salary}");
        }
    }
}
