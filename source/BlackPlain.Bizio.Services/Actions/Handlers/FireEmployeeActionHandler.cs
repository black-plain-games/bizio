﻿using Bizio.Core.Model.Companies;
using Bizio.Core.Model.Game;
using BlackPlain.Bizio.Services.Actions.Data;
using D = Bizio.Core.Data;

namespace BlackPlain.Bizio.Services.Actions.Handlers
{
    internal class FireEmployeeActionHandler() : ActionHandlerBase(D.Actions.ActionType.FireEmployee, "Human Resources")
    {
        public override CompanyMessage ProcessAction(IActionData data, Game game, Company company)
        {
            var action = Interpret<FireEmployeeActionData>(data);

            var employee = company.Employees.FirstOrDefault(e => e.Id == action.EmployeeId);

            if (employee == null)
            {
                return Message(game, "Could not fire employee", $"Employee {action.EmployeeId} not found");
            }

            company.Employees.Remove(employee);

            var allocations = company.Allocations.Where(a => a.Employee == employee).ToList();

            foreach (var allocationToRemove in allocations)
            {
                company.Allocations.Remove(allocationToRemove);
            }

            game.People.Add(employee.Person);

            return Message(game, "Employee fired", $"Employee {employee.Person.FirstName} {employee.Person.LastName} fired");
        }
    }
}
