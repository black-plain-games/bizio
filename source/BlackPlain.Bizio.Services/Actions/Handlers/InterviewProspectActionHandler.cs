﻿using Bizio.Core;
using Bizio.Core.Model.Companies;
using Bizio.Core.Model.Game;
using Bizio.Core.Utilities;
using BlackPlain.Bizio.Services.Actions.Data;
using D = Bizio.Core.Data;

namespace BlackPlain.Bizio.Services.Actions.Handlers
{
    internal class InterviewProspectActionHandler(
        IConfigurationService configurationService,
        IPersonService personService
        ) : ActionHandlerBase(D.Actions.ActionType.InterviewProspect, "Human Resources")
    {
        public override CompanyMessage ProcessAction(IActionData data, Game game, Company company)
        {
            var action = Interpret<InterviewProspectActionData>(data);

            var person = game.People.Find(action.PersonId);

            if (person == null)
            {
                return Message(game, "Could not interview prospect", $"Person {action.PersonId} not found");
            }

            var prospect = company.Prospects.FirstOrDefault(p => p.Person == person);

            if (prospect == null)
            {
                var salary = personService.GetDesiredSalary(person);

                var accuracy = configurationService.Get<int>(D.Configuration.ConfigurationKey.DefaultProspectAccuracy);

                prospect = new Prospect
                {
                    Id = Guid.NewGuid(),
                    Accuracy = configurationService.Get<int>(D.Configuration.ConfigurationKey.DefaultProspectAccuracy),
                    Person = person,
                    Salary = salary.ToRange(accuracy),
                    Skills = person.Skills.Select(s => new ProspectSkill
                    {
                        SkillDefinition = s.SkillDefinition,
                        Value = s.Value.ToRange(accuracy)
                    }).Observe()
                };

                company.Prospects.Add(prospect);
            }
            else
            {
                prospect.Accuracy += (int)(.5 * (100 - prospect.Accuracy));

                foreach (var skill in person.Skills)
                {
                    var prospectSkill = prospect.Skills.FirstOrDefault(s => s.SkillDefinition == skill.SkillDefinition);

                    if (prospectSkill == null)
                    {
                        prospect.Skills.Add(new ProspectSkill
                        {
                            SkillDefinition = skill.SkillDefinition,
                            Value = skill.Value.ToRange(prospect.Accuracy)
                        });
                    }
                    else
                    {
                        prospectSkill.Value = skill.Value.ToRange(prospect.Accuracy);
                    }
                }
            }

            return Message(game, "Prospect interviewed", $"Prospect {prospect.Person.FirstName} {prospect.Person.LastName} interviewed");
        }
    }
}
