﻿using Bizio.Core;
using Bizio.Core.Model.Companies;
using Bizio.Core.Model.Game;
using Bizio.Core.Model.People;
using BlackPlain.Bizio.Services.Actions.Data;
using D = Bizio.Core.Data;

namespace BlackPlain.Bizio.Services.Actions.Handlers
{
    internal class MakeOfferActionHandler(IPersonService personService) : ActionHandlerBase(D.Actions.ActionType.MakeOffer, "Human Resources")
    {
        public override CompanyMessage ProcessAction(IActionData data, Game game, Company company)
        {
            var action = Interpret<MakeOfferActionData>(data);

            var person = game.People.Find(action.PersonId);

            if (person == null)
            {
                return Message(game, "Could not make offer", $"Person {action.PersonId} not found");
            }

            var desiredSalary = personService.GetDesiredSalary(person);

            if (desiredSalary > action.Salary)
            {
                return Message(game, "Offer rejected", $"Offered salary is too low for {person.FirstName} {person.LastName}");
            }

            var employee = new Employee
            {
                Id = Guid.NewGuid(),
                Person = person,
                Salary = action.Salary,
                Happiness = 100,
                IsFounder = false,
                HireDate = game.CurrentDate
            };

            person.WorkHistory.Add(new WorkHistory
            {
                Company = company,
                StartingSalary = action.Salary,
                StartDate = game.CurrentDate
            });

            var prospect = company.Prospects.FirstOrDefault(p => p.Person == person);

            if (prospect != null)
            {
                company.Prospects.Remove(prospect);
            }

            company.Employees.Add(employee);

            game.People.Remove(person);

            return Message(game, "Offer accepted", $"{person.FirstName} {person.LastName} has agreed to work with us for {action.Salary}");
        }
    }
}
