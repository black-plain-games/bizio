﻿using Bizio.Core;
using Bizio.Core.Model.Companies;
using Bizio.Core.Model.Game;
using BlackPlain.Bizio.Services.Actions.Data;
using D = Bizio.Core.Data;

namespace BlackPlain.Bizio.Services.Actions.Handlers
{
    internal class PurchasePerkActionHandler() : ActionHandlerBase(D.Actions.ActionType.PurchasePerk, "Human Resources")
    {
        public override CompanyMessage ProcessAction(IActionData data, Game game, Company company)
        {
            var action = Interpret<PurchasePerkActionData>(data);

            var perk = game.Industry.Perks.Find(action.PerkId);

            if (perk == null)
            {
                return Message(game, "Could not purchase perk", $"Perk {action.PerkId} not found");
            }

            var count = company.Perks.Count(p => p.Perk == perk);

            var companyPerk = company.Perks.FirstOrDefault(p => p.Perk == perk);

            if (perk.MaxCount < count + action.Quantity)
            {
                return Message(game, "Could not purchase perk", $"You can only purchase {perk.MaxCount} of the {perk.Name}. You currently have {count} and cannot purchase {action.Quantity}.");
            }

            if (perk.InitialCost.HasValue)
            {
                var cost = perk.InitialCost.Value * action.Quantity;

                if (company.Money < cost)
                {
                    return Message(game, "Could not purchase perk", $"Cannot afford to spend {cost} on {action.Quantity} {perk.Name}.");
                }

                company.Money -= cost;

                company.Transactions.Add(new Transaction
                {
                    Id = Guid.NewGuid(),
                    Amount = cost,
                    Date = game.CurrentDate,
                    Description = $"Purchased perk {perk.Name}",
                    Type = D.Companies.TransactionType.Perk,
                    EndingBalance = company.Money
                });
            }

            DateTime? nextPaymentDate = perk.RecurringCostInterval.HasValue ? game.CurrentDate.AddDays(perk.RecurringCostInterval.Value) : null;

            var purchasedCount = 0;

            while (purchasedCount < action.Quantity)
            {
                company.Perks.Add(new CompanyPerk
                {
                    Perk = perk,
                    NextPaymentDate = nextPaymentDate
                });

                purchasedCount++;
            }

            return Message(game, "Perk purchased", $"Purchased {action.Quantity} {perk.Name}");
        }
    }
}
