﻿using Bizio.Core;
using Bizio.Core.Model.Companies;
using Bizio.Core.Model.Game;
using Bizio.Core.Utilities;
using BlackPlain.Bizio.Services.Actions.Data;
using D = Bizio.Core.Data;

namespace BlackPlain.Bizio.Services.Actions.Handlers
{
    internal class RequestExtensionActionHandler() : ActionHandlerBase(D.Actions.ActionType.RequestExtension, "Project Management")
    {
        public override CompanyMessage ProcessAction(IActionData data, Game game, Company company)
        {
            var action = Interpret<RequestExtensionActionData>(data);

            var project = company.Projects.Find(action.ProjectId);

            if (project == null)
            {
                return Message(game, "Could not request extension", "Project not found.");
            }

            if (project.Status != D.Projects.ProjectStatus.InProgress)
            {
                return Message(game, "Could not request extension", "Project is not in progress.");
            }

            if (project.ExtensionDeadline.HasValue)
            {
                return Message(game, "Could not request extension", "Extension already requested.");
            }

            var isExtensionGranted = Utilities.GetRandomBool(1.0m / action.Turns);

            if (!isExtensionGranted)
            {
                return Message(game, "Extension denied", $"Extension denied for project {project.ProjectDefinition.Name}.");
            }

            project.ExtensionDeadline = project.Deadline.AddDays(action.Turns);

            return Message(game, "Extension requested", $"Extension requested for project {project.ProjectDefinition.Name}. New deadline is {project.ExtensionDeadline}.");
        }
    }
}
