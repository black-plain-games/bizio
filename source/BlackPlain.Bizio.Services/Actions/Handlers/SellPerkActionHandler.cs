﻿using Bizio.Core;
using Bizio.Core.Model.Companies;
using Bizio.Core.Model.Game;
using BlackPlain.Bizio.Services.Actions.Data;
using D = Bizio.Core.Data;

namespace BlackPlain.Bizio.Services.Actions.Handlers
{
    internal class SellPerkActionHandler() : ActionHandlerBase(D.Actions.ActionType.SellPerk, "Human Resources")
    {
        public override CompanyMessage ProcessAction(IActionData data, Game game, Company company)
        {
            var action = Interpret<SellPerkActionData>(data);

            var perk = game.Industry.Perks.Find(action.PerkId);

            var perks = company.Perks.Where(p => p.Perk == perk);

            if (!perks.Any())
            {
                return Message(game, "Could not sell perk", $"You do not own any {perk.Name}.");
            }

            if (perks.Count() < action.Quantity)
            {
                return Message(game, "Could not sell perk", $"You only have {perks.Count()} {perk.Name} and cannot sell {action.Quantity}.");
            }

            var soldCount = 0;
            var totalCost = 0;

            while (soldCount < action.Quantity)
            {
                var p = perks.First();
                company.Perks.Remove(p);

                var cost = perk.InitialCost ?? 0;

                totalCost += cost;
                company.Money += cost;

                soldCount++;
            }

            company.Transactions.Add(new Transaction
            {
                Id = Guid.NewGuid(),
                Amount = totalCost,
                Date = game.CurrentDate,
                Description = $"Sold {soldCount} {perk.Name}",
                Type = D.Companies.TransactionType.Perk,
                EndingBalance = company.Money
            });

            return Message(game, "Perk sold", $"Sold {action.Quantity} {perk.Name}");
        }
    }
}
