﻿using Bizio.Core;
using Bizio.Core.Model.Companies;
using Bizio.Core.Model.Game;
using BlackPlain.Bizio.Services.Actions.Data;
using D = Bizio.Core.Data;

namespace BlackPlain.Bizio.Services.Actions.Handlers
{
    internal class SubmitProjectActionHandler(IProjectService _projectService) : ActionHandlerBase(D.Actions.ActionType.SubmitProject, "Project Management")
    {
        public override CompanyMessage ProcessAction(IActionData data, Game game, Company company)
        {
            var action = Interpret<SubmitProjectActionData>(data);

            var project = company.Projects.Find(action.ProjectId);

            if (project == null)
            {
                return Message(game, "Could not submit project", "Project not found.");
            }

            if (project.Status != D.Projects.ProjectStatus.InProgress)
            {
                return Message(game, "Could not submit project", "Project is not in progress.");
            }

            _projectService.CompleteProject(game, company, project);

            if (project.Result == D.Projects.ProjectResult.Failure)
            {
                return Message(game, "Project failed", $"Project {project.ProjectDefinition.Name} was a failure. Our reputation has been impacted negatively.");
            }

            return Message(game, "Project completed successfully", $"Project {project.ProjectDefinition.Name} was a success. We earned {project.Value}.");
        }
    }
}
