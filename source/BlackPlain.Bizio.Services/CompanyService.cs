﻿using Bizio.Core.Model.Companies;
using Bizio.Core.Model.People;
using System.Collections.ObjectModel;
using D = Bizio.Core.Data;

namespace BlackPlain.Bizio.Services
{
    public interface ICompanyService
    {
        Company CreateCompany(Guid? userId, DateTime currentDate, Person founderPerson, string name, long initialFunds);
    }

    public class CompanyService(IStaticDataService _staticDataService) : ICompanyService
    {
        public Company CreateCompany(Guid? userId, DateTime currentDate, Person founderPerson, string name, long initialFunds)
        {
            var founder = new Employee
            {
                Id = Guid.NewGuid(),
                Person = founderPerson,
                IsFounder = true,
                Salary = 0,
                Happiness = 100,
                HireDate = currentDate
            };

            var actions = new ObservableCollection<CompanyAction>();

            foreach (var action in _staticDataService.StaticData.Actions)
            {
                var accumulations = new ObservableCollection<CompanyActionAccumulation>();

                foreach (var requirement in action.Requirements)
                {
                    accumulations.Add(new CompanyActionAccumulation
                    {
                        SkillDefinition = requirement.SkillDefinition,
                        Value = 0
                    });
                }

                actions.Add(new CompanyAction
                {
                    Action = action,
                    Count = action.DefaultCount,
                    IsActive = true,
                    Accumulations = accumulations
                });
            }

            return new Company
            {
                Id = Guid.NewGuid(),
                UserId = userId,
                Name = name,
                Money = initialFunds,
                Status = D.Companies.CompanyStatus.InBusiness,
                Reputation = new(),
                InitialAccuracy = 100,
                Actions = actions,
                Allocations = [],
                Employees = [founder],
                Messages = [],
                Perks = [],
                Projects = [],
                Prospects = [],
                Transactions = [],
                TurnActions = []
            };
        }
    }
}
