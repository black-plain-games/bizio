﻿using Bizio.Core.Data.Configuration;
using Bizio.Core.Model.Game;
using Bizio.Core.Repositories;
using Bizio.Core.Utilities;

namespace BlackPlain.Bizio.Services
{
    public interface IConfigurationService
    {
        Task LoadAsync(CancellationToken cancellationToken);
        T Get<T>(ConfigurationKey key);
        T Get<T>(ConfigurationKey key, T fallbackValue);
        T Get<T>(Game game, ConfigurationKey key);
        T Get<T>(Game game, ConfigurationKey key, T fallbackValue);
    }

    internal class ConfigurationService(
        IConfigurationRepository _configurationRepository
        ) : IConfigurationService
    {
        private IDictionary<ConfigurationKey, string>? _configuration;

        public async Task LoadAsync(CancellationToken cancellationToken)
        {
            _configuration = await _configurationRepository.LoadAsync(cancellationToken);
        }

        public T Get<T>(ConfigurationKey key) => Get<T>(_configuration, key);

        public T Get<T>(ConfigurationKey key, T fallbackValue) => Get(_configuration, key, fallbackValue);

        public T Get<T>(Game game, ConfigurationKey key) => Get<T>(game.Configuration, key);

        public T Get<T>(Game game, ConfigurationKey key, T fallbackValue) => Get(game.Configuration, key, fallbackValue);

        private T Get<T>(IDictionary<ConfigurationKey, string>? configuration, ConfigurationKey key)
        {
            if (configuration != null)
            {
                if (configuration.TryGetValue(key, out var result1))
                {
                    return result1.ChangeType<T>();
                }
            }

            if (_configuration == null)
            {
                throw new Exception("Global configuration not loaded");
            }

            if (_configuration.TryGetValue(key, out var result2))
            {
                return result2.ChangeType<T>();
            }

            throw new Exception("Key not found in global configuration");
        }

        private T Get<T>(IDictionary<ConfigurationKey, string>? configuration, ConfigurationKey key, T fallbackValue)
        {
            if (configuration != null)
            {
                if (configuration.TryGetValue(key, out var result1))
                {
                    return result1.ChangeType<T>();
                }

                return fallbackValue;
            }

            if (_configuration == null)
            {
                throw new Exception("Global configuration not loaded");
            }

            if (_configuration.TryGetValue(key, out var result2))
            {
                return result2.ChangeType<T>();
            }

            return fallbackValue;
        }
    }
}
