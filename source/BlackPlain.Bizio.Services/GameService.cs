﻿using Bizio.Core;
using Bizio.Core.Model.Companies;
using Bizio.Core.Model.Game;
using Bizio.Core.Model.People;
using Bizio.Core.Model.Perks;
using Bizio.Core.Model.Projects;
using Bizio.Core.Repositories;
using Bizio.Core.Services;
using Bizio.Core.Utilities;
using Newtonsoft.Json;
using System.Collections.ObjectModel;
using CK = Bizio.Core.Data.Configuration.ConfigurationKey;
using D = Bizio.Core.Data;

namespace BlackPlain.Bizio.Services
{
    public interface IGameService
    {
        Task<Guid> CreateAsync(CreateGameParameters newGameData, CancellationToken cancellationToken);
        Task SaveAsync(Game game, CancellationToken cancellationToken);
        Task<IEnumerable<GameMetadata>> LoadAsync(CancellationToken cancellationToken);
        Task<Game> LoadAsync(Guid id, CancellationToken cancellationToken);
        Task DeleteAsync(Guid id, CancellationToken cancellationToken);
    }

    internal class GameService(
        IStaticDataService _staticDataService,
        IConfigurationService _configurationService,
        IGameRepository _gameRepository,
        IPersonService _personService,
        ICompanyService _companyService,
        IProjectService _projectService
        ) : IGameService
    {
        public async Task<Guid> CreateAsync(CreateGameParameters parameters, CancellationToken cancellationToken)
        {
            var industry = _staticDataService.StaticData.Industries.FirstOrDefault(i => i.Id == parameters.IndustryId) ?? throw new Exception("Industry not found");

            var game = new Game
            {
                Id = Guid.NewGuid(),
                CreatedDate = DateTimeOffset.Now,
                UserId = parameters.UserId,
                Status = D.Game.GameStatus.Playing,
                StartDate = parameters.StartDate,
                Industry = industry,
                CurrentTurn = 0,
                Configuration = parameters.Configuration,
                UserCompanyName = parameters.CompanyName,
                DaysPerTurn = parameters.DaysPerTurn,
                FounderFirstName = parameters.Founder.FirstName,
                FounderLastName = parameters.Founder.LastName
            };

            // Create People
            var initialPersonPoolSize = _configurationService.Get(game, CK.InitialPersonPoolSize, 10);

            game.People = _personService
                .CreatePeople(initialPersonPoolSize, industry, parameters.StartDate)
                .Observe();

            // Create Founder
            var userFounderPerson = _personService.CreatePerson(parameters.Founder, industry);

            // Create User Company
            var userCompany = _companyService.CreateCompany(
                parameters.UserId,
                parameters.StartDate,
                userFounderPerson,
                parameters.CompanyName,
                parameters.InitialFunds
            );

            // Create Companies
            game.Companies = CreateCompanies(
                userCompany,
                parameters.StartDate,
                game.People,
                industry,
                parameters.NumberOfCompanies,
                parameters.InitialFunds
            );

            foreach (var company in game.Companies)
            {
                game.People.Remove(company.Employees.First(e => e.IsFounder).Person);
            }

            // Create Projects
            var projectGenerationBatchSize = _configurationService.Get(game, CK.ProjectGenerationBatchSize, 10);

            game.Projects = _projectService
                .CreateProjects(projectGenerationBatchSize, industry, parameters.StartDate)
                .Observe();

            await SaveAsync(game, cancellationToken);

            return game.Id;
        }

        public async Task SaveAsync(Game game, CancellationToken cancellationToken) => await _gameRepository.SaveAsync(Convert(game), cancellationToken);

        public async Task<IEnumerable<GameMetadata>> LoadAsync(CancellationToken cancellationToken)
        {
            var games = await _gameRepository.LoadAsync(cancellationToken);

            return games.Select(Convert).ToList();
        }

        public async Task<Game> LoadAsync(Guid id, CancellationToken cancellationToken)
        {
            var game = await _gameRepository.LoadAsync(id, cancellationToken);

            return Convert(game);
        }

        public async Task DeleteAsync(Guid id, CancellationToken cancellationToken) => await _gameRepository.DeleteAsync(id, cancellationToken);

        #region Model to Data Conversions

        private D.Projects.ProjectRequirement Convert(ProjectRequirement requirement)
        {
            return new D.Projects.ProjectRequirement
            {
                CurrentValue = requirement.CurrentValue,
                SkillDefinitionId = requirement.SkillDefinition.Id,
                TargetValue = requirement.TargetValue
            };
        }

        private D.Projects.Project Convert(Project project)
        {
            return new D.Projects.Project
            {
                Id = project.Id,
                Deadline = project.Deadline,
                ExtensionDeadline = project.ExtensionDeadline,
                ProjectDefinitionId = project.ProjectDefinition.Id,
                ReputationRequired = project.ReputationRequired,
                Result = project.Result,
                Status = project.Status,
                Value = project.Value,
                Requirements = project.Requirements.Select(Convert).ToList()
            };
        }

        private D.People.Skill Convert(Skill skill)
        {
            return new D.People.Skill
            {
                ForgetRate = skill.ForgetRate,
                LearnRate = skill.LearnRate,
                SkillDefinitionId = skill.SkillDefinition.Id,
                Value = skill.Value
            };
        }

        private D.People.WorkHistory Convert(WorkHistory workHistory)
        {
            return new D.People.WorkHistory
            {
                CompanyId = workHistory.Company.Id,
                EndDate = workHistory.EndDate,
                EndingSalary = workHistory.EndingSalary,
                StartDate = workHistory.StartDate,
                StartingSalary = workHistory.StartingSalary
            };
        }

        private D.People.Person Convert(Person person)
        {
            return new D.People.Person
            {
                Id = person.Id,
                Birthday = person.Birthday,
                FirstName = person.FirstName,
                LastName = person.LastName,
                Gender = person.Gender,
                PersonalityId = person.Personality.Id,
                ProfessionId = person.Profession?.Id,
                RetirementDate = person.RetirementDate,
                Skills = person.Skills.Select(Convert).ToList(),
                WorkHistory = person.WorkHistory.Select(Convert).ToList()
            };
        }

        private D.Companies.CompanyActionAccumulation Convert(CompanyActionAccumulation accumulation)
        {
            return new D.Companies.CompanyActionAccumulation
            {
                SkillDefinitionId = accumulation.SkillDefinition.Id,
                Value = accumulation.Value
            };
        }

        private D.Companies.CompanyAction Convert(CompanyAction action)
        {
            return new D.Companies.CompanyAction
            {
                ActionId = action.Action.Id,
                Count = action.Count,
                IsActive = action.IsActive,
                Accumulations = action.Accumulations.Select(Convert).ToList()
            };
        }

        private D.Companies.Allocation Convert(Allocation allocation)
        {
            return new D.Companies.Allocation
            {
                EmployeeId = allocation.Employee.Id,
                Percent = allocation.Percent,
                ProjectId = allocation.Project.Id,
                Role = allocation.Role
            };
        }

        private D.Companies.Employee Convert(Employee employee)
        {
            return new D.Companies.Employee
            {
                Happiness = employee.Happiness,
                Id = employee.Id,
                IsFounder = employee.IsFounder,
                Person = Convert(employee.Person),
                Salary = employee.Salary,
                HireDate = employee.HireDate
            };
        }

        private D.Companies.CompanyMessage Convert(CompanyMessage message)
        {
            return new D.Companies.CompanyMessage
            {
                DateCreated = message.DateCreated,
                Id = message.Id,
                Message = message.Message,
                Source = message.Source,
                Status = message.Status,
                Subject = message.Subject
            };
        }

        private D.Companies.CompanyPerk Convert(CompanyPerk perk)
        {
            return new D.Companies.CompanyPerk
            {
                NextPaymentDate = perk.NextPaymentDate,
                PerkId = perk.Perk.Id
            };
        }

        private D.Companies.ProspectSkill Convert(ProspectSkill skill)
        {
            return new D.Companies.ProspectSkill
            {
                SkillDefinitionId = skill.SkillDefinition.Id,
                Value = skill.Value
            };
        }

        private D.Companies.Prospect Convert(Prospect prospect)
        {
            return new D.Companies.Prospect
            {
                Accuracy = prospect.Accuracy,
                Id = prospect.Id,
                PersonId = prospect.Person.Id,
                Salary = prospect.Salary,
                Skills = prospect.Skills.Select(Convert).ToList()
            };
        }

        private D.Companies.Transaction Convert(Transaction transaction)
        {
            return new D.Companies.Transaction
            {
                Id = transaction.Id,
                Amount = transaction.Amount,
                Date = transaction.Date,
                Description = transaction.Description,
                EndingBalance = transaction.EndingBalance,
                Type = transaction.Type
            };
        }

        private D.Companies.ActionData Convert(IActionData actionData)
        {
            return new D.Companies.ActionData
            {
                ActionType = actionData.ActionType,
                DataType = actionData.GetType().FullName,
                Data = actionData.Serialize()
            };
        }

        private D.Companies.Company Convert(Company company)
        {
            return new D.Companies.Company
            {
                Id = company.Id,
                InitialAccuracy = company.InitialAccuracy,
                Money = company.Money,
                Name = company.Name,
                Status = company.Status,
                UserId = company.UserId,
                Reputation = new D.Companies.Reputation
                {
                    EarnedStars = company.Reputation.EarnedStars,
                    PossibleStars = company.Reputation.PossibleStars,
                },
                Actions = company.Actions.Select(Convert).ToList(),
                Allocations = company.Allocations.Select(Convert).ToList(),
                Employees = company.Employees.Select(Convert).ToList(),
                Messages = company.Messages.Select(Convert).ToList(),
                Perks = company.Perks.Select(Convert).ToList(),
                Projects = company.Projects.Select(Convert).ToList(),
                Prospects = company.Prospects.Select(Convert).ToList(),
                Transactions = company.Transactions.Select(Convert).ToList(),
                TurnActions = company.TurnActions.Select(Convert).ToList()
            };
        }

        private D.Game.Game Convert(Game game)
        {
            return new D.Game.Game
            {
                Id = game.Id,
                CreatedDate = game.CreatedDate,
                CurrentTurn = game.CurrentTurn,
                UserId = game.UserId,
                DaysPerTurn = game.DaysPerTurn,
                FounderFirstName = game.FounderFirstName,
                FounderLastName = game.FounderLastName,
                StartDate = game.StartDate,
                Status = game.Status,
                IndustryId = game.Industry.Id,
                UserCompanyName = game.UserCompanyName,
                Configuration = new Dictionary<CK, string>(game.Configuration),
                Companies = game.Companies.Select(Convert).ToList(),
                People = game.People.Select(Convert).ToList(),
                Projects = game.Projects.Select(Convert).ToList(),
            };
        }

        #endregion

        #region Data to Model Conversions

        private GameMetadata Convert(D.Game.GameMetadata game)
        {
            return new GameMetadata
            {
                Id = game.Id,
                CreatedDate = game.CreatedDate,
                CurrentTurn = game.CurrentTurn,
                Industry = _staticDataService.StaticData.Industries.Find(game.IndustryId),
                DaysPerTurn = game.DaysPerTurn,
                FounderFirstName = game.FounderFirstName,
                FounderLastName = game.FounderLastName,
                StartDate = game.StartDate,
                Status = game.Status,
                UserId = game.UserId,
                UserCompanyName = game.UserCompanyName
            };
        }

        private CompanyActionAccumulation Convert(D.Companies.CompanyActionAccumulation accumulation)
        {
            return new CompanyActionAccumulation
            {
                SkillDefinition = _staticDataService.StaticData.SkillDefinitions.Find(accumulation.SkillDefinitionId),
                Value = accumulation.Value
            };
        }

        private CompanyAction Convert(D.Companies.CompanyAction companyAction)
        {
            return new CompanyAction
            {
                IsActive = companyAction.IsActive,
                Count = companyAction.Count,
                Action = _staticDataService.StaticData.Actions.Find(companyAction.ActionId),
                Accumulations = companyAction.Accumulations.Select(Convert).Observe()
            };
        }

        private static Transaction Convert(D.Companies.Transaction transactions)
        {
            return new Transaction
            {
                Id = transactions.Id,
                Type = transactions.Type,
                Amount = transactions.Amount,
                Date = transactions.Date,
                Description = transactions.Description,
                EndingBalance = transactions.EndingBalance
            };
        }

        private static CompanyMessage Convert(D.Companies.CompanyMessage message)
        {
            return new CompanyMessage
            {
                Id = message.Id,
                DateCreated = message.DateCreated,
                Message = message.Message,
                Source = message.Source,
                Status = message.Status,
                Subject = message.Subject
            };
        }

        private static CompanyPerk Convert(D.Companies.CompanyPerk perk, IEnumerable<Perk> perks)
        {
            return new CompanyPerk
            {
                NextPaymentDate = perk.NextPaymentDate,
                Perk = perks.Find(perk.PerkId)
            };
        }

        private ProjectRequirement Convert(D.Projects.ProjectRequirement requirement)
        {
            return new ProjectRequirement
            {
                CurrentValue = requirement.CurrentValue,
                SkillDefinition = _staticDataService.StaticData.SkillDefinitions.Find(requirement.SkillDefinitionId),
                TargetValue = requirement.TargetValue
            };
        }

        private Project Convert(D.Projects.Project project, Industry industry)
        {
            return new Project
            {
                Id = project.Id,
                Deadline = project.Deadline,
                ExtensionDeadline = project.ExtensionDeadline,
                ProjectDefinition = industry.ProjectDefinitions.Find(project.ProjectDefinitionId),
                ReputationRequired = project.ReputationRequired,
                Result = project.Result,
                Status = project.Status,
                Value = project.Value,
                Requirements = project.Requirements.Select(Convert).Observe(),
            };
        }

        private ProspectSkill Convert(D.Companies.ProspectSkill skill)
        {
            return new ProspectSkill
            {
                Value = skill.Value,
                SkillDefinition = _staticDataService.StaticData.SkillDefinitions.Find(skill.SkillDefinitionId)
            };
        }

        private Prospect Convert(D.Companies.Prospect prospect, IEnumerable<Person> people)
        {
            return new Prospect
            {
                Id = prospect.Id,
                Person = people.Find(prospect.PersonId),
                Accuracy = prospect.Accuracy,
                Salary = prospect.Salary,
                Skills = prospect.Skills.Select(Convert).Observe()
            };
        }

        private Employee Convert(D.Companies.Employee employee, Industry industry)
        {
            return new Employee
            {
                Id = employee.Id,
                Happiness = employee.Happiness,
                IsFounder = employee.IsFounder,
                Salary = employee.Salary,
                Person = Convert(employee.Person, industry),
                HireDate = employee.HireDate
            };
        }

        private static Allocation Convert(D.Companies.Allocation allocation, IEnumerable<Employee> employees, IEnumerable<Project> projects)
        {
            return new Allocation
            {
                Role = allocation.Role,
                Percent = allocation.Percent,
                Employee = employees.Find(allocation.EmployeeId),
                Project = projects.Find(allocation.ProjectId)
            };
        }

        private static IActionData Convert(D.Companies.ActionData actionData)
        {
            return JsonConvert.DeserializeObject(actionData.Data, Type.GetType(actionData.DataType)) as IActionData;
        }

        private Company Convert(D.Companies.Company company, IEnumerable<Person> people, Industry industry)
        {
            var employees = company.Employees.Select(e => Convert(e, industry)).Observe();
            var projects = company.Projects.Select(p => Convert(p, industry)).Observe();

            return new Company
            {
                Id = company.Id,
                InitialAccuracy = company.InitialAccuracy,
                Money = company.Money,
                Name = company.Name,
                Status = company.Status,
                UserId = company.UserId,
                Reputation = new Reputation
                {
                    EarnedStars = company.Reputation.EarnedStars,
                    PossibleStars = company.Reputation.PossibleStars,
                },
                Actions = company.Actions.Select(Convert).Observe(),
                Transactions = company.Transactions.Select(Convert).Observe(),
                Messages = company.Messages.Select(Convert).Observe(),
                Perks = company.Perks.Select(p => Convert(p, industry.Perks)).Observe(),
                Projects = projects,
                Prospects = company.Prospects.Select(p => Convert(p, people)).Observe(),
                Employees = employees,
                Allocations = company.Allocations.Select(a => Convert(a, employees, projects)).Observe(),
                TurnActions = company.TurnActions.Select(Convert).Observe()
            };
        }

        private static WorkHistory Convert(D.People.WorkHistory workHistory, IEnumerable<Company> companies)
        {
            return new WorkHistory
            {
                Company = companies.Find(workHistory.CompanyId),
                EndDate = workHistory.EndDate,
                EndingSalary = workHistory.EndingSalary,
                StartDate = workHistory.StartDate,
                StartingSalary = workHistory.StartingSalary
            };
        }

        private Skill Convert(D.People.Skill skill)
        {
            return new Skill
            {
                ForgetRate = skill.ForgetRate,
                LearnRate = skill.LearnRate,
                Value = skill.Value,
                SkillDefinition = _staticDataService.StaticData.SkillDefinitions.Find(skill.SkillDefinitionId)
            };
        }

        private Person Convert(D.People.Person person, Industry industry)
        {
            var personalities = _staticDataService.StaticData.Personalities;

            return new Person
            {
                Id = person.Id,
                Birthday = person.Birthday,
                FirstName = person.FirstName,
                Gender = person.Gender,
                LastName = person.LastName,
                RetirementDate = person.RetirementDate,
                Profession = industry.Professions.Find(person.ProfessionId ?? Guid.Empty),
                Personality = personalities.Find(person.PersonalityId),
                Skills = person.Skills.Select(Convert).Observe()
            };
        }

        private Game Convert(D.Game.Game game)
        {
            var industries = _staticDataService.StaticData.Industries;
            var industry = industries.FirstOrDefault(i => i.Id == game.IndustryId) ?? throw new Exception("Industry not found");
            var personalities = _staticDataService.StaticData.Personalities;

            var projects = game.Projects.Select(p => Convert(p, industry)).Observe();
            var people = game.People.Select(p => Convert(p, industry)).Observe();
            var companies = game.Companies.Select(c => Convert(c, people, industry)).Observe();

            foreach (var person in people)
            {
                var dPerson = game.People.Find(person.Id);
                person.WorkHistory = dPerson.WorkHistory.Select(wh => Convert(wh, companies)).Observe();
            }

            foreach (var company in companies)
            {
                var dCompany = game.Companies.Find(company.Id);

                foreach (var employee in company.Employees)
                {
                    var dPerson = dCompany.Employees.Find(employee.Id).Person;

                    employee.Person.WorkHistory = dPerson.WorkHistory.Select(wh => Convert(wh, companies)).Observe();
                }
            }

            var output = new Game
            {
                Id = game.Id,
                CreatedDate = game.CreatedDate,
                CurrentTurn = game.CurrentTurn,
                UserId = game.UserId,
                StartDate = game.StartDate,
                Status = game.Status,
                UserCompanyName = game.UserCompanyName,
                Industry = industry,
                Configuration = new Dictionary<CK, string>(game.Configuration),
                People = people,
                Companies = companies,
                Projects = projects
            };

            foreach (var dCompany in game.Companies)
            {
                var company = output.Companies.Find(dCompany.Id);
            }

            return output;
        }

        #endregion

        private ObservableCollection<Company> CreateCompanies(Company userCompany, DateTime currentDate, IEnumerable<Person> people, Industry industry, int numberOfCompanies, long initialFunds)
        {
            var companies = new ObservableCollection<Company> { userCompany };

            for (var index = 0; index < numberOfCompanies; index++)
            {
                var founderPerson = people.Random();
                var companyName = industry.CompanyNames.Random(n => !companies.Any(c => c.Name == n));

                var npcCompany = _companyService.CreateCompany(null, currentDate, founderPerson, companyName, initialFunds);

                companies.Add(npcCompany);
            }

            return companies;
        }
    }
}
