﻿using Bizio.Core.Services;
using BlackPlain.Bizio.Services.Actions.Handlers;
using BlackPlain.Bizio.Services.Processors;
using Microsoft.Extensions.DependencyInjection;

namespace BlackPlain.Bizio.Services
{
    public static class Package
    {
        public static IServiceCollection AddBizioServices(this IServiceCollection services)
        {
            return services
                .AddSingleton<IConfigurationService, ConfigurationService>()
                .AddTransient<ICompanyService, CompanyService>()
                .AddTransient<IPersonService, PersonService>()
                .AddTransient<IProjectService, ProjectService>()
                .AddTransient<IGameService, GameService>()
                .AddTransient<ITurnService, TurnService>()
                .AddSingleton<IStaticDataService, StaticDataService>()

                .AddActionHandler<AcceptProjectActionHandler>()
                .AddActionHandler<AdjustAllocationActionHandler>()
                .AddActionHandler<MakeOfferActionHandler>()
                .AddActionHandler<AdjustSalaryActionHandler>()
                .AddActionHandler<FireEmployeeActionHandler>()
                .AddActionHandler<InterviewProspectActionHandler>()
                .AddActionHandler<PurchasePerkActionHandler>()
                .AddActionHandler<SellPerkActionHandler>()
                .AddActionHandler<SubmitProjectActionHandler>()
                .AddActionHandler<RequestExtensionActionHandler>()

                .AddTurnProcessor<ProjectProgressProcessor>()
                .AddTurnProcessor<PayrollProcesor>()
                .AddTurnProcessor<PerkCostProcessor>()
                .AddTurnProcessor<CompanyTaxesProcessor>()
                .AddTurnProcessor<GeneratePeopleProcessor>()
                .AddTurnProcessor<EmployeeSkillChangeProcessor>()
                .AddTurnProcessor<GenerateProjectsProcess>()
                .AddTurnProcessor<PersonRetirementProcessor>()
                .AddTurnProcessor<CompanyBankruptProcessor>()
                .AddTurnProcessor<CompanyActionProcessor>();
        }

        private static IServiceCollection AddActionHandler<T>(this IServiceCollection services)
            where T : class, IActionHandler
        {
            return services.AddTransient<IActionHandler, T>();
        }

        private static IServiceCollection AddTurnProcessor<T>(this IServiceCollection services)
            where T : class, ITurnProcessor
        {
            return services.AddTransient<ITurnProcessor, T>();
        }
    }
}
