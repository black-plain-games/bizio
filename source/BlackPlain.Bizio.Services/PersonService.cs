﻿using Bizio.Core;
using Bizio.Core.Model.Companies;
using Bizio.Core.Model.People;
using Bizio.Core.Services;
using Bizio.Core.Utilities;
using System.Collections.ObjectModel;
using CK = Bizio.Core.Data.Configuration.ConfigurationKey;
using D = Bizio.Core.Data;

namespace BlackPlain.Bizio.Services
{
    public interface IPersonService
    {
        Person CreatePerson(CreatePersonParameters parameters, Industry industry);
        Person CreatePerson(Industry industry, DateTime currentDate);
        IEnumerable<Person> CreatePeople(int count, Industry industry, DateTime currentDate);
        int GetDesiredSalary(Person person);
    }

    internal class PersonService(
        IStaticDataService _staticDataService,
        IConfigurationService _configurationService
        ) : IPersonService
    {
        public Person CreatePerson(CreatePersonParameters parameters, Industry industry)
        {
            var sd = _staticDataService.StaticData;

            var person = new Person
            {
                Id = Guid.NewGuid(),
                FirstName = parameters.FirstName,
                LastName = parameters.LastName,
                Birthday = parameters.Birthday,
                Gender = parameters.Gender,
                Personality = sd.Personalities.Find(parameters.PersonalityId),
                Profession = null,
                RetirementDate = parameters.Birthday.AddYears(100),
                WorkHistory = [],
                Skills = []
            };

            foreach ((var skillDefinitionId, var value) in parameters.Skills)
            {
                var learnRate = Utilities.GetRandomInt(1, 3);
                var forgetRate = Utilities.GetRandomInt(1, 3);

                if (industry.MandatorySkills.Any(sd => sd.Id == skillDefinitionId))
                {
                    learnRate = forgetRate = 0;
                }

                person.Skills.Add(new()
                {
                    SkillDefinition = sd.SkillDefinitions.Find(skillDefinitionId),
                    Value = value,
                    LearnRate = learnRate,
                    ForgetRate = forgetRate
                });
            }

            return person;
        }

        private List<SkillDefinition> GetOptionalSkillDefinitions(IEnumerable<SkillDefinition> optionalSkillDefinitions)
        {
            var minimumOptionalSkillCount = _configurationService.Get<int>(CK.MinimumOptionalSkillCount);
            var maximumOptionalSkillCount = _configurationService.Get<int>(CK.MaximumOptionalSkillCount);
            var optionalSkillCount = Utilities.GetRandomInt(minimumOptionalSkillCount, maximumOptionalSkillCount);

            var output = new List<SkillDefinition>();

            var remainingOptionalSkillDefinitionIds = optionalSkillDefinitions.Except(output);

            while (output.Count < optionalSkillCount && remainingOptionalSkillDefinitionIds.Any())
            {
                // This used to be weighted
                output.Add(remainingOptionalSkillDefinitionIds.Random());
            }

            return output;
        }

        public Person CreatePerson(Industry industry, DateTime currentDate)
        {
            var sd = _staticDataService.StaticData;

            var gender = Utilities.GetRandomValue(D.People.Gender.None);
            var birthday = Utilities.GetRandomDate(currentDate.AddYears(-60), currentDate.AddYears(-20));
            var personality = sd.Personalities.Random();
            var minimumRetirementAge = personality.GetValue(D.People.PersonalityAttributeId.MinimumRetirementAge);

            var skills = new ObservableCollection<Skill>();

            foreach (var skillDefinition in industry.MandatorySkills)
            {
                skills.Add(new Skill
                {
                    SkillDefinition = skillDefinition,
                    Value = Utilities.GetRandomInt(skillDefinition.Value),
                    LearnRate = Utilities.GetRandomInt(1, 3),
                    ForgetRate = Utilities.GetRandomInt(1, 3)
                });
            }

            var optionalSkillDefinitions = GetOptionalSkillDefinitions(industry.OptionalSkills);

            foreach (var skillDefinition in optionalSkillDefinitions)
            {
                skills.Add(new Skill
                {
                    SkillDefinition = skillDefinition,
                    Value = Utilities.GetRandomInt(skillDefinition.Value),
                    LearnRate = Utilities.GetRandomInt(1, 3),
                    ForgetRate = Utilities.GetRandomInt(1, 3)
                });
            }

            var person = new Person
            {
                Id = Guid.NewGuid(),
                Gender = gender,
                FirstName = sd.FirstNames[gender].Random(),
                LastName = sd.LastNames.Random(),
                Birthday = birthday,
                Personality = personality,
                RetirementDate = Utilities.GetRandomDate(birthday.AddYears(minimumRetirementAge), birthday.AddYears(85)),
                Profession = industry.Professions.Random(),
                Skills = skills,
                WorkHistory = []
            };

            return person;
        }

        public IEnumerable<Person> CreatePeople(int count, Industry industry, DateTime currentDate)
        {
            var output = new List<Person>();

            for (var index = 0; index < count; index++)
            {
                output.Add(CreatePerson(industry, currentDate));
            }

            return output;
        }

        public int GetDesiredSalary(Person person)
        {
            return person.Skills.Sum(s => s.Value) / 3;
        }
    }
}
