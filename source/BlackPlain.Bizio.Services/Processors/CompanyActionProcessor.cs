﻿using Bizio.Core.Model.Companies;
using Bizio.Core.Model.Game;
using Bizio.Core.Model.People;
using Bizio.Core.Services;
using D = Bizio.Core.Data;

namespace BlackPlain.Bizio.Services.Processors
{
    internal class CompanyActionProcessor : ITurnProcessor
    {
        public void ProcessTurn(Game game)
        {
            foreach (var company in game.Companies)
            {
                if (company.Status == D.Companies.CompanyStatus.Bankrupt)
                {
                    continue;
                }

                var accumulationGroups = new Dictionary<SkillDefinition, ICollection<CompanyActionAccumulation>>();

                foreach (var action in company.Actions)
                {
                    if (!action.IsActive)
                    {
                        continue;
                    }

                    foreach (var accumulation in action.Accumulations)
                    {
                        if (!accumulationGroups.ContainsKey(accumulation.SkillDefinition))
                        {
                            accumulationGroups[accumulation.SkillDefinition] = [];
                        }

                        accumulationGroups[accumulation.SkillDefinition].Add(accumulation);
                    }
                }

                foreach ((var skillDefinition, var accumulations) in accumulationGroups)
                {
                    var total = company.Employees.Sum(e => e.Person.Skills.FirstOrDefault(s => s.SkillDefinition == skillDefinition)?.Value ?? 0);

                    var average = total / accumulations.Count;

                    foreach (var accumulation in accumulations)
                    {
                        accumulation.Value += average;
                    }
                }

                foreach (var action in company.Actions)
                {
                    if (!action.IsActive)
                    {
                        continue;
                    }

                    var requirementsMet = true;

                    while (requirementsMet)
                    {
                        if (action.Action.Requirements.Count == 0)
                        {
                            action.Count = action.Action.MaxCount;
                            break;
                        }

                        foreach (var requirement in action.Action.Requirements)
                        {
                            var accumulation = action.Accumulations.FirstOrDefault(a => a.SkillDefinition == requirement.SkillDefinition);

                            if (requirement.Value > accumulation.Value)
                            {
                                requirementsMet = false;
                                break;
                            }
                        }

                        if (requirementsMet)
                        {
                            foreach (var accumulation in action.Accumulations)
                            {
                                var requirement = action.Action.Requirements.FirstOrDefault(r => r.SkillDefinition == accumulation.SkillDefinition);

                                accumulation.Value -= requirement.Value;
                            }

                            action.Count++;
                        }
                    }
                }
            }
        }
    }
}
