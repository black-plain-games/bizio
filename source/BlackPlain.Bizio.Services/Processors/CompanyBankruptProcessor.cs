﻿using Bizio.Core.Model.Companies;
using Bizio.Core.Model.Game;
using Bizio.Core.Services;
using D = Bizio.Core.Data;

namespace BlackPlain.Bizio.Services.Processors
{
    internal class CompanyBankruptProcessor : ITurnProcessor
    {
        public void ProcessTurn(Game game)
        {
            foreach (var company in game.Companies)
            {
                if (company.Status == D.Companies.CompanyStatus.Bankrupt)
                {
                    continue;
                }

                if (company.Money > 0)
                {
                    continue;
                }

                company.Status = D.Companies.CompanyStatus.Bankrupt;

                company.Allocations.Clear();

                var projects = company.Projects.ToList();

                foreach (var project in projects)
                {
                    game.Projects.Add(project);
                    company.Projects.Remove(project);
                }

                var employees = company.Employees.ToList();

                foreach (var employee in employees)
                {
                    game.People.Add(employee.Person);
                    company.Employees.Remove(employee);
                }

                foreach (var otherCompany in game.Companies)
                {
                    if (otherCompany.Status == D.Companies.CompanyStatus.Bankrupt)
                    {
                        continue;
                    }

                    if (company.Id == otherCompany.Id)
                    {
                        otherCompany.Messages.Add(new CompanyMessage
                        {
                            Id = Guid.NewGuid(),
                            DateCreated = game.CurrentDate,
                            Source = "News",
                            Subject = "We're bankrupt",
                            Message = $"We've gone bankrupt, failing {projects.Count} projects and leaving {employees.Count} people unemployed."
                        });

                        continue;
                    }

                    otherCompany.Messages.Add(new CompanyMessage
                    {
                        Id = Guid.NewGuid(),
                        DateCreated = game.CurrentDate,
                        Source = "News",
                        Subject = "Company bankruptcy",
                        Message = $"{company.Name} has gone bankrupt, freeing up {projects.Count} projects and leaving {employees.Count} people unemployed."
                    });
                }
            }
        }
    }
}
