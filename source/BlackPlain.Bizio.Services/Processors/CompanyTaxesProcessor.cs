﻿using Bizio.Core.Model.Companies;
using Bizio.Core.Model.Game;
using Bizio.Core.Services;
using D = Bizio.Core.Data;

namespace BlackPlain.Bizio.Services.Processors
{
    internal class CompanyTaxesProcessor : ITurnProcessor
    {
        public void ProcessTurn(Game game)
        {
            foreach (var company in game.Companies)
            {
                if (company.Status == D.Companies.CompanyStatus.Bankrupt)
                {
                    continue;
                }

                var employeeTax = 0;

                foreach (var employee in company.Employees)
                {
                    var cost = employee.Person.Skills.Sum(s => s.Value) / 1000;

                    employeeTax += cost;
                }

                if (employeeTax > 0)
                {
                    company.Money -= employeeTax;
                    company.Transactions.Add(new Transaction
                    {
                        Id = Guid.NewGuid(),
                        Amount = -employeeTax,
                        Date = game.CurrentDate,
                        Description = "Employee taxes",
                        EndingBalance = company.Money,
                        Type = D.Companies.TransactionType.EmployeeTax
                    });
                }

                var perkTax = company.Perks.Count;

                if (perkTax > 0)
                {
                    company.Money -= perkTax;
                    company.Transactions.Add(new Transaction
                    {
                        Id = Guid.NewGuid(),
                        Amount = -perkTax,
                        Date = game.CurrentDate,
                        Description = "Perk taxes",
                        EndingBalance = company.Money,
                        Type = D.Companies.TransactionType.PerkTax
                    });
                }

                var projectTax = company.Projects.Count;

                if (projectTax > 0)
                {
                    company.Money -= projectTax;

                    company.Transactions.Add(new Transaction
                    {
                        Id = Guid.NewGuid(),
                        Amount = -projectTax,
                        Date = game.CurrentDate,
                        Description = "Project taxes",
                        EndingBalance = company.Money,
                        Type = D.Companies.TransactionType.ProjectTax
                    });
                }
            }
        }
    }
}
