﻿using Bizio.Core.Model.Game;
using Bizio.Core.Model.People;
using Bizio.Core.Services;
using D = Bizio.Core.Data;

namespace BlackPlain.Bizio.Services.Processors
{
    internal class EmployeeSkillChangeProcessor : ITurnProcessor
    {
        public void ProcessTurn(Game game)
        {
            foreach (var company in game.Companies)
            {
                if (company.Status == D.Companies.CompanyStatus.Bankrupt)
                {
                    continue;
                }

                foreach (var allocation in company.Allocations)
                {
                    foreach (var requirement in allocation.Project.Requirements)
                    {
                        var skill = allocation.Employee.Person.Skills.FirstOrDefault(s => s.SkillDefinition == requirement.SkillDefinition);

                        if (skill != null)
                        {
                            skill.Value += skill.LearnRate;
                        }
                        else
                        {
                            skill = new Skill
                            {
                                SkillDefinition = requirement.SkillDefinition,
                                Value = skill.LearnRate
                            };

                            allocation.Employee.Person.Skills.Add(skill);
                        }

                        skill.Value = Math.Min(skill.Value, skill.SkillDefinition.Value.Maximum);
                    }
                }

                foreach (var employee in company.Employees)
                {
                    var allocations = company.Allocations.Where(a => a.Employee == employee);

                    foreach (var skill in employee.Person.Skills)
                    {
                        var allocation = allocations.Any(a => a.Project.Requirements.Any(r => r.SkillDefinition == skill.SkillDefinition));

                        if (!allocation)
                        {
                            skill.Value -= skill.ForgetRate;

                            skill.Value = Math.Max(skill.SkillDefinition.Value.Minimum, skill.Value);
                        }
                    }
                }
            }
        }
    }
}
