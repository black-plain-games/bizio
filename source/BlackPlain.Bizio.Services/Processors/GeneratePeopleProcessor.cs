﻿using Bizio.Core.Model.Game;
using Bizio.Core.Services;
using K = Bizio.Core.Data.Configuration.ConfigurationKey;

namespace BlackPlain.Bizio.Services.Processors
{
    internal class GeneratePeopleProcessor(
        IConfigurationService _configurationService,
        IPersonService _personService
        ) : ITurnProcessor
    {
        public void ProcessTurn(Game game)
        {
            var minimumPersonCount = _configurationService.Get<int>(game, K.MinimumPersonCount);

            if (game.People.Count >= minimumPersonCount)
            {
                return;
            }

            var count = minimumPersonCount - game.People.Count;

            var people = _personService.CreatePeople(count, game.Industry, game.CurrentDate);

            foreach (var person in people)
            {
                game.People.Add(person);
            }
        }
    }
}
