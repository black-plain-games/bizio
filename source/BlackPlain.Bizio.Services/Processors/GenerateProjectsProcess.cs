﻿using Bizio.Core.Model.Game;
using Bizio.Core.Services;
using K = Bizio.Core.Data.Configuration.ConfigurationKey;

namespace BlackPlain.Bizio.Services.Processors
{
    internal class GenerateProjectsProcess(
        IConfigurationService _configurationService,
        IProjectService _projectService
        ) : ITurnProcessor
    {
        public void ProcessTurn(Game game)
        {
            var minimumProjectCount = _configurationService.Get<int>(game, K.MinimumProjectCount);

            if (game.Projects.Count >= minimumProjectCount)
            {
                return;
            }

            var count = minimumProjectCount - game.Projects.Count;

            var projects = _projectService.CreateProjects(count, game.Industry, game.CurrentDate);

            foreach (var project in projects)
            {
                game.Projects.Add(project);
            }
        }
    }
}
