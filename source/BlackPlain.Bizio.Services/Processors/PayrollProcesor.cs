﻿using Bizio.Core.Model.Companies;
using Bizio.Core.Model.Game;
using Bizio.Core.Services;
using D = Bizio.Core.Data;

namespace BlackPlain.Bizio.Services.Processors
{
    internal class PayrollProcesor : ITurnProcessor
    {
        public void ProcessTurn(Game game)
        {
            foreach (var company in game.Companies)
            {
                if (company.Status == D.Companies.CompanyStatus.Bankrupt)
                {
                    continue;
                }

                foreach (var employee in company.Employees)
                {
                    if (employee.Salary > 0)
                    {
                        company.Money -= employee.Salary;

                        company.Transactions.Add(new Transaction
                        {
                            Id = Guid.NewGuid(),
                            Amount = -employee.Salary,
                            Date = game.CurrentDate,
                            Description = $"Salary for {employee.Person.FirstName} {employee.Person.LastName}",
                            EndingBalance = company.Money,
                            Type = D.Companies.TransactionType.Payroll
                        });
                    }
                }
            }
        }
    }
}
