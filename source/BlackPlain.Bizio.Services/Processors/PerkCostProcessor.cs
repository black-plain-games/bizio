﻿using Bizio.Core.Model.Companies;
using Bizio.Core.Model.Game;
using Bizio.Core.Services;
using D = Bizio.Core.Data;

namespace BlackPlain.Bizio.Services.Processors
{
    internal class PerkCostProcessor : ITurnProcessor
    {
        public void ProcessTurn(Game game)
        {
            foreach (var company in game.Companies)
            {
                if (company.Status == D.Companies.CompanyStatus.Bankrupt)
                {
                    continue;
                }

                foreach (var perk in company.Perks)
                {
                    if (!perk.NextPaymentDate.HasValue ||
                        !perk.Perk.RecurringCost.HasValue ||
                        perk.NextPaymentDate > game.CurrentDate)
                    {
                        continue;
                    }

                    company.Money -= perk.Perk.RecurringCost.Value;

                    company.Transactions.Add(new Transaction
                    {
                        Id = Guid.NewGuid(),
                        Amount = -perk.Perk.RecurringCost.Value,
                        Date = game.CurrentDate,
                        Description = $"Perk {perk.Perk.Name} cost",
                        EndingBalance = company.Money,
                        Type = D.Companies.TransactionType.Perk
                    });

                    perk.NextPaymentDate = game.CurrentDate.AddDays(perk.Perk.RecurringCostInterval.Value);
                }
            }
        }
    }
}
