﻿using Bizio.Core.Model.Game;
using Bizio.Core.Services;
using C = Bizio.Core.Model.Companies;
using D = Bizio.Core.Data;

namespace BlackPlain.Bizio.Services.Processors
{
    internal class PersonRetirementProcessor : ITurnProcessor
    {
        public void ProcessTurn(Game game)
        {
            var retiredPeople = game.People.Where(p => p.RetirementDate <= game.CurrentDate).ToList();

            foreach (var person in retiredPeople)
            {
                game.People.Remove(person);
            }

            foreach (var company in game.Companies)
            {
                if (company.Status == D.Companies.CompanyStatus.Bankrupt)
                {
                    continue;
                }

                var retiredEmployees = company.Employees.Where(e => e.Person.RetirementDate <= game.CurrentDate).ToList();

                foreach (var employee in retiredEmployees)
                {
                    company.Employees.Remove(employee);

                    company.Messages.Add(new C.CompanyMessage
                    {
                        Id = Guid.NewGuid(),
                        DateCreated = game.CurrentDate,
                        Source = "Human Resources",
                        Subject = "Employee Retired",
                        Message = $"Employee {employee.Person.FirstName} {employee.Person.LastName} retired",
                        Status = D.Companies.CompanyMessageStatus.UnRead
                    });
                }
            }
        }
    }
}
