﻿using Bizio.Core.Model.Companies;
using Bizio.Core.Model.Game;
using Bizio.Core.Services;
using C = Bizio.Core.Data.Companies;
using D = Bizio.Core.Data;
using P = Bizio.Core.Data.Projects;

namespace BlackPlain.Bizio.Services.Processors
{
    internal class ProjectProgressProcessor(IProjectService _projectService) : ITurnProcessor
    {
        public void ProcessTurn(Game game)
        {
            foreach (var company in game.Companies)
            {
                if (company.Status == D.Companies.CompanyStatus.Bankrupt)
                {
                    continue;
                }

                foreach (var allocation in company.Allocations)
                {
                    foreach (var requirement in allocation.Project.Requirements)
                    {
                        var skill = allocation.Employee.Person.Skills.FirstOrDefault(s => s.SkillDefinition == requirement.SkillDefinition);

                        if (skill == null)
                        {
                            continue;
                        }

                        requirement.CurrentValue += (int)(skill.Value * (allocation.Percent / 100.0));

                        requirement.CurrentValue = Math.Min(requirement.CurrentValue, requirement.TargetValue);
                    }
                }

                foreach (var project in company.Projects)
                {
                    var deadline = project.ExtensionDeadline ?? project.Deadline;

                    if (deadline <= game.CurrentDate)
                    {
                        _projectService.CompleteProject(game, company, project);
                    }
                    else if (project.Requirements.All(r => r.CurrentValue >= r.TargetValue))
                    {
                        _projectService.CompleteProject(game, company, project);
                    }

                    if (project.Status == P.ProjectStatus.Completed)
                    {
                        if (project.Result == P.ProjectResult.Failure)
                        {
                            company.Messages.Add(new CompanyMessage
                            {
                                Id = Guid.NewGuid(),
                                DateCreated = game.CurrentDate,
                                Source = "Project Management",
                                Subject = "Project failed",
                                Message = $"Project {project.ProjectDefinition.Name} was a failure. Our reputation has been impacted negatively.",
                                Status = C.CompanyMessageStatus.UnRead
                            });
                        }
                        else
                        {
                            company.Messages.Add(new CompanyMessage
                            {
                                Id = Guid.NewGuid(),
                                DateCreated = game.CurrentDate,
                                Source = "Project Management",
                                Subject = "Project completed successfully",
                                Message = $"Project {project.ProjectDefinition.Name} was a success. We earned {project.Value}.",
                                Status = C.CompanyMessageStatus.UnRead
                            });
                        }
                    }
                }
            }

            var projects = game.Projects.ToList();

            foreach (var project in projects)
            {
                if (project.Deadline > game.CurrentDate)
                {
                    continue;
                }

                game.Projects.Remove(project);
            }
        }
    }
}
