﻿using Bizio.Core;
using Bizio.Core.Model.Companies;
using Bizio.Core.Model.Game;
using Bizio.Core.Model.Projects;
using Bizio.Core.Utilities;
using D = Bizio.Core.Data;
using P = Bizio.Core.Data.Projects;

namespace BlackPlain.Bizio.Services
{
    public interface IProjectService
    {
        IEnumerable<Project> CreateProjects(int count, Industry industry, DateTime currentDate);
        void CompleteProject(Game game, Company company, Project project);
    }

    internal class ProjectService(IStaticDataService _staticDataService) : IProjectService
    {
        public IEnumerable<Project> CreateProjects(int count, Industry industry, DateTime currentDate)
        {
            var data = _staticDataService.StaticData;

            var output = new List<Project>();

            while (output.Count < count)
            {
                var definition = industry.ProjectDefinitions.Random();

                var length = Utilities.GetRandomInt(definition.ProjectLength);

                var requirements = definition.Skills
                    .Where(s => s.IsRequired)
                    .Concat(definition.Skills
                        .Where(s => !s.IsRequired && Utilities.GetRandomBool()))
                    .Select(s => new ProjectRequirement
                    {
                        CurrentValue = 0,
                        SkillDefinition = s.SkillDefinition,
                        TargetValue = Utilities.GetRandomInt(s.Value)
                    })
                    .Observe();

                output.Add(new Project
                {
                    Id = Guid.NewGuid(),
                    ProjectDefinition = definition,
                    Status = P.ProjectStatus.Unassigned,
                    ReputationRequired = Utilities.GetRandomValue<P.StarCategory>(),
                    Deadline = currentDate.AddDays(length),
                    Value = (Utilities.GetRandomInt(definition.Value) / 100) * 100,
                    Requirements = requirements
                });
            }

            return output;
        }

        public void CompleteProject(Game game, Company company, Project project)
        {
            var allocations = company.Allocations.Where(a => a.Project == project).ToList();

            foreach (var allocationToRemove in allocations)
            {
                company.Allocations.Remove(allocationToRemove);
            }

            project.Status = P.ProjectStatus.Completed;

            var currentTotal = 0;
            var targetTotal = 0;

            foreach (var requirement in project.Requirements)
            {
                currentTotal += Math.Min(requirement.CurrentValue, requirement.TargetValue);
                targetTotal += requirement.TargetValue;
            }

            var minumum = targetTotal * 0.8;
            var reputation = (int)project.ReputationRequired;

            company.Reputation.PossibleStars += reputation + 1;

            if (currentTotal < minumum)
            {
                project.Result = P.ProjectResult.Failure;

                return;
            }

            project.Result = P.ProjectResult.Success;
            company.Reputation.EarnedStars += reputation + 1;
            company.Money += project.Value;

            company.Transactions.Add(new Transaction
            {
                Id = Guid.NewGuid(),
                Amount = project.Value,
                Date = game.CurrentDate,
                Description = $"Project {project.ProjectDefinition.Name} completed",
                EndingBalance = company.Money,
                Type = D.Companies.TransactionType.Project
            });

            company.Projects.Remove(project);
        }
    }
}
