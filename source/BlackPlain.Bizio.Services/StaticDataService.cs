﻿using Bizio.Core;
using Bizio.Core.Model;
using Bizio.Core.Model.Companies;
using Bizio.Core.Model.People;
using Bizio.Core.Model.Perks;
using Bizio.Core.Model.Projects;
using Bizio.Core.Repositories;
using MA = Bizio.Core.Model.Actions;

namespace BlackPlain.Bizio.Services
{
    public interface IStaticDataService
    {
        StaticData StaticData { get; }
        Task<StaticData> LoadAsync(CancellationToken cancellationToken);
        Task<StaticData> LoadAsync(bool forceReload, CancellationToken cancellationToken);
    }

    internal class StaticDataService(IStaticDataRepository _staticDataRepository) : IStaticDataService
    {
        public StaticData StaticData
        {
            get => _staticData ?? throw new InvalidOperationException("StaticData has not been loaded");
            set => _staticData = value;
        }

        public async Task<StaticData> LoadAsync(CancellationToken cancellationToken)
        {
            return _staticData = await LoadAsync(false, cancellationToken);
        }

        public async Task<StaticData> LoadAsync(bool forceReload, CancellationToken cancellationToken)
        {
            if (_staticData != null && !forceReload)
            {
                return _staticData;
            }

            var data = await _staticDataRepository.LoadAsync(forceReload, cancellationToken);

            _staticData = new StaticData
            {
                SkillDefinitions = [],
                Actions = [],
                DefaultPeople = [],
                Industries = [],
                Personalities = [],
                FirstNames = data.FirstNames,
                LastNames = data.LastNames
            };

            // Skill definitions
            foreach ((_, var skillDefinition) in data.SkillDefinitions)
            {
                _staticData.SkillDefinitions.Add(new SkillDefinition
                {
                    Id = skillDefinition.Id,
                    Name = skillDefinition.Name,
                    Value = skillDefinition.Value
                });
            }

            // Actions
            foreach ((_, var action) in data.Actions)
            {
                var a = new MA.Action
                {
                    Id = action.Id,
                    Type = action.Type,
                    Name = action.Name,
                    DefaultCount = action.DefaultCount,
                    MaxCount = action.MaxCount,
                    Requirements = []
                };

                foreach (var requirement in action.Requirements)
                {
                    var r = new MA.ActionRequirement
                    {
                        Value = requirement.Value,
                        SkillDefinition = _staticData.SkillDefinitions.Find(requirement.SkillDefinitionId)
                    };

                    a.Requirements.Add(r);
                }

                _staticData.Actions.Add(a);
            }

            // Personalities
            foreach ((_, var personality) in data.Personalities)
            {
                _staticData.Personalities.Add(new Personality
                {
                    Id = personality.Id,
                    Name = personality.Name,
                    Description = personality.Description,
                    Attributes = personality.Attributes.Select(a => new PersonalityAttribute
                    {
                        Id = a.Id,
                        Value = a.Value
                    }).Observe(),
                });
            }

            // Default People
            // Gotta make default people tied to an industry so we can avoid the SelectMany here
            foreach ((_, var person) in data.DefaultPeople)
            {
                _staticData.DefaultPeople.Add(new Person
                {
                    Id = person.Id,
                    Birthday = person.Birthday,
                    FirstName = person.FirstName,
                    Gender = person.Gender,
                    LastName = person.LastName,
                    RetirementDate = person.RetirementDate,
                    Personality = _staticData.Personalities.Find(person.PersonalityId),
                    Profession = _staticData.Industries.SelectMany(i => i.Professions).FirstOrDefault(p => p.Id == person.ProfessionId),
                    Skills = person.Skills.Select(s => new Skill
                    {
                        SkillDefinition = _staticData.SkillDefinitions.Find(s.SkillDefinitionId),
                        ForgetRate = s.ForgetRate,
                        LearnRate = s.LearnRate,
                        Value = s.Value
                    }).Observe(),
                    WorkHistory = []
                });
            }

            // Industries
            foreach ((_, var industry) in data.Industries)
            {
                _staticData.Industries.Add(new Industry
                {
                    Id = industry.Id,
                    Name = industry.Name,
                    CompanyNames = industry.CompanyNames.Observe(),
                    MandatorySkills = industry.MandatorySkills.Select(_staticData.SkillDefinitions.Find).Observe(),
                    OptionalSkills = industry.OptionalSkills.Select(_staticData.SkillDefinitions.Find).Observe(),
                    Professions = industry.Professions.Select(p => new Profession
                    {
                        Id = p.Id,
                        Name = p.Name,
                        SkillDefinitions = p.SkillDefinitions.Select(psd => new ProfessionSkillDefinition
                        {
                            SkillDefinition = _staticData.SkillDefinitions.Find(psd.SkillDefinitionId),
                            Weight = psd.Weight
                        }).Observe()
                    }).Observe(),
                    ProjectDefinitions = industry.ProjectDefinitions.Select(pd => new ProjectDefinition
                    {
                        Id = pd.Id,
                        Name = pd.Name,
                        Description = pd.Description,
                        ProjectLength = pd.ProjectLength,
                        Value = pd.Value,
                        Skills = pd.Skills.Select(s => new ProjectDefinitionSkill
                        {
                            SkillDefinition = _staticData.SkillDefinitions.Find(s.SkillDefinitionId),
                            IsRequired = s.IsRequired,
                            Value = s.Value
                        }).Observe()
                    }).Observe(),
                    Perks = industry.Perks.Select(p => new Perk
                    {
                        Id = p.Id,
                        Name = p.Name,
                        Description = p.Description,
                        InitialCost = p.InitialCost,
                        RecurringCost = p.RecurringCost,
                        RecurringCostInterval = p.RecurringCostInterval,
                        MaxCount = p.MaxCount,
                        Benefits = p.Benefits.Select(b => new PerkBenefit
                        {
                            Attribute = b.Attribute,
                            Target = b.Target,
                            Value = b.Value,
                            ValueType = b.ValueType
                        }).Observe(),
                        Conditions = p.Conditions.Select(c => new PerkCondition
                        {
                            Attribute = c.Attribute,
                            Target = c.Target,
                            Comparison = c.Comparison,
                            Value = c.Value
                        }).Observe()
                    }).Observe()
                });
            }

            return StaticData;
        }

        private StaticData? _staticData;
    }
}
