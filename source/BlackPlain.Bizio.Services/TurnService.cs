﻿using Bizio.Core.Model.Companies;
using Bizio.Core.Model.Game;
using Bizio.Core.Services;
using System.Diagnostics;
using D = Bizio.Core.Data;

namespace BlackPlain.Bizio.Services
{
    public interface ITurnService
    {
        void ProcessTurn(Game game);
    }

    internal class TurnService(
        IEnumerable<IActionHandler> _actionHandlers,
        IEnumerable<ITurnProcessor> _turnProcessors
        ) : ITurnService
    {
        public void ProcessTurn(Game game)
        {
            foreach (var turnProcessor in _turnProcessors)
            {
                Debug.WriteLine($"Running turn processor: {turnProcessor.GetType().Name}");
                try
                {
                    turnProcessor.ProcessTurn(game);
                }
                catch (Exception err)
                {
                    Debug.WriteLine(err.Message);
                }
            }

            var random = new Random(game.CurrentTurn);

            var allTurnActions = game.Companies
                .SelectMany(c => c.TurnActions)
                .OrderBy(_ => random.Next())
                .ToList();

            foreach (var turnAction in allTurnActions)
            {
                var company = game.Companies.First(c => c.TurnActions.Contains(turnAction));

                company.TurnActions.Remove(turnAction);

                var handler = _actionHandlers.FirstOrDefault(x => x.Type == turnAction.ActionType) ?? throw new Exception($"No handler found for action type {turnAction.ActionType}");

                var canPerformResult = handler.CanPerformAction(company, turnAction);

                if (!canPerformResult.CanPerformAction)
                {
                    company.Messages.Add(new CompanyMessage
                    {
                        Id = Guid.NewGuid(),
                        DateCreated = game.CurrentDate,
                        Source = "HR",
                        Status = D.Companies.CompanyMessageStatus.UnRead,
                        Message = $"Action {turnAction.ActionType} cannot be performed: {canPerformResult.Message}"
                    });

                    continue;
                }

                var companyAction = company.Actions.First(a => a.Action.Type == turnAction.ActionType);
                companyAction.Count--;

                var response = handler.ProcessAction(turnAction, game, company);

                if (response != null)
                {
                    company.Messages.Add(response);
                }
            }

            game.CurrentTurn++;
        }
    }
}
