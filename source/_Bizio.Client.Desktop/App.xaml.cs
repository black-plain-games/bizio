﻿using Bizio.Client.Desktop.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Threading;

namespace Bizio.Client.Desktop
{
    public partial class App : Application
    {
        public App()
        {
            DispatcherUnhandledException += OnUnhandledException;
        }

        private void OnUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            List<Exception> exceptions;

            if (!File.Exists(Paths.Exceptions))
            {
                using (var file = File.Create(Paths.Exceptions))
                {
                }

                exceptions = new List<Exception>();
            }
            else
            {
                try
                {
                    var fileContents = "[]";

                    using (var reader = new StreamReader(Paths.Exceptions))
                    {
                        fileContents = reader.ReadToEnd();
                    }

                    exceptions = JsonConvert.DeserializeObject<List<Exception>>(fileContents);
                }
                catch (Exception err)
                {
                    exceptions = new List<Exception>
                    {
                        err
                    };
                }
            }

            exceptions.Add(e.Exception);

            try
            {
                using (var writer = new StreamWriter(Paths.Exceptions))
                {
                    writer.Write(JsonConvert.SerializeObject(exceptions));
                }
            }
            finally { }
        }
    }
}