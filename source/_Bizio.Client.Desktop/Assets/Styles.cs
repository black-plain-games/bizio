﻿using Bizio.Client.Desktop.Core;
using System.Windows;
using System.Windows.Controls;

namespace Bizio.Client.Desktop.Assets
{
    public partial class Styles
    {
        private void OnTextBoxTextChanged(object sender, TextChangedEventArgs e) => TextBoxExtensions.OnTextChanged(sender as TextBox);

        private void OnTextBoxGotFocus(object sender, RoutedEventArgs e) => TextBoxExtensions.OnGotFocus(sender as TextBox);

        private void OnTextBoxLostFocus(object sender, RoutedEventArgs e) => TextBoxExtensions.OnLostFocus(sender as TextBox);
    }
}