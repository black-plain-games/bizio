﻿using Bizio.Client.Desktop.Services;
using BlackPlain.DI;
using Microsoft.Extensions.Options;
using System.Windows;

namespace Bizio.Client.Desktop
{
    public class BizioClientBundle : IResolverBundle
    {
        public void CreateResolvers(IHub hub)
        {
            hub.CreateResolver<IOptions<BizioClientOptions>, RegistryOptions>();

            hub.CreateResolver<IBizioClient, BizioClient>();

            hub.CreateResolver<IApplicationSettingService, ApplicationSettingService>();

            hub.CreateResolver<IEmailService, EmailService>();

            hub.CreateResolver<IAuthorizationClientService, AuthorizationClientService>();

            hub.CreateResolver<IResourceClientService, ResourceClientService>();

            hub.CreateResolver<IViewStateService, ViewStateService>();

            hub.CreateResolver<INotificationService, NotificationService>();

            hub.CreateResolver<IGameService, GameService>();
        }

        public void SetResolutions(IHub hub)
        {
            hub.SetResolution(Application.Current);
        }
    }
}