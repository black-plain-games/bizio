﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows;

namespace Bizio.Client.Desktop.Controls
{
    public class Accordion : BaseControl, IList
    {
        public static readonly DependencyProperty ItemsProperty = Register<IList, Accordion>(nameof(Items));

        public IList Items
        {
            get
            {
                return GetValue<IList>(ItemsProperty);
            }
            set
            {
                SetValue(ItemsProperty, value);
            }
        }

        static Accordion()
        {
            ResetDefaultStyleKey<Accordion>();
        }

        public Accordion()
        {
            Items = new List<AccordionItem>();
        }

        #region IList Implementation

        public bool IsReadOnly => Items.IsReadOnly;

        public bool IsFixedSize => Items.IsFixedSize;

        public int Count => Items.Count;

        public object SyncRoot => Items.SyncRoot;

        public bool IsSynchronized => Items.IsSynchronized;

        public object this[int index]
        {
            get
            {
                return Items[index];
            }
            set
            {
                Items[index] = value;
            }
        }

        public int Add(object value) => Items.Add(value);

        public bool Contains(object value) => Items.Contains(value);

        public void Clear() => Items.Clear();

        public int IndexOf(object value) => Items.IndexOf(value);

        public void Insert(int index, object value) => Items.Insert(index, value);

        public void Remove(object value) => Items.Remove(value);

        public void RemoveAt(int index) => Items.RemoveAt(index);

        public void CopyTo(Array array, int index) => Items.CopyTo(array, index);

        public IEnumerator GetEnumerator() => Items.GetEnumerator();

        #endregion
    }
}