﻿using System.Windows;
using System.Windows.Markup;

namespace Bizio.Client.Desktop.Controls
{
    [ContentProperty("Content")]
    public class AccordionItem : BaseControl
    {
        public static readonly DependencyProperty HeaderProperty = Register<object, AccordionItem>(nameof(Header));

        public static readonly DependencyProperty ContentProperty = Register<object, AccordionItem>(nameof(Content));

        public static readonly DependencyProperty IsExpandedProperty = Register<bool, AccordionItem>(nameof(IsExpanded), false);

        public object Header
        {
            get
            {
                return GetValue<object>(HeaderProperty);
            }
            set
            {
                SetValue(HeaderProperty, value);
            }
        }

        public object Content
        {
            get
            {
                return GetValue<object>(ContentProperty);
            }
            set
            {
                SetValue(ContentProperty, value);
            }
        }

        public bool IsExpanded
        {
            get
            {
                return GetValue<bool>(IsExpandedProperty);
            }
            set
            {
                SetValue(IsExpandedProperty, value);
            }
        }

        static AccordionItem()
        {
            ResetDefaultStyleKey<AccordionItem>();
        }
    }
}