﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Media;

namespace Bizio.Client.Desktop.Controls
{
    public partial class AccordionStyles
    {
        protected void OnAccordionItemSizeChanged(object sender, SizeChangedEventArgs args)
        {
            ResizeItems(sender);
        }

        protected void OnResizeItems(object sender, EventArgs args)
        {
            ResizeItems(sender);
        }

        protected void ResizeItems(object sender)
        {
            var accordion = FindParent<Accordion>(sender as DependencyObject);

            var items = accordion.Items.Cast<AccordionItem>();

            var visibleItems = items.Where(ai => ai.Visibility != Visibility.Collapsed);

            var expandedItems = visibleItems.Where(ai => ai.IsExpanded);

            var collapsedItems = visibleItems.Where(ai => !ai.IsExpanded);

            foreach (var item in collapsedItems)
            {
                item.Height = double.NaN;
            }

            var collapsedSize = collapsedItems.Sum(ai => ai.ActualHeight);

            var availableHeight = accordion.ActualHeight - collapsedSize;

            if (expandedItems.Any())
            {
                var expandedSize = availableHeight / (double)expandedItems.Count();

                foreach (var item in expandedItems)
                {
                    item.Height = expandedSize;
                }
            }
        }

        protected T FindParent<T>(DependencyObject origin)
            where T : FrameworkElement
        {
            if (!(VisualTreeHelper.GetParent(origin) is FrameworkElement parent))
            {
                return null;
            }

            while (!(parent is T))
            {
                parent = VisualTreeHelper.GetParent(parent) as FrameworkElement;

                if (parent == null)
                {
                    return null;
                }
            }

            return (T)parent;
        }

        protected T FindParent<T>(DependencyObject origin, string name)
            where T : FrameworkElement
        {
            if (!(VisualTreeHelper.GetParent(origin) is FrameworkElement parent))
            {
                return null;
            }

            while (!(parent is T) || parent.Name != name)
            {
                parent = VisualTreeHelper.GetParent(parent) as FrameworkElement;

                if (parent == null)
                {
                    return null;
                }
            }

            return (T)parent;
        }
    }
}