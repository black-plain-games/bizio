﻿using System.Windows;

namespace Bizio.Client.Desktop.Controls
{
    public class Badge : BaseControl
    {
        public static readonly DependencyProperty ValueProperty = Register<object, Badge>(nameof(Value));

        public static readonly DependencyProperty HideValueProperty = Register<object, Badge>(nameof(HideValue));

        public static readonly DependencyProperty LabelProperty = Register<object, Badge>(nameof(Label));

        public static readonly DependencyProperty ValueSizeProperty = Register<double, Badge>(nameof(ValueSize), 15);

        public static readonly DependencyProperty ValuePositionProperty = Register<VerticalAlignment, Badge>(nameof(ValuePosition), VerticalAlignment.Center);

        public static readonly DependencyProperty ValueCornerRadiusProperty = Register<CornerRadius, Badge>(nameof(ValueCornerRadius), new CornerRadius(10));

        public object Value
        {
            get
            {
                return GetValue(ValueProperty);
            }
            set
            {
                SetValue(ValueProperty, value);
            }
        }

        public object HideValue
        {
            get
            {
                return GetValue(HideValueProperty);
            }
            set
            {
                SetValue(HideValueProperty, value);
            }
        }

        public object Label
        {
            get
            {
                return GetValue(LabelProperty);
            }
            set
            {
                SetValue(LabelProperty, value);
            }
        }

        public double ValueSize
        {
            get
            {
                return GetValue<double>(ValueSizeProperty);
            }
            set
            {
                SetValue(ValueSizeProperty, value);
            }
        }

        public VerticalAlignment ValuePosition
        {
            get
            {
                return GetValue<VerticalAlignment>(ValuePositionProperty);
            }
            set
            {
                SetValue(ValuePositionProperty, value);
            }
        }

        public CornerRadius ValueCornerRadius
        {
            get
            {
                return GetValue<CornerRadius>(ValueCornerRadiusProperty);
            }
            set
            {
                SetValue(ValueCornerRadiusProperty, value);
            }
        }

        static Badge()
        {
            ResetDefaultStyleKey<Badge>();
        }
    }
}