﻿using System.Windows;
using System.Windows.Controls;

namespace Bizio.Client.Desktop.Controls
{
    public abstract class BaseControl : Control
    {
        protected static DependencyProperty Register<TData, TOwner>(string name) => Register<TData, TOwner>(name, default);

        protected static DependencyProperty Register<TData, TOwner>(string name, TData defaultValue) => Register<TData, TOwner>(name, defaultValue, FrameworkPropertyMetadataOptions.None);

        protected static DependencyProperty Register<TData, TOwner>(string name, TData defaultValue, FrameworkPropertyMetadataOptions flags) => DependencyProperty.Register(name, typeof(TData), typeof(TOwner), new FrameworkPropertyMetadata(defaultValue, flags));

        protected T GetValue<T>(DependencyProperty property)
        {
            return (T)GetValue(property);
        }

        protected static void ResetDefaultStyleKey<T>()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(T), new FrameworkPropertyMetadata(typeof(T)));
        }
    }
}