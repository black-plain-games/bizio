﻿using System.Windows;

namespace Bizio.Client.Desktop.Controls
{
    public partial class ControlStyles
    {
        public void OnBadgeSizeChanged(object sender, SizeChangedEventArgs args)
        {
            var border = sender as FrameworkElement;

            if (args.NewSize.Width < args.NewSize.Height)
            {
                border.MinWidth = args.NewSize.Height;
            }
        }
    }
}