﻿using Bizio.Client.Desktop.Model.Companies;
using Bizio.Client.Desktop.Model.Perks;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;

namespace Bizio.Client.Desktop.Controls.Panels
{
    public class PerkPanel : BaseControl
    {
        public static readonly DependencyProperty PerkProperty = Register<Perk, PerkPanel>(nameof(Perk));

        public static readonly DependencyProperty CompanyPerksProperty = Register<IEnumerable<CompanyPerk>, PerkPanel>(nameof(CompanyPerks));

        public static readonly DependencyProperty PurchasePerkCommandProperty = Register<ICommand, PerkPanel>(nameof(PurchasePerkCommand));

        public static readonly DependencyProperty SellPerkCommandProperty = Register<ICommand, PerkPanel>(nameof(SellPerkCommand));

        public static readonly DependencyProperty CancelSellPerkCommandProperty = Register<ICommand, PerkPanel>(nameof(CancelSellPerkCommand));

        public Perk Perk
        {
            get
            {
                return GetValue<Perk>(PerkProperty);
            }
            set
            {
                SetValue(PerkProperty, value);
            }
        }

        public IEnumerable<CompanyPerk> CompanyPerks
        {
            get
            {
                return GetValue<IEnumerable<CompanyPerk>>(CompanyPerksProperty);
            }
            set
            {
                SetValue(CompanyPerksProperty, value);
            }
        }

        public ICommand PurchasePerkCommand
        {
            get
            {
                return GetValue<ICommand>(PurchasePerkCommandProperty);
            }
            set
            {
                SetValue(PurchasePerkCommandProperty, value);
            }
        }

        public ICommand CancelSellPerkCommand
        {
            get
            {
                return GetValue<ICommand>(CancelSellPerkCommandProperty);
            }
            set
            {
                SetValue(CancelSellPerkCommandProperty, value);
            }
        }

        public ICommand SellPerkCommand
        {
            get
            {
                return GetValue<ICommand>(SellPerkCommandProperty);
            }
            set
            {
                SetValue(SellPerkCommandProperty, value);
            }
        }

        static PerkPanel()
        {
            ResetDefaultStyleKey<PerkPanel>();
        }
    }
}