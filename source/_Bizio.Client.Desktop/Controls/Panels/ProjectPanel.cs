﻿using Bizio.Client.Desktop.Model.Projects;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;

namespace Bizio.Client.Desktop.Controls.Panels
{
    public class ProjectPanel : BaseControl
    {
        public static readonly DependencyProperty ProjectProperty = Register<Project, ProjectPanel>(nameof(Project));

        public static readonly DependencyProperty AddAllocationCommandProperty = Register<ICommand, ProjectPanel>(nameof(AddAllocationCommand));

        public static readonly DependencyProperty ReputationVisibilityProperty = Register<Visibility, ProjectPanel>(nameof(ReputationVisibility), Visibility.Collapsed);

        public static readonly DependencyProperty ToggleExtensionRequestCommandProperty = Register<ICommand, ProjectPanel>(nameof(ToggleExtensionRequestCommand));

        public static readonly DependencyProperty ToggleExtensionRequestVisibilityProperty = Register<Visibility, ProjectPanel>(nameof(ToggleExtensionRequestVisibility), Visibility.Collapsed);

        public static readonly DependencyProperty RemainingTurnActionCountProperty = Register<IDictionary<byte, int>, ProjectPanel>(nameof(RemainingTurnActionCount));

        public Project Project
        {
            get
            {
                return GetValue<Project>(ProjectProperty);
            }
            set
            {
                SetValue(ProjectProperty, value);
            }
        }

        public ICommand AddAllocationCommand
        {
            get
            {
                return GetValue<ICommand>(AddAllocationCommandProperty);
            }
            set
            {
                SetValue(AddAllocationCommandProperty, value);
            }
        }

        public Visibility ReputationVisibility
        {
            get
            {
                return GetValue<Visibility>(ReputationVisibilityProperty);
            }
            set
            {
                SetValue(ReputationVisibilityProperty, value);
            }
        }

        public ICommand ToggleExtensionRequestCommand
        {
            get
            {
                return GetValue<ICommand>(ToggleExtensionRequestCommandProperty);
            }
            set
            {
                SetValue(ToggleExtensionRequestCommandProperty, value);
            }
        }

        public Visibility ToggleExtensionRequestVisibility
        {
            get
            {
                return GetValue<Visibility>(ToggleExtensionRequestVisibilityProperty);
            }
            set
            {
                SetValue(ToggleExtensionRequestVisibilityProperty, value);
            }
        }

        public IDictionary<byte, int> RemainingTurnActionCount
        {
            get
            {
                return GetValue<IDictionary<byte, int>>(RemainingTurnActionCountProperty);
            }
            set
            {
                SetValue(RemainingTurnActionCountProperty, value);
            }
        }

        static ProjectPanel()
        {
            ResetDefaultStyleKey<ProjectPanel>();
        }
    }
}