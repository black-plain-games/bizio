﻿using Bizio.Client.Desktop.Model.Core;
using Bizio.Client.Desktop.Model.People;
using System.Collections.Generic;
using System.Windows;

namespace Bizio.Client.Desktop.Controls.Panels
{
    public class ProspectPanel : BaseControl
    {
        public static readonly DependencyProperty PersonProperty = Register<Person, ProspectPanel>(nameof(Person));

        public static readonly DependencyProperty SalaryProperty = Register<Range<decimal>, ProspectPanel>(nameof(Salary));

        public static readonly DependencyProperty ProspectSkillsProperty = Register<IDictionary<byte, Range<decimal>>, ProspectPanel>(nameof(ProspectSkills));

        public static readonly DependencyProperty SkillsProperty = Register<IEnumerable<Skill>, ProspectPanel>(nameof(Skills));

        public static readonly DependencyProperty OfferValueProperty = Register<decimal?, ProspectPanel>(nameof(OfferValue));

        public static readonly DependencyProperty DisplayAdditionalInformationProperty = Register<bool, ProspectPanel>(nameof(DisplayAdditionalInformation), false);

        public Person Person
        {
            get
            {
                return GetValue<Person>(PersonProperty);
            }
            set
            {
                SetValue(PersonProperty, value);
            }
        }

        public Range<decimal> Salary
        {
            get
            {
                return GetValue<Range<decimal>>(SalaryProperty);
            }
            set
            {
                SetValue(SalaryProperty, value);
            }
        }

        public IDictionary<byte, Range<decimal>> ProspectSkills
        {
            get
            {
                return GetValue<IDictionary<byte, Range<decimal>>>(ProspectSkillsProperty);
            }
            set
            {
                SetValue(ProspectSkillsProperty, value);
            }
        }

        public IEnumerable<Skill> Skills
        {
            get
            {
                return GetValue<IEnumerable<Skill>>(SkillsProperty);
            }
            set
            {
                SetValue(SkillsProperty, value);
            }
        }

        public decimal? OfferValue
        {
            get
            {
                return GetValue<decimal?>(OfferValueProperty);
            }
            set
            {
                SetValue(OfferValueProperty, value);
            }
        }

        public bool DisplayAdditionalInformation
        {
            get
            {
                return GetValue<bool>(DisplayAdditionalInformationProperty);
            }
            set
            {
                SetValue(DisplayAdditionalInformationProperty, value);
            }
        }

        static ProspectPanel()
        {
            ResetDefaultStyleKey<ProspectPanel>();
        }
    }
}