﻿using System.Windows;

namespace Bizio.Client.Desktop.Controls
{
    public class Sprite : SpriteBase
    {
        #region Dependency Properties

        public static readonly DependencyProperty ColumnsProperty = DependencyProperty.Register("Columns", typeof(int), typeof(Sprite),
            new FrameworkPropertyMetadata(1,
                FrameworkPropertyMetadataOptions.AffectsMeasure |
                FrameworkPropertyMetadataOptions.AffectsRender));

        public static readonly DependencyProperty RowsProperty = DependencyProperty.Register("Rows", typeof(int), typeof(Sprite),
            new FrameworkPropertyMetadata(1,
                FrameworkPropertyMetadataOptions.AffectsMeasure |
                FrameworkPropertyMetadataOptions.AffectsRender));

        public static readonly DependencyProperty FrameCountProperty = DependencyProperty.Register("FrameCount", typeof(int), typeof(Sprite),
            new FrameworkPropertyMetadata(1,
                FrameworkPropertyMetadataOptions.AffectsMeasure |
                FrameworkPropertyMetadataOptions.AffectsRender));

        #endregion

        public int Columns
        {
            get
            {
                return (int)GetValue(ColumnsProperty);
            }
            set
            {
                SetValue(ColumnsProperty, value);

                UpdateViewbox();
            }
        }

        public int Rows
        {
            get
            {
                return (int)GetValue(RowsProperty);
            }
            set
            {
                SetValue(RowsProperty, value);

                UpdateViewbox();
            }
        }

        public int FrameCount
        {
            get
            {
                return (int)GetValue(FrameCountProperty);
            }
            set
            {
                SetValue(FrameCountProperty, value);

                UpdateViewbox();
            }
        }

        protected override int GetFrameCount()
        {
            return FrameCount;
        }

        protected override Rect GetViewbox()
        {
            var width = Source.Width / (double)Columns;

            var x = (CurrentFrame % Columns) * width;

            var height = Source.Height / (double)Rows;

            var y = CurrentFrame / Columns * height;

            return new Rect(x, y, width, height);
        }
    }
}