﻿using Bizio.Client.Desktop.Core;
using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Windows;

namespace Bizio.Client.Desktop.Converters
{
    public class ActionDataRequirementConverter : BaseMultiConverter
    {
        public override object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null ||
                values.Length != 2 ||
                values[0] == DependencyProperty.UnsetValue ||
                values[1] == DependencyProperty.UnsetValue)
            {
                return null;
            }

            var actionId = int.Parse($"{values[0]}");

            var skillDefinitionId = byte.Parse($"{values[1]}");

            var action = Utilities.StaticData.ActionsData.Actions.FirstOrDefault(a => a.Id == actionId);

            if (!action.Requirements.ContainsKey(skillDefinitionId))
            {
                Debug.WriteLine($"Action {actionId} does not contain a requirement for skill definition {skillDefinitionId}");

                return null;
            }

            return action.Requirements[skillDefinitionId];
        }
    }
}