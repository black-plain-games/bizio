﻿using Bizio.Client.Desktop.Core;
using Bizio.Client.Desktop.Model.Game.ActionData;
using System;
using System.Globalization;
using System.Linq;

namespace Bizio.Client.Desktop.Converters
{
    public class ActionDataToInfoConverter : GameDataConverter
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var actionData = value as IActionData;

            switch ((ActionType)actionData.ActionTypeId)
            {
                case ActionType.AcceptProject:
                    return GetProjectName((actionData as AcceptProjectActionData).Data.ProjectId);

                case ActionType.AdjustAllocation:
                    return GetPersonFullName((actionData as AdjustAllocationActionData).Data.PersonId);

                case ActionType.AdjustSalary:
                    return GetPersonFullName((actionData as AdjustSalaryActionData).Data.PersonId);

                case ActionType.FireEmployee:
                    return GetPersonFullName((actionData as FireEmployeeActionData).Data.PersonId);

                case ActionType.InterviewProspect:
                    return GetPersonFullName((actionData as InterviewProspectActionData).Data.PersonId);

                case ActionType.MakeOffer:
                    return GetPersonFullName((actionData as MakeOfferActionData).Data.PersonId);

                case ActionType.PurchasePerk:
                    return GetPerkName((actionData as PurchasePerkActionData).Data.PerkId);

                case ActionType.RecruitPerson:
                    return GetPersonFullName((actionData as RecruitPersonActionData).Data.PersonId);

                case ActionType.RequestExtension:
                    return GetProjectName((actionData as RequestExtensionTurnActionData).Data.ProjectId);

                case ActionType.SellPerk:
                    return GetCompanyPerkName((actionData as SellPerkActionData).Data.CompanyPerkId);

                case ActionType.SubmitProject:
                    return GetProjectName((actionData as SubmitProjectTurnActionData).Data.ProjectId);
            }

            return null;
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }

        private string GetProjectName(int id)
        {
            var project = Utilities.GameData.ProjectsData.Projects.FirstOrDefault(p => p.Id == id);

            var projectDefinition = Utilities.StaticData.ProjectsData.ProjectDefinitions.FirstOrDefault(pd => pd.Id == project.ProjectDefinitionId);

            return $"{projectDefinition.Name}";
        }

        private string GetPerkName(int id)
        {
            var perk = Utilities.StaticData.PerksData.Perks.FirstOrDefault(p => p.Id == id);

            return $"{perk.Name}";
        }

        private string GetCompanyPerkName(int id)
        {
            var companyPerk = Utilities.GameData.CompaniesData.Companies.FirstOrDefault(c => c.UserId.HasValue).Perks.FirstOrDefault(cp => cp.Id == id);

            return GetPerkName(companyPerk.PerkId);
        }

        private string GetPersonFullName(int id)
        {
            var person = Utilities.GameData.PeopleData.People.FirstOrDefault(p => p.Id == id);

            return $"{person.FirstName} {person.LastName}";
        }
    }
}