﻿using Bizio.Client.Desktop.Model.Game.ActionData;
using System;
using System.Globalization;

namespace Bizio.Client.Desktop.Converters
{
    public class ActionTypeToStringConverter : BaseConverter
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch ((ActionType)value)
            {
                case ActionType.AcceptProject:
                    return "Accept Project";

                case ActionType.AdjustAllocation:
                    return "Adjust Allocation";

                case ActionType.AdjustSalary:
                    return "Adjust Salary";

                case ActionType.FireEmployee:
                    return "Fire Employee";

                case ActionType.InterviewProspect:
                    return "Interview Prospect";

                case ActionType.MakeOffer:
                    return "Make Offer";

                case ActionType.PurchasePerk:
                    return "Purchase Perk";

                case ActionType.RecruitPerson:
                    return "Recruit Person";

                case ActionType.RequestExtension:
                    return "Request Extension";

                case ActionType.SellPerk:
                    return "Sell Perk";

                case ActionType.SubmitProject:
                    return "Submit Project";
            }

            return "Unknown";
        }
    }
}