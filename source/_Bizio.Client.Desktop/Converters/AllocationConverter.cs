﻿using Bizio.Client.Desktop.Core;
using System;
using System.Globalization;
using System.Linq;
using System.Windows;

namespace Bizio.Client.Desktop.Converters
{
    public class AllocationConverter : BaseMultiConverter
    {
        public override object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null ||
                values.Length < 2 ||
                values[0] == DependencyProperty.UnsetValue ||
                values[1] == DependencyProperty.UnsetValue)
            {
                return null;
            }

            var personId = (int)values[0];

            var projectId = (int)values[1];

            var userCompany = Utilities.GameData.CompaniesData.Companies.FirstOrDefault(c => c.UserId.HasValue);

            var allAllocations = userCompany.Allocations.Concat(Utilities.GameData.NewAllocations);

            var allocation = allAllocations.FirstOrDefault(a => a.PersonId == personId && a.ProjectId == projectId);

            return allocation;
        }
    }
}