﻿using System.Windows.Data;

namespace Bizio.Client.Desktop.Converters
{
    public class BindingWatcher : System.Windows.Data.Binding
    {
        public BindingWatcher()
            : base()
        {
        }

        public BindingWatcher(string path)
            : base(path)
        {
        }

        public new RelativeSource RelativeSource
        {
            get
            {
                return base.RelativeSource;
            }
            set
            {
                base.RelativeSource = value;
            }
        }
    }
}