﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;

namespace Bizio.Client.Desktop.Converters
{
    public class CollectionConcatMultiConverter : BaseMultiConverter
    {
        public override object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var output = new List<object>();

            for (var index = 0; index < values.Length; index++)
            {
                if (values[index] is IEnumerable collection)
                {
                    foreach (var item in collection)
                    {
                        output.Add(item);
                    }

                    continue;
                }
                else if (values[index] != null)
                {
                    output.Add(values[index]);

                }
            }

            return output;
        }
    }
}