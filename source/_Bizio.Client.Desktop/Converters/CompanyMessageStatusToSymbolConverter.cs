﻿using Bizio.Client.Desktop.Core;
using Bizio.Client.Desktop.Model.Companies;
using System;
using System.Globalization;

namespace Bizio.Client.Desktop.Converters
{
    public class CompanyMessageStatusToSymbolConverter : BaseConverter
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch ((CompanyMessageStatus)value)
            {
                case CompanyMessageStatus.Deleted:
                    return Glyphs.DeletedMessageSymbol;

                case CompanyMessageStatus.Read:
                    return Glyphs.ReadMessageSymbol;

                case CompanyMessageStatus.UnRead:
                    return Glyphs.UnReadMessageSymbol;
            }

            throw new ArgumentOutOfRangeException($"Unknown CompanyMessageType '{value}'");
        }
    }
}