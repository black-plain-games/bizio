﻿using System;
using System.Globalization;

namespace Bizio.Client.Desktop.Converters
{
    public class DivisionConverter : BaseConverter
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (parameter == null)
            {
                return value;
            }

            var bNumerator = double.Parse($"{value}");

            var bDenominator = double.Parse($"{parameter}");

            return bNumerator / bDenominator;
        }
    }
}
