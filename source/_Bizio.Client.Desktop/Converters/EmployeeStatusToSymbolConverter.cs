﻿using Bizio.Client.Desktop.Core;
using Bizio.Client.Desktop.Model.Companies;
using System;
using System.Globalization;
using System.Windows.Data;

namespace Bizio.Client.Desktop.Converters
{
    public class EmployeeStatusToSymbolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch (value.AsEnum<EmployeeStatus>())
            {
                case EmployeeStatus.None:
                    return string.Empty;

                case EmployeeStatus.IncreasingSalary:
                    return Glyphs.IncreaseArrowSymbol + culture.NumberFormat.CurrencySymbol;

                case EmployeeStatus.DecreasingSalary:
                    return Glyphs.DecreaseArrowSymbol + culture.NumberFormat.CurrencySymbol;

                case EmployeeStatus.Firing:
                    return Glyphs.FiringSymbol;
            }

            throw new ArgumentOutOfRangeException(nameof(value));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}