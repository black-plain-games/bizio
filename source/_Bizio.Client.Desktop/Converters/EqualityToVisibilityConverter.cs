﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace Bizio.Client.Desktop.Converters
{
    public class EqualityToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool result;

            if (parameter is int)
            {
                var iValue = int.Parse($"{value}");

                var iParameter = int.Parse($"{parameter}");

                result = iValue == iParameter;
            }
            else if (value is bool)
            {
                result = bool.Parse($"{value}") == bool.Parse($"{parameter}");
            }
            else
            {
                var collection = $"{parameter}".Split('|');

                result = collection.Contains($"{value}");
            }

            if (result)
            {
                return Visibility.Visible;
            }

            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}