﻿using System;
using System.Globalization;
using System.Windows;

namespace Bizio.Client.Desktop.Converters
{
    public class EqualityToVisibilityMultiConverter : BaseMultiConverter
    {
        public override object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            bool.TryParse($"{parameter}", out var isInverted);

            var result = false;

            if (values != null || values.Length == 1)
            {
                result = true;

                for (var index = 1; index < values.Length; index++)
                {
                    if ($"{values[index - 1]}" != $"{ values[index]}")
                    {
                        result = false;

                        break;
                    }
                }
            }

            Visibility output;

            if (isInverted)
            {
                output = result ? Visibility.Collapsed : Visibility.Visible;
            }
            else
            {
                output = result ? Visibility.Visible : Visibility.Collapsed;
            }

            return output;
        }
    }
}