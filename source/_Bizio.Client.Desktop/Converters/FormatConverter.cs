﻿using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace Bizio.Client.Desktop.Converters
{
    public class FormatConverter : BaseConverter
    {
        private static readonly Regex _quantifier = new Regex("~(.*?)~");

        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var formatString = $"{parameter}";

            var quantifiers = _quantifier.Matches(formatString);

            if (quantifiers.Count == 0)
            {
                return string.Format(formatString, value);
            }

            var number = decimal.Parse($"{value}");

            foreach (Match quantifier in quantifiers)
            {
                var values = quantifier.Value.
                    Trim('~')
                    .Split('|')
                    .Select(x => x.Split(':'))
                    .ToList();

                var index = 0;

                while (index < values.Count && number > GetKey(values[index]))
                {
                    index++;
                }

                var replacement = GetReplacement(values[index]);

                formatString = formatString.Replace(quantifier.Value, replacement);
            }

            return string.Format(formatString, value);
        }

        private static decimal GetKey(string[] values)
        {
            if (!decimal.TryParse(values[0], out var result))
            {
                return decimal.MaxValue;
            }

            return result;
        }

        private static string GetReplacement(string[] values)
        {
            if (!decimal.TryParse(values[0], out _))
            {
                return string.Join(":", values);
            }

            return string.Join(":", values.Skip(1));
        }
    }
}
