﻿using Bizio.Client.Desktop.Core;
using Bizio.Client.Desktop.Model.Companies;
using Bizio.Client.Desktop.Model.People;
using Bizio.Client.Desktop.Model.Perks;
using Bizio.Client.Desktop.Model.Projects;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Bizio.Client.Desktop.Converters
{
    public class GameDataConverter : BaseConverter
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var target = parameter as string;

            switch (target.ToUpper())
            {
                case "AGE":
                    return GetAge(value);

                case "PERSON":
                    return GetPerson(value);

                case "PROSPECT":
                    return GetProspect(value);

                case "COMPANY":
                    return GetCompany(value);

                case "DATEDIFF":
                    return DateDiff(value);

                case "TURNDIFF":
                    return TurnDiff(value);

                case "EMPLOYEE":
                    return GetEmployee(value);

                case "TENURE":
                    return GetTenure(value);

                case "PERKCOUNT":
                    return GetCompanyPerkCount(value);

                case "PROJECT":
                    return GetProject(value);

                case "PERK":
                    return GetPerk(value);

                case "COMPANYPERK":
                    return GetCompanyPerk(value);

                case "EMPLOYEE.ALLOCATION":
                    return GetEmployeeAllocation(value);
            }

            return null;
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }

        private IEnumerable<Allocation> GetEmployeeAllocation(object value)
        {
            var personId = (int)value;

            return Utilities.GameData.CompaniesData.Companies.FirstOrDefault(c => c.Employees.Any(e => e.PersonId == personId)).Allocations.Where(a => a.PersonId == personId);
        }

        private CompanyPerk GetCompanyPerk(object value)
        {
            var companyPerkId = (int)value;

            return Utilities.GameData.CompaniesData.Companies.FirstOrDefault(c => c.UserId.HasValue).Perks.FirstOrDefault(p => p.Id == companyPerkId);
        }

        private Perk GetPerk(object value)
        {
            var perkId = (int)value;

            return Utilities.StaticData.PerksData.Perks.FirstOrDefault(p => p.Id == perkId);
        }

        private Project GetProject(object value)
        {
            var projectId = (int)value;

            return Utilities.GameData.ProjectsData.Projects.FirstOrDefault(p => p.Id == projectId);
        }

        private Prospect GetProspect(object value)
        {
            return UserCompany.Prospects.FirstOrDefault(p => p.PersonId == (int)value);
        }

        private int GetCompanyPerkCount(object value)
        {
            return UserCompany.Perks.Count(p => p.PerkId == (int)value);
        }

        private double? TurnDiff(object value)
        {
            var date = (DateTime?)value;

            if (!date.HasValue)
            {
                return null;
            }

            var diff = date - Utilities.GameData.GameClockData.CurrentDate;

            return diff.Value.TotalDays / (7.0 * Utilities.GameData.GameClockData.WeeksPerTurn);
        }

        private string GetTenure(object value)
        {
            var employee = value as Employee;

            var person = Utilities.GameData.PeopleData.People.FirstOrDefault(p => p.Id == employee.PersonId);

            var startDate = person.WorkHistory.Single(wh => !wh.EndDate.HasValue).StartDate;

            var diff = DateTimeSpan.CompareDates(Utilities.GameData.GameClockData.CurrentDate, startDate);

            return $"{diff.Years}y {diff.Months}m";
        }

        private Employee GetEmployee(object value)
        {
            return Utilities.GameData.CompaniesData.Companies.SelectMany(c => c.Employees).FirstOrDefault(e => e.PersonId == (int)value);
        }

        private double? DateDiff(object value)
        {
            var date = (DateTime?)value;

            if (!date.HasValue)
            {
                return null;
            }

            var diff = date - Utilities.GameData.GameClockData.CurrentDate;

            return Math.Abs(diff.Value.TotalDays);
        }

        private Company GetCompany(object value)
        {
            return Utilities.GameData.CompaniesData.Companies.FirstOrDefault(c => c.Id == (int)value);
        }

        private Person GetPerson(object value)
        {
            return Utilities.GameData.PeopleData.People.FirstOrDefault(p => p.Id == (int)value);
        }

        private int GetAge(object value)
        {
            var date = (DateTime)value;

            var age = Utilities.GameData.GameClockData.CurrentDate.Year - date.Year;

            if (date > Utilities.GameData.GameClockData.CurrentDate.AddYears(-age))
            {
                age--;
            }

            return age;
        }

        private Company UserCompany => Utilities.GameData.CompaniesData.Companies.First(c => c.UserId.HasValue);
    }
}