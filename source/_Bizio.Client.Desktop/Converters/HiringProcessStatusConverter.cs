﻿using Bizio.Client.Desktop.Core;
using Bizio.Client.Desktop.Model.Game;
using System;
using System.Globalization;
using System.Windows.Data;

namespace Bizio.Client.Desktop.Converters
{
    public class HiringProcessStatusConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch ((HiringProcessStatus)value)
            {
                case HiringProcessStatus.None:
                    return string.Empty;

                case HiringProcessStatus.RecruitPending:
                    return Glyphs.RecruitPendingSymbol;

                case HiringProcessStatus.InterviewPending:
                    return Glyphs.RInterviewPendingSymbol;

                case HiringProcessStatus.OfferPending:
                    return Glyphs.OfferPendingSymbol;
            }

            throw new ArgumentOutOfRangeException(nameof(value));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new InvalidOperationException();
        }
    }
}