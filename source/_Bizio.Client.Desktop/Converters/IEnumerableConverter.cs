﻿using System;
using System.Collections;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace Bizio.Client.Desktop.Converters
{
    public class IEnumerableConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var enumerable = value as IEnumerable;

            var objectEnumerable = enumerable.Cast<object>();

            var parameterString = parameter as string;

            switch (parameterString.ToUpper())
            {
                case "COUNT":
                    return objectEnumerable.Count();
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}