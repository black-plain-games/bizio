﻿using System.Globalization;

namespace Bizio.Client.Desktop.Converters
{
    public class MultiBinding : System.Windows.Data.MultiBinding
    {
        public static CultureInfo GlobalCulture { get; set; }

        public MultiBinding()
            : base()
        {
            ConverterCulture = GlobalCulture;
        }
    }
}