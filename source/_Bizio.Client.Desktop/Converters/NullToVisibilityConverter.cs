﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Bizio.Client.Desktop.Converters
{
    public class NullToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var inverted = false;

            if (parameter != null &&
                $"{parameter}".ToUpper() == "INVERTED")
            {
                inverted = true;
            }

            if (value == null)
            {
                return inverted ? Visibility.Visible : Visibility.Collapsed;
            }

            return inverted ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}