﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Media;

namespace Bizio.Client.Desktop.Converters
{
    public class NumericComparisonToBrushConverter : BaseMultiConverter
    {
        public override object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values[0] == DependencyProperty.UnsetValue ||
                (values.Length == 2 && values[1] == DependencyProperty.UnsetValue))
            {
                return null;
            }

            var number = (decimal)System.Convert.ChangeType(values[0], typeof(decimal));

            var midpoint = 0m;

            if (values.Length > 1)
            {
                midpoint = (decimal)System.Convert.ChangeType(values[1], typeof(decimal));
            }

            if (number > midpoint)
            {
                return AboveValueBrush;
            }
            else if (number < midpoint)
            {
                return BelowValueBrush;
            }

            return AtValueBrush;
        }

        private static readonly SolidColorBrush AboveValueBrush = new SolidColorBrush(Colors.Green);

        private static readonly SolidColorBrush BelowValueBrush = new SolidColorBrush(Colors.Red);

        private static readonly SolidColorBrush AtValueBrush = new SolidColorBrush(Colors.Black);
    }
}