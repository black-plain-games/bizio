﻿using Bizio.Client.Desktop.Model.Perks;
using System;
using System.Globalization;

namespace Bizio.Client.Desktop.Converters
{
    public class PerkBenefitToStringConverter : PerkToStringConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var benefit = (PerkBenefit)value;

            string valueString;

            switch (benefit.ValueType)
            {
                case PerkValueType.Percentage:
                    valueString = string.Format(culture, "{0:P0}", benefit.Value);
                    break;
                case PerkValueType.Literal:
                    switch (benefit.Attribute)
                    {
                        case PerkBenefitAttribute.Money:
                            valueString = string.Format(culture, "{0:C0}", benefit.Value);
                            break;
                        default:
                            valueString = $"{benefit.Value}";

                            if (benefit.TargetType != PerkTargetType.Company)
                            {
                                valueString += " point";

                                if (benefit.Value != 1 && benefit.Value != -1)
                                {
                                    valueString += "s";
                                }
                            }
                            break;
                    }
                    break;
                default:
                    valueString = $"{benefit.Value}";
                    break;
            }

            var verb = benefit.Value > 0 ? "increased" : "reduced";

            var target = GetTargetType(benefit.TargetType);

            return $"{target} {benefit.Attribute} {verb} by {valueString}";
        }
    }

    public abstract class PerkToStringConverterBase : BaseConverter
    {
        protected string GetTargetType(PerkTargetType targetType)
        {
            switch (targetType)
            {
                default:
                case PerkTargetType.Company:
                    return $"{targetType}";
                case PerkTargetType.Employee:
                    return $"{targetType}s";
                case PerkTargetType.Person:
                    return "People's";
            }
        }
    }
}