﻿using Bizio.Client.Desktop.Model.Perks;
using System;
using System.Globalization;

namespace Bizio.Client.Desktop.Converters
{
    public class PerkConditionToStringConverter : PerkToStringConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var condition = (PerkCondition)value;

            var target = GetTargetType(condition.TargetType);

            string comparison;

            switch (condition.ComparisonType)
            {
                case PerkConditionComparisonType.Equal:
                    comparison = "be";
                    break;
                case PerkConditionComparisonType.NotEqual:
                    comparison = "not be";
                    break;
                case PerkConditionComparisonType.GreaterThan:
                    comparison = "be greater than";
                    break;
                case PerkConditionComparisonType.GreaterThanEqualTo:
                    comparison = "be at least";
                    break;
                case PerkConditionComparisonType.LessThan:
                    comparison = "be less than";
                    break;
                case PerkConditionComparisonType.LessThanEqualTo:
                    comparison = "be at most";
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(condition.ComparisonType));
            }

            string valueString;

            switch (condition.Attribute)
            {
                case PerkConditionAttribute.Money:
                    valueString = string.Format(culture, "{0:C0}", condition.Value);
                    break;
                default:
                    valueString = $"{condition.Value}";

                    if (condition.TargetType != PerkTargetType.Company &&
                        !condition.Attribute.ToString().EndsWith("Count"))
                    {
                        valueString += " point";

                        if (condition.Value != 1 && condition.Value != -1)
                        {
                            valueString += "s";
                        }
                    }
                    break;
            }

            return $"{target} {condition.Attribute} must {comparison} {valueString}";
        }
    }
}