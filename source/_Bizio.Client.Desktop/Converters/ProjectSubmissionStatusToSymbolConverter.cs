﻿using Bizio.Client.Desktop.Core;
using Bizio.Client.Desktop.Model.Projects;
using System;
using System.Globalization;
using System.Windows.Data;

namespace Bizio.Client.Desktop.Converters
{
    public class ProjectSubmissionStatusToSymbolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var submissionStatus = value.AsEnum<ProjectSubmissionStatus>();

            switch (submissionStatus)
            {
                case ProjectSubmissionStatus.InProgress:
                    return string.Empty;

                case ProjectSubmissionStatus.Submitting:
                    return Glyphs.SubmittingProjectSymbol;

                case ProjectSubmissionStatus.Cancelling:
                    return Glyphs.CancelSymbol;

                case ProjectSubmissionStatus.RequestingExtension:
                    return Glyphs.RequestExtensionSymbol;
            }

            throw new ArgumentException(nameof(submissionStatus));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}