﻿using Bizio.Client.Desktop.Core;
using Bizio.Client.Desktop.Model.Game.ActionData;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Bizio.Client.Desktop.Converters
{
    public class PurchasePerkCountConverter : BaseMultiConverter
    {
        public override object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null)
            {
                return 0;
            }

            if (values.Length < 2)
            {
                return 0;
            }

            if (!(values[0] is IEnumerable<IActionData> turnActions))
            {
                return 0;
            }

            var perkId = (int)values[1];

            var purchaseCount = turnActions
                .Where(ta => ta.ActionTypeId == (byte)ActionType.PurchasePerk)
                .Cast<PurchasePerkActionData>()
                .Count(ppad => ppad.Data.PerkId == perkId);


            var output = "";

            while (output.Length < purchaseCount)
            {
                output += Glyphs.IncreaseArrowSymbol;
            }

            return output;
        }
    }
}