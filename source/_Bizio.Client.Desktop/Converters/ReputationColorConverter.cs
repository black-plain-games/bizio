﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace Bizio.Client.Desktop.Converters
{
    public class ReputationColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!double.TryParse($"{value}", out var reputation))
            {
                return double.Epsilon;
            }

            return new Color
            {
                A = 255,
                R = 128,
                G = (byte)(reputation * 255.0),
                B = 0
            };
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}