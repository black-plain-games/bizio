using Bizio.Client.Desktop.Model.Projects;
using System;
using System.Globalization;

namespace Bizio.Client.Desktop.Converters
{
    public class ReputationWidthConverter : BaseMultiConverter
    {
        public override object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Length != 2)
            {
                return double.Epsilon;
            }

            if (!double.TryParse($"{values[0]}", out var targetWidth))
            {
                return double.Epsilon;
            }

            if (!double.TryParse($"{values[1]}", out var coefficient))
            {
                if (!Enum.TryParse<StarCategory>($"{values[1]}", out var starCategory))
                {
                    return double.Epsilon;
                }

                coefficient = ((double)(starCategory - 1)) / 5.0;
            }

            return targetWidth * coefficient;
        }
    }
}