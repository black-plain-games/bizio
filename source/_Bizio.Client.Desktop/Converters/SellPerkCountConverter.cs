﻿using Bizio.Client.Desktop.Core;
using Bizio.Client.Desktop.Model.Companies;
using Bizio.Client.Desktop.Model.Game.ActionData;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Bizio.Client.Desktop.Converters
{
    public class SellPerkCountConverter : BaseMultiConverter
    {
        public override object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null)
            {
                return 0;
            }

            if (values.Length < 2)
            {
                return 0;
            }

            if (!(values[0] is IEnumerable<IActionData> turnActions))
            {
                return 0;
            }

            var perkId = (int)values[2];

            var sellingCount = 0;

            if (values[1] is IEnumerable<CompanyPerk> companyPerks)
            {
                companyPerks = companyPerks.Where(cp => cp.PerkId == perkId);

                sellingCount = turnActions
                    .Where(ta => ta.ActionTypeId == (byte)ActionType.SellPerk)
                    .Cast<SellPerkActionData>()
                    .Count(ppad => companyPerks.Any(cp => cp.Id == ppad.Data.CompanyPerkId));
            }

            var output = "";

            while (output.Length < sellingCount)
            {
                output += Glyphs.DecreaseArrowSymbol;
            }

            return output;
        }
    }
}