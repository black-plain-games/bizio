﻿using Bizio.Client.Desktop.Core;
using System;
using System.Globalization;
using System.Linq;

namespace Bizio.Client.Desktop.Converters
{
    public class StaticDataConverter : BaseConverter
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var target = parameter as string;

            if (!int.TryParse($"{value}", out var id))
            {
                return null;
            }

            switch (target.ToUpper())
            {
                case "SKILLDEFINITION":
                    return Utilities.StaticData.PeopleData.SkillDefinitions.FirstOrDefault(sd => sd.Id == id);

                case "ACTION":
                    return Utilities.StaticData.ActionsData.Actions.FirstOrDefault(a => a.Id == id);

                case "GENDER":
                    return Utilities.StaticData.PeopleData.Genders.SafeGet((byte)id);

                case "PERSONALITY":
                    return Utilities.StaticData.PeopleData.Personalities.FirstOrDefault(p => p.Id == id);

                case "TRANSACTIONTYPE":
                    return Utilities.StaticData.CompaniesData.TransactionTypes.SafeGet((byte)id);

                case "PROJECTDEFINITION":
                    return Utilities.StaticData.ProjectsData.ProjectDefinitions.FirstOrDefault(pd => pd.Id == id);

                case "ROLE":
                    return Utilities.StaticData.ProjectsData.ProjectRoles.SafeGet((byte)id);

                case "STARCATEGORY":
                    return Utilities.StaticData.ProjectsData.StarCategories.SafeGet((byte)id);

                case "PROFESSION":
                    return Utilities.StaticData.PeopleData.Professions.SafeGet(Utilities.GameData.IndustryId).FirstOrDefault(p => p.Id == id);
            }

            return null;
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var target = parameter as string;

            switch (target.ToUpper())
            {
                case "SKILLDEFINITION":
                    var skillDefinition = Utilities.StaticData.PeopleData.SkillDefinitions.FirstOrDefault(sd => sd.Name == $"{value}");

                    if (skillDefinition != null)
                    {
                        return skillDefinition.Id;
                    }
                    break;
                case "ACTION":
                    var action = Utilities.StaticData.ActionsData.Actions.FirstOrDefault(a => a.Name == $"{value}");

                    if (action != null)
                    {
                        return action.Id;
                    }
                    break;
            }

            return null;
        }
    }
}