﻿using Bizio.Client.Desktop.Model.Companies;
using Bizio.Client.Desktop.Model.Game.ActionData;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Bizio.Client.Desktop.Converters
{
    public class TurnActionCountConverter : BaseMultiConverter
    {
        public override object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null || values.Length != 2 || parameter == null)
            {
                return null;
            }

            if (!(values[0] is IEnumerable<IActionData> turnActions))
            {
                return null;
            }

            if (!(values[1] is IEnumerable<CompanyAction> companyActions))
            {
                return null;
            }

            var actionTypeId = (byte)Enum.Parse(typeof(ActionType), $"{parameter}", true);

            var turnActionCount = turnActions.Count(ta => ta.ActionTypeId == actionTypeId);

            var companyActionCount = companyActions.FirstOrDefault(ca => ca.ActionId == actionTypeId).Count;

            return companyActionCount - turnActionCount;
        }
    }
}