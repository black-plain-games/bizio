﻿namespace Bizio.Client.Desktop.Core
{
    public class ChartGroup
    {
        public object Category { get; set; }

        public object Value { get; set; }
    }
}