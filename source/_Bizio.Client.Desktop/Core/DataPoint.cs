﻿namespace Bizio.Client.Desktop.Core
{
    public class DataPoint<TKey, TValue>
    {
        public TKey Key { get; set; }

        public TValue Value { get; set; }

        public DataPoint(TKey key, TValue value)
        {
            Key = key;

            Value = value;
        }
    }
}