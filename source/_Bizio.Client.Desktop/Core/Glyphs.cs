﻿namespace Bizio.Client.Desktop.Core
{
    public static class Glyphs
    {
        public const string DollarSign = "$";

        public const string EuroSign = "€";

        public const string PoundSign = "£";

        public const string YenSign = "¥";

        public const string FrancSign = "₣";

        public const string RecruitPendingSymbol = "📋";

        public const string RInterviewPendingSymbol = "📓";

        public const string OfferPendingSymbol = "💵";

        public const string TextTextBoxSymbol = "🖊";

        public const string NumberTextBoxSymbol = "#";

        public const string MoneyTextBoxSymbol = "💰";

        public const string DateTextBoxSymbol = "📅";

        public const string ArrowDown = "⏷";

        public const string ArrowRight = "⏵";

        public const string ArrowLeft = "❮";

        public const string PeopleSymbol = "🌎";

        public const string UserCompanySymbol = "🏢";

        public const string CompaniesSymbol = "🏙";

        public const string ProjectsSymbol = "🏗";

        public const string StoreSymbol = "🛒";

        public const string MailboxSymbol = "📪";

        public const string EditSymbol = "🖊";

        public const string SaveSymbol = "🖫";

        public const string CancelSymbol = "🗙";

        public const string IncreaseArrowSymbol = "▲";

        public const string DecreaseArrowSymbol = "▼";

        public const string FiringSymbol = "⮿";

        public const string FounderSymbol = "★";

        public const string FilledStarSymbol = "★";

        public const string EmptyStarSymbol = "☆";

        public const string RequestingProjectSymbol = "📥";

        public const string RequestExtensionSymbol = "🕙";

        public const string SubmittingProjectSymbol = "📤";

        public const string CancelRequestExtensionSymbol = "⮿";

        public const string EnvelopeSymbol = "🖂";

        public const string DeletedMessageSymbol = "🗙";

        public const string ReadMessageSymbol = "";

        public const string UnReadMessageSymbol = "!";

        public const string LockSymbol = "🔒";

        public const string PhoneSymbol = "📱";

        public const string DotSymbol = "◦";

        public const string ServerUpSymbol = "▲";

        public const string ServerDownSymbol = "▼";

        public const string KeySymbol = "🔑";

        public const string CheckMark = "✓";

        public const string IndeterminateMark = "•";

        public const string DecreaseSymbol = "-";

        public const string IncreaseSymbol = "+";
    }
}