﻿namespace Bizio.Client.Desktop.Core
{
    public interface ILog
    {
        void LogMessage(string message);

        void LogMessage(string format, params object[] args);
    }
}