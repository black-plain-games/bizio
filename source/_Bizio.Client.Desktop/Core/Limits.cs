﻿using Bizio.Client.Desktop.Model.Core;
using System;

namespace Bizio.Client.Desktop.Core
{
    public static class Limits
    {
        public static readonly decimal MaximumMoneyAmount = 9999999.99m;

        public static readonly Range<int> CompanyNameLength = new Range<int>(3, 64);

        public static readonly Range<int> FirstNameLength = new Range<int>(2, 64);

        public static readonly Range<int> LastNameLength = new Range<int>(2, 64);

        public static readonly Range<byte> WeeksPerTurn = new Range<byte>(1, 1);

        public static readonly Range<byte> NumberOfCompanies = new Range<byte>(0, 10);

        public static readonly Range<DateTime> StartDate = new Range<DateTime>(new DateTime(1970, 1, 1), DateTime.Today.AddYears(100));

        public static readonly Range<byte> Age = new Range<byte>(18, 65);
    }
}
