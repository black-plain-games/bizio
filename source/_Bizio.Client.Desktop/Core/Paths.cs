﻿using System;

namespace Bizio.Client.Desktop.Core
{
    public static class Paths
    {
        public static readonly string LanguagesPath = "languages";

        public static readonly string SettingsPath = "settings";

        public static readonly string SavedGamesPath = "saves";

        public static readonly string SavedGamesUpdatesPath = "saves_updates";

        public static readonly string NotificationsPath = "notifications";

        public static readonly string Exceptions = "exceptions";
    }
}
