﻿using System.Windows;
using System.Windows.Controls;

namespace Bizio.Client.Desktop.Core
{
    public static class TextBoxExtensions
    {
        public static DependencyProperty ValueProperty = DependencyProperty.RegisterAttached("Value", typeof(string), typeof(TextBoxExtensions));

        public static DependencyProperty MaskProperty = DependencyProperty.RegisterAttached("Mask", typeof(string), typeof(TextBoxExtensions));

        public static DependencyProperty IsMaskingProperty = DependencyProperty.RegisterAttached("IsMask", typeof(bool), typeof(TextBoxExtensions));

        public static void SetValue(this FrameworkElement element, string value)
        {
            element.SetValue(ValueProperty, value);
        }

        public static string GetValue(this TextBox textBox)
        {
            return (string)textBox.GetValue(ValueProperty);
        }

        public static void SetMask(this FrameworkElement element, string value)
        {
            element.SetValue(MaskProperty, value);
        }

        public static string GetMask(this TextBox textBox)
        {
            return (string)textBox.GetValue(MaskProperty);
        }

        public static void SetIsMasking(this FrameworkElement element, bool value)
        {
            element.SetValue(IsMaskingProperty, value);
        }

        public static bool GetIsMaskingProperty(this TextBox textBox)
        {
            return (bool)textBox.GetValue(IsMaskingProperty);
        }

        public static void OnTextChanged(TextBox textBox)
        {
            if (textBox.GetIsMaskingProperty())
            {
                return;
            }

            textBox.SetValue(textBox.Text);
        }

        public static void OnGotFocus(TextBox textBox)
        {
            textBox.SetIsMasking(true);

            textBox.Text = textBox.GetValue();

            textBox.SetIsMasking(false);
        }

        public static void OnLostFocus(TextBox textBox)
        {
            var mask = textBox.GetMask();

            if (string.IsNullOrWhiteSpace(mask))
            {
                return;
            }

            textBox.SetIsMasking(true);

            var maskString = "";

            while (maskString.Length < textBox.Text.Length)
            {
                maskString += mask;
            }

            textBox.SetValue(textBox.Text);

            textBox.Text = maskString;

            textBox.SetIsMasking(false);
        }
    }
}
