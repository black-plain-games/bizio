﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;

namespace Bizio.Client.Desktop.Core
{
    public static class UiText
    {
        #region Tooltips

        public static string PeoplePanelTooltip => Get();

        public static string UserCompanyPanelTooltip => Get();

        public static string CompaniesPanelTooltip => Get();

        public static string ProjectsPanelTooltip => Get();

        public static string StorePanelTooltip => Get();

        public static string MailboxPanelTooltip => Get();

        public static string SkillsTooltip => Get();

        public static string EmploymentHistoryTooltip => Get();

        public static string MakeOfferTooltipFormat => Get();

        public static string CancelMakeOfferTooltip => Get();

        public static string RecruitTooltipFormat => Get();

        public static string CancelRecruitTooltip => Get();

        public static string InterviewTooltipFormat => Get();

        public static string CancelInterviewTooltip => Get();

        public static string FireEmployeeTooltipFormat => Get();

        public static string CancelFireEmployeeTooltip => Get();

        public static string EmployeeAllocationsTooltip => Get();

        #endregion

        #region Text

        public static string Authorizing => Get();

        public static string Login => Get();

        public static string Register => Get();

        public static string Quit => Get();

        public static string NewGame => Get();

        public static string LoadGame => Get();

        public static string Notifications => Get();

        public static string Settings => Get();

        public static string Logout => Get();

        public static string Username => Get();

        public static string Password => Get();

        public static string Cancel => Get();

        public static string ForgotPassword => Get();

        public static string Submit => Get();

        public static string ForgotYourPassword => Get();

        public static string ForgotYourPasswordDetails => Get();

        public static string EmailAddress => Get();

        public static string SendToken => Get();

        public static string ConfirmPassword => Get();

        public static string Name => Get();

        public static string PhoneNumber => Get();

        public static string ChangePassword => Get();

        public static string ChangePasswordDetails => Get();

        public static string Token => Get();

        public static string ConfirmQuit => Get();

        public static string ConfirmQuitDetails => Get();

        public static string Yes => Get();

        public static string No => Get();

        public static string Date => Get();

        public static string Subject => Get();

        public static string Back => Get();

        public static string UiSize => Get();

        public static string Small => Get();

        public static string Medium => Get();

        public static string Large => Get();

        public static string CurrencySymbol => Get();

        public static string Culture => Get();

        public static string Save => Get();

        public static string InProgress => Get();

        public static string Complete => Get();

        public static string Created => Get();

        public static string Company => Get();

        public static string Turn => Get();

        public static string Load => Get();

        public static string Founder => Get();

        public static string Industry => Get();

        public static string TurnSpeed => Get();

        public static string WeeksPerTurn => Get();

        public static string GameSettings => Get();

        public static string StartDate => Get();

        public static string CompanyName => Get();

        public static string NumberOfCompanies => Get();

        public static string Difficulty => Get();

        public static string InitialFunds => Get();

        public static string Continue => Get();

        public static string FounderSettings => Get();

        public static string FirstName => Get();

        public static string LastName => Get();

        public static string Gender => Get();

        public static string Birthday => Get();

        public static string RemainingSkillPoints => Get();

        public static string ConfirmNewGame => Get();

        public static string ConfirmNewGameDetails => Get();

        public static string Employees => Get();

        public static string Fire => Get();

        public static string DontFire => Get();

        public static string EditSalary => Get();

        public static string Age => Get();

        public static string Tenure => Get();

        public static string Finances => Get();

        public static string AccountBalance => Get();

        public static string Income => Get();

        public static string Expenses => Get();

        public static string Transactions => Get();

        public static string Amount => Get();

        public static string Payroll => Get();

        public static string HireEmployee => Get();

        public static string FireEmployee => Get();

        public static string PurchasePerk => Get();

        public static string SellPerk => Get();

        public static string Perk => Get();

        public static string Project => Get();

        public static string Maintenance => Get();

        public static string Skills => Get();

        public static string MandatorySkills => Get();

        public static string OptionalSkills => Get();

        public static string Total => Get();

        public static string Count => Get();

        public static string Best => Get();

        public static string Unemployed => Get();

        public static string MakeOffer => Get();

        public static string CancelOffer => Get();

        public static string Recruit => Get();

        public static string CancelRecruit => Get();

        public static string Prospective => Get();

        public static string Interview => Get();

        public static string CancelInterview => Get();

        public static string Personality => Get();

        public static string Profession => Get();

        public static string OfferValue => Get();

        public static string Salary => Get();

        public static string EmploymentHistory => Get();

        public static string EndDate => Get();

        public static string Happiness => Get();

        public static string Allocations => Get();

        public static string Percent => Get();

        public static string Role => Get();

        public static string Add => Get();

        public static string Cost => Get();

        public static string Upkeep => Get();

        public static string Description => Get();

        public static string Conditions => Get();

        public static string Benefits => Get();

        public static string NextPaymentDate => Get();

        public static string Sell => Get();

        public static string Value => Get();

        public static string Deadline => Get();

        public static string Reputation => Get();

        public static string Requirements => Get();

        public static string Companies => Get();

        public static string Money => Get();

        public static string CompletedProjects => Get();

        public static string Bankrupt => Get();

        public static string Actions => Get();

        public static string Action => Get();

        public static string Info => Get();

        public static string Old => Get();

        public static string New => Get();

        public static string ProjectRequirements => Get();

        public static string EmployeeSkills => Get();

        public static string Management => Get();

        public static string Max => Get();

        public static string Active => Get();

        public static string MaxCount => Get();

        public static string IsActive => Get();

        public static string StartAll => Get();

        public static string StopAll => Get();

        public static string Start => Get();

        public static string Stop => Get();

        public static string Messages => Get();

        public static string CopyCallStack => Get();

        public static string GameInfo => Get();

        public static string NextTurn => Get();

        public static string Inbox => Get();

        public static string From => Get();

        public static string Delete => Get();

        public static string UndoDelete => Get();

        public static string Read => Get();

        public static string AvailableProjects => Get();

        public static string Due => Get();

        public static string RequestProject => Get();

        public static string CancelRequest => Get();

        public static string ProjectsInProgress => Get();

        public static string PercentComplete => Get();

        public static string CancelProject => Get();

        public static string UndoCancellation => Get();

        public static string SubmitProject => Get();

        public static string UndoSubmission => Get();

        public static string Perks => Get();

        public static string Owned => Get();

        public static string PurchasingSelling => Get();

        public static string CancelPurchase => Get();

        public static string Purchase => Get();

        public static string Overview => Get();

        public static string Employee => Get();

        public static string Capacity => Get();

        public static string Quantity => Get();

        #endregion

        #region Initialization

        public static string Language { get; set; }

        public static readonly string DefaultLanguage = "en";

        public static IEnumerable<string> Languages => _languages?.Keys;

        public static void LoadLanguages()
        {
            using (var reader = new StreamReader(Paths.LanguagesPath))
            {
                _languages = JsonConvert.DeserializeObject<IDictionary<string, IDictionary<string, string>>>(reader.ReadToEnd());
            }
        }

        public static void SetLanguages(IDictionary<string, IDictionary<string, string>> languages)
        {
            _languages = languages;
        }

        private static string Get([CallerMemberName]string key = "")
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                Debug.WriteLine("Unknown language key");

                return "NOKEY";
            }

            if (string.IsNullOrWhiteSpace(Language))
            {
                Debug.WriteLine("Using default langauge since none was selected");

                Language = DefaultLanguage;
            }

            var language = Language;

            if (!_languages.ContainsKey(language))
            {
                Debug.WriteLine($"Using default language because '{language}' does not exist");

                language = DefaultLanguage;

                if (!_languages.ContainsKey(language))
                {
                    Debug.WriteLine("Default language missing!");

                    return $"NOLANG:{key}";
                }
            }

            if (!_languages[language].ContainsKey(key))
            {
                Debug.WriteLine($"Language '{language}' missing key '{key}'");

                return $"NOTEXT:{key}";
            }

            return _languages[language][key];
        }

        private static IDictionary<string, IDictionary<string, string>> _languages = new Dictionary<string, IDictionary<string, string>>();

        #endregion
    }
}