﻿using Bizio.Client.Desktop.Core;
using Bizio.Client.Desktop.Model.Companies;
using Bizio.Client.Desktop.Model.Core;
using Bizio.Client.Desktop.Model.Game;
using Bizio.Client.Desktop.Model.Game.ActionData;
using Bizio.Client.Desktop.Services;
using Bizio.Client.Desktop.ViewModels.Popups;
using Bizio.Client.Desktop.Views.Popups;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;

namespace Bizio.Client.Desktop.Core
{
    public static class Utilities
    {
        private const int GWL_STYLE = -16; //WPF's Message code for Title Bar's Style 
        private const int WS_SYSMENU = 0x80000; //WPF's Message code for System Menu

        private static bool _isWindowContextMenuDisabled = false;

        public static StaticData StaticData { get; set; }

        public static GameData GameData { get; set; }

        public static bool CanCompanyPerformActionType(Company company, ActionType actionType, IEnumerable<IActionData> turnActions)
        {
            var actionTypeId = (byte)actionType;

            var userCompanyAction = company.Actions.FirstOrDefault(a => a.ActionId == actionTypeId);

            return userCompanyAction.Count > turnActions.Count(ta => ta.ActionTypeId == actionTypeId);
        }

        public static bool CanCompanyPerformActionType<TActionDataType>(Company company, ActionType actionType, IEnumerable<IActionData> turnActions, Predicate<TActionDataType> shouldExclude)
            where TActionDataType : class, IActionData
        {
            var actionTypeId = (byte)actionType;

            var userCompanyAction = company.Actions.FirstOrDefault(a => a.ActionId == actionTypeId);

            var tas = turnActions.Where(ta => ta is TActionDataType && !shouldExclude(ta as TActionDataType));

            return userCompanyAction.Count > tas.Count();
        }

        public static void DisableWindowContextMenu(Window window)
        {
            if (_isWindowContextMenuDisabled)
            {
                return;
            }

            var hwnd = new WindowInteropHelper(window).Handle;

            NativeMethods.SetWindowLong(hwnd, GWL_STYLE, NativeMethods.GetWindowLong(hwnd, GWL_STYLE) & ~WS_SYSMENU);

            _isWindowContextMenuDisabled = true;
        }

        public static void ResizeListViewColumns(ListView listView)
        {
            if (listView == null)
            {
                return;
            }

            if (!(listView.View is GridView gridView))
            {
                return;
            }

            foreach (var column in gridView.Columns)
            {
                column.Width = 0;

                column.Width = double.NaN;
            }
        }

        public static TValue SafeGet<TKey, TValue>(this IDictionary<TKey, TValue> source, TKey key)
        {
            if (source.ContainsKey(key))
            {
                return source[key];
            }

            return default;
        }

        public static TValue SafeGet<TKey, TValue>(this IReadOnlyDictionary<TKey, TValue> source, TKey key)
        {
            if (source.ContainsKey(key))
            {
                return source[key];
            }

            return default;
        }

        public static int Clamp(int value, int minimum, int maximum)
        {
            if (value < minimum)
            {
                return minimum;
            }
            else if (value > maximum)
            {
                return maximum;
            }

            return value;
        }

        public static TEnum AsEnum<TEnum>(this object value) => (TEnum)Enum.Parse(typeof(TEnum), $"{value}");

        public static string Join(this IEnumerable<string> collection, string delimiter)
        {
            if (collection == null || !collection.Any())
            {
                return string.Empty;
            }

            return collection.Aggregate((x, y) => $"{x}{delimiter}{y}");
        }

        public static IEnumerable<TResult> ToList<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, TResult> selector)
        {
            return source.Select(selector).ToList();
        }

        public static IEnumerable<DataPoint<TKey1, IEnumerable<DataPoint<TKey2, TValue>>>> CreateStackedData<TKey1, TKey2, TValue, TElement>(this IEnumerable<TElement> collection, Func<TElement, TKey1> key1Selector, Func<TElement, TKey2> key2Selector, Func<TElement, TValue> valueSelector, Func<TValue, TValue, TValue> accumulator)
        {
            var output = new List<DataPoint<TKey1, IEnumerable<DataPoint<TKey2, TValue>>>>();

            foreach (var element in collection)
            {
                var key1 = key1Selector(element);

                var data = output.FirstOrDefault(tcd => tcd.Key.Equals(key1));

                if (data == null)
                {
                    output.Add(data = new DataPoint<TKey1, IEnumerable<DataPoint<TKey2, TValue>>>(key1, Enum.GetValues(typeof(TKey2)).Cast<TKey2>().Select(k2 => new DataPoint<TKey2, TValue>(k2, default)).ToList()));
                }

                var key2 = key2Selector(element);

                var group = data.Value.First(a => a.Key.Equals(key2));

                var value = valueSelector(element);

                group.Value = accumulator(group.Value, value);
            }

            return output;
        }

        public static void Remove<T>(this ICollection<T> source, Predicate<T> predicate)
        {
            var toRemove = source.Where(x => predicate(x)).ToList();

            foreach (var element in toRemove)
            {
                source.Remove(element);
            }
        }

        public static MessagePopupViewModel CreateMessagePopup(IViewStateService viewStateService, string title, string message, string dismissLabel)
        {
            return CreateMessagePopup(new MessagePopupSetup
            {
                ViewStateService = viewStateService,
                Title = title,
                Message = message,
                DismissLabel = dismissLabel
            });
        }

        public static MessagePopupViewModel CreateMessagePopup(IViewStateService viewStateService, string title, string message, string dismissLabel, EventHandler dismissedHandler)
        {
            return CreateMessagePopup(new MessagePopupSetup
            {
                ViewStateService = viewStateService,
                Title = title,
                Message = message,
                DismissLabel = dismissLabel,
                DismissedHandler = dismissedHandler
            });
        }

        public static MessagePopupViewModel CreateMessagePopup(IViewStateService viewStateService, string title, string message, string dismissLabel, Action dismissedHandler)
        {
            return CreateMessagePopup(new MessagePopupSetup
            {
                ViewStateService = viewStateService,
                Title = title,
                Message = message,
                DismissLabel = dismissLabel,
                DismissedHandler = (s, e) => dismissedHandler()
            });
        }

        public static MessagePopupViewModel CreateMessagePopup(MessagePopupSetup setup)
        {
            var viewModel = new MessagePopupViewModel(setup.ViewStateService)
            {
                Title = setup.Title,
                Message = setup.Message
            };

            if (setup.DismissedHandler != null)
            {
                viewModel.Dismissed += setup.DismissedHandler;
            }

            viewModel.AddOption(setup.DismissLabel);

            setup.ViewStateService.PushPopup<MessagePopupView>(viewModel, setup.MaxWidth, setup.MaxHeight);

            return viewModel;
        }

        public static T Get<T>(this IDictionary<string, object> data, string key)
        {
            return (T)Convert.ChangeType(data[key], typeof(T));
        }

        public static bool Between(this byte value, Range<byte> range) => value.Between(range.Minimum, range.Maximum);

        public static bool Between(this byte value, byte minimum, byte maximum)
        {
            return value >= minimum && value <= maximum;
        }

        public static bool Between(this short value, Range<short> range) => value.Between(range.Minimum, range.Maximum);

        public static bool Between(this short value, short minimum, short maximum)
        {
            return value >= minimum && value <= maximum;
        }

        public static bool Between(this int value, Range<int> range) => value.Between(range.Minimum, range.Maximum);

        public static bool Between(this int value, int minimum, int maximum)
        {
            return value >= minimum && value <= maximum;
        }

        public static bool Between(this decimal value, Range<decimal> range) => value.Between(range.Minimum, range.Maximum);

        public static bool Between(this decimal value, decimal minimum, decimal maximum)
        {
            return value >= minimum && value <= maximum;
        }

        public static bool Between(this DateTime value, Range<DateTime> range) => value.Between(range.Minimum, range.Maximum);

        public static bool Between(this DateTime value, DateTime minimum, DateTime maximum)
        {
            return value >= minimum && value <= maximum;
        }

        public static bool Between(this DateTimeOffset value, Range<DateTimeOffset> range) => value.Between(range.Minimum, range.Maximum);

        public static bool Between(this DateTimeOffset value, DateTimeOffset minimum, DateTimeOffset maximum)
        {
            return value >= minimum && value <= maximum;
        }

        public static bool ShouldShowActionTypeId(byte actionTypeId)
        {
            switch ((ActionType)actionTypeId)
            {
                case ActionType.ChangeMessageStatus:
                case ActionType.ToggleCompanyAction:
                case ActionType.None:
                    return false;

                default:
                    return true;
            }
        }
    }

    public class MessagePopupSetup
    {
        public IViewStateService ViewStateService { get; set; }

        public string Title { get; set; }

        public string Message { get; set; }

        public string DismissLabel { get; set; }

        public EventHandler DismissedHandler { get; set; }

        public double MaxWidth { get; set; } = double.PositiveInfinity;

        public double MaxHeight { get; set; } = double.PositiveInfinity;
    }
}