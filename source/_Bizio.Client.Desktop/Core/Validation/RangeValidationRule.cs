﻿using System;
using System.Globalization;
using System.Windows.Controls;

namespace Bizio.Client.Desktop.Core.Validation
{
    public class RangeValidationRule : ValidationRule
    {
        public double? MinimumValue { get; set; }

        public bool IsMinimumInclusive { get; set; } = true;

        public double? MaximumValue { get; set; }

        public bool IsMaximumInclusive { get; set; } = true;

        public double? Step { get; set; }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (!double.TryParse($"{value}", out var number))
            {
                return new ValidationResult(false, "Value must be a number");
            }

            if (MinimumValue.HasValue &&
                ((IsMinimumInclusive && number < MinimumValue) ||
                (!IsMinimumInclusive && number <= MinimumValue)))
            {
                return new ValidationResult(false, $"Value must be greater than {MinimumValue}");
            }

            if (MinimumValue.HasValue &&
                ((IsMaximumInclusive && number > MaximumValue) ||
                (!IsMaximumInclusive && number >= MaximumValue)))
            {
                return new ValidationResult(false, $"Value must be less than {MaximumValue}");
            }

            if (Step.HasValue && Math.IEEERemainder(number, Step.Value) != 0)
            {
                return new ValidationResult(false, $"Value must be a multiple of {Step}");
            }

            return new ValidationResult(true, null);
        }
    }
}
