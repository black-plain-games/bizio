﻿using Bizio.Client.Desktop.Model.Core;
using BlackPlain.Wpf.Core;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Bizio.Client.Desktop.Core.Validation
{
    public abstract class ValidationBase : NotifyOnPropertyChanged
    {
        public bool HasErrors
        {
            get
            {
                return GetField<bool>();
            }
            private set
            {
                SetField(value);
            }
        }

        public IEnumerable<string> Errors => _validationErrors.SelectMany(e => e.Value);

        protected static IEnumerable<string> NoErrors = Enumerable.Empty<string>();

        protected void CancelOnHasErrors<TArgs>(object sender, CancelEventArgs<TArgs> args)
        {
            if (HasErrors)
            {
                args.Cancel = true;
            }
        }

        protected ValidationBase()
        {
            PropertyChanged += OnValidate;

            MarkAsUnvalidated(nameof(HasErrors));

            MarkAsUnvalidated(nameof(Errors));
        }

        protected virtual IEnumerable<string> Validate(string propertyName) => Enumerable.Empty<string>();

        protected void MarkAsUnvalidated(string propertyName)
        {
            if (!_unvalidatedProperties.Contains(propertyName))
            {
                _unvalidatedProperties.Add(propertyName);
            }
        }

        private void OnValidate(object sender, PropertyChangedEventArgs e)
        {
            if (_unvalidatedProperties.Contains(e.PropertyName))
            {
                return;
            }

            var errors = Validate(e.PropertyName);

            if (!_validationErrors.ContainsKey(e.PropertyName))
            {
                _validationErrors.Add(e.PropertyName, errors);
            }
            else
            {
                _validationErrors[e.PropertyName] = errors;
            }

            HasErrors = _validationErrors.Values.Any(v => v.Any());

            OnPropertyChanged(nameof(Errors));
        }

        protected IDictionary<string, IEnumerable<string>> _validationErrors = new Dictionary<string, IEnumerable<string>>();

        private readonly ICollection<string> _unvalidatedProperties = new List<string>();
    }
}