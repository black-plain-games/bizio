﻿using System.Windows;
using System.Windows.Controls;

namespace Bizio.Client.Desktop.Core
{
    public static class WebBrowserExtensions
    {
        public static readonly DependencyProperty HtmlProperty = DependencyProperty.RegisterAttached("Html", typeof(string), typeof(WebBrowserExtensions), new FrameworkPropertyMetadata(OnHtmlChanged));

        [AttachedPropertyBrowsableForType(typeof(WebBrowser))]
        public static string GetHtml(WebBrowser webBrowser)
        {
            return (string)webBrowser.GetValue(HtmlProperty);
        }

        public static void SetHtml(WebBrowser webBrowser, string value)
        {
            webBrowser.SetValue(HtmlProperty, value);
        }

        public static void OnHtmlChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is WebBrowser webBrowser)
            {
                var target = (string)e.NewValue ?? "<div></div>";

                webBrowser.NavigateToString(target);
            }
        }
    }
}