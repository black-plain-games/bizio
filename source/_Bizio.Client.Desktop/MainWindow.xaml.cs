﻿using Bizio.Client.Desktop.ViewModels;
using Bizio.Client.Desktop.Views;
using BlackPlain.DI;
using BlackPlain.Wpf.Core;
using System;
using System.Windows;
using System.Windows.Input;

namespace Bizio.Client.Desktop
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            var tb = FindResource("DefaultCursor") as FrameworkElement;

            Mouse.OverrideCursor = tb.Cursor;
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);

            UIManager.SetUI();

            _hub.SetResolution<Window>(this);

            _hub.CreateResolverBundle<BizioClientBundle>();

            _hub.CreateResolverBundle<ViewBundle>();

            _hub.CreateResolverBundle<ViewModelBundle>();

            try
            {
                DataContext = _hub.Resolve<IViewStateRoot>();
            }
            catch(Exception err)
            {

            }
        }

        private readonly IHub _hub = new Hub();
    }
}
