﻿using Bizio.Server.Model.Actions;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Client.Desktop.Model.Actions
{
    public class Action
    {
        public byte Id { get; }

        public string Name { get; }

        public short MaxCount { get; }

        public IReadOnlyDictionary<byte, decimal> Requirements { get; }

        public Action(ActionDto dataDto)
        {
            Id = dataDto.Id;

            Name = dataDto.Name;

            MaxCount = dataDto.MaxCount;

            Requirements = dataDto.Requirements.ToDictionary(r => r.Id, r => r.Value);
        }
    }
}