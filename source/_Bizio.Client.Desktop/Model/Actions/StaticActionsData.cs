﻿using Bizio.Server.Model.Actions;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Client.Desktop.Model.Actions
{
    public class StaticActionsData
    {
        public IEnumerable<Action> Actions { get; }

        public StaticActionsData(StaticActionsDataDto dataDto)
        {
            Actions = dataDto.Actions.Select(a => new Action(a));
        }
    }
}