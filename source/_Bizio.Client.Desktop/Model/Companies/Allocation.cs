﻿using Bizio.Server.Model.Projects;
using BlackPlain.Wpf.Core;

namespace Bizio.Client.Desktop.Model.Companies
{
    public class Allocation : NotifyOnPropertyChanged
    {
        public int PersonId { get; }

        public int ProjectId { get; }

        public byte Percent
        {
            get
            {
                return GetField<byte>();
            }
            set
            {
                SetField(value);
            }
        }

        public byte RoleId
        {
            get
            {
                return GetField<byte>();
            }
            set
            {
                SetField(value);
            }
        }

        public byte OriginalPercent
        {
            get
            {
                return GetField<byte>();
            }
            set
            {
                SetField(value);
            }
        }

        public byte OriginalRoleId
        {
            get
            {
                return GetField<byte>();
            }
            set
            {
                SetField(value);
            }
        }

        public bool IsDirty
        {
            get
            {
                return
                    OriginalRoleId != RoleId ||
                    OriginalPercent != Percent;
            }
        }

        public Allocation(AllocationDto dataDto)
        {
            PersonId = dataDto.PersonId;

            OriginalPercent = Percent = dataDto.Percent;

            ProjectId = dataDto.ProjectId;

            OriginalRoleId = RoleId = dataDto.RoleId;
        }

        public Allocation(int projectId, int personId, byte roleId, byte percent)
        {
            ProjectId = projectId;

            PersonId = personId;

            OriginalRoleId = RoleId = roleId;

            OriginalPercent = Percent = percent;
        }
    }
}