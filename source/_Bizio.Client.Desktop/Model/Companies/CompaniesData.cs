﻿using Bizio.Client.Desktop.Core;
using Bizio.Server.Model.Companies;
using System.Collections.Generic;

namespace Bizio.Client.Desktop.Model.Companies
{
    public class CompaniesData
    {
        public IEnumerable<Company> Companies { get; set; }

        public CompaniesData(CompaniesDataDto dataDto)
        {
            Companies = dataDto.Companies.ToList(c => new Company(c));
        }
    }
}