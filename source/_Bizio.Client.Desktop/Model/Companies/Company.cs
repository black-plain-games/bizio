﻿using Bizio.Client.Desktop.Core;
using Bizio.Client.Desktop.Model.Projects;
using Bizio.Server.Model.Companies;
using BlackPlain.Wpf.Core;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Client.Desktop.Model.Companies
{
    public class Company : NotifyOnPropertyChanged
    {
        public int Id { get; }

        public string Name { get; }

        public decimal Money { get; }

        public byte InitialAccuracy { get; }

        public int? UserId { get; }

        public int? RivalId { get; }

        public CompanyStatus Status { get; }

        public IEnumerable<CompanyAction> Actions { get; }

        public IEnumerable<Allocation> Allocations
        {
            get
            {
                return GetField<IEnumerable<Allocation>>();
            }
            set
            {
                SetField(value);
            }
        }

        public IEnumerable<Employee> Employees { get; }

        public IEnumerable<CompanyMessage> Messages { get; }

        public IEnumerable<CompanyPerk> Perks { get; }

        public IEnumerable<Project> Projects { get; }

        public IEnumerable<Prospect> Prospects { get; }

        public Reputation Reputation { get; }

        public IEnumerable<Transaction> Transactions { get; }

        public Company(CompanyDto dataDto)
        {
            Id = dataDto.Id;

            Name = dataDto.Name;

            Money = dataDto.Money;

            InitialAccuracy = dataDto.InitialAccuracy;

            UserId = dataDto.UserId;

            RivalId = dataDto.RivalId;

            Status = (CompanyStatus)dataDto.StatusId;

            Actions = dataDto.Actions.ToList(a => new CompanyAction(a));

            Allocations = dataDto.Allocations.ToList(a => new Allocation(a));

            Employees = dataDto.Employees.ToList(e => new Employee(e));

            Messages = dataDto.Messages.ToList(m => new CompanyMessage(m));

            Perks = dataDto.Perks.ToList(p => new CompanyPerk(p));

            Projects = dataDto.Projects.ToList(p => new Project(p));

            Prospects = dataDto.Prospects.ToList(p => new Prospect(p));

            Reputation = new Reputation(dataDto.Reputation);

            Transactions = dataDto.Transactions.ToList(t => new Transaction(t));
        }
    }
}