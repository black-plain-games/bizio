﻿using Bizio.Client.Desktop.Model.Core;
using Bizio.Server.Model.Companies;
using BlackPlain.Wpf.Core;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Client.Desktop.Model.Companies
{
    public class CompanyAction : NotifyOnPropertyChanged
    {
        public byte ActionId { get; set; }

        public short Count { get; set; }

        public bool IsActive
        {
            get
            {
                return GetField<bool>();
            }
            set
            {
                SetField(value);
            }
        }

        public IEnumerable<IdValuePair<byte, decimal>> Accumulations { get; set; }

        public CompanyAction(CompanyActionDto dataDto)
        {
            ActionId = dataDto.ActionId;

            Count = dataDto.Count;

            IsActive = dataDto.IsActive;

            Accumulations = dataDto.Accumulations.Select(a => new IdValuePair<byte, decimal>(a.Id, a.Value));
        }
    }
}