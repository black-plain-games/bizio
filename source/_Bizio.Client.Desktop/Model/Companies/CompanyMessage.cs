﻿using Bizio.Server.Model.Companies;
using BlackPlain.Wpf.Core;
using System;

namespace Bizio.Client.Desktop.Model.Companies
{
    public class CompanyMessage : NotifyOnPropertyChanged
    {
        public int Id { get; }

        public DateTime DateCreated { get; }

        public CompanyMessageStatus Status
        {
            get
            {
                return GetField<CompanyMessageStatus>();
            }
            set
            {
                SetField(value);
            }
        }

        public string Source { get; }

        public string Subject { get; }

        public string Message { get; }

        public CompanyMessage(CompanyMessageDto dataDto)
        {
            Id = dataDto.Id;

            DateCreated = dataDto.DateCreated;

            Status = (CompanyMessageStatus)dataDto.Status;

            Source = dataDto.Source;

            Subject = dataDto.Subject;

            Message = dataDto.Message;
        }
    }
}