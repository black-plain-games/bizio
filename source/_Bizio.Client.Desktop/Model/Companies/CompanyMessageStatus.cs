﻿namespace Bizio.Client.Desktop.Model.Companies
{
    public enum CompanyMessageStatus : byte
    {
        UnRead = 1,
        Read = 2,
        Deleted = 3
    }
}