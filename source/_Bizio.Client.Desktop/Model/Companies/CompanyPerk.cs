﻿using Bizio.Server.Model.Companies;
using BlackPlain.Wpf.Core;
using System;

namespace Bizio.Client.Desktop.Model.Companies
{
    public class CompanyPerk : NotifyOnPropertyChanged
    {
        public int Id { get; }

        public int PerkId { get; }

        public DateTime? NextPaymentDate { get; }

        public bool IsSelling
        {
            get
            {
                return GetField<bool>();
            }
            set
            {
                SetField(value);
            }
        }

        public CompanyPerk(CompanyPerkDto dtoData)
        {
            Id = dtoData.Id;

            PerkId = dtoData.PerkId;

            NextPaymentDate = dtoData.NextPaymentDate;
        }

        public CompanyPerk(int perkId)
        {
            PerkId = perkId;
        }
    }
}