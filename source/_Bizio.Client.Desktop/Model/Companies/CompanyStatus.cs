﻿namespace Bizio.Client.Desktop.Model.Companies
{
    public enum CompanyStatus
    {
        InBusiness = 1,
        Bankrupt = 2
    }
}