﻿using Bizio.Server.Model.Companies;
using BlackPlain.Wpf.Core;

namespace Bizio.Client.Desktop.Model.Companies
{
    public class Employee : NotifyOnPropertyChanged
    {
        public decimal Happiness { get; }

        public bool IsFounder { get; }

        public int PersonId { get; }

        public decimal Salary { get; }

        public EmployeeStatus Status
        {
            get
            {
                return GetField<EmployeeStatus>();
            }
            set
            {
                SetField(value);
            }
        }

        public Employee(EmployeeDto dataDto)
        {
            PersonId = dataDto.PersonId;

            Salary = dataDto.Salary;

            IsFounder = dataDto.IsFounder;

            Happiness = dataDto.Happiness;
        }
    }
}