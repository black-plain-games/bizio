﻿namespace Bizio.Client.Desktop.Model.Companies
{
    public enum EmployeeStatus
    {
        None,
        IncreasingSalary,
        DecreasingSalary,
        Firing
    }
}