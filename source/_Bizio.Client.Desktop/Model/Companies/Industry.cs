﻿using Bizio.Server.Model.Companies;

namespace Bizio.Client.Desktop.Model.Companies
{
    public class Industry
    {
        public byte Id { get; }

        public string Name { get; }

        public Industry(IndustryDto dtoData)
        {
            Id = dtoData.Id;

            Name = dtoData.Name;
        }
    }
}