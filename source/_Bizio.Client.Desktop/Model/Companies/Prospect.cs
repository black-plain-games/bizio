﻿using Bizio.Client.Desktop.Model.Core;
using Bizio.Client.Desktop.Model.Game;
using Bizio.Server.Model.Companies;
using BlackPlain.Wpf.Core;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Client.Desktop.Model.Companies
{
    public class Prospect : NotifyOnPropertyChanged
    {
        public byte Accuracy { get; }

        public int PersonId { get; }

        public Range<decimal> Salary { get; }

        public IDictionary<byte, Range<decimal>> Skills { get; }

        public HiringProcessStatus Status
        {
            get
            {
                return GetField<HiringProcessStatus>();
            }
            set
            {
                SetField(value);
            }
        }

        public Prospect(ProspectDto dataDto)
        {
            PersonId = dataDto.PersonId;

            Accuracy = dataDto.Accuracy;

            Salary = new Range<decimal>(dataDto.Salary);

            Skills = dataDto.Skills.ToDictionary(s => s.Id, s => new Range<decimal>(s.Value));
        }
    }
}