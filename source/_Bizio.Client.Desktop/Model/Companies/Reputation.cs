﻿using Bizio.Server.Model.Companies;

namespace Bizio.Client.Desktop.Model.Companies
{
    public class Reputation
    {
        public decimal EarnedStars { get; }

        public int PossibleStars { get; }

        public decimal Value { get; }

        public Reputation(ReputationDto dataDto)
        {
            EarnedStars = dataDto.EarnedStars;

            PossibleStars = dataDto.PossibleStars;

            Value = dataDto.Value;
        }
    }
}