﻿using Bizio.Server.Model.Companies;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Client.Desktop.Model.Companies
{
    public class StaticCompaniesData
    {
        public IEnumerable<Industry> Industries { get; }

        public IReadOnlyDictionary<byte, string> TransactionTypes { get; }

        public StaticCompaniesData(StaticCompaniesDataDto dtoData)
        {
            Industries = dtoData.Industries.Select(i => new Industry(i));

            TransactionTypes = dtoData.TransactionTypes.ToDictionary(tt => tt.Id, tt => tt.Value);
        }
    }
}