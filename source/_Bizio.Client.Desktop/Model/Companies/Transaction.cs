﻿using Bizio.Server.Model.Companies;
using System;

namespace Bizio.Client.Desktop.Model.Companies
{
    public class Transaction
    {
        public int Id { get; }

        public decimal Amount { get; }

        public DateTime Date { get; }

        public string Description { get; }

        public decimal EndingBalance { get; }

        public TransactionType TransactionType { get; }

        public Transaction(TransactionDto dataDto)
        {
            Id = dataDto.Id;

            Amount = dataDto.Amount;

            Date = dataDto.Date;

            Description = dataDto.Description;

            EndingBalance = dataDto.EndingBalance;

            TransactionType = (TransactionType)dataDto.TransactionType;
        }
    }
}