﻿namespace Bizio.Client.Desktop.Model.Companies
{
    public enum TransactionType : byte
    {
        None,
        Payroll = 1,
        HireEmployee,
        FireEmployee,
        PurchasePerk,
        SellPerk,
        Perk,
        Project,
        Maintenance
    }
}