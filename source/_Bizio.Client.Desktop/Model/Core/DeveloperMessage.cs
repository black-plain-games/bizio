﻿using System;

namespace Bizio.Client.Desktop.Model.Core
{
    public class DeveloperMessage
    {
        public DateTime TimeStamp { get; }

        public string CallStack { get; }

        public string Message { get; }

        public DeveloperMessage(string message)
        {
            TimeStamp = DateTime.Now;

            CallStack = Environment.StackTrace;

            Message = message;
        }
    }
}