﻿using System;

namespace Bizio.Client.Desktop.Model.Core
{

    public class EventResultArgs<TArg, TResult> : EventArgs
    {
        public TArg Argument { get; }

        public TResult Result { get; set; }

        public bool DoDismiss { get; set; }

        public EventResultArgs(TArg argument)
        {
            Argument = argument;

            DoDismiss = true;
        }
    }
}