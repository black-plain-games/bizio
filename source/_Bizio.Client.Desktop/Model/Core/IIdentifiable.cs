﻿namespace Bizio.Client.Desktop.Model.Core
{
    public interface IIdentifiable<T>
    {
        T Id { get; }
    }
}