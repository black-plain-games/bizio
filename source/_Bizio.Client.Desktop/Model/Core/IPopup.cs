﻿using Bizio.Client.Desktop.ViewModels.Popups;
using System.Windows;

namespace Bizio.Client.Desktop.Model.Core
{
    public interface IPopup
    {
        FrameworkElement View { get; }

        IPopupViewModel ViewModel { get; }
    }
}