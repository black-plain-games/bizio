﻿namespace Bizio.Client.Desktop.Model.Core
{
    public enum Panel
    {
        None = 0,
        People = 1,
        UserCompany = 2,
        OtherCompanies = 3,
        Projects = 4,
        Store = 5,
        Mailbox = 6
    }
}