﻿using Bizio.Client.Desktop.ViewModels.Popups;
using System.Windows;

namespace Bizio.Client.Desktop.Model.Core
{
    public class Popup<TViewModel> : IPopup
        where TViewModel : IPopupViewModel
    {
        public FrameworkElement View { get; }

        public IPopupViewModel ViewModel => TypedViewModel;

        public TViewModel TypedViewModel { get; }

        public Popup(FrameworkElement view, TViewModel viewModel)
        {
            View = view;

            TypedViewModel = viewModel;
        }
    }
}