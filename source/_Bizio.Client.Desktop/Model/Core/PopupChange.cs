﻿namespace Bizio.Client.Desktop.Model.Core
{
    public enum PopupChange : byte
    {
        Pushed,
        Popped
    }
}