﻿using System;

namespace Bizio.Client.Desktop.Model.Core
{
    public class PopupChangedEventArgs : EventArgs
    {
        public IPopup Popup { get; }

        public PopupChange Change { get; }

        public int PopupCount { get; }

        public PopupChangedEventArgs(IPopup popup, PopupChange change, int popupCount)
        {
            Popup = popup;

            Change = change;

            PopupCount = popupCount;
        }
    }
}