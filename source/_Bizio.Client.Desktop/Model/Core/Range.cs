﻿using Bizio.Server.Model.Core;

namespace Bizio.Client.Desktop.Model.Core
{
    public class Range<T>
    {
        public T Minimum { get; set; }

        public T Maximum { get; set; }

        public Range() { }

        public Range(RangeDto<T> range)
        {
            Minimum = range.Minimum;

            Maximum = range.Maximum;
        }

        public Range(T minimum, T maximum)
        {
            Minimum = minimum;

            Maximum = maximum;
        }
    }
}