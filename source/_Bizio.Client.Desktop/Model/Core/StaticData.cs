﻿using Bizio.Client.Desktop.Model.Actions;
using Bizio.Client.Desktop.Model.Companies;
using Bizio.Client.Desktop.Model.Game;
using Bizio.Client.Desktop.Model.People;
using Bizio.Client.Desktop.Model.Perks;
using Bizio.Client.Desktop.Model.Projects;
using Bizio.Server.Model.Core;

namespace Bizio.Client.Desktop.Model.Core
{
    public class StaticData
    {
        public StaticActionsData ActionsData { get; }

        public StaticGameData GameData { get; }

        public StaticCompaniesData CompaniesData { get; }

        public StaticPeopleData PeopleData { get; }

        public StaticProjectsData ProjectsData { get; }

        public StaticPerksData PerksData { get; }

        public StaticData(StaticDataDto dataDto)
        {
            ActionsData = new StaticActionsData(dataDto.ActionsData);

            GameData = new StaticGameData(dataDto.GameData);

            CompaniesData = new StaticCompaniesData(dataDto.CompaniesData);

            PeopleData = new StaticPeopleData(dataDto.PeopleData);

            ProjectsData = new StaticProjectsData(dataDto.ProjectsData);

            PerksData = new StaticPerksData(dataDto.PerksData);
        }
    }
}