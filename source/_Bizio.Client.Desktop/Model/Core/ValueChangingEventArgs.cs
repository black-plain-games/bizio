﻿using System;

namespace Bizio.Client.Desktop.Model.Core
{
    public class ValueChangingEventArgs<TValue> : EventArgs
    {
        public bool IsCancelled { get; set; }

        public TValue OriginalValue { get; }

        public TValue NewValue { get; }

        public ValueChangingEventArgs(TValue originalValue, TValue newValue)
        {
            OriginalValue = originalValue;

            NewValue = newValue;

            IsCancelled = false;
        }
    }
}