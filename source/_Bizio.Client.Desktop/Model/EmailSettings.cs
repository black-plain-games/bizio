﻿namespace Bizio.Client.Desktop.Model.Settings
{
    public class EmailSettings
    {
        public string ReportIssueToEmailAddress { get; set; }
        public string SmtpClientHost { get; set; }
        public int SmtpClientPort { get; set; }
        public string SmtpClientUsername { get; set; }
        public string SmtpClientPassword { get; set; }
        public string EmailServiceFromAddress { get; set; }
        public string EmailServiceDisplayName { get; set; }
    }
}