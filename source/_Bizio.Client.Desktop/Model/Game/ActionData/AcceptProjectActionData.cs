﻿using System.Collections.Generic;

namespace Bizio.Client.Desktop.Model.Game.ActionData
{
    public class AcceptProjectActionData : BaseActionData<AcceptProjectActionData.ActionData>
    {
        public class ActionData
        {
            public int ProjectId { get; }

            public ActionData(int projectId)
            {
                ProjectId = projectId;
            }
        }

        public AcceptProjectActionData(int projectId)
            : base(ActionType.AcceptProject, projectId)
        {
        }

        public AcceptProjectActionData(IDictionary<string, object> data)
            : base(ActionType.AcceptProject, data)
        {
        }
    }
}