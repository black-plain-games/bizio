﻿namespace Bizio.Client.Desktop.Model.Game.ActionData
{
    public enum ActionType : byte
    {
        None = 0,
        RecruitPerson = 1,
        InterviewProspect = 2,
        MakeOffer = 3,
        FireEmployee = 4,
        AdjustSalary = 5,
        AdjustAllocation = 6,
        AcceptProject = 7,
        RequestExtension = 8,
        SubmitProject = 9,
        PurchasePerk = 10,
        SellPerk = 11,
        ChangeMessageStatus = 12,
        ToggleCompanyAction = 13
    }
}