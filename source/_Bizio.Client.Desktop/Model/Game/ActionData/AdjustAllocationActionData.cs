﻿using System.Collections.Generic;

namespace Bizio.Client.Desktop.Model.Game.ActionData
{
    public class AdjustAllocationActionData : BaseActionData<AdjustAllocationActionData.ActionData>
    {
        public class ActionData
        {
            public int PersonId { get; }

            public int ProjectId { get; }

            public byte Percentage { get; set; }

            public byte Role { get; set; }

            public ActionData(int personId, int projectId, byte percentage, byte roleId)
            {
                PersonId = personId;

                ProjectId = projectId;

                Percentage = percentage;

                Role = roleId;
            }
        }

        public AdjustAllocationActionData(int personId, int projectId, byte percentage, byte roleId)
            : base(ActionType.AdjustAllocation, personId, projectId, percentage, roleId)
        {
        }

        public AdjustAllocationActionData(IDictionary<string, object> data)
            : base(ActionType.AdjustAllocation, data)
        {
        }
    }
}