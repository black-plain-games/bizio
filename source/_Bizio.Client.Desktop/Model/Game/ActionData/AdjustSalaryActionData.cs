﻿using System.Collections.Generic;

namespace Bizio.Client.Desktop.Model.Game.ActionData
{
    public class AdjustSalaryActionData : BaseActionData<AdjustSalaryActionData.ActionData>
    {
        public class ActionData
        {
            public int PersonId { get; }

            public decimal Salary { get; }

            public ActionData(int personId, decimal salary)
            {
                PersonId = personId;

                Salary = salary;
            }
        }

        public AdjustSalaryActionData(int personId, decimal salary)
            : base(ActionType.AdjustSalary, personId, salary)
        {
        }

        public AdjustSalaryActionData(IDictionary<string, object> data)
            : base(ActionType.AdjustSalary, data)
        {
        }
    }
}