﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Client.Desktop.Model.Game.ActionData
{
    public abstract class BaseActionData<T> : IActionData
    {
        public byte ActionTypeId => _actionTypeId;

        public T Data { get; }

        public event EventHandler Cancelled;

        protected BaseActionData(ActionType actionType, params object[] args)
        {
            _actionTypeId = (byte)actionType;

            var dataType = typeof(T);

            var constructor = dataType.GetConstructors().First();

            Data = (T)constructor.Invoke(args);
        }

        protected BaseActionData(ActionType actionType, IDictionary<string, object> data)
        {
            _actionTypeId = (byte)actionType;

            var dataType = typeof(T);

            var constructor = dataType.GetConstructors().First();

            var parameters = constructor.GetParameters();

            var input = new List<object>();

            foreach (var parameter in parameters)
            {
                var dataValue = data[parameter.Name.ToLower()];

                var inputValue = Convert.ChangeType(dataValue, parameter.ParameterType);

                input.Add(inputValue);
            }

            Data = (T)constructor.Invoke(input.ToArray());
        }

        public IDictionary<string, object> GetData()
        {
            var dataType = typeof(T);

            var properties = dataType.GetProperties();

            var output = new Dictionary<string, object>();

            foreach (var property in properties)
            {
                output.Add(property.Name.ToLower(), property.GetValue(Data));
            }

            return output;
        }

        public void Cancel()
        {
            Cancelled?.Invoke(this, EventArgs.Empty);
        }

        private readonly byte _actionTypeId;
    }
}