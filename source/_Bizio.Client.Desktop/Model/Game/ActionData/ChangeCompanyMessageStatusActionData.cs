﻿using Bizio.Client.Desktop.Model.Companies;
using System.Collections.Generic;

namespace Bizio.Client.Desktop.Model.Game.ActionData
{
    public class ChangeCompanyMessageStatusActionData : BaseActionData<ChangeCompanyMessageStatusActionData.ActionData>
    {
        public class ActionData
        {
            public int MessageId { get; }

            public byte Status { get; }

            public ActionData(int messageId, byte status)
            {
                MessageId = messageId;

                Status = status;
            }
        }

        public ChangeCompanyMessageStatusActionData(int messageId, CompanyMessageStatus status)
            : base(ActionType.ChangeMessageStatus, messageId, (byte)status)
        {
        }

        public ChangeCompanyMessageStatusActionData(IDictionary<string, object> data)
            : base(ActionType.ChangeMessageStatus, data)
        {
        }
    }
}