﻿using System.Collections.Generic;

namespace Bizio.Client.Desktop.Model.Game.ActionData
{
    public class FireEmployeeActionData : BaseActionData<FireEmployeeActionData.ActionData>
    {
        public class ActionData
        {
            public int PersonId { get; }

            public ActionData(int personId)
            {
                PersonId = personId;
            }
        }

        public FireEmployeeActionData(int personId)
            : base(ActionType.FireEmployee, personId)
        {
        }

        public FireEmployeeActionData(IDictionary<string, object> data)
            : base(ActionType.FireEmployee, data)
        {
        }
    }
}