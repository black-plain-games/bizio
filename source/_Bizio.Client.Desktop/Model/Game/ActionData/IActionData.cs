﻿using System;
using System.Collections.Generic;

namespace Bizio.Client.Desktop.Model.Game.ActionData
{
    public interface IActionData
    {
        byte ActionTypeId { get; }

        IDictionary<string, object> GetData();

        event EventHandler Cancelled;

        void Cancel();
    }
}