﻿using System.Collections.Generic;

namespace Bizio.Client.Desktop.Model.Game.ActionData
{
    public class InterviewProspectActionData : BaseActionData<InterviewProspectActionData.ActionData>
    {
        public class ActionData
        {
            public int PersonId { get; }

            public ActionData(int personId)
            {
                PersonId = personId;
            }
        }

        public InterviewProspectActionData(int personId)
            : base(ActionType.InterviewProspect, personId)
        {
        }

        public InterviewProspectActionData(IDictionary<string, object> data)
            : base(ActionType.InterviewProspect, data)
        {
        }
    }
}