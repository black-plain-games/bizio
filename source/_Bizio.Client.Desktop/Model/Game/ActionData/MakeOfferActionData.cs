﻿using System.Collections.Generic;

namespace Bizio.Client.Desktop.Model.Game.ActionData
{
    public class MakeOfferActionData : BaseActionData<MakeOfferActionData.ActionData>
    {
        public class ActionData
        {
            public int PersonId { get; }

            public decimal OfferValue { get; }

            public ActionData(int personId, decimal offerValue)
            {
                PersonId = personId;

                OfferValue = offerValue;
            }
        }

        public MakeOfferActionData(int personId, decimal offerValue)
            : base(ActionType.MakeOffer, personId, offerValue)
        {
        }

        public MakeOfferActionData(IDictionary<string, object> data)
            : base(ActionType.MakeOffer, data)
        {
        }
    }
}