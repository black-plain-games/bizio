﻿using System.Collections.Generic;

namespace Bizio.Client.Desktop.Model.Game.ActionData
{
    public class PurchasePerkActionData : BaseActionData<PurchasePerkActionData.ActionData>
    {
        public class ActionData
        {
            public int PerkId { get; }

            public ActionData(int perkId)
            {
                PerkId = perkId;
            }
        }

        public PurchasePerkActionData(int perkId)
            : base(ActionType.PurchasePerk, perkId)
        {
        }

        public PurchasePerkActionData(IDictionary<string, object> data)
            : base(ActionType.PurchasePerk, data)
        {
        }
    }
}