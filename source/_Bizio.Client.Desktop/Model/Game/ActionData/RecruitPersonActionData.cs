﻿using System.Collections.Generic;

namespace Bizio.Client.Desktop.Model.Game.ActionData
{
    public class RecruitPersonActionData : BaseActionData<RecruitPersonActionData.ActionData>
    {
        public class ActionData
        {
            public int PersonId { get; }

            public ActionData(int personId)
            {
                PersonId = personId;
            }
        }

        public RecruitPersonActionData(int personId)
            : base(ActionType.RecruitPerson, personId)
        {
        }

        public RecruitPersonActionData(IDictionary<string, object> data)
            : base(ActionType.RecruitPerson, data)
        {
        }
    }
}