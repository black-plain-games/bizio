﻿using System.Collections.Generic;

namespace Bizio.Client.Desktop.Model.Game.ActionData
{
    public class RequestExtensionTurnActionData : BaseActionData<RequestExtensionTurnActionData.ActionData>
    {
        public class ActionData
        {
            public int ProjectId { get; }

            public ActionData(int projectId)
            {
                ProjectId = projectId;
            }
        }

        public RequestExtensionTurnActionData(int projectId)
            : base(ActionType.RequestExtension, projectId)
        {
        }

        public RequestExtensionTurnActionData(IDictionary<string, object> data)
            : base(ActionType.RequestExtension, data)
        {
        }
    }
}