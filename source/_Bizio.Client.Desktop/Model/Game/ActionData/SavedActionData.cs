﻿using System.Collections.Generic;

namespace Bizio.Client.Desktop.Model.Game.ActionData
{
    public class SavedActionData
    {
        public byte ActionTypeId { get; set; }

        public IDictionary<string, object> Data { get; set; }
    }
}