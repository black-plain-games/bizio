﻿using System.Collections.Generic;

namespace Bizio.Client.Desktop.Model.Game.ActionData
{
    public class SellPerkActionData : BaseActionData<SellPerkActionData.ActionData>
    {
        public class ActionData
        {
            public int CompanyPerkId { get; }

            public ActionData(int companyPerkId)
            {
                CompanyPerkId = companyPerkId;
            }
        }

        public SellPerkActionData(int companyPerkId)
            : base(ActionType.SellPerk, companyPerkId)
        {
        }

        public SellPerkActionData(IDictionary<string, object> data)
            : base(ActionType.SellPerk, data)
        {
        }
    }
}