﻿using System.Collections.Generic;

namespace Bizio.Client.Desktop.Model.Game.ActionData
{
    public class SubmitProjectTurnActionData : BaseActionData<SubmitProjectTurnActionData.ActionData>
    {
        public class ActionData
        {
            public int ProjectId { get; }

            public bool IsCancellation { get; }

            public ActionData(int projectId, bool isCancellation)
            {
                ProjectId = projectId;

                IsCancellation = isCancellation;
            }
        }

        public SubmitProjectTurnActionData(int projectId, bool isCancellation)
            : base(ActionType.SubmitProject, projectId, isCancellation)
        {
        }

        public SubmitProjectTurnActionData(IDictionary<string, object> data)
            : base(ActionType.SubmitProject, data)
        {
        }
    }
}