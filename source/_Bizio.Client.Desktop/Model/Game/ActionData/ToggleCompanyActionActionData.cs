﻿using System.Collections.Generic;

namespace Bizio.Client.Desktop.Model.Game.ActionData
{
    public class ToggleCompanyActionActionData : BaseActionData<ToggleCompanyActionActionData.ActionData>
    {
        public class ActionData
        {
            public byte ActionId { get; }

            public ActionData(byte actionId)
            {
                ActionId = actionId;
            }
        }

        public ToggleCompanyActionActionData(byte actionId)
            : base(ActionType.ToggleCompanyAction, actionId)
        {
        }

        public ToggleCompanyActionActionData(IDictionary<string, object> data)
            : base(ActionType.ToggleCompanyAction, data)
        {
        }
    }
}