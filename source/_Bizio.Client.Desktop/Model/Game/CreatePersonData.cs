﻿using Bizio.Client.Desktop.Model.Core;
using Bizio.Client.Desktop.Model.People;
using BlackPlain.Wpf.Core;
using System;
using System.Collections.ObjectModel;

namespace Bizio.Client.Desktop.Model.Game
{
    public class CreatePersonData : NotifyOnPropertyChanged
    {
        public string FirstName
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public string LastName
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public byte Gender
        {
            get
            {
                return GetField<byte>();
            }
            set
            {
                SetField(value);
            }
        }

        public DateTime Birthday
        {
            get
            {
                return GetField<DateTime>();
            }
            set
            {
                SetField(value);
            }
        }

        public ushort PersonalityId
        {
            get
            {
                return GetField<ushort>();
            }
            set
            {
                SetField(value);
            }
        }

        public byte IndustryId
        {
            get
            {
                return GetField<byte>();
            }
            set
            {
                SetField(value);

                OnPropertyChanged(nameof(MandatorySkills));

                OnPropertyChanged(nameof(OptionalSkills));
            }
        }

        public ObservableCollection<IdValuePair<SkillDefinition, byte>> MandatorySkills
        {
            get
            {
                return GetField<ObservableCollection<IdValuePair<SkillDefinition, byte>>>();
            }
            set
            {
                SetField(value);
            }
        }

        public ObservableCollection<IdValuePair<SkillDefinition, byte>> OptionalSkills
        {
            get
            {
                return GetField<ObservableCollection<IdValuePair<SkillDefinition, byte>>>();
            }
            set
            {
                SetField(value);
            }
        }

        public CreatePersonData()
        {
            PersonalityId = 1;

            Birthday = DateTime.Today.AddYears(-18);

            MandatorySkills = new ObservableCollection<IdValuePair<SkillDefinition, byte>>();

            OptionalSkills = new ObservableCollection<IdValuePair<SkillDefinition, byte>>();
        }
    }
}