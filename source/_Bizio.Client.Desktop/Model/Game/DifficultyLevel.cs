﻿using Bizio.Server.Model.Game;

namespace Bizio.Client.Desktop.Model.Game
{
    public class DifficultyLevel
    {
        public byte Id { get; }

        public string Name { get; }

        public byte MaxOptionalSkillCount { get; }

        public short MaxTotalSkillPoints { get; }

        public decimal InitialFunds { get; }

        public DifficultyLevel(DifficultyLevelDto dataDto)
        {
            Id = dataDto.Id;

            Name = dataDto.Name;

            MaxOptionalSkillCount = dataDto.MaximumOptionalSkillCount;

            MaxTotalSkillPoints = dataDto.MaximumTotalSkillPoints;

            InitialFunds = dataDto.InitialFunds;
        }
    }
}