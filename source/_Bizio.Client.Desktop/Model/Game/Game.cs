﻿using Bizio.Server.Model.Game;
using System;

namespace Bizio.Client.Desktop.Model.Game
{
    public class Game
    {
        public int Id { get; set; }

        public string FounderFirstName { get; set; }

        public string FounderLastName { get; set; }

        public string CompanyName { get; set; }

        public DateTimeOffset CreatedDate { get; set; }

        public short IndustryId { get; set; }

        public byte WeeksPerTurn { get; set; }

        public short CurrentTurn { get; set; }

        public GameStatus Status { get; set; }

        public Game(GameDto data)
        {
            Id = data.Id;

            CompanyName = data.CompanyName;

            CreatedDate = data.CreatedDate;

            CurrentTurn = data.CurrentTurn;

            FounderFirstName = data.FounderFirstName;

            FounderLastName = data.FounderLastName;

            IndustryId = data.IndustryId;

            WeeksPerTurn = data.WeeksPerTurn;

            Status = (GameStatus)data.StatusId;
        }
    }
}