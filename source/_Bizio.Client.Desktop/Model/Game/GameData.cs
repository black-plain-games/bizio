﻿using Bizio.Client.Desktop.Model.Companies;
using Bizio.Client.Desktop.Model.GameClock;
using Bizio.Client.Desktop.Model.People;
using Bizio.Client.Desktop.Model.Projects;
using Bizio.Server.Model.Game;
using System.Collections.ObjectModel;

namespace Bizio.Client.Desktop.Model.Game
{
    public class GameData
    {
        public int GameId { get; }

        public byte IndustryId { get; }

        public GameStatus Status { get; set; }

        public GameClockData GameClockData { get; }

        public ProjectsData ProjectsData { get; }

        public PeopleData PeopleData { get; }

        public CompaniesData CompaniesData { get; }

        public ObservableCollection<Allocation> NewAllocations { get; }

        public GameData(GameDataDto dataDto)
        {
            GameId = dataDto.GameId;

            IndustryId = dataDto.IndustryId;

            Status = (GameStatus)dataDto.StatusId;

            GameClockData = new GameClockData(dataDto.GameClockData);

            ProjectsData = new ProjectsData(dataDto.ProjectsData);

            PeopleData = new PeopleData(dataDto.PeopleData);

            CompaniesData = new CompaniesData(dataDto.CompaniesData);

            NewAllocations = new ObservableCollection<Allocation>();
        }
    }
}