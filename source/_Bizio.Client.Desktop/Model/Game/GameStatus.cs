﻿namespace Bizio.Client.Desktop.Model.Game
{
    public enum GameStatus
    {
        Playing = 1,
        UserLost = 2
    }
}