﻿namespace Bizio.Client.Desktop.Model.Game
{
    public enum HiringProcessStatus : byte
    {
        None,
        RecruitPending,
        InterviewPending,
        OfferPending
    }
}