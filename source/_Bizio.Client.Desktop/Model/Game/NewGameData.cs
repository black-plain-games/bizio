﻿using BlackPlain.Wpf.Core;
using System;

namespace Bizio.Client.Desktop.Model.Game
{
    public class NewGameData : NotifyOnPropertyChanged
    {
        public DateTime StartDate
        {
            get
            {
                return GetField<DateTime>();
            }
            set
            {
                SetField(value);
            }
        }

        public byte WeeksPerTurn
        {
            get
            {
                return GetField<byte>();
            }
            set
            {
                SetField(value);
            }
        }

        public string CompanyName
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public byte NumberOfCompanies
        {
            get
            {
                return GetField<byte>();
            }
            set
            {
                SetField(value);
            }
        }

        public byte IndustryId
        {
            get
            {
                return GetField<byte>();
            }
            set
            {
                SetField(value);
            }
        }

        public decimal InitialFunds
        {
            get
            {
                return GetField<decimal>();
            }
            set
            {
                SetField(value);
            }
        }

        public CreatePersonData Founder
        {
            get
            {
                return GetField<CreatePersonData>();
            }
            set
            {
                SetField(value);
            }
        }

        public NewGameData()
        {
            Founder = new CreatePersonData();
        }
    }
}