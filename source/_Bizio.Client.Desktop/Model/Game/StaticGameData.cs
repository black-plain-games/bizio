﻿using Bizio.Client.Desktop.Core;
using Bizio.Server.Model.Game;
using System.Collections.Generic;

namespace Bizio.Client.Desktop.Model.Game
{
    public class StaticGameData
    {
        public byte MaximumNumberOfCompanies { get; }

        public IEnumerable<DifficultyLevel> DifficultyLevels { get; }

        public StaticGameData(StaticGameDataDto dataDto)
        {
            MaximumNumberOfCompanies = 10;

            DifficultyLevels = dataDto.DifficultyLevels.ToList(dl => new DifficultyLevel(dl));
        }
    }
}