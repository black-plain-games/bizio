﻿using Bizio.Server.Model.GameClock;
using System;

namespace Bizio.Client.Desktop.Model.GameClock
{
    public class GameClockData
    {
        public DateTime StartDate { get; }

        public DateTime CurrentDate { get; }

        public short CurrentTurn { get; }

        public byte WeeksPerTurn { get; }

        public GameClockData(GameClockDataDto dataDto)
        {
            StartDate = dataDto.StartDate;

            CurrentDate = dataDto.CurrentDate;

            CurrentTurn = dataDto.CurrentTurn;

            WeeksPerTurn = dataDto.WeeksPerTurn;
        }
    }
}