﻿using Bizio.Server.Model.Text;
using System.Collections.Generic;

namespace Bizio.Client.Desktop.Model
{
    public class Language
    {
        public byte Id { get; set; }

        public string Name { get; set; }

        public string Iso2LetterCode { get; set; }

        public IDictionary<short, string> Text { get; set; }

        public Language(LanguageDto data)
        {
            Id = data.Id;

            Name = data.Name;

            Iso2LetterCode = data.Iso2LetterCode;

            Text = new Dictionary<short, string>(data.Text);
        }
    }
}
