﻿using Bizio.Server.Model.Text;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Client.Desktop.Model
{
    public class LanguageData
    {
        public IEnumerable<Language> Languages { get; set; }

        public IDictionary<short, string> TextResourceKeys { get; set; }

        public LanguageData(LanguageDataDto data)
        {
            Languages = data.Languages.Select(l => new Language(l)).ToList();

            TextResourceKeys = data.TextResourceKeys.ToDictionary(trk => trk.Key, trk => trk.Value);
        }
    }
}
