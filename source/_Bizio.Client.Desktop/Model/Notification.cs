﻿using Bizio.Server.Model;
using BlackPlain.Wpf.Core;
using System;

namespace Bizio.Client.Desktop.Model
{
    public class Notification : NotifyOnPropertyChanged
    {
        public int Id { get; set; }

        public DateTimeOffset DateCreated { get; set; }

        public string Subject { get; set; }

        public string Message { get; set; }

        public bool IsRead
        {
            get
            {
                return GetField<bool>();
            }
            set
            {
                SetField(value);
            }
        }

        public Notification()
        {
        }

        public Notification(NotificationDto data)
        {
            Id = data.Id;

            DateCreated = data.DateCreated;

            Subject = data.Subject;

            Message = data.Message;

            IsRead = false;
        }
    }
}