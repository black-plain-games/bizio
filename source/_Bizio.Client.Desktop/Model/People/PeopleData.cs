﻿using Bizio.Client.Desktop.Core;
using Bizio.Server.Model.People;
using System.Collections.Generic;

namespace Bizio.Client.Desktop.Model.People
{
    public class PeopleData
    {
        public IEnumerable<Person> People { get; }

        public PeopleData(PeopleDataDto dataDto)
        {
            People = dataDto.People.ToList(p => new Person(p));
        }
    }
}