﻿using Bizio.Client.Desktop.Model.Game;
using Bizio.Server.Model.People;
using BlackPlain.Wpf.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Client.Desktop.Model.People
{
    public class Person : NotifyOnPropertyChanged
    {
        public int Id { get; }

        public string FirstName { get; }

        public string LastName { get; }

        public byte GenderId { get; }

        public DateTime Birthday { get; }

        public byte PersonalityId { get; }

        public DateTime RetirementDate { get; }

        public byte? ProfessionId { get; }

        public IEnumerable<Skill> Skills { get; }

        public IEnumerable<WorkHistory> WorkHistory { get; }

        public HiringProcessStatus Status
        {
            get
            {
                return GetField<HiringProcessStatus>();
            }
            set
            {
                SetField(value);
            }
        }

        public Person(PersonDto dataDto)
        {
            Id = dataDto.Id;

            FirstName = dataDto.FirstName;

            LastName = dataDto.LastName;

            GenderId = dataDto.GenderId;

            Birthday = dataDto.Birthday;

            PersonalityId = dataDto.PersonalityId;

            RetirementDate = dataDto.RetirementDate;

            ProfessionId = dataDto.ProfessionId;

            Skills = dataDto.Skills.Select(s => new Skill(s));

            WorkHistory = dataDto.WorkHistory.Select(wh => new WorkHistory(wh));
        }
    }
}