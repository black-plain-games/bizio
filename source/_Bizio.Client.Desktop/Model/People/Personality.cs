﻿using Bizio.Server.Model.People;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Client.Desktop.Model.People
{
    public class Personality
    {
        public byte Id { get; }

        public string Name { get; }

        public string Description { get; }

        public IReadOnlyDictionary<byte, decimal> Attributes { get; }

        public Personality(PersonalityDto dataDto)
        {
            Id = dataDto.Id;

            Name = dataDto.Name;

            Description = dataDto.Description;

            Attributes = dataDto.Attributes.ToDictionary(a => a.Id, a => a.Value);
        }
    }
}