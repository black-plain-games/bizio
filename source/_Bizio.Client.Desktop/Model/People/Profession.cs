﻿using Bizio.Client.Desktop.Model.Core;
using Bizio.Server.Model.People;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Client.Desktop.Model.People
{
    public class Profession : IIdentifiable<byte>
    {
        public byte Id { get; }

        public string Name { get; }

        public IEnumerable<ProfessionSkillDefinition> SkillDefinitions { get; }

        public Profession(ProfessionDto dataDto)
        {
            Id = dataDto.Id;

            Name = dataDto.Name;

            SkillDefinitions = dataDto.SkillDefinitions.Select(sd => new ProfessionSkillDefinition(sd));
        }
    }
}