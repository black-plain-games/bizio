﻿using Bizio.Server.Model.People;

namespace Bizio.Client.Desktop.Model.People
{
    public class ProfessionSkillDefinition
    {
        public byte SkillDefinitionId { get; }

        public byte Weight { get; }

        public ProfessionSkillDefinition(ProfessionSkillDefinitionDto dataDto)
        {
            SkillDefinitionId = dataDto.SkillDefinitionId;

            Weight = dataDto.Weight;
        }
    }
}