﻿using Bizio.Server.Model.People;

namespace Bizio.Client.Desktop.Model.People
{
    public class Skill
    {
        public byte ForgetRate { get; }

        public byte LearnRate { get; }

        public decimal Value { get; }

        public byte SkillDefinitionId { get; }

        public Skill(SkillDto dataDto)
        {
            ForgetRate = dataDto.ForgetRate;

            LearnRate = dataDto.LearnRate;

            Value = dataDto.Value;

            SkillDefinitionId = dataDto.SkillDefinitionId;
        }
    }
}