﻿using Bizio.Client.Desktop.Model.Core;
using Bizio.Server.Model.People;

namespace Bizio.Client.Desktop.Model.People
{
    public class SkillDefinition : IIdentifiable<int>
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public Range<byte> Value { get; set; }

        public SkillDefinition() { }

        public override bool Equals(object obj)
        {
            if (obj is SkillDefinition)
            {
                return (obj as SkillDefinition).Id == Id;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public SkillDefinition(SkillDefinitionDto skillDefinition)
        {
            Id = skillDefinition.Id;

            Name = skillDefinition.Name;

            Value = new Range<byte>(skillDefinition.Value);
        }
    }
}