﻿using Bizio.Server.Model.People;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Client.Desktop.Model.People
{
    public class StaticPeopleData
    {
        public IReadOnlyDictionary<byte, string> Genders { get; }

        public IEnumerable<Personality> Personalities { get; }

        public IEnumerable<SkillDefinition> SkillDefinitions { get; }

        public IReadOnlyDictionary<byte, IEnumerable<SkillDefinition>> MandatorySkillDefinitions { get; }

        public IReadOnlyDictionary<byte, IEnumerable<SkillDefinition>> OptionalSkillDefinitions { get; }

        public IReadOnlyDictionary<byte, IEnumerable<Profession>> Professions { get; }

        public StaticPeopleData(StaticPeopleDataDto dataDto)
        {
            Genders = dataDto.Genders.ToDictionary(g => g.Id, g => g.Value);

            Personalities = dataDto.Personalities.Select(p => new Personality(p));

            SkillDefinitions = dataDto.SkillDefinitions.Select(sd => new SkillDefinition(sd));

            MandatorySkillDefinitions = dataDto.MandatorySkillDefinitions.ToDictionary(i => i.Id, i => i.Value.Select(isd => SkillDefinitions.First(sd => sd.Id == isd.Id)));

            OptionalSkillDefinitions = dataDto.Professions.ToDictionary(i => i.Id, i => i.Value
                .SelectMany(p => p.SkillDefinitions.Select(psd => psd.SkillDefinitionId))
                .Distinct()
                .Select(sdid => SkillDefinitions.First(sd => sd.Id == sdid)));

            Professions = dataDto.Professions.ToDictionary(i => i.Id, i => i.Value.Select(p => new Profession(p)));
        }
    }
}