﻿using Bizio.Server.Model.People;
using System;

namespace Bizio.Client.Desktop.Model.People
{
    public class WorkHistory
    {
        public int CompanyId { get; set; }

        public DateTime? EndDate { get; set; }

        public decimal? EndingSalary { get; set; }

        public DateTime StartDate { get; set; }

        public decimal StartingSalary { get; set; }

        public WorkHistory(WorkHistoryDto dataDto)
        {
            CompanyId = dataDto.CompanyId;

            EndDate = dataDto.EndDate;

            EndingSalary = dataDto.EndingSalary;

            StartDate = dataDto.StartDate;

            StartingSalary = dataDto.StartingSalary;
        }
    }
}