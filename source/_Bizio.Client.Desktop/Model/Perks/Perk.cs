﻿using Bizio.Client.Desktop.Model.Core;
using Bizio.Server.Model.Perks;
using BlackPlain.Wpf.Core;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Client.Desktop.Model.Perks
{
    public class Perk : NotifyOnPropertyChanged, IIdentifiable<int>
    {
        public int Id
        {
            get
            {
                return GetField<int>();
            }
            set
            {
                SetField(value);
            }
        }

        public string Name { get; }

        public string Description { get; }

        public byte? MaxCount { get; }

        public decimal? InitialCost { get; }

        public decimal? RecurringCost { get; }

        public byte? RecurringCostInterval { get; }

        public IEnumerable<PerkBenefit> Benefits { get; }

        public IEnumerable<PerkCondition> Conditions { get; }

        public Perk(PerkDto dataDto)
        {
            Id = dataDto.Id;

            Name = dataDto.Name;

            Description = dataDto.Description;

            MaxCount = dataDto.MaxCount;

            InitialCost = dataDto.InitialCost;

            RecurringCost = dataDto.RecurringCost;

            RecurringCostInterval = dataDto.RecurringCostInterval;

            Benefits = dataDto.Benefits.Select(b => new PerkBenefit(b));

            Conditions = dataDto.Conditions.Select(c => new PerkCondition(c));
        }
    }
}