﻿using Bizio.Server.Model.Perks;

namespace Bizio.Client.Desktop.Model.Perks
{
    public class PerkBenefit
    {
        public PerkTargetType TargetType { get; set; }

        public PerkBenefitAttribute Attribute { get; set; }

        public decimal Value { get; set; }

        public PerkValueType ValueType { get; set; }

        public PerkBenefit(PerkBenefitDto dataDto)
        {
            TargetType = (PerkTargetType)dataDto.TargetType;

            Attribute = (PerkBenefitAttribute)dataDto.Attribute;

            Value = dataDto.Value;

            ValueType = (PerkValueType)dataDto.ValueType;
        }
    }
}