﻿namespace Bizio.Client.Desktop.Model.Perks
{
    public enum PerkBenefitAttribute : byte
    {
        Money = 1,
        Happiness = 2
    }
}