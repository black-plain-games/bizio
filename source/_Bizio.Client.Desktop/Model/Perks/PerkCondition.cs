﻿using Bizio.Server.Model.Perks;

namespace Bizio.Client.Desktop.Model.Perks
{
    public class PerkCondition
    {
        public PerkTargetType TargetType { get; set; }

        public PerkConditionAttribute Attribute { get; set; }

        public decimal Value { get; set; }

        public PerkConditionComparisonType ComparisonType { get; set; }

        public PerkCondition(PerkConditionDto dataDto)
        {
            TargetType = (PerkTargetType)dataDto.TargetType;

            Attribute = (PerkConditionAttribute)dataDto.Attribute;

            Value = dataDto.Value;

            ComparisonType = (PerkConditionComparisonType)dataDto.ComparisonType;
        }
    }
}