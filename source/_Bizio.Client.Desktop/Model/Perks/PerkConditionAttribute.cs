﻿namespace Bizio.Client.Desktop.Model.Perks
{
    public enum PerkConditionAttribute : byte
    {
        Money = 1,
        PerkCount = 2,
        ProjectCount = 3,
        ProspectCount = 4,
        EmployeeCount = 5,
        SkillCount = 6,
        Happiness = 7
    }
}