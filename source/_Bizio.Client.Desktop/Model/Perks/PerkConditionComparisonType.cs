﻿namespace Bizio.Client.Desktop.Model.Perks
{
    public enum PerkConditionComparisonType : byte
    {
        Equal = 1,
        NotEqual = 2,
        LessThan = 3,
        LessThanEqualTo = 4,
        GreaterThan = 5,
        GreaterThanEqualTo = 6
    }
}