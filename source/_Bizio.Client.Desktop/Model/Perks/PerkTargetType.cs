﻿namespace Bizio.Client.Desktop.Model.Perks
{
    public enum PerkTargetType : byte
    {
        Person = 1,
        Employee = 2,
        Company = 3
    }
}