﻿namespace Bizio.Client.Desktop.Model.Perks
{
    public enum PerkValueType : byte
    {
        Literal = 1,
        Percentage = 2
    }
}