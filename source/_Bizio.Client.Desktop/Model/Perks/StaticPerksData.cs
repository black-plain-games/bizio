﻿using Bizio.Server.Model.Perks;
using BlackPlain.Wpf.Core;
using System.Collections.ObjectModel;
using System.Linq;

namespace Bizio.Client.Desktop.Model.Perks
{
    public class StaticPerksData : NotifyOnPropertyChanged
    {
        public ObservableCollection<Perk> Perks
        {
            get
            {
                return GetField<ObservableCollection<Perk>>();
            }
            private set
            {
                SetField(value);
            }
        }

        public StaticPerksData(StaticPerksDataDto dataDto)
        {
            Perks = new ObservableCollection<Perk>(dataDto.Perks.Select(p => new Perk(p)));
        }
    }
}