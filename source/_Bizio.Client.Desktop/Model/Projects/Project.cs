﻿using Bizio.Server.Model.Projects;
using BlackPlain.Wpf.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Client.Desktop.Model.Projects
{
    public class Project : NotifyOnPropertyChanged
    {
        public int Id { get; }

        public DateTime Deadline { get; }

        public DateTime? ExtensionDeadline { get; }

        public short ProjectDefinitionId { get; }

        public StarCategory ReputationRequired { get; }

        public decimal Value { get; }

        public ProjectStatus Status { get; }

        public IEnumerable<ProjectRequirement> Requirements { get; }

        public decimal PercentComplete => Requirements.Sum(r => Math.Min(r.CurrentValue, r.TargetValue)) / Requirements.Sum(r => r.TargetValue);

        public bool IsRequesting
        {
            get
            {
                return GetField<bool>();
            }
            set
            {
                SetField(value);
            }
        }

        public ProjectSubmissionStatus SubmissionStatus
        {
            get
            {
                return GetField<ProjectSubmissionStatus>();
            }
            set
            {
                SetField(value);
            }
        }

        public Project(ProjectDto dataDto)
        {
            Id = dataDto.Id;

            Deadline = dataDto.Deadline;

            ExtensionDeadline = dataDto.ExtensionDeadline;

            ProjectDefinitionId = dataDto.ProjectDefinitionId;

            ReputationRequired = (StarCategory)dataDto.ReputationRequired;

            Value = dataDto.Value;

            Status = (ProjectStatus)dataDto.Status;

            Requirements = dataDto.Requirements.Select(r => new ProjectRequirement(r));
        }
    }
}