﻿using Bizio.Client.Desktop.Model.Core;
using Bizio.Server.Model.Projects;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Client.Desktop.Model.Projects
{
    public class ProjectDefinition : IIdentifiable<short>
    {
        public short Id { get; }

        public string Name { get; }

        public string Description { get; }

        public Range<byte> ProjectLength { get; }

        public Range<decimal> Value { get; }

        public IEnumerable<byte> Industries { get; }

        public IDictionary<byte, Range<decimal>> Skills { get; set; }

        public ProjectDefinition(ProjectDefinitionDto dataDto)
        {
            Id = dataDto.Id;

            Name = dataDto.Name;

            Description = dataDto.Description;

            ProjectLength = new Range<byte>(dataDto.ProjectLength);

            Value = new Range<decimal>(dataDto.Value);

            Industries = dataDto.Industries;

            Skills = dataDto.Skills.ToDictionary(s => s.Id, s => new Range<decimal>(s.Value));
        }
    }
}