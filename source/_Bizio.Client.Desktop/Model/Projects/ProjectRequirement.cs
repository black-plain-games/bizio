﻿using Bizio.Server.Model.Projects;

namespace Bizio.Client.Desktop.Model.Projects
{
    public class ProjectRequirement
    {
        public byte SkillDefinitionId { get; }

        public decimal CurrentValue { get; }

        public decimal TargetValue { get; }

        public ProjectRequirement(ProjectRequirementDto dataDto)
        {
            SkillDefinitionId = dataDto.SkillDefinitionId;

            CurrentValue = dataDto.CurrentValue;

            TargetValue = dataDto.TargetValue;
        }
    }
}