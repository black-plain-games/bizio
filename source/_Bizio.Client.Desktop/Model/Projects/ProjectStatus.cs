﻿namespace Bizio.Client.Desktop.Model.Projects
{
    public enum ProjectStatus : byte
    {
        None = 0,
        Unassigned = 1,
        InProgress = 2,
        Completed = 3,
        CompletedWithExtension = 4,
        Cancelled = 5,
        Failed = 6
    }
}