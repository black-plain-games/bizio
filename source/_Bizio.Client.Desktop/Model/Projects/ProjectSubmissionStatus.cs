﻿namespace Bizio.Client.Desktop.Model.Projects
{
    public enum ProjectSubmissionStatus
    {
        InProgress,
        Submitting,
        Cancelling,
        RequestingExtension
    }
}