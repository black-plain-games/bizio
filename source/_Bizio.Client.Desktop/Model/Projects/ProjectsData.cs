﻿using Bizio.Client.Desktop.Core;
using Bizio.Server.Model.Projects;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Client.Desktop.Model.Projects
{
    public class ProjectsData
    {
        public IEnumerable<Project> Projects { get; }

        public ProjectsData(ProjectsDataDto dataDto)
        {
            Projects = dataDto.Projects.ToList(p => new Project(p));
        }
    }
}