﻿namespace Bizio.Client.Desktop.Model.Projects
{
    public enum StarCategory : byte
    {
        None = 0,
        ZeroStars,
        OneStars,
        TwoStars,
        ThreeStars,
        FourStars,
        FiveStars
    }
}