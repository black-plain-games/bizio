﻿using Bizio.Server.Model.Projects;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Client.Desktop.Model.Projects
{
    public class StaticProjectsData
    {
        public IEnumerable<ProjectDefinition> ProjectDefinitions { get; }

        public IReadOnlyDictionary<byte, string> ProjectStatuses { get; }

        public IReadOnlyDictionary<byte, string> ProjectRoles { get; }

        public IReadOnlyDictionary<byte, string> StarCategories { get; }

        public StaticProjectsData(StaticProjectsDataDto dataDto)
        {
            ProjectDefinitions = dataDto.ProjectDefinitions.Select(pd => new ProjectDefinition(pd));

            ProjectStatuses = dataDto.ProjectStatuses.ToDictionary(pr => pr.Id, pr => pr.Value);

            ProjectRoles = dataDto.ProjectRoles.ToDictionary(pr => pr.Id, pr => pr.Value);

            StarCategories = dataDto.StarCategories.ToDictionary(sc => sc.Id, sc => sc.Value);
        }
    }
}