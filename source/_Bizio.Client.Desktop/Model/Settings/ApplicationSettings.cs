﻿using BlackPlain.Wpf.Core;
using System;

namespace Bizio.Client.Desktop.Model.Settings
{
    public class ApplicationSettings : NotifyOnPropertyChanged
    {
        public UiScale UiScale
        {
            get
            {
                return GetField<UiScale>();
            }
            set
            {
                SetField(value);
            }
        }

        public string BaseCulture
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public string CurrencySymbol
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public DateTimeOffset LanguagesUpdatedDate
        {
            get
            {
                return GetField<DateTimeOffset>();
            }
            set
            {
                SetField(value);
            }
        }

        public EmailSettings EmailSettings
        {
            get
            {
                return GetField<EmailSettings>();
            }
            set
            {
                SetField(value);
            }
        }
    }
}