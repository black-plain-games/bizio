﻿namespace Bizio.Client.Desktop.Model.Settings
{
    public class UiScale
    {
        public double X { get; set; }

        public double Y { get; set; }

        public UiScale() { }

        public UiScale(double x, double y)
        {
            X = x;

            Y = y;
        }

        public static bool operator ==(UiScale a, UiScale b)
        {
            if (ReferenceEquals(a, b))
            {
                return true;
            }

            if ((a is null) || (b is null))
            {
                return false;
            }

            return a.Equals(b);
        }

        public static bool operator !=(UiScale a, UiScale b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            var uiScale = obj as UiScale;

            if (uiScale == null)
            {
                return false;
            }

            return Equals(uiScale);
        }

        private bool Equals(UiScale uiScale)
        {
            return
                X == uiScale.X &&
                Y == uiScale.Y;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public static readonly UiScale One = new UiScale(1, 1);
    }
}