﻿using Bizio.Client.Desktop.Core;
using Bizio.Client.Desktop.Model.Core;
using Bizio.Client.Desktop.Model.Settings;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Bizio.Client.Desktop.Services
{
    public class ApplicationSettingService : IApplicationSettingService
    {
        public event OnValueChanged<ApplicationSettings> SettingsSaved
        {
            add
            {
                _settingsSaved += value;
            }
            remove
            {
                _settingsSaved -= value;
            }
        }

        public ApplicationSettingService(IResourceClientService resourceClientService)
        {
            _serializer = new JsonSerializer
            {
                Formatting = Formatting.Indented
            };

            _resourceClientService = resourceClientService;
        }

        public ApplicationSettings GetSettings(bool reload)
        {
            if (_settings != null && !reload)
            {
                return _settings;
            }

            return _settings = JsonConvert.DeserializeObject<ApplicationSettings>(File.ReadAllText(Paths.SettingsPath));
        }

        public ApplicationSettings GetSettings()
        {
            return GetSettings(false);
        }

        public IDictionary<string, IDictionary<string, string>> GetLanguages()
        {
            var languagesUpdatedDate = _settings.LanguagesUpdatedDate;

            if (_settings.LanguagesUpdatedDate > DateTime.Today.AddDays(-1))
            {
                if (_languages == null)
                {
                    _languages = LoadLanguages();
                }

                if (_languages?.ContainsKey(UiText.DefaultLanguage) == true)
                {
                    return _languages;
                }
                else
                {
                    languagesUpdatedDate = DateTimeOffset.MinValue;
                }
            }

            var response = _resourceClientService.RetrieveLanguageData(languagesUpdatedDate);

            if (response.Code != ClientResponseCode.Succeeded ||
                response.Result.Languages == null ||
                !response.Result.Languages.Any())
            {
                if (_languages == null)
                {
                    _languages = LoadLanguages();
                }

                return _languages;
            }

            var languages = new Dictionary<string, IDictionary<string, string>>();

            foreach (var language in response.Result.Languages)
            {
                var languageData = new Dictionary<string, string>();

                foreach (var textResourceKey in response.Result.TextResourceKeys)
                {
                    if (language.Text.ContainsKey(textResourceKey.Key))
                    {
                        languageData.Add(textResourceKey.Value, language.Text[textResourceKey.Key]);
                    }
                }

                languages.Add(language.Iso2LetterCode, languageData);
            }

            _languages = languages;

            using (var writer = new StreamWriter(Paths.LanguagesPath))
            {
                _serializer.Serialize(writer, _languages);
            }
            _settings.LanguagesUpdatedDate = DateTimeOffset.Now;

            SaveSettings();

            return _languages;
        }

        public void SaveSettings()
        {
            var previousValue = _settings;

            using (var streamWriter = new StreamWriter(File.Open(Paths.SettingsPath, FileMode.Truncate)))
            {
                _serializer.Serialize(streamWriter, _settings);
            }

            _settingsSaved?.Invoke(this, new ValueChangedEventArgs<ApplicationSettings>(previousValue, _settings));
        }

        private IDictionary<string, IDictionary<string, string>> LoadLanguages()
        {
            try
            {
                var result = JsonConvert.DeserializeObject<IDictionary<string, IDictionary<string, string>>>(File.ReadAllText(Paths.LanguagesPath));

                return result ?? new Dictionary<string, IDictionary<string, string>>();
            }
            catch
            {
                return null;
            }
        }

        private ApplicationSettings _settings;

        private OnValueChanged<ApplicationSettings> _settingsSaved;

        private IDictionary<string, IDictionary<string, string>> _languages;

        private readonly JsonSerializer _serializer;

        private readonly IResourceClientService _resourceClientService;
    }
}