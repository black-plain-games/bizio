﻿using Bizio.Client.Desktop.Core;
using Bizio.Client.Desktop.Model.Core;
using Bizio.Client.Desktop.ViewModels;
using BlackPlain.Core.Web;
using System;
using System.Runtime.CompilerServices;

namespace Bizio.Client.Desktop.Services
{
    public class AuthorizationClientService : IAuthorizationClientService
    {
        public string UserName => _user?.UserName;

        public int? UserId => _user?.Id;

        public string AccessToken => "1234-abcd";

        public string EmailAddress => _user.EmailAddress;

        public string LastErrorMessage { get; private set; }

        public AuthorizationClientService(IDeveloperViewModel log)
        {
            _log = log;
        }

        public bool VerifyServerStatus()
        {
            return true;
            //try
            //{
            //    var response = _authorizationClient.UserClient.Ping("test");

            //    if (response.Code == ResponseCode.OK)
            //    {
            //        return true;
            //    }

            //    LogAuthenticationError(response);
            //}
            //catch (Exception err)
            //{
            //    LogException(err);
            //}

            //return false;
        }

        public bool Authorize()
        {
            RefreshUser();
            return true;
            //try
            //{
            //    var response = _authorizationClient.Authorize();

            //    switch (response.Code)
            //    {
            //        case ResponseCode.OK:
            //            return RefreshUser();

            //        case ResponseCode.Unauthorized:
            //        case ResponseCode.BadRequest:
            //            break;

            //        default:
            //            LogAuthenticationError(response);
            //            break;
            //    }
            //}
            //catch (Exception err)
            //{
            //    LogException(err);
            //}

            //return false;
        }

        public bool Register(CreateUserData createUserData)
        {
            RefreshUser();
            return true;
            //try
            //{
            //    var response = _authorizationClient.UserClient.Create(new BlackPlain.Authorization.Web.CreateUserRequest
            //    {
            //        UserName = createUserData.UserName,
            //        Password = createUserData.Password,
            //        EmailAddress = createUserData.EmailAddress,
            //        FirstName = createUserData.FirstName,
            //        LastName = createUserData.LastName,
            //        PhoneNumber = createUserData.PhoneNumber
            //    });

            //    if (response.Code != ResponseCode.OK)
            //    {
            //        LogAuthenticationError(response);

            //        return false;
            //    }

            //    return Login(createUserData.UserName, createUserData.Password);
            //}
            //catch (Exception err)
            //{
            //    LogException(err);
            //}

            //return false;
        }

        public bool Login(string username, string password)
        {
            RefreshUser();
            return true;
            //try
            //{
            //    var response = _authorizationClient.Login(username, password);

            //    if (response.Code != ResponseCode.OK)
            //    {
            //        LogAuthenticationError(response);

            //        return false;
            //    }

            //    RefreshUser();

            //    return true;
            //}
            //catch (Exception err)
            //{
            //    LogException(err);
            //}

            //return false;
        }

        public bool Logout()
        {
            return true;
            //try
            //{
            //    var response = _authorizationClient.Logout();

            //    if (response.Code == ResponseCode.OK)
            //    {
            //        _user = null;

            //        return true;
            //    }

            //    LogAuthenticationError(response);
            //}
            //catch (Exception err)
            //{
            //    LogException(err);
            //}

            //return false;
        }

        public bool GeneratePasswordResetToken(string username, string emailAddress)
        {
            return true;
            //try
            //{
            //    var response = _authorizationClient.UserClient.GeneratePasswordResetToken(new BlackPlain.Authorization.Web.GeneratePasswordResetTokenRequest
            //    {
            //        UserName = username,
            //        EmailAddress = emailAddress
            //    });

            //    if (response.Code == ResponseCode.OK)
            //    {
            //        return true;
            //    }

            //    LogAuthenticationError(response);
            //}
            //catch (Exception err)
            //{
            //    LogException(err);
            //}

            //return false;
        }

        public bool ResetPassword(string username, string token, string password)
        {
            return true;
            //try
            //{
            //    var response = _authorizationClient.UserClient.ResetPassword(new BlackPlain.Authorization.Web.ResetPasswordRequest
            //    {
            //        UserName = username,
            //        Token = token,
            //        Password = password
            //    });

            //    if (response.Code == ResponseCode.OK)
            //    {
            //        return true;
            //    }

            //    LogAuthenticationError(response);
            //}
            //catch (Exception err)
            //{
            //    LogException(err);
            //}

            //return false;
        }

        private bool RefreshUser()
        {
            _user = new User
            {
                Id = 700,
                UserName = "Fake User",
                EmailAddress = "wjcustode@gmail.com"
            };

            return true;
            //try
            //{
            //    var response = _authorizationClient.UserClient.Retrieve(_authorizationClient.AccessToken, _authorizationClient.UserId.Value);

            //    if (response.Code != ResponseCode.OK)
            //    {
            //        LogAuthenticationError(response);

            //        _user = null;

            //        return false;
            //    }

            //    _user = new User
            //    {
            //        Id = response.Content.Id,
            //        UserName = response.Content.UserName,
            //        EmailAddress = response.Content.EmailAddress
            //    };

            //    return true;
            //}
            //catch (Exception err)
            //{
            //    LogException(err);
            //}

            //_user = null;

            //return false;
        }

        private void LogAuthenticationError(Response response, [CallerMemberName]string caller = "")
        {
            LastErrorMessage = response.Message;

            _log.LogMessage("{1}{0}\t[{2}] {3}", Environment.NewLine, caller, response.Code, response.Message);
        }

        private void LogAuthenticationError<T>(Response<T> response, [CallerMemberName]string caller = "")
        {
            LastErrorMessage = response.Message;

            _log.LogMessage("{1}{0}\t[{2}] {3}", Environment.NewLine, caller, response.Code, response.Message);
        }

        private void LogException(Exception err, [CallerMemberName]string caller = "")
        {
            _log.LogMessage("{1}{0}\tException{0}\t\t{2}", Environment.NewLine, caller, err);
        }

        private User _user;

        private readonly ILog _log;
    }
}