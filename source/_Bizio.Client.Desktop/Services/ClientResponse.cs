﻿namespace Bizio.Client.Desktop.Services
{
    public class ClientResponse<T>
    {
        public ClientResponseCode Code { get; }

        public T Result { get; }

        public ClientResponse(ClientResponseCode code)
        {
            Code = code;
        }

        public ClientResponse(T result)
        {
            Code = ClientResponseCode.Succeeded;

            Result = result;
        }
    }
}