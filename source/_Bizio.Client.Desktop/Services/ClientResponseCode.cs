﻿namespace Bizio.Client.Desktop.Services
{
    public enum ClientResponseCode
    {
        Failed,
        Succeeded,
        Unauthorized
    }
}