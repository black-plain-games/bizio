﻿using System.ComponentModel;
using System.Net;
using System.Net.Mail;

namespace Bizio.Client.Desktop.Services
{
    public class EmailService : IEmailService
    {
        public EmailService(IApplicationSettingService applicationSettingService)
        {
            var settings = applicationSettingService.GetSettings();

            _client = new SmtpClient(settings.EmailSettings.SmtpClientHost, settings.EmailSettings.SmtpClientPort)
            {
                Credentials = new NetworkCredential(settings.EmailSettings.SmtpClientUsername, settings.EmailSettings.SmtpClientPassword),
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network
            };

            _sender = new MailAddress(settings.EmailSettings.EmailServiceFromAddress, settings.EmailSettings.EmailServiceDisplayName);

            _toEmailAddress = settings.EmailSettings.ReportIssueToEmailAddress;

            _client.SendCompleted += OnSendCompleted;
        }

        public void SendEmail(string subject, string body, string filename)
        {
            var email = new MailMessage
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true,
                DeliveryNotificationOptions = DeliveryNotificationOptions.OnSuccess,
                From = _sender,
                Sender = _sender
            };

            email.Attachments.Add(new Attachment(filename));

            email.To.Add(new MailAddress(_toEmailAddress));

            _client.Send(email);
        }

        private void OnSendCompleted(object sender, AsyncCompletedEventArgs e)
        {
        }

        private readonly SmtpClient _client;

        private readonly MailAddress _sender;

        private readonly string _toEmailAddress;
    }
}
