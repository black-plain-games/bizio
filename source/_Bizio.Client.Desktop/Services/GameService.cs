﻿using Bizio.Client.Desktop.Core;
using Bizio.Client.Desktop.Model.Core;
using Bizio.Client.Desktop.Model.Game.ActionData;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Bizio.Client.Desktop.Services
{
    public class GameService : IGameService
    {
        public IEnumerable<IActionData> Load(int gameId)
        {
            if (!File.Exists(Paths.SavedGamesPath))
            {
                using (var stream = File.Create(Paths.SavedGamesPath))
                using (var streamWriter = new StreamWriter(stream))
                {
                    streamWriter.Write("{}");
                }
            }

            var savedGames = JsonConvert.DeserializeObject<IDictionary<int, IEnumerable<SavedActionData>>>(File.ReadAllText(Paths.SavedGamesPath));

            if (!savedGames.ContainsKey(gameId))
            {
                return Enumerable.Empty<IActionData>();
            }

            var output = new List<IActionData>();

            foreach (var actionData in savedGames[gameId])
            {
                switch ((ActionType)actionData.ActionTypeId)
                {
                    case ActionType.AcceptProject:
                        output.Add(new AcceptProjectActionData(actionData.Data));
                        break;

                    case ActionType.AdjustAllocation:
                        output.Add(new AdjustAllocationActionData(actionData.Data));
                        break;

                    case ActionType.AdjustSalary:
                        output.Add(new AdjustSalaryActionData(actionData.Data));
                        break;

                    case ActionType.ChangeMessageStatus:
                        output.Add(new ChangeCompanyMessageStatusActionData(actionData.Data));
                        break;

                    case ActionType.FireEmployee:
                        output.Add(new FireEmployeeActionData(actionData.Data));
                        break;

                    case ActionType.InterviewProspect:
                        output.Add(new InterviewProspectActionData(actionData.Data));
                        break;

                    case ActionType.MakeOffer:
                        output.Add(new MakeOfferActionData(actionData.Data));
                        break;

                    case ActionType.PurchasePerk:
                        output.Add(new PurchasePerkActionData(actionData.Data));
                        break;

                    case ActionType.RecruitPerson:
                        output.Add(new RecruitPersonActionData(actionData.Data));
                        break;

                    case ActionType.RequestExtension:
                        output.Add(new RequestExtensionTurnActionData(actionData.Data));
                        break;

                    case ActionType.SellPerk:
                        output.Add(new SellPerkActionData(actionData.Data));
                        break;

                    case ActionType.SubmitProject:
                        output.Add(new SubmitProjectTurnActionData(actionData.Data));
                        break;

                    default:
                        throw new ArgumentException(nameof(actionData.ActionTypeId));
                }
            }

            savedGames.Remove(gameId);

            Save(savedGames);

            return output;
        }

        private static IDictionary<Panel, byte> GetEmptyUpdates() => new Dictionary<Panel, byte>
        {
            { Panel.Mailbox, 0 },
            { Panel.OtherCompanies, 0 },
            { Panel.People, 0 },
            { Panel.Projects, 0 },
            { Panel.Store, 0 },
            { Panel.UserCompany, 0 }
        };

        public IDictionary<Panel, byte> LoadUpdates(int gameId)
        {
            if (!File.Exists(Paths.SavedGamesUpdatesPath))
            {
                using (var stream = File.Create(Paths.SavedGamesUpdatesPath))
                using (var streamWriter = new StreamWriter(stream))
                {
                    streamWriter.Write("{}");
                }

                return GetEmptyUpdates();
            }

            var savedGames = JsonConvert.DeserializeObject<IDictionary<int, IDictionary<Panel, byte>>>(File.ReadAllText(Paths.SavedGamesUpdatesPath));

            if (!savedGames.ContainsKey(gameId))
            {
                return GetEmptyUpdates();
            }

            return savedGames[gameId];
        }

        public void Save(int gameId, IEnumerable<IActionData> turnActions)
        {
            var savedGames = JsonConvert.DeserializeObject<IDictionary<int, IEnumerable<SavedActionData>>>(File.ReadAllText(Paths.SavedGamesPath));

            savedGames[gameId] = turnActions.Select(ta => new SavedActionData
            {
                ActionTypeId = ta.ActionTypeId,
                Data = ta.GetData()
            });

            Save(savedGames);
        }

        public void Save(int gameId, IDictionary<Panel, byte> updates)
        {
            var savedGames = JsonConvert.DeserializeObject<IDictionary<int, IDictionary<Panel, byte>>>(File.ReadAllText(Paths.SavedGamesUpdatesPath));

            savedGames[gameId] = updates;

            Save(savedGames);
        }

        private void Save(IDictionary<int, IEnumerable<SavedActionData>> games)
        {
            File.WriteAllText(Paths.SavedGamesPath, JsonConvert.SerializeObject(games));
        }

        private void Save(IDictionary<int, IDictionary<Panel, byte>> games)
        {
            File.WriteAllText(Paths.SavedGamesUpdatesPath, JsonConvert.SerializeObject(games));
        }
    }
}