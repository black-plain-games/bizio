﻿using Bizio.Client.Desktop.Model.Core;
using Bizio.Client.Desktop.Model.Settings;
using System.Collections.Generic;

namespace Bizio.Client.Desktop.Services
{
    public interface IApplicationSettingService
    {
        event OnValueChanged<ApplicationSettings> SettingsSaved;

        ApplicationSettings GetSettings();

        ApplicationSettings GetSettings(bool reload);

        IDictionary<string, IDictionary<string, string>> GetLanguages();

        void SaveSettings();
    }
}