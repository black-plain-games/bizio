﻿using Bizio.Client.Desktop.Model.Core;
using System;

namespace Bizio.Client.Desktop.Services
{
    public interface IAuthorizationClientService
    {
        int? UserId { get; }

        string UserName { get; }

        string EmailAddress { get; }

        string AccessToken { get; }

        string LastErrorMessage { get; }

        bool VerifyServerStatus();

        bool Authorize();

        bool Login(string username, string password);

        bool Logout();

        bool Register(CreateUserData createUserData);

        bool GeneratePasswordResetToken(string username, string emailAddress);

        bool ResetPassword(string username, string token, string password);
    }
}