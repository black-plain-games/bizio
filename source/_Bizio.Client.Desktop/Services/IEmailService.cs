﻿namespace Bizio.Client.Desktop.Services
{
    public interface IEmailService
    {
        void SendEmail(string subject, string body, string filename);
    }
}
