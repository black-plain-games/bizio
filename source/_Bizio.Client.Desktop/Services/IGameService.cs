﻿using Bizio.Client.Desktop.Model.Core;
using Bizio.Client.Desktop.Model.Game.ActionData;
using System.Collections.Generic;

namespace Bizio.Client.Desktop.Services
{
    public interface IGameService
    {
        IEnumerable<IActionData> Load(int gameId);

        IDictionary<Panel, byte> LoadUpdates(int gameId);

        void Save(int gameId, IEnumerable<IActionData> turnActions);

        void Save(int gameId, IDictionary<Panel, byte> updates);
    }
}