﻿using Bizio.Client.Desktop.Model;
using System.Collections.Generic;

namespace Bizio.Client.Desktop.Services
{
    public interface INotificationService
    {
        IEnumerable<Notification> RefreshNotifications(int userId);

        void SaveNotifications(int userId, IEnumerable<Notification> notifications);
    }
}