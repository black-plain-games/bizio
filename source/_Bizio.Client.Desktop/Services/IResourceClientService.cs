﻿using Bizio.Client.Desktop.Model;
using Bizio.Client.Desktop.Model.Core;
using Bizio.Client.Desktop.Model.Game;
using Bizio.Client.Desktop.Model.Game.ActionData;
using System;
using System.Collections.Generic;

namespace Bizio.Client.Desktop.Services
{
    public interface IResourceClientService
    {
        bool VerifyServerStatus();

        ClientResponse<int> CreateNewGame(NewGameData data);

        ClientResponse<GameData> FetchGameData(int gameId);

        ClientResponse<StaticData> FetchStaticGameData();

        ClientResponse<bool> ProcessTurn(int gameId, IEnumerable<IActionData> actionData);

        ClientResponse<IEnumerable<Game>> RetrieveGames();

        ClientResponse<IEnumerable<Notification>> RetrieveNotifications(int? minimumId);

        ClientResponse<LanguageData> RetrieveLanguageData(DateTimeOffset minimumDateModified);
    }
}