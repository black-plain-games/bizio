﻿using Bizio.Client.Desktop.Model.Core;
using Bizio.Client.Desktop.ViewModels.Popups;
using System;
using System.Windows;

namespace Bizio.Client.Desktop.Services
{
    public delegate void OnPopupChanged(object sender, PopupChangedEventArgs e);

    public interface IViewStateService
    {
        event OnPopupChanged PopupChanged;

        event EventHandler PanelClosed;

        IPopup CurrentPopup { get; }

        void NavigateToMainMenuView();

        void NavigateToLoadGameView();

        void NavigateToNewGameView();

        void NavigateToSettingsView();

        void NavigateToNotificationsView();

        void NavigateToGameView(int gameId);

        void ClosePanel();

        void PushPopup<TView>(IPopupViewModel viewModel)
            where TView : FrameworkElement, new();

        void PushPopup<TView>(IPopupViewModel viewModel, double maxWidth, double maxHeight)
            where TView : FrameworkElement, new();
        void PushPopup<TView>(IPopupViewModel viewModel, HorizontalAlignment horizontalAlignment)
            where TView : FrameworkElement, new();

        void PushPopup(IPopup popup);

        void PushPopup(IPopup popup, HorizontalAlignment horizontalAlignment);

        void PushPopup(IPopup popup, VerticalAlignment verticalAlignment);

        void PushPopup(IPopup popup, HorizontalAlignment horizontalAlignment, VerticalAlignment verticalAlignment);

        void PopPopup();

        void RemovePopup(IPopupViewModel popupViewModel);

        string SaveScreenshot();
    }
}