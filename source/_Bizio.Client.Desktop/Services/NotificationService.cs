﻿using Bizio.Client.Desktop.Core;
using Bizio.Client.Desktop.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Bizio.Client.Desktop.Services
{
    public class NotificationService : INotificationService
    {
        public NotificationService(IResourceClientService resourceClientService)
        {
            _resourceClientService = resourceClientService;

            _lastUpdateDate = null;
        }

        public IEnumerable<Notification> RefreshNotifications(int userId)
        {
            var allNotifications = JsonConvert.DeserializeObject<IDictionary<int, IEnumerable<Notification>>>(File.ReadAllText(Paths.NotificationsPath));

            var notifications = Enumerable.Empty<Notification>();

            if (allNotifications.ContainsKey(userId))
            {
                notifications = allNotifications[userId];
            }

            if (_lastRefreshUserId.HasValue && _lastRefreshUserId == userId &&
                _lastUpdateDate.HasValue && _lastUpdateDate > DateTimeOffset.Now.AddMinutes(-30))
            {
                return notifications;
            }

            int? minimumId = null;

            if (notifications.Any())
            {
                minimumId = notifications.Max(n => n.Id);
            }

            var newNotifications = _resourceClientService.RetrieveNotifications(minimumId);

            if (newNotifications.Code == ClientResponseCode.Succeeded && newNotifications.Result.Any())
            {
                allNotifications[userId] = notifications = notifications.Concat(newNotifications.Result);

                File.WriteAllText(Paths.NotificationsPath, JsonConvert.SerializeObject(allNotifications));
            }

            _lastUpdateDate = DateTimeOffset.Now;

            _lastRefreshUserId = userId;

            return notifications;
        }

        public void SaveNotifications(int userId, IEnumerable<Notification> notifications)
        {
            var allNotifications = JsonConvert.DeserializeObject<IDictionary<int, IEnumerable<Notification>>>(File.ReadAllText(Paths.NotificationsPath));

            allNotifications[userId] = notifications;

            File.WriteAllText(Paths.NotificationsPath, JsonConvert.SerializeObject(allNotifications));
        }

        private int? _lastRefreshUserId;

        private DateTimeOffset? _lastUpdateDate;

        private readonly IResourceClientService _resourceClientService;
    }
}