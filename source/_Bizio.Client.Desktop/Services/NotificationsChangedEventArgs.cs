﻿using System;

namespace Bizio.Client.Desktop.Services
{
    public class NotificationsChangedEventArgs : EventArgs
    {
        public int UnreadNotificationsCount { get; }

        public NotificationsChangedEventArgs(int unreadNotificationsCount)
        {
            UnreadNotificationsCount = unreadNotificationsCount;
        }
    }
}