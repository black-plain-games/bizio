﻿using Microsoft.Extensions.Options;
using Microsoft.Win32;

namespace Bizio.Client.Desktop.Services
{
    public class RegistryOptions : IOptions<BizioClientOptions>
    {
        public BizioClientOptions Value
        {
            get
            {
                return new BizioClientOptions
                {
                    BizioBaseUrl = $"{Registry.GetValue(RegistryRoot, BizioBaseUrlKey, null)}"
                };
            }
        }

        private const string BizioBaseUrlKey = "BizioBaseUrl";

        private const string RegistryRoot = "HKEY_CURRENT_USER\\SOFTWARE\\Black Plain Games\\Bizio";
    }
}