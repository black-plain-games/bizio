﻿using Bizio.Client.Desktop.Core;
using Bizio.Client.Desktop.Model;
using Bizio.Client.Desktop.Model.Core;
using Bizio.Client.Desktop.Model.Game;
using Bizio.Client.Desktop.Model.Game.ActionData;
using Bizio.Client.Desktop.ViewModels;
using Bizio.Server.Model.Core;
using Bizio.Server.Model.Game;
using Bizio.Server.Model.People;
using BlackPlain.Core.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Bizio.Client.Desktop.Services
{
    public class ResourceClientService : IResourceClientService
    {
        public ResourceClientService(
            IDeveloperViewModel log,
            IBizioClient bizioClient,
            IAuthorizationClientService authorizationClientService)
        {
            _log = log;

            _bizioClient = bizioClient;

            _authorizationClientService = authorizationClientService;
        }

        public bool VerifyServerStatus()
        {
            try
            {
                var response = _bizioClient.Ping("test");

                if (IsOk(response))
                {
                    return true;
                }

                LogError(response);
            }
            catch (Exception err)
            {
                LogException(err);
            }

            return false;
        }

        public ClientResponse<StaticData> FetchStaticGameData()
        {
            if (_staticData != null &&
                _lastStaticDataRefreshDate.HasValue &&
                _lastStaticDataRefreshDate.Value > DateTimeOffset.Now.Add(_refreshStaticDataTimeSpan))
            {
                return new ClientResponse<StaticData>(_staticData);
            }

            return MakeAuthorizedRequest(_bizioClient.Data.RetrieveStaticData, UpdateStaticData);
        }

        private StaticData UpdateStaticData(Response<ApiResponseMessage<StaticDataDto>> response)
        {
            _lastStaticDataRefreshDate = DateTimeOffset.Now;

            return Utilities.StaticData = _staticData = new StaticData(response.Content.Data);
        }

        public ClientResponse<int> CreateNewGame(NewGameData data)
        {
            var dataDto = new NewGameDto
            {
                CompanyName = data.CompanyName,
                IndustryId = data.IndustryId,
                InitialFunds = data.InitialFunds,
                NumberOfCompanies = data.NumberOfCompanies,
                StartDate = data.StartDate,
                WeeksPerTurn = data.WeeksPerTurn,
                Founder = new CreatePersonDto
                {
                    Birthday = data.Founder.Birthday,
                    FirstName = data.Founder.FirstName,
                    Gender = data.Founder.Gender,
                    LastName = data.Founder.LastName,
                    PersonalityId = data.Founder.PersonalityId,
                    Skills = data.Founder.MandatorySkills.Concat(data.Founder.OptionalSkills).Select(s => new IdValuePairDto<byte, decimal>((byte)s.Id.Id, s.Value))
                }
            };

            return MakeAuthorizedRequest(at => _bizioClient.Games.Create(at, dataDto), r =>
            {
                return r.Content.Data;
            });
        }

        public ClientResponse<GameData> FetchGameData(int gameId)
        {
            return MakeAuthorizedRequest(token => _bizioClient.Games.Retrieve(token, gameId), r =>
            {
                var gameData = new GameData(r.Content.Data);

                return Utilities.GameData = gameData;
            });
        }

        public ClientResponse<bool> ProcessTurn(int gameId, IEnumerable<IActionData> actionData)
        {
            var processTurnDto = new ProcessTurnDto
            {
                UserId = _authorizationClientService.UserId.Value,
                Actions = actionData.Select(a => new ActionDataDto
                {
                    ActionTypeId = a.ActionTypeId,
                    Data = a.GetData()
                })
            };

            return MakeAuthorizedRequest(token => _bizioClient.Games.Update(token, gameId, processTurnDto), r => true);
        }

        public ClientResponse<IEnumerable<Game>> RetrieveGames()
        {
            return MakeAuthorizedRequest(token => _bizioClient.Games.Retrieve(token), r => r.Content.Data.Select(g => new Game(g)));
        }

        public ClientResponse<IEnumerable<Notification>> RetrieveNotifications(int? minimumId)
        {
            return MakeAuthorizedRequest(token => _bizioClient.Notifications.Retrieve(token, minimumId), r => r.Content.Data.Select(n => new Notification(n)));
        }

        public ClientResponse<LanguageData> RetrieveLanguageData(DateTimeOffset minimumDateModified)
        {
            return new ClientResponse<LanguageData>(ClientResponseCode.Failed);
            //return MakeRequest(() => _bizioClient.Data.RetrieveLanguageData(minimumDateModified), r => new LanguageData(r.Content.Data));
        }

        private ClientResponse<TResult> MakeAuthorizedRequest<TResult, TResponse>(Func<string, Response<TResponse>> target, Func<Response<TResponse>, TResult> processResponse)
            where TResponse : ApiResponseMessage
        {
            try
            {
                var response = target(_authorizationClientService.AccessToken);

                if (response.Code == ResponseCode.Unauthorized)
                {
                    if (_authorizationClientService.Authorize())
                    {
                        response = target(_authorizationClientService.AccessToken);
                    }
                    else
                    {
                        return new ClientResponse<TResult>(ClientResponseCode.Unauthorized);
                    }
                }

                if (IsOk(response))
                {
                    return new ClientResponse<TResult>(processResponse(response));
                }

                LogError(response);
            }
            catch (Exception err)
            {
                LogException(err);
            }

            return new ClientResponse<TResult>(ClientResponseCode.Failed);
        }

        private ClientResponse<TResult> MakeRequest<TResult, TResponse>(Func<Response<TResponse>> target, Func<Response<TResponse>, TResult> processResponse)
            where TResponse : ApiResponseMessage
        {
            try
            {
                var response = target();

                if (IsOk(response))
                {
                    return new ClientResponse<TResult>(processResponse(response));
                }

                LogError(response);
            }
            catch (Exception err)
            {
                LogException(err);
            }

            return new ClientResponse<TResult>(ClientResponseCode.Failed);
        }

        private bool IsOk<TApiResponseMessage>(Response<TApiResponseMessage> response)
            where TApiResponseMessage : ApiResponseMessage
        {
            return response.Code == ResponseCode.OK &&
                response.Content != null &&
                response.Content.Exception == null &&
                (response.Content.ValidationErrors == null || !response.Content.ValidationErrors.Any());
        }

        private void LogException(Exception err, [CallerMemberName]string caller = "")
        {
            _log.LogMessage("{1}{0}\tException{0}\t\t{2}", Environment.NewLine, caller, err);
        }

        private void LogError<TApiResponseMessage>(Response<TApiResponseMessage> response, [CallerMemberName]string caller = "")
            where TApiResponseMessage : ApiResponseMessage
        {
            var validationErrors = string.Empty;

            Exception err = null;

            if (response.Content != null)
            {
                validationErrors = response.Content.ValidationErrors.Join(Environment.NewLine + "\t\t");

                err = response.Content.Exception;
            }

            var message = caller + Environment.NewLine +
                $"\tResponse Code: {response.Code}" + Environment.NewLine +
                $"\tResponse Message: {response.Message}" + Environment.NewLine +
                $"\tValidation Errors" + Environment.NewLine +
                $"\t\t{validationErrors}" + Environment.NewLine +
                $"\tException: {err?.Message}" + Environment.NewLine +
                $"\tInner Exception: {err?.InnerException?.Message}" + Environment.NewLine +
                $"\tStack Trace: {err?.StackTrace}";

            _log.LogMessage(message);
        }

        private StaticData _staticData;

        private DateTimeOffset? _lastStaticDataRefreshDate;

        private readonly IAuthorizationClientService _authorizationClientService;

        private readonly IBizioClient _bizioClient;

        private readonly ILog _log;

        private static readonly TimeSpan _refreshStaticDataTimeSpan = TimeSpan.FromHours(-4);
    }
}