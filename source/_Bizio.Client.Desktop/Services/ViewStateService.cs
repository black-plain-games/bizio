﻿using Bizio.Client.Desktop.Model.Core;
using Bizio.Client.Desktop.ViewModels;
using Bizio.Client.Desktop.ViewModels.Popups;
using Bizio.Client.Desktop.Views;
using BlackPlain.DI;
using BlackPlain.Wpf.Core;
using System;
using System.IO;
using System.Windows;
using Forms = System.Windows.Forms;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Bizio.Client.Desktop.Services
{
    public class ViewStateService : IViewStateService
    {
        public event OnPopupChanged PopupChanged;

        public event EventHandler PanelClosed;

        public IPopup CurrentPopup => _popupPresenterViewModel.CurrentPopup;

        public ViewStateService(
            ILazyResolution<IViewStateRoot> lazyViewStateRoot,
            ILazyResolution<IMainMenuView> lazyMainMenuView,
            ILazyResolution<IMainMenuViewModel> lazyMainMenuViewModel,
            ILazyResolution<ILoadGameView> lazyLoadGameView,
            ILazyResolution<ILoadGameViewModel> lazyLoadGameViewModel,
            ILazyResolution<INewGameView> lazyNewGameView,
            ILazyResolution<INewGameViewModel> lazyNewGameViewModel,
            ILazyResolution<IGameLayoutView> lazyGameView,
            ILazyResolution<IGameViewModel> lazyGameViewModel,
            ILazyResolution<ISettingsView> lazySettingsView,
            ILazyResolution<ISettingsViewModel> lazySettingsViewModel,
            ILazyResolution<INotificationsView> lazyNotificationsView,
            ILazyResolution<INotificationsViewModel> lazyNotificationsViewModel,
            IPopupPresenterViewModel popupPresenterViewModel)
        {
            _lazyViewStateRoot = lazyViewStateRoot;

            _lazyMainMenuView = lazyMainMenuView;

            _lazyMainMenuViewModel = lazyMainMenuViewModel;

            _lazyLoadGameView = lazyLoadGameView;

            _lazyLoadGameViewModel = lazyLoadGameViewModel;

            _lazyNewGameView = lazyNewGameView;

            _lazyNewGameViewModel = lazyNewGameViewModel;

            _lazyGameView = lazyGameView;

            _lazyGameViewModel = lazyGameViewModel;

            _lazySettingsView = lazySettingsView;

            _lazySettingsViewModel = lazySettingsViewModel;

            _popupPresenterViewModel = popupPresenterViewModel;

            _lazyNotificationsView = lazyNotificationsView;

            _lazyNotificationsViewModel = lazyNotificationsViewModel;

            _popupPresenterViewModel.PopupChanged += OnPopupChanged;
        }

        public void NavigateToMainMenuView() => UIManager.TryInvokeOnUIThread(() => NavigateTo(_lazyMainMenuView, _lazyMainMenuViewModel));

        public void NavigateToNewGameView() => UIManager.TryInvokeOnUIThread(() => NavigateTo(_lazyNewGameView, _lazyNewGameViewModel));

        public void NavigateToLoadGameView() => UIManager.TryInvokeOnUIThread(() => NavigateTo(_lazyLoadGameView, _lazyLoadGameViewModel));

        public void NavigateToSettingsView() => UIManager.TryInvokeOnUIThread(() => NavigateTo(_lazySettingsView, _lazySettingsViewModel));

        public void NavigateToNotificationsView() => UIManager.TryInvokeOnUIThread(() => NavigateTo(_lazyNotificationsView, _lazyNotificationsViewModel));

        public void NavigateToGameView(int gameId)
        {
            UIManager.TryInvokeOnUIThread(() =>
            {
                var viewStateRoot = _lazyViewStateRoot.Instance;

                viewStateRoot.CurrentView = _lazyGameView.Instance as FrameworkElement;

                var viewModel = _lazyGameViewModel.Instance;

                viewModel.GameId = gameId;

                viewStateRoot.CurrentViewModel = viewModel;
            });
        }

        private void NavigateTo<TView, TViewModel>(ILazyResolution<TView> lazyView, ILazyResolution<TViewModel> lazyViewModel)
            where TView : class
            where TViewModel : class
        {
            var viewStateRoot = _lazyViewStateRoot.Instance;

            viewStateRoot.CurrentView = lazyView.Instance as FrameworkElement;

            viewStateRoot.CurrentViewModel = lazyViewModel.Instance;
        }

        public void PushPopup<TView>(IPopupViewModel viewModel) where TView : FrameworkElement, new() => PushPopup<TView>(viewModel, double.PositiveInfinity, double.PositiveInfinity);

        public void PushPopup<TView>(IPopupViewModel viewModel, double maxWidth, double maxHeight)
            where TView : FrameworkElement, new()
        {
            UIManager.TryInvokeOnUIThread(() =>
            {
                PushPopup(new Popup<IPopupViewModel>(new TView
                {
                    MaxWidth = maxWidth,
                    MaxHeight = maxHeight
                }, viewModel));
            });
        }
        public void PushPopup<TView>(IPopupViewModel viewModel, HorizontalAlignment horizontalAlignment)
            where TView : FrameworkElement, new()
        {
            UIManager.TryInvokeOnUIThread(() =>
            {
                PushPopup(new Popup<IPopupViewModel>(new TView(), viewModel), horizontalAlignment);
            });
        }

        public void PushPopup(IPopup popup) => _popupPresenterViewModel.PushPopup(popup);

        public void PushPopup(IPopup popup, HorizontalAlignment horizontalAlignment) => _popupPresenterViewModel.PushPopup(popup, horizontalAlignment);

        public void PushPopup(IPopup popup, VerticalAlignment verticalAlignment) => _popupPresenterViewModel.PushPopup(popup, verticalAlignment);

        public void PushPopup(IPopup popup, HorizontalAlignment horizontalAlignment, VerticalAlignment verticalAlignment) => _popupPresenterViewModel.PushPopup(popup, horizontalAlignment, verticalAlignment);

        public void PopPopup() => _popupPresenterViewModel.PopPopup();

        public void RemovePopup(IPopupViewModel popupViewModel) => _popupPresenterViewModel.RemovePopup(popupViewModel);

        private void OnPopupChanged(object sender, PopupChangedEventArgs args)
        {
            PopupChanged?.Invoke(sender, args);
        }

        public void ClosePanel() => PanelClosed?.Invoke(this, EventArgs.Empty);

        public string SaveScreenshot()
        {
            var filename = $"screenshot_{Guid.NewGuid():N}.png";

            var element = _lazyViewStateRoot.Instance.CurrentView;

            while (element != null && element.Name != "DisplayRoot")
            {
                element = VisualTreeHelper.GetParent(element) as FrameworkElement;
            }

            if(element == null)
            {
                return null;
            }

            var bitmap = new RenderTargetBitmap((int)element.ActualWidth, (int)element.ActualHeight, 120, 120, PixelFormats.Pbgra32);

            bitmap.Render(element);

            var encoder = new PngBitmapEncoder();

            encoder.Frames.Add(BitmapFrame.Create(bitmap));

            using (var stream = File.OpenWrite(filename))
            {
                encoder.Save(stream);
            }

            return filename;
        }

        private readonly ILazyResolution<IViewStateRoot> _lazyViewStateRoot;

        private readonly ILazyResolution<IMainMenuView> _lazyMainMenuView;

        private readonly ILazyResolution<IMainMenuViewModel> _lazyMainMenuViewModel;

        private readonly ILazyResolution<ILoadGameView> _lazyLoadGameView;

        private readonly ILazyResolution<ILoadGameViewModel> _lazyLoadGameViewModel;

        private readonly ILazyResolution<INewGameView> _lazyNewGameView;

        private readonly ILazyResolution<INewGameViewModel> _lazyNewGameViewModel;

        private readonly ILazyResolution<IGameLayoutView> _lazyGameView;

        private readonly ILazyResolution<IGameViewModel> _lazyGameViewModel;

        private readonly ILazyResolution<ISettingsView> _lazySettingsView;

        private readonly ILazyResolution<ISettingsViewModel> _lazySettingsViewModel;

        private readonly ILazyResolution<INotificationsView> _lazyNotificationsView;

        private readonly ILazyResolution<INotificationsViewModel> _lazyNotificationsViewModel;

        private readonly IPopupPresenterViewModel _popupPresenterViewModel;
    }
}