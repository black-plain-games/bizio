﻿using Bizio.Client.Desktop.Core;
using Bizio.Client.Desktop.Model.Companies;
using Bizio.Client.Desktop.Model.Core;
using Bizio.Client.Desktop.Model.Game;
using Bizio.Client.Desktop.Model.Game.ActionData;
using Bizio.Client.Desktop.Model.People;
using Bizio.Client.Desktop.Model.Projects;
using Bizio.Client.Desktop.Services;
using Bizio.Client.Desktop.ViewModels.Popups;
using Bizio.Client.Desktop.Views.Popups;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bizio.Client.Desktop.ViewModels
{
    public class AllocationPopupManager
    {
        private readonly IViewStateService _viewStateService;

        private readonly GameData _data;

        private readonly Company _userCompany;

        private readonly IReadOnlyDictionary<byte, string> _roles;

        private readonly IEnumerable<Allocation> _allocations;

        private readonly ICollection<IActionData> _turnActions;

        private readonly Person _person;

        private readonly Project _project;

        private readonly Allocation _allocation;

        public event EventHandler OnAllocationsChanged;

        public event EventHandler OnTurnActionsChanged;

        public AllocationPopupManager(IViewStateService viewStateService,
            GameData data,
            Company userCompany,
            IReadOnlyDictionary<byte, string> roles,
            IEnumerable<Allocation> allocations,
            ICollection<IActionData> turnActions,
            Person person)
            : this(viewStateService, data, userCompany, roles, allocations, turnActions)
        {
            _person = person;
        }

        public AllocationPopupManager(IViewStateService viewStateService,
            GameData data,
            Company userCompany,
            IReadOnlyDictionary<byte, string> roles,
            IEnumerable<Allocation> allocations,
            ICollection<IActionData> turnActions,
            Project project)
            : this(viewStateService, data, userCompany, roles, allocations, turnActions)
        {
            _project = project;
        }

        public AllocationPopupManager(IViewStateService viewStateService,
            GameData data,
            Company userCompany,
            IReadOnlyDictionary<byte, string> roles,
            IEnumerable<Allocation> allocations,
            ICollection<IActionData> turnActions,
            Allocation allocation)
            : this(viewStateService, data, userCompany, roles, allocations, turnActions)
        {
            _allocation = allocation;

            if (_allocation == null)
            {
                return;
            }

            _person = FindPerson(allocation.PersonId);

            _project = _userCompany.Projects.First(p => p.Id == allocation.ProjectId);
        }

        protected AllocationPopupManager(IViewStateService viewStateService,
            GameData data,
            Company userCompany,
            IReadOnlyDictionary<byte, string> roles,
            IEnumerable<Allocation> allocations,
            ICollection<IActionData> turnActions)
        {
            _viewStateService = viewStateService;

            _data = data;

            _userCompany = userCompany;

            _roles = roles;

            _allocations = allocations;

            _turnActions = turnActions;
        }

        private IEnumerable<Person> People => _userCompany?.Employees?.Select(e => FindPerson(e.PersonId));

        private Person FindPerson(int personId)
        {
            return _data.PeopleData.People.First(p => p.Id == personId);
        }

        private IEnumerable<AdjustAllocationActionData> TurnActions => _turnActions.Where(ta => ta is AdjustAllocationActionData).Cast<AdjustAllocationActionData>();

        public void AddAllocation()
        {
            var people = _userCompany.Employees
                .Where(e => !_allocations.Any(a => a.PersonId == e.PersonId))
                .Select(p => FindPerson(p.PersonId));

            EditAllocationPopupViewModel viewModel;

            if (_person == null)
            {
                viewModel = new EditAllocationPopupViewModel(_viewStateService, _roles, _allocations, TurnActions, _project, People);
            }
            else if (_project == null)
            {
                viewModel = new EditAllocationPopupViewModel(_viewStateService, _roles, _allocations, TurnActions, _person, _userCompany.Projects.Where(p => !_allocations.Any(a => a.ProjectId == p.Id && a.Percent > 0 && a.PersonId == _person.Id)));
            }
            else
            {
                throw new InvalidOperationException("Cannot add an allocation if the manager was initialized with an allocation");
            }

            DisplayEditAllocationPopup(viewModel);
        }

        public void EditAllocation()
        {
            if (_allocation == null)
            {
                throw new InvalidOperationException("Cannot edit an allocation if the manager was initialized without an allocation");
            }
            var viewModel = new EditAllocationPopupViewModel(_viewStateService, _roles, _allocations, TurnActions, _person, _project, _allocation.RoleId, _allocation.Percent);

            DisplayEditAllocationPopup(viewModel);
        }

        public bool CanAddAllocation()
        {
            if (_project != null)
            {
                var people = People.Where(p =>
                !_allocations.Any(a => a.PersonId == p.Id && a.ProjectId == _project.Id) &&
                !TurnActions.Any(ta => ta.Data.PersonId == p.Id && ta.Data.ProjectId == _project.Id && ta.Data.Percentage > 0));

                return _data != null &&
                    _userCompany != null &&
                    _project != null &&
                    people.Any() == true &&
                    Utilities.CanCompanyPerformActionType(_userCompany, ActionType.AdjustAllocation, _turnActions) &&
                    (_project.SubmissionStatus == ProjectSubmissionStatus.InProgress ||
                    _project.SubmissionStatus == ProjectSubmissionStatus.RequestingExtension);
            }

            if (_person != null)
            {
                var total = _allocations.Where(a => a.PersonId == _person.Id).Sum(a => a.Percent);

                if (byte.MaxValue - 1 <= total)
                {
                    return false;
                }

                return _userCompany.Projects.Any(p => !_allocations.Any(a => a.ProjectId == p.Id && a.Percent > 0 && a.PersonId == _person.Id));
            }

            return false;
        }

        public bool CanEditAllocation()
        {
            return _data != null &&
                _userCompany != null &&
                _allocation != null &&
                _project != null &&
                (Utilities.CanCompanyPerformActionType<AdjustAllocationActionData>(_userCompany, ActionType.AdjustAllocation, _turnActions, ta => ta.Data.PersonId == _allocation.PersonId && ta.Data.ProjectId == _allocation.ProjectId) ||
                _data.NewAllocations.Any(a => a.ProjectId == _allocation.ProjectId && a.PersonId == _allocation.PersonId)) &&
                (_project.SubmissionStatus == ProjectSubmissionStatus.InProgress || _project.SubmissionStatus == ProjectSubmissionStatus.RequestingExtension);
        }

        public void DeleteAllocation()
        {
            var actionData = TurnActions.FirstOrDefault(a => a.Data.PersonId == _allocation.PersonId && a.Data.ProjectId == _allocation.ProjectId);

            if (actionData != null)
            {
                actionData.Cancel();

                return;
            }

            if (!_data.NewAllocations.Contains(_allocation))
            {
                _allocation.Percent = 0;

                actionData = new AdjustAllocationActionData(_allocation.PersonId, _allocation.ProjectId, _allocation.Percent, _allocation.RoleId);

                actionData.Cancelled += (s, e) =>
                {
                    _turnActions.Remove(actionData);

                    _allocation.Percent = _allocation.OriginalPercent;

                    _allocation.RoleId = _allocation.OriginalRoleId;

                    OnTurnActionsChanged?.Invoke(this, null);

                    OnAllocationsChanged?.Invoke(this, null);
                };

                _turnActions.Add(actionData);

                OnTurnActionsChanged?.Invoke(this, null);

                OnAllocationsChanged?.Invoke(this, null);
            }
        }

        protected void CanApplyAllocationEdit(object sender, CancelEventArgs<EditAllocationEventArgs> args)
        {
            var maxPercent = byte.MaxValue - _allocations.Where(a => a.PersonId == args.Data.PersonId && a.ProjectId != args.Data.ProjectId).Sum(a => a.Percent);

            if (args.Data.Percent <= 0 || args.Data.Percent > maxPercent)
            {
                args.Cancel = true;

                return;
            }
        }

        protected void ApplyAllocationEdit(object sender, EditAllocationEventArgs args)
        {
            var allocation = _allocation;

            if (allocation == null)
            {
                allocation = _allocations.FirstOrDefault(a => a.PersonId == args.PersonId && a.ProjectId == args.ProjectId);
            }

            AdjustAllocationActionData actionData;

            if (allocation == null)
            {
                allocation = new Allocation(args.ProjectId, args.PersonId, args.RoleId, args.Percent);

                _data.NewAllocations.Add(allocation);

                actionData = new AdjustAllocationActionData(args.PersonId, args.ProjectId, args.Percent, args.RoleId);

                actionData.Cancelled += (s, e) =>
                {
                    _data.NewAllocations.Remove(allocation);

                    _turnActions.Remove(actionData);

                    OnAllocationsChanged?.Invoke(s, e);

                    OnTurnActionsChanged?.Invoke(s, e);
                };

                _turnActions.Add(actionData);

                OnAllocationsChanged?.Invoke(this, null);

                OnTurnActionsChanged?.Invoke(this, null);

                return;
            }

            allocation.RoleId = args.RoleId;

            allocation.Percent = args.Percent;

            actionData = TurnActions.FirstOrDefault(a => a.Data.PersonId == args.PersonId && a.Data.ProjectId == args.ProjectId);

            if (!allocation.IsDirty)
            {
                if (actionData != null)
                {
                    actionData.Cancel();
                }

                return;
            }

            if (actionData == null)
            {
                actionData = new AdjustAllocationActionData(args.PersonId, args.ProjectId, args.Percent, args.RoleId);

                actionData.Cancelled += (s, e) =>
                {
                    _turnActions.Remove(actionData);

                    allocation.RoleId = allocation.OriginalRoleId;

                    allocation.Percent = allocation.OriginalPercent;

                    OnTurnActionsChanged?.Invoke(this, null);
                };

                _turnActions.Add(actionData);

                OnAllocationsChanged?.Invoke(this, null);

                OnTurnActionsChanged?.Invoke(this, null);

                return;
            }

            actionData.Data.Role = args.RoleId;

            actionData.Data.Percentage = args.Percent;

            OnAllocationsChanged?.Invoke(this, null);

            OnTurnActionsChanged?.Invoke(this, null);

        }

        protected void DisplayEditAllocationPopup(EditAllocationPopupViewModel viewModel)
        {
            viewModel.EditingAllocation += CanApplyAllocationEdit;

            viewModel.EditAllocation += ApplyAllocationEdit;

            var popup = new Popup<IPopupViewModel>(new EditAllocationPopupView(), viewModel);

            _viewStateService.PushPopup(popup);
        }
    }
}
