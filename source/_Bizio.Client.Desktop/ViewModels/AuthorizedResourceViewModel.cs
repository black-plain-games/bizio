﻿using Bizio.Client.Desktop.Services;
using Bizio.Client.Desktop.ViewModels.Popups;
using Bizio.Client.Desktop.Views.Popups;
using BlackPlain.DI;
using System;

namespace Bizio.Client.Desktop.ViewModels
{
    public class AuthorizedResourceViewModel : ViewModel
    {
        public AuthorizedResourceViewModel(
            IHub hub,
            IViewStateService viewStateService,
            IAuthorizationClientService authorizationClientService,
            IResourceClientService resourceClientService)
            : base(hub)
        {
            ViewStateService = viewStateService;

            AuthorizationClientService = authorizationClientService;

            ResourceClientService = resourceClientService;
        }

        public AuthorizedResourceViewModel(
            IHub hub,
            IViewStateService viewStateService,
            IAuthorizationClientService authorizationClientService,
            IResourceClientService resourceClientService,
            bool canLoadAsync)
            : base(hub, canLoadAsync)
        {
            ViewStateService = viewStateService;

            AuthorizationClientService = authorizationClientService;

            ResourceClientService = resourceClientService;
        }

        protected TResult TryGetAuthorizedResource<TResult>(Func<ClientResponse<TResult>> method, Action onLoginSuccessful, Action onLoginFailed)
        {
            var response = method();

            switch (response.Code)
            {
                case ClientResponseCode.Succeeded:
                    return response.Result;

                case ClientResponseCode.Unauthorized:
                    var viewModel = new LoginPopupViewModel(ViewStateService, AuthorizationClientService);

                    viewModel.Dismissed += (s, e) =>
                    {
                        if (!AuthorizationClientService.Authorize())
                        {
                            onLoginFailed?.Invoke();
                        }
                    };

                    viewModel.Login += (s, e) =>
                    {
                        if (e.Result)
                        {
                            onLoginSuccessful?.Invoke();
                        }
                    };

                    ViewStateService.PushPopup<LoginPopupView>(viewModel);

                    break;
            }

            return default;
        }

        protected IViewStateService ViewStateService { get; }

        protected IAuthorizationClientService AuthorizationClientService { get; }

        protected IResourceClientService ResourceClientService { get; }
    }
}
