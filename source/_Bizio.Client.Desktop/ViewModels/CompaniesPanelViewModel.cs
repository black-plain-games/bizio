﻿using Bizio.Client.Desktop.Model.Companies;
using Bizio.Client.Desktop.Model.Game.ActionData;
using Bizio.Client.Desktop.Model.Projects;
using Bizio.Client.Desktop.Services;
using BlackPlain.DI;
using System.Linq;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels
{
    public class CompaniesPanelViewModel : GameFragmentViewModelBase, ICompaniesPanelViewModel
    {
        public Company SelectedCompany
        {
            get
            {
                return GetField<Company>();
            }
            set
            {
                SetField(value);

                OnPropertyChanged(nameof(CompletedProjects));
            }
        }

        public int? CompletedProjects
        {
            get
            {
                return SelectedCompany?.Projects.Count(p => p.Status == ProjectStatus.Completed);
            }
        }

        public IActionData SelectedTurnAction
        {
            get
            {
                return GetField<IActionData>();
            }
            set
            {
                SetField(value);
            }
        }

        public CompanyAction SelectedCompanyAction
        {
            get
            {
                return GetField<CompanyAction>();
            }
            set
            {
                SetField(value);
            }
        }

        public ICommand DeleteSelectedTurnActionCommand => GetCommand(DeleteSelectedTurnAction);

        public ICommand ToggleCompanyActionIsActiveCommand => GetCommand(ToggleCompanyActionIsActive);

        public ICommand ActivateAllCompanyActionsCommand => GetCommand(ActivateAllCompanyActions, CanActivateAllCompanyActions);

        public ICommand DeactivateAllCompanyActionsCommand => GetCommand(DeactivateAllCompanyActions, CanDeactivateAllCompanyActions);

        public CompaniesPanelViewModel(IHub hub,
            IViewStateService viewStateService,
            IAuthorizationClientService authorizationClientService,
            IResourceClientService resourceClientService)
            : base(hub, viewStateService, authorizationClientService, resourceClientService)
        {
        }

        protected override bool CloseSubPanel(object state)
        {
            if (SelectedCompany != null)
            {
                SelectedCompany = null;

                return true;
            }

            if (SelectedTurnAction != null)
            {
                SelectedTurnAction = null;

                return true;
            }

            return false;
        }

        // TODO: calculate this
        private bool CanActivateAllCompanyActions() => true;

        // TODO: calculate this
        private bool CanDeactivateAllCompanyActions() => true;

        private void DeleteSelectedTurnAction()
        {
            var turnAction = SelectedTurnAction;

            turnAction.Cancel();

            SelectedTurnAction = null;
        }

        private void ToggleCompanyActionIsActive(object parameter)
        {
            var actionId = SelectedCompanyAction.ActionId;

            if (parameter != null)
            {
                var companyAction = parameter as CompanyAction;

                companyAction.IsActive = !companyAction.IsActive;

                actionId = companyAction.ActionId;
            }

            var turnAction = FindTurnAction<ToggleCompanyActionActionData>(ta => ta.Data.ActionId == actionId);

            if (turnAction != null)
            {
                TurnActions.Remove(turnAction);
            }
            else
            {
                TurnActions.Add(new ToggleCompanyActionActionData(actionId));
            }
        }

        private void ActivateAllCompanyActions() => ChangeIsActivateForAllCompanyActions(true);

        private void DeactivateAllCompanyActions() => ChangeIsActivateForAllCompanyActions(false);

        private void ChangeIsActivateForAllCompanyActions(bool isActive)
        {
            foreach (var action in UserCompany.Actions)
            {
                if (action.IsActive != isActive)
                {
                    var turnAction = FindTurnAction<ToggleCompanyActionActionData>(ta => ta.Data.ActionId == action.ActionId);

                    if (turnAction == null)
                    {
                        TurnActions.Add(new ToggleCompanyActionActionData(action.ActionId));
                    }
                    else
                    {
                        TurnActions.Remove(turnAction);
                    }

                    action.IsActive = isActive;
                }
            }
        }
    }
}