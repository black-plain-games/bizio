﻿using Bizio.Client.Desktop.Model.Core;
using Bizio.Client.Desktop.Model.Game;
using Bizio.Client.Desktop.Services;
using BlackPlain.DI;
using BlackPlain.Wpf.Core;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels
{
    public class DeveloperViewModel : ViewModel, IDeveloperViewModel
    {
        public ObservableCollection<DeveloperMessage> Messages
        {
            get
            {
                return GetField<ObservableCollection<DeveloperMessage>>();
            }
            private set
            {
                SetField(value);
            }
        }

        public GameData Data
        {
            get
            {
                return GetField<GameData>();
            }
            set
            {
                SetField(value);
            }
        }

        public int? UserId => _authorizationClientService.Instance.UserId;

        public string UserName => _authorizationClientService.Instance.UserName;

        public int? UserCompanyId => Data?.CompaniesData.Companies.FirstOrDefault(c => c.UserId == UserId)?.Id;

        public DateTimeOffset CurrentDateTime => DateTimeOffset.Now;

        public ICommand CopyToClipboardCommand => GetCommand(CopyToClipboard);

        public DeveloperViewModel(IHub hub,
            ILazyResolution<IAuthorizationClientService> authorizationClientService)
            : base(hub)
        {
            Messages = new ObservableCollection<DeveloperMessage>();

            _authorizationClientService = authorizationClientService;
        }

        public void Refresh()
        {
            OnPropertyChanged(nameof(UserId));

            OnPropertyChanged(nameof(UserName));

            OnPropertyChanged(nameof(UserCompanyId));

            OnPropertyChanged(nameof(CurrentDateTime));
        }

        public void LogMessage(string message)
        {
            UIManager.TryInvokeOnUIThread(() => Messages.Add(new DeveloperMessage(message)));
        }

        public void LogMessage(string format, params object[] args)
        {
            LogMessage(string.Format(format, args));
        }

        private void CopyToClipboard(object state)
        {
            var stackTrace = string.Join(Environment.NewLine, $"{state}".Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).Skip(4));

            Clipboard.SetText(stackTrace);
        }

        private readonly ILazyResolution<IAuthorizationClientService> _authorizationClientService;
    }
}