﻿using Bizio.Client.Desktop.Core;
using Bizio.Client.Desktop.Model.Companies;
using Bizio.Client.Desktop.Model.Core;
using Bizio.Client.Desktop.Model.Game;
using Bizio.Client.Desktop.Model.Game.ActionData;
using Bizio.Client.Desktop.Model.People;
using Bizio.Client.Desktop.Services;
using BlackPlain.DI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels
{
    public abstract class GameFragmentViewModelBase : AuthorizedResourceViewModel, IGameFragmentViewModel
    {
        public ObservableCollection<IActionData> TurnActions
        {
            get
            {
                return GetField<ObservableCollection<IActionData>>();
            }
            private set
            {
                SetField(value);
            }
        }

        public IDictionary<byte, int> RemainingTurnActionCount
        {
            get
            {
                var result = UserCompany?.Actions.ToDictionary(a => a.ActionId, a => a.Count - TurnActions?.Count(ta => ta.ActionTypeId == a.ActionId) ?? 0);

                return result;
            }
        }

        public StaticData StaticData
        {
            get
            {
                return GetField<StaticData>();
            }
            private set
            {
                SetField(value);
            }
        }

        public GameData Data
        {
            get
            {
                return GetField<GameData>();
            }
            set
            {
                SetField(value);
            }
        }

        public Company UserCompany
        {
            get
            {
                return GetField<Company>();
            }
            set
            {
                SetField(value);
            }
        }

        public ICommand ClosePanelCommand => GetAsyncCommand(ClosePanel);

        protected GameFragmentViewModelBase(IHub hub,
            IViewStateService viewStateService,
            IAuthorizationClientService authorizationClientService,
            IResourceClientService resourceClientService)
            : base(hub, viewStateService, authorizationClientService, resourceClientService)
        {
        }

        public virtual void RefreshGameData(GameData data, ObservableCollection<IActionData> turnActions)
        {
            Data = data;

            var staticData = TryGetAuthorizedResource(ResourceClientService.FetchStaticGameData, () => RefreshGameData(data, turnActions), ViewStateService.NavigateToMainMenuView);

            if (staticData == null)
            {
                return;
            }

            StaticData = staticData;

            UserCompany = Data.CompaniesData.Companies.FirstOrDefault(c => c.UserId.HasValue);

            TurnActions = turnActions;

            TurnActions.CollectionChanged += (s, e) => OnPropertyChanged(nameof(RemainingTurnActionCount));

            OnPropertyChanged(nameof(RemainingTurnActionCount));
        }

        protected virtual bool CloseSubPanel(object state)
        {
            return false;
        }

        protected void ClosePanel(object state)
        {
            if (!CloseSubPanel(state))
            {
                ViewStateService.ClosePanel();
            }
        }

        protected bool CanUserCompanyPerformActionType(ActionType actionType) => Utilities.CanCompanyPerformActionType(UserCompany, actionType, TurnActions);

        protected bool CanUserCompanyPerformActionType<TActionDataType>(ActionType actionType, Predicate<TActionDataType> shouldExclude)
            where TActionDataType : class, IActionData => Utilities.CanCompanyPerformActionType(UserCompany, actionType, TurnActions, shouldExclude);

        protected TActionData FindTurnAction<TActionData>(Predicate<TActionData> predicate)
            where TActionData : class, IActionData
        {
            return TurnActions?.FirstOrDefault(ta => ta is TActionData && predicate(ta as TActionData)) as TActionData;
        }

        protected IEnumerable<TActionData> FindTurnActions<TActionData>(Predicate<TActionData> predicate)
            where TActionData : class, IActionData
        {
            return TurnActions?.Where(ta => ta is TActionData && predicate(ta as TActionData)).Cast<TActionData>();
        }

        protected IEnumerable<TActionData> FindTurnActions<TActionData>()
            where TActionData : class, IActionData
        {
            return FindTurnActions<TActionData>(TruePredicate);
        }

        private bool TruePredicate<T>(T data) => true;

        protected Person FindPerson(int personId) => Data.PeopleData.People.First(p => p.Id == personId);

        protected Prospect FindProspect(int personId) => UserCompany.Prospects.FirstOrDefault(p => p.PersonId == personId);

        protected DateTime GetTurnDate(int turnDifference)
        {
            return Data.GameClockData.CurrentDate.AddDays(turnDifference * 7 * Data.GameClockData.WeeksPerTurn);
        }
    }
}