﻿using Bizio.Client.Desktop.Core;
using Bizio.Client.Desktop.Model.Companies;
using Bizio.Client.Desktop.Model.Core;
using Bizio.Client.Desktop.Model.Game;
using Bizio.Client.Desktop.Model.Game.ActionData;
using Bizio.Client.Desktop.Model.Projects;
using Bizio.Client.Desktop.Services;
using BlackPlain.DI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels
{
    public class GameViewModel : AuthorizedResourceViewModel, IGameViewModel
    {
        public Panel VisiblePanel
        {
            get
            {
                return GetField<Panel>();
            }
            set
            {
                SetField(value);
            }
        }

        public int GameId
        {
            get
            {
                return GetField<int>();
            }
            set
            {
                SetField(value);
            }
        }

        public GameData Data
        {
            get
            {
                return GetField<GameData>();
            }
            set
            {
                SetField(value);
            }
        }

        public Company UserCompany
        {
            get
            {
                return GetField<Company>();
            }
            set
            {
                SetField(value);
            }
        }

        public ObservableCollection<IActionData> TurnActions
        {
            get
            {
                return GetField<ObservableCollection<IActionData>>();
            }
            set
            {
                SetField(value);
            }
        }

        public byte UserRank
        {
            get
            {
                return GetField<byte>();
            }
            set
            {
                SetField(value);
            }
        }

        public double LoadingAngle1
        {
            get
            {
                return GetField<double>();
            }
            set
            {
                SetField(value);
            }
        }

        public double LoadingAngle2
        {
            get
            {
                return GetField<double>();
            }
            set
            {
                SetField(value);
            }
        }

        public IDictionary<Panel, byte> PanelUpdates
        {
            get
            {
                return GetField<IDictionary<Panel, byte>>();
            }
            set
            {
                SetField(value);
            }
        }

        public IPeoplePanelViewModel PeoplePanelViewModel => _gameFragmentViewModels?[Panel.People] as IPeoplePanelViewModel;

        public IUserCompanyPanelViewModel UserCompanyPanelViewModel => _gameFragmentViewModels?[Panel.UserCompany] as IUserCompanyPanelViewModel;

        public ICompaniesPanelViewModel CompaniesPanelViewModel => _gameFragmentViewModels?[Panel.OtherCompanies] as ICompaniesPanelViewModel;

        public IProjectsPanelViewModel ProjectsPanelViewModel => _gameFragmentViewModels?[Panel.Projects] as IProjectsPanelViewModel;

        public IStorePanelViewModel StorePanelViewModel => _gameFragmentViewModels?[Panel.Store] as IStorePanelViewModel;

        public IMailboxPanelViewModel MailboxPanelViewModel => _gameFragmentViewModels?[Panel.Mailbox] as IMailboxPanelViewModel;

        public ICommand TryQuitCommand => GetCommand(TryQuit);

        public ICommand NextTurnCommand => GetAsyncCommand(NextTurn, true);

        public ICommand TogglePanelCommand => GetAsyncCommand(TogglePanel);

        public ICommand ClosePanelCommand => GetAsyncCommand(ClosePanel);

        public GameViewModel(
            IHub hub,
            IViewStateService viewStateService,
            IAuthorizationClientService authorizationClientService,
            IResourceClientService resourceClientService,
            IPeoplePanelViewModel peoplePanelViewModel,
            IUserCompanyPanelViewModel userCompanyPanelViewModel,
            ICompaniesPanelViewModel companiesPanelViewModel,
            IProjectsPanelViewModel projectsPanelViewModel,
            IStorePanelViewModel storePanelViewModel,
            IMailboxPanelViewModel mailboxPanelViewModel,
            IDeveloperViewModel developerViewModel,
            IGameService gameService)
            : base(hub, viewStateService, authorizationClientService, resourceClientService)
        {
            ViewStateService.PanelClosed += (s, e) => ClosePanel();

            _gameFragmentViewModels = new Dictionary<Panel, IGameFragmentViewModel>
            {
                { Panel.People, peoplePanelViewModel },
                { Panel.UserCompany, userCompanyPanelViewModel },
                { Panel.OtherCompanies, companiesPanelViewModel },
                { Panel.Projects, projectsPanelViewModel },
                { Panel.Store, storePanelViewModel },
                { Panel.Mailbox, mailboxPanelViewModel }
            };

            _gameService = gameService;

            _developerViewModel = developerViewModel;

            _loadingAngleCancellationTokenSource = new CancellationTokenSource();

            var token = _loadingAngleCancellationTokenSource.Token;

            Task.Factory.StartNew(() =>
            {
                var angle = 0.0;

                while (!token.IsCancellationRequested)
                {
                    angle += .5;

                    var newLoadingAngle = LoadingAngle1 + (10.0 * Math.Sin(angle * Math.PI / 180.0));

                    if (angle == 360)
                    {
                        angle = 0;
                    }

                    LoadingAngle1 = newLoadingAngle;

                    LoadingAngle2 = -newLoadingAngle;

                    Thread.Sleep(10);
                }
            }, token);
        }

        protected override void LoadViewModel(object state)
        {
            base.LoadViewModel(state);

            var savedGame = _gameService.Load(GameId);

            RefreshGameData(savedGame);
        }

        private static byte GetChangedEmployeeCount(Company oldCompany, Company newCompany)
        {
            var newEmployeeCount = newCompany.Employees.Count(e => !oldCompany.Employees.Any(e2 => e2.PersonId == e.PersonId));

            var lostEmployeeCount = oldCompany.Employees.Count(e => !newCompany.Employees.Any(e2 => e2.PersonId == e.PersonId));

            return (byte)(newEmployeeCount + lostEmployeeCount);
        }

        protected void RefreshGameData(IEnumerable<IActionData> turnActions)
        {
            TryGetAuthorizedResource(ResourceClientService.FetchStaticGameData, () => RefreshGameData(turnActions), ViewStateService.NavigateToMainMenuView);

            var gameData = TryGetAuthorizedResource(() => ResourceClientService.FetchGameData(GameId), () => RefreshGameData(turnActions), ViewStateService.NavigateToMainMenuView);

            if (gameData == null)
            {
                return;
            }

            var updates = _gameService.LoadUpdates(GameId);

            if (Data != null && UserCompany != null)
            {
                // People Panel
                var newPersonCount = gameData.PeopleData.People.Count(p => !Data.PeopleData.People.Any(p2 => p.Id == p2.Id));

                var userCompany = gameData.CompaniesData.Companies.FirstOrDefault(c => c.UserId.HasValue);

                var newProspectCount = userCompany.Prospects.Count(p => !UserCompany.Prospects.Any(p2 => p2.PersonId == p.PersonId));

                updates[Panel.People] = (byte)(newPersonCount + newProspectCount);

                // User Company Panel
                updates[Panel.UserCompany] = GetChangedEmployeeCount(UserCompany, userCompany);

                // Other Companies Panel
                var otherCompaniesChangedEmployeeCount = 0;

                var otherCompaniesCompletedProjectsCount = 0;

                foreach (var company in gameData.CompaniesData.Companies)
                {
                    if (company.UserId == AuthorizationClientService.UserId)
                    {
                        continue;
                    }

                    var oldCompany = Data.CompaniesData.Companies.First(c => c.Id == company.Id);

                    otherCompaniesChangedEmployeeCount += GetChangedEmployeeCount(oldCompany, company);

                    otherCompaniesCompletedProjectsCount += company.Projects.Count(p => p.Status == ProjectStatus.Completed) - oldCompany.Projects.Count(p => p.Status == ProjectStatus.Completed);
                }

                updates[Panel.OtherCompanies] = (byte)(otherCompaniesChangedEmployeeCount + otherCompaniesCompletedProjectsCount);

                // Projects Panel
                var newProjectCount = gameData.ProjectsData.Projects.Count(p => !Data.ProjectsData.Projects.Any(p2 => p2.Id == p.Id));

                updates[Panel.Projects] = (byte)newProjectCount;

                // Perks Panel
                // Nothing to report here

                // Mailbox Panel
                updates[Panel.Mailbox] = (byte)userCompany.Messages.Count(m => m.Status == CompanyMessageStatus.UnRead);

                _gameService.Save(GameId, updates);
            }

            PanelUpdates = updates;

            Data = gameData;

            UserCompany = Data.CompaniesData.Companies.FirstOrDefault(c => c.UserId.HasValue);

            TurnActions = new ObservableCollection<IActionData>(turnActions);

            foreach (var gameFragmentViewModel in _gameFragmentViewModels)
            {
                gameFragmentViewModel.Value.RefreshGameData(Data, TurnActions);
            }

            _developerViewModel.Data = Data;

            var orderedCompanies = Data.CompaniesData.Companies.OrderByDescending(c => c.Money);

            byte userRank = 1;

            foreach (var company in orderedCompanies)
            {
                if (company.UserId == AuthorizationClientService.UserId)
                {
                    UserRank = userRank;

                    break;
                }

                userRank++;
            }
        }

        protected void Quit()
        {
            _developerViewModel.Data = null;

            _loadingAngleCancellationTokenSource.Cancel();

            ViewStateService.NavigateToMainMenuView();
        }

        private void SaveAndQuit()
        {
            _gameService.Save(GameId, TurnActions);

            Quit();
        }

        private void TryQuit()
        {
            var isTurnInProgress = TurnActions?.Any() == true;

            var title = isTurnInProgress ? "Save and Quit" : "Confirm Quit";

            var message = "Are you sure you want to quit?";

            if (isTurnInProgress)
            {
                message = "It looks like you've taken some actions this turn. Would you like to save them before you quit?";
            }

            var viewModel = Utilities.CreateMessagePopup(ViewStateService, title, message, "Cancel");

            viewModel.AddOption(isTurnInProgress ? "Just Quit" : "Quit", (s, e) => Quit());

            if (isTurnInProgress)
            {
                viewModel.AddOption("Save and Quit", (s, e) => SaveAndQuit());
            }
        }

        protected void NextTurn()
        {
            var result = TryGetAuthorizedResource(() => ResourceClientService.ProcessTurn(GameId, TurnActions), NextTurn, ViewStateService.NavigateToMainMenuView);

            if (!result)
            {
                return;
            }

            RefreshGameData(Enumerable.Empty<IActionData>());
        }

        protected void TogglePanel(object state)
        {
            var panel = state.AsEnum<Panel>();

            if (VisiblePanel == panel)
            {
                VisiblePanel = (int)Panel.None;
            }
            else
            {
                VisiblePanel = panel;
            }
        }

        protected void ClosePanel()
        {
            VisiblePanel = Panel.None;
        }

        private readonly IGameService _gameService;

        private readonly IDictionary<Panel, IGameFragmentViewModel> _gameFragmentViewModels;

        private readonly IDeveloperViewModel _developerViewModel;

        private readonly CancellationTokenSource _loadingAngleCancellationTokenSource;
    }
}