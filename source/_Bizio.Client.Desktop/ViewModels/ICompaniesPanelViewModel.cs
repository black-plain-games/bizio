﻿using Bizio.Client.Desktop.Model.Companies;

namespace Bizio.Client.Desktop.ViewModels
{
    public interface ICompaniesPanelViewModel : IGameFragmentViewModel
    {
        Company SelectedCompany { get; set; }

        int? CompletedProjects { get; }
    }
}