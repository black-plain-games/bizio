﻿using Bizio.Client.Desktop.Core;
using Bizio.Client.Desktop.Model.Core;
using Bizio.Client.Desktop.Model.Game;
using System.Collections.ObjectModel;

namespace Bizio.Client.Desktop.ViewModels
{
    public interface IDeveloperViewModel : ILog
    {
        ObservableCollection<DeveloperMessage> Messages { get; }

        GameData Data { get; set; }

        int? UserId { get; }

        string UserName { get; }

        int? UserCompanyId { get; }

        void Refresh();
    }
}