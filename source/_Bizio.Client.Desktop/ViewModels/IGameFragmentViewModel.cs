﻿using Bizio.Client.Desktop.Model.Game;
using Bizio.Client.Desktop.Model.Game.ActionData;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels
{
    public interface IGameFragmentViewModel
    {
        ICommand ClosePanelCommand { get; }

        void RefreshGameData(GameData data, ObservableCollection<IActionData> turnActions);
    }
}