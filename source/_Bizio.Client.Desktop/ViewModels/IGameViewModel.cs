﻿using Bizio.Client.Desktop.Model.Core;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels
{
    public interface IGameViewModel
    {
        Panel VisiblePanel { get; set; }

        int GameId { get; set; }

        ICommand TogglePanelCommand { get; }
    }
}