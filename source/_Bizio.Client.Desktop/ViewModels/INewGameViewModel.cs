﻿using Bizio.Client.Desktop.Model.Core;
using Bizio.Client.Desktop.Model.Game;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels
{
    public interface INewGameViewModel
    {
        NewGameData Data { get; set; }

        StaticData StaticData { get; set; }

        DifficultyLevel SelectedDifficultyLevel { get; set; }

        int CurrentStepNumber { get; set; }

        decimal RemainingSkillPoints { get; }

        string HelpText { get; set; }

        bool IsNavigateToFounderSettingsEnabled { get; }

        ICommand BackCommand { get; }

        ICommand ContinueCommand { get; }

        ICommand ChangeHelpTextCommand { get; }
    }
}