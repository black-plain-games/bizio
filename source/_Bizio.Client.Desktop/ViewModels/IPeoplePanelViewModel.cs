﻿using Bizio.Client.Desktop.Model.Companies;
using Bizio.Client.Desktop.Model.People;
using System.Collections.Generic;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels
{
    public interface IPeoplePanelViewModel : IGameFragmentViewModel
    {
        IEnumerable<Person> UnemployedPeople { get; }

        Person SelectedPerson { get; set; }

        Prospect SelectedProspect { get; set; }

        bool HasRecruitedSelectedPerson { get; }

        bool HasMadeOfferToSelectedPerson { get; }

        bool HasInterviewedSelectedProspect { get; }

        bool HasMadeOfferToSelectedProspect { get; }

        ICommand RecruitCommand { get; }

        ICommand CancelRecruitCommand { get; }

        ICommand InterviewCommand { get; }

        ICommand DraftOfferCommand { get; }

        ICommand CancelOfferCommand { get; }
    }
}