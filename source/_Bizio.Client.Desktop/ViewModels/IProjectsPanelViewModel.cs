﻿using Bizio.Client.Desktop.Model.Companies;
using Bizio.Client.Desktop.Model.Projects;
using System.Collections.Generic;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels
{
    public interface IProjectsPanelViewModel : IGameFragmentViewModel
    {
        Project SelectedProject { get; }

        Project SelectedUserCompanyProject { get; }

        IEnumerable<Allocation> Allocations { get; }

        IEnumerable<Project> AvailableProjects { get; }

        IEnumerable<Project> ProjectsInProgress { get; }

        ICommand RequestProjectCommand { get; }

        ICommand ToggleExtensionRequestCommand { get; }

        ICommand ToggleProjectSubmissionCommand { get; }

        ICommand AddAllocationCommand { get; }

        ICommand DeleteAllocationCommand { get; }
    }
}