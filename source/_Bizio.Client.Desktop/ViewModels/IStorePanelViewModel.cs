﻿using Bizio.Client.Desktop.Model.Companies;
using Bizio.Client.Desktop.Model.Perks;
using System.Collections.Generic;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels
{
    public interface IStorePanelViewModel : IGameFragmentViewModel
    {
        Perk SelectedPerk { get; set; }

        IEnumerable<CompanyPerk> CompanyPerks { get; }

        int SelectedPerkCount { get; }

        ICommand PurchasePerkCommand { get; }

        ICommand CancelPurchasePerkCommand { get; }

        ICommand SellPerkCommand { get; }

        ICommand CancelSellPerkCommand { get; }
    }
}