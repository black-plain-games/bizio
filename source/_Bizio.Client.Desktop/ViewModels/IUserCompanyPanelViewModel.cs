﻿using Bizio.Client.Desktop.Model.Companies;
using System.Collections.Generic;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels
{
    public interface IUserCompanyPanelViewModel : IGameFragmentViewModel
    {
        Employee SelectedEmployee { get; set; }

        IEnumerable<Transaction> UserCompanyTransactions { get; }

        decimal TotalIncome { get; }

        decimal TotalExpenses { get; }

        decimal? OfferValue { get; set; }

        ICommand ToggleFireSelectedEmployeeCommand { get; }

        ICommand EditSalaryCommand { get; }

        ICommand CancelSalaryChangeCommand { get; }
    }
}