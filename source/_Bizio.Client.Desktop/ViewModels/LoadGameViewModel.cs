﻿using Bizio.Client.Desktop.Model.Core;
using Bizio.Client.Desktop.Model.Game;
using Bizio.Client.Desktop.Services;
using BlackPlain.DI;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels
{
    public class LoadGameViewModel : AuthorizedResourceViewModel, ILoadGameViewModel
    {
        public StaticData StaticData
        {
            get
            {
                return GetField<StaticData>();
            }
            set
            {
                SetField(value);
            }
        }

        public IEnumerable<Game> Games
        {
            get
            {
                return GetField<IEnumerable<Game>>();
            }
            set
            {
                SetField(value);
            }
        }

        public Game SelectedGame
        {
            get
            {
                return GetField<Game>();
            }
            set
            {
                SetField(value);
            }
        }

        public Game SelectedGameInProgress
        {
            get
            {
                return GetField<Game>();
            }
            set
            {
                SetField(value);

                if (value != null)
                {
                    SelectedGameCompleted = null;
                }

                SelectedGame = value ?? SelectedGameCompleted;
            }
        }

        public Game SelectedGameCompleted
        {
            get
            {
                return GetField<Game>();
            }
            set
            {
                SetField(value);

                if (value != null)
                {
                    SelectedGameInProgress = null;
                }

                SelectedGame = value ?? SelectedGameInProgress;
            }
        }

        public ICommand LoadGameCommand => GetCommand(LoadGame, CanLoadGame);

        public ICommand BackCommand => GetCommand(Back);

        public LoadGameViewModel(IHub hub,
            IViewStateService viewStateService,
            IAuthorizationClientService authorizationClientService,
            IResourceClientService resourceClientService)
            : base(hub, viewStateService, authorizationClientService, resourceClientService)
        {
        }

        protected override void LoadViewModel(object state)
        {
            base.LoadViewModel(state);

            var staticData = TryGetAuthorizedResource(ResourceClientService.FetchStaticGameData, () => LoadViewModel(state), ViewStateService.NavigateToMainMenuView);

            if (staticData == null)
            {
                return;
            }

            StaticData = staticData;

            var games = TryGetAuthorizedResource(ResourceClientService.RetrieveGames, () => LoadViewModel(state), ViewStateService.NavigateToMainMenuView);

            if (games == null)
            {
                return;
            }

            Games = games;

            SelectedGame = Games.FirstOrDefault();

            SelectedGameCompleted = null;
        }

        public override void OnEscapePressed()
        {
            Back();
        }

        private void Back()
        {
            ViewStateService.NavigateToMainMenuView();
        }

        private void LoadGame()
        {
            ViewStateService.NavigateToGameView(SelectedGame.Id);
        }

        private bool CanLoadGame()
        {
            return SelectedGameInProgress?.Status == GameStatus.Playing;
        }
    }
}