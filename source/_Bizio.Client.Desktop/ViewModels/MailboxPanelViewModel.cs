﻿using Bizio.Client.Desktop.Model.Companies;
using Bizio.Client.Desktop.Model.Game;
using Bizio.Client.Desktop.Model.Game.ActionData;
using Bizio.Client.Desktop.Services;
using BlackPlain.DI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels
{
    public class MailboxPanelViewModel : GameFragmentViewModelBase, IMailboxPanelViewModel
    {
        public IEnumerable<CompanyMessage> UnReadMessages
        {
            get
            {
                if (UserCompany == null)
                {
                    return null;
                }

                return UserCompany.Messages.Where(m => m.Status == CompanyMessageStatus.UnRead).OrderByDescending(m => m.DateCreated);
            }
        }

        public IEnumerable<CompanyMessage> ReadMessages
        {
            get
            {
                if (UserCompany == null)
                {
                    return null;
                }

                return UserCompany.Messages.Where(m => m.Status == CompanyMessageStatus.Read).OrderByDescending(m => m.DateCreated);
            }
        }

        public CompanyMessage SelectedInboxMessage
        {
            get
            {
                return GetField<CompanyMessage>();
            }
            set
            {
                SetField(value);

                MarkAsRead(value);
            }
        }

        public CompanyMessage SelectedReadMessage
        {
            get
            {
                return GetField<CompanyMessage>();
            }
            set
            {
                SetField(value);
            }
        }

        public ICommand ChangeInboxMessageStatusCommand => GetCommand(x => ChangeMessageStatus(SelectedInboxMessage, x), x => CanChangeMessageStatus(SelectedInboxMessage, x));

        public ICommand ChangeReadMessageStatusCommand => GetCommand(x => ChangeMessageStatus(SelectedReadMessage, x), x => CanChangeMessageStatus(SelectedReadMessage, x));

        public MailboxPanelViewModel(IHub hub,
            IViewStateService viewStateService,
            IAuthorizationClientService authorizationClientService,
            IResourceClientService resourceClientService)
            : base(hub, viewStateService, authorizationClientService, resourceClientService)
        {
        }

        public override void RefreshGameData(GameData data, ObservableCollection<IActionData> turnActions)
        {
            base.RefreshGameData(data, turnActions);

            OnPropertyChanged(nameof(UnReadMessages));

            OnPropertyChanged(nameof(ReadMessages));
        }

        protected bool CanChangeMessageStatus(CompanyMessage message, object state)
        {
            if (Data == null ||
                Data.CompaniesData == null ||
                message == null)
            {
                return false;
            }

            var turnAction = FindTurnAction<ChangeCompanyMessageStatusActionData>(ta => ta.Data.MessageId == message.Id);

            if (turnAction == null)
            {
                return true;
            }

            var statusId = (byte)Enum.Parse(typeof(CompanyMessageStatus), $"{state}");

            return turnAction.Data.Status != statusId;
        }

        protected void ChangeMessageStatus(CompanyMessage message, object state)
        {
            var turnAction = FindTurnAction<ChangeCompanyMessageStatusActionData>(ta => ta.Data.MessageId == message.Id);

            if (turnAction != null)
            {
                TurnActions.Remove(turnAction);
            }

            message.Status = (CompanyMessageStatus)Enum.Parse(typeof(CompanyMessageStatus), $"{state}");

            TurnActions.Add(new ChangeCompanyMessageStatusActionData(message.Id, message.Status));
        }

        protected void MarkAsRead(CompanyMessage message)
        {
            if (Data == null ||
                message == null)
            {
                return;
            }

            var turnAction = FindTurnAction<ChangeCompanyMessageStatusActionData>(ta => ta.Data.MessageId == message.Id);

            if (turnAction == null)
            {
                message.Status = CompanyMessageStatus.Read;

                TurnActions.Add(new ChangeCompanyMessageStatusActionData(message.Id, CompanyMessageStatus.Read));
            }
            else if (turnAction.Data.Status != (byte)CompanyMessageStatus.Deleted)
            {
                TurnActions.Remove(turnAction);

                message.Status = CompanyMessageStatus.Read;

                TurnActions.Add(new ChangeCompanyMessageStatusActionData(message.Id, CompanyMessageStatus.Read));
            }
        }
    }
}