﻿using Bizio.Client.Desktop.Core;
using Bizio.Client.Desktop.Model.Core;
using Bizio.Client.Desktop.Services;
using Bizio.Client.Desktop.ViewModels.Popups;
using Bizio.Client.Desktop.Views.Popups;
using BlackPlain.DI;
using BlackPlain.Wpf.Core;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels
{
    public class MainMenuViewModel : ViewModel, IMainMenuViewModel, IDisposable
    {
        public bool IsAuthorizationServerReady
        {
            get
            {
                return GetField<bool>();
            }
            set
            {
                SetField(value);
            }
        }

        public bool IsResourceServerReady
        {
            get
            {
                return GetField<bool>();
            }
            set
            {
                SetField(value);
            }
        }

        public bool HasAttemptedToAuthorize
        {
            get
            {
                return GetField<bool>();
            }
            set
            {
                SetField(value);
            }
        }

        public bool IsAuthorized
        {
            get
            {
                return GetField<bool>();
            }
            set
            {
                SetField(value);
            }
        }

        public string StatusDescription
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public string UserName
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public int UnreadNotificationsCount
        {
            get
            {
                return GetField<int>();
            }
            set
            {
                SetField(value);
            }
        }

        public ICommand ShowLoginPopupCommand => GetCommand(ShowLoginPopup);

        public ICommand ShowRegisterPopupCommand => GetCommand(ShowRegisterPopup);

        public ICommand NewGameCommand => GetCommand(CreateNewGame);

        public ICommand LoadGameCommand => GetCommand(LoadGame);

        public ICommand ViewSettingsCommand => GetCommand(ViewSettings);

        public ICommand ViewNotificationsCommand => GetCommand(ViewNotifications);

        public ICommand LogoutCommand => GetCommand(Logout);

        public ICommand QuitCommand => GetCommand(Quit);

        public ICommand VerifyServerStatusCommand => GetAsyncCommand(VerifyServerStatus, true);

        public MainMenuViewModel(
            IHub hub,
            Application application,
            IAuthorizationClientService authorizationClientService,
            IResourceClientService resourceClientService,
            IViewStateService viewStateService,
            IDeveloperViewModel developerViewModel,
            INotificationService notificationService)
            : base(hub)
        {
            _application = application;

            _authorizationClientService = authorizationClientService;

            _resourceClientService = resourceClientService;

            _viewStateService = viewStateService;

            _developerViewModel = developerViewModel;

            _notificationService = notificationService;

            _verifyServerStatusTask = new Task(RunVerifyServerStatusLoop);
        }

        ~MainMenuViewModel()
        {
            Dispose(false);
        }

        public override void OnEscapePressed()
        {
            Quit();
        }

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        protected override void LoadViewModel(object state)
        {
            base.LoadViewModel(state);

            if (_verifyServerStatusTask.Status != TaskStatus.Running)
            {
                _verifyServerStatusTask.Start();
            }

            if (_authorizationClientService.Authorize())
            {
                IsAuthorized = true;

                UserName = _authorizationClientService.UserName;

                var notifications = _notificationService.RefreshNotifications(_authorizationClientService.UserId.Value);

                UnreadNotificationsCount = notifications.Count(n => !n.IsRead);
            }
            else
            {
                IsAuthorized = false;
            }

            HasAttemptedToAuthorize = true;
        }

        private void VerifyServerStatus()
        {
            var step = "verify resource server";

            try
            {
                IsResourceServerReady = _resourceClientService.VerifyServerStatus();

                step = "verify authorization server";

                IsAuthorizationServerReady = _authorizationClientService.VerifyServerStatus();

                step = "update description";

                var statusFormat = "A{0} R{1}";

                StatusDescription = string.Format(statusFormat, IsAuthorizationServerReady ? Glyphs.ServerUpSymbol : Glyphs.ServerDownSymbol, IsResourceServerReady ? Glyphs.ServerUpSymbol : Glyphs.ServerDownSymbol);
            }
            catch (Exception err)
            {
                _developerViewModel.LogMessage("Failed to {0}\n\t{1}", step, err.Message);

                StatusDescription = "Something went wrong. Let's try again.";
            }
        }

        private void RunVerifyServerStatusLoop()
        {
            while (true)
            {
                VerifyServerStatus();

                if (IsResourceServerReady && IsAuthorizationServerReady)
                {
                    Thread.Sleep(300000);
                }
                else
                {
                    Thread.Sleep(10000);
                }
            }
        }

        private void ShowLoginPopup()
        {
            if (_viewStateService.CurrentPopup != null)
            {
                return;
            }

            var viewModel = new LoginPopupViewModel(_viewStateService, _authorizationClientService);

            viewModel.Login += OnLogin;

            _viewStateService.PushPopup<LoginPopupView>(viewModel);
        }

        private void OnLogin(object sender, EventResultArgs<LoginEventArgs, bool> e)
        {
            if (!IsAuthorized == true && e.Result)
            {
                var notifications = _notificationService.RefreshNotifications(_authorizationClientService.UserId.Value);

                UnreadNotificationsCount = notifications.Count(n => !n.IsRead);
            }

            IsAuthorized = e.Result;

            UserName = _authorizationClientService.UserName;
        }

        private void ShowRegisterPopup()
        {
            if (_viewStateService.CurrentPopup != null)
            {
                return;
            }

            var viewModel = new RegisterPopupViewModel(_viewStateService, _authorizationClientService);

            viewModel.Register += OnRegister;

            var popup = new Popup<IPopupViewModel>(new RegisterPopupView(), viewModel);

            _viewStateService.PushPopup(popup, HorizontalAlignment.Stretch);
        }

        private void OnRegister(object sender, EventResultArgs<RegisterEventArgs, bool> e)
        {
            IsAuthorized = _authorizationClientService.Authorize();

            UserName = _authorizationClientService.UserName;
        }

        private void CreateNewGame()
        {
            _viewStateService.NavigateToNewGameView();
        }

        private void LoadGame()
        {
            _viewStateService.NavigateToLoadGameView();
        }

        private void ViewSettings()
        {
            _viewStateService.NavigateToSettingsView();
        }

        private void ViewNotifications()
        {
            _viewStateService.NavigateToNotificationsView();
        }

        private void Logout()
        {
            IsAuthorized = !_authorizationClientService.Logout();

            UserName = _authorizationClientService.UserName;
        }

        private void Quit()
        {
            if (_viewStateService.CurrentPopup != null)
            {
                return;
            }

            var viewModel = Utilities.CreateMessagePopup(_viewStateService, UiText.ConfirmQuit, UiText.ConfirmQuitDetails, UiText.No);

            viewModel.AddOption(UiText.Yes, (s, e) => UIManager.TryInvokeOnUIThread(() => _application.Shutdown()));
        }

        protected virtual void Dispose(bool isDisposing)
        {
            if (isDisposing)
            {
                if (_verifyServerStatusTask != null)
                {
                    _verifyServerStatusTask.Dispose();
                }
            }
        }

        private readonly INotificationService _notificationService;

        private readonly IDeveloperViewModel _developerViewModel;

        private readonly Application _application;

        private readonly IAuthorizationClientService _authorizationClientService;

        private readonly IResourceClientService _resourceClientService;

        private readonly IViewStateService _viewStateService;

        private readonly Task _verifyServerStatusTask;
    }
}