﻿using Bizio.Client.Desktop.Converters;
using Bizio.Client.Desktop.Core;
using Bizio.Client.Desktop.Model.Core;
using Bizio.Client.Desktop.Model.Settings;
using Bizio.Client.Desktop.Services;
using Bizio.Client.Desktop.ViewModels.Popups;
using Bizio.Client.Desktop.Views.Popups;
using BlackPlain.DI;
using BlackPlain.Wpf.Core;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels
{
    public class MainViewModel : ViewModel, IViewStateRoot
    {
        public FrameworkElement CurrentView
        {
            get
            {
                return GetField<FrameworkElement>();
            }
            set
            {
                SetField(value);
            }
        }

        public object CurrentViewModel
        {
            get
            {
                return GetField<object>();
            }
            set
            {
                SetField(value);
            }
        }

        public IDeveloperViewModel DeveloperViewModel => Hub.Resolve<IDeveloperViewModel>();

        public bool IsDeveloperViewVisible
        {
            get
            {
                return GetField<bool>();
            }
            set
            {
                SetField(value);

                if(value)
                {
                    DeveloperViewModel.Refresh();
                }
            }
        }

        public IPopupPresenterViewModel PopupPresenterViewModel => Hub.Resolve<IPopupPresenterViewModel>();

        public bool IsPopupVisible
        {
            get
            {
                return GetField<bool>();
            }
            set
            {
                SetField(value);
            }
        }

        public double UiScaleX
        {
            get
            {
                return GetField<double>();
            }
            set
            {
                SetField(value);
            }
        }

        public double UiScaleY
        {
            get
            {
                return GetField<double>();
            }
            set
            {
                SetField(value);
            }
        }

        public WindowStyle WindowStyle
        {
            get
            {
                return GetField<WindowStyle>();
            }
            set
            {
                SetField(value);
            }
        }

        public WindowState WindowState
        {
            get
            {
                return GetField<WindowState>();
            }
            set
            {
                SetField(value);
            }
        }

        public ICommand ToggleDeveloperViewCommand => GetAsyncCommand(ToggleDeveloperView, CanToggleDeveloperView);

        public ICommand EscapeCommand => GetCommand(OnEscapePressed);

        public ICommand ReportIssueCommand => GetCommand(ShowCreateNewIssuePopup);

        public MainViewModel(IHub hub, IApplicationSettingService applicationSettingService, IViewStateService viewStateService, IEmailService emailService, IDeveloperViewModel developerViewModel)
            : base(hub, false)
        {
            WindowState = WindowState.Maximized;

            WindowStyle = WindowStyle.None;

            _applicationSettingService = applicationSettingService;

            _viewStateService = viewStateService;

            _emailService = emailService;

            _developerViewModel = developerViewModel;

            _viewStateService.PopupChanged += OnPopupChanged;
        }

        private void OnPopupChanged(object sender, PopupChangedEventArgs args)
        {
            IsPopupVisible = args.PopupCount > 0;
        }

        private void ShowCreateNewIssuePopup()
        {
            var viewModel = new ReportIssuePopupViewModel(_viewStateService, _emailService, _developerViewModel);

            _viewStateService.PushPopup<ReportIssuePopupView>(viewModel, HorizontalAlignment.Stretch);
        }

        public override void OnEscapePressed()
        {
            if (CurrentViewModel is ViewModel currentViewModel)
            {
                currentViewModel.OnEscapePressed();
            }
        }

        protected override void LoadViewModel(object state)
        {
            base.LoadViewModel(state);

            Utilities.DisableWindowContextMenu(Application.Current.MainWindow);

            _applicationSettingService.SettingsSaved += ApplySettings;

            LoadSettings();

            _viewStateService.NavigateToMainMenuView();
        }

        protected bool CanToggleDeveloperView(object state)
        {
            return true;
        }

        protected void ToggleDeveloperView(object state)
        {
            IsDeveloperViewVisible = !IsDeveloperViewVisible;
        }

        protected void LoadSettings()
        {
            var settings = _applicationSettingService.GetSettings(true);

            UiText.SetLanguages(_applicationSettingService.GetLanguages());

            ApplySettings(settings);
        }

        protected void ApplySettings(object sender, ValueChangedEventArgs<ApplicationSettings> args)
        {
            ApplySettings(args.CurrentValue);
        }

        protected void ApplySettings(ApplicationSettings settings)
        {
            if (settings.UiScale.X <= 0 ||
                settings.UiScale.Y <= 0)
            {
                settings.UiScale = UiScale.One;

                _applicationSettingService.SaveSettings();
            }

            UiScaleX = settings.UiScale.X * Application.Current.MainWindow.ActualWidth / _baseResolution.X;

            UiScaleY = settings.UiScale.Y * Application.Current.MainWindow.ActualHeight / _baseResolution.Y;

            if (string.IsNullOrWhiteSpace(settings.BaseCulture))
            {
                settings.BaseCulture = CultureInfo.CurrentCulture.TwoLetterISOLanguageName;

                settings.CurrencySymbol = CultureInfo.CurrentCulture.NumberFormat.CurrencySymbol;

                _applicationSettingService.SaveSettings();
            }

            var culture = new CultureInfo(settings.BaseCulture);

            culture.NumberFormat.CurrencyNegativePattern = 1;

            culture.NumberFormat.CurrencySymbol = settings.CurrencySymbol;

            MultiBinding.GlobalCulture = Binding.GlobalCulture = culture;

            UiText.LoadLanguages();

            UiText.Language = culture.TwoLetterISOLanguageName;
        }

        private readonly IViewStateService _viewStateService;

        private readonly IApplicationSettingService _applicationSettingService;

        private readonly IEmailService _emailService;

        private readonly IDeveloperViewModel _developerViewModel;

        private static readonly Point _baseResolution = new Point(3840, 2160);
    }
}