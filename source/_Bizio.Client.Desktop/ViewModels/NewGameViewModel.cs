﻿using Bizio.Client.Desktop.Core;
using Bizio.Client.Desktop.Model.Core;
using Bizio.Client.Desktop.Model.Game;
using Bizio.Client.Desktop.Model.People;
using Bizio.Client.Desktop.Services;
using BlackPlain.DI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels
{
    public class NewGameViewModel : AuthorizedResourceViewModel, INewGameViewModel
    {
        public NewGameData Data
        {
            get
            {
                return GetField<NewGameData>();
            }
            set
            {
                SetField(value);
            }
        }

        public StaticData StaticData
        {
            get
            {
                return GetField<StaticData>();
            }
            set
            {
                SetField(value);
            }
        }

        public DifficultyLevel SelectedDifficultyLevel
        {
            get
            {
                return GetField<DifficultyLevel>();
            }
            set
            {
                SetField(value);

                OnSelectedDifficultyChanged();
            }
        }

        public int CurrentStepNumber
        {
            get
            {
                return GetField<int>();
            }
            set
            {
                SetField(value);
            }
        }

        public decimal RemainingSkillPoints
        {
            get
            {
                if (SelectedDifficultyLevel == null)
                {
                    return 0;
                }

                var totalSkillPoints = Data.Founder.MandatorySkills.Sum(s => s.Value);

                totalSkillPoints += Data.Founder.OptionalSkills.Sum(s => s.Value);

                return SelectedDifficultyLevel.MaxTotalSkillPoints - totalSkillPoints;
            }
        }

        public string HelpText
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public bool IsNavigateToFounderSettingsEnabled => CanNavigateToFounderSettings();

        public IEnumerable<SkillDefinition> AvailableOptionalSkillDefinitions
        {
            get
            {
                if (StaticData == null ||
                    Data == null)
                {
                    return null;
                }

                return StaticData.PeopleData.OptionalSkillDefinitions[Data.IndustryId].Where(sd => !Data.Founder.OptionalSkills.Any(s => s.Id != null && s.Id.Id == sd.Id));
            }
        }

        public IEnumerable<SkillDefinition> MandatorySkillDefinitions
        {
            get
            {
                return GetField<IEnumerable<SkillDefinition>>();
            }
            set
            {
                SetField(value);
            }
        }

        public ICommand BackCommand => GetCommand(Back);

        public ICommand ContinueCommand => GetCommand(Continue, CanContinue);

        public ICommand ChangeHelpTextCommand => GetCommand(ChangeHelpText);

        public ICommand IndustryIdChangedCommand => GetCommand(OnIndustryIdChanged);

        public ICommand DecreaseSkillValueCommand => GetCommand(DecreaseSkillValue, CanDecreaseSkillValue);

        public ICommand IncreaseSkillValueCommand => GetCommand(IncreaseSkillValue, CanIncreaseSkillValue);

        public NewGameViewModel(
            IHub hub,
            IViewStateService viewStateService,
            IAuthorizationClientService authorizationClientService,
            IResourceClientService resourceClientService)
            : base(hub, viewStateService, authorizationClientService, resourceClientService)
        {
        }

        public override void OnEscapePressed()
        {
            Back();
        }

        bool _isLoaded = false;

        protected override void LoadViewModel(object state)
        {
            IsBusy = true;

            base.LoadViewModel(state);

            _helpText = new Dictionary<string, string>
            {
                { "GameName", "Something helpful about game name" },
                { "StartDate", "Something helpful about start date" },
                { "TurnSpeed", "Something helpful about turn speed" },
                { "CompanyName", "Something helpful about company name" },
                { "NumberOfCompanies", "Something helpful about number of companies" },
                { "Industry", "Something helpful about industry" },
                { "Difficulty", "Something helpful about difficulty" },
                { "InitialFunds", "Something helpful about initial funds" }
            };

            StaticData = TryGetAuthorizedResource(ResourceClientService.FetchStaticGameData, () => LoadViewModel(state), ViewStateService.NavigateToMainMenuView);

            if (StaticData == null)
            {
                return;
            }

            Data = new NewGameData
            {
                StartDate = DateTime.Today,
                WeeksPerTurn = 1,
                NumberOfCompanies = 4
            };

            RegisterForValidation(Data);

            RegisterForValidation(Data.Founder);

            UnregisterFromValidation(nameof(HelpText), nameof(IsNavigateToFounderSettingsEnabled), nameof(CurrentStepNumber));

            OnIndustryIdChanged();

            SelectedDifficultyLevel = StaticData.GameData.DifficultyLevels.First();

            IsBusy = false;

            _isLoaded = true;
        }

        private Range<DateTime> GetBirthdayRange()
        {
            // This can fail if Data is not loaded 
            // or if the user enters an unrealistic date
            try
            {
                return new Range<DateTime>(Data.StartDate.AddYears(-Limits.Age.Maximum), Data.StartDate.AddYears(-Limits.Age.Minimum));
            }
            catch
            {
                return new Range<DateTime>(DateTime.Today.AddYears(-Limits.Age.Maximum), DateTime.Today.AddYears(-Limits.Age.Minimum));
            }
        }

        protected override IEnumerable<string> Validate(string propertyName)
        {
            if (!_isLoaded)
            {
                yield break;
            }

            var birthdayRange = GetBirthdayRange();

            switch ((NewGameStep)CurrentStepNumber)
            {
                case NewGameStep.GameSettings:
                    switch (propertyName)
                    {
                        case nameof(Data.CompanyName):
                            if (string.IsNullOrWhiteSpace(Data.CompanyName) ||
                                !Data.CompanyName.Trim().Length.Between(Limits.CompanyNameLength))
                            {
                                yield return RangeErrorMessage("Company name", Limits.CompanyNameLength, "letters");
                            }
                            break;

                        case nameof(Data.NumberOfCompanies):
                            if (!Data.NumberOfCompanies.Between(Limits.NumberOfCompanies))
                            {
                                yield return RangeErrorMessage("Number of companies", Limits.NumberOfCompanies);
                            }
                            break;

                        case nameof(Data.WeeksPerTurn):
                            if (!Data.WeeksPerTurn.Between(Limits.WeeksPerTurn))
                            {
                                yield return RangeErrorMessage("Weeks per turn", Limits.WeeksPerTurn);
                            }
                            break;

                        case nameof(Data.StartDate):
                            if (!Data.StartDate.Between(Limits.StartDate))
                            {
                                yield return RangeErrorMessage("Start date", Limits.StartDate);
                            }
                            break;

                        case nameof(SelectedDifficultyLevel):
                            if (SelectedDifficultyLevel == null)
                            {
                                yield return "Please select a difficulty level";
                            }
                            break;
                    }
                    break;

                case NewGameStep.FounderSettings:
                    switch (propertyName)
                    {
                        case nameof(Data.Founder.FirstName):
                            if (string.IsNullOrWhiteSpace(Data.Founder.FirstName) ||
                                !Data.Founder.FirstName.Length.Between(Limits.FirstNameLength))
                            {
                                yield return RangeErrorMessage("First name", Limits.FirstNameLength, "letters");
                            }
                            break;

                        case nameof(Data.Founder.LastName):
                            if (string.IsNullOrWhiteSpace(Data.Founder.LastName) ||
                                !Data.Founder.LastName.Length.Between(Limits.LastNameLength))
                            {
                                yield return RangeErrorMessage("Last name", Limits.LastNameLength, "letters");
                            }
                            break;

                        case nameof(Data.Founder.Birthday):
                            if (!Data.Founder.Birthday.Between(birthdayRange))
                            {
                                yield return RangeErrorMessage("Birthday", birthdayRange);
                            }
                            break;

                        case "Value":
                            var zeroValueSkills = new List<string>();

                            zeroValueSkills.AddRange(Data.Founder.MandatorySkills.Where(s => s.Value == 0).Select(s => s.Id.Name));

                            zeroValueSkills.AddRange(Data.Founder.OptionalSkills.Where(s => s.Id != null && s.Value == 0).Select(s => s.Id.Name));

                            if (zeroValueSkills.Any())
                            {
                                yield return $"{string.Join(", ", zeroValueSkills)} must be greater than 0";
                            }
                            break;

                        case nameof(RemainingSkillPoints):
                            if (RemainingSkillPoints != 0)
                            {
                                yield return $"Please use all of the provided skill points";
                            }
                            break;
                    }
                    break;
            }

            yield break;
        }

        private static string RangeErrorMessage<T>(string propertyName, Range<T> range, string suffix = null) => $"{propertyName} must be between {range.Minimum} and {range.Maximum} {suffix}";

        private static string RangeErrorMessage(string propertyName, Range<DateTime> range, string suffix = null) => $"{propertyName} must be between {range.Minimum:d} and {range.Maximum:d} {suffix}";

        private void OnIndustryIdChanged()
        {
            Data.Founder.MandatorySkills.Clear();

            Data.Founder.OptionalSkills.Clear();

            if (SelectedDifficultyLevel == null ||
                !StaticData.PeopleData.MandatorySkillDefinitions.ContainsKey(Data.IndustryId))
            {
                return;
            }

            MandatorySkillDefinitions = StaticData.PeopleData.MandatorySkillDefinitions[Data.IndustryId];

            var maxOptionalSkillCount = SelectedDifficultyLevel.MaxOptionalSkillCount;

            var initialSkillValue = (byte)(SelectedDifficultyLevel.MaxTotalSkillPoints / (MandatorySkillDefinitions.Count() + maxOptionalSkillCount));

            foreach (var mandatorySkillDefinition in MandatorySkillDefinitions)
            {
                AddNewSkill(mandatorySkillDefinition, initialSkillValue, true);
            }

            while (Data.Founder.OptionalSkills.Count < maxOptionalSkillCount)
            {
                AddNewSkill(null, 0, false);
            }

            OnPropertyChanged(nameof(RemainingSkillPoints));
        }

        private void AddNewSkill(SkillDefinition skillDefinition, byte initialValue, bool isMandatory)
        {
            var skill = new IdValuePair<SkillDefinition, byte>(skillDefinition, initialValue);

            skill.ValueChanging += OnSkillValueChanging;

            skill.ValueChanged += OnSkillValueChanged;

            RegisterForValidation(skill);

            if (isMandatory)
            {
                Data.Founder.MandatorySkills.Add(skill);

                return;
            }

            skill.IdChanged += (s, e) =>
            {
                if (e.CurrentValue != e.PreviousValue)
                {
                    OnPropertyChanged(nameof(AvailableOptionalSkillDefinitions));
                }
            };

            Data.Founder.OptionalSkills.Add(skill);
        }

        private void OnSkillValueChanged(object sender, ValueChangedEventArgs<byte> args)
        {
            OnPropertyChanged(nameof(RemainingSkillPoints));
        }

        private void OnSelectedDifficultyChanged()
        {
            OnIndustryIdChanged();

            if (SelectedDifficultyLevel != null)
            {
                Data.InitialFunds = SelectedDifficultyLevel.InitialFunds;
            }
            else
            {
                Data.InitialFunds = 0;
            }

            OnPropertyChanged(nameof(RemainingSkillPoints));
        }

        private void OnSkillValueChanging(object sender, ValueChangingEventArgs<byte> args)
        {
            var difference = args.NewValue - args.OriginalValue;

            args.IsCancelled = RemainingSkillPoints < difference;
        }

        private bool CanContinue(object data)
        {
            OnPropertyChanged(nameof(IsNavigateToFounderSettingsEnabled));

            var targetStep = (NewGameStep)CurrentStepNumber + 1;

            switch (targetStep)
            {
                case NewGameStep.ReturnToMainMenu:
                case NewGameStep.GameSettings:
                    return true;
                case NewGameStep.FounderSettings:
                    return CanNavigateToFounderSettings();
                case NewGameStep.StartNewGame:
                    return CanStartNewGame();
            }

            return false;
        }

        private void Back() => NavigateToStep((NewGameStep)CurrentStepNumber - 1);

        private void Continue() => NavigateToStep((NewGameStep)CurrentStepNumber + 1);

        private void NavigateToStep(NewGameStep targetStep)
        {
            switch (targetStep)
            {
                case NewGameStep.ReturnToMainMenu:
                    ViewStateService.NavigateToMainMenuView();
                    break;
                case NewGameStep.GameSettings:
                    CurrentStepNumber = 0;
                    break;
                case NewGameStep.FounderSettings:
                    CurrentStepNumber = 1;
                    break;
                case NewGameStep.StartNewGame:
                    ConfirmStartNewGame();
                    break;
            }
        }

        private void ChangeHelpText(object data)
        {
            var helpTextKey = data as string;

            if (data == null || _helpText == null || !_helpText.ContainsKey(helpTextKey))
            {
                HelpText = null;

                return;
            }

            HelpText = _helpText[helpTextKey];
        }

        private bool CanNavigateToFounderSettings()
        {
            if (Data == null)
            {
                return false;
            }

            if (string.IsNullOrWhiteSpace(Data.CompanyName) ||
                !Data.CompanyName.Trim().Length.Between(Limits.CompanyNameLength))
            {
                return false;
            }

            if (!Data.NumberOfCompanies.Between(Limits.NumberOfCompanies))
            {
                return false;
            }

            if (!Data.WeeksPerTurn.Between(Limits.WeeksPerTurn))
            {
                return false;
            }

            if (!Data.StartDate.Between(Limits.StartDate))
            {
                return false;
            }

            if (SelectedDifficultyLevel == null)
            {
                return false;
            }

            if (Data.IndustryId == 0)
            {
                return false;
            }

            return true;
        }

        private bool CanStartNewGame()
        {
            if (HasErrors)
            {
                return false;
            }

            if (!CanNavigateToFounderSettings())
            {
                return false;
            }

            if (string.IsNullOrWhiteSpace(Data.Founder.FirstName) ||
                !Data.Founder.FirstName.Length.Between(Limits.FirstNameLength))
            {
                return false;
            }

            if (string.IsNullOrWhiteSpace(Data.Founder.LastName) ||
                !Data.Founder.LastName.Length.Between(Limits.LastNameLength))
            {
                return false;
            }

            if (!StaticData.PeopleData.Genders.ContainsKey(Data.Founder.Gender))
            {
                return false;
            }

            var birthdayRange = GetBirthdayRange();

            if (!Data.Founder.Birthday.Between(birthdayRange))
            {
                return false;
            }

            if (RemainingSkillPoints < 0)
            {
                return false;
            }

            if (Data.Founder.OptionalSkills.Any(s => s.Id == null))
            {
                return false;
            }

            return true;
        }

        private void ConfirmStartNewGame()
        {
            var viewModel = Utilities.CreateMessagePopup(new MessagePopupSetup
            {
                ViewStateService = ViewStateService,
                Title = UiText.ConfirmNewGame,
                Message = UiText.ConfirmNewGameDetails,
                DismissLabel = UiText.Cancel,
                MaxWidth = 1000
            });

            viewModel.AddOption(UiText.Continue, (s, e) => StartNewGame());
        }

        private void StartNewGame()
        {
            var gameId = TryGetAuthorizedResource(() => ResourceClientService.CreateNewGame(Data), StartNewGame, ViewStateService.NavigateToMainMenuView);

            if (gameId == default)
            {
                return;
            }

            ViewStateService.NavigateToGameView(gameId);
        }

        private static void IncreaseSkillValue(object obj)
        {
            var value = (IdValuePair<SkillDefinition, byte>)obj;

            value.Value++;
        }

        private static bool CanIncreaseSkillValue(object obj)
        {
            var value = (IdValuePair<SkillDefinition, byte>)obj;

            if (value == null || value.Id == null)
            {
                return false;
            }

            return value.Id.Value.Maximum > value.Value;
        }

        private static void DecreaseSkillValue(object obj)
        {
            var value = (IdValuePair<SkillDefinition, byte>)obj;

            value.Value--;
        }

        private static bool CanDecreaseSkillValue(object obj)
        {
            var value = (IdValuePair<SkillDefinition, byte>)obj;

            if (value == null || value.Id == null)
            {
                return false;
            }

            return value.Id.Value.Minimum < value.Value;
        }

        private IDictionary<string, string> _helpText;

        private enum NewGameStep
        {
            ReturnToMainMenu = -1,
            GameSettings = 0,
            FounderSettings = 1,
            StartNewGame = 2
        }
    }
}