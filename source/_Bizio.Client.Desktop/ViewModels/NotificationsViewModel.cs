﻿using Bizio.Client.Desktop.Model;
using Bizio.Client.Desktop.Services;
using BlackPlain.DI;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels
{
    public class NotificationsViewModel : ViewModel, INotificationsViewModel
    {
        public IEnumerable<Notification> Notifications
        {
            get
            {
                return GetField<IEnumerable<Notification>>();
            }
            set
            {
                SetField(value);
            }
        }

        public ICommand BackCommand => GetAsyncCommand(Back);

        public Notification SelectedNotification
        {
            get
            {
                return GetField<Notification>();
            }
            set
            {
                if (value != null && value.IsRead == false)
                {
                    _hasUpdatedNotifications = value.IsRead = true;
                }

                SetField(value);
            }
        }

        public NotificationsViewModel(IHub hub,
            INotificationService notificationService,
            IViewStateService viewStateService,
            IAuthorizationClientService authorizationClientService)
            : base(hub)
        {
            _notificationService = notificationService;

            _viewStateService = viewStateService;

            _authorizationClientService = authorizationClientService;
        }

        public override void OnEscapePressed()
        {
            base.OnEscapePressed();

            Back();
        }

        protected override void LoadViewModel(object state)
        {
            base.LoadViewModel(state);

            Notifications = _notificationService.RefreshNotifications(_authorizationClientService.UserId.Value).OrderByDescending(n => n.DateCreated);
        }

        private void Back()
        {
            if (_hasUpdatedNotifications)
            {
                _notificationService.SaveNotifications(_authorizationClientService.UserId.Value, Notifications);
            }

            _viewStateService.NavigateToMainMenuView();
        }

        private bool _hasUpdatedNotifications;

        private readonly IViewStateService _viewStateService;

        private readonly INotificationService _notificationService;

        private readonly IAuthorizationClientService _authorizationClientService;
    }
}