﻿using Bizio.Client.Desktop.Core;
using Bizio.Client.Desktop.Model.Companies;
using Bizio.Client.Desktop.Model.Core;
using Bizio.Client.Desktop.Model.Game;
using Bizio.Client.Desktop.Model.Game.ActionData;
using Bizio.Client.Desktop.Model.People;
using Bizio.Client.Desktop.Services;
using Bizio.Client.Desktop.ViewModels.Popups;
using Bizio.Client.Desktop.Views.Popups;
using BlackPlain.DI;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels
{
    public class PeoplePanelViewModel : GameFragmentViewModelBase, IPeoplePanelViewModel
    {
        public IEnumerable<Person> UnemployedPeople
        {
            get
            {
                if (Data == null)
                {
                    return null;
                }

                return Data.PeopleData.People.Where(p => !p.WorkHistory.Any() || p.WorkHistory.All(wh => wh.EndDate.HasValue) && !UserCompany.Prospects.Any(ucp => ucp.PersonId == p.Id)).ToList();
            }
        }

        public Person SelectedPerson
        {
            get
            {
                return GetField<Person>();
            }
            set
            {
                SetField(value);

                OnPropertyChanged(nameof(HasRecruitedSelectedPerson));

                OnPropertyChanged(nameof(HasMadeOfferToSelectedPerson));
            }
        }

        public Prospect SelectedProspect
        {
            get
            {
                return GetField<Prospect>();
            }
            set
            {
                SetField(value);

                OnPropertyChanged(nameof(HasInterviewedSelectedProspect));

                OnPropertyChanged(nameof(HasMadeOfferToSelectedProspect));
            }
        }

        public bool HasRecruitedSelectedPerson
        {
            get
            {
                return SelectedPerson != null && FindTurnAction<RecruitPersonActionData>(ta => ta.Data.PersonId == SelectedPerson.Id) != null;
            }
        }

        public bool HasMadeOfferToSelectedPerson
        {
            get
            {
                return SelectedPerson != null && HasMadeOfferTo(SelectedPerson.Id);
            }
        }

        public bool HasInterviewedSelectedProspect
        {
            get
            {
                return SelectedProspect != null && FindTurnAction<InterviewProspectActionData>(ta => ta.Data.PersonId == SelectedProspect.PersonId) != null;
            }
        }

        public bool HasMadeOfferToSelectedProspect
        {
            get
            {
                return SelectedProspect != null && HasMadeOfferTo(SelectedProspect.PersonId);
            }
        }

        public ICommand RecruitCommand => GetCommand(Recruit, CanRecruit);

        public ICommand CancelRecruitCommand => GetCommand(CancelRecruit);

        public ICommand InterviewCommand => GetCommand(Interview, CanInterview);

        public ICommand CancelInterviewCommand => GetCommand(CancelInterview);

        public ICommand DraftOfferCommand => GetCommand(DraftOffer, CanDraftOffer);

        public ICommand CancelOfferCommand => GetCommand(CancelOffer);

        public PeoplePanelViewModel(IHub hub,
            IViewStateService viewStateService,
            IAuthorizationClientService authorizationClientService,
            IResourceClientService resourceClientService)
            : base(hub, viewStateService, authorizationClientService, resourceClientService)
        {
        }

        public override void RefreshGameData(GameData data, ObservableCollection<IActionData> turnActions)
        {
            base.RefreshGameData(data, turnActions);

            Person person;

            foreach (var turnAction in turnActions)
            {
                switch ((ActionType)turnAction.ActionTypeId)
                {
                    case ActionType.RecruitPerson:
                        var recruitAction = turnAction as RecruitPersonActionData;

                        person = FindPerson(recruitAction.Data.PersonId);

                        person.Status = HiringProcessStatus.RecruitPending;
                        break;

                    case ActionType.InterviewProspect:
                        var interviewAction = turnAction as InterviewProspectActionData;

                        person = FindPerson(interviewAction.Data.PersonId);

                        person.Status = HiringProcessStatus.InterviewPending;
                        break;

                    case ActionType.MakeOffer:
                        var makeOfferAction = turnAction as MakeOfferActionData;

                        person = FindPerson(makeOfferAction.Data.PersonId);

                        person.Status = HiringProcessStatus.OfferPending;
                        break;
                }
            }

            OnPropertyChanged(nameof(UnemployedPeople));
        }

        protected override bool CloseSubPanel(object state)
        {
            var panel = state.AsEnum<PeopleSubPanel>();

            switch (panel)
            {
                case PeopleSubPanel.Unemployed:
                    if (SelectedPerson != null)
                    {
                        SelectedPerson = null;

                        return true;
                    }
                    break;

                case PeopleSubPanel.Prospective:
                    if (SelectedProspect != null)
                    {
                        SelectedProspect = null;

                        return true;
                    }
                    break;
            }

            return false;
        }

        private bool HasMadeOfferTo(int personId)
        {
            return FindTurnAction<MakeOfferActionData>(ta => ta.Data.PersonId == personId) != null;
        }

        protected bool CanRecruit()
        {
            return Data != null &&
                SelectedPerson != null &&
                CanUserCompanyPerformActionType(ActionType.RecruitPerson) &&
                !UserCompany.Prospects.Any(p => p.PersonId == SelectedPerson.Id) &&
                !HasMadeOfferToSelectedPerson &&
                !HasRecruitedSelectedPerson;
        }

        protected void Recruit()
        {
            var turnAction = new RecruitPersonActionData(SelectedPerson.Id);

            turnAction.Cancelled += (s, e) =>
            {
                SelectedPerson.Status = HiringProcessStatus.None;

                TurnActions.Remove(turnAction);

                OnPropertyChanged(nameof(HasRecruitedSelectedPerson));
            };

            SelectedPerson.Status = HiringProcessStatus.RecruitPending;

            TurnActions.Add(turnAction);

            OnPropertyChanged(nameof(HasRecruitedSelectedPerson));
        }

        private void CancelRecruit()
        {
            var turnAction = FindTurnAction<RecruitPersonActionData>(ta => ta.Data.PersonId == SelectedPerson.Id);

            turnAction.Cancel();
        }

        protected bool CanInterview()
        {
            return Data != null &&
                SelectedProspect != null &&
                CanUserCompanyPerformActionType(ActionType.InterviewProspect) &&
                !HasMadeOfferToSelectedProspect &&
                !HasInterviewedSelectedProspect;
        }

        protected void Interview()
        {
            var turnAction = new InterviewProspectActionData(SelectedProspect.PersonId);

            turnAction.Cancelled += (s, e) =>
            {
                TurnActions.Remove(turnAction);

                SelectedProspect.Status = HiringProcessStatus.None;

                OnPropertyChanged(nameof(HasInterviewedSelectedProspect));
            };

            SelectedProspect.Status = HiringProcessStatus.InterviewPending;

            TurnActions.Add(turnAction);

            OnPropertyChanged(nameof(HasInterviewedSelectedProspect));
        }

        private void CancelInterview()
        {
            var turnAction = FindTurnAction<InterviewProspectActionData>(ta => ta.Data.PersonId == SelectedProspect.PersonId);

            turnAction.Cancel();
        }

        protected bool CanDraftOffer(object state)
        {
            var canMakeOffer = Data != null && CanUserCompanyPerformActionType(ActionType.MakeOffer);

            if (!canMakeOffer)
            {
                return false;
            }

            if (state is Person)
            {
                return !HasMadeOfferToSelectedPerson && !HasRecruitedSelectedPerson;
            }

            if (state is Prospect)
            {
                return !HasMadeOfferToSelectedProspect && !HasInterviewedSelectedProspect;
            }

            return false;
        }

        protected void DraftOffer(object state)
        {
            var person = state as Person;

            if (person is null)
            {
                person = FindPerson(SelectedProspect.PersonId);
            }

            var viewModel = new MakeOfferPopupViewModel(ViewStateService, person);

            viewModel.MakingOffer += CanMakeOffer;

            viewModel.OfferMade += MakeOffer;

            ViewStateService.PushPopup<MakeOfferPopupView>(viewModel);
        }

        protected void MakeOffer(object sender, MakeOfferEventArgs args)
        {
            var turnAction = new MakeOfferActionData(args.Person.Id, args.OfferValue);

            turnAction.Cancelled += (s, e) =>
            {
                TurnActions.Remove(turnAction);

                TrySetProspectStatus(args.Person.Id, HiringProcessStatus.None);

                OnPropertyChanged(nameof(HasMadeOfferToSelectedPerson));

                OnPropertyChanged(nameof(HasMadeOfferToSelectedProspect));
            };

            TurnActions.Add(turnAction);

            TrySetProspectStatus(args.Person.Id, HiringProcessStatus.OfferPending);

            OnPropertyChanged(nameof(HasMadeOfferToSelectedPerson));

            OnPropertyChanged(nameof(HasMadeOfferToSelectedProspect));
        }

        private void TrySetProspectStatus(int personId, HiringProcessStatus status)
        {
            var prospect = FindProspect(personId);

            if (prospect != null)
            {
                prospect.Status = status;
            }

            var person = FindPerson(personId);

            person.Status = status;
        }

        private void CancelOffer(object state)
        {
            int personId;

            if (state is Person person)
            {
                personId = person.Id;

                person.Status = HiringProcessStatus.None;
            }
            else
            {
                var prospect = state as Prospect;

                personId = prospect.PersonId;

                prospect.Status = HiringProcessStatus.None;
            }

            var turnAction = FindTurnAction<MakeOfferActionData>(ta => ta.Data.PersonId == personId);

            turnAction.Cancel();
        }

        protected void CanMakeOffer(object sender, CancelEventArgs<MakeOfferEventArgs> args)
        {
            args.Cancel = Data == null ||
                !CanUserCompanyPerformActionType(ActionType.MakeOffer) ||
                args.Data.OfferValue > UserCompany.Money ||
                args.Data.OfferValue <= 0;
        }

        private enum PeopleSubPanel
        {
            Unemployed = 0,
            Prospective = 1
        }
    }
}