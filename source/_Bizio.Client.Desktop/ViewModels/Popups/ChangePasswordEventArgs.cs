﻿using System;

namespace Bizio.Client.Desktop.ViewModels.Popups
{
    public class ChangePasswordEventArgs : EventArgs
    {
        public string UserName { get; }

        public string Token { get; }

        public string Password { get; }

        public ChangePasswordEventArgs(string userName, string token, string password)
        {
            UserName = userName;

            Token = token;

            Password = password;
        }
    }
}