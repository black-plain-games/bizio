﻿using Bizio.Client.Desktop.Core;
using Bizio.Client.Desktop.Model.Core;
using Bizio.Client.Desktop.Services;
using Bizio.Client.Desktop.Views.Popups;
using System;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels.Popups
{
    public class ChangePasswordPopupViewModel : PopupViewModelBase
    {
        public event EventHandler<EventResultArgs<LoginEventArgs, bool>> Login;

        public event EventHandler<EventResultArgs<ChangePasswordEventArgs, bool>> ChangePassword;

        public event EventHandler<CancelEventArgs<ChangePasswordEventArgs>> ChangingPassword;

        public string UserName { get; }

        public string Token
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public string Password
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public string ErrorMessage
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public ICommand ChangePasswordCommand => GetAsyncCommand(ChangePassword, ChangingPassword, () => new ChangePasswordEventArgs(UserName, Token, Password), true);

        public ChangePasswordPopupViewModel(
            IViewStateService viewStateService,
            IAuthorizationClientService authorizationClientService,
            string userName)
            : base(viewStateService)
        {
            _authorizationClientService = authorizationClientService;

            UserName = userName;

            ChangePassword += OnChangePassword;

            ChangingPassword += OnChangingPassword;
        }

        private void OnChangingPassword(object sender, CancelEventArgs<ChangePasswordEventArgs> args)
        {
            if (string.IsNullOrWhiteSpace(args.Data.Token) ||
                string.IsNullOrWhiteSpace(args.Data.Password) ||
                args.Data.Token.Length < 6 ||
                args.Data.Password.Length < 6)
            {
                args.Cancel = true;
            }
        }

        private void OnChangePassword(object sender, EventResultArgs<ChangePasswordEventArgs, bool> args)
        {
            if (!_authorizationClientService.ResetPassword(args.Argument.UserName, args.Argument.Token, args.Argument.Password))
            {
                args.DoDismiss = false;

                args.Result = false;

                ErrorMessage = "Password reset failed";

                return;
            }

            args.DoDismiss = true;

            args.Result = true;

            var loginResultArgs = new EventResultArgs<LoginEventArgs, bool>(new LoginEventArgs(args.Argument.UserName, args.Argument.Password));

            Login(this, loginResultArgs);

            if (!loginResultArgs.Result)
            {
                args.Result = false;

                var viewModel = Utilities.CreateMessagePopup(_viewStateService, "Unusual error", "Your password was reset but we couldn't log you in. If this problem persists please contact support.", "Okay");

                _viewStateService.PushPopup<MessagePopupView>(viewModel);
            }
        }

        private readonly IAuthorizationClientService _authorizationClientService;
    }
}