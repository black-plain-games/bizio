﻿using System;

namespace Bizio.Client.Desktop.ViewModels.Popups
{
    public class EditAllocationEventArgs : EventArgs
    {
        public int ProjectId { get; }

        public int PersonId { get; }

        public byte RoleId { get; }

        public byte Percent { get; }

        public EditAllocationEventArgs(int projectId, int personId, byte roleId, byte percent)
        {
            ProjectId = projectId;

            PersonId = personId;

            RoleId = roleId;

            Percent = percent;
        }
    }
}