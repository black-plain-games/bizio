﻿using Bizio.Client.Desktop.Model.Companies;
using Bizio.Client.Desktop.Model.Core;
using Bizio.Client.Desktop.Model.Game.ActionData;
using Bizio.Client.Desktop.Model.People;
using Bizio.Client.Desktop.Model.Projects;
using Bizio.Client.Desktop.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels.Popups
{
    public class EditAllocationPopupViewModel : PopupViewModelBase
    {
        public event EventHandler<EditAllocationEventArgs> EditAllocation;

        public event EventHandler<CancelEventArgs<EditAllocationEventArgs>> EditingAllocation;

        public IReadOnlyDictionary<byte, string> Roles { get; }

        public IEnumerable<Person> People
        {
            get
            {
                var output = _allPeople;

                if (Project == null || CanEditPerson == false)
                {
                    return output;
                }

                return output.Where(p =>
                !_allocations.Any(a => a.PersonId == p.Id && a.ProjectId == Project.Id && a.Percent > 0) &&
                !_turnActions.Any(ta => ta.Data.PersonId == p.Id && ta.Data.ProjectId == Project.Id && ta.Data.Percentage > 0));
            }
        }

        public IEnumerable<Project> Projects { get; }

        public bool CanEditPerson { get; }

        public bool CanEditProject { get; }

        public double MaxPercent => GetMaxPercent();

        public Project Project
        {
            get
            {
                return GetField<Project>();
            }
            set
            {
                SetField(value);

                OnPropertyChanged(nameof(People));
            }
        }

        public Person Person
        {
            get
            {
                return GetField<Person>();
            }
            set
            {
                SetField(value);

                OnPropertyChanged(nameof(MaxPercent));
            }
        }

        public double Percent
        {
            get
            {
                return GetField<double>();
            }
            set
            {
                SetField(value);
            }
        }

        public byte RoleId
        {
            get
            {
                return GetField<byte>();
            }
            set
            {
                SetField(value);
            }
        }

        public ICommand EditAllocationCommand => GetCommand(EditAllocation, EditingAllocation, () => new EditAllocationEventArgs(Project.Id, Person.Id, RoleId, GetPercent()));

        public EditAllocationPopupViewModel(IViewStateService viewStateService,
            IReadOnlyDictionary<byte, string> roles,
            IEnumerable<Allocation> allocations,
            IEnumerable<AdjustAllocationActionData> turnActions,
            Person person,
            IEnumerable<Project> projects)
            : base(viewStateService)
        {
            CanEditPerson = false;

            CanEditProject = true;

            _allocations = allocations;

            _turnActions = turnActions;

            Roles = roles;

            RoleId = Roles.First().Key;

            _allPeople = new[] { person };

            Person = person;

            Projects = projects;

            Project = Projects.First();

            EditingAllocation += CancelOnHasErrors;
        }

        public EditAllocationPopupViewModel(IViewStateService viewStateService,
            IReadOnlyDictionary<byte, string> roles,
            IEnumerable<Allocation> allocations,
            IEnumerable<AdjustAllocationActionData> turnActions,
            Project project,
            IEnumerable<Person> people)
            : base(viewStateService)
        {
            CanEditPerson = true;

            CanEditProject = false;

            _allocations = allocations;

            _turnActions = turnActions;

            Roles = roles;

            RoleId = Roles.First().Key;

            Projects = new[] { project };

            Project = project;

            _allPeople = people;

            Person = People.First();

            EditingAllocation += CancelOnHasErrors;
        }

        public EditAllocationPopupViewModel(IViewStateService viewStateService,
            IReadOnlyDictionary<byte, string> roles,
            IEnumerable<Allocation> allocations,
            IEnumerable<AdjustAllocationActionData> turnActions,
            Person person,
            Project project,
            byte roleId,
            byte percent)
            : base(viewStateService)
        {
            CanEditPerson = false;

            CanEditProject = false;

            _allocations = allocations;

            _turnActions = turnActions;

            Roles = roles;

            RoleId = roleId;

            Projects = new[] { project };

            Project = project;

            _allPeople = new[] { person };

            Person = person;

            Percent = Math.Round(((double)percent) / 2.55);

            EditingAllocation += CancelOnHasErrors;
        }

        protected override IEnumerable<string> Validate(string propertyName)
        {
            if (propertyName != nameof(Percent))
            {
                yield break;
            }

            if (Percent <= 0)
            {
                yield return "Percent must be greater than 0";
            }

            var maxPercent = MaxPercent * 100.0;

            if (Percent > maxPercent)
            {
                yield return $"Percent must be less than or equal to {maxPercent:0}";
            }

            if (Math.IEEERemainder(Percent, 1) != 0)
            {
                yield return "Percent must be a whole number";
            }
        }

        private byte GetPercent()
        {
            return (byte)(Percent / 100.0 * byte.MaxValue);
        }

        private double GetMaxPercent()
        {
            var maxPercent = 1.0;

            if (Project == null || Person == null)
            {
                return maxPercent;
            }

            var existingTurnActions = _turnActions.Where(ta => ta.Data.PersonId == Person.Id && ta.Data.ProjectId != Project.Id);

            var existingAllocations = _allocations.Where(a => a.PersonId == Person.Id && a.ProjectId != Project.Id &&
                !existingTurnActions.Any(eta => eta.Data.PersonId == a.PersonId && eta.Data.ProjectId == a.ProjectId));

            double currentAllocation = existingTurnActions.Sum(ta => ta.Data.Percentage) + existingAllocations.Sum(a => a.Percent);

            maxPercent -= currentAllocation / 255.0;

            return maxPercent;
        }

        private readonly IEnumerable<Allocation> _allocations;

        private readonly IEnumerable<AdjustAllocationActionData> _turnActions;

        private readonly IEnumerable<Person> _allPeople;
    }
}