﻿using Bizio.Client.Desktop.Model.People;
using System;

namespace Bizio.Client.Desktop.ViewModels.Popups
{
    public class EditSalaryEventArgs : EventArgs
    {
        public Person Person { get; }

        public decimal Salary { get; }

        public EditSalaryEventArgs(Person person, decimal salary)
        {
            Person = person;

            Salary = salary;
        }
    }
}