﻿using Bizio.Client.Desktop.Core;
using Bizio.Client.Desktop.Model.Core;
using Bizio.Client.Desktop.Model.People;
using Bizio.Client.Desktop.Services;
using System;
using System.Collections.Generic;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels.Popups
{
    public class EditSalaryPopupViewModel : PopupViewModelBase
    {
        public event EventHandler<EditSalaryEventArgs> EditSalary;

        public event EventHandler<CancelEventArgs<EditSalaryEventArgs>> EditingSalary;

        public Person Person { get; }

        public decimal Salary
        {
            get
            {
                return GetField<decimal>();
            }
            set
            {
                SetField(value);
            }
        }

        public ICommand EditSalaryCommand => GetCommand(EditSalary, EditingSalary, () => new EditSalaryEventArgs(Person, Salary));

        public EditSalaryPopupViewModel(IViewStateService viewStateService, Person person, decimal salary)
            : base(viewStateService)
        {
            Person = person;

            Salary = salary;

            EditingSalary += OnEditingSalary;
        }

        protected override IEnumerable<string> Validate(string propertyName)
        {
            if (propertyName != nameof(Salary))
            {
                yield break;
            }

            if (Salary > Limits.MaximumMoneyAmount)
            {
                yield return $"Salary must less than or equal to {Limits.MaximumMoneyAmount:C0}";
            }
        }

        private void OnEditingSalary(object sender, CancelEventArgs<EditSalaryEventArgs> e)
        {
            if (e.Data.Salary > Limits.MaximumMoneyAmount)
            {
                e.Cancel = true;
            }
        }
    }
}