﻿using System;

namespace Bizio.Client.Desktop.ViewModels.Popups
{
    public class ForgotPasswordEventArgs : EventArgs
    {
        public string UserName { get; }

        public ForgotPasswordEventArgs(string userName)
        {
            UserName = userName;
        }
    }
}