﻿using Bizio.Client.Desktop.Model.Core;
using Bizio.Client.Desktop.Services;
using Bizio.Client.Desktop.Views.Popups;
using System;
using System.Net.Mail;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels.Popups
{
    public class ForgotPasswordPopupViewModel : PopupViewModelBase
    {
        public event EventHandler<EventResultArgs<LoginEventArgs, bool>> Login;

        public event EventHandler<EventResultArgs<GeneratePasswordResetTokenArgs, bool>> GeneratePasswordResetToken;

        public event EventHandler<CancelEventArgs<GeneratePasswordResetTokenArgs>> GeneratingPasswordResetToken;

        public string UserName
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public string EmailAddress
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public string ErrorMessage
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public ICommand GeneratePasswordResetTokenCommand => GetAsyncCommand(GeneratePasswordResetToken, GeneratingPasswordResetToken, () => new GeneratePasswordResetTokenArgs(UserName, EmailAddress), true);

        public ForgotPasswordPopupViewModel(IViewStateService viewStateService, IAuthorizationClientService authorizationClientService)
            : base(viewStateService)
        {
            _authorizationClientService = authorizationClientService;

            GeneratePasswordResetToken += OnGeneratePasswordResetToken;

            GeneratingPasswordResetToken += OnGeneratingPasswordResetToken;
        }

        private void OnGeneratingPasswordResetToken(object sender, CancelEventArgs<GeneratePasswordResetTokenArgs> args)
        {
            var isEmailAddressValid = true;

            try
            {
                var email = new MailAddress(args.Data.EmailAddress).Address;
            }
            catch
            {
                isEmailAddressValid = false;
            }

            if (string.IsNullOrWhiteSpace(args.Data.UserName) ||
                string.IsNullOrWhiteSpace(args.Data.EmailAddress) ||
                args.Data.UserName.Length < 6 ||
                !isEmailAddressValid)
            {
                args.Cancel = true;
            }
        }

        private void OnGeneratePasswordResetToken(object sender, EventResultArgs<GeneratePasswordResetTokenArgs, bool> args)
        {
            if (!_authorizationClientService.GeneratePasswordResetToken(args.Argument.UserName, args.Argument.EmailAddress))
            {
                args.DoDismiss = false;

                args.Result = false;

                ErrorMessage = _authorizationClientService.LastErrorMessage;

                return;
            }

            args.DoDismiss = true;

            args.Result = true;

            var viewModel = new ChangePasswordPopupViewModel(_viewStateService, _authorizationClientService, args.Argument.UserName);

            viewModel.Login += Login;

            _viewStateService.PushPopup<ChangePasswordPopupView>(viewModel);
        }

        private readonly IAuthorizationClientService _authorizationClientService;
    }
}