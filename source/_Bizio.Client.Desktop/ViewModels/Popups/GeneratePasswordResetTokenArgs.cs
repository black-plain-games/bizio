﻿using System;

namespace Bizio.Client.Desktop.ViewModels.Popups
{
    public class GeneratePasswordResetTokenArgs : EventArgs
    {
        public string UserName { get; }

        public string EmailAddress { get; }

        public GeneratePasswordResetTokenArgs(string userName, string emailAddress)
        {
            UserName = userName;

            EmailAddress = emailAddress;
        }
    }
}