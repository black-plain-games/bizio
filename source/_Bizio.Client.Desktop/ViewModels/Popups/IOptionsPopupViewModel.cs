﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Bizio.Client.Desktop.ViewModels.Popups
{
    public interface IOptionsPopupViewModel : IPopupViewModel
    {
        IEnumerable<IPopupOption> Options { get; }

        void AddOption(string label);

        void AddOption(string label, EventHandler selected);

        void AddOption(string label, EventHandler selected, bool doesDismiss);

        void AddOption(string label, EventHandler selected, EventHandler<CancelEventArgs> selecting);

        void AddOption(string label, EventHandler selected, EventHandler<CancelEventArgs> selecting, bool doesDismiss);
    }
}