﻿using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels.Popups
{
    public interface IPopupOption
    {
        string Label { get; }

        ICommand SelectCommand { get; }
    }
}