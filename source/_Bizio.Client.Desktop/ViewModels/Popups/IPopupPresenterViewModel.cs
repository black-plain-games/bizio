﻿using Bizio.Client.Desktop.Model.Core;
using System;
using System.Windows;

namespace Bizio.Client.Desktop.ViewModels.Popups
{
    public interface IPopupPresenterViewModel
    {
        event EventHandler<PopupChangedEventArgs> PopupChanged;

        IPopup CurrentPopup { get; }

        void PushPopup(IPopup popup);

        void PushPopup(IPopup popup, HorizontalAlignment horizontalAlignment);

        void PushPopup(IPopup popup, VerticalAlignment verticalAlignment);

        void PushPopup(IPopup popup, HorizontalAlignment horizontalAlignment, VerticalAlignment verticalAlignment);

        void PopPopup();

        void RemovePopup(IPopupViewModel popupViewModel);
    }
}