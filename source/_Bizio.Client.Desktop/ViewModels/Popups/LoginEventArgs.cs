﻿using System;

namespace Bizio.Client.Desktop.ViewModels.Popups
{
    public class LoginEventArgs : EventArgs
    {
        public string Username { get; }

        public string Password { get; }

        public LoginEventArgs(string username, string password)
        {
            Username = username;

            Password = password;
        }
    }
}