﻿using Bizio.Client.Desktop.Model.Core;
using Bizio.Client.Desktop.Services;
using Bizio.Client.Desktop.Views.Popups;
using System;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels.Popups
{
    public class LoginPopupViewModel : PopupViewModelBase
    {
        public event EventHandler<EventResultArgs<LoginEventArgs, bool>> Login;

        public event EventHandler<CancelEventArgs<LoginEventArgs>> LoggingIn;

        public string Username
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public string Password
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public string ErrorMessage
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public ICommand LoginCommand => GetAsyncCommand(Login, LoggingIn, () => new LoginEventArgs(Username, Password), true);

        public ICommand ForgotPasswordCommand => GetAsyncCommand(ShowForgotPasswordPopup, () => new ForgotPasswordEventArgs(Username));

        public LoginPopupViewModel(IViewStateService viewStateService, IAuthorizationClientService authorizationClientService)
            : base(viewStateService)
        {
            _authorizationClientService = authorizationClientService;

            Login += OnLogin;

            LoggingIn += OnLoggingIn;
        }

        private void OnLoggingIn(object sender, CancelEventArgs<LoginEventArgs> e)
        {
            if (string.IsNullOrWhiteSpace(Username) ||
                string.IsNullOrWhiteSpace(Password) ||
                Username.Length < 6 ||
                Password.Length < 6)
            {
                e.Cancel = true;
            }
        }

        private void OnLogin(object sender, EventResultArgs<LoginEventArgs, bool> e)
        {
            if (_authorizationClientService.Login(e.Argument.Username, e.Argument.Password))
            {
                e.DoDismiss = true;

                e.Result = true;

                return;
            }

            e.DoDismiss = false;

            e.Result = false;

            ErrorMessage = "Username or password is incorrect";
        }

        private void ShowForgotPasswordPopup(object sender, ForgotPasswordEventArgs args)
        {
            var viewModel = new ForgotPasswordPopupViewModel(_viewStateService, _authorizationClientService)
            {
                UserName = args.UserName
            };

            viewModel.Login += Login;

            _viewStateService.PushPopup<ForgotPasswordPopupView>(viewModel);
        }

        private readonly IAuthorizationClientService _authorizationClientService;
    }
}