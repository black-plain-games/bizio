﻿using Bizio.Client.Desktop.Model.People;
using System;

namespace Bizio.Client.Desktop.ViewModels.Popups
{
    public class MakeOfferEventArgs : EventArgs
    {
        public Person Person { get; }

        public decimal OfferValue { get; }

        public MakeOfferEventArgs(Person person, decimal offerValue)
        {
            Person = person;

            OfferValue = offerValue;
        }
    }
}