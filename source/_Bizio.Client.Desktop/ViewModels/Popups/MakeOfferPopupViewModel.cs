﻿using Bizio.Client.Desktop.Core;
using Bizio.Client.Desktop.Model.Core;
using Bizio.Client.Desktop.Model.People;
using Bizio.Client.Desktop.Services;
using System;
using System.Collections.Generic;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels.Popups
{
    public class MakeOfferPopupViewModel : PopupViewModelBase
    {
        public event EventHandler<MakeOfferEventArgs> OfferMade;

        public event EventHandler<CancelEventArgs<MakeOfferEventArgs>> MakingOffer;

        public Person Person { get; }

        public decimal OfferValue
        {
            get
            {
                return GetField<decimal>();
            }
            set
            {
                SetField(value);
            }
        }

        public ICommand MakeOfferCommand => GetCommand(OfferMade, MakingOffer, () => new MakeOfferEventArgs(Person, OfferValue));

        public MakeOfferPopupViewModel(IViewStateService viewStateService, Person person)
            : base(viewStateService)
        {
            Person = person;

            MakingOffer += CancelOnHasErrors;
        }

        protected override IEnumerable<string> Validate(string propertyName)
        {
            if (propertyName != nameof(OfferValue))
            {
                yield break;
            }

            if (OfferValue < 0)
            {
                yield return $"Offer must be greater than {0:C}";
            }

            if (OfferValue > Limits.MaximumMoneyAmount)
            {
                yield return $"Offer must be less than {Limits.MaximumMoneyAmount:C}";
            }
        }
    }
}