﻿using Bizio.Client.Desktop.Services;

namespace Bizio.Client.Desktop.ViewModels.Popups
{
    public class MessagePopupViewModel : OptionsPopupViewModelBase
    {
        public string Title { get; set; }

        public string Message { get; set; }

        public MessagePopupViewModel(IViewStateService viewStateService)
            : base(viewStateService)
        {
        }
    }
}