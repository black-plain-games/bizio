﻿using Bizio.Client.Desktop.Services;
using BlackPlain.Wpf.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels.Popups
{
    public abstract class OptionsPopupViewModelBase : PopupViewModelBase, IOptionsPopupViewModel
    {
        public IEnumerable<IPopupOption> Options => _options;

        protected OptionsPopupViewModelBase(IViewStateService viewStateService)
            : base(viewStateService)
        {
            _options = new List<IPopupOption>();
        }

        public void AddOption(string label) => AddOption(label, null, null, true);

        public void AddOption(string label, EventHandler selected) => AddOption(label, selected, null, true);

        public void AddOption(string label, EventHandler selected, bool doDismiss) => AddOption(label, selected, null, doDismiss);

        public void AddOption(string label, EventHandler selected, EventHandler<CancelEventArgs> selecting) => AddOption(label, selected, selecting, true);

        public void AddOption(string label, EventHandler selected, EventHandler<CancelEventArgs> selecting, bool doDismiss)
        {
            var option = new CustomPopupOption(label, x => IsBusy = x);

            if (selecting != null)
            {
                option.Selecting += selecting;
            }

            if (selected != null)
            {
                option.Selected += selected;
            }

            if (doDismiss)
            {
                option.Selecting += (s, e) => CanDismiss();

                option.Selected += (s, e) =>
                {
                    Dismiss();
                };
            }

            _options.Add(option);
        }

        private readonly ICollection<IPopupOption> _options;

        private class CustomPopupOption : IPopupOption
        {
            public string Label { get; }

            public ICommand SelectCommand => new RelayCommand(x => OnSelected(), x => OnSelecting());

            public event EventHandler Selected;

            public event EventHandler<CancelEventArgs> Selecting;

            private readonly Action<bool> _setIsBusy;

            public CustomPopupOption(string label, Action<bool> setIsBusy)
            {
                Label = label;

                _setIsBusy = setIsBusy;
            }

            private bool OnSelecting()
            {
                var args = new CancelEventArgs();

                Selecting?.Invoke(this, args);

                return !args.Cancel;
            }

            private void OnSelected()
            {
                Task.Factory.StartNew(() =>
                {
                    _setIsBusy(true);

                    Selected?.Invoke(this, EventArgs.Empty);

                    _setIsBusy(false);
                });
            }
        }
    }
}