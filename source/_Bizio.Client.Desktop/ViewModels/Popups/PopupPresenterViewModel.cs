﻿using Bizio.Client.Desktop.Model.Core;
using BlackPlain.DI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace Bizio.Client.Desktop.ViewModels.Popups
{
    public class PopupPresenterViewModel : ViewModel, IPopupPresenterViewModel
    {
        public event EventHandler<PopupChangedEventArgs> PopupChanged;

        public IPopup CurrentPopup => _popups.LastOrDefault();

        public HorizontalAlignment HorizontalAlignment
        {
            get
            {
                return GetField<HorizontalAlignment>();
            }
            set
            {
                SetField(value);
            }
        }

        public VerticalAlignment VerticalAlignment
        {
            get
            {
                return GetField<VerticalAlignment>();
            }
            set
            {
                SetField(value);
            }
        }

        public PopupPresenterViewModel(IHub hub)
            : base(hub)
        {
            _popups = new List<IPopup>();
        }

        public void PushPopup(IPopup popup)
        {
            PushPopup(popup, HorizontalAlignment.Center, VerticalAlignment.Center);
        }

        public void PushPopup(IPopup popup, HorizontalAlignment horizontalAlignment)
        {
            PushPopup(popup, horizontalAlignment, VerticalAlignment.Center);
        }

        public void PushPopup(IPopup popup, VerticalAlignment verticalAlignment)
        {
            PushPopup(popup, HorizontalAlignment.Center, verticalAlignment);
        }

        public void PushPopup(IPopup popup, HorizontalAlignment horizontalAlignment, VerticalAlignment verticalAlignment)
        {
            HorizontalAlignment = horizontalAlignment;

            VerticalAlignment = verticalAlignment;

            _popups.Add(popup);

            PopupChanged(this, new PopupChangedEventArgs(popup, PopupChange.Pushed, _popups.Count));

            OnPropertyChanged(nameof(CurrentPopup));
        }

        public void PopPopup() => RemovePopup(_popups.Last());

        public void RemovePopup(IPopupViewModel popupViewModel)
        {
            var popup = _popups.FirstOrDefault(p => p.ViewModel == popupViewModel);

            if (popup != null)
            {
                RemovePopup(popup);
            }
        }

        private void RemovePopup(IPopup popup)
        {
            _popups.Remove(popup);

            PopupChanged(this, new PopupChangedEventArgs(popup, PopupChange.Popped, _popups.Count));

            OnPropertyChanged(nameof(CurrentPopup));
        }

        protected ICollection<IPopup> _popups;
    }
}