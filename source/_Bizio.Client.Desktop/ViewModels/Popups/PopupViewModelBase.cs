﻿using Bizio.Client.Desktop.Core.Validation;
using Bizio.Client.Desktop.Model.Core;
using Bizio.Client.Desktop.Services;
using BlackPlain.Wpf.Core;
using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels.Popups
{
    public abstract class PopupViewModelBase : ValidationBase, IPopupViewModel
    {
        public bool IsBusy
        {
            get
            {
                return GetField<bool>();
            }
            set
            {
                SetField(value);
            }
        }

        public event EventHandler Dismissed;

        public event EventHandler<CancelEventArgs> Dismissing;

        public ICommand DismissCommand => new RelayCommand(x => Dismiss(), x => CanDismiss());

        protected PopupViewModelBase(IViewStateService viewStateService)
        {
            _viewStateService = viewStateService;
        }

        protected bool CanDismiss()
        {
            var args = new CancelEventArgs();

            Dismissing?.Invoke(this, args);

            return !args.Cancel;
        }

        protected void Dismiss()
        {
            _viewStateService.RemovePopup(this);

            Dismissed?.Invoke(this, EventArgs.Empty);
        }


        private void PerformAction<TArgs>(EventHandler<TArgs> action, Func<TArgs> argsGetter)
            where TArgs : EventArgs
        {
            action?.Invoke(this, argsGetter?.Invoke());

            Dismiss();
        }

        private void PerformActionAsync<TArgs>(EventHandler<TArgs> action, Func<TArgs> argsGetter, bool isBusy)
            where TArgs : EventArgs
        {
            var args = argsGetter?.Invoke();

            ExecuteAsync(null, x =>
            {
                action?.Invoke(this, args);

                Dismiss();
            }, isBusy);
        }


        private void PerformAction<TArgs, TResult>(EventHandler<EventResultArgs<TArgs, TResult>> action, Func<TArgs> argsGetter)
            where TArgs : EventArgs
        {
            var innerArgs = argsGetter?.Invoke();

            var args = new EventResultArgs<TArgs, TResult>(innerArgs);

            action?.Invoke(this, args);

            if (args.DoDismiss)
            {
                Dismiss();
            }
        }

        private void PerformActionAsync<TArgs, TResult>(EventHandler<EventResultArgs<TArgs, TResult>> action, Func<TArgs> argsGetter, bool isBusy)
            where TArgs : EventArgs
        {
            var innerArgs = argsGetter?.Invoke();

            var args = new EventResultArgs<TArgs, TResult>(innerArgs);

            ExecuteAsync(null, x =>
            {
                action?.Invoke(this, args);

                if (args.DoDismiss)
                {
                    Dismiss();
                }
            }, isBusy);
        }

        private bool CanPerformAction<TArgs>(EventHandler<CancelEventArgs<TArgs>> canPerformAction, Func<TArgs> argsGetter)
            where TArgs : EventArgs
        {
            if (!CanDismiss())
            {
                return false;
            }

            var args = new CancelEventArgs<TArgs>(argsGetter?.Invoke());

            if (canPerformAction != null)
            {
                foreach (EventHandler<CancelEventArgs<TArgs>> canPerformActionHandler in canPerformAction.GetInvocationList())
                {
                    canPerformActionHandler(this, args);

                    if (args.Cancel)
                    {
                        break;
                    }
                }
            }

            return !args.Cancel;
        }


        protected ICommand GetCommand<TArgs>(EventHandler<TArgs> action, Func<TArgs> argsGetter)
            where TArgs : EventArgs
        {
            return new RelayCommand(x => PerformAction(action, argsGetter), x => CanDismiss());
        }

        protected ICommand GetAsyncCommand<TArgs>(EventHandler<TArgs> action, Func<TArgs> argsGetter)
            where TArgs : EventArgs
        {
            return GetAsyncCommand(action, argsGetter, false);
        }

        protected ICommand GetAsyncCommand<TArgs>(EventHandler<TArgs> action, Func<TArgs> argsGetter, bool isBusy)
            where TArgs : EventArgs
        {
            return new RelayCommand(x => PerformActionAsync(action, argsGetter, isBusy), x => CanDismiss());
        }


        protected ICommand GetCommand<TArgs>(EventHandler<TArgs> action, EventHandler<CancelEventArgs<TArgs>> canPerformAction, Func<TArgs> argsGetter)
            where TArgs : EventArgs
        {
            return new RelayCommand(x => PerformAction(action, argsGetter), x => CanPerformAction(canPerformAction, argsGetter));
        }

        protected ICommand GetAsyncCommand<TArgs>(EventHandler<TArgs> action, EventHandler<CancelEventArgs<TArgs>> canPerformAction, Func<TArgs> argsGetter)
            where TArgs : EventArgs
        {
            return GetAsyncCommand(action, canPerformAction, argsGetter, false);
        }

        protected ICommand GetAsyncCommand<TArgs>(EventHandler<TArgs> action, EventHandler<CancelEventArgs<TArgs>> canPerformAction, Func<TArgs> argsGetter, bool isBusy)
            where TArgs : EventArgs
        {
            return new RelayCommand(x => PerformActionAsync(action, argsGetter, isBusy), x => CanPerformAction(canPerformAction, argsGetter));
        }


        protected ICommand GetCommand<TArgs, TResult>(EventHandler<EventResultArgs<TArgs, TResult>> action, Func<TArgs> argsGetter)
            where TArgs : EventArgs
        {
            return new RelayCommand(x => PerformAction(action, argsGetter), x => CanDismiss());
        }

        protected ICommand GetAsyncCommand<TArgs, TResult>(EventHandler<EventResultArgs<TArgs, TResult>> action, Func<TArgs> argsGetter)
            where TArgs : EventArgs
        {
            return GetAsyncCommand(action, argsGetter, false);
        }

        protected ICommand GetAsyncCommand<TArgs, TResult>(EventHandler<EventResultArgs<TArgs, TResult>> action, Func<TArgs> argsGetter, bool isBusy)
            where TArgs : EventArgs
        {
            return new RelayCommand(x => PerformActionAsync(action, argsGetter, isBusy), x => CanDismiss());
        }


        protected ICommand GetCommand<TArgs, TResult>(EventHandler<EventResultArgs<TArgs, TResult>> action, EventHandler<CancelEventArgs<TArgs>> canPerformAction, Func<TArgs> argsGetter)
            where TArgs : EventArgs
        {
            return new RelayCommand(x => PerformAction(action, argsGetter), x => CanPerformAction(canPerformAction, argsGetter));
        }

        protected ICommand GetAsyncCommand<TArgs, TResult>(EventHandler<EventResultArgs<TArgs, TResult>> action, EventHandler<CancelEventArgs<TArgs>> canPerformAction, Func<TArgs> argsGetter)
            where TArgs : EventArgs
        {
            return GetAsyncCommand(action, canPerformAction, argsGetter, false);
        }

        protected ICommand GetAsyncCommand<TArgs, TResult>(EventHandler<EventResultArgs<TArgs, TResult>> action, EventHandler<CancelEventArgs<TArgs>> canPerformAction, Func<TArgs> argsGetter, bool isBusy)
            where TArgs : EventArgs
        {
            return new RelayCommand(x => PerformActionAsync(action, argsGetter, isBusy), x => CanPerformAction(canPerformAction, argsGetter));
        }


        private void ExecuteAsync(object data, Action<object> execute, bool isBusy)
        {
            if (isBusy)
            {
                IsBusy = true;
            }

            Task.Factory.StartNew(() =>
            {
                execute(data);

                if (isBusy)
                {
                    IsBusy = false;
                }
            });
        }


        protected readonly IViewStateService _viewStateService;
    }
}