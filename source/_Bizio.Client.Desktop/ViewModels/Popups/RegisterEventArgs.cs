﻿using System;

namespace Bizio.Client.Desktop.ViewModels.Popups
{
    public class RegisterEventArgs : EventArgs
    {
        public string Username { get; }

        public string Password { get; }

        public string FirstName { get; }

        public string LastName { get; }

        public string EmailAddress { get; }

        public string PhoneNumber { get; }

        public RegisterEventArgs(string username, string password, string firstName, string lastName, string emailAddress, string phoneNumber)
        {
            Username = username;

            Password = password;

            FirstName = firstName;

            LastName = lastName;

            EmailAddress = emailAddress;

            PhoneNumber = phoneNumber;
        }
    }
}