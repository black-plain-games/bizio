﻿using Bizio.Client.Desktop.Model.Core;
using Bizio.Client.Desktop.Services;
using System;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels.Popups
{
    public class RegisterPopupViewModel : PopupViewModelBase
    {
        public event EventHandler<EventResultArgs<RegisterEventArgs, bool>> Register;

        public event EventHandler<CancelEventArgs<RegisterEventArgs>> Registering;

        public string Username
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public string Password
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public string ConfirmPassword
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public string FirstName
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public string LastName
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public string EmailAddress
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public string PhoneNumber
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public string ErrorMessage
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public ICommand RegisterCommand => GetAsyncCommand(Register, Registering, () => new RegisterEventArgs(Username, Password, FirstName, LastName, EmailAddress, PhoneNumber), true);

        public RegisterPopupViewModel(IViewStateService viewStateService, IAuthorizationClientService authorizationClientService)
            : base(viewStateService)
        {
            _authorizationClientService = authorizationClientService;

            Registering += OnRegistering;

            Register += OnRegister;
        }

        private bool HasFilledOutRequiredFields()
        {
            if (string.IsNullOrWhiteSpace(Username) ||
                string.IsNullOrWhiteSpace(Password) ||
                string.IsNullOrWhiteSpace(ConfirmPassword) ||
                string.IsNullOrWhiteSpace(FirstName) ||
                string.IsNullOrWhiteSpace(LastName) ||
                string.IsNullOrWhiteSpace(EmailAddress))
            {
                return false;
            }

            return true;
        }

        private string GetValidationMessage()
        {
            if (string.IsNullOrWhiteSpace(Username) || !_usernameRegex.IsMatch(Username))
            {
                return "Username must be at least 6 characters with no spaces";
            }

            if (string.IsNullOrWhiteSpace(Password) || !_usernameRegex.IsMatch(Password))
            {
                return "Password must be at least 6 characters with no spaces";
            }

            if (string.IsNullOrWhiteSpace(ConfirmPassword) || Password != ConfirmPassword)
            {
                return "Passwords do not match";
            }

            if (string.IsNullOrWhiteSpace(FirstName) || FirstName.Length < 1)
            {
                return "That's a really short first name";
            }

            if (string.IsNullOrWhiteSpace(LastName) || LastName.Length < 1)
            {
                return "That's a really short last name";
            }

            try
            {
                var email = new MailAddress(EmailAddress).Address;
            }
            catch
            {
                return "That email address doesn't look quite right";
            }

            if (!string.IsNullOrWhiteSpace(PhoneNumber) && !_phoneNumberRegex.IsMatch(PhoneNumber))
            {
                return "That phone number doesn't look quite right";
            }

            return null;
        }

        private void OnRegistering(object sender, CancelEventArgs<RegisterEventArgs> e)
        {
            if (!HasFilledOutRequiredFields())
            {
                e.Cancel = true;

                return;
            }

            ErrorMessage = GetValidationMessage();

            e.Cancel = ErrorMessage != null;
        }

        private void OnRegister(object sender, EventResultArgs<RegisterEventArgs, bool> e)
        {
            ErrorMessage = GetValidationMessage();

            if (ErrorMessage != null)
            {
                e.DoDismiss = false;

                return;
            }

            var data = new CreateUserData
            {
                UserName = Username,
                Password = Password,
                EmailAddress = EmailAddress,
                FirstName = FirstName,
                LastName = LastName,
                PhoneNumber = PhoneNumber
            };

            if (!_authorizationClientService.Register(data))
            {
                ErrorMessage = "Registration failed";

                e.DoDismiss = false;

                return;
            }
        }

        private readonly IAuthorizationClientService _authorizationClientService;

        private static readonly Regex _usernameRegex = new Regex(@"^[^\s]{6,128}$");

        private static readonly Regex _phoneNumberRegex = new Regex(@"^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$");

    }
}