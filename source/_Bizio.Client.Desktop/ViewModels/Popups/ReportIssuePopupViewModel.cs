﻿using Bizio.Client.Desktop.Model.Core;
using Bizio.Client.Desktop.Services;
using BlackPlain.DI;
using System;
using System.Collections.Generic;
using System.Management;
using System.Windows;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels.Popups
{
    public class ReportIssuePopupViewModel : PopupViewModelBase
    {
        public string Subject
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public string Details
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public ICommand ReportIssueCommand => GetCommand(ReportIssue, CanReportIssue, () => new ReportIssueEventArgs(Subject, Details));

        private void ReportIssue(object sender, EventArgs e)
        {
            var specs = new Dictionary<string, object>
            {
                { "key", "value" },
                { "---", "-----" },
                { "User ID", _developerViewModel.UserId },
                { "User Name", _developerViewModel.UserName},
                { "Game ID", _developerViewModel.Data?.GameId },
                { "User Company ID", _developerViewModel.UserCompanyId },
                { "Client Time", DateTimeOffset.Now.ToString("dddd, MMM dd yyyy HH:mm:ss zzz") },
            };

            var memoryObject = QueryFirst("SELECT TotalPhysicalMemory FROM Win32_ComputerSystem");

            var memory = long.Parse($"{memoryObject["TotalPhysicalMemory"]}") / (1024 * 1024);

            specs.Add("Total Phyical Memory (MB)", memory);

            var processorName = QueryFirst("SELECT * FROM Win32_Processor")["Name"];

            specs.Add("Processor", processorName);

            var gpu = QueryFirst("SELECT * FROM Win32_VideoController");

            var gpuName = gpu["Name"];

            specs.Add("GPU", gpuName);

            var gpuRAM = long.Parse($"{gpu["AdapterRAM"]}") / (1024 * 1024);

            specs.Add("GPU RAM", gpuRAM);

            var gpuRefreshRate = gpu["CurrentRefreshRate"];

            specs.Add("GPU Refresh Rate", gpuRefreshRate);

            var x = Application.Current.MainWindow.ActualWidth;

            var y = Application.Current.MainWindow.ActualHeight;

            specs.Add("Resolution", $"{x}x{y}");

            var body = $"# Description<br />{Details.Replace("\n", "<br />")}<br /># Specs<br />";

            foreach(var spec in specs)
            {
                body += $"| {spec.Key} | {spec.Value} |<br />";
            }

            var screenshotFile = _viewStateService.SaveScreenshot();

            _emailService.SendEmail(Subject, body, screenshotFile);
        }

        private ManagementBaseObject QueryFirst(string query)
        {
            var results = new ManagementObjectSearcher(new ObjectQuery(query)).Get();

            foreach (var result in results)
            {
                return result;
            }

            return null;
        }

        private void CanReportIssue(object sender, CancelEventArgs<ReportIssueEventArgs> args)
        {
            args.Cancel = string.IsNullOrWhiteSpace(args.Data.Subject) ||
                args.Data.Subject.Length < 10 ||
                string.IsNullOrWhiteSpace(args.Data.Details) ||
                args.Data.Details.Length < 15;
        }

        public ReportIssuePopupViewModel(
            IViewStateService viewStateService,
            IEmailService emailService,
            IDeveloperViewModel developerViewModel)
            : base(viewStateService)
        {
            _emailService = emailService;

            _developerViewModel = developerViewModel;
        }

        private readonly IEmailService _emailService;

        private readonly IDeveloperViewModel _developerViewModel;

        private class ReportIssueEventArgs : EventArgs
        {
            public ReportIssueEventArgs(string subject, string details)
            {
                Subject = subject;

                Details = details;
            }

            public string Subject { get; private set; }

            public string Details { get; private set; }
        }
    }
}