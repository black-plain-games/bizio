﻿using Bizio.Client.Desktop.Core;
using Bizio.Client.Desktop.Model.Companies;
using Bizio.Client.Desktop.Model.Core;
using Bizio.Client.Desktop.Model.Game;
using Bizio.Client.Desktop.Model.Game.ActionData;
using Bizio.Client.Desktop.Model.Projects;
using Bizio.Client.Desktop.Services;
using BlackPlain.DI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels
{
    public class ProjectsPanelViewModel : GameFragmentViewModelBase, IProjectsPanelViewModel
    {
        public Project SelectedProject
        {
            get
            {
                return GetField<Project>();
            }
            set
            {
                SetField(value);
            }
        }

        public Project SelectedUserCompanyProject
        {
            get
            {
                return GetField<Project>();
            }
            set
            {
                SetField(value);

                OnPropertyChanged(nameof(Allocations));
            }
        }

        public IEnumerable<Allocation> Allocations
        {
            get
            {
                if (Data == null ||
                    UserCompany == null ||
                    SelectedUserCompanyProject == null)
                {
                    return null;
                }

                return AllAllocations.Where(a => FindTurnAction<AdjustAllocationActionData>(ta => ta.Data.PersonId == a.PersonId && ta.Data.ProjectId == a.ProjectId && ta.Data.Percentage == 0) == null);
            }
        }

        private IEnumerable<Allocation> AllAllocations
        {
            get
            {
                return Data.NewAllocations
                    .Concat(UserCompany.Allocations)
                    .Where(a => a.ProjectId == SelectedUserCompanyProject.Id);
            }
        }

        public IEnumerable<Project> AvailableProjects
        {
            get
            {
                if (Data == null ||
                    Data.CompaniesData == null ||
                    Data.CompaniesData.Companies == null ||
                    Data.ProjectsData == null ||
                    Data.ProjectsData.Projects == null)
                {
                    return null;
                }

                return Data.ProjectsData.Projects.Where(p => !Data.CompaniesData.Companies.SelectMany(c => c.Projects).Any(cp => cp.Id == p.Id));
            }
        }

        public IEnumerable<Project> ProjectsInProgress
        {
            get
            {
                if (Data == null ||
                    UserCompany == null ||
                    UserCompany.Projects == null)
                {
                    return null;
                }

                return UserCompany.Projects.Where(p => p.Status == ProjectStatus.InProgress);
            }
        }

        public ICommand RequestProjectCommand => GetCommand(RequestProject, CanRequestProject);

        public ICommand ToggleExtensionRequestCommand => GetCommand(ToggleExtensionRequest, CanToggleExtensionRequest);

        public ICommand ToggleProjectSubmissionCommand => GetCommand(ToggleProjectSubmission, CanToggleProjectSubmission);

        public ICommand AddAllocationCommand => GetCommand(AddAllocation, CanAddAllocation);

        public ICommand EditAllocationCommand => GetCommand(EditAllocation, CanEditAllocation);

        public ICommand DeleteAllocationCommand => GetCommand(DeleteAllocation, CanEditAllocation);

        public ProjectsPanelViewModel(IHub hub,
            IViewStateService viewStateService,
            IAuthorizationClientService authorizationClientService,
            IResourceClientService resourceClientService)
            : base(hub, viewStateService, authorizationClientService, resourceClientService)
        {
        }

        public override void RefreshGameData(GameData data, ObservableCollection<IActionData> turnActions)
        {
            var selectedUserCompanyProjectId = SelectedUserCompanyProject?.Id;

            var selectedProjectId = SelectedProject?.Id;

            base.RefreshGameData(data, turnActions);

            if (selectedUserCompanyProjectId.HasValue)
            {
                SelectedUserCompanyProject = UserCompany.Projects.FirstOrDefault(p => p.Id == selectedUserCompanyProjectId.Value);
            }

            if (selectedProjectId.HasValue)
            {
                SelectedProject = Data.ProjectsData.Projects.FirstOrDefault(p => p.Id == selectedProjectId.Value);
            }

            Project project = null;

            foreach (var turnAction in turnActions)
            {
                switch ((ActionType)turnAction.ActionTypeId)
                {
                    case ActionType.AcceptProject:
                        var acceptProjectAction = turnAction as AcceptProjectActionData;

                        project = AvailableProjects.FirstOrDefault(p => p.Id == acceptProjectAction.Data.ProjectId);

                        project.IsRequesting = true;
                        break;

                    case ActionType.RequestExtension:
                        var requestExtensionAction = turnAction as RequestExtensionTurnActionData;

                        project = UserCompany.Projects.FirstOrDefault(p => p.Id == requestExtensionAction.Data.ProjectId);

                        project.SubmissionStatus = ProjectSubmissionStatus.RequestingExtension;
                        break;

                    case ActionType.SubmitProject:
                        var submitProjectAction = turnAction as SubmitProjectTurnActionData;

                        project = UserCompany.Projects.FirstOrDefault(p => p.Id == submitProjectAction.Data.ProjectId);

                        project.SubmissionStatus = submitProjectAction.Data.IsCancellation ? ProjectSubmissionStatus.Cancelling : ProjectSubmissionStatus.Submitting;
                        break;

                    case ActionType.AdjustAllocation:
                        var adjustAllocationAction = turnAction as AdjustAllocationActionData;

                        project = UserCompany.Projects.FirstOrDefault(p => p.Id == adjustAllocationAction.Data.ProjectId);

                        var existingAllocation = AllAllocations.FirstOrDefault(a => a.PersonId == adjustAllocationAction.Data.PersonId && a.ProjectId == adjustAllocationAction.Data.ProjectId);

                        if (existingAllocation == null)
                        {
                            var allocation = new Allocation(adjustAllocationAction.Data.ProjectId, adjustAllocationAction.Data.PersonId, adjustAllocationAction.Data.Role, adjustAllocationAction.Data.Percentage);

                            Data.NewAllocations.Add(allocation);

                            OnPropertyChanged(nameof(Allocations));
                        }
                        break;
                }
            }

            OnPropertyChanged(nameof(AvailableProjects));

            OnPropertyChanged(nameof(ProjectsInProgress));
        }

        protected override bool CloseSubPanel(object state)
        {
            var subPanel = state.AsEnum<SubPanel>();

            switch (subPanel)
            {
                case SubPanel.AvailableProjects:
                    if (SelectedProject != null)
                    {
                        SelectedProject = null;

                        return true;
                    }
                    break;

                case SubPanel.ProjectsInProgress:
                    if (SelectedUserCompanyProject != null)
                    {
                        SelectedUserCompanyProject = null;

                        return true;
                    }
                    break;
            }

            return false;
        }

        protected void RequestProject(object state)
        {
            if (state is string cancel && cancel.ToUpper() == "CANCEL")
            {
                var existingTurnAction = FindTurnAction<AcceptProjectActionData>(d => d.Data.ProjectId == SelectedProject.Id);

                TurnActions.Remove(existingTurnAction);

                SelectedProject.IsRequesting = false;

                return;
            }

            var turnAction = new AcceptProjectActionData(SelectedProject.Id);

            turnAction.Cancelled += (s, e) => RequestProject("CANCEL");

            TurnActions.Add(turnAction);

            SelectedProject.IsRequesting = true;
        }

        protected bool CanRequestProject(object state)
        {
            if (Data == null || SelectedProject == null)
            {
                return false;
            }

            var existingTurnAction = FindTurnAction<AcceptProjectActionData>(d => d.Data.ProjectId == SelectedProject.Id);

            if (state is string cancel &&
                cancel.ToUpper() == "CANCEL" &&
                existingTurnAction != null)
            {
                return true;
            }

            return CanUserCompanyPerformActionType(ActionType.AcceptProject) && existingTurnAction == null;
        }

        protected void CancelExtensionRequest(RequestExtensionTurnActionData turnAction)
        {
            TurnActions.Remove(turnAction);

            SelectedUserCompanyProject.SubmissionStatus = ProjectSubmissionStatus.InProgress;
        }

        protected void ToggleExtensionRequest()
        {
            var existingTurnAction = FindTurnAction<RequestExtensionTurnActionData>(ta => ta.Data.ProjectId == SelectedUserCompanyProject.Id);

            if (existingTurnAction != null)
            {
                CancelExtensionRequest(existingTurnAction);
            }
            else
            {
                var turnAction = new RequestExtensionTurnActionData(SelectedUserCompanyProject.Id);

                turnAction.Cancelled += (s, e) => CancelExtensionRequest(turnAction);

                TurnActions.Add(turnAction);

                SelectedUserCompanyProject.SubmissionStatus = ProjectSubmissionStatus.RequestingExtension;
            }
        }

        protected bool CanToggleExtensionRequest(object state)
        {
            if (Data == null || SelectedUserCompanyProject == null)
            {
                return false;
            }

            switch (SelectedUserCompanyProject.SubmissionStatus)
            {
                case ProjectSubmissionStatus.InProgress:
                    return CanUserCompanyPerformActionType(ActionType.RequestExtension);

                case ProjectSubmissionStatus.Cancelling:
                case ProjectSubmissionStatus.Submitting:
                    return false;

                case ProjectSubmissionStatus.RequestingExtension:
                    return true;
            }

            throw new ArgumentException(nameof(SelectedUserCompanyProject.SubmissionStatus));
        }

        protected void CancelProjectSubmission(SubmitProjectTurnActionData turnAction)
        {
            TurnActions.Remove(turnAction);

            SelectedUserCompanyProject.SubmissionStatus = ProjectSubmissionStatus.InProgress;
        }

        protected void ToggleProjectSubmission(object data)
        {
            var existingTurnAction = FindTurnAction<SubmitProjectTurnActionData>(ta => ta.Data.ProjectId == SelectedUserCompanyProject.Id);

            if (existingTurnAction != null)
            {
                CancelProjectSubmission(existingTurnAction);
            }
            else
            {
                var isCancellation = bool.Parse($"{data}");

                var turnAction = new SubmitProjectTurnActionData(SelectedUserCompanyProject.Id, isCancellation);

                turnAction.Cancelled += (s, e) => CancelProjectSubmission(turnAction);

                TurnActions.Add(turnAction);

                SelectedUserCompanyProject.SubmissionStatus = isCancellation ? ProjectSubmissionStatus.Cancelling : ProjectSubmissionStatus.Submitting;
            }
        }

        protected bool CanToggleProjectSubmission(object state)
        {
            if (Data == null || SelectedUserCompanyProject == null)
            {
                return false;
            }

            var isCancelling = bool.Parse($"{state}");

            switch (SelectedUserCompanyProject.SubmissionStatus)
            {
                case ProjectSubmissionStatus.Cancelling:
                    return isCancelling;

                case ProjectSubmissionStatus.InProgress:
                    return CanUserCompanyPerformActionType(ActionType.SubmitProject);

                case ProjectSubmissionStatus.RequestingExtension:
                    return false;

                case ProjectSubmissionStatus.Submitting:
                    return !isCancelling;
            }

            throw new ArgumentException(nameof(SelectedUserCompanyProject.SubmissionStatus));
        }

        private AllocationPopupManager GetAddAllocationPopupManager()
        {
            var output = new AllocationPopupManager(ViewStateService, Data, UserCompany, StaticData.ProjectsData.ProjectRoles, Data.NewAllocations.Concat(UserCompany.Allocations), TurnActions, SelectedUserCompanyProject);

            output.OnTurnActionsChanged += (s, e) => OnPropertyChanged(nameof(TurnActions));

            output.OnAllocationsChanged += (s, e) =>
            {
                OnPropertyChanged(nameof(Allocations));

                OnPropertyChanged(nameof(AllAllocations));
            };

            return output;
        }

        private bool CanAddAllocation() => GetAddAllocationPopupManager().CanAddAllocation();

        private void AddAllocation() => GetAddAllocationPopupManager().AddAllocation();

        private AllocationPopupManager GetEditAllocationPopupManager(Allocation allocation)
        {
            var output = new AllocationPopupManager(ViewStateService, Data, UserCompany, StaticData.ProjectsData.ProjectRoles, Data.NewAllocations.Concat(UserCompany.Allocations), TurnActions, allocation);

            output.OnTurnActionsChanged += (s, e) => OnPropertyChanged(nameof(TurnActions));

            output.OnAllocationsChanged += (s, e) =>
            {
                OnPropertyChanged(nameof(Allocations));

                OnPropertyChanged(nameof(AllAllocations));
            };

            return output;
        }

        private bool CanEditAllocation(object state) => GetEditAllocationPopupManager(state as Allocation).CanEditAllocation();

        private void EditAllocation(object state) => GetEditAllocationPopupManager(state as Allocation).EditAllocation();

        protected void DeleteAllocation(object state) => GetEditAllocationPopupManager(state as Allocation).DeleteAllocation();

        protected bool CanAllocatePerson() => UserCompany?.Employees?.Any(e => !Allocations.Any(a => a.ProjectId == SelectedUserCompanyProject.Id && a.PersonId == e.PersonId)) == true;

        private enum SubPanel
        {
            AvailableProjects,
            ProjectsInProgress
        }
    }
}