﻿using Bizio.Client.Desktop.Core;
using Bizio.Client.Desktop.Model.Core;
using Bizio.Client.Desktop.Model.Settings;
using Bizio.Client.Desktop.Services;
using Bizio.Client.Desktop.ViewModels.Popups;
using Bizio.Client.Desktop.Views.Popups;
using BlackPlain.DI;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels
{
    public class SettingsViewModel : ViewModel, ISettingsViewModel
    {
        public IDictionary<string, UiScale> UiScales
        {
            get
            {
                return GetField<IDictionary<string, UiScale>>();
            }
            set
            {
                SetField(value);
            }
        }

        public IEnumerable<CultureInfo> Cultures => CultureInfo.GetCultures(CultureTypes.NeutralCultures).Where(c => UiText.Languages.Contains(c.TwoLetterISOLanguageName)).OrderBy(ci => ci.DisplayName);

        public IDictionary<string, string> CurrencySymbols
        {
            get
            {
                return GetField<IDictionary<string, string>>();
            }
            set
            {
                SetField(value);
            }
        }

        public ApplicationSettings Settings
        {
            get
            {
                return GetField<ApplicationSettings>();
            }
            set
            {
                SetField(value);
            }
        }

        public ICommand CancelCommand => GetCommand(Cancel);

        public ICommand SaveCommand => GetCommand(Save);

        public ICommand ShowChangePasswordPopupCommand => GetAsyncCommand(ShowChangePasswordPopup);

        public SettingsViewModel(IHub hub,
            IViewStateService viewStateService,
            IApplicationSettingService applicationSettingService,
            IAuthorizationClientService authorizationClientService)
            : base(hub)
        {
            _viewStateService = viewStateService;

            _applicationSettingService = applicationSettingService;

            _authorizationClientService = authorizationClientService;
        }

        public override void OnEscapePressed()
        {
            Cancel();
        }

        protected override void LoadViewModel(object state)
        {
            base.LoadViewModel(state);

            Settings = _applicationSettingService.GetSettings(true);

            UiScales = new Dictionary<string, UiScale>
            {
                { UiText.Small, new UiScale(.9, .9) },
                { UiText.Medium, new UiScale(1, 1) },
                { UiText.Large, new UiScale(1.1, 1.1) }
            };

            CurrencySymbols = new Dictionary<string, string>
            {
                { Glyphs.DollarSign, Glyphs.DollarSign },
                { Glyphs.EuroSign, Glyphs.EuroSign },
                { Glyphs.PoundSign, Glyphs.PoundSign },
                { Glyphs.YenSign, Glyphs.YenSign },
                { Glyphs.FrancSign, Glyphs.FrancSign }
            };
        }

        private void ShowChangePasswordPopup()
        {
            if (!_authorizationClientService.GeneratePasswordResetToken(_authorizationClientService.UserName, _authorizationClientService.EmailAddress))
            {
                // Something went wrong

                return;
            }

            var viewModel = new ChangePasswordPopupViewModel(_viewStateService, _authorizationClientService, _authorizationClientService.UserName);

            viewModel.Login += Login;

            _viewStateService.PushPopup<ChangePasswordPopupView>(viewModel);
        }

        private void Login(object sender, EventResultArgs<LoginEventArgs, bool> e)
        {
            if (_authorizationClientService.Login(e.Argument.Username, e.Argument.Password))
            {
                e.DoDismiss = true;

                e.Result = true;

                return;
            }
        }

        private void Cancel()
        {
            _viewStateService.NavigateToMainMenuView();
        }

        private void Save()
        {
            _applicationSettingService.SaveSettings();

            _viewStateService.NavigateToMainMenuView();
        }

        private readonly IViewStateService _viewStateService;

        private readonly IApplicationSettingService _applicationSettingService;

        private readonly IAuthorizationClientService _authorizationClientService;
    }
}