﻿namespace Bizio.Client.Desktop.ViewModels
{
    public class SkillSummary
    {
        public int SkillDefinitionId { get; set; }

        public decimal TotalValue { get; set; }

        public int PersonCount { get; set; }

        public int BestPersonId { get; set; }

        public decimal BestPersonAmount { get; set; }
    }
}