﻿using Bizio.Client.Desktop.Model.Companies;
using Bizio.Client.Desktop.Model.Game;
using Bizio.Client.Desktop.Model.Game.ActionData;
using Bizio.Client.Desktop.Model.Perks;
using Bizio.Client.Desktop.Services;
using BlackPlain.DI;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels
{
    public class StorePanelViewModel : GameFragmentViewModelBase, IStorePanelViewModel
    {
        public Perk SelectedPerk
        {
            get
            {
                return GetField<Perk>();
            }
            set
            {
                SetField(value);

                OnPropertyChanged(nameof(CompanyPerks));

                OnPropertyChanged(nameof(SelectedPerkCount));
            }
        }

        public IEnumerable<CompanyPerk> CompanyPerks
        {
            get
            {
                if (Data == null ||
                    UserCompany == null ||
                    SelectedPerk == null)
                {
                    return null;
                }

                return UserCompany.Perks.Where(p => p.PerkId == SelectedPerk.Id);
            }
        }

        public int SelectedPerkCount
        {
            get
            {
                if (Data == null ||
                    UserCompany == null ||
                    SelectedPerk == null)
                {
                    return 0;
                }

                return UserCompany.Perks.Count(p => p.PerkId == SelectedPerk.Id);
            }
        }

        public ICommand PurchasePerkCommand => GetCommand(PurchasePerk, CanPurchasePerk);

        public ICommand CancelPurchasePerkCommand => GetCommand(CancelPurchasePerk, CanCancelPurchasePerk);

        public ICommand SellPerkCommand => GetCommand(SellPerk, CanSellPerk);

        public ICommand CancelSellPerkCommand => GetCommand(CancelSellPerk, CanCancelSellPerk);

        public StorePanelViewModel(IHub hub,
            IViewStateService viewStateService,
            IAuthorizationClientService authorizationClientService,
            IResourceClientService resourceClientService)
            : base(hub, viewStateService, authorizationClientService, resourceClientService)
        {
        }

        public override void RefreshGameData(GameData data, ObservableCollection<IActionData> turnActions)
        {
            foreach (var turnAction in turnActions.Where(ta => ta is SellPerkActionData).Cast<SellPerkActionData>())
            {
                UserCompany.Perks.First(p => turnAction.Data.CompanyPerkId == p.Id).IsSelling = true;
            }

            SelectedPerk = null;

            base.RefreshGameData(data, turnActions);
        }

        protected override bool CloseSubPanel(object state)
        {
            if (SelectedPerk != null)
            {
                SelectedPerk = null;

                return true;
            }

            return base.CloseSubPanel(state);
        }

        private bool CanSellPerk(object data)
        {
            if (Data == null ||
                UserCompany == null ||
                SelectedPerk == null ||
                !(data is CompanyPerk) ||
                !CanUserCompanyPerformActionType(ActionType.SellPerk))
            {
                return false;
            }

            return true;
        }

        private void SellPerk(object data)
        {
            var companyPerk = (CompanyPerk)data;

            companyPerk.IsSelling = true;

            var turnAction = new SellPerkActionData(companyPerk.Id);

            turnAction.Cancelled += (s, e) => CancelSellPerk(data);

            TurnActions.Add(turnAction);

            SelectedPerk.OnPropertyChanged(nameof(SelectedPerk.Id));
        }

        private bool CanCancelSellPerk(object data)
        {
            var companyPerk = (CompanyPerk)data;

            if (Data == null ||
                UserCompany == null ||
                SelectedPerk == null ||
                companyPerk == null ||
                FindTurnAction<SellPerkActionData>(ta => ta.Data.CompanyPerkId == companyPerk.Id) == null)
            {
                return false;
            }

            return true;
        }

        private void CancelSellPerk(object data)
        {
            var companyPerk = (CompanyPerk)data;

            companyPerk.IsSelling = false;

            var turnAction = FindTurnAction<SellPerkActionData>(ta => ta.Data.CompanyPerkId == companyPerk.Id);

            TurnActions.Remove(turnAction);

            SelectedPerk.OnPropertyChanged(nameof(SelectedPerk.Id));
        }

        private bool CanPurchasePerk()
        {
            if (Data == null ||
                UserCompany == null ||
                SelectedPerk == null ||
                !CanUserCompanyPerformActionType(ActionType.PurchasePerk) ||
                CompanyPerks.Count() == SelectedPerk.MaxCount)
            {
                return false;
            }

            return true;
        }

        private void PurchasePerk()
        {
            var turnAction = new PurchasePerkActionData(SelectedPerk.Id);

            turnAction.Cancelled += (s, e) => CancelPurchasePerk(turnAction);

            TurnActions.Add(turnAction);

            SelectedPerk.OnPropertyChanged(nameof(SelectedPerk.Id));
        }

        private bool CanCancelPurchasePerk()
        {
            if (Data == null ||
                UserCompany == null ||
                SelectedPerk == null ||
                FindTurnAction<PurchasePerkActionData>(ta => ta.Data.PerkId == SelectedPerk.Id) == null)
            {
                return false;
            }

            return true;
        }

        private void CancelPurchasePerk()
        {
            var turnAction = FindTurnAction<PurchasePerkActionData>(ta => ta.Data.PerkId == SelectedPerk.Id);

            CancelPurchasePerk(turnAction);
        }

        private void CancelPurchasePerk(PurchasePerkActionData turnAction)
        {
            TurnActions.Remove(turnAction);

            SelectedPerk.OnPropertyChanged(nameof(SelectedPerk.Id));
        }
    }
}