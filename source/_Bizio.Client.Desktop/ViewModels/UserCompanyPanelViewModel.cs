﻿using Bizio.Client.Desktop.Core;
using Bizio.Client.Desktop.Model.Companies;
using Bizio.Client.Desktop.Model.Core;
using Bizio.Client.Desktop.Model.Game;
using Bizio.Client.Desktop.Model.Game.ActionData;
using Bizio.Client.Desktop.Services;
using Bizio.Client.Desktop.ViewModels.Popups;
using Bizio.Client.Desktop.Views.Popups;
using BlackPlain.DI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Data;
using System.Windows.Input;

namespace Bizio.Client.Desktop.ViewModels
{
    public class UserCompanyPanelViewModel : GameFragmentViewModelBase, IUserCompanyPanelViewModel
    {
        public ICollectionView Employees
        {
            get
            {
                return GetField<ICollectionView>();
            }
            set
            {
                SetField(value);
            }
        }

        public Employee SelectedEmployee
        {
            get
            {
                return GetField<Employee>();
            }
            set
            {
                SetField(value);

                if (value == null)
                {
                    return;
                }

                OnPropertyChanged(nameof(SelectedEmployeeAllocations));

                var turnAction = FindTurnAction<AdjustSalaryActionData>(ta => ta.Data.PersonId == value.PersonId);

                if (turnAction == null)
                {
                    OfferValue = null;

                    return;
                }

                OfferValue = turnAction.Data.Salary;
            }
        }

        public IEnumerable<Transaction> UserCompanyTransactions
        {
            get
            {
                return UserCompany?.Transactions?.OrderByDescending(t => t.Date);
            }
        }

        public IEnumerable<Allocation> SelectedEmployeeAllocations
        {
            get
            {
                if (Data == null ||
                    UserCompany == null ||
                    SelectedEmployee == null)
                {
                    return null;
                }

                return UserCompany.Allocations
                    .Where(a => a.PersonId == SelectedEmployee.PersonId)
                    .Concat(Data.NewAllocations.Where(a => a.PersonId == SelectedEmployee.PersonId))
                    .Where(a => a.Percent > 0);
            }
        }

        public IEnumerable<DataPoint<DateTime, IEnumerable<DataPoint<TransactionType, decimal>>>> TransactionsData
        {
            get
            {
                return UserCompanyTransactions?.CreateStackedData(t => t.Date, t => t.TransactionType, t => t.Amount, (a, b) => a + b).OrderBy(x => x.Key);
            }
        }

        public decimal TotalIncome
        {
            get
            {
                if (Data == null || UserCompany == null)
                {
                    return 0;
                }

                return UserCompany.Transactions.Where(t => t.Amount > 0).Sum(t => t.Amount);
            }
        }

        public decimal TotalExpenses
        {
            get
            {
                if (Data == null || UserCompany == null)
                {
                    return 0;
                }

                return UserCompany.Transactions.Where(t => t.Amount < 0).Sum(t => t.Amount);
            }
        }

        public decimal? OfferValue
        {
            get
            {
                return GetField<decimal?>();
            }
            set
            {
                SetField(value);
            }
        }

        public IEnumerable<SkillSummary> MandatorySkillSummaries
        {
            get
            {
                return GetField<IEnumerable<SkillSummary>>();
            }
            set
            {
                SetField(value);
            }
        }

        public IEnumerable<SkillSummary> OptionalSkillSummaries
        {
            get
            {
                return GetField<IEnumerable<SkillSummary>>();
            }
            set
            {
                SetField(value);
            }
        }

        public ICommand ToggleFireSelectedEmployeeCommand => GetCommand(ToggleFireSelectedEmployee, CanToggleFireSelectedEmployee);

        public ICommand EditSalaryCommand => GetCommand(EditSalary, CanEditSalary);

        public ICommand CancelSalaryChangeCommand => GetCommand(CancelSalaryChanges);

        public ICommand AddAllocationCommand => GetCommand(ShowAddAllocationPopup, CanShowAddAllocationPopup);

        public ICommand EditAllocationCommand => GetCommand(ShowEditAllocationPopup, CanShowEditAllocationPopup);

        public ICommand DeleteAllocationCommand => GetCommand(DeleteAllocation);

        public UserCompanyPanelViewModel(IHub hub,
            IViewStateService viewStateService,
            IAuthorizationClientService authorizationClientService,
            IResourceClientService resourceClientService)
            : base(hub, viewStateService, authorizationClientService, resourceClientService)
        {
        }

        public override void RefreshGameData(GameData data, ObservableCollection<IActionData> turnActions)
        {
            base.RefreshGameData(data, turnActions);

            Data.NewAllocations.CollectionChanged += (s, e) => OnPropertyChanged(nameof(SelectedEmployeeAllocations));

            Employees = CollectionViewSource.GetDefaultView(UserCompany.Employees);

            Employees.SortDescriptions.Add(new SortDescription("IsFounder", ListSortDirection.Descending));

            Employees.SortDescriptions.Add(new SortDescription("PersonId", ListSortDirection.Ascending));

            Employee employee = null;

            foreach (var turnAction in turnActions)
            {
                switch ((ActionType)turnAction.ActionTypeId)
                {
                    case ActionType.AdjustSalary:
                        var adjustSalaryAction = turnAction as AdjustSalaryActionData;

                        employee = UserCompany.Employees.First(e => e.PersonId == adjustSalaryAction.Data.PersonId);

                        employee.Status = employee.Salary > adjustSalaryAction.Data.Salary ? EmployeeStatus.DecreasingSalary : EmployeeStatus.IncreasingSalary;
                        break;

                    case ActionType.FireEmployee:
                        var fireEmployeeAction = turnAction as FireEmployeeActionData;

                        employee = UserCompany.Employees.First(e => e.PersonId == fireEmployeeAction.Data.PersonId);

                        employee.Status = EmployeeStatus.Firing;
                        break;
                }
            }

            OfferValue = null;

            var mandatorySkillSummaries = new List<SkillSummary>();

            var optionalSkillSummaries = new List<SkillSummary>();

            foreach (var ucEmployee in UserCompany.Employees)
            {
                var person = Data.PeopleData.People.FirstOrDefault(p => p.Id == ucEmployee.PersonId);

                foreach (var skill in person.Skills)
                {
                    var isMandatorySkill = StaticData.PeopleData.MandatorySkillDefinitions[Data.IndustryId].Any(sd => sd.Id == skill.SkillDefinitionId);

                    var skillSummaries = isMandatorySkill ? mandatorySkillSummaries : optionalSkillSummaries;

                    var skillSummary = skillSummaries.FirstOrDefault(ss => ss.SkillDefinitionId == skill.SkillDefinitionId);

                    if (skillSummary == null)
                    {
                        skillSummaries.Add(skillSummary = new SkillSummary
                        {
                            SkillDefinitionId = skill.SkillDefinitionId
                        });
                    }

                    skillSummary.TotalValue += skill.Value;

                    skillSummary.PersonCount++;

                    if (skill.Value > skillSummary.BestPersonAmount)
                    {
                        skillSummary.BestPersonAmount = skill.Value;

                        skillSummary.BestPersonId = ucEmployee.PersonId;
                    }
                }
            }

            MandatorySkillSummaries = mandatorySkillSummaries.OrderByDescending(ss => ss.TotalValue);

            OptionalSkillSummaries = optionalSkillSummaries.OrderByDescending(ss => ss.TotalValue);

            OnPropertyChanged(nameof(UserCompanyTransactions));

            OnPropertyChanged(nameof(TotalIncome));

            OnPropertyChanged(nameof(TotalExpenses));

            OnPropertyChanged(nameof(TransactionsData));
        }

        protected override bool CloseSubPanel(object state)
        {
            var subPanel = state.AsEnum<SubPanel>();

            switch (subPanel)
            {
                case SubPanel.Employees:
                    if (SelectedEmployee != null)
                    {
                        SelectedEmployee = null;

                        return true;
                    }
                    break;
            }

            return false;
        }

        private bool CanToggleFireSelectedEmployee()
        {
            if (Data == null ||
                SelectedEmployee == null ||
                SelectedEmployee.IsFounder ||
                OfferValue.HasValue)
            {
                return false;
            }

            if (SelectedEmployee.Status == EmployeeStatus.Firing)
            {
                return true;
            }

            return CanUserCompanyPerformActionType(ActionType.FireEmployee) &&
                !TurnActions.Any(ta => (ta as FireEmployeeActionData)?.Data.PersonId == SelectedEmployee.PersonId);
        }

        private void ToggleFireSelectedEmployee()
        {
            var turnAction = FindTurnAction<FireEmployeeActionData>(ta => ta.Data.PersonId == SelectedEmployee.PersonId);

            if (turnAction != null)
            {
                turnAction.Cancel();
            }
            else
            {
                turnAction = new FireEmployeeActionData(SelectedEmployee.PersonId);

                turnAction.Cancelled += (s, e) =>
                {
                    SelectedEmployee.Status = EmployeeStatus.None;

                    TurnActions.Remove(turnAction);
                };

                TurnActions.Add(turnAction);

                SelectedEmployee.Status = EmployeeStatus.Firing;
            }
        }

        private bool CanEditSalary()
        {
            return Data != null &&
                SelectedEmployee != null &&
                !SelectedEmployee.IsFounder &&
                SelectedEmployee.Status != EmployeeStatus.Firing &&
                (CanUserCompanyPerformActionType(ActionType.AdjustSalary) ||
                FindTurnAction<AdjustSalaryActionData>(ta => ta.Data.PersonId == SelectedEmployee.PersonId) != null);
        }

        private void EditSalary()
        {
            var person = FindPerson(SelectedEmployee.PersonId);

            var viewModel = new EditSalaryPopupViewModel(ViewStateService, person, OfferValue ?? SelectedEmployee.Salary);

            viewModel.EditingSalary += CanSaveSalaryChange;

            viewModel.EditSalary += SaveSalaryChange;

            ViewStateService.PushPopup<EditSalaryPopupView>(viewModel);
        }

        private void CanSaveSalaryChange(object sender, CancelEventArgs<EditSalaryEventArgs> e)
        {
            e.Cancel = !e.Data.Salary.Between(.01m, UserCompany.Money);
        }

        private void SaveSalaryChange(object sender, EditSalaryEventArgs e)
        {
            var existingAction = FindTurnAction<AdjustSalaryActionData>(ta => ta.Data.PersonId == SelectedEmployee.PersonId);

            if (existingAction != null)
            {
                TurnActions.Remove(existingAction);
            }

            if (e.Salary != SelectedEmployee.Salary)
            {
                var turnAction = new AdjustSalaryActionData(SelectedEmployee.PersonId, e.Salary);

                turnAction.Cancelled += (s, args) => CancelSalaryChanges();

                TurnActions.Add(turnAction);

                SelectedEmployee.Status = e.Salary > SelectedEmployee.Salary ? EmployeeStatus.IncreasingSalary : EmployeeStatus.DecreasingSalary;

                OfferValue = e.Salary;
            }
            else
            {
                SelectedEmployee.Status = EmployeeStatus.None;

                OfferValue = null;
            }
        }

        private void CancelSalaryChanges()
        {
            var turnAction = FindTurnAction<AdjustSalaryActionData>(x => x.Data.PersonId == SelectedEmployee.PersonId);

            if (turnAction != null)
            {
                TurnActions.Remove(turnAction);

                SelectedEmployee.Status = EmployeeStatus.None;
            }

            OfferValue = null;
        }

        private void OnAllocationsChanged(object sender, EventArgs args) => OnPropertyChanged(nameof(SelectedEmployeeAllocations));

        private AllocationPopupManager GetAddAllocationPopupManager()
        {
            if (Data == null)
            {
                return null;
            }

            var output = new AllocationPopupManager(ViewStateService, Data, UserCompany, StaticData.ProjectsData.ProjectRoles, Data.NewAllocations.Concat(UserCompany.Allocations), TurnActions, FindPerson(SelectedEmployee.PersonId));

            output.OnAllocationsChanged += OnAllocationsChanged;

            output.OnTurnActionsChanged += OnAllocationsChanged;

            return output;
        }

        private AllocationPopupManager GetEditAllocationPopupManager(object data)
        {
            if (Data == null)
            {
                return null;
            }

            var allocation = data as Allocation;

            var output = new AllocationPopupManager(ViewStateService, Data, UserCompany, StaticData.ProjectsData.ProjectRoles, Data.NewAllocations.Concat(UserCompany.Allocations), TurnActions, allocation);

            output.OnAllocationsChanged += OnAllocationsChanged;

            output.OnTurnActionsChanged += OnAllocationsChanged;

            return output;
        }

        private bool CanShowAddAllocationPopup() => GetAddAllocationPopupManager()?.CanAddAllocation() == true;

        private void ShowAddAllocationPopup() => GetAddAllocationPopupManager().AddAllocation();

        private bool CanShowEditAllocationPopup(object data) => GetEditAllocationPopupManager(data)?.CanEditAllocation() == true;

        private void ShowEditAllocationPopup(object data) => GetEditAllocationPopupManager(data).EditAllocation();

        private void DeleteAllocation(object state) => GetEditAllocationPopupManager(state).DeleteAllocation();

        protected enum SubPanel
        {
            Employees,
            Finances
        }
    }
}