﻿using Bizio.Client.Desktop.Model.Core;
using BlackPlain.DI;
using BlackPlain.Wpf.Core;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Bizio.Client.Desktop.ViewModels
{
    public abstract class ViewModel : ViewModelBase
    {
        public bool HasErrors
        {
            get
            {
                return GetField<bool>();
            }
            private set
            {
                SetField(value);
            }
        }

        public IEnumerable<string> Errors => _validationErrors.SelectMany(e => e.Value);

        protected static IEnumerable<string> NoErrors = Enumerable.Empty<string>();

        public virtual void OnEscapePressed()
        {
        }

        protected void CancelOnHasErrors<TArgs>(object sender, CancelEventArgs<TArgs> args)
        {
            if (HasErrors)
            {
                args.Cancel = true;
            }
        }

        protected ViewModel(IHub hub)
            : this(hub, true)
        {
        }

        protected ViewModel(IHub hub, bool canLoadAsync)
            : base(canLoadAsync)
        {
            Hub = hub;

            PropertyChanged += OnValidate;

            UnregisterFromValidation(nameof(HasErrors), nameof(Errors), nameof(IsBusy));
        }

        protected void RegisterForValidation(INotifyPropertyChanged data)
        {
            data.PropertyChanged += OnValidate;
        }

        protected void UnregisterFromValidation(INotifyPropertyChanged data)
        {
            data.PropertyChanged -= OnValidate;
        }

        protected virtual IEnumerable<string> Validate(string propertyName) => Enumerable.Empty<string>();

        protected void UnregisterFromValidation(params string[] propertyNames)
        {
            var newProperties = propertyNames.Except(_unvalidatedProperties);

            _unvalidatedProperties.AddRange(newProperties);
        }

        private void OnValidate(object sender, PropertyChangedEventArgs e)
        {
            if (_unvalidatedProperties.Contains(e.PropertyName))
            {
                return;
            }

            var errors = Validate(e.PropertyName);

            if (!_validationErrors.ContainsKey(e.PropertyName))
            {
                _validationErrors.Add(e.PropertyName, errors);
            }
            else
            {
                _validationErrors[e.PropertyName] = errors;
            }

            HasErrors = _validationErrors.Values.Any(v => v.Any());

            OnPropertyChanged(nameof(Errors));
        }

        protected IDictionary<string, IEnumerable<string>> _validationErrors = new Dictionary<string, IEnumerable<string>>();

        protected IHub Hub { get; }

        private readonly List<string> _unvalidatedProperties = new List<string>();
    }
}