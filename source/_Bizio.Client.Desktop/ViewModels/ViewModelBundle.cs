﻿using Bizio.Client.Desktop.ViewModels.Popups;
using BlackPlain.DI;
using BlackPlain.Wpf.Core;

namespace Bizio.Client.Desktop.ViewModels
{
    public class ViewModelBundle : IResolverBundle
    {
        public void CreateResolvers(IHub hub)
        {
            hub.CreateResolver<IViewStateRoot, MainViewModel>();

            hub.CreateResolver<IDeveloperViewModel, DeveloperViewModel>();

            hub.CreateResolver<IPopupPresenterViewModel, PopupPresenterViewModel>();

            hub.CreateResolver<IMainMenuViewModel, MainMenuViewModel>();

            hub.CreateResolver<ISettingsViewModel, SettingsViewModel>(ResolutionFrequency.OncePerRequest);

            hub.CreateResolver<INewGameViewModel, NewGameViewModel>(ResolutionFrequency.OncePerRequest);

            hub.CreateResolver<IGameViewModel, GameViewModel>(ResolutionFrequency.OncePerRequest);

            hub.CreateResolver<ILoadGameViewModel, LoadGameViewModel>(ResolutionFrequency.OncePerRequest);

            hub.CreateResolver<IPeoplePanelViewModel, PeoplePanelViewModel>(ResolutionFrequency.OncePerRequest);

            hub.CreateResolver<IUserCompanyPanelViewModel, UserCompanyPanelViewModel>(ResolutionFrequency.OncePerRequest);

            hub.CreateResolver<ICompaniesPanelViewModel, CompaniesPanelViewModel>(ResolutionFrequency.OncePerRequest);

            hub.CreateResolver<IProjectsPanelViewModel, ProjectsPanelViewModel>(ResolutionFrequency.OncePerRequest);

            hub.CreateResolver<IStorePanelViewModel, StorePanelViewModel>(ResolutionFrequency.OncePerRequest);

            hub.CreateResolver<IMailboxPanelViewModel, MailboxPanelViewModel>(ResolutionFrequency.OncePerRequest);

            hub.CreateResolver<INotificationsViewModel, NotificationsViewModel>(ResolutionFrequency.OncePerRequest);
        }

        public void SetResolutions(IHub hub)
        {
        }
    }
}