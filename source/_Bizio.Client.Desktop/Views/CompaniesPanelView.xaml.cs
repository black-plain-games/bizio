﻿using Bizio.Client.Desktop.Core;
using Bizio.Client.Desktop.Model.Companies;
using Bizio.Client.Desktop.Model.Game.ActionData;
using System.Windows.Controls;
using System.Windows.Data;

namespace Bizio.Client.Desktop.Views
{
    public partial class CompaniesPanelView : UserControl
    {
        public CompaniesPanelView()
        {
            InitializeComponent();
        }

        private void OnFilterTurnActions(object sender, FilterEventArgs e)
        {
            var actionData = e.Item as IActionData;

            e.Accepted = Utilities.ShouldShowActionTypeId(actionData.ActionTypeId);
        }

        private void OnFilterCompanyActions(object sender, FilterEventArgs e)
        {
            var companyAction = e.Item as CompanyAction;

            e.Accepted = Utilities.ShouldShowActionTypeId(companyAction.ActionId);
        }
    }
}