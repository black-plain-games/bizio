<UserControl x:Class="Bizio.Client.Desktop.Views.LoadGameView"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:i="clr-namespace:System.Windows.Interactivity;assembly=System.Windows.Interactivity"
             xmlns:util="clr-namespace:BlackPlain.Wpf.Core.UI.Utilities;assembly=BlackPlain.Wpf.Core"
             xmlns:z="clr-namespace:Bizio.Client.Desktop.Core"
             xmlns:s="clr-namespace:System.ComponentModel;assembly=WindowsBase"
             xmlns:controls="clr-namespace:Bizio.Client.Desktop.Controls">
    <UserControl.Resources>
        <ResourceDictionary>
            <CollectionViewSource x:Key="PlayableGamesView"
                                  Source="{Binding Games}"
                                  Filter="OnFilterPlayableGames">
                <CollectionViewSource.SortDescriptions>
                    <s:SortDescription PropertyName="CreatedDate" Direction="Descending" />
                </CollectionViewSource.SortDescriptions>
            </CollectionViewSource>
            <CollectionViewSource x:Key="UnplayableGamesView"
                                  Source="{Binding Games}"
                                  Filter="OnFilterUnplayableGames">
                <CollectionViewSource.SortDescriptions>
                    <s:SortDescription PropertyName="CreatedDate" Direction="Descending" />
                </CollectionViewSource.SortDescriptions>
            </CollectionViewSource>
        </ResourceDictionary>
    </UserControl.Resources>
    <i:Interaction.Triggers>
        <i:EventTrigger EventName="Loaded">
            <i:InvokeCommandAction Command="{Binding LoadViewModelCommand}" />
        </i:EventTrigger>
    </i:Interaction.Triggers>
    <Grid>
        <Grid IsEnabled="{Binding IsBusy, Converter={StaticResource BooleanInversionConverter}}">
            <Grid.ColumnDefinitions>
                <ColumnDefinition Width="*" />
                <ColumnDefinition Width="3*" />
                <ColumnDefinition Width="*" />
            </Grid.ColumnDefinitions>
            <Grid.RowDefinitions>
                <RowDefinition Height="2*" />
                <RowDefinition Height="5*" />
                <RowDefinition Height="*" />
            </Grid.RowDefinitions>
            <TextBlock Grid.Column="0" Grid.Row="0" Grid.ColumnSpan="4" HorizontalAlignment="Center"
                   VerticalAlignment="Center" FontSize="144" FontFamily="{StaticResource TitleFontFamily}"
                   Text="Bizio" />

            <Grid Grid.Row="1" Grid.Column="1">
                <Grid.RowDefinitions>
                    <RowDefinition Height="*" />
                    <RowDefinition Height="Auto" />
                </Grid.RowDefinitions>
                <Grid.ColumnDefinitions>
                    <ColumnDefinition Width="*" />
                    <ColumnDefinition Width="*" />
                </Grid.ColumnDefinitions>
                <controls:Accordion Grid.Row="0" Grid.Column="0">
                    <controls:AccordionItem Header="{x:Static z:UiText.InProgress}" Padding="0" IsExpanded="True">
                        <ListView ItemsSource="{Binding Source={StaticResource PlayableGamesView}}"
                                  SelectedItem="{Binding SelectedGameInProgress, Mode=TwoWay}" util:ListViewLayoutManager.Enabled="True">
                            <ListView.View>
                                <GridView>
                                    <GridViewColumn Header="{x:Static z:UiText.Created}" util:ProportionalColumn.Width="2" DisplayMemberBinding="{Binding CreatedDate, StringFormat='g'}" />
                                    <GridViewColumn Header="{x:Static z:UiText.Company}" util:ProportionalColumn.Width="2" DisplayMemberBinding="{Binding CompanyName}" />
                                    <GridViewColumn Header="{x:Static z:UiText.Turn}" util:ProportionalColumn.Width="1" DisplayMemberBinding="{Binding CurrentTurn}" />
                                </GridView>
                            </ListView.View>
                        </ListView>
                    </controls:AccordionItem>
                    <controls:AccordionItem Header="{x:Static z:UiText.Complete}" Padding="0" IsExpanded="False">
                        <ListView ItemsSource="{Binding Source={StaticResource UnplayableGamesView}}"
                                  SelectedItem="{Binding SelectedGameCompleted, Mode=TwoWay}" util:ListViewLayoutManager.Enabled="True">
                            <ListView.View>
                                <GridView>
                                    <GridViewColumn Header="{x:Static z:UiText.Created}" util:ProportionalColumn.Width="2" DisplayMemberBinding="{Binding CreatedDate, StringFormat='g'}" />
                                    <GridViewColumn Header="{x:Static z:UiText.Company}" util:ProportionalColumn.Width="2" DisplayMemberBinding="{Binding CompanyName}" />
                                    <GridViewColumn Header="{x:Static z:UiText.Turn}" util:ProportionalColumn.Width="1" DisplayMemberBinding="{Binding CurrentTurn}" />
                                </GridView>
                            </ListView.View>
                        </ListView>
                    </controls:AccordionItem>
                </controls:Accordion>
                <ContentControl Grid.Column="1" Margin="30,0,0,0" Style="{StaticResource PrimaryContainer}">
                    <Grid Grid.IsSharedSizeScope="True">
                        <Grid.RowDefinitions>
                            <RowDefinition Height="Auto" />
                            <RowDefinition Height="Auto" />
                            <RowDefinition Height="Auto" />
                            <RowDefinition Height="Auto" />
                            <RowDefinition Height="Auto" />
                        </Grid.RowDefinitions>
                        <Grid Grid.Row="0" Margin="0,15,0,0">
                            <Grid.ColumnDefinitions>
                                <ColumnDefinition Width="Auto" SharedSizeGroup="Labels" />
                                <ColumnDefinition Width="*" />
                            </Grid.ColumnDefinitions>
                            <TextBlock Grid.Column="0" Margin="0,0,5,0" VerticalAlignment="Center" Text="{x:Static z:UiText.Company}" />
                            <TextBox Grid.Column="1" IsEnabled="False" Text="{Binding SelectedGame.CompanyName}" />
                        </Grid>
                        <Grid Grid.Row="1" Margin="0,15,0,0">
                            <Grid.ColumnDefinitions>
                                <ColumnDefinition Width="Auto" SharedSizeGroup="Labels" />
                                <ColumnDefinition Width="*" />
                            </Grid.ColumnDefinitions>
                            <TextBlock Grid.Column="0" Margin="0,0,5,0" VerticalAlignment="Center" Text="{x:Static z:UiText.Founder}" />
                            <TextBox Grid.Column="1" IsEnabled="False">
                                <TextBox.Text>
                                    <MultiBinding StringFormat="{}{0} {1}">
                                        <Binding Path="SelectedGame.FounderFirstName" />
                                        <Binding Path="SelectedGame.FounderLastName" />
                                    </MultiBinding>
                                </TextBox.Text>
                            </TextBox>
                        </Grid>
                        <Grid Grid.Row="2" Margin="0,15,0,0">
                            <Grid.ColumnDefinitions>
                                <ColumnDefinition Width="Auto" SharedSizeGroup="Labels" />
                                <ColumnDefinition Width="*" />
                            </Grid.ColumnDefinitions>
                            <TextBlock Grid.Column="0" Margin="0,0,5,0" VerticalAlignment="Center" Text="{x:Static z:UiText.Created}" />
                            <TextBox Grid.Column="1" IsEnabled="False" Style="{StaticResource DateTextBox}" Text="{Binding SelectedGame.CreatedDate, StringFormat='g'}" />
                        </Grid>
                        <Grid Grid.Row="3" Margin="0,15,0,0">
                            <Grid.ColumnDefinitions>
                                <ColumnDefinition Width="Auto" SharedSizeGroup="Labels" />
                                <ColumnDefinition Width="*" />
                            </Grid.ColumnDefinitions>
                            <TextBlock Grid.Column="0" Margin="0,0,5,0" VerticalAlignment="Center" Text="{x:Static z:UiText.Industry}" />
                            <ComboBox Grid.Column="1" IsEnabled="False" SelectedValue="{Binding SelectedGame.IndustryId}"
                              ItemsSource="{Binding StaticData.CompaniesData.Industries}"
                              DisplayMemberPath="Name" SelectedValuePath="Id" />
                        </Grid>
                        <Grid Grid.Row="4" Margin="0,15,0,0">
                            <Grid.ColumnDefinitions>
                                <ColumnDefinition Width="Auto" SharedSizeGroup="Labels" />
                                <ColumnDefinition Width="*" />
                                <ColumnDefinition Width="3*" />
                            </Grid.ColumnDefinitions>
                            <TextBlock Grid.Column="0" Margin="0,0,5,0" VerticalAlignment="Center" Text="{x:Static z:UiText.TurnSpeed}" />
                            <TextBox Grid.Column="1" IsEnabled="False" Style="{StaticResource NumericTextBox}" Text="{Binding SelectedGame.WeeksPerTurn}" />
                            <TextBlock Grid.Column="2" Margin="10,0,0,0" VerticalAlignment="Center" Text="{x:Static z:UiText.WeeksPerTurn}" />
                        </Grid>
                    </Grid>
                </ContentControl>
                <ContentControl Grid.Row="1" Grid.ColumnSpan="2" Margin="0,15,0,0" VerticalAlignment="Top" Style="{StaticResource PrimaryContainer}">
                    <Grid>
                        <Grid.ColumnDefinitions>
                            <ColumnDefinition Width="*" />
                            <ColumnDefinition Width="*" />
                        </Grid.ColumnDefinitions>
                        <Button Grid.Column="0" VerticalAlignment="Center" HorizontalAlignment="Stretch" Command="{Binding BackCommand}" Content="{x:Static z:UiText.Back}" />
                        <Button Grid.Column="1" VerticalAlignment="Center" HorizontalAlignment="Stretch" Command="{Binding LoadGameCommand}" Content="{x:Static z:UiText.Load}" />
                    </Grid>
                </ContentControl>
            </Grid>
        </Grid>
        <Grid VerticalAlignment="Stretch" HorizontalAlignment="Stretch"
              Visibility="{Binding IsBusy, Converter={StaticResource BooleanToVisibilityConverter}}">
            <Canvas Background="Black" Opacity=".25" />
        </Grid>
    </Grid>
</UserControl>
