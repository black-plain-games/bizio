﻿using Bizio.Client.Desktop.Model.Game;
using System.Windows.Controls;
using System.Windows.Data;

namespace Bizio.Client.Desktop.Views
{
    public partial class LoadGameView : UserControl, ILoadGameView
    {
        public LoadGameView()
        {
            InitializeComponent();
        }

        private void OnFilterPlayableGames(object sender, FilterEventArgs e)
        {
            e.Accepted = (e.Item as Game).Status == GameStatus.Playing;
        }

        private void OnFilterUnplayableGames(object sender, FilterEventArgs e)
        {
            e.Accepted = (e.Item as Game).Status == GameStatus.UserLost;
        }
    }
}