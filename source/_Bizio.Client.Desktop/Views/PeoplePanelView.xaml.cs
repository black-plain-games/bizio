﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace Bizio.Client.Desktop.Views
{
    public partial class PeoplePanelView : UserControl
    {
        public PeoplePanelView()
        {
            InitializeComponent();
        }

        private void OnHeaderClicked(object sender, RoutedEventArgs e)
        {
            if (e.OriginalSource is GridViewColumnHeader header && header.Role == GridViewColumnHeaderRole.Normal)
            {
                ListSortDirection direction;

                if (header != _lastHeaderClicked)
                {
                    direction = ListSortDirection.Ascending;
                }
                else
                {
                    direction = _lastDirection == ListSortDirection.Ascending ? ListSortDirection.Descending : ListSortDirection.Ascending;
                }

                var listView = sender as ListView;

                var dataView = CollectionViewSource.GetDefaultView(listView.ItemsSource);

                dataView.SortDescriptions.Clear();

                dataView.SortDescriptions.Add(new SortDescription((string)header.Column.Header, direction));

                dataView.Refresh();

                _lastDirection = direction;

                _lastHeaderClicked = header;
            }
        }

        private GridViewColumnHeader _lastHeaderClicked = null;

        private ListSortDirection _lastDirection;
    }
}