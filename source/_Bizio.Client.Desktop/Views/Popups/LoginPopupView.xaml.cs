﻿using System.Windows.Controls;

namespace Bizio.Client.Desktop.Views.Popups
{
    /// <summary>
    /// Interaction logic for LoginPopupView.xaml
    /// </summary>
    public partial class LoginPopupView : UserControl
    {
        public LoginPopupView()
        {
            InitializeComponent();
        }
    }
}
