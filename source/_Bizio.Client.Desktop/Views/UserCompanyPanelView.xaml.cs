﻿using Bizio.Client.Desktop.Converters;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;

namespace Bizio.Client.Desktop.Views
{
    public partial class UserCompanyPanelView : UserControl
    {
        public UserCompanyPanelView()
        {
            InitializeComponent();
        }

        private void OnSalaryLostFocus(object sender, RoutedEventArgs e)
        {
            var textBox = sender as TextBox;

            if (!decimal.TryParse(textBox.Text, NumberStyles.Currency, Binding.GlobalCulture, out _))
            {
                textBox.Text = string.Format(Binding.GlobalCulture, "{0:C}", 0);
            }
        }
    }
}