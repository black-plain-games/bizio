﻿using Bizio.Client.Desktop.Views.Popups;
using BlackPlain.DI;

namespace Bizio.Client.Desktop.Views
{
    public class ViewBundle : IResolverBundle
    {
        public void CreateResolvers(IHub hub)
        {
            hub.CreateResolver<IDeveloperView, DeveloperView>();

            hub.CreateResolver<IPopupPresenterView, PopupPresenterView>();

            hub.CreateResolver<IMainMenuView, MainMenuView>();

            hub.CreateResolver<ILoadGameView, LoadGameView>();

            hub.CreateResolver<INewGameView, NewGameView>();

            hub.CreateResolver<ISettingsView, SettingsView>();

            hub.CreateResolver<IGameLayoutView, GameLayoutView>();

            hub.CreateResolver<INotificationsView, NotificationsView>();
        }

        public void SetResolutions(IHub hub)
        {
        }
    }
}