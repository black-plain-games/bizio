Param(
    [string] $xunitPath
)

$tests = Get-ChildItem -Recurse -Path source\*.Tests\bin\debug\ -Filter *.Tests.dll
$failures = ""
foreach ($test in $tests) {
    $output = source/packages/xunit.runner.console.2.3.1/tools/net452/xunit.console.exe $test.FullName

    $outputLines = $output.Split("`n")

    $result = $outputLines[$outputLines.Length - 1]
    
    if (-Not ($result -match "Errors: 0, Failed: 0,")) {
        $failures += $result + "`n"
    }
}

if($failures.Length -gt 0){
    Exit 1
}